SELECT
 sfo.increment_id AS "Order Number"
 , sfo.entity_id AS order_view
 , CONVERT_TZ(sfo.created_at, '+0:00', 'America/Los_Angeles') AS "Order Date"
 , sfo.status
 , DATE(CONVERT_TZ(sfo.captured_at, '+0:00', 'America/Los_Angeles')) AS "Billed Date"
 , CONVERT_TZ(sfo.created_at, '+0:00', 'America/Los_Angeles') AS "Redeemed Date"
 , giftcard_amount AS "Amount Redeemed"
 , RIGHT(giftcard_number, 10) AS "Gift Card Number"
 , ggo.notes AS "Comments"
 , fa.created_by AS "User Name"
FROM
 sales_flat_order AS sfo
LEFT OUTER JOIN
 gosolid_givex_giftcard AS ggo
ON
 ggo.card_number = sfo.giftcard_number
LEFT OUTER JOIN
 frans_activity AS fa
ON
 fa.entity_id = sfo.entity_id
AND
 fa.activity_type = 'order_created'
AND
 fa.entity_type = 'sales/order'
WHERE
 LENGTH(sfo.giftcard_number) > 0
AND
 DATE(CONVERT_TZ(sfo.created_at, '+0:00', 'America/Los_Angeles')) BETWEEN '{Start date:date}' AND '{End date:date}'