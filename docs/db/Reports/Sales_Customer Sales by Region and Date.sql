SELECT o.customer_id, CONCAT(ba.firstname, ' ', ba.lastname) AS 'Billing Address Name', SUM(o.base_subtotal + o.base_discount_amount)
FROM 
sales_flat_order AS o
INNER JOIN 
sales_flat_order_address AS ba
ON ba.entity_id = o.billing_address_id
WHERE 
DATE(CONVERT_TZ(o.created_at, '-0:00', 'America/Los_Angeles')) BETWEEN '{From Date:date}' AND '{To Date:date}'
AND
COALESCE(o.multiship_parent_id, 0) = 0
AND
COALESCE(o.customer_id, 0) > 0
AND
ba.region IN ({Billing State:multiselect[usregions]})
AND
o.status NOT IN ('canceled')
GROUP BY o.customer_id
HAVING SUM(o.base_subtotal + o.base_discount_amount) > {Min Spend:number}
