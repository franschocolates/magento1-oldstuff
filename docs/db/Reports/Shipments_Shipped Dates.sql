SELECT
	DATE(CONVERT_TZ(sfs.created_at, '+0:00', 'America/Los_Angeles')) AS "Ship Date"
	, COUNT(*) AS "Total Shipments"
	, CONCAT('$', FORMAT(SUM(sfo.base_shipping_amount),2)) AS "Shipping Total"
	, CONCAT('$', FORMAT(SUM(sfo.base_subtotal + IFNULL(sfo.base_discount_amount,0)),2)) AS "Grand Total (No Shipping)"
FROM
	sales_flat_shipment AS sfs
INNER JOIN sales_flat_order AS sfo
    ON sfo.entity_id = sfs.order_id
WHERE sfo.accounting_label IN ({Accounting Label:multiselect[SELECT label_name AS `label`, label_name AS `value` FROM `frans_accounting_label`]})
    AND sfo.origination IN ({Origination:multiselect[SELECT origination AS `label`, origination AS `value` FROM `sales_flat_order` GROUP BY origination]})
	AND DATE(CONVERT_TZ(sfs.created_at, '+0:00', 'America/Los_Angeles')) BETWEEN '{Start Date:date}' AND '{End Date:date}'
GROUP BY
    DATE(CONVERT_TZ(sfs.created_at, '+0:00', 'America/Los_Angeles'))