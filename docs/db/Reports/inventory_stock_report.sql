SELECT
	cpe.sku AS 'SKU'
	, cpev_name.value AS 'Name'
	, CASE
		WHEN (config_stock.value = 1) THEN	/* Magento Manage Stock set to YES */
			CASE
				WHEN csi.use_config_manage_stock = 1 THEN	/* Product Use Config checked */
					CASE
						WHEN csi.manage_stock = 1 AND csi.is_in_stock = 1 THEN 'In Stock'
						WHEN csi.manage_stock = 1 AND csi.is_in_stock = 0 THEN 'Out of Stock'
						WHEN csi.manage_stock = 0 AND csi.is_in_stock = 1 THEN 'In Stock'
						WHEN csi.manage_stock = 0 AND csi.is_in_stock = 0 THEN 'Out of Stock'
					END
				ELSE	/* Product Use Config unchecked */
					CASE
						WHEN csi.manage_stock = 1 AND csi.is_in_stock = 1 THEN 'In Stock'
						WHEN csi.manage_stock = 1 AND csi.is_in_stock = 0 THEN 'Out of Stock'
						WHEN csi.manage_stock = 0 AND csi.is_in_stock = 1 THEN 'In Stock'
						WHEN csi.manage_stock = 0 AND csi.is_in_stock = 0 THEN 'In Stock'
					END
			END
		ELSE	/* Magento Manage Stock set to NO */
			CASE
				WHEN csi.use_config_manage_stock = 1 THEN	/* Product Use Config checked */
					CASE
						WHEN csi.manage_stock = 1 AND csi.is_in_stock = 1 THEN 'In Stock'
						WHEN csi.manage_stock = 1 AND csi.is_in_stock = 0 THEN 'In Stock'
						WHEN csi.manage_stock = 0 AND csi.is_in_stock = 1 THEN 'In Stock'
						WHEN csi.manage_stock = 0 AND csi.is_in_stock = 0 THEN 'In Stock'
					END
				ELSE	/* Product Use Config unchecked */
					CASE
						WHEN csi.manage_stock = 1 AND csi.is_in_stock = 1 THEN 'In Stock'
						WHEN csi.manage_stock = 1 AND csi.is_in_stock = 0 THEN 'Out of Stock'
						WHEN csi.manage_stock = 0 AND csi.is_in_stock = 1 THEN 'In Stock'
						WHEN csi.manage_stock = 0 AND csi.is_in_stock = 0 THEN 'In Stock'
					END
			END
	END AS 'Availability'
	, CASE
		WHEN (config_stock.value = 1) THEN	/* Mangento Manage Stock set to YES */
			CASE
				WHEN csi.use_config_manage_stock = 1 AND csi.manage_stock = 1 THEN 'Yes'
				WHEN csi.use_config_manage_stock = 0 AND csi.manage_stock = 1 THEN 'Yes'
				WHEN csi.use_config_manage_stock = 0 AND csi.manage_stock = 0 THEN 'No'
				ELSE 'Yes'
			END
		ELSE	/* Mangento Manage Stock set to NO */
			CASE
				WHEN csi.use_config_manage_stock = 1 AND csi.manage_stock = 0 THEN 'No'
				WHEN csi.use_config_manage_stock = 0 AND csi.manage_stock = 1 THEN 'Yes'
				WHEN csi.use_config_manage_stock = 0 AND csi.manage_stock = 0 THEN 'No'
				ELSE 'No'
			END
	END AS 'Manage Stock'
	, csi.qty AS 'Stock QTY'
	, IF(COALESCE(cpei_cto.value, 0) = 1, 'Yes', 'No') AS 'Call to Order'
	, CASE cpei_visibility.value
		WHEN 1 THEN 'None'
		WHEN 2 THEN 'Catalog'
		WHEN 3 THEN 'Search'
		WHEN 4 THEN 'Catalog,Search'
	END AS 'Visibility'
	, IF(cpei_status.value = 1, 'Enabled', 'Disabled') AS 'Status'
	/* Retrieve prices - Added 10/2/14 */
	, cped.value AS 'Price'
	/* End revision 10/2/2014 */
	, cpe.accpac_sku
FROM
	catalog_product_entity AS cpe /* we cant use flat because flat doesnt have disabled products */
/* always have a name*/
INNER JOIN
	catalog_product_entity_varchar AS cpev_name
ON
	cpev_name.entity_id = cpe.entity_id
AND
	cpev_name.attribute_id = (
		SELECT attribute_id 
		FROM eav_attribute 
		WHERE attribute_code = 'name' 
		AND entity_type_id = (SELECT entity_type_id FROM eav_entity_type WHERE entity_type_code='catalog_product')
	)
/* always have a status */
INNER JOIN
	catalog_product_entity_int AS cpei_status
ON
	cpei_status.entity_id = cpe.entity_id
AND
	cpei_status.attribute_id = (
		SELECT attribute_id 
		FROM eav_attribute 
		WHERE attribute_code = 'status' 
		AND entity_type_id = (SELECT entity_type_id FROM eav_entity_type WHERE entity_type_code='catalog_product')
	)
/* always have a visibility */
INNER JOIN
	catalog_product_entity_int AS cpei_visibility
ON
	cpei_visibility.entity_id = cpe.entity_id
AND
	cpei_visibility.attribute_id = (
		SELECT attribute_id 
		FROM eav_attribute 
		WHERE attribute_code = 'visibility' 
		AND entity_type_id = (SELECT entity_type_id FROM eav_entity_type WHERE entity_type_code='catalog_product')
	)
INNER JOIN
	cataloginventory_stock_item AS csi
ON
	csi.product_id = cpe.entity_id
INNER JOIN
	core_config_data AS config_stock
ON
	config_stock.path = 'cataloginventory/item_options/manage_stock' /* this will do a cross join, which is fine */
LEFT OUTER JOIN
	catalog_product_entity_int AS cpei_cto
ON
	cpei_cto.entity_id = cpe.entity_id
AND
	cpei_cto.attribute_id = (
		SELECT attribute_id 
		FROM eav_attribute 
		WHERE attribute_code = 'call_to_order' 
		AND entity_type_id = (SELECT entity_type_id FROM eav_entity_type WHERE entity_type_code='catalog_product')
	)
/* Retrieve prices - Added 10/2/2014 */
LEFT JOIN
	catalog_product_entity_decimal AS cped
ON
	cped.entity_id = cpe.entity_id AND cped.attribute_id IN(SELECT `attribute_id` FROM eav_attribute WHERE `attribute_code` = 'price')
	
GROUP BY cpe.sku
/* End changes 10/2/2014 */
ORDER BY cpe.sku