
## Pre SQL
## Firts create a temp table of duplicate order quotes

CREATE TEMPORARY TABLE IF NOT EXISTS  sales_duplicate_order_quote_ids ENGINE=MEMORY 

AS (

 SELECT q.entity_id AS "Quote ID" 

 FROM   sales_flat_quote AS q 

        LEFT JOIN sales_flat_order AS o 

       ON o.quote_id = q.entity_id 

 WHERE  ( o.is_multiship_parent = 1 

   OR ( o.is_multiship_parent IS NULL 

        AND o.multiship_parent_id IS NULL ) ) # 

        AND o.origination = 'WEB' 

 GROUP  BY q.entity_id 

 HAVING COUNT(o.increment_id) > 1 

 ORDER  BY q.entity_id DESC

);

## Report SQL Query
## getting all orders from the orders that belong to a duplicate quote_id

SELECT CONVERT_TZ(sfo.created_at, '+0:00', 'America/Los_Angeles') AS `order_created_date`, 
       CONCAT('<a target="_blank" href="/index.php/admin/sales_order/view/order_id/',sfo.entity_id,'" >',sfo.increment_id,'</a>' ) AS `order`, 
       sfo.increment_id AS `order id`, 
       sfo.quote_id, 
       IF( STRCMP( IFNULL(sfo.customer_id,'NA'),'NA'),CONCAT('<a target="_blank" href="/index.php/admin/customer/edit/id/',sfo.customer_id,'" >',sfo.customer_id,'</a>' ),'') AS `customer`,
       IF( STRCMP( IFNULL(sfo.customer_id,'NA'),'NA'), sfo.customer_id,'') AS `customer id`,
       sfo.customer_email, 
       CONCAT(sfo.customer_firstname, ' ', sfo.customer_lastname) AS 
       'customer name' 
FROM   sales_flat_order AS sfo
RIGHT JOIN sales_duplicate_order_quote_ids AS doqi ON sfo.quote_id = doqi.`Quote ID`
WHERE  DATE(CONVERT_TZ(sfo.created_at, '+0:00', 'America/Los_Angeles')) BETWEEN  '{Order Start Date:date}' AND '{Order End Date:date}'
	AND ( is_multiship_parent = 1 
              OR ( is_multiship_parent IS NULL 
                   AND multiship_parent_id IS NULL ) ) 
GROUP  BY increment_id 
ORDER  BY quote_id
