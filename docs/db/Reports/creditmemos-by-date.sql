SELECT
	DATE( sfo.captured_at ) AS "Order Captured Date"

	, GROUP_CONCAT( DATE( CONVERT_TZ( sfc.created_at, "+0:00", "America/Los_Angeles" ) ) ) AS "Credit Memo(s) Date"
	, COUNT( sfo.entity_id ) AS "Total Orders"
	, ROUND( SUM( sfc.base_subtotal ), 2 ) AS "Subtotal ($)"
	, ROUND( SUM( sfc.base_shipping_amount ), 2 ) AS "Shipping ($)"
	, ROUND( SUM( sfc.base_grand_total ), 2 ) AS "Grand Total ($)"

FROM
	sales_flat_order AS sfo
INNER JOIN sales_flat_creditmemo AS sfc
	ON sfo.entity_id = sfc.order_id

WHERE
	DATE( sfc.created_at ) BETWEEN '{Credit Memo Start Date:date}' AND '{Credit Memo End Date:date}'
	#DATE( sfc.created_at ) BETWEEN '2014-01-01' AND '2014-12-30'
	AND sfo.accounting_label IN ({Accounting Label:multiselect[SELECT label_name AS `label`, label_name AS `value` FROM `frans_accounting_label`]})
	AND sfo.origination IN ({Origination:multiselect[SELECT origination_name AS 'label', origination_name AS 'value' FROM frans_accounting_origination]})
	AND (sfo.is_multiship_parent = 1 OR sfo.is_multiship_parent IS NULL)

GROUP BY
	DATE( sfo.captured_at )

ORDER BY
	DATE( sfo.captured_at ) ASC


/* Columns to Total */

Total Orders
Subtotal ($)
Shipping ($)
Grand Total ($)