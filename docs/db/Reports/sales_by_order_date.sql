SELECT DATE(CONVERT_TZ(sfo.created_at, '+0:00', 'America/Los_Angeles')) AS "Create Date" ,
	COUNT(sfo.entity_id) AS "Total Orders",
	IFNULL(tsfo.shipments,0) AS "Total Shipments" , 
	CONCAT('$', FORMAT(SUM(sfo.base_subtotal + IFNULL(sfo.base_discount_amount,0)),2)) AS "Sales", 
	CONCAT('$', FORMAT(SUM(sfo.base_shipping_amount),2)) AS "Shipping" ,
	CONCAT('$', FORMAT(SUM(sfo.base_grand_total),2)) AS "Grand Total" 
FROM sales_flat_order AS sfo 
LEFT JOIN ( 
	SELECT 
		COUNT(entity_id) AS shipments,
		accounting_label,
		planned_ship_date,
		DATE(CONVERT_TZ(created_at, '+0:00', 'America/Los_Angeles')) AS created_at
	FROM sales_flat_order
	WHERE (is_multiship_parent = 0 OR is_multiship_parent IS NULL)
		AND STATUS IN  ({ORDER STATUS:multiselect[SELECT UPPER(REPLACE(`status`,'_',' ')) AS `label`, `status` AS `value` FROM sales_flat_order GROUP BY `status`]})
                AND accounting_label IN ({Accounting Label:multiselect[SELECT label_name AS `label`, label_name AS `value` FROM `frans_accounting_label` ORDER BY FIELD(label_name,'BV','DT','GTR','UV','MFG','BUS','MO','WEB','GTW')]})
                AND origination IN ({Origination:multiselect[SELECT origination AS `label`, origination AS `value` FROM `sales_flat_order` GROUP BY origination ORDER BY FIELD(origination,'BELL','DT','GTR','UV','MO','WEB','MFG','GTW')]})
	GROUP BY 
		DATE(CONVERT_TZ(created_at, '+0:00', 'America/Los_Angeles'))
) AS tsfo 
ON tsfo.created_at= DATE(CONVERT_TZ(sfo.created_at, '+0:00', 'America/Los_Angeles'))
WHERE
	DATE(CONVERT_TZ(sfo.created_at, '+0:00', 'America/Los_Angeles')) BETWEEN  '{Order Start Date:date}' AND '{Order End Date:date}'
	AND sfo.status IN ({ORDER STATUS:multiselect[SELECT UPPER(REPLACE(`status`,'_',' ')) AS `label`, `status` AS `value` FROM sales_flat_order GROUP BY `status`]})
        AND sfo.accounting_label IN ({Accounting Label:multiselect[SELECT label_name AS `label`, label_name AS `value` FROM `frans_accounting_label` ORDER BY FIELD(label_name,'BV','DT','GTR','UV','MFG','BUS','MO','WEB','GTW')]})
        AND sfo.origination IN ({Origination:multiselect[SELECT origination AS `label`, origination AS `value` FROM `sales_flat_order` GROUP BY origination ORDER BY FIELD(origination,'BELL','DT','GTR','UV','MO','WEB','MFG','GTW')]})
	AND (sfo.is_multiship_parent = 1 OR sfo.is_multiship_parent IS NULL)	
GROUP BY 
	DATE(CONVERT_TZ(sfo.created_at, '+0:00', 'America/Los_Angeles'))