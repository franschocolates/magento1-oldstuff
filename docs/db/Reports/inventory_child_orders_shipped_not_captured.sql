#Child Orders Shipped NotCaptured, is 4 unioned queries


# single Shipment Orders = shipped but not captured
SELECT
	* 
FROM
	sales_flat_order
WHERE
	shipping_method NOT LIKE 'pickupatstore_%'
AND	
	ship_date IS NOT NULL 
AND 
	captured_at IS NULL
AND
	is_multiship_parent IS NULL
AND
	multiship_parent_id IS NULL
UNION	

# Multi Shipment Orders = Any child shipped and Parent not captured
SELECT
	sfoChild.*
FROM
	sales_flat_order AS sfoParent
LEFT JOIN
	sales_flat_order AS sfoChild ON sfoParent.entity_id = sfoChild.multiship_parent_id AND sfoChild.ship_date IS NOT NULL
WHERE
	sfoParent.is_multiship_parent IS NOT NULL
AND
	sfoChild.multiship_parent_id IS NOT NULL
AND
	sfoParent.captured_at IS NULL
UNION	

# Pick Up Orders = Ready to be picked up and not captured
SELECT
	*
FROM
	sales_flat_order
WHERE
	`status` = 'ready_for_pickup'
AND
	shipping_method LIKE 'pickupatstore_%'
AND 
	captured_at IS NULL
AND
	is_multiship_parent IS NULL
AND
	multiship_parent_id IS NULL
UNION	
	
# Child Pick Up Orders = Ready to be picked up and not captured
SELECT
	sfoChild.*
FROM
	sales_flat_order AS sfoParent
LEFT JOIN
	sales_flat_order AS sfoChild ON sfoParent.entity_id = sfoChild.multiship_parent_id AND sfoChild.ship_date IS NOT NULL
WHERE
	sfoParent.is_multiship_parent IS NOT NULL
AND
	sfoChild.multiship_parent_id IS NOT NULL
AND
	sfoChild.shipping_method LIKE 'pickupatstore_%'
AND 
	sfoChild.captured_at IS NULL
AND
	sfoChild.multiship_parent_id IS NOT NULL
