SELECT
	  sfoi.sku AS "SKU"
	, sfoi.name AS "Product Name"
	, SUM(sfoi.qty_ordered) AS "Quantity"
	, sfo.planned_ship_date AS "Ship Date"
	, IF (COUNT(DISTINCT sfo.increment_id) > 1,
		CONCAT(COUNT(DISTINCT sfo.increment_id), ' shipments'),
		CONCAT('#',sfo.increment_id)
	  ) AS 'Order #'
	, sfoi.notes AS "Product Notes"
	, sfoi.`accpac_sku` AS "ACCPAC SKU"
FROM sales_flat_order_item AS sfoi
INNER JOIN sales_flat_order AS sfo
ON sfoi.order_id = sfo.entity_id
WHERE sfo.is_printed = 0
AND sfo.planned_ship_date BETWEEN '{Start Planned Ship Date:date}' AND '{End Planned Ship Date:date}'
AND sfo.status IN ('ready_for_print','ready_for_review','processing')
GROUP BY sfoi.`sku`, sfoi.`name`, sfo.planned_ship_date, sfoi.notes, sfoi.`accpac_sku`
ORDER BY sfo.planned_ship_date DESC, sfo.entity_id DESC