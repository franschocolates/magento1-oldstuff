===================================
SQL QUERY
===================================
/* finally, return just joins the two temp tables with some grouping */
SELECT 
	increment_id AS 'Invoice #'
	, order_date AS 'Order Date'
	, ship_date AS 'Ship Date'
	, item_totals.skus AS 'Item #'
	, item_totals.product_names AS 'Item Description'
	, item_totals.qtys AS 'Quantity'
	, item_totals.prices AS 'Item(s) Price'
	, item_totals.donation_amount
	, b_name AS 'Sold To Name'
	, b_street AS 'Address'
	, b_city AS 'City'
	, b_state AS 'State'
	, b_postcode AS 'Zip Code'
	, b_telephone AS 'Phone Number'
	, customer_email
	, s_name
	, s_state
	, item_totals.accpac_skus AS 'ACCPAC Item #'
FROM
	tmp_SafeCrossing_Orders AS tmpo
INNER JOIN
(
	SELECT order_id
		, GROUP_CONCAT(tmpoi.sku SEPARATOR '
') AS skus
		, GROUP_CONCAT(tmpoi.accpac_sku SEPARATOR '
') AS accpac_skus
		, GROUP_CONCAT(tmpoi.name SEPARATOR '
') AS product_names
		, GROUP_CONCAT(tmpoi.qty SEPARATOR '
') AS qtys
		, GROUP_CONCAT((tmpoi.price * tmpoi.qty) SEPARATOR '
') AS prices
		, SUM(tmpoi.donation * tmpoi.qty) AS donation_amount
	FROM 
		tmp_SafeCrossing_OrderItems AS tmpoi
	GROUP BY tmpoi.order_id
) AS item_totals
ON
	item_totals.order_id = tmpo.order_id;


===================================================
Pre SQL QUERY (IS run BEFORE the QUERY)
===================================================

DROP TEMPORARY TABLE IF EXISTS tmp_SafeCrossing_OrderItems;
DROP TEMPORARY TABLE IF EXISTS tmp_SafeCrossing_Orders;

CREATE TEMPORARY TABLE tmp_SafeCrossing_OrderItems
(
	order_id INT NOT NULL,
	increment_id VARCHAR(50) NOT NULL,
	sku VARCHAR(64) NOT NULL,
	`name` VARCHAR(255) NOT NULL,
	qty INT NOT NULL,
	price DECIMAL(12,4) NOT NULL,
	donation DECIMAL(12,4) NOT NULL,
	accpac_sku VARCHAR(64) NOT NULL,
	PRIMARY KEY (order_id, sku)
);

CREATE TEMPORARY TABLE tmp_SafeCrossing_Orders
(
	order_id INT NOT NULL,
	increment_id VARCHAR(50) NOT NULL,
	order_date DATE NOT NULL,
	ship_date DATE NOT NULL,
	b_name VARCHAR(100) NOT NULL,
	b_street VARCHAR(255) NOT NULL,
	b_city VARCHAR(50) NOT NULL,
	b_state VARCHAR(2) NOT NULL,
	b_postcode VARCHAR(10) NOT NULL,
	b_telephone VARCHAR(50) NULL,
	customer_email VARCHAR(255) NOT NULL,
	s_name VARCHAR(100) NOT NULL,
	s_state VARCHAR(10) NOT NULL,
	
	PRIMARY KEY (order_id)
);

/* find any shipped items that are safe crossings and have an invoice */
INSERT INTO tmp_SafeCrossing_OrderItems
(
	order_id, increment_id, sku, `name`, qty, price, donation, accpac_sku
)
SELECT
	sfoi.order_id
	, sfo.increment_id
	, sfoi.sku
	, sfoi.name
	, sfoi.qty_shipped
	, sfoi.price
	, cped_don.`value`
	, sfoi.accpac_sku
FROM
	sales_flat_order_item AS sfoi
INNER JOIN
	catalog_product_entity_int AS cped_sc
ON
	cped_sc.entity_id = sfoi.product_id
AND
	cped_sc.attribute_id = (
		SELECT attribute_id 
		FROM eav_attribute ea 
		INNER JOIN eav_entity_type AS eet 
		ON eet.entity_type_id = ea.entity_type_id 
		WHERE ea.attribute_code = 'is_safe_crossings_product' 
		AND eet.entity_type_code='catalog_product'
	)
	AND
		cped_sc.value = 1
	LEFT OUTER JOIN
		catalog_product_entity_decimal AS cped_don
	ON
		cped_don.entity_id = sfoi.product_id
	AND
		cped_don.attribute_id = (
			SELECT attribute_id 
			FROM eav_attribute ea 
			INNER JOIN eav_entity_type AS eet 
			ON eet.entity_type_id = ea.entity_type_id 
			WHERE ea.attribute_code = 'safe_crossings_donation' 
			AND eet.entity_type_code='catalog_product'
		)
		INNER JOIN
			sales_flat_order AS sfo
		ON
			sfo.entity_id = sfoi.order_id
		LEFT OUTER JOIN
			sales_flat_order AS sfp
		ON
			sfp.entity_id = sfo.multiship_parent_id
		INNER JOIN 
			sales_flat_invoice AS sfi
		ON
			sfi.order_id = COALESCE(sfp.entity_id, sfo.entity_id)
		WHERE
			DATE(sfo.ship_date) BETWEEN '{Start Ship date:date}' AND '{End Ship date:date}'
		AND
			(sfo.is_multiship_parent = 0 OR sfo.is_multiship_parent IS NULL);

/* make a table with the main order information */
INSERT INTO tmp_SafeCrossing_Orders
SELECT sfo.entity_id
	, sfo.increment_id
	, DATE(sfo.created_at)
	, sfo.ship_date
	, CONCAT(sfoa_bill.firstname, ' ', sfoa_bill.lastname)
	, sfoa_bill.street
	, sfoa_bill.city
	, sfoa_bill_region.code
	, sfoa_bill.postcode
	, sfoa_bill.telephone
	, sfo.customer_email
	, CONCAT(sfoa_ship.firstname, ' ', sfoa_ship.lastname)
	, sfoa_ship_region.code
	
FROM
	sales_flat_order AS sfo
LEFT OUTER JOIN
	sales_flat_order AS sfp
ON
	sfp.entity_id = sfo.multiship_parent_id
INNER JOIN
	sales_flat_order_address AS sfoa_bill
ON
	sfoa_bill.entity_id = COALESCE(sfp.billing_address_id, sfo.billing_address_id)
INNER JOIN
	directory_country_region AS sfoa_bill_region
ON
	sfoa_bill_region.region_id = sfoa_bill.region_id
INNER JOIN
	sales_flat_order_address AS sfoa_ship
ON
	sfoa_ship.entity_id = sfo.shipping_address_id
INNER JOIN
	directory_country_region AS sfoa_ship_region
ON
	sfoa_ship_region.region_id = sfoa_ship.region_id
RIGHT JOIN
	tmp_SafeCrossing_OrderItems AS tscoi
ON
	sfo.entity_id = tscoi.order_id
GROUP BY entity_id