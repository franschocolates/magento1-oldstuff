SELECT
	DATE(CONVERT_TZ(sfo.captured_at, '+0:00', 'America/Los_Angeles')) AS "Captured Date"
	, COUNT(*) AS "Total Orders"
	, (SELECT COUNT(*)
	   FROM sales_flat_shipment
	   WHERE ship_date=DATE(CONVERT_TZ(sfo.captured_at, '+0:00', 'America/Los_Angeles'))) AS "Total Shipments"

	, CONCAT('$', FORMAT(SUM(sfo.base_subtotal + IFNULL(sfo.base_discount_amount,0)),2)) AS "Subtotal"
	, CONCAT('$', FORMAT(SUM(sfo.base_shipping_amount),2)) AS "Shipping"
	, CONCAT('$', FORMAT(SUM(sfo.base_grand_total + IFNULL(sfo.base_giftcard_amount,0)),2)) AS "Grand Total"
FROM
	sales_flat_order AS sfo
WHERE
	sfo.accounting_label IN ({Accounting Label:multiselect[SELECT label_name AS `label`, label_name AS `value` FROM `frans_accounting_label`]})
	AND sfo.origination IN ({Origination:multiselect[SELECT origination AS `label`, origination AS `value` FROM `sales_flat_order` GROUP BY origination]})
	AND DATE(CONVERT_TZ(sfo.captured_at, '+0:00', 'America/Los_Angeles')) BETWEEN '{Start Date:date}' AND '{End Date:date}'
	AND sfo.status IN ({ORDER STATUS:multiselect[SELECT UPPER(REPLACE(`status`,'_',' ')) AS `label`, `status` AS `value` FROM sales_flat_order GROUP BY `status`]})
GROUP BY
	DATE(CONVERT_TZ(sfo.captured_at, '+0:00', 'America/Los_Angeles'));