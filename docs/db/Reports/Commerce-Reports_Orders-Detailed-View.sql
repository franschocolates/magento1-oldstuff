SELECT
	sfo.accounting_label
	, sfo.`status`
	, sfo.increment_id AS "Order Shipment #"
	, sfo.entity_id AS order_view
	, CONVERT_TZ(sfo.created_at, '+0:00', 'America/Los_Angeles') AS "order date"
	, DATE(CONVERT_TZ(sfo.ship_date, '+0:00', 'America/Los_Angeles')) AS "Ship Date"
	, sfoa_bill.firstname AS "Billing First Name"
	, sfoa_bill.lastname AS "Billing Last Name"
	, sfoa_bill.company AS "Billing Company"
	, sfo.customer_email AS "email"
	, sfoa_ship.firstname AS "Recipient First Name"
	, sfoa_ship.lastname AS "Recipient Last Name"
	, sfo.subtotal AS subtotal
	, sfo.shipping_amount AS "Shipping Cost"
	, sfo.grand_total AS "Order Grand Total"
	, sfo.shipping_description AS "Shipping Method"
	, ccd_payment.`value` AS "Payment Type"
	, sfop.last_trans_id AS "Authcode"
	, fa.created_by AS "Placed By"
FROM
	sales_flat_order AS sfo
INNER JOIN
	sales_flat_order_address AS sfoa_ship
ON
	sfoa_ship.parent_id = sfo.entity_id /* this should filter out multiship, they have no shipping */
AND
	sfoa_ship.address_type = 'shipping'
LEFT OUTER JOIN
	sales_flat_order AS sfp
ON
	sfp.entity_id = sfo.multiship_parent_id
LEFT OUTER JOIN
	sales_flat_order_address AS sfoa_bill
ON
	sfoa_bill.parent_id = COALESCE(sfp.entity_id, sfo.entity_id)
AND
	sfoa_bill.address_type = 'billing'
LEFT OUTER JOIN
	sales_flat_order_payment AS sfop
ON
	sfop.parent_id = COALESCE(sfp.entity_id, sfo.entity_id)
LEFT OUTER JOIN
	core_config_data AS ccd_payment
ON
	ccd_payment.path = CONCAT('payment/', sfop.method, '/title')
AND
	ccd_payment.scope = 'default'
LEFT OUTER JOIN
	frans_activity AS fa
ON
	fa.entity_id = COALESCE(sfp.entity_id, sfo.entity_id)
AND
	fa.entity_type = 'sales/order'
AND
	fa.activity_type = 'order_created'
WHERE
	CONVERT_TZ(sfo.created_at, '+0:00', 'America/Los_Angeles') BETWEEN '{Start date:date}' AND '{End date:date}'
AND
sfo.accounting_label IN ({Accounting label:multiselect[SELECT label_name AS `label`, label_name AS `value` FROM `frans_accounting_label`]})