#Pre-SQL

DROP TEMPORARY TABLE IF EXISTS unique_sales_flat_billing_address;

CREATE TEMPORARY TABLE IF NOT EXISTS unique_sales_flat_billing_address AS (
SELECT * FROM (SELECT * FROM `sales_flat_order_address` ORDER BY entity_id DESC) AS sfoasub ORDER BY entity_id DESC
);

#end of Pre-SQL

#Customer subscription status who has made a purchase
SELECT
	sfoa.firstname AS "Billing First Name"
	, sfoa.lastname AS "Billing Last Name"
	, sfoa.company AS "Company Name"
	, CONCAT(sfoa.street, ", ", sfoa.city, " ", sfoa.region, ", ", sfoa.postcode, ", ", sfoa.country_id) AS "Billing Address"
	, sfoa.telephone AS "Billing Telephone"
	, sfo.customer_email AS "Email Address"
	, CONCAT('$', FORMAT((SUM(sfo.base_subtotal) - SUM(IF(sfo.base_subtotal_refunded IS NULL, 0, sfo.base_subtotal_refunded)))  + (SUM(sfo.base_discount_amount) + SUM(IF(sfo.base_discount_refunded IS NULL, 0, sfo.base_discount_refunded))),2)) AS "Subtotal"
	, IF(subscription.value LIKE '%2%', 'Yes', 'No') AS "Print Catalog"
	, IF(subscription.value LIKE '%1%', 'Yes', 'No') AS "Newsletter"

FROM
	sales_flat_order AS sfo 
	
	RIGHT JOIN
		unique_sales_flat_billing_address AS sfoa
		ON sfo.billing_address_id = sfoa.entity_id
		
	LEFT JOIN
		(SELECT
			ce.entity_id
			, cev.value

		FROM
			customer_entity_varchar AS cev
			
			INNER JOIN
				eav_attribute AS ea
				ON ea.attribute_id = cev.attribute_id

			INNER JOIN
				customer_entity AS ce
				ON ce.entity_id = cev.entity_id

		WHERE
			ea.attribute_code = 'subscriptions') AS subscription
		ON subscription.entity_id = sfo.customer_id

WHERE
	CONVERT_TZ(sfo.created_at, 'GMT', 'America/Los_Angeles') BETWEEN '{Order Start Date:date} 00:00:00' AND '{Order End Date:date} 23:59:59'
	AND sfo.status != 'canceled'

GROUP BY
	sfo.customer_id
	

ORDER BY
	sfoa.firstname, sfoa.lastname