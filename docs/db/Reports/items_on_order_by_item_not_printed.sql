SELECT
	  `sku`
	, `name` AS 'Product Name'
	, SUM(sfoi.qty_ordered - (sfoi.qty_shipped + sfoi.qty_canceled)) AS 'Quantity'
	, accpac_sku
FROM
	sales_flat_order_item AS sfoi
INNER JOIN
	sales_flat_order AS sfo
ON
	sfo.entity_id = sfoi.order_id
WHERE
	(sfoi.qty_ordered - (sfoi.qty_shipped + sfoi.qty_canceled)) > 0
AND
	sfo.`status` IN ('processing', 'ready_for_review', 'ready_to_print', 'holded')
AND
	DATE(sfo.planned_ship_date) BETWEEN '{Start Planned Ship Date:date}' AND '{End Planned Ship Date:date}'
GROUP BY
	sfoi.`sku`, sfoi.`name`, accpac_sku
ORDER BY
	SUM(sfoi.qty_ordered - (sfoi.qty_shipped + sfoi.qty_canceled)) DESC