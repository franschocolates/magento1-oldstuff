SELECT
 sfo.increment_id AS "Order Number"
 , sfo.entity_id AS order_view
 , CONVERT_TZ(sfo.created_at, '+0:00', 'America/Los_Angeles') AS "Order Date"
 , sfo.status
 , CONVERT_TZ(sfo.captured_at, '+0:00', 'America/Los_Angeles') AS "Billed Date"
 , CONVERT_TZ(ggg.activated_at, '+0:00', 'America/Los_Angeles') AS "Activation Date"
 , ggg.amount
 , RIGHT(ggg.card_number, 10) AS "Gift Card Number" 
 , ggg.notes AS "Comments"
 , ggg.activated_by AS "User Name"
FROM
 sales_flat_order AS sfo
INNER JOIN
 gosolid_givex_giftcard AS ggg
ON
 ggg.order_id = sfo.entity_id
WHERE
 DATE(CONVERT_TZ(ggg.activated_at, '+0:00', 'America/Los_Angeles')) BETWEEN '{Start date:date}' AND '{End date:date}'