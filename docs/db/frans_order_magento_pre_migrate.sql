
ALTER TABLE xcart_orders
/* add a column to orders to indicate the email of the login that ordered it  so we can use it in magento */
ADD COLUMN customer_email VARCHAR(128)
/* add a column that indicates the date the order was created, but as a timestamp (not an int) */
, ADD COLUMN created_at TIMESTAMP
/* add a column that has all SKUS involved in the order */
, ADD COLUMN sku_list VARCHAR(255)
/* also add a column with the shipment number, which comes from shipment table */
, ADD COLUMN order_number VARCHAR(10)
/* flatten the order status so we have it handy */
, ADD COLUMN order_status VARCHAR(32)
/* need a column for full billing name */
, ADD COLUMN billing_name VARCHAR(256)
/* and full shipping name */
, ADD COLUMN shipping_name VARCHAR(256);


/* populate the email based on the login for the order */
/* dev = 2 minutes */
/* this is the actual version */
/*
UPDATE 
	xcart_orders AS xo
INNER JOIN 
	xcart_customers AS xc
ON
	xc.login = xo.login
SET
	xo.customer_email = xc.email;
*/	

/* this is the test version that works with gosolid emails */
/* we have to join to our export table to determine the proper login so we have the correct email */
UPDATE 
	xcart_orders AS xo
INNER JOIN 
	xcart_customers AS xc
ON
	xc.login = xo.login
INNER JOIN
	gosolid_export_logins gel
ON
	gel.email = xc.email
INNER JOIN
	xcart_customers AS xcg
ON
	xcg.login = gel.login
SET
	xo.customer_email = 
		CASE
		WHEN xcg.email LIKE '%gosolid.net' THEN xcg.email
		WHEN xcg.email LIKE '%enginei.com' THEN xcg.email
		WHEN xcg.email LIKE '%frans.com' THEN xcg.email
		ELSE CONCAT(xcg.customerId, '@gosolid.net') /* REMOVE FOR PRODUCTION! */
		END
;

CREATE INDEX IDX_XCART_ORDERS_CUSTOMER_EMAIL
ON xcart_orders (customer_email);

/* populate SKU list from the order details */
UPDATE xcart_orders AS xo
INNER JOIN
(
	SELECT xod.orderid, GROUP_CONCAT(xod.productcode SEPARATOR '\n') AS sku_list
	FROM xcart_order_details AS xod
	GROUP BY xod.orderid
) AS order_skus
ON order_skus.orderid = xo.orderid
SET xo.sku_list = order_skus.sku_list;

/* populate the order_number and the status from the shipments table */
UPDATE xcart_orders AS xo
INNER JOIN 
	fc_shipments AS fs
ON
	fs.orderid = xo.orderid
INNER JOIN
	fcadmin_order_status AS fos
ON
	fos.order_status_id = fs.order_status_id
SET
	xo.order_number = CONCAT(fs.shipmentgroupid, '-', fs.shipmentnr)
	, xo.order_status = fos.order_status;
	
/* set billing and shipping name by concatenating names */
UPDATE xcart_orders
SET	billing_name = CONCAT(b_firstname, ' ', b_lastname)
	, shipping_name = CONCAT(s_firstname, ' ', s_lastname);
	
/* populate the created_at from the old date */
UPDATE
	xcart_orders AS xo
SET
	/*xo.created_at = TIMESTAMP(CONVERT_TZ(FROM_UNIXTIME(xo.date), 'US/Pacific', 'UTC')); */
	xo.created_at = FROM_UNIXTIME(UNIX_TIMESTAMP(FROM_UNIXTIME(xo.date)));
