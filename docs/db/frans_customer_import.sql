DROP TABLE IF EXISTS gosolid_dupe_emails;
DROP TABLE IF EXISTS gosolid_export_logins;
DROP TABLE IF EXISTS gosolid_export;
DROP TABLE IF EXISTS gosolid_export_addresses;

CREATE TABLE gosolid_dupe_emails
(
	email VARCHAR(128) PRIMARY KEY
);

INSERT INTO gosolid_dupe_emails
SELECT
	xc.email
FROM
	xcart_customers AS xc
WHERE
	xc.login NOT LIKE 'anonymous%'
AND
	xc.status = 'Y'
AND
	xc.email LIKE '%@%'
GROUP BY 
	xc.email
HAVING COUNT(xc.login) > 1;


CREATE TABLE gosolid_export_logins
(
	login VARCHAR(64)
	, email VARCHAR(128)
	, PRIMARY KEY (login)
);

ALTER TABLE gosolid_export_logins
ADD UNIQUE INDEX `IX_EXPORT_EMAIL` (`email`);

CREATE TABLE gosolid_export_addresses
(
	address_id INT NOT NULL PRIMARY KEY
);


/* first, identify which customers to work on. Logic:

	- for non anonymous accounts: must have placed an order
		- when multiple with the same email, use the most recent logged in account
	- for anonymous accounts: must have placed 2 or more orders
*/
/* find any that are the most recent from the duplicates with an order */
INSERT INTO gosolid_export_logins
( login, email)
SELECT
	xc.login,
	xc.email
FROM 
	xcart_customers AS xc USE INDEX (email_index)
INNER JOIN
(
	SELECT xc3.email,
		MAX(xc3.customerId) AS customerId
	FROM
		xcart_customers AS xc3 USE INDEX (email_index)
	INNER JOIN
	(
		SELECT	xc2.email
			, MAX(xc2.last_login) AS last_login
		FROM
			xcart_customers AS xc2
		INNER JOIN
			gosolid_dupe_emails de
		ON
			de.email = xc2.email
		WHERE
			xc2.login NOT LIKE 'anonymous%'
		AND
			xc2.login NOT LIKE 'call-center%'
		AND 
			xc2.email LIKE '%@%'
		AND
			EXISTS
			(
				SELECT * FROM xcart_orders AS xo WHERE xo.login = xc2.login
			)
		GROUP BY 
			xc2.email
	) AS tmp2
	ON 
		tmp2.email = xc3.email
	AND
		tmp2.last_login = xc3.last_login
	WHERE
		xc3.login NOT LIKE 'anonymous%'
	AND
		xc3.login NOT LIKE 'call-center%'
	AND 
		xc3.email LIKE '%@%'
	AND
		EXISTS
		(
			SELECT * FROM xcart_orders AS xo WHERE xo.login = xc3.login
		)
	GROUP BY xc3.email
		
) AS tmp
ON
	tmp.email = xc.email
AND
	tmp.customerId = xc.customerId
WHERE
	xc.login NOT LIKE 'anonymous%'
AND
	xc.login NOT LIKE 'call-center%'
AND 
	xc.email LIKE '%@%'
AND
	EXISTS
(
	SELECT * FROM xcart_orders AS xo2 WHERE xo2.login = xc.login
);


/* now do those with single emails, but not anonymous */
INSERT INTO gosolid_export_logins
( login, email)
SELECT
	DISTINCT xc.login
	, xc.email
FROM 
	xcart_customers AS xc USE INDEX (email_index)
INNER JOIN 
	xcart_orders AS xo
ON
	xo.login = xc.login
LEFT OUTER JOIN 
	gosolid_dupe_emails de
ON
	de.email = xc.email
WHERE
	de.email IS NULL
AND
	xc.login NOT LIKE 'anonymous%'
AND
	xc.login NOT LIKE 'call-center%'
AND 
	xc.email LIKE '%@%';

/* finally, do any that are anonymous that have > 1 order for the samne email */
INSERT INTO gosolid_export_logins
( email, login)
SELECT
	DISTINCT xc.email
	, MAX(xc.login)
FROM 
	xcart_customers AS xc
INNER JOIN
	xcart_orders AS xo
ON
	xo.login = xc.login
LEFT OUTER JOIN
	gosolid_export_logins AS gel
ON
	gel.email = xc.email
WHERE 
	gel.email IS NULL /* make sure haven't done this email */
AND
	(xc.login LIKE 'anonymous%' OR	xc.login NOT LIKE 'call-center%' )
AND 
	xc.email LIKE '%@%'
GROUP BY
	xc.email
HAVING 
	COUNT(xo.orderid) > 1
;
	
/* need to make a bunch be NULL so that we can do the multi-row thing that magento needs for multiple addresses */
CREATE TABLE gosolid_export
(
	CustomerId INT NOT NULL
	, email VARCHAR(255) NOT NULL
	, _website CHAR(4)
	, _store CHAR(8)
	, company_name VARCHAR(255)
	, customer_number VARCHAR(255)
	, created_in VARCHAR(20)
	, group_id INT NULL
	, lastname VARCHAR(128)
	, firstname VARCHAR(128)
	, password_hash VARCHAR(255)
	, store_id TINYINT
	, website_id TINYINT
	, prefix VARCHAR(32)
	, _address_city VARCHAR(64)
	, _address_country_id CHAR(2)
	, _address_firstname VARCHAR(100)
	, _address_lastname VARCHAR(100)
	, _address_postcode VARCHAR(32)
	, _address_prefix VARCHAR(32)
	, _address_region VARCHAR(32)
	, _address_street VARCHAR(64)
	, _address_telephone VARCHAR(32)
	, _address_company VARCHAR(255)
	, _address_default_billing_ TINYINT
	, _address_default_shipping_ TINYINT
);

INSERT INTO gosolid_export
SELECT 
	xc.CustomerId
	, CASE 
		WHEN xc.email LIKE '%gosolid.net' THEN xc.email
		WHEN xc.email LIKE '%enginei.com' THEN xc.email
		WHEN xc.email LIKE '%frans.com' THEN xc.email
		ELSE CONCAT(xc.customerId, '@gosolid.net') /* REMOVE FOR PRODUCTION! */
	END
	, 'base'
	, 'default' 
	, IFNULL(xc.company, '')
	, IFNULL(fc.accpac_custID, '')
	, 'XCart Import' 
	,  /* have to hardcode magento customer groups */ 
	   CASE fc.static_discount
		WHEN 25 THEN 9
		WHEN 20 THEN 8
		WHEN 15 THEN 7
		WHEN 10 THEN 6
		WHEN 5 THEN 5
		ELSE 1
	END AS group_id
	, lastname
	, firstname
	, `password` AS password_hash
	, 1 /* store id */
	, 1 /* website ID */
	, IFNULL(NULLIF(title, 'Array'), '') /* some titles are "Array", don't want those */
	, b_city 
	, b_country 
	/* conditional nulling in the event we don't really have a billing address */
	, IF(LENGTH(b_city) > 0, b_firstname, '')
	, IF(LENGTH(b_city) > 0, b_lastname, '')
	, b_zipcode 
	, b_title 
	, b_state 
	, b_address 
	, IF(LENGTH(b_city) > 0, phone, '')
	, '' /* company, we use it above for the customer on the billing address */
	, IF(LENGTH(b_city) > 0, 1, 0)
	, 0 
FROM
	xcart_customers AS xc
INNER JOIN
	gosolid_export_logins AS tl
ON
	tl.login = xc.login
LEFT OUTER JOIN
	fc_customer AS fc
ON
	fc.login = xc.login;
	
/* STEP 1 - get the default shipping addresses from the address list */
INSERT INTO gosolid_export
(CustomerId, email, _address_city, _address_country_id, _address_firstname
	, _address_lastname, _address_postcode, _address_prefix, _address_region
	, _address_street , _address_telephone, _address_company
	, _address_default_billing_ , _address_default_shipping_ )
SELECT 
	xc.CustomerId
	, '' /* email */
	, sa.s_city 
	, sa.s_country 
	, sa.s_firstname 
	, sa.s_lastname 
	, sa.s_zipcode 
	, sa.s_title 
	, sa.s_state 
	, CONCAT_WS('\n', sa.s_address, NULLIF(sa.s_address2, ''))
	, sa.s_phone
	, sa.s_company
	, 0 
	, sa.is_default
FROM
	xcart_customers AS xc
INNER JOIN
	gosolid_export_logins AS tl
ON
	tl.login = xc.login
INNER JOIN
	shipping_addresses AS sa
ON
	sa.login = xc.login
WHERE
	/* make sure the address has the required fields, like first/last name, city, zipcode, street */
	LENGTH(sa.s_firstname) > 0
AND
	LENGTH(sa.s_lastname) > 0
AND
	LENGTH(sa.s_city) > 0
AND
	LENGTH(sa.s_zipcode) > 0
AND
	LENGTH(sa.s_address) > 0
AND
	sa.is_default = 1
;

/* Step 2 - try and identify "unique" addresses that don't already exist */
/* first, try and get it from the address book */
INSERT INTO gosolid_export
(CustomerId
	, _address_firstname, _address_lastname, _address_street 
	, _address_city, _address_country_id
	, _address_postcode, _address_prefix, _address_region
	, _address_telephone, _address_company
	, email, _address_default_billing_ , _address_default_shipping_ )
SELECT 
	DISTINCT xc.CustomerId
	, sa.s_firstname 
	, sa.s_lastname 
	, CONCAT_WS('\n', sa.s_address, NULLIF(sa.s_address2, ''))
	, sa.s_city 
	, sa.s_country 
	, sa.s_zipcode 
	, sa.s_title 
	, sa.s_state 
	, sa.s_phone
	, sa.s_company
	, '' /* email */
	, 0 
	, 0
FROM
	xcart_customers AS xc
INNER JOIN
	gosolid_export_logins AS tl
ON
	tl.login = xc.login
INNER JOIN
	shipping_addresses AS sa
ON
	sa.login = xc.login
WHERE
	/* make sure the address has the required fields, like first/last name, city, zipcode, street */
	LENGTH(sa.s_firstname) > 0
AND
	LENGTH(sa.s_lastname) > 0
AND
	LENGTH(sa.s_city) > 0
AND
	LENGTH(sa.s_zipcode) > 0
AND
	LENGTH(sa.s_address) > 0
AND
	sa.is_default = 0
AND
	addressbook_id > 0 /* make sure it exists in an actual address book */
;


/* now we need to get ones that don't exist in address book */
INSERT INTO gosolid_export
(CustomerId, email, _address_city, _address_country_id, _address_firstname
	, _address_lastname, _address_postcode, _address_prefix, _address_region
	, _address_street , _address_default_billing_ , _address_default_shipping_ )
SELECT 
	xc.CustomerId
	, '' /* email */
	, s_city 
	, s_country 
	, s_firstname 
	, s_lastname 
	, s_zipcode 
	, s_title 
	, s_state 
	, s_address 
	, 0 
	, 1 
FROM
	xcart_customers AS xc
INNER JOIN
	gosolid_export_logins AS tl
ON
	tl.login = xc.login
/* only where nothing in address book */
WHERE NOT EXISTS
(
	SELECT	*
	FROM	shipping_addresses AS sa
	WHERE	sa.login = xc.login
)
AND
	LENGTH(s_city) > 0
AND
	LENGTH(s_address) > 0
AND
	LENGTH(s_country) > 0
AND
	LENGTH(s_zipcode) > 0
;

/* annoyingly, it doesn't do column names */
SELECT
'email'
,'_website'
,'_store'
,'company_name'
,'customer_number'
,'created_in'
,'group_id'
,'lastname'
,'firstname'
,'password_hash'
,'prefix'
,'store_id'
,'website_id'
,'_address_city'
,'_address_country_id'
,'_address_firstname'
,'_address_lastname'
,'_address_postcode'
,'_address_prefix'
,'_address_region'
,'_address_street'
,'_address_telephone'
,'_address_company'
,'_address_default_billing_'
,'_address_default_shipping_'
UNION ALL
SELECT tmp.* FROM
(
SELECT
	email
	, _website
	, _store
	, company_name
	, customer_number
	, created_in
	, group_id
	, lastname
	, firstname
	, password_hash
	, prefix
	, store_id
	, website_id
	, _address_city
	, _address_country_id
	, _address_firstname
	, _address_lastname
	, _address_postcode
	, _address_prefix
	, _address_region
	, _address_street
	, _address_telephone
	, _address_company
	, _address_default_billing_
	, _address_default_shipping_
	FROM
	gosolid_export
	ORDER BY CustomerId ASC, _address_default_billing_ DESC
) AS tmp
INTO OUTFILE 'frans_customer_import.csv'
FIELDS TERMINATED BY ',' 
OPTIONALLY ENCLOSED BY '"'
ESCAPED BY '' /* otherwise it escapes CR with a \ and it freaks out the import */
LINES TERMINATED BY '\n'
;
