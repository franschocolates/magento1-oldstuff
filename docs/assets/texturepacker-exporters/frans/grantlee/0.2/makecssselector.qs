var MakeSelectorFilter = function(input)
{
  var input = input.rawString();
  input = input.substring(input.lastIndexOf('/') + 1); // remove path from beginning of filename
  input = input.replace(/\s+/g,"_");
  return input;
};
MakeSelectorFilter.filterName = "makecssselector";
MakeSelectorFilter.isSafe = false;
Library.addFilter("MakeSelectorFilter");
