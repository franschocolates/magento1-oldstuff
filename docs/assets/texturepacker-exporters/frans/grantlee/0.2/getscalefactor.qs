var GetScaleFactor = function(input)
{
  var isRetina = (input.rawString().indexOf('retina/') != -1); // determine if it's a retina sprite by the directory it's in
  var factor = isRetina ? '0.5' : '1.0'; // multiply the height/width of the image by this factor
  return factor;
};
GetScaleFactor.filterName = "getscalefactor";
GetScaleFactor.isSafe = false;
Library.addFilter("GetScaleFactor");
