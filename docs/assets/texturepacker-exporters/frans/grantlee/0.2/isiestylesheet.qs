var IsIeStylesheet = function(input)
{
  return (input == 0) ? 'true' : ''; // TexturePacker treats 'true' as bool TRUE and '' as bool FALSE
};
IsIeStylesheet.filterName = "isiestylesheet";
IsIeStylesheet.isSafe = false;
Library.addFilter("IsIeStylesheet");
