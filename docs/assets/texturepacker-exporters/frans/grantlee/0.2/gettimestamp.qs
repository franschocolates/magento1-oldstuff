var GetTimestamp = function(input)
{
  return Date.now().toString();
};
GetTimestamp.filterName = "gettimestamp";
GetTimestamp.isSafe = false;
Library.addFilter("GetTimestamp");
