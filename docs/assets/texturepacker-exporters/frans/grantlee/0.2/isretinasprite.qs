var IsRetinaSprite = function(input)
{
  var isRetina = (input.rawString().indexOf('retina/') != -1); // determine if it's a retina sprite by the directory it's in
  return isRetina ? 'true' : ''; // TexturePacker treats 'true' as bool TRUE and '' as bool FALSE
};
IsRetinaSprite.filterName = "isretinasprite";
IsRetinaSprite.isSafe = false;
Library.addFilter("IsRetinaSprite");
