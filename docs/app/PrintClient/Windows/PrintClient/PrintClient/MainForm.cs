﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Contrib;

using AppSettings = Frans.PrintClient.Properties.Settings;

namespace Frans.PrintClient
{
    public partial class MainForm : Form
    {
        private const int MAX_LOG_ENTRIES = 500;
        private bool _disabled = false;

        delegate void DispatchDelegate();

        public MainForm()
        {
            InitializeComponent();
        }

        # region Event Handlers

        protected void OnExitClick(object sender, EventArgs e)
        {
            Exit();
        }

        protected void OnStartPollingClick(object sender, EventArgs e)
        {
            EnablePolling();
        }

        protected void OnStopPollingClick(object sender, EventArgs e)
        {
            DisablePolling();
        }

        protected void OnClearLogClick(object sender, EventArgs e)
        {
            ClearLog();
        }

        protected void OnLogBoxCopyClick(object sender, EventArgs e)
        {
            Clipboard.SetText(logBox.SelectedItem.ToString());
        }

        protected void OnTimerPoll(object sender, EventArgs e)
        {
            // turn off polling so that we don't keep polling if we have long timeouts
            try
            {
                StopPolling(false);

                CheckForNewPrintJob();
            }
            catch (Exception exception)
            {
                AddExceptionToLog(exception);
                // need to re-enable - safe to call again if already running
                StartPolling(false);
            }
        }

        # endregion Event Handlers

        protected void CheckForNewPrintJob()
        {
            try
            {
                // logging is overkill when we check every X seconds
                // AddToLog("Checking for new items to print...");
                var client = CreateRestClient();
                // our ID for the initial request is our print station name from the config
                var queryRequest = CreatePrintJobRequest(AppSettings.Default.PrintStationName);

                // we could do this async, but then we need to worry about not enabling the 
                // timer again until the async thread gets back. could be a while for the timeout
                // let's just wait synchronously for now
                var asyncHandle = client.GetAsync<PrintJob>(queryRequest, HandleRequestResponse);

            }
            catch (Exception exception)
            {
                AddExceptionToLog(exception);
                throw exception;
            }

        }

        private void HandleRequestResponse(IRestResponse<PrintJob> response, RestRequestAsyncHandle requestHandle)
        {
            try
            {
                if (response == null)
                {
                    throw new ApplicationException("Error connecting to " + requestHandle.WebRequest.Address);
                }

                if (response.Data != null)
                {
                    HandlePrintJob(response.Data);
                }
                else if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    AddToLog("Received an error communicating with the server: " + response.ErrorMessage);
                }
            }
            catch (Exception exception)
            {
                AddExceptionToLog(exception);
            }
            finally
            {
                // always need to restart
                StartPolling(false);
            }
        }

        protected void HandlePrintJob(PrintJob printJob)
        {
            try
            {
                AddToLog(String.Format("Processing job with ID of {0} for entity type of {1} (ID: {2}, Job Type: {3})...",
                    printJob.id, printJob.entity_type, printJob.entity_id, printJob.print_job_type));

                if (RawPrinterHelper.SendStringToPrinter(AppSettings.Default.PrinterName, printJob.document))
                {
                    AddToLog("Successfully sent to print queue, updating server...");

                    printJob.print_status = PrintJobStatus.Printed;
                }
                else
                {
                    MessageBox.Show("Failed to print! Please check the printer configuration.");
                    printJob.print_status = PrintJobStatus.Failed;
                }

                UpdatePrintJobStatus(printJob);
            }
            catch (Exception exception)
            {
                AddExceptionToLog(exception);
            }
        }

        protected void Exit()
        {
            this.Close();
            Application.Exit();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            StartPolling(true);
        }

        private void EnablePolling()
        {
            _disabled = false;
            pollTimer.Interval = AppSettings.Default.PollInterval;
            StartPolling(true);
        }

        private void DisablePolling()
        {
            _disabled = true;
            StopPolling(true);
        }

        protected void StopPolling(bool log)
        {
            // for now, just disabled timer
            // we only disable if they've done it manually,
            // or if they haven't, but we aren't manually disabled
            pollTimer.Stop();

            if (log)
            {
                AddToLog("Stopping polling.");
            }
        }

        protected void StartPolling(bool log)
        {
            // can't start if we are disabled
            // this is mostly just in case they disable while an async operation is pending
            if (!_disabled)
            {
                // may need to return to our original thread
                if (this.InvokeRequired)
                {
                    DispatchDelegate d = delegate()
                    {
                        this.StartPolling(log);
                    };
                    this.Invoke(d);
                }
                else
                {
                    pollTimer.Enabled = true;
                    PopulateSettingsDisplay();

                    if (log)
                    {
                        AddToLog("Starting to Poll....");
                    }
                }
            }
        }

        protected void PopulateSettingsDisplay()
        {
            sourceUrlValue.Text = AppSettings.Default.RestBaseUrl;
            printerNameValue.Text = AppSettings.Default.PrinterName;
            stationNameValue.Text = AppSettings.Default.PrintStationName;
        }

        protected void AddExceptionToLog(Exception exception)
        {
            string errorMessage = String.Format("ERROR! Message: {0}", exception.Message);
            AddToLog(errorMessage);
        }

        protected void AddToLog(string message)
        {
            if (this.InvokeRequired)
            {
                DispatchDelegate d = delegate()
                {
                    this.AddToLog(message);
                };
                this.Invoke(d);
            }
            else
            {
                logBox.Items.Insert(0, message);

                if (logBox.Items.Count > MAX_LOG_ENTRIES)
                {
                    // remove from the end
                    while (logBox.Items.Count > MAX_LOG_ENTRIES)
                    {
                        logBox.Items.RemoveAt(logBox.Items.Count - 1);
                    }
                }
            }

        }

        private void OnLogBoxKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                OnLogBoxCopyClick(sender, e);
            }
        }

        private RestRequest CreatePrintJobRequest(object id)
        {
            var request = new RestRequest(AppSettings.Default.RestPrintJobPath);
            request.AddParameter(RestParameterNames.ID, id, ParameterType.UrlSegment);

            return request;
        }

        private RestClient CreateRestClient()
        {
            // need to build it from our config settings with OAuth as the authentication
            var client = new RestClient(AppSettings.Default.RestBaseUrl);

            client.Authenticator = OAuth1Authenticator.ForProtectedResource(
                AppSettings.Default.RestOauthConsumerKey, AppSettings.Default.RestOauthConsumerSecret, 
                AppSettings.Default.RestOauthAccessToken, AppSettings.Default.RestOauthAccessTokenSecret
            );

            return client;
        }

        private void UpdatePrintJobStatus(PrintJob printJob)
        {
            var client = CreateRestClient();
            var request = CreatePrintJobRequest(printJob.id);

            var data = new { print_status = printJob.print_status };

            string jsonSerialized = SimpleJson.SerializeObject(data);
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("accept", "application/json");
            request.AddParameter("application/json", jsonSerialized, ParameterType.RequestBody);

            // we can this one sync, since are on a background thread...
            
            // we trust our calling method will catch the exception
            // we don't care about the return value.
            var updateResponse = client.Put(request);

            if (updateResponse.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new ApplicationException("Error updating print job: " + updateResponse.ErrorMessage);
            }
        }

        private void ClearLog()
        {
            // should always be on correct thread
            logBox.Items.Clear();
        }

    }
}
