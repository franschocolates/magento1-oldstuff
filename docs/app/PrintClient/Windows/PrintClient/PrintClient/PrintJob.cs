﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Frans.PrintClient
{
    /// <summary>
    /// This class defines the properties we need from a print job. 
    /// 
    /// Note that we don't define everything, and that's fine - just enough to get what we need.
    /// Nothing will break because all properties aren't defined.
    /// </summary>
    public class PrintJob
    {
        public int id { get; set; }
        public int entity_id { get; set; }
        public string entity_type { get; set; }
        public string document { get; set; }
        public string print_status { get; set; }
        public string print_job_type { get; set; }
    }
}
