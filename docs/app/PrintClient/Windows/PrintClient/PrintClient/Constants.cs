﻿namespace Frans.PrintClient
{
    struct PrintJobStatus
    {
        public const string Queued = "Q";
        public const string Printed = "P";
        public const string Failed = "F";
    }

    struct PrintJobType
    {
        public const string Zebra = "ZPLII";
    }

    struct RestParameterNames
    {
        public const string ID = "id";
    }
}