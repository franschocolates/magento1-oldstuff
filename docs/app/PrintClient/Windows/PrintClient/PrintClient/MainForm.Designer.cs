﻿namespace Frans.PrintClient
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pollTimer = new System.Windows.Forms.Timer(this.components);
            this.headerPanel = new System.Windows.Forms.Panel();
            this.printerNameValue = new System.Windows.Forms.Label();
            this.printNameLabel = new System.Windows.Forms.Label();
            this.sourceUrlValue = new System.Windows.Forms.Label();
            this.sourceUrlLabel = new System.Windows.Forms.Label();
            this.logBox = new System.Windows.Forms.ListBox();
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.mainMenuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.mainMenuFileStartPolling = new System.Windows.Forms.ToolStripMenuItem();
            this.mainMenuFileStopPolling = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.mainMenuFileExit = new System.Windows.Forms.ToolStripMenuItem();
            this.logContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mainMenuFileClearLog = new System.Windows.Forms.ToolStripMenuItem();
            this.stationNameValue = new System.Windows.Forms.Label();
            this.stationNameLabel = new System.Windows.Forms.Label();
            this.headerPanel.SuspendLayout();
            this.mainMenu.SuspendLayout();
            this.logContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // pollTimer
            // 
            this.pollTimer.Interval = 1000;
            this.pollTimer.Tick += new System.EventHandler(this.OnTimerPoll);
            // 
            // headerPanel
            // 
            this.headerPanel.Controls.Add(this.stationNameValue);
            this.headerPanel.Controls.Add(this.stationNameLabel);
            this.headerPanel.Controls.Add(this.printerNameValue);
            this.headerPanel.Controls.Add(this.printNameLabel);
            this.headerPanel.Controls.Add(this.sourceUrlValue);
            this.headerPanel.Controls.Add(this.sourceUrlLabel);
            this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel.Location = new System.Drawing.Point(0, 24);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(697, 84);
            this.headerPanel.TabIndex = 4;
            // 
            // printerNameValue
            // 
            this.printerNameValue.AutoSize = true;
            this.printerNameValue.Location = new System.Drawing.Point(91, 34);
            this.printerNameValue.Name = "printerNameValue";
            this.printerNameValue.Size = new System.Drawing.Size(80, 13);
            this.printerNameValue.TabIndex = 7;
            this.printerNameValue.Text = "Example Printer";
            // 
            // printNameLabel
            // 
            this.printNameLabel.AutoSize = true;
            this.printNameLabel.Location = new System.Drawing.Point(11, 35);
            this.printNameLabel.Name = "printNameLabel";
            this.printNameLabel.Size = new System.Drawing.Size(71, 13);
            this.printNameLabel.TabIndex = 6;
            this.printNameLabel.Text = "Printer Name:";
            // 
            // sourceUrlValue
            // 
            this.sourceUrlValue.AutoSize = true;
            this.sourceUrlValue.Location = new System.Drawing.Point(88, 8);
            this.sourceUrlValue.Name = "sourceUrlValue";
            this.sourceUrlValue.Size = new System.Drawing.Size(100, 13);
            this.sourceUrlValue.TabIndex = 5;
            this.sourceUrlValue.Text = "http://example.com";
            // 
            // sourceUrlLabel
            // 
            this.sourceUrlLabel.AutoSize = true;
            this.sourceUrlLabel.Location = new System.Drawing.Point(12, 9);
            this.sourceUrlLabel.Name = "sourceUrlLabel";
            this.sourceUrlLabel.Size = new System.Drawing.Size(69, 13);
            this.sourceUrlLabel.TabIndex = 4;
            this.sourceUrlLabel.Text = "Source URL:";
            // 
            // logBox
            // 
            this.logBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.logBox.FormattingEnabled = true;
            this.logBox.Location = new System.Drawing.Point(0, 108);
            this.logBox.Name = "logBox";
            this.logBox.Size = new System.Drawing.Size(697, 292);
            this.logBox.TabIndex = 5;
            this.logBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnLogBoxKeyDown);
            // 
            // mainMenu
            // 
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mainMenuFile});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(697, 24);
            this.mainMenu.TabIndex = 6;
            this.mainMenu.Text = "menuStrip1";
            // 
            // mainMenuFile
            // 
            this.mainMenuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mainMenuFileStartPolling,
            this.mainMenuFileStopPolling,
            this.toolStripSeparator1,
            this.mainMenuFileClearLog,
            this.toolStripSeparator2,
            this.mainMenuFileExit});
            this.mainMenuFile.Name = "mainMenuFile";
            this.mainMenuFile.Size = new System.Drawing.Size(37, 20);
            this.mainMenuFile.Text = "&File";
            // 
            // mainMenuFileStartPolling
            // 
            this.mainMenuFileStartPolling.Name = "mainMenuFileStartPolling";
            this.mainMenuFileStartPolling.Size = new System.Drawing.Size(164, 22);
            this.mainMenuFileStartPolling.Text = "&Start Polling";
            this.mainMenuFileStartPolling.Click += new System.EventHandler(this.OnStartPollingClick);
            // 
            // mainMenuFileStopPolling
            // 
            this.mainMenuFileStopPolling.Name = "mainMenuFileStopPolling";
            this.mainMenuFileStopPolling.Size = new System.Drawing.Size(164, 22);
            this.mainMenuFileStopPolling.Text = "Sto&p Polling";
            this.mainMenuFileStopPolling.Click += new System.EventHandler(this.OnStopPollingClick);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(161, 6);
            // 
            // mainMenuFileExit
            // 
            this.mainMenuFileExit.Name = "mainMenuFileExit";
            this.mainMenuFileExit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.mainMenuFileExit.Size = new System.Drawing.Size(164, 22);
            this.mainMenuFileExit.Text = "E&xit";
            this.mainMenuFileExit.Click += new System.EventHandler(this.OnExitClick);
            // 
            // logContextMenu
            // 
            this.logContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyToolStripMenuItem});
            this.logContextMenu.Name = "logContextMenu";
            this.logContextMenu.Size = new System.Drawing.Size(145, 26);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.copyToolStripMenuItem.Text = "&Copy";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.OnLogBoxCopyClick);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(161, 6);
            // 
            // mainMenuFileClearLog
            // 
            this.mainMenuFileClearLog.Name = "mainMenuFileClearLog";
            this.mainMenuFileClearLog.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.mainMenuFileClearLog.Size = new System.Drawing.Size(164, 22);
            this.mainMenuFileClearLog.Text = "Clear &Log";
            this.mainMenuFileClearLog.Click += new System.EventHandler(this.OnClearLogClick);
            // 
            // stationNameValue
            // 
            this.stationNameValue.AutoSize = true;
            this.stationNameValue.Location = new System.Drawing.Point(92, 58);
            this.stationNameValue.Name = "stationNameValue";
            this.stationNameValue.Size = new System.Drawing.Size(83, 13);
            this.stationNameValue.TabIndex = 9;
            this.stationNameValue.Text = "Example Station";
            // 
            // stationNameLabel
            // 
            this.stationNameLabel.AutoSize = true;
            this.stationNameLabel.Location = new System.Drawing.Point(12, 59);
            this.stationNameLabel.Name = "stationNameLabel";
            this.stationNameLabel.Size = new System.Drawing.Size(74, 13);
            this.stationNameLabel.TabIndex = 8;
            this.stationNameLabel.Text = "Station Name:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(697, 400);
            this.Controls.Add(this.logBox);
            this.Controls.Add(this.headerPanel);
            this.Controls.Add(this.mainMenu);
            this.MainMenuStrip = this.mainMenu;
            this.Name = "MainForm";
            this.Text = "Print Client";
            this.headerPanel.ResumeLayout(false);
            this.headerPanel.PerformLayout();
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            this.logContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer pollTimer;
        private System.Windows.Forms.Panel headerPanel;
        private System.Windows.Forms.Label printerNameValue;
        private System.Windows.Forms.Label printNameLabel;
        private System.Windows.Forms.Label sourceUrlValue;
        private System.Windows.Forms.Label sourceUrlLabel;
        private System.Windows.Forms.ListBox logBox;
        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem mainMenuFile;
        private System.Windows.Forms.ToolStripMenuItem mainMenuFileStartPolling;
        private System.Windows.Forms.ToolStripMenuItem mainMenuFileStopPolling;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem mainMenuFileExit;
        private System.Windows.Forms.ContextMenuStrip logContextMenu;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem mainMenuFileClearLog;
        private System.Windows.Forms.Label stationNameValue;
        private System.Windows.Forms.Label stationNameLabel;
    }
}

