#! /bin/bash

BACKUP_TYPE=$1
BACKUP_COUNT=$2
: ${BACKUP_COUNT:=7}
DB_BACKUP=`pwd`
DB_USER="fc_app"
DB_PASSWD="6EatzxwmaAxD2XKq"
DB_NAME="frans_new"
DB_BACKUP_NAME="Backup_"$BACKUP_TYPE"_"`date +%Y-%m-%d-%H-%M`.sql
DB_BACKUP_TMP="tmp"
SUCCESS=false
FROM_SERVER='localhost'

mkdir -p $DB_BACKUP/$BACKUP_TYPE/$DB_BACKUP_TMP

if [ "$BACKUP_TYPE" == "all" ];
then
       RESULT=$((mysqldump --user=$DB_USER --password=$DB_PASSWD --single-transaction $DB_NAME > $DB_BACKUP/$BACKUP_TYPE/$DB_BACKUP_TMP/$DB_BACKUP_NAME) 2>&1)

        if [ "$?" -eq 0  ]
        then
                echo "succes"
                SUCCESS=true
        else
                echo "fail"
        fi
else
        TABLELIST=`cat ./frans_xcart_order_tables.txt`
        RESULT=$((mysqldump --user=$DB_USER --password=$DB_PASSWD --single-transaction $DB_NAME $TABLELIST  > $DB_BACKUP/$BACKUP_TYPE/$DB_BACKUP_TMP/$DB_BACKUP_NAME) 2>&1)

        if [ "$?" -eq 0  ]
        then
                echo "succes"
                SUCCESS=true
        else
                echo "fail"
        fi
fi


if [ $SUCCESS == true ];
then
        # we assume that we always have 2 digits (%02d)
        last=`printf "%02d" $BACKUP_COUNT`
        rm -rf $DB_BACKUP/$BACKUP_TYPE/$last

        i=$BACKUP_COUNT
        while [ $i -gt 1 ]; do
                current=`printf "%02d" $i`
                let previous=$i-1
                prev=`printf "%02d" $previous`
                mv $DB_BACKUP/$BACKUP_TYPE/$prev $DB_BACKUP/$BACKUP_TYPE/$current
                let i=i-1
        done
        mkdir -p $DB_BACKUP/$BACKUP_TYPE/01
        mv $DB_BACKUP/$BACKUP_TYPE/$DB_BACKUP_TMP/$DB_BACKUP_NAME $DB_BACKUP/$BACKUP_TYPE/01/$DB_BACKUP_NAME
        rm -rf $DB_BACKUP/$DB_BACKUP_TMP

        echo "$FROM_SERVER $BACKUP_TYPE tables have been successfully backed up at `date`" | mail -s "Success $FROM_SERVER $BACKUP_TYPE DB Backup" "frans_orders_backup@gosolid.net"
else
        echo "$FROM_SERVER $BACKUP_TYPE tables have failed to back up at `date`. Error $RESULT" | mail -s "Fail $FROM_SERVER $BACKUP_TYPE DB Backup" "frans_orders_backup@gosolid.net"
fi
