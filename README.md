## frans.com / magento1.fransstage.com
This repo contains our Magento 1 code base (currently v1.7.0.2).  Content / images are by in large not contained in this repository

### Deployment
  We're currently hosting on AWS ELB, with all the necessary build and configuration scripts in this repo to allow for autoscaling and other happy magic that Elastic Beanstalk supports (auto-rebuild, simple platform upgrades, etc)

#### magento1.fransstage.com
The stage environment is automatically deployed from the stage branch of this repo

#### frans.com / fcadmin.frans.com
The production environments are deployed from the production branch of this repo.  Per environment deployments can be achieved using the production-frontend-only and production-admin-only branches.  
