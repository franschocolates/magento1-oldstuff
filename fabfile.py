from fabric.api import *
from slacker import Slacker
import os, re


# Slack configuration
SLACK_API_TOKEN = "xoxp-79352107462-192371145553-205319621793-235f6d7315f47106be108183d77b5529"
SLACKER = Slacker(SLACK_API_TOKEN)


# Server / environment configuration
STAGING_SERVER  = ['52.25.241.74']

PRODUCTION_PUBLIC_FACING = [
                '54.164.159.66',
                '52.5.144.89'
            ]
PRODUCTION_ADMIN = ['52.5.108.128']
PRODUCTION_SERVERS = PRODUCTION_PUBLIC_FACING + PRODUCTION_ADMIN


# for later:
# PRODUCTION_LOAD_BALANCER_NAME = 'FransChoc-ElasticL-5UHTCGYGCIVP'
# PRODUCTION_LOAD_BALANCER_DNS_NAME = 'FransChoc-ElasticL-5UHTCGYGCIVP-65613291.us-east-1.elb.amazonaws.com'


def push_message_to_slack(message, channel="#webdev"):
    global SLACKER
    SLACKER.chat.post_message(channel, message)


def announce_deploy():
    message = "*%(env)s deploy*: %(user)s is now deploying `%(branch)s`"

    match_ticket_in_branch_name = re.search(r'mbi\-(?P<ticket_number>\d+)', env.branch_name)
    if match_ticket_in_branch_name:
        ticket_number = "MBI-%s" % match_ticket_in_branch_name.groups('ticket_number')[0]
        message += "\nView on Jira: https://franschocolates.atlassian.net/browse/%s" % ticket_number

    push_message_to_slack(message % {
            'env': env.env_name,
            'user': os.environ['USER'],
            'branch': env.branch_name
        })


def staging():
    env.env_name = "staging"
    env.user = 'ec2-user'
    env.hosts = STAGING_SERVER
    env.forward_agent = True
    env.target_directory = '/var/www/html'


def production():
    env.env_name = "production"
    env.user = 'ec2-user'
    env.hosts = PRODUCTION_SERVERS
    env.forward_agent = True
    env.target_directory = '/var/www/html'


def deploy(branch="dev", clear_cache="true"):
    env.branch_name = branch
    announce_deploy()
    with cd(env.target_directory):
        sudo("git fetch --prune")
        sudo("git stash save")
        sudo("git checkout -f %s" % branch)
        sudo("git pull origin %s" % branch)

        # may be other truthy values we want to catch / test for ...
        if clear_cache in ['true']: 
            sudo("php scripts/cli-clear-cache.php")
