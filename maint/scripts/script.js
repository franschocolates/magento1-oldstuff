//script.js
//define the jquery var
var $ = jQuery.noConflict();
//when the document is ready...
$(document).ready(function(){
			
	// Find the links and the fade background
	var links = $('#storeLinks a');
	var fade = $('#fadeOut');
	var hide = $('.hide');
	var timing = 250;
	
	// Hide the Elements on load
	hide.hide();
	fade.hide();
	
	// Global Variable for the storeLink and close button
	var storeLink;
	var closeButton = $('.close');
	closeButton.show();
	
	// Give the modals the modal class 
	hide.addClass('modal');
		
	// Clicking on one of the links
	links.click(function(e){
		e.preventDefault();
		// Determine the link clicked
		storeLink = $(this).attr('href');
		// Fade the background, fade in the link
		fade.fadeIn(timing);
		$(storeLink).fadeIn(timing);
	});
		
	// Clicking on the background
	fade.click(function(e){
		e.preventDefault();
		// Fade out the store link then the background
		$(storeLink).fadeOut(timing);
		// Remove the close button after the fadeout
		fade.fadeOut(timing);
	});

	// Clicking on the close button
	closeButton.click( function(e){
		e.preventDefault();
		// console.log("clicked on button");
		// Fade out the store link then the background
		$(storeLink).fadeOut(timing);
		// Remove the close button after the fadeout
		fade.fadeOut(timing);
	});

});