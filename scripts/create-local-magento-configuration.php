<?php

    // 12factor ftw
    $SECRET_KEY = getenv('SECRET_KEY');
    $RDS_HOSTNAME = getenv('RDS_HOSTNAME');
    $RDS_USERNAME = getenv('RDS_USERNAME');
    $RDS_PASSWORD = getenv('RDS_PASSWORD');
    $RDS_DB_NAME = getenv('RDS_DB_NAME');

    $REDIS_CACHE_ENDPOINT = getenv('REDIS_CACHE_ENDPOINT');
    $REDIS_SESSION_ENDPOINT = getenv('REDIS_SESSION_ENDPOINT');

    // todo: Redis for sessions / caching

    $LOCAL_XML_TEMPLATE = <<< END_OF_XML_TEMPLATE
<?xml version="1.0"?>
<!--
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Mage
 * @package    Mage_Core
 * @copyright  Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
-->
<config>
    <global>
        <install>
            <date><![CDATA[Tue, 22 Oct 2013 20:50:59 +0000]]></date>
        </install>
        <crypt>
            <key><![CDATA[$SECRET_KEY]]></key>
        </crypt>
        <disable_local_modules>false</disable_local_modules>
        <resources>
            <db>
                <table_prefix></table_prefix>
            </db>
            <default_setup>
                <connection>
                    <host><![CDATA[$RDS_HOSTNAME]]></host>
                    <username><![CDATA[$RDS_USERNAME]]></username>
                    <password><![CDATA[$RDS_PASSWORD]]></password>
                    <dbname><![CDATA[$RDS_DB_NAME]]></dbname>
                    <initStatements><![CDATA[SET NAMES utf8]]></initStatements>
                    <model><![CDATA[mysql4]]></model>
                    <type><![CDATA[pdo_mysql]]></type>
                    <pdoType><![CDATA[]]></pdoType>
                    <active>1</active>
                </connection>
            </default_setup>
        </resources>
        <session_save>db</session_save>
        <redis_session><!-- All options seen here are the defaults -->
            <host>$REDIS_SESSION_ENDPOINT</host> <!-- Specify an absolute path if using a unix socket -->
            <port>6379</port>
            <password></password> <!-- Specify if your Redis server requires authentication -->
            <timeout>2.5</timeout> <!-- This is the Redis connection timeout, not the locking timeout -->
            <persistent></persistent> <!-- Specify unique string to enable persistent connections. E.g.: sess-db0; bugs with phpredis and php-fpm are known: https://github.com/nicolasff/phpredis/issues/70 -->
            <db>0</db> <!-- Redis database number; protection from accidental loss is improved by using a unique DB number for sessions -->
            <compression_threshold>2048</compression_threshold> <!-- Set to 0 to disable compression (recommended when suhosin.session.encrypt=on); known bug with strings over 64k: https://github.com/colinmollenhour/Cm_C … /issues/18 -->
            <compression_lib>gzip</compression_lib> <!-- gzip, lzf or snappy -->
            <log_level>1</log_level> <!-- 0 (emergency: system is unusable), 4 (warning; additional information, recommended), 5 (notice: normal but significant condition), 6 (info: informational messages), 7 (debug: the most information for development/testing) -->
            <max_concurrency>100</max_concurrency> <!-- maximum number of processes that can wait for a lock on one session; for large production clusters, set this to at least 10% of the number of PHP processes -->
            <break_after_frontend>5</break_after_frontend> <!-- seconds to wait for a session lock in the frontend; not as critical as admin -->
            <break_after_adminhtml>30</break_after_adminhtml>
            <bot_lifetime>7200</bot_lifetime> <!-- Bots get shorter session lifetimes. 0 to disable -->
        </redis_session>
        <cache>
            <backend><![CDATA[Cm_Cache_Backend_Redis]]></backend>
            <backend_options>
                <server><![CDATA[$REDIS_CACHE_ENDPOINT]]></server>
                <port><![CDATA[6379]]></port>
                <database><![CDATA[0]]></database>
                <force_standalone><![CDATA[0]]></force_standalone>
                <automatic_cleaning_factor><![CDATA[0]]></automatic_cleaning_factor>
                <compress_data><![CDATA[1]]></compress_data>
                <compress_tags><![CDATA[1]]></compress_tags>
                <compress_threshold><![CDATA[20480]]></compress_threshold>
                <compression_lib><![CDATA[gzip]]></compression_lib>
                <lifetimelimit><![CDATA[86400]]></lifetimelimit>
            </backend_options>
        </cache>
    </global>
    <admin>
        <routers>
            <adminhtml>
                <args>
                    <frontName><![CDATA[FrAdmLogin]]></frontName>
                </args>
            </adminhtml>
        </routers>
    </admin>
</config>
END_OF_XML_TEMPLATE;

    file_put_contents("magento/app/etc/local.xml", $LOCAL_XML_TEMPLATE);

?>