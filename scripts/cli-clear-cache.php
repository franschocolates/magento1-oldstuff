<?php

    PHP_SAPI == 'cli' or die();

    ini_set('memory_limit','1024M');
    set_time_limit(0);
    error_reporting(E_ALL | E_STRICT);

    require_once 'magento/app/Mage.php';
    Mage::app()->getCacheInstance()->flush();

    // clear JS / CSS cache
    Mage::getModel('core/design_package')->cleanMergedJsCss();
    Mage::dispatchEvent('clean_media_cache_after');

    // Varnish
    Mage::dispatchEvent('turpentine_varnish_flush_all');
    $result = Mage::getModel('turpentine/varnish_admin')->flushAll();

    // todo:
    // - Fastly

?>
