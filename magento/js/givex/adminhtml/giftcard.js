var applyGiftcard = function(url) {
    new Ajax.Request(url, {
        parameters: { giftcard_number: $F('giftcard_number') }
        , onSuccess: function(transport) {
            var responseData = transport.responseText.evalJSON(true);
            if (responseData.status == 'success')
            {
                order.loadArea(['shipping_method', 'totals', 'billing_method'], true, { reset_shipping: false });
            }
            else
            {
                if (responseData.hasOwnProperty('message'))
                {
                    alert(responseData.message);
                }
                else {
                    alert('An error occurred applying the giftcard');
                }
            }
        }
    })
}

var removeGiftcard = function(url) {
    new Ajax.Request(url, {
        parameters: {  }
        , onSuccess: function(transport, json) {
            var responseData = transport.responseText.evalJSON(true);
            if (responseData.status == 'success')
            {
                // we reload items because it may show a discount, depending on their load order.
                order.loadArea(['shipping_method', 'totals', 'billing_method', 'items'], true, { reset_shipping: false });
            }
            else
            {
                alert('An error occurred removing the giftcard');
                console.log(transport);
            }
        }
    })
}

function onCardNumberChanged(e) {

    if (e.keyCode != Event.KEY_TAB && e.keyCode != Event.KEY_ESC && $('status').value != 'I')
    {
        $('send_activation').enable();
    }
}

function onStatusChanged(e) {
    if ($('status').value != 'I' && $('card_number').value.length > 0)
    {
        $('send_activation').enable();
    }
    else
    {
        $('send_activation').disable();
    }
}

document.observe("dom:loaded", function() {
    // annoyingly, VarienCheckbox doesn't support setting value
    // so we need to set it here.
    if ($('send_activation'))
    {
        $('send_activation').value = 1;
    }

    if ($('card_number'))
    {
        $('card_number').observe('keyup', onCardNumberChanged);
        $('status').observe('change', onStatusChanged);
    }
});

