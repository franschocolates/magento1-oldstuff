// https://github.com/parndt/jquery-html5-placeholder-shim
// GoSolid edited this shim to implement multiline placeholders in textareas, and execution after AJAX requests

(function($) {
	// @todo Document this.
	$.extend($,{ placeholder: {
		browser_supported: function() {
			return this._supported !== undefined ?
				this._supported :
				( this._supported = !!('placeholder' in $('<input type="text">')[0]) );
		},
		shim: function(opts) {
			var config = {
				color: '#888',
				cls: 'placeholder',
				selector: 'input[placeholder], textarea[placeholder]'
			};
			$.extend(config,opts);
			return !this.browser_supported() && $(config.selector)._placeholder_shim(config);
		},
		reset: function() {
			$('input[placeholder], textarea[placeholder]').each(function(){
				$(this).prev('label.placeholder').remove();
				$(this).removeData('placeholder');
			});
			this.shim();
		},
	}});

	$.extend($.fn,{
		_placeholder_shim: function(config) {
			function calcPositionCss(target)
			{
				var op = $(target).offsetParent().offset();
				var ot = $(target).offset();

				return {
					top: ot.top - op.top,
					left: ot.left - op.left,
					width: $(target).width()
				};
			}
			function adjustToResizing(label) {
				var $target = label.data('target');
				if(typeof $target !== "undefined") {
					label.css(calcPositionCss($target));
					$(window).one("resize", function () { adjustToResizing(label); });
				}
			}
			function wordwrap( str, width, brk, cut ) {
				brk = brk || '\n';
				width = width || 75;
				cut = cut || false;
				if (!str) { return str; }
				var regex = '.{1,' +width+ '}(\\s|$)' + (cut ? '|.{' +width+ '}|.+$' : '|\\S+?(\\s|$)');
				return str.match( RegExp(regex, 'g') ).join( brk );
			}
			return this.each(function() {
				var $this = $(this);

				if( $this.is(':visible') ) {

					if( $this.data('placeholder') ) {
						var $ol = $this.data('placeholder');
						$ol.css(calcPositionCss($this));
						return true;
					}

					var possible_line_height = {};
					if( !$this.is('textarea') && $this.css('height') != 'auto') {
						possible_line_height = { lineHeight: $this.css('height'), whiteSpace: 'nowrap' };
					}
					var placeholderText = $this.attr('placeholder');
					if($this.prop('tagName') == 'TEXTAREA'){
						placeholderText = wordwrap(placeholderText, Math.floor($this.width() / 5.4));
					}

					var ol = $('<label />')
						.text(placeholderText)
						.addClass(config.cls + ($this.is('textarea') ? ' placeholder-textarea' : ''))
						.css($.extend({
							position:'absolute',
							display: 'inline',
							float:'none',
							overflow:'hidden',
							textAlign: 'left',
							color: config.color,
							cursor: 'text',
							paddingTop: $this.css('padding-top') + 2,
							paddingRight: $this.css('padding-right'),
							paddingBottom: $this.css('padding-bottom'),
							paddingLeft: $this.css('padding-left'),
							fontSize: $this.css('font-size'),
							fontFamily: $this.css('font-family'),
							fontStyle: $this.css('font-style'),
							fontWeight: $this.css('font-weight'),
							textTransform: $this.css('text-transform'),
							backgroundColor: 'transparent',
							whiteSpace: 'pre',
							zIndex: 99
						}, possible_line_height))
						.css(calcPositionCss(this))
						.attr('for', this.id)
						.data('target',$this)
						.click(function(){
							$(this).data('target').focus();
						})
						.insertBefore(this);
					$this
						.data('placeholder',ol)
						.keydown(function(){
							ol.hide();
						})
						.blur(function() {
							ol[$this.val().length ? 'hide' : 'show']();
						}).triggerHandler('blur');
					$(window).one("resize", function () { adjustToResizing(ol); });
				}
			});
		}
	});
})(jQuery);

jQuery(document).add(window).on('ready load click mousedown mouseup focus blur keydown change', function() {
	if (jQuery.placeholder) {
		jQuery.placeholder.shim();
	}
});

// by default the shim runs once, on document ready... we'll run it again after every AJAX request
jQuery(document).ajaxComplete(function() {
	if (jQuery.placeholder) {
		// wait a bit to give the DOM time to update first
		setTimeout(function() {
			jQuery.placeholder.shim();
		}, 700);
	}
});