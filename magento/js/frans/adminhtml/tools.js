// this code shamelessly based on the code for configured products pop ups
AddressValidation = Class.create();
AddressValidation.prototype = {
    blockWindow:                null,
    blockApplyBtn:              null,
    blockCancelBtn:             null,
    blockMask:                  null,
    blockForm:                  null,
    windowHeight:               null,
    applyCallback:              {},
    cancelCallback:             {},

    /**
     * Initialize object
     */
    initialize: function() {
        this._initWindowElements();
    },

    /**
     * Initialize window elements
     */
    _initWindowElements: function() {
        this.blockWindow                = $('address_validation');
        this.blockApplyBtn              = $('address_validation_apply');
        this.blockCancelBtn             = $('address_validation_cancel');
        this.blockMask                  = $('popup-window-mask');
        this.blockForm                  = $('address_validation_display');
        this.windowHeight               = $('html-body').getHeight();
    },

    /**
     * Shows address validation options
     *
     */
    showValidationOptions: function(applyCallback, cancelCallback) {
        this._initWindowElements();
        this.applyCallback = applyCallback;
        this.cancelCallback = cancelCallback || function() { /* just need some sort of callback if not supplied */ };
        this._showWindow();

        return false;
    },

    /**
     * Triggered on cancel button click
     */
    onCancelBtn: function() {
        this._closeWindow();
        this.cancelCallback();
        return this;
    },

    onApplyBtn: function() {
        // serialize the data
        var fields = this.blockForm.select('select', 'input', 'textarea');
        params = Form.serializeElements(fields, { hash: true });

        this.applyCallback(params);
        this._closeWindow();
    },

    /**
     * Show address validation window
     */
    _showWindow: function() {
        this._toggleSelectsExceptBlock(false);
        this.blockMask.setStyle({'height':this.windowHeight+'px'}).show();
        this.blockWindow.setStyle({'marginTop':-this.blockWindow.getHeight()/2 + "px", 'display':'block'});
    },

    /**
     * toggles Selects states (for IE) except those to be shown in popup
     */
    _toggleSelectsExceptBlock: function(flag) {
        if(Prototype.Browser.IE){
            if (this.blockForm) {
                var states = new Array;
                var selects = this.blockForm.getElementsByTagName("select");
                for(var i=0; i<selects.length; i++){
                    states[i] = selects[i].style.visibility
                }
            }
            toggleSelectsUnderBlock(this.blockMask, flag);
            if (this.blockForm) {
                for(i=0; i<selects.length; i++){
                    selects[i].style.visibility = states[i]
                }
            }
        }
    },

    /**
     * Close configuration window
     */
    _closeWindow: function() {
        toggleSelectsUnderBlock(this.blockMask, true);
        this.blockMask.style.display = 'none';
        this.blockWindow.style.display = 'none';
        //this.clean('window');
    }

};

Event.observe(window, 'load',  function() {
    addressValidation = new AddressValidation();
});


    /**
 * Helper methods similar to mage/adminhtml/tools.js
 */
function submitAreaInBackground(area, url) {
    if($(area)) {
        var fields = $(area).select('input', 'select', 'textarea');
        var data = Form.serializeElements(fields, true);
        url = url + (url.match(new RegExp('\\?')) ? '&isAjax=true' : '?isAjax=true');
        new Ajax.Request(url, {
            parameters: $H(data),
            loaderArea: area,
            onSuccess: function(transport) {
                try {
                    // assume json
                    var response = transport.responseText.evalJSON()
                    if (response.error) {
                        alert(response.message);
                    }
                    if(response.ajaxExpired && response.ajaxRedirect) {
                        setLocation(response.ajaxRedirect);
                    }
                }
                catch (e) {
                    alert(transport.responseText);
                }
            }
        });
    }
}

jQuery(document).ready(function($){

	var bodyEl = $('body');

	bodyEl.on('keyup', '.soft-maxlength', function(){
		validateSoftMaxLength($(this));
	});

	function validateSoftMaxLength(el){
		var textLength = el.val().length;
		el.toggleClass('validation-failed', (textLength > 300));
	}

	bodyEl.on('click', '.copy-shipping-address', function(){
		var sourceContainer = $('#order-shipping_address');
		var destinationContainer = $('#order-billing_address');
		duplicateFieldValues(sourceContainer, destinationContainer);
		undo('order-billing_address');
	});

	function duplicateFieldValues(sourceContainer, destinationContainer){
		// currently a pretty dumb function, it expects both fieldsets to have identical fields, in identical positions
		var sourceFields = sourceContainer.find('input[type=text], select, textarea, input[type=hidden], input[type=checkbox]');
		var destinationFields = destinationContainer.find('input[type=text], select, textarea, input[type=hidden], input[type=checkbox]');
		sourceFields.each(function(i){
			var type = $(this).attr('type');
			switch(type){
				case 'checkbox':
					var isChecked = $(this).prop('checked');
					destinationFields.eq(i).prop('checked', isChecked);
					break;
				default:
					var fieldVal = $(this).val();
					destinationFields.eq(i).val(fieldVal);
					break;
			}
		});
	}

});

virtualGiftCardResend = {
    toggleForm: function(id) {
        $('vgc_resend_link_' + id).toggleClassName('hide');
        $('vgc_resend_container_' + id).toggleClassName('hide');
    },
    clearForm: function(id) {
        $('vgc_resend_' + id + '_emails').value = '';
        this.toggleForm(id);
    },
    sendEmail: function(id) {
        var formId = 'vgc_resend_form_' + id;
        var combinedAddresses = $('vgc_resend_' + id + '_emails').value;
        var lines = combinedAddresses.split(/\n/);
        var emails = [];
        for (var i=0; i < lines.length; i++) {
            // only push this line if it contains a non whitespace character.
            if (/\S/.test(lines[i])) {
                emails.push(jQuery.trim(lines[i]));
            }
        }

        var emailCount = emails.length;
        if (emails.length > 0) {
            if (confirm('This will resend the gift card email to ' + emailCount + ' recipient(s). Are you sure you wish to proceed?'))
            {
                new Ajax.Request(
                    $(formId).action,
                    {
                        method:'post',
                        onSuccess: function() { alert('Successfully resent email.'); virtualGiftCardResend.clearForm(id); },
                        onFailure: function(response) { alert('Failed to resend email: ' + response.responseText)},
                        parameters: {
                            'order_item_id': id,
                            'emails[]': emails
                        }
                    }
                );

            }
        }
        else {
            alert('Please enter one or more email addresses.');
        }
    }
};

/*
 add an extra method to varien grid to allow us to submit only once
 */
varienGridMassaction.prototype.applyOnce = function(submitButton)
{
    // this is all the same as the original except that it will disable the button.
    if(varienStringArray.count(this.checkedString) == 0) {
        alert(this.errorText);
        return;
    }

    var item = this.getSelectedItem();
    if(!item) {
        this.validator.validate();
        return;
    }
    this.currentItem = item;
    var fieldName = (item.field ? item.field : this.formFieldName);
    var fieldsHtml = '';

    if(this.currentItem.confirm && !window.confirm(this.currentItem.confirm)) {
        return;
    }

    this.formHiddens.update('');
    new Insertion.Bottom(this.formHiddens, this.fieldTemplate.evaluate({name: fieldName, value: this.checkedString}));
    new Insertion.Bottom(this.formHiddens, this.fieldTemplate.evaluate({name: 'massaction_prepare_key', value: fieldName}));

    if(!this.validator.validate()) {
        return;
    }

    if(this.useAjax && item.url) {
        new Ajax.Request(item.url, {
            'method': 'post',
            'parameters': this.form.serialize(true),
            'onComplete': this.onMassactionComplete.bind(this)
        });
    } else if(item.url) {
        this.form.action = item.url;
        // this is the one line change to only disable once.
        submitButton.disabled = true;
        this.form.submit();
    }
};
