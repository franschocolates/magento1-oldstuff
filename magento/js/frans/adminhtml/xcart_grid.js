/**
 * Methods to make xcart order grid info open in a new tab
 */
function openExternalGridRow(grid, event) {
    var element = Event.findElement(event, 'tr');
    if(['a', 'input', 'select', 'option'].indexOf(Event.element(event).tagName.toLowerCase())!=-1) {
        return;
    }

    if(element.title){
        window.open(element.title,'_blank');
    }
}
