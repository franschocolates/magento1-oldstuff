
var icePackController = {
    
    saveWeight: function(container, url) {

        if(!$(this.getFieldId(container, 'form')).validator) {
            $(this.getFieldId(container, 'form')).validator = new Validation(this.getFieldId(container, 'form'));
        }

        if(!$(this.getFieldId(container, 'form')).validator.validate()) {
            return;
        }

        submitAndReloadArea($(container), url);
        //new Ajax.Request($(this.getFieldId(container, 'form')).action, {
        //    parameters: Form.serialize($(this.getFieldId(container, 'form')), true),
        //    loaderArea: container
        //});
    },
    getFieldId: function(container, name) {
        return container + '_' + name;
    }
};
