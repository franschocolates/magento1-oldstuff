jQuery.noConflict();
(function($) { 
  
	var hideNotesEditing = false;
	
	//Document ready functions go in here
	$(function() {

	  	//Check for the Email shipment notification
	  	if($('.head-sales-order-shipment').length > 0 && $('#send_email').length > 0 )
	  	{
	  		setSendEmailTrue();
	  	}

	  	if ($('#customer_notes').length > 0)
	  	{
	  		$("#save-customer-notes").click(updateCustomerNotes);

	  		if ($("#customer-notes-display").length > 0)
	  		{
	  			$("#customer-notes-display").click(toggleNotesEditing);
	  		}
	  	}
  });
  
  function setSendEmailTrue()
  {
	  $('#send_email').attr('checked', true);
  }
  
  function toggleNotesEditing()
  {
	  $(this).hide();
	  $("#customer-notes-edit").show().focus();
	  hideNotesEditing = true;
  }
  
  function updateCustomerNotes()
  {
	  // pull URL from parent form
	  var $form = $(this).closest("div#customer-notes-edit");
	  var url = $form.attr("action");
	  var customerId = $form.find("#notes_customer_id").val() || 0;
	  var orderId = $form.find("#notes_order_id").val() || 0; 
	  var notes = $("#customer_notes").val();
	  var button = $(this);
	  
	  $(this).text("Saving...");
	  
	  $.post(
			  url
			  , {
				  customer_notes: notes
				  , form_key: $form.find("input[name='form_key']").val()
				  , customer_id: customerId
				  , order_id: orderId
			  }
			  , function(result) {
				  if (result.status === 'success')
				  {
						  alert('Successfully saved notes.');
						  $("#customer_info_tabs_customer_edit_tab_view").removeClass("changed");					  
						  $("#sales_order_view_tabs_order_info").removeClass("changed");					  
						  
						  if (hideNotesEditing)
						  {
							  $("#customer-notes-display").text(notes);
							  $("#customer-notes-display").show();
							  $("#customer-notes-edit").hide();
							  if (notes.length > 0)
							  {
								  $("#customer-notes-display").addClass("highlighted");
							  }
							  else
							  {
								  $("#customer-notes-display").removeClass("highlighted");
								  $("#customer-notes-display").text("Add Notes");
							  }
					  }
				  }
				  else
				  {
					  alert('Failed to save notes.')
				  }
				  button.text("Save");
			  
		}, 'json'
	   );
	  
	  return false;
  }
  
  $(".picked-box").live("click", function(event){

	    var e = $(this);
	  
	  	var orderId = $(e).attr("order-id");



		if($(e).hasClass("picked-off"))
		{
			$(e).removeClass("picked-off");
			$(e).addClass("picked-on");

			//send pick ajax.
			// - /metadata/adminhtml_index/pick
			$.get("/metadata/adminhtml_index/pick", { order_id: orderId, status: 1 } );
		}
		else
		{
			$(e).removeClass("picked-on");
			$(e).addClass("picked-off");

			$.get("/metadata/adminhtml_index/pick", { order_id: orderId, status: 0 } );
		}



  });
  
  
  
  
})(jQuery);



