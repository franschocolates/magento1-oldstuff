/**
 * Classes for interacting with the shipment manager form(s)
 */
var ShipmentManagerSearch = new Class.create();
ShipmentManagerSearch.prototype = {

		_outputContainer: null,
		_form: null,
		_entryField: null,
		
	    initialize : function(form, outputContainer, entryField){
	    	var self = this;
	    	this._form = form;
	    	this._outputContainer = outputContainer;
	    	this._entryField = entryField;
	    	
	    	form.on('submit', function(event, element) {
		   	   	event.stop();
		   	   	var shipmentForm = element;
	
		   	   	self.doSearch(shipmentForm, false, outputContainer);
				return false;		
	    });
	},

	forceShipment: function(orderId) {
		this._entryField.value = orderId;
		//Form.Element.setValue(this._form, 'order_id', orderId);
		//this._form.Element.setValue('order_id', orderId);
		this.doSearch(this._form, true, this._outputContainer);
	},
	
	clearForm: function() {
		this._outputContainer.update('');
		this.resetSearch();
	},



	resetSearch: function() {
		this._entryField.value = '';
		this._entryField.activate();
	},
	
	doSearch: function(form, force, outputContainer) {
		var params = form.serialize(true);

		// clear shipping label reprint status message, if exist
		if(jQuery("#page\\:main-container > #messages > .messages").length) {
			jQuery("#page\\:main-container > #messages > .messages").hide();
		}
		if (force) {
			//paramData = params.evalJSON();
			params.force = 1;
			//paramData.force = 1;
			//params = paramData.toJSON();
		}

		new Ajax.Request(form.action, { 
			parameters: params ,
			onSuccess: function(transport) {
                retjson = transport.responseText.evalJSON();
                if (retjson.success) {
                	outputContainer.update(retjson.html);
                } else {
	                alert('Failed to get shipment details.');
                }
             },
             onError: function() {
	             alert('An error occurred querying the server for the shipment.');
             }
		});
	}
}

var NewShipmentForm = new Class.create();
NewShipmentForm.prototype = {
		_itemWeight: 0,
		_icePackWeight: 0,
		_form: null,
		_container: null,
		_icePackDisplay: null,
		_icePackValue: null,
		_shippingWeightValue: null,
		_self: null,
		_printLabelFlagValue: null,
        _printStation: null,
		
	    initialize : function(data){
	    	this._itemWeight = data.itemWeight;
	    	this._icePackWeight = data.icePackWeight;
	    	this._form = data.form;
	    	this._container = data.container;
	    	this._icePackDisplay = data.icePackDisplay;
	    	this._icePackValue = data.icePackValue;
            this._printStation = data.printStation;
	    	this._shippingWeightValue = data.shippingWeightValue;
	    	this._printLabelFlagValue = data.printLabelFlagValue;
	    	
	    	var self = this;
	    	
	    	Event.observe(data.manualButton, 'click', function(event) { 
	    		self.shipManual(event);
	    	});
	    	Event.observe(data.printButton, 'click', function(event) {
	    		self.createShipment(event, true);
	    	});
	    	/*
	    	Event.observe(data.cancelButton, 'click', function(event) {
	    		self.cancelShipment(event);
	    	});
	    	*/
	    	Event.observe(data.addIceButton, 'click', function() {
	    		self.updateIcePackCount(1);
	    	});
	    	Event.observe(data.removeIceButton, 'click', function() {
	    		self.updateIcePackCount(-1);
	    	});
	    	
	    	if (data.shipMethodToggle !== 'undefined') {
	    		Event.observe(data.shipMethodToggle, 'click', function(event, element) {
	    	    	event.stop();
	    			var parent = $(this).up();
	    			parent.next().show();
	    			parent.remove();
	    		});
	    	}
	    		
	    	
	    },
	    
	    shipManual: function(event) {
	    	// make it so we don't need a label
	    	this.createShipment(event, false);
	    },
	    
	    createShipment: function(event, print) {
	   	   	event.stop();
	   	   	var shipmentForm = this._form;

            var qtyInputs = shipmentForm.select("input[type='text'][name^='items[']");

            var hasQty = false;
            for(var i=0; i<qtyInputs.length; i++){
                if(qtyInputs[i].value > 0) {
                    hasQty = true;
                    break;
                }
            }

            if (!hasQty) {
                alert('Please specify a quantity for at least one item.');
                return;
            }

	   	   	var printLabelVal = (print) ? 1 : 0;
	   	   	this._printLabelFlagValue.value = printLabelVal; // updates the form element
	   	   	
	   	   	var targetContainer = this._container;
			new Ajax.Request(shipmentForm.action, { 
						parameters: shipmentForm.serialize(true) ,
						onSuccess: function(transport) {
			                retjson = transport.responseText.evalJSON();
			                if (retjson.success) {
			                	targetContainer.update(retjson.html);
			                	resetSearch();
			                } else {
				                var msg = retjson.hasOwnProperty('message') ? 
						                retjson.message :
						                'There was an error in printing the shipment.';
				                alert(msg);
			                }
			             },
			             onError: function() {
				             alert('An error connecting to the server to print.');
			             }
			});

			return false;		
	    },
	    
	    updateIcePackCount: function(mod) {
			var currentCount = parseInt(this._icePackValue.value);

			// make sure not going negative
			if (currentCount == 0 && mod < 0)
			{
				return;
			}
			var newCount = currentCount + mod;
			this._icePackValue.value = newCount;
			this._icePackDisplay.update(newCount + ' Ice Packs');
			
			var totalWeight = this._itemWeight + ( newCount * this._icePackWeight );
			this._shippingWeightValue.update(totalWeight);

		},
	    
	    cancelShipment: function(event) {
	    	event.stop();
	    	this._container.update('');
	    },
	    
}