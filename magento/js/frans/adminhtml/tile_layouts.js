jQuery(document).ready(function($){

	var productOptions = [];
	var categoryOptions = [];
	var body = $('body');

	if(body.hasClass('adminhtml-catalog-category-edit')){
		// load when Varien event is triggered by displaying a tab, if you're editing a category
		varienGlobalEvents.attachEventHandler('showTab', function(arg){
			// we're only interested in the tab for group 5 (Display Settings)
			if(arg.tab.id == 'category_info_tabs_group_5'){
				loadLayoutImages(1);
			}
		});
	} else if(body.hasClass("adminhtml-system-config-edit") && $("#system_config_tabs a[href*='section/frans/']").hasClass('active')){
		// load immediately, if you're editing the Fran's Settings tab in system configuration
		loadLayoutImages(4);
	}

	// update the Product Grid Layout or Child Category Layout icons whenever the dropdowns change
	body.on('change', 'select[name*="[product_grid_layout]"]', function(){
		var icon = $('#product-grid-layout-icon');
		icon.attr('src', ''); // clear out the current src to fix bug where replacement image won't load
		icon.attr('src', productOptions[$(this).val()]);
	});
	body.on('change', 'select[name*="[child_category_layout]"]', function(){
		var icon = $('#child-category-layout-icon');
		var currentSelection = $(this).val();
		if(categoryOptions.hasOwnProperty(currentSelection)){
			icon.attr('src', ''); // clear out the current src to fix bug where replacement image won't load
			icon.attr('src', categoryOptions[currentSelection]).show();
		} else {
			icon.hide();
		}
	});

	function loadLayoutImages(directoryLevelDepth){

		// we can't get a Magento base admin URL in JS, so we'll build a relative path up and out of the current directory
		var directoryLevels = '';
		for(i = 0; i < directoryLevelDepth; i++){
			directoryLevels += '../'
		}

		var productGridLayoutSelect = $('select[name*="[product_grid_layout]"]');
		var childCategoryLayoutSelect = $('select[name*="[child_category_layout]"]');

		// we don't know what the option INT values correspond to yet, so load them via ajax
		if(productGridLayoutSelect.length > 0){
			$.ajax({
				url: directoryLevels + 'catalog_ajax/getProductGridLayouts',
				dataType: 'json',
				success: function(data){
					productOptions = [];
					$.map(data, function(value, key) {
						productOptions[key] = value;
					});
					$('#product-grid-layout-icon').attr('src', productOptions[productGridLayoutSelect.val()]);
				}
			});
		}
		if(childCategoryLayoutSelect.length > 0){
			$.ajax({
				url: '../catalog_ajax/getChildCategoryLayouts',
				dataType: 'json',
				success: function(data){
					categoryOptions = [];
					var currentSelection = childCategoryLayoutSelect.val();
					$.map(data, function(value, key) {
						categoryOptions[key] = value;
					});
					if(categoryOptions.hasOwnProperty(currentSelection)){
						$('#child-category-layout-icon').attr('src', categoryOptions[currentSelection]).show();
					} else {
						$('#child-category-layout-icon').hide();
					}

				}
			});
		}

		// create the element for the icons, if they don't exist yet
		if($('#product-grid-layout-icon').length == 0){
			$('select[name*="[product_grid_layout]"] ~ .note').after('<img id="product-grid-layout-icon" />');
		}
		if($('#child-category-layout-icon').length == 0){
			$('select[name*="[child_category_layout]"] ~ .note').after('<img id="child-category-layout-icon" />');
		}
	}

});