var colorDefault = "#f58200";
var colorDone = "#43af37";
var knobSettings = {
	readOnly: true,
	width: 50,
	height: 50,
	cursor: false,
	fgColor: colorDefault,
	bgColor: "transparent",
	thickness: 0.2,
	angleOffset: 360,
	format: function(v) {
		return v + "%";
	}
};

// mark a step done, by ID; provide an array of IDs to loop through them, returns bool of whether anything was changed
function done(ids, event){
	ids = stringOrArray(ids);
	for(i = 0; i < ids.length; i++){
		// ID-specific logic for adding additional functionality to Done button actions
        var container = false;
        var areaId = ids[i];
        switch (areaId)
        {
            case 'order-shipping_address':
                container = order.shippingAddressContainer;
                break;
            case 'order-billing_address':
                container = order.billingAddressContainer;
                break;
        }

        if (container && !order.isMultiship)
        {
            data = order.serializeData(container);
            data = data.toObject();
            data['reset_shipping'] = 1;
            order.isShippingMethodReseted = true;
            order.loadArea(['shipping_address', 'billing_address', 'shipping_method', 'totals', 'items'], true, data);
            undo(['order-shipping_method'], true); // just need something in the second argument so it doesn't spawn another callback
        }
	}
	return setProperty(ids, 'done',  1);
};

// mark a step not done, by ID; provide an array of IDs to loop through them, returns bool of whether anything was changed
function undo(ids, event){
	ids = stringOrArray(ids);
	for(i = 0; i < ids.length; i++){
		// ID-specific logic for adding additional functionality to Done button actions
		switch(ids[i]){
			case 'order-shipping_method':
				if(typeof event === 'undefined'){ // reset shipping method, but only if there's no event associated with the undo
					shippingAddressData = order.serializeData(order.shippingAddressContainer);
					billingAddressData = order.serializeData(order.billingAddressContainer);
					data = shippingAddressData.merge(billingAddressData).toObject();
					order.resetShippingMethod(data);
				}
				break;
		}
	}
	return setProperty(ids, 'done', 0);
};

// update one property (done, required, or hidden) of a step, by ID; provide an array of IDs to loop through them, returns bool whether anything was changed
function setProperty(ids, property, value){
	ids = stringOrArray(ids);
	var data = {};
	var areasToLoad = ['progress'];
	var silentMode = false;
	for(i = 0; i < ids.length; i++){
        var idToCheck = ids[i];
		if(idToCheck == 'order-items-wrapper') silentMode = true; // don't send Ajax transmission if updating items
        // sometimes we just want to mark the shipping method as not done but not do other work
        // the undo method will reset shipping method (via Ajax); if we have display in there, it won't do the ajax update
        // but we need it to clear out the value in the progress
        idToCheck = (idToCheck != 'order-shipping_method-display') ? idToCheck : 'order-shipping_method';
		var stepElement = jQuery('#order-progress .steps').find('li[data-step-id='+idToCheck+']');
		var currentValue = getProperty(idToCheck, property);
		// only proceed if the new status differs from the existing one
		if(currentValue == !value){
			stepElement.toggleClass(property); // toggle the DOM property (class) for instant feedback
			data['steps[' + idToCheck + '][' + property + ']'] = value; // add this property change to web request data
		}
	}
	// only proceed if nothing has been added to the data object
	var didChange = !jQuery.isEmptyObject(data);
	if(didChange){
		updateProgress();
		// if(!silentMode){
		// 	order.loadArea(areasToLoad, false, data); // send the web request and refresh the block
		// }
	}
	return didChange; // return as a status so we can track if the action changed anything
};

// get one property (done, required, or hidden) of a step, by ID
function getProperty(id, property){
	var stepElement = jQuery('#order-progress .steps').find('li[data-step-id='+id+']');
	return stepElement.hasClass(property);
};

// accepts a string or array; returns the array or converts the string to one
function stringOrArray(data){
	if(typeof data == 'string'){
		data = [data];
	}
	return data;
};

// Read the number of completed required steps and update the progress bar accordingly
function updateProgress(){
	// TODO: Animate transitions between percentages
	var knob = jQuery("#knob");
	var steps = jQuery(".steps li.required:not(.hidden)"); // only count required, un-hidden steps toward completion
	var doneStepCount = steps.filter('.done').length;
	var percentDone = (doneStepCount > 0) ? Math.ceil((doneStepCount / steps.length) * 100) : 0; // calculate progress percentage
	var lastPercentDone = parseInt(knob.val());
	var isSubmitDisabled = (percentDone < 100);

	if(lastPercentDone < 100 && percentDone == 100){ // change the indicator color when 100% is achieved
		knobSettings.fgColor = colorDone;
		knob.trigger('configure', knobSettings).css('color', colorDone);
	} else if(lastPercentDone == 100 && percentDone < 100) { // change the indicator color when percentage decreases from 100%
		knobSettings.fgColor = colorDefault;
		knob.trigger('configure', knobSettings).css('color', colorDefault);
	}

	// enable or disable the submit button
	jQuery('.progress-submit').toggleClass('disabled', isSubmitDisabled).prop('disabled', isSubmitDisabled);

	knob.val(percentDone).trigger('change');
};

function scrollToElement(id){
	var body = jQuery('body');
	var maxPosition = body.height() - jQuery(window).height();
	var startPosition = body.scrollTop();
	var endPosition = jQuery('#'+id).offset().top - 45;
	if(endPosition > maxPosition) endPosition = maxPosition;
	var distance = Math.abs(startPosition - endPosition);
	var duration = Math.floor(distance * 0.15);
	body.delay(0).animate({
		scrollTop: endPosition
	}, duration);
};


document.observe("dom:loaded", function() {


	$$("body").invoke("on", "click", ".progress-done", function(event, element) {
		var parentId = element.up('.progress-step').id;
		done(parentId, event);
	});

	$$("body").invoke("on", "click", ".progress-undo", function(event, element) {
		var parentId = element.up('.progress-step').id;
		undo(parentId, event);
	});

	$$("body").invoke("on", "click", ".steps li a", function(event, element) {
		Event.stop(event);
		var targetId = element.up('li').readAttribute('data-step-id');
		scrollToElement(targetId);
	});

	$$("body").invoke("on", "click", "#add_shipment_button", function(event, element) {
		undo('order-shipments');
	});

	$$("body").invoke("on", "change", "#order-form_account *", function(){undo('order-form_account');});
	$$("body").invoke("on", "change", "#order-form_accounting *", function(){undo('order-form_accounting');});
	$$("body").invoke("on", "change", "#order-items-wrapper *", function(){
		var stepsToUndo = order.isMultiship ?  'order-items-wrapper' : ['order-items-wrapper', 'order-shipping_method-display'];
		undo(stepsToUndo);
	});
    $$("body").invoke("on", "keyup", "#order-items-wrapper .frans-options .input-textarea", function(e,elm){
        if(order.isMultiship){
            undo('order-items-wrapper');
            var textAreaId = elm.readAttribute('id');
            $$('button[data-ta-id="' + textAreaId + '"]').first().removeClassName('disabled');
        }
    });
	$$("body").invoke("on", "change", "#order-additional_fields *", function(){undo('order-additional_fields');});
	$$("body").invoke("on", "change", "#order-shipments *", function(){undo('order-shipments');});
	$$("body").invoke("on", "change", "#order-shipping_address *", function(){undo('order-shipping_address');});
	$$("body").invoke("on", "change", "#order-billing_address *", function(){undo('order-billing_address');});
	$$("body").invoke("on", "change", "#order-shipping_method *", function(event){undo('order-shipping_method', event);});
	$$("body").invoke("on", "change", "#order-payment_method *", function(){undo('order-payment_method');});
	$$("body").invoke("on", "change", "#order-gift_card *", function(){undo('order-gift_card');});
	$$("body").invoke("on", "change", "#order-comments *", function(){undo('order-comments');});
	$$("body").invoke("on", "change", "#order-admin_notes *", function(){undo('order-admin_notes');});
    $$("body").invoke("on", "change", "#order-customer_notes *", function(){undo('order-customer_notes');});

});
