var customerAddressForm;

function addOrEditAddress(url, targetElementId) {
    new Ajax.Request(url, {
        parameters: {form_key: FORM_KEY},
        evalScripts: true,
        onSuccess: function(transport) {
            try {
                if (transport.responseText.isJSON()) {
                    var response = transport.responseText.evalJSON()
                    if (response.error) {
                        alert(response.message);
                    }
                    if(response.ajaxExpired && response.ajaxRedirect) {
                        setLocation(response.ajaxRedirect);
                    }
                } else {
                    $(targetElementId).update(transport.responseText);
                }
            }
            catch (e) {
                $(targetElementId).update(transport.responseText);
            }
        }
    });
}

varienGrid.prototype.doAddAddress = function(addUrl, targetElementId) {
    addOrEditAddress(addUrl, targetElementId);
}

varienGrid.prototype.doEditAddress = function(grid, evt) {
    var element = Event.findElement(evt, 'tr');
    if(['a', 'input', 'select', 'option'].indexOf(Event.element(evt).tagName.toLowerCase())!=-1) {
        return;
    }

    if(element.title)
    {
        // todo - unhardcode
        addOrEditAddress(element.title, 'customer_info_tabs_addresses_content');
    }

}

varienTabs.prototype.refreshTab = function(tab) {
    tab.addClassName('notloaded');
    // clear out any error/changed states
    tab.removeClassName('changed').removeClassName('error');
    this.showTabContent(tab);
}

varienGrid.prototype.cancelAddressEdit = function() {
    customer_info_tabsJsTabs.refreshTab($('customer_info_tabs_addresses'));
}

varienTabs.prototype.confirmDeleteAddress = function(url) {
    if( confirm('Are you sure you want to delete this address?'))
    {
        addOrEditAddress(url, 'customer_info_tabs_addresses_content');
    }
}

varienTabs.prototype.saveCustomerAddress = function(url, formContainer) {

    if (customerAddressForm.validator.validate())
    {
        var addressFields = formContainer.select('input', 'select', 'textarea')
        addressFields.push($$("input[name=form_key]")[0]);

        new Ajax.Request(url, {
            parameters: Form.serializeElements(addressFields),
            method: 'post',
            evalScripts: true,
            onSuccess: function(transport) {
                try {
                    if (transport.responseText.isJSON()) {
                        var response = transport.responseText.evalJSON()
                        if (response.error) {
                            alert(response.message);
                        }
                        if(response.ajaxExpired && response.ajaxRedirect) {
                            setLocation(response.ajaxRedirect);
                        }
                    } else {
                        customer_info_tabsJsTabs.refreshTab($('customer_info_tabs_addresses'));
                    }
                }
                catch (e) {
                    $(targetElementId).update(transport.responseText);
                }
            }
        });
    }
}

