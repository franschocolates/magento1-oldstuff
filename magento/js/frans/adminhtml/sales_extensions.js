// filter out the CC number in submit
varienForm.prototype._submit = varienForm.prototype._submit.wrap(
    function(callOriginal) {
       // just remove the CC number if present,
        // and then submit as normal.
        if ($('stripe_cc_number'))
        {
            $('stripe_cc_number').disable();
            $('stripe_cc_cid').disable();
        }

        callOriginal();
    });

var FransAdminOrder = Class.create(AdminOrder, {
   // overrides

    // override initialize so that we can set whether we are multiship, and include retail store addresses
    initialize: function($super, data) {
       $super(data);

       if(!data) data = {};
       this.isMultiship = (data.is_multiship) ? Boolean(parseInt(data.is_multiship)) : false; // ugly, but there's no way to parse a string to bool directly
	   this.stores = data.stores ? data.stores : $H({});
   }

    // override prepareArea so that we always include coupons if we include items
    , prepareArea:  function($super, area) {
        if (area instanceof Array) {
            if (area.indexOf('items') > -1) {
                area.push('coupons');

                // also push shipments if we are updating items....
                if (this.isMultiship) {
                    area.push('shipments');
                }
            }
        }

        return $super(area);
    }

	// mirrors selectAddress but governs the retail store drop-down
	, selectStore : function(el, container){
		id = el.value;
		if (id.length == 0) {
			id = '0';
		}
		if(this.stores[id]){
			this.fillAddressFields(container, this.stores[id]);
		}
		else{
			this.fillAddressFields(container, {});
		}

		var data = this.serializeData(container);
		data[el.name] = id;
		if(this.isShippingField(container) && !this.isShippingMethodReseted){
			this.resetShippingMethod(data);
		}
		else{
			this.saveData(data);
		}

		// TODO: choosing a retail store should set the corresponding Store Pickup shipping method
		//this.setShippingMethod('method_pickupatstore_DT');

		// uncheck the Save In Address Book checkbox if a retail store is chosen
		$$('order-shipping_address_save_in_address_book').checked = false;
	}

    // allow us to show the multiship button when available/applicable
    , dataShow: function($super) {
        if ($('switch_multiship_button') && !this.isMultiship) {
            $('switch_multiship_button').show();
        }

        $super();
    }

    // override loadArea so that we can force it to do things it doesn't want
    // like update areas without showing a loader
    , loadArea: function($super, area, indicator, params, force) {
        var force = force || false;

        if (force) {
            var url = this.loadBaseUrl;
            if (area) {
                area = this.prepareArea(area);
                url += 'block/' + area;
            }
            if (indicator === true) indicator = 'html-body';
            params = this.prepareParams(params);
            params.json = true;
            if (!this.loadingAreas) this.loadingAreas = [];
            this.loadingAreas = area;
            new Ajax.Request(url, {
                parameters:params,
                loaderArea: indicator,
                onSuccess: function(transport) {
                    var response = transport.responseText.evalJSON();
                    this.loadAreaResponseHandler(response);
                }.bind(this)
            });
        }
        else {
            $super(area, indicator, params);
        }
    }

	// override forced serialization of billing method
	, prepareParams : function(params){
		if (!params) {
			params = {};
		}
		if (!params.customer_id) {
			params.customer_id = this.customerId;
		}
		if (!params.store_id) {
			params.store_id = this.storeId;
		}
		if (!params.currency_id) {
			params.currency_id = this.currencyId;
		}
		if (!params.form_key) {
			params.form_key = FORM_KEY;
		}
		// don't serialize the billing method data if shipping address is inapplicable
		if(!params['steps[order-shipping_address][hidden]']){
			var data = this.serializeData('order-billing_method');
			if (data) {
				data.each(function(value) {
					params[value[0]] = value[1];
				});
			}
		}
		return params;
	}

	// override processOverlay to improve the CSS that is applied
	, processOverlay: function($super, elId, show) {
		$super(elId, show);
		var el = $(elId);
		if (!el) return false;
		var parentEl = el.up(1);
		el.setStyle({
			width: 'auto',
			height: 'auto',
			top: 0,
			left: 0,
			bottom: 0,
			right: 0
		});
		parentEl.setStyle({ position: 'relative' });
	}

    , resetShippingMethod: function($super, data) {
		if(getProperty('order-shipping_address', 'done') && getProperty('order-billing_address', 'done')){
			data['collect_shipping_rates'] = 1;
			this.isShippingMethodReseted = false;
		} else {
			data['reset_shipping'] = 1;
			this.isShippingMethodReseted = true;
		}
        // pass force as the last param so that it will still load it in the background
        // it just won't show a loading pop up
        this.loadArea(['shipping_method'], false, data, true);
    }

    // override fillAddressFields so that we can handle the extra level of [] that is present
    , fillAddressFields : function($super, container, data){
            var regionIdElem = false;
            var regionIdElemValue = false;

            // if it's billing or we aren't multiship, use parent
            if (container.indexOf('billing') >= 0 || !this.isMultiship)
            {
                $super(container, data);
                return;
            }

            var fields = $(container).select('input', 'select', 'textarea');
            // original:
            //var re = /[^\[]*\[[^\]]*\]\[([^\]]*)\](\[(\d)\])?/;
            // had to add an extra section with [] to match the actual field name.
            var re = /[^\[]*\[[^\]]*\]\[([^\]]*)\]\[([\w\d_]*)\](\[(\d)\])?/;
            for(var i=0;i<fields.length;i++){
                // skip input type file @Security error code: 1000
                if (fields[i].tagName.toLowerCase() == 'input' && fields[i].type.toLowerCase() == 'file') {
                    continue;
                }
                var matchRes = fields[i].name.match(re);
                if (matchRes === null) {
                    continue;
                }
                var name = matchRes[2]; // upped from 1 to 2 since we have extra [] with address id
                var index = matchRes[4]; // upped from 3 to 4 since we have extra [] with address id

                if (index){
                    // multiply line
                    if (data[name]){
                        var values = data[name].split("\n");
                        fields[i].value = values[index] ? values[index] : '';
                    } else {
                        fields[i].value = '';
                    }
                } else if (fields[i].tagName.toLowerCase() == 'select' && fields[i].multiple) {
                    // multiselect
                    if (data[name]) {
                        values = [''];
                        if (Object.isString(data[name])) {
                            values = data[name].split(',');
                        } else if (Object.isArray(data[name])) {
                            values = data[name];
                        }
                        fields[i].setValue(values);
                    }
                } else {
                    fields[i].setValue(data[name] ? data[name] : '');
                }

                if (fields[i].changeUpdater) fields[i].changeUpdater();
                if (name == 'region' && data['region_id'] && !data['region']){
                    fields[i].value = data['region_id'];
                }
            }
    }

    // override to handle not resetting shipping on multiship
    , productConfigureSubmit : function($super, listType, area, fieldsPrepare, itemsFilter) {

		if (!this.isMultiship) {
            return $super(listType, area, fieldsPrepare, itemsFilter);
        }

        // our code is pretty much exactly the same, we just don't set reset shipping if we are doing an add.

        // prepare loading areas and build url
        area = this.prepareArea(area);
        this.loadingAreas = area;
        var url = this.loadBaseUrl + 'block/' + area + '?isAjax=true';

        // prepare additional fields
        fieldsPrepare = this.prepareParams(fieldsPrepare);

        // only change is here - on add we don't want to reset shipping
        if (listType !== 'product_to_add')
        {
            fieldsPrepare.reset_shipping = 1;
        }
        fieldsPrepare.json = 1;

        // create fields
        var fields = [];
        for (var name in fieldsPrepare) {
            fields.push(new Element('input', {type: 'hidden', name: name, value: fieldsPrepare[name]}));
        }
        productConfigure.addFields(fields);

        // filter items
        if (itemsFilter) {
            productConfigure.addItemsFilter(listType, itemsFilter);
        }

        // prepare and do submit
        productConfigure.addListType(listType, {urlSubmit: url});
        productConfigure.setOnLoadIFrameCallback(listType, function(response){
            this.loadAreaResponseHandler(response);
        }.bind(this));
        productConfigure.submit(listType);
        // clean
        this.productConfigureAddFields = {};
    }

	// update progress to hide or show steps as their overlays appear or disappear, respectively
	, overlay: function($super, elId, show, observe) {
		$super(elId, show, observe);
		if(!this.isMultiship){ // the following logic should not be applied to multiship orders
			switch(elId){
				case 'address-shipping-overlay':
					var hiddenValue = show ? 0 : 1; // invert value of show param and cast as int
					var shipMethodInitiallyHidden = getProperty('order-shipping_method', 'hidden'); // previous value before change
					// if setting the shipping address overlay, hide/show that step and shipping method in progress checklist
					setProperty(['order-shipping_address', 'order-shipping_method'], 'hidden', hiddenValue);
					// also reset shipping method progress if that step was previously hidden and is now being reshown
					if(hiddenValue == 0 && shipMethodInitiallyHidden == true){
						undo('order-shipping_method');
					}
					break;
			}
		}
	}

    // changed to do different things
    , saveShippingData: function($super) {
        // make sure we have values for everything
        var arrivalDate = $('preferred_arrival_date').value;
        var plannedDate = $('planned_ship_date').value;
        var price = $('base_shipping_amount_adjusted').value;
        var originalPrice = ($('current_shipping_amount')) ? $('current_shipping_amount').value : 0;
        var method = $$("input:checked[type=radio][name='order[shipping_method]']")[0].value;

        var message = '';

        if (!method) {
            message += 'Please select a shipping method.\n';
        }

        if (!arrivalDate) {
            message += 'Please enter an arrival date.\n';
        }

        if (!plannedDate) {
            message += 'Please enter a planned ship date.\n';
        }

        try{
            if(!arrivalDate.match(/^(\d{1,2})\/(\d{1,2})\/(\d{2,4})$/)) {
                message += 'Invalid arrival date.\n';
            }
            if(!plannedDate.match(/^(\d{1,2})\/(\d{1,2})\/(\d{2,4})$/)) {
                message += 'Invalid shipment date.\n';
            }
           if(message.length == 0) {

               var plannedDataAsDate = new Date(plannedDate);
               var arrivalDateAsDate = new Date(arrivalDate);
               if (Object.prototype.toString.call(plannedDataAsDate) === "[object Date]") {
                   if (isNaN(plannedDataAsDate.getTime())) {
                       message += 'Invalid shipment date.\n';
                   }
               } else {
                   message += 'Invalid shipment date.\n';
               }

               if (Object.prototype.toString.call(arrivalDateAsDate) === "[object Date]") {
                   if (isNaN(arrivalDateAsDate.getTime())) {
                       message += 'Invalid arrival date.\n';
                   }
               } else {
                   message += 'Invalid arrival date.\n';
               }

               if (plannedDataAsDate >= arrivalDateAsDate) {
                   message += 'Shipment date must be before arrival date.\n';
               }
           }
        }catch(ex){
            message += 'Invalid shipment or arrival date.\n';
        }

        if (!price) {
            message += 'Please enter a price.';
        }

        if (message.length > 0) {
            alert(message);
            return;
        }

        var data = {};
        data['order[shipping_method]'] = method;
        data['order[preferred_arrival_date]'] = arrivalDate;
        data['order[planned_ship_date]'] = plannedDate;
        data['order[base_shipping_amount_adjusted]'] = price;
        var areasToLoad = ['shipping_method', 'totals'];

        // if the price has changed and one was zero, need to refresh billing method
        originalPrice = parseFloat(originalPrice);
        price = parseFloat(price);
        if (originalPrice != price && (originalPrice == 0 || price == 0))
        {
            areasToLoad.push('billing_method');
        }

        this.loadArea(areasToLoad, true, data);

    }

    // This overridden to add an extra param indicating we just toggled
    , setShippingAsBilling : function($super, flag){
        this.disableShippingAddress(flag);
        if(flag){
            var data = this.serializeData(this.billingAddressContainer);
        }
        else{
            var data = this.serializeData(this.shippingAddressContainer);
        }
        data = data.toObject();
        data['shipping_as_billing'] = flag ? 1 : 0;
        data['reset_shipping'] = 1;

        if (!flag)
        {
            // we are no longer the same as billing, default to saving in address book
            data['order[shipping_address][save_in_address_book]'] = 1;
        }

        this.loadArea(['shipping_method', 'billing_method', 'shipping_address', 'totals', 'giftmessage'], true, data);
    }

	, sidebarApplyChanges : function($super, auxiliaryParams) {
		if ($(this.getAreaId('sidebar'))) {
			var data = {};
			if (this.collectElementsValue) {
				var elems = $(this.getAreaId('sidebar')).select('input');
				for (var i=0; i < elems.length; i++) {
					if (elems[i].getValue()) {
						data[elems[i].name] = elems[i].getValue();
						data['steps[order-items-wrapper][done]'] = 0; // set "Add Items and Options" progress to not done
					}
				}
			}
			if (auxiliaryParams instanceof Object) {
				for (var paramName in auxiliaryParams) {
					data[paramName] = String(auxiliaryParams[paramName]);
				}
			}
			data.reset_shipping = true;
			this.loadArea(['sidebar', 'items', 'shipping_method', 'billing_method','totals', 'progress', 'giftmessage'], true, data);
		}
	}

    // new functions
    , setMultiship: function() {
        this.isMultiship = true;
        this.shippingAsBilling = false; //otherwise it updates all shipping on billing.
        $('switch_multiship_button').hide();
        $(this.getAreaId('data')).callback = 'dataLoaded';
        this.loadArea('data', true, { 'order[set_multiship]': true });
    }

    , loadShipmentShippingRates: function(addressId) {
        // sometimes we are in edit mode and sometimes we aren't
        // for now, only try and persist data if we are in edit mode and have a form
        var params = { };

        if ($('shipment-address-form-' + addressId))
        {
            var addressFormContainer = $('shipment-address-form-' + addressId);
            var shipmentForm = new varienForm(addressFormContainer.id);

            if (shipmentForm.validator.validate())
            {
                // also make sure we have some qty
                // find any text inputs in this container that have the qty name
                // and then make sure at least one has a value > 0
                var qtySelector = '#order-shipment_address_' + addressId + " input[type=text][name$='[qty]']";
                var qtyCount = $$(qtySelector).findAll(function(elm) { return parseInt(elm.value) > 0; } );

                if (qtyCount.length == 0)
                {
                    alert('You must specify at least one item for this address.');
                    return;
                }

                // we serialize the entire section to include the qtys
                var fields = $('order-shipment_address_' + addressId).select('select', 'input', 'textarea');
                params = Form.serializeElements(fields, { hash: true });
                params['order[update_shipments]'] = true; // tell it to update shipments, so that it doesn't other times
            }
            else
            {
                // errors shown, return
                return;
            }
        }

        params['collect_shipping_rates'] = 1;
        params['edit_address_id'] = addressId;

        this.loadArea(['shipments', 'totals'], true, params);
    }

	, itemsUpdate : function(){
		var area = ['sidebar', 'items', 'shipping_method', 'billing_method', 'totals', 'giftmessage', 'account'];
		// prepare additional fields
		var fieldsPrepare = {update_items: 1};
		var info = $('order-items_grid').select('input', 'select', 'textarea');
		for(var i=0; i<info.length; i++){
			if(!info[i].disabled && (info[i].type != 'checkbox' || info[i].checked)) {
				fieldsPrepare[info[i].name] = info[i].getValue();
			}
		}
		fieldsPrepare = Object.extend(fieldsPrepare, this.productConfigureAddFields);
		this.productConfigureSubmit('product_to_add', area, fieldsPrepare);
		this.orderItemChanged = false;
	}

    , updateItemNotes: function(itemId){
        url = BASE_URL + 'sales_order_create/saveQuoteNote';
        indicator = 'html-body';
        new_note = $('item_' + itemId + '_notes').getValue();
        params = this.prepareParams({ item_id: itemId, item_note: new_note });
        params.json = true;
        new Ajax.Request(url, {
            parameters:params,
            loaderArea: indicator,
            onSuccess: function(transport) {
                var response = transport.responseText.evalJSON();

                if (response.success) {
                    $$('button[data-ta-id="item_' + itemId + '_notes"]').first().addClassName('disabled');
                    if(this.isMultiship) {
                        if($$('button[data-ta-id].disabled').length == $$('button[data-ta-id]').length) {
                            $$('li[data-step-id="order-items-wrapper"]').first().addClassName('done');
                            updateProgress();
                        }
                    }
                }
                else {
                    alert('Error storing item notes!');
                }

            }.bind(this)
        });
    }

    , saveShipmentShippingMethod: function(addressId) {
        var shippingDataContainer = $('shipment-method-form-' + addressId);
        var validationForm = new varienForm(shippingDataContainer.id);

        if (validationForm.validator.validate())
        {
            var fields = shippingDataContainer.select('select', 'input', 'textarea');
            var params = Form.serializeElements(fields, { hash: true });
            params['order[shipments][' + addressId + '][save_method]'] = true;
            params['order[update_shipments]'] = true; // tell it to update shipments, so that it doesn't other times

            this.loadArea(['shipments', 'totals', 'billing_method'], true, params);
        }
    }

    , addShipment: function() {
        this.loadArea('shipments', true, { 'order[add_shipment]': true });
    }

    , bindShipmentAddressFields: function(container, addressId) {
        var fields = $(container).select('input', 'select', 'textarea');
        for(var i=0;i<fields.length;i++){
            fields[i].store('address_id', addressId);

            Event.observe(fields[i], 'change', this.changeShipmentAddressField.bind(this));
        }
    }

    , bindShipmentQuantityFields: function(container, addressId) {
        var fields = $(container).select("input[type='text'][name$='[qty]']");
        for(var i=0;i<fields.length;i++){
            fields[i].store('address_id', addressId);
            Event.observe(fields[i], 'change', this.changeShipmentAddressField.bind(this));
        }
    }

    , changeShipmentAddressField: function(event) {
        var field = Event.element(event);
        var addressId = field.retrieve('address_id');
        var resetRatesField = $('shipment_' + addressId + '_reset_rates');
        if (resetRatesField)
        {
            resetRatesField.removeClassName('hide');
            resetRatesField.show();
            // note that by putting "hide" on it, it can't be toggled anymore (with prototype)
            // prototype show/hide only affects the style tag, it does not override CSS values
            $('shipment_' + addressId + '_order-shipping-method-dates').addClassName('hide');
        }
    }

    , editShipment: function(addressId) {
        this.loadArea(['shipments'], true, { edit_address_id: addressId });
    }

    , cancelEditShipment: function(addressId) {
        this.loadArea(['shipments'], true, { });
    }

    , removeShipment: function(addressId) {
        if (confirm('Are you sure you want to remove this shipment?'))
        {
            this.loadArea('shipments', true, { 'order[remove_address_id]': addressId });
        }

    }

    // this is basically a copy of select address
    // except that we don't send the data in the background, they have to initiate it
    , selectShipmentAddress : function(el, container){
        id = el.value;
        if (id.length == 0) {
            id = '0';
        }
        if(this.addresses[id]){
            this.fillAddressFields(container, this.addresses[id]);
        }
        else{
            this.fillAddressFields(container, {});
        }
    }

    , setShippingDatesAndPrices: function(selectedOption) {
        var idPrefix = Element.readAttribute(selectedOption, 'data-id-prefix');
        $(idPrefix + 'base_shipping_amount_adjusted').value = Element.readAttribute(selectedOption, 'data-price');
        // we have a || '' to make sure it sets it to blank if there is no value.
        $(idPrefix + 'planned_ship_date').value = Element.readAttribute(selectedOption, 'data-planned-date');
        $(idPrefix + 'preferred_arrival_date').value = Element.readAttribute(selectedOption, 'data-preferred-date');
    }

    , showImport: function() {
        // need to append a file input for them to use
        $('shipment-import-inline-form').insert({
            top: new Element('input', { type: 'file', name: 'import_file', id: 'import_file_entry', title: 'Select File to Import'})
        });
        $('shipment-import-inline-form').show();

        $('show_import_button').hide();
    }

    , validateImport: function(url) {
        // make sure they have entered a file
        if (!$('import_file_entry').value)
        {
            alert('You must specify an input file.');
            return;
        }

        // create an iframe for them to work with
        // so we can post
        var iFrame = new Element('iframe', { id: 'shipments-import-frame', style: 'display:none;' });
        $('order-shipments').insert(iFrame);

        // for the rest of element creation, we use jQuery
        // for some reason prototype doesn't let us find the body of the iframe and create stuff
        var $iFrameBody = jQuery('#shipments-import-frame').contents().find('body');

        var $iFrameForm = jQuery('<form></form>')
            .attr({ action: url, method: 'post', enctype: 'multipart/form-data' });

        $iFrameForm.append(
            jQuery('<input />').attr({ type: 'hidden', name: 'form_key', value: FORM_KEY })
            , jQuery('<input />').attr({ type: 'hidden', name: 'entity', value: 'quote_address'})
        );

        $iFrameForm.appendTo($iFrameBody);

        // move the input file element to the form
        // jQuery works better for these sorts of things
        var $quoteForm = jQuery('#shipments-import-frame').contents().find('form').get(0);
        jQuery('#import_file_entry').appendTo($quoteForm);
        varienLoaderHandler.handler.onCreate({ options: { loaderArea: true } });
        $quoteForm.submit();

        // now hide the inline form
        $('shipment-import-inline-form').hide();
        $('show_import_button').show();
    }

    // adapted from importexport before.phtml
    , showImportValidationResults: function(response) {
        varienLoaderHandler.handler.onComplete();
        if ('object' != typeof(response)) {
            return alert('Invalid response');
        }
        $H(response).each(function(pair) {
            switch (pair.key) {
                case 'show':
                case 'clear':
                case 'hide':
                    $H(pair.value).each(function(val) {if ($(val.value)) $(val.value)[pair.key]();});
                    break;
                case 'innerHTML':
                case 'value':
                    $H(pair.value).each(function(val) {if ($(val.key)) $(val.key)[pair.key] = val.value;});
                    break;
                case 'removeClassName':
                case 'addClassName':
                    $H(pair.value).each(function(val) {if ($(val.key)) $(val.key)[pair.key](val.value);});
                    break;
                default:
                    alert(pair.key + ': ' + pair.value);
                    break;
            }
        });
    }

    , startImport: function(url) {
        indicator = 'html-body';
        params = this.prepareParams({ });
        params.json = true;
        new Ajax.Request(url, {
            parameters:params,
            loaderArea: indicator,
            onSuccess: function(transport) {
                var response = transport.responseText.evalJSON();

                if (response.success) {
                    this.loadArea(['items', 'shipments', 'totals', 'billing_method'], true, { });
                    $$('li[data-step-id="order-items-wrapper"]').first().addClassName('done');
                    $('order-search').addClassName('hide');
                    updateProgress();
                }
                else {
                    alert('Error importing!');
                }

            }.bind(this)
        });
    }

    , saveQuote: function(params) {

        if (this.orderItemChanged) {
            if (!confirm('Some items have changed. Press Cancel to save those items (you will then need to save the quote). Click OK to save the quote without the item changes.')) {
                this.itemsUpdate();
                return;
            }
        }
        var progress = [];
        var url = BASE_URL + 'sales_order_create/saveQuoteProgress';
       jQuery("#order-progress ul.steps li.done").map(function() {
           var step = this.getAttribute('data-step-id');
           progress.push(step);
       });

        var progressJson = JSON.stringify(progress);
        var parent = this;
        jQuery.ajax({
            type: 'POST',
            url: url,
            data: {
                form_key: FORM_KEY,
                progress: progressJson
            }
        }).done(function(x){
            Element.hide('loading-mask');
            var data = params || { };
            data['order[quote_save]'] = 1;
            data['order[admin_notes]'] = $('admin_notes').value;
            data['order[comment][customer_note]'] = $('order-comment-value').value;
            /* not sure this is the best way but will fix the bug for today and should not harm any other logic. */
            if($('order-gift-message'))
            {
                data['order[gift_message]'] = $('order-gift-message').value;
            }
            if($('special_instructions'))
            {
                data['order[special_instructions]'] = $('special_instructions').value;
            }
            jQuery.when(parent.loadArea(['data'], true, data)).then(function(){
                    var getUrl = BASE_URL + 'sales_order_create/getQuoteProgress';
                    jQuery.get(getUrl, function(response){
                        Element.hide('loading-mask');
                        var progressElems = JSON.parse(response);

                        jQuery.each(progressElems, function() {
                            var stepElement = jQuery('#order-progress .steps').find('li[data-step-id='+this+']');
                            stepElement.addClass('done');
                        });
                        updateProgress();
                    });

                }
            );
        });

    }

    // download not currently working, but leaving function here.
    , saveAndDownloadQuote: function() {
        this.saveQuote({download_quote: true});
    }

    , saveAndPrintQuote: function() {
        this.saveQuote({print_quote: true});
    }
});
