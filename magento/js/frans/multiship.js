/**
 *
 */
jQuery(document).ready(function($){

    var timeout = 0;
    var currentPage = 1;
    var pageBreak = 5;
    var hasShippingError = false;

    var returnToMultishipAddresses = function (response) {
        if (response.status == 'success')
        {
            //Update Multiship Address List
            if (response.hasOwnProperty('addressItemsHtml')) {
                $("#address-items-content").replaceWith(response.addressItemsHtml);
                $("#addresses-content").replaceWith(response.addressesHtml);
            }
            updateGiveAll();
        }
        else
        {
            alert('Some data in your shipping info is invalid.');
        }
    };

    var goToMultishipEditAddresses = function (response) {
        if (response.status == 'success')
        {
            var url = response.editUrl;
            //Update Multiship Address List
            if (response.hasOwnProperty('addressItemsHtml')) {
                $("#address-items-content").replaceWith(response.addressItemsHtml);
                $("#addresses-content").replaceWith(response.addressesHtml);
            }

            showAjaxModal(url);
            updateGiveAll();

        }
        else
        {
            alert('Some data in your shipping info is invalid.');
        }
    };


    function giveAllUpdate(evt) {

        var val = $(this).val();
        if (val.length > 1 && val.charAt(0) == '0'){
            val = val.substring(1);
            $(this).val(val);
        }

        var keyCode = evt.keyCode || evt.which;
        if (keyCode == 9) {
            return;
        }

        if (val.length > 0)
        {
            var name = $(this).prop('name');

            // remove giveall from it
            var orderItemSuffix = name.replace('give_all', '');
            $("input:enabled[name$='" + orderItemSuffix + "[qty]']").val(val);
            queueTotalsUpdate();
        }
    }

    function giveOneUpdate(evt) {
        var val = $(this).val();
        if (val.length > 1 && val.charAt(0) == '0'){
            val = val.substring(1);
            $(this).val(val);
        }

        if (val.length > 0 && parseInt(val) !== 'NaN')
        {
            queueTotalsUpdate();
        }
    }

    function queueTotalsUpdate() {
        clearAndRecheckErrors();

        // make sure we have shipping form elements (which means we have addresses)
        if ($("input[name^='shipping_info[']").length > 0)
        {
            clearTimeout(timeout);
            timeout = setTimeout(saveTotals, 250);
        }
    }

    function updateTotals(data) {
        if (data.success) {
            $("#summary-totals").replaceWith(data.totalsHtml);
        }

    }

	function updateItemsAndTotals(data) {
		if (data.status == 'success') {

			$("#summary-totals").replaceWith(data.totalsHtml);
			$("#items-content").replaceWith(data.itemsHtml);

			var pageCount = ($(".item-details .allocation-column").length / pageBreak);

            if (data.hasOwnProperty('addressItemsHtml'))
            {
                $("#address-items-content").replaceWith(data.addressItemsHtml);
                $(".pagination").replaceWith(data.pagerHtml);
            }

			$(".allocation-table").attr('width', (pageCount * 100) + '%');
        }
    }

    function saveTotals() {
        var $form = $('#shipping-form');
        $.post($form.attr('action'), $form.serialize(),
            updateItemsAndTotals, 'json').fail(showError);

    }

	function adjustPage(mod) {

		var table = $('.allocation-table');
		var tableWidth = table.attr('width')
		var pageCount = tableWidth.substring(0, (tableWidth.length - 3));

		if(parseInt(table.css('margin-left')) < 1) {
			if ((mod > 0 && currentPage < pageCount)
				|| (mod < 0 && currentPage > 1))
			{
				currentPage += mod;
				table.css('margin-left', ((currentPage - 1) * -100)+'%');
                $('.page-display').text(currentPage + ' of ' + pageCount);
            }
        }

        if (currentPage == 1) {
            $('.prevlink').addClass('disabled');
        } else {
            $('.prevlink').removeClass('disabled');
        }

        if (currentPage == pageCount) {
            $('.nextlink').addClass('disabled');
        }
        else {
            $('.nextlink').removeClass('disabled');
        }
    }

    function nextPage(evt) {
        evt.preventDefault();
        adjustPage(1);
    }

    function prevPage(evt) {
        evt.preventDefault();
        adjustPage(-1);
    }

	function checkPage() {
		var inPage = Math.floor((pageBreak + $(this).parents('.allocation-column').index()) / pageBreak);
		if (inPage != currentPage) {
			adjustPage(inPage - currentPage);
		}
	}

    function addAddress(evt) {
        evt.preventDefault();

        //show the modal without the close button.
        //customer should need to remove the address
        showAjaxModal($(this).attr('href'), {closeHTML: ''}, showError);
    }




    function removeItem(evt) {
        evt.preventDefault();

        sendDataShowLoading( $(this).attr('href'), { }, updateItemsAndTotals, showError);
    }

    function editAddress(event, element) {
        event.preventDefault();
        var $this = $(this);
        var url = ($this.prop('href')) ? $this.prop('href') : $this.find('a.edit-address').attr('href');

        showAjaxModal(url,  {closeHtml: ""});
    }

    function onNewAddressAdded(response) {
        if(response.hasOwnProperty('pobox')){
            if (response.hasOwnProperty('shippingHtml')) {
                $("#address-modal").html(response.shippingHtml);
                return
            }
        }
        if (response.status == 'success') {
            if (response.invalidFields == 'Yes') {
                if (response.hasOwnProperty('shippingHtml')) {
                    if (response['score'] > 0) {
                        if(response.editAddress == 'Yes'){
                            if (response.hasOwnProperty('addressItemsHtml')) {
                                $("#address-items-content").replaceWith(response.addressItemsHtml);
                                $("#addresses-content").replaceWith(response.addressesHtml);
                            }
                            $("#address-modal").html(response.shippingHtml);
                        }
                        if (response.hasOwnProperty('addressItemsHtml')) {
                            $("#address-items-content").replaceWith(response.addressItemsHtml);
                            $("#addresses-content").replaceWith(response.addressesHtml);
                        }
                        else if (response.hasOwnProperty('address_id')) {
                            $('#shipping-address-' + response.address_id).html(
                                response.address_html
                            );
                        }
                        else if (response.hasOwnProperty('refresh_delivery')) {
                            // need to refresh the entire delivery section, because they changed stuff
                            // enough that the shipping information could have changed
                            jQuery('.checkout-delivery').html(response.deliveryHtml);
                            if (response.hasOwnProperty('summaryHtml')) {
                                jQuery('.order-summary').replaceWith(response.summaryHtml);
                            }
                        }
                        updateGiveAll();

                        $("#address-modal").html(response.shippingHtml);
                    } else {
                        $("#address-modal").html(response.shippingHtml);
                    }
                }
            }
            else {
                if (response.hasOwnProperty('addressItemsHtml')) {
                $("#address-items-content").replaceWith(response.addressItemsHtml);
                $("#addresses-content").replaceWith(response.addressesHtml);
                }
                else if (response.hasOwnProperty('address_id')) {
                $('#shipping-address-' + response.address_id).html(
                    response.address_html
                );
                }
                else if (response.hasOwnProperty('refresh_delivery')) {
                // need to refresh the entire delivery section, because they changed stuff
                // enough that the shipping information could have changed
                jQuery('.checkout-delivery').html(response.deliveryHtml);
                    if (response.hasOwnProperty('summaryHtml')) {
                        jQuery('.order-summary').replaceWith(response.summaryHtml);
                    }
                }
                updateGiveAll();
             }
        }

    }

    function removeAddress(event, element) {
        event.preventDefault();

        makeAjaxRequest($(this).attr('href'), null, onAddressRemoved);
    }

    function onAddressRemoved(response) {
        if(response['status'] == 'success')
        {
            $("#summary-totals").replaceWith(response.totalsHtml);
            $("#address-items-content").replaceWith(response.addressItemsHtml);
            $("#addresses-content").replaceWith(response.addressesHtml);
            jQuery.modal.close();
            updateGiveAll();
        }
        else
        {
            showErrorsInModal(response[msg]);
        }
    }

    function onShippingRefreshed(response) {
        if(response['status'] == 'success')
        {
            $('.multiship-checkout').parent('.frans-tab-content').replaceWith(response.shippingHtml);
        }
        else
        {
            alert('An error occurred reloading your address data. Please refresh the page.');
        }
    }

    function showAddressError(errorDivId) {
        toggleTooltip($('#' + errorDivId), $('#address-error-anchor'), true);
        hasShippingError = true;
    }

    function getEmptyShipments() {
        var shipments = [];

        $('.allocation-names li:not(.add)').each(function(idx) {
            var addressId = $(this).find("input[name='addressId[]']").val();
            var addressPrefix = "shipping_info[" + addressId + "]";

            // find any in this row that have a qty
            var $hasQty = $("input:enabled[name^='" + addressPrefix + "'][name$='[qty]']")
                .filter(function()
                {
                    return parseInt(jQuery(this).val()) > 0;
                });
            if ($hasQty.length == 0) {
                shipments.push(addressId);
            }
        });

        return shipments;
    }

    function highlightShipments(shipments) {
        $.each(shipments, function(idx, val) {
            var selector = ".input-qty[name^='shipping_info[" + val + "'][name$='[qty]']";
            $(selector).addClass('item-error');
            $(".allocation-names input[value=" + val + "]").parent().addClass('recipient-error');
        });
    }

    function getUnassignedProducts() {
        var products = [];

        $('.input-give-all').each(function (index) {
            var itemId = $(this).attr('name').replace('give_all[', '').replace(']', '');
            var itemSuffix = '[' + itemId + '][qty]';
            // find any with no qty at all
            var $hasQty = $("input:enabled[name^='shipping_info'][name$='" + itemSuffix + "']")
                .filter(function()
                {
                    return parseInt(jQuery(this).val()) > 0;
                });
            if ($hasQty.length == 0) {
                products.push(itemId);
            }
        });

        return products;
    }

    function highlightProducts(products) {
        $.each(products, function(idx, val) {
            var selector = ".input-qty[name*='[" + val + "']";
            $(selector).addClass('item-error');
        });

        // get first one and make sure it's visible
        var firstId = products[0];
        var itemGiveAll = $("input[name='give_all[" + firstId + "]']").get(0);
        var firstIndex = $(".input-give-all").index(itemGiveAll);

        var desiredPage = Math.floor((firstIndex + 1) / pageBreak) + 1;

        if (desiredPage != currentPage) {
            adjustPage(desiredPage - currentPage);
        }
    }

    function clearErrors() {
        toggleTooltip(null, $('#address-error-anchor'), false);
        $('.input-qty.item-error').removeClass('item-error');
        $('.allocation-names li.recipient-error').removeClass('recipient-error');
        hasShippingError = false;
    }


    var proceedToMultishipDelivery = function (response) {
        if (response.status == 'success')
        {
            setActiveStep(2);
            jQuery('.checkout-delivery').html(response.deliveryHtml);
            sendSection('multiship_delivery_options');
            if (response.hasOwnProperty('summaryHtml'))
            {
                jQuery('.order-summary').replaceWith(response.summaryHtml);
            }
        }
        else if (response.status == 'error' && response.hasOwnProperty('invalid_addresses'))
        {
            var showMinAmount = false;
            var addressIds = [];
            for (var addressId in response.invalid_addresses)
            {
                if (response.invalid_addresses[addressId] == 'min-amount')
                {
                    showMinAmount = true;
                    addressIds.push(addressId);
                }
            }

            if (showMinAmount) {
                showAddressError('error-min-amount');
                highlightShipments(addressIds);
            }
        }
        else
        {
            alert('Some data in your shipping info is invalid.');
        }
    };

    var proceedToMultishipPayment = function (response) {
        if (response.status == 'success')
        {
            setActiveStep(3);

	        // fix for IE8 bug: response string is too long for $.html() so use innerHTML then evaluate <script> tags within instead
	        var paymentStep = jQuery('.checkout-payment');
	        paymentStep[0].innerHTML = response.paymentHtml;
	        paymentStep.find('script').each(function(){
		        eval(jQuery(this).html());
	        });

            sendSection('multiship_payment_review');
            if (response.hasOwnProperty('summaryHtml'))
            {
                jQuery('.order-summary').replaceWith(response.summaryHtml);
            }
        }
        else
        {
            ///alert('Some data in your shipping info is invalid.'); //Mac wasn't sure if we want an alert.
            if (response.hasOwnProperty('address_ids'))
            {
                highlightAddressDateErrors(response.address_ids);
            }
        }
    };

    function clearAndRecheckErrors() {
        if (hasShippingError) {
            checkShippingErrors();
        }
    }


    function checkShippingErrors() {
        clearErrors();

        var $addresses = $('.allocation-names li');

        if ($addresses.length == 1 && $($addresses[0]).hasClass('add'))
        {
            showAddressError('error-no-addresses');
            return false;
        }
        else if ($addresses.length == 2 && $($addresses[1]).hasClass('add'))
        {
            showAddressError('error-one-recipient');
            return false;
        }

        var emptyShipments = getEmptyShipments();

        if (emptyShipments.length > 0)
        {
            highlightShipments(emptyShipments);
            showAddressError('error-empty-shipment');
            return false;
        }

        var unassignedProducts = getUnassignedProducts();

        if (unassignedProducts.length > 0)
        {
            highlightProducts(unassignedProducts);
            showAddressError('error-products-not-assigned');
            return false;
        }

        // everything passed, we can proceed
        return true;
    }

    function tryFinishShipping() {

        if (checkShippingErrors())
        {
            sendDataShowLoading(getUrl('validateShipping'), { }, proceedToMultishipDelivery, showError);
        }
    }

    function tryFinishDeliveryWithDefaults(evt) {
        evt.preventDefault();

        applyToAll();

        tryFinishDelivery(evt);
    }
    function tryFinishDelivery(evt) {
        evt.preventDefault();

        if (deliveryForm.validator.validate())
        {
            // see if they all have dates that are available
            var $firstFailure = null;
            $(deliveryForm.form).find('.validate-date').each(function(item, element) {
                if (!validateArrivalDate(element)) {
                    $firstFailure = $firstFailure || $(element);
                }
            });

            if ($firstFailure != null)
            {
                $firstFailure.focus();
                return;
            }

            sendDataShowLoading(getUrl('validateDelivery'), $('#delivery-form').serialize(), proceedToMultishipPayment, showError);
        }
    }

    // This function selects the inner text when clicking on qty fields
    function selectInnerText(evt) {
        $(this).select();
    }


    function applyToAll()
    {
        //gather the details of the quick options and attempt to apply them to each shipment.

        //get the data to apply.
        var giftMessage = $("#giftmsg-all").val();
        var instructionMessage = $("#instructmsg-all").val();
        var arrivalDate = $("#arrivaldate-all").val();
        var arrivalBest = $('input[name=quickship_delivery_method]:checked').val();

        //get all the shipments
        var shipments = $("#multiship-delivery-address-list .shipment");
        var calendar = $('body').find(".future-ship-date").data("frans-calendar");
        for(var i = 0; i < shipments.length; i++)
        {
            var shipment = shipments[i];

            $(shipment).find(".gift_message").val(giftMessage);
            $(shipment).find(".special_instructions").val(instructionMessage);

            //set the dates
            var instanceId = $(shipment).find(".future-ship-date").attr("id");
            var instance = calendar.findInstanceById(instanceId);

            if(arrivalBest == "bestvalue" || arrivalBest == "fastest")
            {
                var day = calendar.getDayByBestType(instance, arrivalBest);
                calendar.setDay(instance, day);
            }
            else if(arrivalBest == "future")
            {
                $(shipment).find(".ship-date").val(arrivalDate);
                calendar.selectDayByText(instance);

            }


        }

    }

    function onShippingReopened() {
        sendDataShowLoading(getUrl('getShippingInfo'), { }, onShippingRefreshed, showError);
    }

    function submitAddressCorrectionMultiship(evt) {
        evt.preventDefault();
        var myForm = $('#addressSelectionForm-multiship');
        sendDataShowLoading(getUrl('addChosenMultishipAddress'), myForm.serialize(), returnToMultishipAddresses, showError);

        jQuery.modal.close();
    }

    function addressMultishipSuggestedEdit(evt){
        evt.preventDefault();
        var myForm = $('#addressSelectionForm-multiship');
        sendDataShowLoading(getUrl('addChosenMultishipAddress'), myForm.serialize(), goToMultishipEditAddresses, showError);

        jQuery.modal.close();
    }

    function updateGiveAll()
    {
        $recipientCount = $('.allocation-names .edit').length;

        if($recipientCount > 0)
        {
            $('.input-give-all').removeAttr("disabled");
        }
        else
        {
            $('.input-give-all').attr("disabled","disabled");
        }

    }

    function toggleStandardFlow(event) {
        window.location = jQuery(event.target).closest('button').attr('data-href');
    }



    $('body').on("focus  change",'#arrivaldate-all',
        function()
        {

            $(".quickship-delivery-method-radio").prop('checked',false);
            $("#quickship_delivery_method_future").prop('checked', true);

        }
    );

    //listen for the calendar load event so we can set the defaults
    $('body').on("calendar-loaded", function(){

        //auto check the best value option.
        $("#quickship_delivery_method_bestvalue").prop('checked', true);
        $("#quickship_delivery_method_bestvalue").change();
    });

    function addressCheckoutSubmitAction(form, successAction){

        if(isUndefined(form) || !(form instanceof jQuery)){
            var form = jQuery(this).closest("form");
        }
        if(addressForm.validator.validate())
        {
            var data = form.serialize();
            makeAjaxRequest(form.attr("action"), data, function(response){

                if((response['status'] == 'success') && (response.pobox != 'Yes'))
                {
                    if(response.invalidFields == 'Yes'){
                        successAction(response);

                    } else {
                        successAction(response);
                        jQuery.modal.close();
                    }

                } else if(response.pobox == 'Yes') {
                    successAction(response);
                }
                else
                {
                    //This may be needed again and should be centralized
                    var $erorrMessages  = '<ul class="messages"><li class="error-msg"><ul>';
                    jQuery.each(response['msg'], function( key, value ) {
                        $erorrMessages  += '<li><span>'+value+'</span></li>';
                    });
                    $erorrMessages  += '</ul></li></ul>';

                    jQuery('#modal-content .page-title').after($erorrMessages);
                }
            });
        }
    }
    //as we set the best value or fastest, update the summary.

    function setAllOption(event)
    {
        var el = $('input[name=quickship_delivery_method]:checked');

        var arrivalBest = el.val();

        if(arrivalBest == "bestvalue" || arrivalBest == "fastest")
        {

            //display the date range.
            var dateRange = $(el).attr("data-date-range");
            var priceRange = $(el).attr("data-price-range");


            $("#applyall").find(".arrival-date").html(dateRange);
            $("#applyall").find(".arrival-method").html(priceRange);

            //unset the future date (for looks)
            $("#arrivaldate-all").val('');
        }
    }

    updateGiveAll();

    $('body').on("change",'.quickship-delivery-method-radio',  setAllOption  );


    $('body').on("calendar-exit", function(){

        setAllOption();
    });

    $('.checkout-shipping').on('mouseenter', '.allocation-names > li', function(event){
        var el = $(this);
        el.data('hover', true);
        setTimeout(function(){
            if(el.data('hover')){
                el.find('.rollover').fadeIn(50);
            }
        },250);
    });

    $('.checkout-shipping').on('mouseleave', '.allocation-names > li', function(event){
        var el = $(this);
        el.data('hover', false);
        el.find('.rollover').fadeOut(50);
    });

	$('.checkout-shipping').on('keyup', '.input-give-all', giveAllUpdate);
	$('.checkout-shipping').on('keyup', '.input-give-one', giveOneUpdate);

	$('.checkout-shipping').on('focus', '.input-qty', checkPage);
    $('.checkout-shipping').on('click', '.input-qty', selectInnerText);

    $('body').on('click', '.multiship-checkout .nextlink', nextPage);
    $('body').on('click', '.multiship-checkout .prevlink', prevPage);

    $('.checkout-shipping').on('click', '.remove-item', removeItem);
    $('.checkout-shipping').on('click', '.add-address', addAddress);


    $('.checkout-shipping').on('click', '.allocation-names > li:not(.add)', editAddress);
    $('body').on('click', '.delivery-address-edit', editAddress);
    $('body').on('submit', '.address-save', function( event ) {
        event.preventDefault();
        addressCheckoutSubmitAction($(this), onNewAddressAdded);
    });
    $('body').on('click', '.remove-address', removeAddress);

    $('.checkout-shipping').on('click', '#multiship-complete-shipping button', tryFinishShipping);

    $('body').on('click', '#tryFinishDelivery', tryFinishDelivery);
    $('body').on('click', '#tryFinishDeliveryWithDefaults', tryFinishDeliveryWithDefaults);

    $('body').on("click",'#btn-apply-all', function() {
        applyToAll();
        $("html, body").delay(0).animate({scrollTop: $('#multiship-delivery-address-list').offset().top - 20 }, 500);
    });

	$('body').on('click', '.button-toggle-standard-flow', toggleStandardFlow);
    $('body').on('stepSelected', '.step-1', onShippingReopened);
    $('body').on('submit', '#addressSelectionForm-multiship', submitAddressCorrectionMultiship);
    $('body').on('click', '#suggestedAddressEdit-multiship', addressMultishipSuggestedEdit);


});

