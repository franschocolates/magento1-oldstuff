








var stripeResponseHandlerSavedCC = function(status, response) {
	  var $form = jQuery('#cc-form');

	  if (response.error) {
	    // Show the errors on the form
		  
		//This may be needed again and should be centralized
			var $erorrMessages  = '<ul class="messages"><li class="error-msg"><ul>';
			$erorrMessages  += '<li><span>'+response.error.message+'</span></li>';
			$erorrMessages  += '</ul></li></ul>';
			
			jQuery('#err-message-wrapper').html($erorrMessages);
			initErrorMessages();
		  
	  } 
	  else {
		  jQuery('#err-message-wrapper').html("");	  
		  
	    // token contains id, last4, and card type
	    var token = response.id;
	    // Insert the token into the form so it gets submitted to the server
	    $form.append(jQuery('<input type="hidden" name="stripeToken" />').val(token));
        // disable any text elements & selects so that we dont' send CC info
        $form.find(':text').prop('disabled', true);
        $form.find('select').prop('disabled', true);
	    // and submit
	    $form.get(0).submit();
	  }
	};

jQuery(function($) {
	  $('#cc-form').submit(function(event) {

		
		    var $form = $(this);
	
		    Stripe.card.createToken($form, stripeResponseHandlerSavedCC);
		     
		    // Prevent the form from submitting with the default action
		    return false;
	  });
	  
	  
	  
	//bind to cc-number forms to update the card type icons.
	 $("#cc-number").bind("keyup change blur", function(){
		 var value = $(this).val();
		 var type = creditCardTypeFromNumber(value);
		 
		 
		 $(".card-types .icon").removeClass("active");
		 $(".card-types").find("." + type).addClass("active");
		 
		 
	 });
	 
	
	 
	 function creditCardTypeFromNumber(num) {
		   // first, sanitize the number by removing all non-digit characters.
		   num = num.replace(/[^\d]/g,'');
		   // now test the number against some regexes to figure out the card type.
		   if (num.match(/^5[1-5]\d{14}$/)) {
		     return 'master';
		   } else if (num.match(/^4\d{15}/) || num.match(/^4\d{12}/)) {
		     return 'visa';
		   } else if (num.match(/^3[47]\d{13}/)) {
		     return 'amex';
		   } else if (num.match(/^6011\d{12}/)) {
		     return 'discover';
		   }
		   return 'UNKNOWN';
	 }
	  
});




