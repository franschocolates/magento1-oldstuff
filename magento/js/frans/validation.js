/* String prototype implementation to fix IE8 / Compatibility Mode. IE
 doesn't natively implement its own String.trim() method, so we will add it here.
 */
if (!String.prototype.trim) {
    (function() {
        // Make sure we trim BOM and NBSP
        var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
        String.prototype.trim = function() {
            return this.replace(rtrim, '');
        };
    })();
}
/* Override default Prototype password validation (see validation.js) */
var PASSWORD_ERROR = 'Your password must be at least 8 characters and contain at least 1 number or symbol.';
var passwordValidationFunction = function (v) {
	var pass = v.strip();
	if (pass.length > 0 && pass.length < 8)
	{
		return false;
	}
	return pass.length == 0 || pass.match(/[1-9\/\,\.<>$%^&*!@#~`*\(\)+-="\'_|]/);
}
Validation.add('validate-password', PASSWORD_ERROR, passwordValidationFunction);
Validation.get('validate-new-password').error = PASSWORD_ERROR;
/* End password overrides */

/* Override default Prototype email validation (see validation.js) */
var EMAIL_ERROR = 'Please enter a valid email address. For example johndoe@domain.com.';

var emailValidationFunction = function (v,elm) {

    v = v.trim();

    elm.setValue(v);
    //$$('validate-email').value(v.trim());
    //return Validation.get('IsEmpty').test(v) || /\w{1,}[@][\w\-]{1,}([.]([\w\-]{1,})){1,3}$/.test(v)
    //return Validation.get('IsEmpty').test(v) || /^[\!\#$%\*/?|\^\{\}`~&\'\+\-=_a-z0-9][\!\#$%\*/?|\^\{\}`~&\'\+\-=_a-z0-9\.]{1,30}[\!\#$%\*/?|\^\{\}`~&\'\+\-=_a-z0-9]@([a-z0-9_-]{1,30}\.){1,5}[a-z]{2,4}$/i.test(v)
    return Validation.get('IsEmpty').test(v) || /^([a-z0-9,!\#\$%&'\*\+\/=\?\^_`\{\|\}~-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z0-9,!\#\$%&'\*\+\/=\?\^_`\{\|\}~-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*@([a-z0-9-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z0-9-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*\.(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]){2,})$/i.test(v)
}
Validation.add('validate-email', EMAIL_ERROR, emailValidationFunction);
/* End email overrides */

var BAD_SHIPMENT_DATES = 'Shipment date must be before arrival date.';
var shipmentDatesFunction = function(v,elem){
    try{
        var tPlanned = v.strip();
        var tArrival = $$('input[id$="preferred_arrival_date"]').first().value;
        if(!tArrival.match(/^(\d{1,2})\/(\d{1,2})\/(\d{2,4})$/)) {
            //Validation.get('validate-shipment-dates').error = "Arrival date is in a incorrect format.";
            return false;
        }
        if(!tPlanned.match(/^(\d{1,2})\/(\d{1,2})\/(\d{2,4})$/)) {
            //Validation.get('validate-shipment-dates').error = "Shipment date is in a incorrect format.";
            return false;
        }
        var plannedShip = new Date(v.strip());
        var arrivalDate = new Date($$('input[id$="preferred_arrival_date"]').first().value);
        if ( Object.prototype.toString.call(plannedShip) === "[object Date]" ){
            if ( isNaN( plannedShip.getTime() ) ){
                //Validation.get('validate-shipment-dates').error = "Arrival date is in a incorrect format.";
                return false;
            }
        }else{
            //Validation.get('validate-shipment-dates').error = "Arrival date is in a incorrect format.";
            return false;
        }

        if ( Object.prototype.toString.call(arrivalDate) === "[object Date]" ){
            if ( isNaN( arrivalDate.getTime() ) ){
                //Validation.get('validate-shipment-dates').error = "Arrival date is in a incorrect format.";
                return false;
            }
        }else{
            //Validation.get('validate-shipment-dates').error = "Shipment date is in a incorrect format.";
            return false;
        }
        //Validation.get('validate-shipment-dates').error = BAD_SHIPMENT_DATES;
        if(plannedShip < arrivalDate){
            return true;
        }
    }catch(ex){
        console.log(ex);
    }
    return false;
}
Validation.add('validate-shipment-dates', BAD_SHIPMENT_DATES, shipmentDatesFunction);

var validateDate = function (v,elem){
    var tArrival = v.strip();
    if(!tArrival.match(/^(\d{1,2})\/(\d{1,2})\/(\d{2,4})$/)) {
        return false;
    }
    return true;
}
Validation.add('validate-date-format','Invalid date format.',validateDate);