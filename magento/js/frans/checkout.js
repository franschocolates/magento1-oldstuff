var giftcardApplied = false;
var deliveryForm = null;
var baseUrl = '';

var modalContainerCss =  {
    position: 'absolute',
    width: '500px'
};

function getUrl(action) {
    return baseUrl + action;
}

function useGuestCheckout(event) {
    event.preventDefault();
    jQuery('#login-section').hide();
	jQuery('#checkout-section').show();
    sendSection('shipping_address');
    jQuery.post('/frans/checkout/useGuestCheckout');
}

function formatCurrency(price){
	price = '$' + price.toFixed(2);
	return price;
}

function showError(xhr, textStatus, errorThrown) {
    // handle session expiration
    if (xhr.status == '403') {
        window.location = '/checkout/cart';
    }
    else {
        jQuery("button[id*='save-order']").attr('disabled', false); // re-enable the button any time this error shows
        alert('Please refresh your cart and try again.');
    }
}

function showPaymentError(xhr, textStatus, errorThrown) {
    if (xhr.status == '403') {
        window.location = '/checkout/cart';
    }
    else {
        errorDisplayHandler('We apologize, but an unknown error occurred saving your data.');
    }
}

function highlightAddressDateErrors(errorIds) {
    var $first = null;
    jQuery.each(errorIds, function(index, val) {
        var $elm = jQuery("input[name='addresses[" + val + "][preferred_arrival_date]']").first();
        $elm.addClass('validation-failed');

        $first = $first || $elm;
    });

    if ($first) {
        $first.focus();
    }
}

var sendDataShowLoading = function (url, data, successFunction, errorFunction) {
    var $ = jQuery;
	showAjaxLoadingIcon();
    $.post(url, data, successFunction, 'json')
        .fail(errorFunction)
        .always(function() { hideAjaxLoadingIcon(); });
};

var sendDataShowLoadingGiftCard = function (url, data, successFunction, errorFunction) {
    var $ = jQuery;
    showGiftCardSyncIcon();
    $.post(url, data, successFunction, 'json')
        .fail(errorFunction)
        .always(function() { hideGiftCardSyncIcon(); });
};

var proceedToDelivery = function (response) {

    if (response.status == 'success')
    {

        //reset part of the form for whenever we go back.
        var $addressIdField = jQuery("#shipping_form").find("input[name=use_suggested]");
        $addressIdField.val("true");



	    if (response.hasOwnProperty('shippingHtml'))
	    {
		    jQuery('.checkout-shipping').html(response.shippingHtml);
	    }
	    else
	    {
		    jQuery('.checkout-delivery').html(response.deliveryHtml);
		    if (response.hasOwnProperty('summaryHtml'))
		    {
			    jQuery('.order-summary').replaceWith(response.summaryHtml);
			    sendSection('delivery_options');
		    }
		    setActiveStep(2);
	    }
    } else if(response.hasOwnProperty('pobox')){

        if (response.hasOwnProperty('shippingHtml'))
        {
            jQuery('.checkout-shipping').html(response.shippingHtml);
        }
    }
    else
    {
        alert('Some data in your shipping info is invalid.');
    }
};

function sendSection(section){

    //Section and title array
    var sectionTitleArray = {
          'shipping_address':           'Shipping Address',
          'shipping_addresses':         'Shipping Addresses', //This may never get used but for now we might need to add it?
          'delivery_options':           'Delivery Options',
          'multiship_delivery_options': 'Multiship Delivery Options',
          'payment_review':             'Payment Review',
          'multiship_payment_review':   'Multiship Payment Review',
          'singleship_success':         'Single Ship Success',
          'multiship_success':          'Multiship Success'
    };

    try{ga('send', 'pageview', {'page': '/frans/checkout/'+section,'title': sectionTitleArray[section]});}catch(e){
        // console.log(e); // removed for IE
    }

}

var setActiveStep = function(stepNumber) {
	// determine which toggler corresponds to the step number, and toggle it
	var toggler = jQuery('.expander-toggle[data-expander-sequence=' + stepNumber + ']');
	toggleExpander(toggler);
};

var proceedToPayment = function (response) {
    if (response.status == 'success')
    {
	    // fix for IE8 bug: response string is too long for $.html() so use innerHTML then evaluate <script> tags within instead
	    var paymentStep = jQuery('.checkout-payment');
	    paymentStep[0].innerHTML = response.paymentHtml;
	    paymentStep.find('script').each(function(){
		    jQuery.globalEval(jQuery(this).html());
	    });

	    sendSection('payment_review');
        if (response.hasOwnProperty('summaryHtml'))
        {
            jQuery('.order-summary').replaceWith(response.summaryHtml);
        }
	    setActiveStep(3);
    }
    else
    {
        ///alert('Some data in your shipping info is invalid.'); //Mac wasn't sure if we want an alert.
        if (response.hasOwnProperty('address_ids'))
        {
            highlightAddressDateErrors(response.address_ids);
        }
    }
};

var setGiftcardPaymentDetails = function(coversBalance) {
    giftcardApplied = true;

    var paymentMethod = (coversBalance) ? 'free' : 'stripe';
    setPaymentMethod(paymentMethod);

    if (coversBalance) {
        jQuery('#cc-save-form, .payment-header').fadeOut(250);
	    jQuery('.giftcard-header').addClass('no-padding');
    }
};

var setPaymentMethod = function(method) {
    var $form = jQuery('#payment-step');
    var $paymentMethod = $form.find("input[name='payment[method]']");
    $paymentMethod.val(method);
};

var setGiftcardRemoved = function() {
    giftcardApplied = false;
    setPaymentMethod('stripe');
	jQuery('#cc-save-form, .payment-header').fadeIn(250);
	jQuery('.giftcard-header').removeClass('no-padding');
};

var errorDisplayHandler = function(message) {
    // reenable CC if needed
    // make sure CC should be enabled

    jQuery("button[id*='save-order']").attr('disabled', false);
    if (!jQuery('.tab-cc').parent().hasClass('disabled'))
    {
        toggleCcFields(false);
    }
	var errorContainer = jQuery('#payment-error-message');
	message = Translator.translate('We were unable to validate your payment:') + '<br />' + Translator.translate(message);
	if(errorContainer.length){
		errorContainer.html(message);
		errorContainer.show();
	} else {
	    alert(message);
	}
};

var paymentCheckHandler = function(status, response, $form, callback) {
    if (response.error) {
        // Show the errors on the form
        errorDisplayHandler(response.error.message);
    }
    else {
        jQuery('#err-message-wrapper').html("");

        // token contains id, last4, and card type
        var token = response.id;
        // Set the token into the form so it gets submitted to the server
        // note that we don't insert because then we'd end up with dupes
        $form.find("input[name='payment[stripe_token]']").val(token);
        // and submit
        callback();
    }
};

var checkPaymentDetails = function(callback, giftcardApplyCallback) {
    var $form = jQuery('#payment-step');

    // make sure they have applied the giftcard
    //var $giftcardNumber = jQuery('#giftcard_number');
    //if ($giftcardNumber.val() && !giftcardApplied)
    //{
    //    giftcardApplyCallback(null);
    //    return;
    //}

    var paymentMethod = $form.find("input[name='payment[method]']").val();
    if (paymentMethod == 'stripe')
    {
        var $existingCard = $form.find("input[name='payment[use_saved_card]']:checked");
        var $useExisting = ($existingCard.length > 0) ? $existingCard.val() == '1' : false;

        if (giftcardApplied && !$useExisting && jQuery('#cc-number').val() == '')
        {
            // locate the message from the giftcard
            var messageText = jQuery('#giftcard-balance-message').text();
            errorDisplayHandler(messageText);
            jQuery('.tab-cc').trigger('click');
            return false;
        }

        if (!$useExisting)
        {
            // populate the hidden fields with the name, region
            $form.find("#region-name").val(
                $form.find("select[name='billing[region]'] option:selected").text()
            );
            $form.find("#billing-full-name").val(
                $form.find("input[name='billing[firstname]']").val() + ' ' + $form.find("input[name='billing[lastname]']").val()
            );
            $form.find("#cc-number").val($form.find("#cc-number").val().strip());

            var handler = function(status, response) {
                paymentCheckHandler(status, response, $form, callback);
            };
            Stripe.card.createToken($form, handler);
        }
        else {
            callback();
        }
    }
    else {
        callback();
    }

    // can just do callback
    return true;
};

function summaryEdit(event) {
	event.preventDefault();
	setActiveStep(jQuery(event.target).attr('data-destination-step'));
}

function expandMsg(event) {
	var parent = jQuery(event.target).parent();
	parent.siblings().removeClass('hide');
	parent.hide();
}

function toggleUseShippingAddress(event) {
    var checkbox = jQuery(event.target);
    // only autofill when the box is checked; do nothing when it's unchecked
    if(checkbox.is(":checked")) {
        // retrieve the shipping address data from the data-autofill attribute on the checkbox
        var autofillData = jQuery.parseJSON(checkbox.attr('data-autofill'));
        // set an index for keeping track of street fields
        var streetIndex = 1;
        // loop through all text inputs and selects in the payment form
        jQuery('.checkout-step').find('input[type=text], select').each(function(){
            var thisName = jQuery(this).attr('name');
            if(typeof thisName === "undefined") { return false; } // continue the loop if no name was found
            // check if there's a key matching this field name in the autofill object
            if(thisName in autofillData){
                // replace the field value with the data from the autofill object
                jQuery(this).val(autofillData[thisName]);
                if(thisName == 'country_id'){
                    paymentUpdater.update();
                }
                if(jQuery(this).val().length){
                    jQuery(this).parent().children('.placeholder').hide();
                }
            } else if(thisName.indexOf('billing[street][]') != -1) {
                // street fields are a special case, get the value matching the current index
                jQuery(this).val(autofillData['street'+streetIndex]);
                streetIndex++;
                if(jQuery(this).val().length){
                    jQuery(this).parent().children('.placeholder').hide();
                }
            }
        });
    }
}
function trySaveOrder(evt) {
    if (window.event) {
        window.event.returnValue = false;
    }
    evt.preventDefault();
	jQuery('#payment-error-message').hide();
    if (paymentForm.validator.validate())
    {
        var submitButton = jQuery("button[id*='save-order']");
        if(!submitButton.is(":disabled")){
            submitButton.attr('disabled', 'disabled');
        var successCallback = function() {

            //Commenting out as IE 8 and 9 are not console compatible
            //console.log("Trying to save order...");
            toggleCcFields(true);

            sendDataShowLoading(getUrl('saveOrder'),
                jQuery('#payment-step').serialize(),
                proceedToConfirmation,
                showPaymentError
            );

        };

        // from checkout.js
        checkPaymentDetails(successCallback, tryApplyGiftcard);
    }
        return true;
    }
}

function toggleCcFields(disabled) {
    jQuery('#cc-number').prop('disabled', disabled);
    jQuery('#cc-cvn').prop('disabled', disabled);
    jQuery('#cc-exp-month').prop('disabled', disabled);
    jQuery('#cc-exp-year').prop('disabled', disabled);
}

function onGiftcardApplied(response) {
    if (response.status == 'success')
    {
        updateGiftcardAndSummary(response);
    }
    else
    {
        showGiftCardError(response);
    }
}

function onGiftcardRemoved(response) {
    if (response.status == 'success')
    {
        updateGiftcardAndSummary(response);
        setGiftcardRemoved();

    }
    else
    {
        alert('An error occurred removing your gift card.');
    }
}

function updateGiftcardAndSummary(response) {
    jQuery('#giftcard-wrapper').html(response.giftcardHtml);
    jQuery('#checkout-sidebar').html(response.summaryHtml);
}


function tryApplyGiftcard(e) {
    if (e != null)
    {
        e.preventDefault();
    }

    if (!jQuery('#giftcard_number').val()) {
        showGiftCardErrorMessage('Please enter a Gift Card number.');
        return false;
    }

    clearGiftCardError();

    sendDataShowLoading(getUrl('applyGiftcard'),
        { card_number: jQuery('#giftcard_number').val() }
        , onGiftcardApplied, showError);
}

function tryApplyGiftcardTypeWatch() {

    clearGiftCardError();

    sendDataShowLoadingGiftCard(getUrl('applyGiftcard'),
        { card_number: jQuery('#giftcard_number').val() }
        , onGiftcardApplied, showError);
}

function tryRemoveGiftcard(e) {
if(e){
    e.preventDefault();
}
    jQuery('#giftcard-validation-check').fadeOut(150);
    var urlRemoveGC = getUrl('removeGiftcard');
    sendDataShowLoadingGiftCard(urlRemoveGC, { }, onGiftcardRemoved, showError);
}

function clearGiftCardError() {
    jQuery('#giftcard_number').siblings('.validation-advice').remove();
    jQuery('#giftcard_number').removeClass('validation-failed');
}

function showGiftCardError(response) {
    var message = (response.hasOwnProperty('message')) ? response.message : 'An unknown error occurred applying the gift card.';
    showGiftCardErrorMessage(message, response);
}

function showGiftCardErrorMessage(message, response) {
    // remove any previous error fields
    clearGiftCardError();
    //if(response.incorrectGiftCardID == 'Yes'){
    //    alert('weeeee');
    //    //jQuery('#giftcard_number').addClass('validation-failed');
    //} else {
    //    jQuery('#giftcard_number').addClass('validation-failed');
    //}

    jQuery('#giftcard_number').addClass('validation-failed');
    var advice = Validation.createAdvice('gc_number', $('giftcard_number'), false, message).show();
    advice.show();
}

function proceedToConfirmation(response) {
    if (response.status == 'success')
    {
        window.location = '/frans/checkout/success';
    }
    else
    {
        errorDisplayHandler(response.message);
    }
}


function validateArrivalDate(dateField) {
    $elm = jQuery(dateField);

    var checkDate = $elm.val();

    if (checkDate == '') {
        return true;
    }

    var $parentCalendar = $elm.closest('.future-ship-date');
    var selector = '.' + $parentCalendar.attr('id') + " li[data-numeric-date='" + checkDate + "']";
    var $calendarItem = jQuery('#future-ship-calendar').find(selector);
    if ($calendarItem.length == 0 || !$calendarItem.hasClass('available'))
    {
        $elm.addClass('validation-failed');
        return false;
    }
    else
    {
        $elm.removeClass('validation-failed');
        return true;
    }

}

function showShippingPopup(event, element) {
	event.preventDefault();
	showModal(jQuery('#shipping-popup').html(), { containerCss: modalContainerCss });
}

function toggleUseInvoice(event){
	var checkbox = jQuery(event.target);
    //event.target
	var wrapper = jQuery("#payment-wrapper");
	if(checkbox.is(":checked")) {
		if(giftcardApplied){
			tryRemoveGiftcard();
			jQuery('#invoice-check').prop('checked', true);
		}
		setPaymentMethod('paybyinvoice');
		wrapper.slideUp(250);
		wrapper.find(":input:not(:hidden)").each(function(index, element){
			jQuery(element).prop("disabled", true);
		});

	} else {
		wrapper.slideDown(250);
		wrapper.find(":input:not(:hidden)").each(function(index, element){
			jQuery(element).prop("disabled", false);
		});
	}
}


jQuery(document).ready(function($){

	var body = $('body');
    jQuery.modal.close();
    body.on('blur', '.validate-date', validateArrivalDate);
	body.on('click', '.about-shipping', showShippingPopup);

    body.on('click', '.giftcard-remove', tryRemoveGiftcard);

    body.on('click', '#multiship-save-order', trySaveOrder);
    body.on('click', '#save-order', trySaveOrder);

	body.on('click', '.order-summary .edit-link', summaryEdit);
	body.on('click', '.truncated-msg-control > a', expandMsg);
	body.on('change', '#toggle-use-shipping-address', toggleUseShippingAddress);

	body.on('change', '#invoice-check', toggleUseInvoice);

    //bind to cc-number forms to update the card type icons.
    body.on("keyup change blur", '#cc-number', function(e){
        var value = $(e.target).val();
        var type = creditCardTypeFromNumber(value);
        $(".card-types .icon").removeClass("active");
        $(".card-types").find("." + type).addClass("active");
    });

    function creditCardTypeFromNumber(num) {
        // first, sanitize the number by removing all non-digit characters.
        num = num.replace(/[^\d]/g,'');
        // now test the number against some regexes to figure out the card type.
        if (num.match(/^5[1-5]\d{14}$/)) {
            return 'mastercard';
        } else if (num.match(/^4\d{15}/) || num.match(/^4\d{12}/)) {
            return 'visa';
        } else if (num.match(/^3[47]\d{13}/)) {
            return 'amex';
        } else if (num.match(/^6011\d{12}/)) {
            return 'discover';
        }
        return 'UNKNOWN';
    }

	function onCustomerAddressSelected(element) {

		var selectedOption = $(this).find(':selected, :checked').first();
		var form = $(this).closest('form');



		// for populating a form other than the one that observed the event
		if(form.is('[data-associated-form-id]')){
			form = $('#'+form.attr('data-associated-form-id'));
		}

		if (selectedOption.val() != 0) {
            // figure out our field names from the format
            var format = form.find("input[name='field_name_format']").val();
            var allFields = form.find('input[type=text], select');
			$.each(selectedOption.data('fields'), function(key, value) {
				var nameSelector = "[name='" + format.replace('%s', key) + "']";
				$("input" + nameSelector).val(value);
				// trigger the change event so the setUpSelects() callback will be executed
				$("select" + nameSelector).val(value).trigger('change');
				if(key == 'country_id'){
					addressUpdater.update();
				}
                if ((key == 'entity_id'))
                {
                    var $addressIdField = form.find("input[name=original_customer_address_id]");
                    $addressIdField.val(value);
                }
                if ((key == 'use_suggested'))
                {
                    var $addressIdField = form.find("input[name=use_suggested]");
                    $addressIdField.val(value);
                }
				// street will be combined.
				if (key == 'street')
				{
                    // need a street selector since the name fields might have a prefix
                    var $streetFields = form.find(".street-entry");
					var streetValues = value.split('\n');
                    $streetFields.first().val(streetValues[0])

                    if ($streetFields.length > 1) {
                        street2 = (streetValues.length > 1) ? streetValues[1] : '';
                        $streetFields.eq(1).val(street2);
                    }
				}
			});
		}
		else
		{
			// clear it out
            form.find("input[type='text']").each(function () {
				$(this).val('');
			});
			// trigger the change events after clearing so the setUpSelects() callback will be executed
            if ($('#country option:not(:disabled)').size() > 1){
                form.find("#country").val('').trigger('change');
            }
			form.find("#region_id").val('').trigger('change');
			form.find("#region").val('');
			addressUpdater.update();
		}

		// reset any placeholder shims
		if(typeof jQuery.placeholder === 'object')
		{
			jQuery.placeholder.reset();
		}

	}

    function trySaveShipping(evt, form) {
        evt.preventDefault();
	    jQuery('#addressValidationMessage').hide();





        // TODO: stop hardcoding the form ID here.
        if (addressForm.validator.validate())
        {
            sendDataShowLoading(getUrl('saveShipping'), addressForm.form.serialize(), proceedToDelivery, showError);



        }
    }

	function submitAddressCorrectionSingleship(evt) {
		evt.preventDefault();
		var form = $('#'+$(this).attr('data-associated-form-id'));
		form.submit();
		jQuery.modal.close();
	}

    function trySaveSingleDelivery(evt) {
        evt.preventDefault();

        if (deliveryForm.validator.validate())
        {
            sendDataShowLoading(getUrl('saveDelivery'), deliveryForm.form.serialize(), proceedToPayment, showError);
        }
    }

	body.on('change', '.select-customer-address', onCustomerAddressSelected);

	body.on('submit', '#shipping_form', trySaveShipping);
	body.on('click', '#suggestedAddressEdit-singleship', jQuery.modal.close);
	body.on('submit', '#addressSelectionForm-singleship, #addressSelectionForm-multiship', submitAddressCorrectionSingleship);
	body.on('change', '#addressSelectionForm-singleship', onCustomerAddressSelected);

    body.on('click', '#single-delivery-form .complete-delivery', trySaveSingleDelivery);

    $('#use-guest-checkout').click(useGuestCheckout);

});

(function($) {
    $.widget( "frans.calendar", {
    options : {
        url:					'',
        defaultArrivalDate: 	'',
defaultArrivalMethod:	'',
        defaultTitle: 			'Preferred Arrival Date',
        defaultMessage: 		'',
        defaultIcon:            ''
    },
    vars: {
        // persistent reference to the main calendar in the DOM
        cal: null,
        // persistent collection of instances
        instances: [],
        // the current instance being used
        activeInstance: null,
        // the current page visible on the calendar
        currentPage: 1,
        // the number of completed Ajax requests
        completedAjaxRequests: 0,
        // month names for quick reference later
        months: ["January","February","March","April","May","June","July","August","September","October","November","December"]
    },
    // constructor, run once for each instance
    _create: function() {
        // we may have reloaded the page, in which case our previous object is no longer valid
        var reset = !$('#future-ship-calendar').is(this.vars.cal);

        if (reset) {
            this.vars.cal = $('#future-ship-calendar');
            this.vars.instances.clear();
            this.vars.activeInstance = null;
            this.vars.currentPage = 1;
            this.vars.completedAjaxRequests = 0;
        }

        var addressId = this.element.attr('data-address-id');
        var data = (typeof addressId !== 'undefined' && addressId !== false) ?
        { 'address_id': addressId } : { };

        // quick way to reference the widget in the event handler context, where "this" refers to the triggering element
        var calendar = this;
        // catalog this element for easy access later
        this.vars.instances.push(this.element);
        // load the pages for this instance's calendar
        this._loadCalendarInstance(this.element, data);
        // bind some events
        this.element.bind('mouseenter', function(){
            calendar.show($(this));
        });
        this.element.bind('mouseleave', function(){
            calendar.hide($(this));
        });
        this.element.find('input[type="text"]').bind('focus', function(){
            calendar.show($(this).parent());
        });

        this.element.find('input[type="text"]').bind('keydown', function (e) {
            if (e.keyCode === 9)
            {
                calendar.hide($(this).parent());
                //e.preventDefault();
            }
        });

        this.element.find('input[type="text"]').bind('change', function(){
            var instance = $(this).parent();
            calendar.validateFormat(instance);
            calendar.selectDayByText(instance);
        });
        this.element.find('.icon').bind('click', function(){
            var textField = $(this).siblings('input[type="text"]');
            textField.click(); // TODO: Fix bug causing calender to hide/reappear when icon is clicked
        });

	    // event fired whenever a new date is chosen for an individual shipment
	    this.element.bind('selectMethod', function(event, data){
		    // the price for shipping for the current selection
		    var shipPriceSelected = data.shipPriceSelected;
		    // if this shipment has a total section, update it with the shipping price
		    var shipmentWrapper = $(this).parents('.shipment-wrapper');
		    var subtotal = shipmentWrapper.find('.subtotal .amount');
		    var shippingTotal = shipmentWrapper.find('.shipping-total .amount');
		    var shipmentTotal = shipmentWrapper.find('.shipment-total .amount');
		    if((subtotal.length == 1) && (shippingTotal.length == 1) && (shipmentTotal.length == 1)){
			    // get the subtotal for this shipment
			    subtotal = parseFloat(subtotal.html().substring(1));
			    // update the shipping total
			    shippingTotal.html(formatCurrency(shipPriceSelected));
			    // update the shipment total with the sum of the subtotal and shipping total
			    shipmentTotal.html(formatCurrency(subtotal + shipPriceSelected));
		    }
	    });

        // is this the first instantiation?
        if(this.vars.instances.length == 1){
            // initialize the calendar events (only do this once as all calendars are loaded into one viewer)
            this.vars.cal.on('click mouseenter', '.day-picker li', function(e){

                if(e.type == "click" && $(this).hasClass("unavailable") == false ){
                    calendar.selectDay($(this), true);
                }
                else
                {
                    calendar.selectDay($(this), false);
                }
            });
            this.vars.cal.on('click', '.nextmonth', function(){
                calendar.nextMonth();
            });
            this.vars.cal.on('click', '.prevmonth', function(){
                calendar.prevMonth();
            });
        }
    },
    _countPages: function() {
        // quick lookup of the number of pages in the active calendar
        return this.vars.cal.find('.all-pages .active .calendar-page').length;
    },
    _loadCalendarInstance: function(instance, data) {
        // assign a unique ID to the instance, to bind
        instance.uniqueId();
        // create a wrapper element for the calendar associated with this instance
        var instanceWrapper = $('<div class="' + instance.attr('id') + ' preload">').appendTo(this.vars.cal.find('.all-pages'));
        // quick way to reference the widget in the event handler context, where "this" refers to the triggering element
        var calendar = this;
        $.ajax({
            type: 'POST',
            dataType: 'json',
            data: data,
            url: this.options.url,
            success: function(response) {
                // insert the response HTML into the DOM
                instanceWrapper.append(response.data.html);
                // store the relevant messages
                instance.data('messages', response.data.messages);
                instance.data('default_message', response.data.messages['default']);
                // note that the request is finished to track loading progress
                calendar._finishAjaxRequest();
            }
        });
        // fill the summary area with defaults
        this.updateSummary(instance);
    },
    _finishAjaxRequest: function(instance) {
        // call this when any Ajax request is completed to increment the global count
        this.vars.completedAjaxRequests++;
        // if all requests are done, ready the calendar
        if(this.vars.completedAjaxRequests == this.vars.instances.length) {
            // store reference to container element for quick access
            var container = this.vars.cal.find('.all-pages');
            // store reference to the collection of all calendar-page elements for quick access
            var allPages = container.find('.calendar-page');
            // get the desired width for the visible calendar
            var calWidth = this.vars.cal.width() + 2;
            allPages.each(function() {
                $(this).width(calWidth);
            });
            // set the width of the container (as wide as all pages combined)
            container.width(calWidth * allPages.length);
            // before calendars are loaded, the calendar shows a loader GIF; remove that
            this.vars.cal.css('background-image', 'none');
            // remove the preload class from calendars after calculations are done, to allow them to be displayed
            container.find('.preload').each(function(){
                $(this).removeClass('preload');
            });
            // unhide the pager buttons
            this.vars.cal.find('.nextmonth').css('visibility', 'visible');
            this.vars.cal.find('.prevmonth').css('visibility', 'visible');


            //fill in the prefilled date if there is one
            for(var i = 0; i< this.vars.instances.length; i++)
            {
                this.selectDayByText( this.vars.instances[i] );
            }

            $( "body" ).trigger( "calendar-loaded" );

        }
    },
    _updatePagers: function() {
        // set references to the next and previous button elements
        var nextbutton = this.vars.cal.find('.nextmonth');
        var prevbutton = this.vars.cal.find('.prevmonth');
        // if first page is visible, disable the previous button
        prevbutton.toggleClass('disabled', (this.vars.currentPage <= 1));
        // if last page is visible, disable the next button
        nextbutton.toggleClass('disabled', (this.vars.currentPage >= this._countPages()));
    },
    findInstanceById: function(id)
    {
        for(var i = 0; i < this.vars.instances.length; i++)
        {
            var instance = this.vars.instances[i];

            if(instance.attr("id") == id)
            {
                return instance;
            }

        }

        return null;
    },
    setDay: function(instance, day)
    {
        var bestDate = day.attr("data-numeric-date");

        instance.find('input[type="text"]').val(bestDate);

        this.selectDay(day, false, instance);

        instance.find('.shipping-method').val(day.data('method'));

    },
    getDayByBestType: function(instance, bestType)
    {
        var instanceId = instance.attr("id");

        var bestDate = null;
        var bestDateDay = null;
        if(bestType == "bestvalue")
        {
            bestDateDay = $("." + instanceId + " .day-picker .cheapest").first();
        }
        else if(bestType == "fastest")
        {
            bestDateDay =  $("." + instanceId + " .day-picker .fastest").first();
        }



        return bestDateDay;

    },
    show: function(instance) {
        // note the current active instance
        this.vars.activeInstance = instance;
        // if the placeholder shim is enabled, hide placeholders while the calendar is open
        $('.placeholder').each(function(){
            $(this).toggleClass('hide', true);
        });
        // attach the calendar to the instance element
        this.vars.cal.appendTo(instance);
        // show the correct calendar for this instance, and hide all others
        this.vars.cal.find('.all-pages > div').each(function(){
            $(this).toggleClass('active', $(this).hasClass(instance.attr('id')));
        });
        // find the correct page to display, then jump to it
        var inputValue = instance.find('input[type="text"]').val();
        this.goToMonth(this.findMonth(inputValue));
        // reveal the hidden calendar
        this.vars.cal.fadeIn(100);
        // flag the instance as having a calendar visible, for CSS transformation
        instance.addClass('calendar-visible');
    },
    hide: function(instance, duration) {
        // check if duration arg is defined and default to 300ms if not
        if(typeof duration === 'undefined') {
            var duration = 300;
        }
        // hide the calendar
        var parentThis = this;

        //this.vars.cal.hide(duration, function(){
            //set / reset the summary to the selected date
            // update the summary text
            this.vars.cal.hide();

            var day = parentThis.selectDayByText(  instance   );

            if(day)
            {
                parentThis.updateSummary(parentThis.vars.activeInstance, day );
            }

            $( "body" ).trigger( "calendar-exit" );

        // restore regular styles to instance
        instance.removeClass('calendar-visible');
        // if the placeholder shim is enabled, unhide placeholders
        $('.placeholder').each(function(){
            $(this).toggleClass('hide', false);
        });

    },
    selectDay: function(day, close, instance) {

        // update the text input with the user selection
        var numericDateSelected = day.attr('data-numeric-date');

        if(!instance)
        {
            instance = this.vars.activeInstance;
        }

        if(close == true) //clicked.
        {
            instance.find('input[type="text"]').val(numericDateSelected);
            instance.find('input[type="text"]').removeClass('validation-failed');

            instance.find('input[type="text"]').change();

            this.highlightDay(day);

            // update the summary text
            this.updateSummary(instance, day);

            // store the selected method
            instance.find('.shipping-method').val(day.data('method'));

            this.hide(instance);
        }
        else //hover only.
        {
            // update the summary text
            this.updateSummary(instance, day);
        }

    },
    /* marks the day as selected in each calendar pane in CSS */
    highlightDay: function(day) {
        var numericDateSelected = day.attr('data-numeric-date');
        // clear previously selected dates and select this one
        // note there can be multiple because of days at end/beginning of month
        var calendarParent = day.closest('.calendar-page').parent();
        calendarParent.find('li.selected').removeClass('selected');
        // select any with the same day
        calendarParent.find("li[data-numeric-date='" + numericDateSelected + "']").addClass('selected');
    },
    hoverDay: function(day) {
        // update the summary text. will change based on if we are available
        var instance = this.vars.activeInstance;
        var messages = instance.data('messages');
        var message;

        if (day.hasClass('unavailable'))
        {
            var messageId = $(this).attr('data-message-id');
            if (typeof messageId !== 'undefined' && messageId != false) {
                message = messages[messageId];

            }
            else {
                message = messages['default'];
            }

            //calendar.showMessage(messageId);
            this.updateSummary(instance, message, message);
        }
        else
        {

            // it's available, use the default message
            message = messages['default'];
            var dayValue = day.data('text-date');
            this.updateSummary(instance, dayValue, message);
        }
    },
    hoverExit: function(day) {
        var instance = this.vars.activeInstance;
        var messages = instance.data('messages');
        var message = messages['default'];

        this.updateSummary(instance);

    },
    selectDayByText: function(instance) {

        // grab the date from the text field (should be formatted as mm/dd/yyyy)
        var inputDate = instance.find('input[type="text"]').val();

        // find the day element that corresponds to the user's input
        var day = this.findDay(inputDate, instance);

        // findDay() returns false if no day is found, so confirm that one was
        if(day){
            // select the day
            this.selectDay(day, false, instance);
            // highlight it, too
            this.highlightDay(day);
            // we can't tell it we closed on selectDay, since that causes a recursion issue
            // so just set the method to match whatever was in that day
            var method = day.data().hasOwnProperty('method') ? day.data('method') : '';
            instance.find('.shipping-method').val(method);

        } else {
            this.updateSummary(instance, 'Please select a valid date.');
            instance.find('.shipping-method').val('');
            // TODO: additional error handling for valid dates that are not available as shipping choices
        }
    },
    updateSummary: function(instance, dayOrMsg) {
        var arrivalSummary = instance.closest('.wrapper').find('.arrival-summary');

        //setup defaults.
        var textDateSelected = this.options.defaultArrivalDate;
        var shipLabelSelected = this.options.defaultArrivalMethod;
        var textTitle = this.options.defaultTitle;
        var textMessage = this.options.defaultMessage;
        var messageIcon = this.options.defaultIcon;

        if( typeof dayOrMsg == "string" )
        {
            textDateSelected = dayOrMsg;
        }
        else
        {
            //if we have a date assigned.
            if(instance.data('messages') != undefined)
            {

                var messages = instance.data('messages');

                //if there is an available
                textDateSelected = dayOrMsg.attr('data-text-date');

                // figure out shipping price and label from key
                if (!dayOrMsg.hasClass('unavailable'))
                {
                    var methodLabel = $('#future-ship-calendar').find('.' + instance.attr("id") + ' .shipmethod-key li.shipmethod_' + dayOrMsg.attr('data-method')).first().children('.shipmethod-label');
                    shipLabelSelected = 'via ' + methodLabel.text();
	                shipPriceSelected = parseFloat(methodLabel.attr('data-raw-price'));
	                // trigger the custom selectMethod event, only if this isn't an "apply to all" instance
	                if(instance.find('#arrivaldate-all').length == 0){
		                instance.trigger('selectMethod', {
			                shipPriceSelected: shipPriceSelected
		                });
	                }
                }

	            var messageId = dayOrMsg.attr('data-message-id');
	            if(typeof messageId !== "undefined"){
	                textTitle = messages[messageId]["title"];
	                textMessage = messages[messageId]["text"];
	                if(messages[messageId]["image_url"].length > 0  )
	                {
	                    messageIcon = messages[messageId]["image_url"];
	                }

	            }

            }
        }

	    // update the arrival summary with user selection data, or defaults as decided above
	    arrivalSummary.find('.arrival-title').html(textTitle);
	    arrivalSummary.find('.arrival-message').html(textMessage);
	    arrivalSummary.find('.arrival-date').html(textDateSelected);
	    arrivalSummary.find('.arrival-method').html( shipLabelSelected ? shipLabelSelected : '');
	    arrivalSummary.find('.arrival-icon').attr("style", "background-image: url('" + messageIcon + "');");

    },
    nextMonth: function() {
        // make sure we can go to a next page (i.e., we're not on the last one)
        if(this.vars.currentPage < this._countPages()){
            // increment the current page index
            this.vars.currentPage++;
            // enable/disable pager buttons as needed
            this._updatePagers();
            // animate the transition
            this.vars.cal.find('.all-pages').animate({
                marginLeft: '-=' + (this.vars.cal.width() + 2) + 'px'
            }, {
	            duration: 600,
	            easing: 'easeOutQuint'
            });
        }
    },
    prevMonth: function() {
        // make sure we can go to a next page (i.e., we're not on the first one)
        if(this.vars.currentPage > 1){
            // decrement the current page index
            this.vars.currentPage--;
            // enable/disable pager buttons as needed
            this._updatePagers();
            // animate the transition
            this.vars.cal.find('.all-pages').animate({
                marginLeft: '+=' + (this.vars.cal.width() + 2) + 'px'
            }, {
	            duration: 600,
	            easing: 'easeOutQuint'
            });
        }
    },
    findDay: function(dateString, instance) {
        var dayElement = false;

        // make sure the string isn't empty
        if(dateString != ''){

            // loop through the pages (we're just focusing on the month labels of each one)
            var els =  this.vars.cal.find('.active .calendar-page .day-picker li');

            if(instance)
            {
                els = $("#future-ship-calendar").find("." + instance.attr("id") + " .calendar-page .day-picker li");
            }

            els.each(function(index, element){
                day = $(element);
                // check for a matching date attached to the element
                if(day.attr('data-numeric-date') == dateString){

                    // we found the day, but only return it if it's a valid choice
                    if(!day.hasClass('unavailable')){
                        dayElement = day;
                    }
                    // break out of the .each() loop
                    return false;
                }
            });
        }
        return dayElement;
    },
    findMonth: function(dateString) {
        // set the number for the page (default to 1 in case none is found below)
        var pagenum = 1;
        // make sure the string isn't empty
        if(dateString != ''){
            // extract month and year from mm/dd/yyyy format
            var date = new Date(dateString);
            var month = this.vars.months[date.getMonth()];
            var year = date.getFullYear();
            // loop through the pages (we're just focusing on the month labels of each one)
            this.vars.cal.find('.active .calendar-page .month').each(function(index, element){
                page = $(element);
                // check for a matching month label
                if(page.find('.month-label').html() == month){
                    // check for a matching year label
                    if(page.find('.year-label').html() == year){
                        // match is found, so update the page number
                        pagenum = index + 1;
                        // break out of the .each() loop
                        return false;
                    }
                }
            });
        }
        return pagenum;
    },
    goToMonth: function(pagenum) {
        // first check that we're not already on this page
        if(this.vars.currentPage != pagenum){
            // update the current page index
            this.vars.currentPage = pagenum;
            // enable/disable pager buttons as needed
            this._updatePagers();
            // switch to the page in the calendar
            var newMargin = this.vars.cal.width() * -(pagenum - 1);
            this.vars.cal.find('.all-pages').css('margin-left', newMargin);
        }
    },
    validateFormat: function(instance) {
        var input = instance.find('input[type="text"]');
        var dateString = input.val();
        // check if JS can parse the string as a valid date
        if(isNaN(Date.parse(dateString))){
            this.updateSummary(instance, 'Please select a valid date.');
            // TODO: additional error handling for invalid dates
        } else {
            // convert the string to a date object and use it to build a string in mm/dd/yyyy format
            var date = new Date(dateString);
            var month = '' + (date.getMonth() + 1);
            var day = '' + date.getDate();
            var year = date.getFullYear();
            if(month.length < 2) month = '0' + month;
            if(day.length < 2) day = '0' + day;
            var formattedDate = month + '/' + day + '/' + year;
            // replace the text field value with the correctly formatted string
            input.val(formattedDate);
        }
    }
});
})(jQuery);
