/* Functionality for configurable product view */

var currentProductId;

jQuery(document).ready(function($){

	var attributes = JSON.parse($('#attribute-data').html());
	var galleries = JSON.parse($('#gallery-data').html());
	var additional = JSON.parse($('#additional-data').html());
	var availabilityStatuses = JSON.parse($('#availability-data').html());

	var wrapper = $('#product_addtocart_form');

	// prepare the initial view and form action URL
	updateOptionDisplay();
	$('#product-name-label').before($('#color-tile-label')); // swap label placement
	updateCurrentProductId();

	wrapper.on('change', 'select[name="piece_count"]', function(){
		// update available options
		updateOptionDisplay();
	});

	wrapper.on('change', 'input[type="radio"][name="color_tile"]', function(){
		// cross-browser compatible method of styling the color tile radio button borders when checked
		$('.color-tiles label').removeClass('checked');
		$(this).parent().addClass('checked');
		// change currently selected product
		updateCurrentProductId();
	});

	function updateCurrentProductId(){
		// note the product ID
		currentProductId = getProductIdFromSelection();
		// show the correct availability status
		updateAvailabilityStatus();
		// change the URL the form posts to
		updateFormAction();
		// change the color tile label text
		updateColorTileLabel();
		// show the correct media gallery
		updateGallery();
		// show the correct attribute accordion
		updateAccordion();
	}

	function updateColorTileLabel(){
		var colorTileName = $('input[type="radio"][name="color_tile"]:checked').parent().attr('title');
		$('#color-tile-label').html(colorTileName);
	}

	function getProductIdFromSelection(){

		var pieceCount = getSelectedPieceCount();
		var colorTile = getSelectedColorTile();
		var pieceCountProducts = getPieceCountProducts(pieceCount);
		var colorTileProducts = getColorTileProducts(colorTile);
		var selectedProductId = currentProductId;

		if(pieceCount && colorTile){
			// loop through all piece count products, checking for a product ID also in color tile products
			for(var pieceCountProductId in pieceCountProducts){
				if(typeof colorTileProducts[pieceCountProductId] === 'object'){

					// intersection found, we'll return the productId
					selectedProductId = pieceCountProductId;
					break;

				}
			}
		} else if(pieceCount){
			// this product has piece counts but no color tile options
			selectedProductId = Object.keys(pieceCountProducts)[0];
		} else if(colorTile){
			// this product has color tiles but no piece count options
			selectedProductId = Object.keys(colorTileProducts)[0];
		}

		return selectedProductId;

	}

	function updateFormAction(){
		var form = $('#product_addtocart_form');
		var currentAction = form.attr('action');
		var regex = /\/product\/.*?\//;
		var replacement = '/product/' + currentProductId + '/';
		form.attr('action', currentAction.replace(regex, replacement));
	}

	// find the products matching the selected piece count
	function getPieceCountProducts(pieceCount){
		return getAttributeOptionProducts('piece_count', pieceCount);
	}

	// find the products matching the selected color tile
	function getColorTileProducts(colorTile){
		return getAttributeOptionProducts('color_tile', colorTile);
	}

	function getAttributeOptionProducts(attributeCode, optionId){
		var option = [];
		var attributeOptionProducts = {};
		if(typeof attributes[attributeCode] === 'object'){
			option = attributes[attributeCode]['options'];
		}
		for(i = 0; i < option.length; i++){
			if(option[i]['id'] == optionId){
				attributeOptionProducts = option[i]['products'];
			break;
			}
		}
		return attributeOptionProducts;
	}

	function getSelectedPieceCount(){
		var pieceCountSelect = $('select[name="piece_count"]');
		return getAttributeSelection(pieceCountSelect, 'piece_count');
	}

	function getSelectedColorTile(){
		var colorTileChecked = $('input[type="radio"][name="color_tile"]:checked');
		return getAttributeSelection(colorTileChecked, 'color_tile');
	}

	// get the selected value of an attribute's element in the DOM; if unavailable, get the first option from JSON by attribute code
	function getAttributeSelection(el, attributeCode){
		var selectedValue = 0;
		if(el.length > 0) {
			selectedValue = el.val();
		} else if(typeof attributes[attributeCode] === 'object'){
			selectedValue = attributes[attributeCode]['options'][0]['id'];
		}
		return selectedValue;
	}

	function updateOptionDisplay(){

		var pieceCount = getSelectedPieceCount();
		var colorTile = getSelectedColorTile();
		var colorTiles = [];

		if(typeof attributes['color_tile'] === 'object'){
			colorTiles = attributes['color_tile']['options'];
		}
		var pieceCountProducts = getPieceCountProducts(pieceCount);

		// if color tiles are available for this product, loop through all options
		if(colorTile){

			for(i = 0; i < colorTiles.length; i++){

				var colorTileProducts = colorTiles[i]['products'];
				var colorTileRadio = $('input[type="radio"][name="color_tile"][value="' + colorTiles[i]['id'] + '"]');
				var colorTileLabel = colorTileRadio.parent();

				// hide all color tiles initially
				colorTileLabel.hide();

				if(pieceCount){
					// loop through all piece count products, checking for a product ID also in color tile products
					for(var productId in pieceCountProducts){
						if(typeof colorTileProducts[productId] === 'object'){

							// intersection found, show this color tile
							colorTileLabel.show();
							break;

						}
					}
				} else {
					// since there are no piece counts, show all color tiles
					colorTileLabel.show();
				}
			}

			// loop through all visible color tiles
			var isLastTileAvailable = false;
			$('.color-tiles label:visible').each(function(){
				// keep the last selected tile if still available, but still trigger a change event
				var radioButton = $(this).find('input[type="radio"]');
				if(radioButton.attr('value') == colorTile){
					radioButton.prop("checked", true).trigger('change');
					isLastTileAvailable = true;
					return false; // break out of the loop
				}
			});

			// if the last selected tile isn't available anymore, select the first available tile instead
			if(!isLastTileAvailable){
				$('.color-tiles label:visible').first().find('input[type="radio"]').prop("checked", true).trigger('change');
			}

		} else {

			// no color tiles are available for this product, just update the current product ID
			updateCurrentProductId();

		}

	}

	// show the media gallery for the current product
	function updateGallery(){
		var gallery = $('#media-gallery');
		var galleryContent = galleries[currentProductId];
		gallery.html(galleryContent);
	}

	// show the accordion for the current product, preserving the expanded state if possible
	function updateAccordion(){
		var accordion = $('#product-additional-accordion');
		var expandedSection = accordion.find('.expander.expanded');
		var expandedSectionId = null;
		if(expandedSection.length){
			expandedSectionId = expandedSection.attr('id');
		}
		var accordionContent = additional[currentProductId];
		accordion.html(accordionContent);
		if(expandedSectionId) {
			sectionToExpand = accordion.find('.expander-toggle[data-expander-id="' + expandedSectionId + '"]');
			toggleExpander(sectionToExpand, 0, true);
		}
	}

	function updateAvailabilityStatus(){
		var currentStatus = availabilityStatuses[currentProductId];
		var statusElements = $('.product-shop .add-to-cart > *');
		statusElements.addClass('hidden');
		if(availabilityStatuses.hasOwnProperty(currentProductId)) {
			if(isAvailabilityStatusCallToOrder(currentStatus)) {
				statusElements.filter('.call-to-order').removeClass('hidden');
			} else if(isAvailabilityStatusOutOfStock(currentStatus)) {
				statusElements.filter('.out-of-stock').removeClass('hidden');
			}
		} else {
			statusElements.filter('.in-stock').removeClass('hidden');
		}
	}

});