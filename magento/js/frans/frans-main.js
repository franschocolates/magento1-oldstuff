var maxLengthDisplay = {}; // namespace for globally available character count callbacks for textareas
if (!String.prototype.trim) {
	(function() {
		// Make sure we trim BOM and NBSP
		var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
		String.prototype.trim = function() {
			return this.replace(rtrim, '');
		};
	})();
}
jQuery(document).ready(function($){

	var body = $('body');

	// assign class flags to body element indicating client platform and browser
	setClientFlags();

	//LOAD IN ERROR MESSAGES
	initErrorMessages();

	// FIX HEIGHTS OF ROWS WITH OVER-TALL CONTENT
	fixRowHeights();

	// //SESSION-SPECIFIC BLOCKS LOAD VIA AJAX AFTER FULL PAGE CACHE
	// $('#topLinks').load("/frans/ajax/toplinks", function(){
	// 	setFlyoutHeight('right'); // set the correct height for the flyout menu
	// });

	// $('#giftcardsBlock').load("/frans/ajax/virtualgiftcards", function(){
	// 	setFlyoutHeight('right'); // set the correct height for the flyout menu
	// });

	// let's just assume it's correct now.
	setFlyoutHeight('right');

	if(body.hasClass('touch')){
		//ENABLE FASTCLICK TO REMOVE 300ms DELAY AFTER MOBILE TOUCH EVENTS FIRE
		FastClick.attach(document.body);

		//INTERCEPT AND DISABLE HORIZONTAL SCROLLING ON TOUCH DEVICES WITH HAMMER.JS
		var options = {
			dragBlockHorizontal: true
		};
		var hammertime = new Hammer(body.get(0), options);
		hammertime.on("dragleft dragright swipeleft swiperight", function(e){
			// determine direction of swipe and only respond if it moved farther than 100px
			if(e.gesture.deltaX < -100 && body.hasClass('flyout-left')){
				toggleFlyout('left'); // swipe to close the left flyout
			} else if(e.gesture.deltaX > 100 && body.hasClass('flyout-right')){
				toggleFlyout('right'); // swipe to close the right flyout
			} else {
				e.preventDefault(); // prevent the event from triggering unwanted clicks
			}
		});
		hammertime.on("dragdown swipedown dragup swipeup", function(e){
			scrollBehaviors();
		});
	}

	function scrollBehaviors(){
		setMobileHeaderGlass();
		resetHorizontalScroll();
	}

	// SHORT DELAY TO ALLOW BROWSER TO LOCATE LAST SCROLL POSITION IF PAGE WAS REFRESHED
	setTimeout(setMobileHeaderGlass, 250);

	// ADD SEMITRANSPARENT BACKGROUND TO MOBILE HEADER AND FORCE HORIZONTAL SCROLL TO THE LEFT
	function setMobileHeaderGlass(){
		var doGlass = (body.hasClass('flyout-left') || body.hasClass('flyout-right') || ($(window).scrollTop() > 0));
		$('header[role="banner"]').toggleClass('glass', doGlass);
	}

	var lastScrollPosition = 0;

	function resetHorizontalScroll(){
		// just in case the user manages to scroll horizontally, put it back
		if($(window).scrollLeft() != 0){
			window.scrollTo(0,$(window).scrollTop());
		}
	}

	$(window).on('resize scroll', function(){
		scrollBehaviors();
	});



	//INDICATE WHETHER A LINK GOT FOCUS BECAUSE OF KEYBOARD NAVIGATION, OR MOUSE INPUT
	body.on('blur', '#flyout-right-toggle.keyboard-navigated', function(event){
		if(body.hasClass('flyout-right')){
			toggleFlyout('right');
		}
	});
	body.on('mousedown', 'a', function(){
		var linkObj = $(this);
		linkObj.addClass('navigation-lock'); // briefly set a lock to prevent the focus event from setting a keyboard-navigated class
		setTimeout(function(){
			linkObj.addClass('navigation-lock'); // remove the lock
		}, 500);
	});
	body.on('focus', 'a', function(){
		if(!$(this).hasClass('navigation-lock')){
			$(this).addClass('keyboard-navigated'); // prevent top-level links from displaying keyboard-navigation styles when clicked
		}
	});

	//TOGGLE MOBILE NAV (LEFT FLYOUT)
	body.on('click', '#flyout-left-toggle', function(event, elm){
		event.preventDefault();
		toggleFlyout('left');
	});

	//TOGGLE MOBILE CART MENU (RIGHT FLYOUT) -- TRIGGERS DESKTOP VIEW TOO
	body.on('click', '#flyout-right-toggle', function(event){
		event.preventDefault(); // don't let the event bubble up to #topLinks
		event.stopPropagation();
		toggleFlyout('right');
	});

	//HIDE DESKTOP VIEW OF CART MENU WHEN USER CLICKS ANYWHERE OUTSIDE OF DROPDOWN
	body.on('click', '#topLinks', function(event){
		if(body.hasClass('flyout-right') && $(window).width() > 743){
			event.preventDefault();
			toggleFlyout('right');
		}
	});

	// PREVENT THE PREVIOUS EVENT HANDLER FROM OBSERVING CLICKS WITHIN THE CART MENU
	body.on('click', '.cart-menu', function(event){
		if(body.hasClass('flyout-right') && $(window).width() > 743){
			event.stopPropagation(); // don't let the event bubble up to #topLinks
		}
	});


	body.on('click', '.cart-menu', function(event){
		if(body.hasClass('flyout-right') && $(window).width() > 743){
			event.stopPropagation(); // don't let the event bubble up to #topLinks
		}
	});


	var disableScroll = false;

	function disableScrolling() {
		disableScroll = true;
	}

	function enableScrolling() {
		disableScroll = false;
	}
	document.ontouchmove = function(e){
		if(disableScroll){
			e.preventDefault();
		}
	};

	body.on('click', '.social-newsletter', function(){
		disableScrolling();
	});

	body.on('click', '.simplemodal-close', function(){
		enableScrolling();
		var html = jQuery('html');
		var scrollPosition = html.data('scroll-position');
		html.css('overflow', html.data('previous-overflow'));
		window.scrollTo(scrollPosition[0], scrollPosition[1])
	});

	body.on('click', '#newsletter-modal-close', function(){
		enableScrolling();
		var html = jQuery('html');
		var scrollPosition = html.data('scroll-position');
		html.css('overflow', html.data('previous-overflow'));
		window.scrollTo(scrollPosition[0], scrollPosition[1])
	});


	body.on('click', '.close-modal', closeModal);

	function closeModal(evt)
	{
		console.log("close modal");
		evt.preventDefault();

		jQuery.modal.close();
	}


	function toggleFlyout(direction){
		var flyout = $('#flyout-' + direction);
		var viewportWidth = $(window).width();
		var viewportHeight = $(window).height();
		var scrollToPosition = 0;
		if(direction == 'left'){
			body.removeClass('flyout-right'); // close the right flyout, just in case it opened somehow
		} else { // direction == 'right'
			body.removeClass('flyout-left'); // close the left flyout, just in case it opened somehow
			$('#topLinks').css({ height: viewportHeight }); // set the correct height for the toplinks div
		}
		setFlyoutHeight(direction);
		// set up scroll behavior and flyout position for mobile view
		if(viewportWidth <= 742){
			if(!body.hasClass('flyout-left') && !body.hasClass('flyout-right')){
				// about to open flyout; position the main content so it appears fixed in place as it was before toggle
				lastScrollPosition = $(document).scrollTop();
				$('#main-wrapper').css({ top: (lastScrollPosition * -1) });
			} else {
				// about to close flyout; restore main content position and scroll page to last position
				$('#main-wrapper').css({ top: 0 });
				scrollToPosition = lastScrollPosition;
			}
		}
		body.toggleClass('flyout-' + direction);
		// cause mobile view to scroll to the desired position
		if(viewportWidth <= 742){
			window.scrollTo(0, scrollToPosition); // known bug: visible delay between flyout toggle and scrollTo on some devices
		}
		setMobileHeaderGlass();
	}

	function setFlyoutHeight(direction){
		// determine and set the correct height for the flyout menu
		var flyout = $('#flyout-' + direction);
		var viewportHeight = $(window).height();
		var flyoutHeight = (flyout.height() < viewportHeight) ? viewportHeight : flyout.height();
		if(direction == 'right'){
			flyout.toggle();
			flyout.css({ height: flyoutHeight });
		}
	}

	//TOGGLE EXPANDERS (ANIMATED SLIDING ACCORDION ELEMENTS)
	body.on('click', '.expander-toggle', function(event){
		if(!$(this).is('.expanded.keep-open, .toggle-disabled')){
			toggleExpander($(this));
		}
		event.preventDefault();
	});

	//MAIN NAV JS
	var menuTimer;
	var menuDelay = 250; // time in ms to wait before showing or hiding the menu
	var currentMenuPanelIndex = 0;
	body.on('mouseenter mouseleave', '.navbar-nav > li, .navbar-main-expand, .navbar-arrow', function(event){
		// check the menu item to see if it needs to be forced closed (i.e., it has no children to display)
		var forceClose = ($(this).attr('data-force-close') == 'true');
		toggleMenu(event, forceClose);
		$('.navbar-nav > li > a').removeClass('keyboard-navigated'); // remove the keyboard-navigation styles if they're visible
	});
	body.on('mouseenter', '.navbar-nav > li', function(){
		switchMenuPane($(this));
	});
	// same as event handlers above, but for keyboard input for mouseless traversal
	body.on('focus blur', '.navbar-nav > li > a', function(event){
		// check the menu item to see if it needs to be forced closed (i.e., it has no children to display)
		var forceClose = ($(this).parent().attr('data-force-close') == 'true');
		toggleMenu(event, forceClose);
	});
	body.on('focus', '.navbar-nav > li > a', function(){
		switchMenuPane($(this).parent());
	});
	function toggleMenu(event, forceClose) {
		clearTimeout(menuTimer);
		menuTimer = null;
		// optional force arg defaults to false
		if(typeof event == 'undefined'){
			event = { type: 'mouseleave' };
		}
		if(typeof forceClose == 'undefined'){
			forceClose = false;
		}
		var doHide = (event.type != 'mouseenter' && event.type != 'focusin');
		var dropdown = $('.navbar-main-expand');
		var delay = (!forceClose) ? menuDelay : 0; // delay before toggling; no delay if forcing it closed
		var duration = 60;
		// make sure the menu is hidden before showing, or visible before hiding
		if(dropdown.is(':visible') == doHide){
			menuTimer = setTimeout(function(){
				if(menuTimer != null){
					menuTimer = null;
					dropdown.slideToggle(duration, 'linear', function(){
						if(doHide || forceClose){
							// if closing the dropdown, remove all panels and reset the current one; hide arrow
							dropdown.hide(); // fixes a bug where quickly reopening the menu would cause it to display with no content
							currentMenuPanelIndex = 0;
							$('.navbar-main-expand .navbar-panel').hide();
							$('.navbar-arrow').hide();
							$('.navbar-nav > li > a').blur(); // remove the keyboard-navigation styles if they're visible
						} else {
							var openPanel = dropdown.find('.navbar-panel').filter(':visible').first();
							$('.navbar-main-expand').css('height', (openPanel.outerHeight() + 5));
						}
					});
				}
			}, delay);
		}
	}
	function switchMenuPane(menuItem){
		var duration = 75;
		var panels = $('.navbar-main-expand .navbar-panel');
		var panelIndex = menuItem.attr('data-node-index');
		var panel = panels.filter('[data-node-index="'+panelIndex+'"]');

		// if there's no menu to display, trigger a mouseleave event to close the dropdown
		if(panel.length == 0){
			$('.navbar-nav > li[data-node-index="'+panelIndex+'"]').trigger('mouseleave');
		}

		// update the variable that remembers what the previous menu item was
		var lastMenuPanelIndex = currentMenuPanelIndex;
		currentMenuPanelIndex = panelIndex;
		var lastPanel = panels.filter('[data-node-index="'+lastMenuPanelIndex+'"]');

		// don't go any further if the menu item hasn't actually changed
		if(currentMenuPanelIndex == lastMenuPanelIndex){
			return;
		}

		// kill any transitions already in progress before beginning a new one
		panels.each(function(){
			$(this).stop(true,true);
		});

		panel.fadeIn({
			duration: duration,
			easing: 'linear',
			always: function(){
				var newHeight = (panel.outerHeight() + 5);
				$('.navbar-main-expand').css('height', newHeight);
			},
			complete: function(){
				moveMenuArrow(menuItem);
			}
		});
		lastPanel.fadeOut({
			duration: duration,
			easing: 'linear'
		});

	}
	function moveMenuArrow(menuItem){
		if(menuItem.attr('data-force-close') != 'true'){
			// center the arrow under the menu item
			var offset = (menuItem.position().left + (menuItem.width() / 2) - 8); // 8px is half the width of the arrow image
			var duration = 25;
			var arrow = $('.navbar-arrow');
			if(arrow.is(':visible')){
				// arrow is already visible, interrupt existing animations to move it to current correct location
				arrow.clearQueue().animate(
					{
						left: offset
					},
					{
						duration: duration
					}
				);
			} else {
				// arrow is not visible so update its position, then show it after delay
				arrow.css({ left: offset });
				setTimeout(function(){
					// if the menu is not visible after the delay elapses, don't bother showing the arrow after all
					if($('.navbar-main-expand').is(':visible')){
						arrow.show();
					}
				}, menuDelay);
			}
		}
	}
	// poll DOM to ensure arrows never remain visible when menu is hidden, and vice versa
	setInterval(function(){
		if(!$('.navbar-main-expand').is(':visible')){
			$('.navbar-arrow').hide().removeClass('active');
		} else {
			$('.navbar-arrow').show().addClass('active');
		}
	}, 500);
	//END MAIN NAV JS

	//SEARCH FORM BEHAVIOR
	var searchField = $('input#search');
	var initialSearchValue = searchField.val();

	body.on('click', '.search-icon', function(event){
		var searchIcon = $(this);
		searchIcon.addClass('focus-lock');
		setTimeout(function(){
			searchIcon.removeClass('focus-lock');
		}, 500);
		if(checkIfSearchIsClosed()){
			toggleSearch(true);
		} else if(searchField.val() == '' || searchField.val() == initialSearchValue){
			toggleSearch(false)
		} else {
			$('#search_mini_form').submit();
		}
	});

	body.on('blur', 'input#search', function(event){
		setTimeout(function(){
			if(!$('.search-icon').hasClass('focus-lock')){
				toggleSearch(false);
			}
		}, 150);
	});

	body.on('focus', 'input#search', function(event){
		toggleSearch(true);
	});

	function checkIfSearchIsClosed(){
		return !searchField.hasClass('active');
	}

	function toggleSearch(doShow){
		if(!doShow && !searchField.is(':focus')){
			if(searchField.is(':focus')){
				searchField.blur();
			}
		}
		searchField.toggleClass('active', doShow);
		if(doShow){
			searchField.select();
		}
	}
	//END SEARCH FORM BEHAVIOR

	//PRODUCT GALLERY NAVIGATION
	body.on('click', '.gallery-control', function(){
		var slideOrDirection = null;
		if($(this).hasClass('gallery-prev')){
			slideOrDirection = 'prev';
		} else if($(this).hasClass('gallery-next')){
			slideOrDirection = 'next';
		} else {
			slideOrDirection = Number($(this).attr('data-target-index'));
		}
		goToSlide(slideOrDirection);
	});

	// add swipe navigation for product image galleries
	if(body.hasClass('touch')){
		var gallerySwipeDelay = 500; // wait this many ms between gallery swipes
		var gallerySwipeAllowed = true;
		var gallery = $('#media-gallery');
		if(gallery.length > 0){
			var touchGallery = new Hammer(gallery.get(0), {dragBlockHorizontal: true});
			touchGallery.on("dragleft dragright", function(e){
				if(gallerySwipeAllowed){
					gallerySwipeAllowed = false;
					if(e.type == 'dragleft'){
						slideOrDirection = 'next';
					} else {
						slideOrDirection = 'prev';
					}
					goToSlide(slideOrDirection);
					setTimeout(function(){
						gallerySwipeAllowed = true;
					}, gallerySwipeDelay);
				}
			});
		}
	}

	function goToSlide(slideOrDirection){
		var slides = [];
		var activeSlideIndex = null;
		var targetIndex = null;
		var targetSlide = null;
		var index = 0;
		$('.product-img-box li').each(function(){
			slides[index] = $(this);
			if($(this).hasClass('active')){
				activeSlideIndex = index;
			}
			index++;
		});
		// define the offset each direction is bound to, for adjusting the slide index from its current active position
		var directions = [];
		directions['next'] = 1; // will increment slide index by one if direction is "next"
		directions['prev'] = slides.length - 1; // instead of just -1, to fix http://javascript.about.com/od/problemsolving/a/modulobug.htm
		// figure out if slideOrDirection contains a slide index, or a direction string
		var slideMatch = slides[slideOrDirection];
		var directionMatch = directions[slideOrDirection];
		if(typeof slideMatch != 'undefined') {
			targetIndex = slideOrDirection;
			targetSlide = slideMatch;
		} else if(typeof directionMatch != 'undefined'){
			targetIndex = ((activeSlideIndex + directionMatch) % slides.length);
			targetSlide = slides[targetIndex];
		}
		if(targetSlide) {
			// do the slide change
			var activeSlide = slides[activeSlideIndex];
			var pagers = $('.gallery-pager .gallery-control');
			var targetPager = pagers.filter('[data-target-index="' + targetIndex + '"]');
			var activePager = pagers.filter('.active');
			activeSlide.fadeOut(250);
			targetSlide.fadeIn(250, function(){
				targetSlide.addClass('active');
				targetPager.addClass('active');
				activeSlide.removeClass('active');
				activePager.removeClass('active');
			});
		}
	}
	//END PRODUCT GALLERY NAVIGATION

	body.on('submit', '#my-account-address', function(ev) {
		if (!ev.preventDefault()) {
			ev.returnValue = false;
			ev.preventDefault();
		} else {
			ev.preventDefault();
			ev.stopPropagation();
			e.stopPropagation();
		}
		addressSubmitAction($('#my-account-address'), accountAddressSuccessAction);
	});

	body.on('submit', '#addressSelectionForm-customer', submitAddressCorrectionCustomer);
	body.on('click', '#suggestedAddressEdit-customer', addressCustomerSuggestedEdit);

	body.on('change', '#order-history-age-filter', function() {
		document.location = $(this).find('option:selected').attr('data-url');
	});

	body.on('click', '.stores-banner .tile-image', function(e){
		if($(e.toElement).hasClass("tile-image")){
			document.location = $(this).attr('data-url');
		}
	});

	$("#btn-edit-cc").bind("click", function(){
		//show the card form.
		$("#cc-saved").addClass('hide');
		$("#cc-save-form").removeClass('hide');
		// recalculate icon positions
		attachHelpIcons();
		return false;
	});

	$("#btn-cc-cancel").bind("click", function(){
		//hide the card form.
		$("#cc-save-form").addClass('hide');
		$("#cc-saved").removeClass('hide');
		$("#err-message-wrapper").html("");
		return false;
	});

	$("#btn-edit-signin").bind("click", function(){
		//show the sign in information form.
		$("#signin-view").addClass('hide');
		$("#signin-form").removeClass('hide');
		return false;
	});

	$(".my-account-container").delegate(".data-table tr[data-url]", "click", null, function(){
		var url = $(this).attr("url");
		showAjaxModal(url);
	});

	//DOB checkbox click
	body.on('change', '#address-modal #dob-checkbox', function(){
		hideFieldsDOB();
	});

	body.on('click', '#receipt-print-button, #order-print-button', function(){
		showAjaxLoadingIcon();
		$('#print-container').attr('src', $(this).attr('data-print-target'));
		$('#print-container').on('load', hideAjaxLoadingIcon);
	});

	body.on('keyup', '.cart-items input.qty', function(){
		var updateButton = $(this).parent().siblings();
		updateButton.children().fadeIn(250);
	});

	body.on('blur', '.cart .data-table input.qty', function(){
		if($(this).val() == '') { $(this).val('0'); }
	});

	body.on('click', '.cart .btn-update', function(){
		showAjaxLoadingIcon();
	});

	//disables the link for the currently selected frans-tab, if there is one
	body.on('click', '.frans-tabs li.active', function(event){
		event.preventDefault();
	});
	
	//add a close icon to the global site notice if a notice is there
	function addNoticeCloseButton(){
		var noticeContainer = $('.notice-container');
		if(noticeContainer.height() > 0){
			var closeBtn = $('<span />').html('X').addClass('close').click(function(event){
				event.preventDefault();
				$(this).parent().fadeOut(200);
			});		
			noticeContainer.append(closeBtn);
		}
	}
	addNoticeCloseButton();

	/* Attach event handlers to tooltip anchors (must have position and ID data attrs, and "autotooltip" class) */
	jQuery('.autotooltip[data-tooltip-position][data-tooltip-id]').each(function(){
		var anchor = jQuery(this);
		anchor.on('focus', function(){
			toggleTooltip(jQuery('#'+anchor.attr('data-tooltip-id')), anchor, true);
		});
		anchor.on('blur', function(){
			toggleTooltip(jQuery('#'+anchor.attr('data-tooltip-id')), anchor, false);
		});
	});

	function hideFieldsDOB(){

		var form = $("#address-modal");
		var checked = form.find(".include-dob-checker input").is(':checked');

		if(!checked)
		{
			form.find(".customer-dob").addClass('hide');
			form.find(".customer-dob input, .customer-dob select").attr("disabled", "disabled").removeClass("validate-custom");
		}
		else
		{
			form.find(".customer-dob").removeClass('hide');
			form.find(".customer-dob input, .customer-dob select").removeAttr("disabled").addClass("validate-custom");
		}

	}

	// char remaining function
	maxLengthDisplay.onEditCallback = function(remaining){
		$(this).siblings('.chars-remaining').text("Characters remaining: " + remaining);

		if(remaining > 0){
			$(this).css('background-color', 'white');
		}
	}

	jQuery.fn.extend({
		limitMaxlength: function(options){
			var settings = jQuery.extend({
				attribute: "maxlength",
				onLimit: function(){},
				onEdit: function(){}
			}, options);

			// Create the span for displaying remaining chars after element
			this.after("<span class=\"chars-remaining\"></span>");

			// Event handler to limit the textarea
			var onEdit = function(){
				var textarea = jQuery(this);
				var maxlength = parseInt(textarea.attr(settings.attribute));

				if(textarea.val().length > maxlength){
					textarea.val(textarea.val().substr(0, maxlength));

					// Call the onlimit handler within the scope of the textarea
					jQuery.proxy(settings.onLimit, this)();
				}

				// Call the onEdit handler within the scope of the textarea
				jQuery.proxy(settings.onEdit, this)(maxlength - textarea.val().length);
			};

			this.each(onEdit);

			return this.keyup(onEdit)
				.keydown(onEdit)
				.focus(onEdit);
		}
	});

	maxLengthDisplay.onLimitCallback = function(){
		$(this).css('background-color', 'red');
	}

	$('textarea[maxlength].show-remaining').limitMaxlength({
		onEdit: maxLengthDisplay.onEditCallback,
		onLimit: maxLengthDisplay.onLimitCallback

	});

	// replace checkboxes with Fran's images
	$('form').jqTransform();
	// select elements should always display the color of their selected option, which requires JS
	setUpSelects();

	jQuery.fn.extend({
		limitMaxlength: function(options){
			var settings = jQuery.extend({
				attribute: "maxlength",
				onLimit: function(){},
				onEdit: function(){}
			}, options);

			// Create the span for displaying remaining chars after element
			this.after("<span class=\"chars-remaining\"></span>");

			// Event handler to limit the textarea
			var onEdit = function(){
				var textarea = jQuery(this);
				var maxlength = parseInt(textarea.attr(settings.attribute));

				if(textarea.val().length > maxlength){
					textarea.val(textarea.val().substr(0, maxlength));

					// Call the onlimit handler within the scope of the textarea
					jQuery.proxy(settings.onLimit, this)();
				}

				// Call the onEdit handler within the scope of the textarea
				jQuery.proxy(settings.onEdit, this)(maxlength - textarea.val().length);
			};

			this.each(onEdit);

			return this.keyup(onEdit)
				.keydown(onEdit)
				.focus(onEdit);
		}
	});

});

// global function to detect if customer is logged in, or set flag for future detection
function isLoggedIn(stateToSet){
	// currently we store the state in the "logged-in" class in body element
	// this is set (if customer's logged in) via JS in the response from /gs/links/toplinks
	if(typeof(stateToSet) === 'boolean'){
		jQuery('body').toggleClass('logged-in', stateToSet);
	}
	return jQuery('body').hasClass('logged-in');
}

//TOGGLE EXPANDERS (ANIMATED SLIDING ACCORDION ELEMENTS)
function toggleExpander(toggler, duration, doSuppressScroll){

	// set a default duration of 150ms, but only use if toggler has no data-expander-duration attr set
	if(typeof duration === "undefined"){
		var togglerDuration = toggler.attr('data-expander-duration');
		duration = 150;
		if(togglerDuration){
			duration = parseInt(togglerDuration);
		}
	}

	// by default don't suppress scrolling behavior, it's governed by scroll-on-open/scroll-for-mobile classes on the toggler element
	if(typeof doSuppressScroll === "undefined"){
		doSuppressScroll = false;
	}

	var selector = '.expander#' + toggler.attr('data-expander-id');
	var expander = jQuery(selector);
	var isMobile = (window.innerWidth < 761);
	var scrollOffset = isMobile ? 60 : 10; // offset amount depends on if mobile header is at the top of the screen

	// make sure there is an expander
	if(expander.length > 0){

		// open or close the expander
		expander.slideToggle(duration, function(){
			// after finished opening, set the toggler's and expander's expanded classes accordingly
			toggler.toggleClass('expanded');
			expander.toggleClass('expanded');
			// confirm that scrolling's not suppressed, and the expander is now expanded (open)
			if(!doSuppressScroll && toggler.is('.expanded')){
				// scroll only if designated as scroll-on-open or scroll-for-mobile (
				if(toggler.is('.scroll-on-open') || (toggler.is('.scroll-for-mobile') && isMobile)){
					// wait until expanders have finished toggling to ensure accurate scroll-to position is calculated
					setTimeout(function(){
						jQuery('html, body').animate({scrollTop: (toggler.offset().top - scrollOffset)}, 500);
					}, duration);
				}
			}
		});

		// detect if this expander is part of a group
		var thisGroup = toggler.attr('data-expander-group');
		if(thisGroup){

			// close the other expanders in the group
			var groupItems = jQuery('.expander-toggle[data-expander-group="' + thisGroup + '"]');
			groupItems.not(toggler).each(function(){
				var selector = '.expander#' + jQuery(this).attr('data-expander-id');
				var expander = jQuery(selector);
				var toggler = jQuery(this);
				expander.slideUp(duration, function(){
					toggler.removeClass('expanded');
					expander.removeClass('expanded');
				});
			});

			// if this expander occurs in a sequence, update surrounding classes accordingly
			var thisPosition = toggler.attr('data-expander-sequence');
			if(thisPosition){

				// sort the elements in the object by their data-expander-sequence attribute values
				var sortedGroupItems = groupItems.sort(function(a,b){
					var contentA = parseInt(jQuery(a).attr('data-expander-sequence'));
					var contentB = parseInt(jQuery(b).attr('data-expander-sequence'));
					return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
				});

				// get the index of the current item
				var activeItemIndex = sortedGroupItems.index(toggler);

				// update the sequence classes (next, active, previous)
				sortedGroupItems.removeClass('previous active next');
				sortedGroupItems.eq(activeItemIndex - 1).addClass('previous');
				sortedGroupItems.eq(activeItemIndex).addClass('active');
				sortedGroupItems.eq(activeItemIndex + 1).addClass('next');

				// mark all togglers in the group as disabled, except for the ones that are before this step
				sortedGroupItems.addClass('toggle-disabled');
				sortedGroupItems.each(function(){
					jQuery(this).removeClass('toggle-disabled');
					if(sortedGroupItems.index(jQuery(this)) == activeItemIndex){
						return false; // exit the loop here
					}
				});

			}
		}

	}

}

// using this event to ensure that jQuery can correctly calculate element positions
jQuery(window).load(function(){
	// spawn a clickable help icon to overlay all text fields that need it
	attachHelpIcons();
});

// on window resize, update absolute positions of help icons, and fix row heights
jQuery(window).resize(function(){
	attachHelpIcons();
	fixRowHeights();
});

// Re-execute DOM manipulation whenever AJAX finishes, with a slight delay to give DOM time to update
jQuery(document).ajaxComplete(function() {
	// wait a bit to give the DOM time to update first
	setTimeout(function() {

		// spawn a clickable help icon to overlay all text fields that need it
		attachHelpIcons();
		// replace checkboxes with Fran's images
		jQuery("form").jqTransform();
		// select elements should always display the color of their selected option, which requires JS
		setUpSelects();

	}, 500);
});

//Modal Functions go here
function showAjaxModal(url, options, errorCallback)
{



	makeAjaxRequest(url, false, function(response){
		// pass through options object if optional param was provided
		if(typeof options === null){
			showModal(response['html']);
			jQuery(window).trigger('resize');
		} else {
			showModal(response['html'], options);
			jQuery(window).trigger('resize');
		}
	}, errorCallback);
}

function showModal(content, options)
{
	defaultOptions = getModalOptions();
	// if the optional options param is null, use the defaults; otherwise apply param
	if(typeof options === null){
		options = defaultOptions;
	} else {
		options = jQuery.extend(defaultOptions, options); // merge extending options object into default one
	}

	jQuery('#modal-content').modal(options);
	updateModal(content);
}

function updateModal(content)
{
	jQuery('#modal-content').html(content);
}

function getModalOptions()
{
	var options = {
		onOpen: function (dialog) {
			jQuery('#simplemodal-container').css({'height': 'auto'});
			dialog.overlay.fadeIn('fast', function () {
				dialog.data.hide();
				dialog.container.fadeIn('fast', function () {
					dialog.data.fadeIn('fast');
				});
			});
		},
		onClose: function (dialog) {
			dialog.overlay.fadeOut('fast');
			dialog.container.fadeOut('fast', function () {
				jQuery.modal.close();
				jQuery('#modal-content').html('');
			});

		},
		closeHTML: "<a href='#' title='Close' class='modal-close'> </a>",
		containerCss: {},
		autoResize: false,
		//autoPosition: true,
		position: [50],
		overlayCss: {},
		overlayClose: false,
		fixed: false
		//opacity: 90
	};
	return options;
}

/* Toggle a Fran's tooltip; 1st arg is tooltip element, 2nd is anchor element, 3rd is bool show/hide */
function toggleTooltip(tooltip, anchor, doShow){
	if(tooltip == null){ tooltip = jQuery('.tooltip'); } // if no tooltip is specified, loop through all available
	tooltip.each(function(){
		var i = jQuery(this);
		if(doShow){
			prepareTooltip(i, anchor);
			i.fadeIn(100);
		} else {
			i.fadeOut(100);
		}
	});
}

/* Calculate and set the tooltip's position relative to an anchor element, and assign it the correct class */
function prepareTooltip(tooltip, anchor){
	var tooltipPosition = anchor.attr('data-tooltip-position').split(' '); // make array of positions sorted by priority
	var elementMetrics = getElementMetrics(anchor);
	var tooltipMetrics = getElementMetrics(tooltip);
	var margin = 10; // the distance in pixels the arrow graphic extends outside the tooltip

	// tooltip is likely a child of a positioned element, so find it and get its position in the document
	var containerMetrics = { top: 0, left: 0 };
	tooltip.parents().each(function(){
		if(jQuery(this).css('position') == 'relative'){
			containerMetrics = getElementMetrics(jQuery(this));
			return false;
		}
	});

	// calculate left and top positions for vertical and horizontal centering, respectively
	var offsetX = (elementMetrics.width - tooltipMetrics.width) / 2;
	var offsetY = (elementMetrics.height - tooltipMetrics.height) / 2;
	var autoLeft = elementMetrics.left - containerMetrics.left + offsetX;
	var autoTop = elementMetrics.top - containerMetrics.top + offsetY;

	for(i = 0; i < tooltipPosition.length; i++){
		// remove positional classes from tooltip, then set the correct one
		tooltip.removeClass('top bottom left right');
		tooltip.addClass(tooltipPosition[i]);
		// calculations will differ depending on position requested
		switch(tooltipPosition[i]){
			case 'top':
				tooltip.css({
					top: elementMetrics.top - containerMetrics.top - tooltipMetrics.height - margin,
					left: autoLeft
				});
				break;
			case 'bottom':
				tooltip.css({
					top: elementMetrics.bottom - containerMetrics.top + margin,
					left: autoLeft
				});
				break;
			case 'left':
				tooltip.css({
					top: autoTop,
					left: elementMetrics.left - containerMetrics.left - tooltipMetrics.width - margin
				});
				break;
			case 'right':
				tooltip.css({
					top: autoTop,
					left: elementMetrics.right - containerMetrics.left + margin
				});
				break;
		}

		var topOffset = (containerMetrics.top + parseFloat(tooltip.css('top')));
		var leftOffset = (containerMetrics.left + parseFloat(tooltip.css('left')));
		var bottomOffset = (jQuery(document).height() - (topOffset + tooltipMetrics.height));
		var rightOffset = (jQuery(window).width() - (leftOffset + tooltipMetrics.width));

		// validate that the tooltip isn't cut off either side of the page, or the document top or bottom
		if(topOffset > 0 && leftOffset > 0 && bottomOffset > 0 && rightOffset > 0){
			break;
		}
	}
}

/* Get object containing height, width, and absolute positions (from top/left) of all edges of a jQuery element */
function getElementMetrics(jqElement){
	var offset = jqElement.offset();
	var height = jqElement.outerHeight();
	var width = jqElement.outerWidth();
	var positions = {
		top: offset.top,
		bottom: offset.top + height,
		left: offset.left,
		right: offset.left + width,
		height: height,
		width: width
	};
	return positions;
}


//created a custom Varien DOBFrans object because the core did not handle a select input.
Varien.DOBFrans = Class.create();
Varien.DOBFrans.prototype = {
	initialize: function(selector, required, format) {
		var el = $$(selector)[0];
		var container       = {};
		container.day       = Element.select(el, '.dob-day select')[0];
		container.month     = Element.select(el, '.dob-month select')[0];
		container.year      = Element.select(el, '.dob-year input')[0];
		container.full      = Element.select(el, '.dob-full input')[0];
		container.advice    = Element.select(el, '.validation-advice')[0];

		new Varien.DateElement('container', container, required, format);

		if (!jQuery("#address-modal").find(".include-dob-checker input").is(':checked'))
			jQuery("#address-modal").find(".customer-dob input, .customer-dob select").removeClass("validate-custom");
	}
};

function showSuccessMessage(msg)
{
	$successMsg = jQuery('#modal-success-msg');
	$successMsg.html(msg);
	$successMsg.fadeIn('fast').delay(1000).fadeOut(800);
}

/* Declare global variable to control animation of loading icon */
var loadingIconTimer;

function showAjaxLoadingIcon(){
	var loadingIcon = jQuery('#loading-icon');
	loadingIcon.fadeIn(150);
	loadingIconTimer = setInterval(function(){
		animateLoadingIcon(loadingIcon);
	}, 66);
}

function hideAjaxLoadingIcon(){
	clearInterval(loadingIconTimer);
	jQuery('#loading-icon').fadeOut(150);
}

function showGiftCardSyncIcon(){
	var giftCardSync = jQuery('#giftcard-validation-sync');
	var submitButton = jQuery("button[id*='save-order']");
	if(!submitButton.is(":disabled")) {
		submitButton.attr('disabled', 'disabled');
	}
	giftCardSync.fadeIn(150);
}

function hideGiftCardSyncIcon(){

	jQuery('#giftcard-validation-sync').fadeOut(150);
	var submitButton = jQuery("button[id*='save-order']");
	if(submitButton.is(":disabled")) {
		submitButton.removeAttr('disabled');
	}
}

function animateLoadingIcon(el){
	var numberOfFrames = 10;
	var currentFrame = parseInt(el.attr('class').slice(18)); // remove word "loading-animation-" from class to get current frame number
	var nextFrame = currentFrame + 1;
	if(nextFrame > numberOfFrames){
		nextFrame = 1;
	}
	el.addClass('loading-animation-'+nextFrame);
	el.removeClass('loading-animation-'+currentFrame);
}

//Generic ajax function
function makeAjaxRequest(url, data, callback, errorCallback){

	var errorHandler = errorCallback || function(jqXHR, textStatus, errorThrown){
			if(jqXHR.status != 0){ // confirm user didn't just click a link while Ajax request was alive
				alert('<strong>Oops! There was a problem:</strong> ' + errorThrown);
			}
		};
	jQuery.ajax({
		beforeSend: showAjaxLoadingIcon,
		dataType: 'json',
		url: url,
		type: 'POST',
		data: data,
		success: function(response){
			callback(response);
		},
		error: errorHandler,
		complete: hideAjaxLoadingIcon
	});
}

//Click event handler for address form submit button
function addressSubmitAction(form, successAction){

	if(isUndefined(form) || !(form instanceof jQuery)){
		var form = jQuery(this).closest("form");
	}
	if(addressForm.validator.validate())
	{
		var data = form.serialize();
		makeAjaxRequest(form.attr("action"), data, function(response){

			if(response['status'] == 'success')
			{
				if(response.invalidFields == 'Yes'){

					if (response['score'] > 0) {
						successAction(response);
						jQuery("#address-modal").replaceWith(response['default_billing_modal']);
					} else {
						jQuery("#address-modal").replaceWith(response['default_billing_modal']);
					}

				} else if(response.pobox == 'Yes'){
					console.log('IN HERE');
					jQuery("#address-modal").replaceWith(response['default_billing_modal']);
				} else {
					successAction(response);
					jQuery.modal.close();
				}

			}
			else
			{
				//This may be needed again and should be centralized
				var $erorrMessages  = '<ul class="messages"><li class="error-msg"><ul>';
				jQuery.each(response['msg'], function( key, value ) {
					$erorrMessages  += '<li><span>'+value+'</span></li>';
				});
				$erorrMessages  += '</ul></li></ul>';

				jQuery('#modal-content .page-title').after($erorrMessages);
			}
		});
	}
}

function submitAddressCorrectionCustomer(evt) {
	evt.preventDefault();
	var myForm = jQuery('#addressSelectionForm-customer');
	makeAjaxRequest('/frans/customer_address/addChosenCustomerAddress', myForm.serialize(), accountAddressSuccessSubmit, showAccountError);

	jQuery.modal.close();
}

function addressCustomerSuggestedEdit(evt){
	evt.preventDefault();
	var myForm = jQuery('#addressSelectionForm-customer');
	makeAjaxRequest('/frans/customer_address/addChosenCustomerAddress', myForm.serialize(), accountAddressSuccessEdit, showAccountError);

	jQuery.modal.close();
}

function accountAddressSuccessAction(response) {
	//get all of the html blocks to update.

	var blocks = response['blocks'];
	var catalogMailingDefaultHtml = blocks["catalog_subscribe_mailing_default"];
	var billingDefaultHtml = blocks["billing_default"];
	var shippingGridHtml = blocks["shipping_address_grid"];
	var subscriptionsHtml = blocks["subscriptions"];

	jQuery("#mailing-address-subscribe-catalog").html(catalogMailingDefaultHtml);
	jQuery("#billing_default").html(billingDefaultHtml);
	jQuery("#shipping_address_grid").html(shippingGridHtml);
	jQuery("#newsletter-email").html(subscriptionsHtml);

	showSuccessMessage(response['msg']);
}

function accountAddressSuccessSubmit(response) {

	if (response.hasOwnProperty('billingDefault')) {
		jQuery("#billing_default").html(response.billingDefault);
	}

	if (response.hasOwnProperty('shippingAddressGrid')) {
		jQuery("#shipping_address_grid").html(response.shippingAddressGrid);
	}

	if (response.hasOwnProperty('catalogSubscribeMailingDefault')) {
		jQuery("#mailing-address-subscribe-catalog").html(response.catalogSubscribeMailingDefault);
	}

	showSuccessMessage(response['msg']);
}

function accountAddressSuccessEdit(response) {

	var url = response.editUrl;

	if (response.hasOwnProperty('billingDefault')) {
		jQuery("#billing_default").html(response.billingDefault);
	}
	if (response.hasOwnProperty('shippingAddressGrid')) {
		jQuery("#shipping_address_grid").html(response.shippingAddressGrid);
	}

	showSuccessMessage(response['msg']);

	showAjaxModal(url);

}

function addressDeleteAction(url)
{
	makeAjaxRequest(url, false, function(response){

		if(response['status'] == 'success')
		{
			jQuery('.my-account-container').html(response['html']);

			var blocks = response['blocks'];

			if (typeof blocks["shipping_default"] != "undefined"){
				var shippingDefaultHtml = blocks["shipping_default"];
				var shippingGridHtml = blocks["shipping_address_grid"];
				jQuery("#shipping_default").html(shippingDefaultHtml);
				jQuery("#shipping_address_grid").html(shippingGridHtml);
			}
			else if (typeof blocks["billing_default"] != "undefined"){
				var catalogMailingDefaultHtml = blocks["catalog_subscribe_mailing_default"];
				var billingDefaultHtml = blocks["billing_default"];
				jQuery("#mailing-address-subscribe-catalog").html(catalogMailingDefaultHtml);
				jQuery("#billing_default").html(billingDefaultHtml);
			}

			jQuery.modal.close();
			showSuccessMessage(response['msg']);
		}
		else
		{
			//This may be needed again and should be centralized
			var $erorrMessages  = '<ul class="messages"><li class="error-msg"><ul>';
			jQuery.each(response['msg'], function( key, value ) {
				$erorrMessages  += '<li><span>'+value+'</span></li>';
			});
			$erorrMessages  += '</ul></li></ul>';

			jQuery('#modal-content .page-title').after($erorrMessages);
		}
	});
}

function showErrorsInModal(messageList) {
	//This may be needed again and should be centralized
	var $erorrMessages  = '<ul class="messages"><li class="error-msg"><ul>';
	jQuery.each(messageList, function( key, value ) {
		$erorrMessages  += '<li><span>'+value+'</span></li>';
	});
	$erorrMessages  += '</ul></li></ul>';

	jQuery('#modal-content .page-title').after($erorrMessages);
}

function showAccountError(xhr, textStatus, errorThrown) {
	// handle session expiration
	if (xhr.status == '403') {
		window.location = '/customer/account';
	}
	else {
		alert('Please refresh the page and try again.');
	}
}

// insert clickable help icons after inputs with class="help"
function attachHelpIcons() {
	jQuery('.help').each(function(){
		var thisInput = jQuery(this);
		// spawn a new icon if one doesn't exist yet
		if(!thisInput.next().hasClass('help-icon')){
			// add a tooltip if the input element has a data-help-tooltip attribute
			var tooltip = null;
			if(thisInput.attr('data-help-tooltip')){
				tooltip = thisInput.attr('data-help-tooltip');
			}

			//get the parent.
			var parent = thisInput.parent();
			parent.css({position: 'relative'});

			thisInput.after(
				jQuery('<a/>', {
					href: 'javascript:void(0)',
					title: tooltip,
					text: '?',
					tabindex: '-1'
				}).addClass('help-icon')
			);
		}

		thisInput.next('.help-icon').bind("click", function(){
			//chekc for a url.
			if(thisInput.attr('data-url')){
				//poup the url.
				showAjaxModal(thisInput.attr('data-url'));
			}
			if(thisInput.attr('data-content'))
			{
				var content = jQuery(thisInput.attr('data-content'));
				showModal(content.html(), {
					containerCss: {
						width: content.children().first().innerWidth() + 42 // 20px padding + 1px border, on both sides
					}
				});
			}
		});

		// calculate the position for the icon
		var padding = (thisInput.outerHeight() / 2) - (thisInput.next('.help-icon').outerHeight() / 2);
		thisInput.next('.help-icon').css({
			top: padding,
			right: (padding * 1.5)
		});
	});
}

function setUpSelects() {
	// define a closure to properly set color of select element (pass it a jQuery object)
	fixSelects = function(jqSelect){
		var selectVal = jqSelect.val();
		if(selectVal == "" || selectVal == null)
		{
			jqSelect.addClass("empty");
		}
		else
		{
			jqSelect.removeClass("empty");
		}
	};
	// initial styling of every select element on the page
	jQuery('select').each(function(){
		fixSelects(jQuery(this));
	});
	// bind to the change event, so color is always correctly set
	jQuery('select').on('change', function(){
		fixSelects(jQuery(this));
	});
}

/* Detect client and add platform/browser classes to page body for CSS transformation */
function setClientFlags() {
	var flags = [
		(/Constructor/.test(window.HTMLElement)) ? 'safari' : '',				// Safari flag
		(!!window.chrome) ? 'chrome' : '',										// Chrome flag
		(navigator.platform.indexOf('Mac') != -1) ? 'mac' : 'notmac',			// Mac flag
		(navigator.platform.indexOf('Win') != -1) ? 'windows' : 'notwindows',	// Windows flag
		(navigator.platform.indexOf('iP') != -1) ? 'ios' : 'notios',			// iOS flag
		('ontouchstart' in window) ? 'touch' : 'notouch',                          // touch events supported
		('backgroundSize' in document.documentElement.style) ? '' : 'nobgsize'  // background-size not supported flag
	];
	jQuery('body').addClass(flags.join(' '));
}

//Check and see if we need this...
function isUndefined(i) {
	return typeof(i) === 'undefined';
}

jQuery.fn.limitMaxlength = function(options){
	var settings = jQuery.extend({
		attribute: "maxlength",
		onLimit: function(){},
		onEdit: function(){}
	}, options);

	// Create the span for displaying remaining chars after element
	this.after("<span class=\"chars-remaining\"></span>");

	// Event handler to limit the textarea
	var onEdit = function(){
		var textarea = jQuery(this);
		var maxlength = parseInt(textarea.attr(settings.attribute));

		if(textarea.val().length > maxlength){
			textarea.val(textarea.val().substr(0, maxlength));

			// Call the onlimit handler within the scope of the textarea
			jQuery.proxy(settings.onLimit, this)();
		}

		// Call the onEdit handler within the scope of the textarea
		jQuery.proxy(settings.onEdit, this)(maxlength - textarea.val().length);
	};

	this.each(onEdit);

	return this.keyup(onEdit)
		.keydown(onEdit)
		.focus(onEdit);
};

function initErrorMessages()
{
	var $ = jQuery;
	//make the container.
	var msgBackground = $('<div/>', {  'class': 'top-message-background'	});
	var msgWrapper = $('<div/>', {  'class': 'top-message-wrapper'	});
	var msgClose = $('<span/>', {  'class': 'close', title: 'Hide message'	});
	var msgMessages = $('<div/>', {'class': 'message-container'});

	msgWrapper.append(msgClose);
	msgWrapper.append(msgMessages);

	msgBackground.append(msgWrapper);

	//getting lazy here and will store the message type.. assuming there is only one message at a time.
	var msgType = "";

	//get each of the high level message lists.
	var messages = [];
	$( ".messages > li" ).each(function( index ) {

		msgType = "frans-" + $(this).attr('class'); //yep, super lazy, just getting the last message class. Appended "frans-" to be able to isolate the message styles.

		//now get each of the messages within this list.
		$(this).find( "ul > li" ).each(function( index2 ) {

			//alert($( this ).find("span").length);
			var msg = '';
			if($( this ).find("span").length > 0)
			{
				msg = $( this ).find("span").html();
			}
			else
			{
				msg = $( this ).html();
			}
			if (messages.indexOf(msg) == -1){
				messages.push(msg);
			}
		});

	});

	if(messages.length == 0) {
		//get out of here.
		return false;
	}

	// if this page has a custom messages function, use that; otherwise continue with default behavior
	if(typeof(showCustomMessages) === "function") {
		// function should return a messages array to use if further display is wanted, or false to stop immediately
		var customResponse = showCustomMessages(messages);
		if($.isArray(customResponse)){
			// continuing to display messages, so replace existing messages array with new one
			messages = customResponse;
		} else {
			// stop processing here, the custom messages function did everything we want
			return false;
		}
	}

	//build out the new messages html.
	var msgHtml = "<ul>";
	for(var i = 0; i < messages.length; i++)
	{
		//alert();
		msgHtml += "<li>" + messages[i] + "</li>";
	}
	msgHtml += "</ul>";

	msgMessages.html(msgHtml);

	//set the message type to the wrapper.
	msgWrapper.addClass(msgType);

	//find the container where we will add them.
	var main = $(".main-container");

	//add a child.
	main.prepend(msgBackground);

	//fade it in
	msgWrapper.slideDown(); //TODO: we can change this.

	//add a close event to the close button
	msgClose.bind("click", function(){
		msgBackground.slideUp();
	});

}

// adjust heights of rows so their content is not cut off
function fixRowHeights(){
	// TODO: Expand this function to fix regular row heights, not just backdrop rows
	var backdrops = jQuery('.backdrop');
	backdrops.each(function(){
		var content = jQuery(this).find('.banner a > .content').first();
		var contentHeight = content.outerHeight(true);
		jQuery(this).height(contentHeight);
		// function needs to execute again after content images load, as they may change content height
		content.find('img:not(".loaded")').one('load', function(){
			jQuery(this).addClass('loaded');
			fixRowHeights();
		}).each(function(){
			if(this.complete) jQuery(this).load();
		});
	});
	// vertically align elements with class "vertical-align"
	var valigns = jQuery('.vertical-align');
	valigns.each(function(){
		var valign = jQuery(this);
		var valignHeight = valign.outerHeight();
		var parentHeight = valign.parent().height();
		if(parentHeight > valignHeight) {
			var calculatedMargin = (parentHeight / 2) - (valignHeight / 2);
			valign.css({marginTop: calculatedMargin});
		}
	});
}

// Base64 helper object with encode() and decode() functions
var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}};