var stripePending = false;

AdminOrder.prototype.changePaymentData = AdminOrder.prototype.changePaymentData.wrap(
    function(callOriginal, event) {
        var elem = Event.element(event);
        if(elem && elem.method && elem.method == 'stripe') {
            var data = this.getStripePaymentData();
            createStripeToken(true, true); // we always want CVV & Address
        }
        else {
            return callOriginal();
        }
    });

Object.extend(AdminOrder.prototype, {
	getStripePaymentData: function() {
		if (typeof (currentMethod) == 'undefined') {
	        if (this.paymentMethod) {
	            currentMethod = this.paymentMethod;
	        } else {
	            return false;
	        }
	    }
	    var data = {};
	    var fields = $('payment_form_' + currentMethod).select('input', 'select');
	    for ( var i = 0; i < fields.length; i++) {
	        data[fields[i].name] = fields[i].getValue();
	    }

	    if ((typeof data['payment[stripe_customer_id]'])!='undefined') {
			if (data['payment[stripe_customer_id]'] != "" && data['payment[create_stripe_customer]']=="0") {
				return data;
			} 
			if (data['payment[stripe_customer_id]'] !="" && data['payment[create_stripe_customer]']=="1" && data['payment[stripe_token]']=="") {
				return false;
			}
		}		
	    
	    if ((typeof data['payment[cc_type]']) != 'undefined'
	            && (!data['payment[cc_type]'] || !data['payment[cc_number]'])) {
	        return false;
	    }
	    if ((typeof data['payment[cc_cid]']) != 'undefined') {
		    if (data['payment[cc_cid]']=="") {
			    return false;
		    }
	    }
	    
	    if (data['payment[cc_number]']=='' || data['payment[cc_exp_month]'] =="" || data['payment[cc_exp_year]'] == "") {
		    return false;
	    }
	    return data;
    }
});

$$('head').first().insert({
    bottom: new Element('script', {
        type: 'text/javascript',
        src: 'https://js.stripe.com/v1/'
    })
});

function createStripeToken(addr,cvv) {
    var errors = [];
    // make sure any populated fields are valid
    if ($F('stripe_cc_number') && !Stripe.card.validateCardNumber($F('stripe_cc_number')))
    {
        errors.push('Credit Card Number appears to be invalid.');
    }

    if ($F('stripe_expiration') && $F('stripe_expiration_yr')
        && !Stripe.card.validateExpiry($F('stripe_expiration'), $F('stripe_expiration_yr'))
        )
    {
        errors.push('Expiration date is not valid.');
    }

    if ($F('stripe_cc_cid') && !Stripe.card.validateCVC($F('stripe_cc_cid')))
    {
        errors.push('Card Verification Number is invalid.');
    }

    if (errors.length > 0)
    {
        alert(errors.join('\n'));
        return false;
    }

    // make sure we have all the fields we need
    if (!$F('stripe_cc_number')
        || !$F('stripe_cc_cid')
        || !$F('stripe_expiration')
        || !$F('stripe_expiration_yr'))
    {
        return false;
    }

    stripePending = true;
	Stripe.setPublishableKey($F('publishable_key'));
	if (addr && cvv) {
        var selectedStates = $$("#order-billing_address_region_id option:selected");
        var state = (selectedStates.length > 0) ? selectedStates[0].text : $F("order-billing_address_region");
		Stripe.createToken({
			name: $F("order-billing_address_firstname") + " " +  $F("order-billing_address_lastname"),
			address_line1: $F("order-billing_address_street0"),
			address_line2: $F("order-billing_address_street1"),
			//address_state: $F("order-billing_address_region"), // this one works if non-US
            address_state: state,
			address_zip: $F("order-billing_address_postcode"),
			address_country: $F('order-billing_address_country_id'),
			number: $F('stripe_cc_number'),
			cvc: $F('stripe_cc_cid'),
			exp_month: $F('stripe_expiration'),
			exp_year: $F('stripe_expiration_yr')}, stripeResponseHandler);
	} 
	if (!addr&&cvv) {
		Stripe.createToken({
			name: $F("order-billing_address_firstname") + " " +  $F("order-billing_address_lastname"),
			number: $F('stripe_cc_number'),
			cvc: $F('stripe_cc_cid'),
			exp_month: $F('stripe_expiration'),
			exp_year: $F('stripe_expiration_yr')}, stripeResponseHandler);
	}
	if (!addr&&!cvv) {
		Stripe.createToken({
			name: $F("order-billing_address_firstname") + " " +  $F("order-billing_address_lastname"),
			number: $F('stripe_cc_number'),
			exp_month: $F('stripe_expiration'),
			exp_year: $F('stripe_expiration_yr')}, stripeResponseHandler);
	}
	
}

function stripeResponseHandler(status, response) {
    stripePending = false;
    if (response.error) {
        alert(response.error.message);
    } else {
        var token = response['id'];
        $('stripe_token').value = token;
    }
}

function toggleValidation(state) {
	if (!state) {
		$('stripe-change-payment-form').select('input','select').each(function(e){
			if ($(e).hasClassName("stripe-toggle-valid")) {
				$(e).removeClassName('required-entry');
			}
		});
	} else {
		$('stripe-change-payment-form').select('input','select').each(function(e){
			if ($(e).hasClassName("stripe-toggle-valid")) {
				$(e).addClassName('required-entry');
			}
		});
	}
}	

function checkStripeValidation() {
	if ($('stripe_stripe_customer_id')) {
		if ($F("stripe_stripe_customer_id")!='') {
			toggleValidation(false);
		}
	}
	
	if ($("stripe_token")) {
		if ($F('stripe_token')) {
			toggleValidation(false);
		}
	}
}

function toggleStripePaymentFields(e, element) {
    Event.stop(e);
    if ($('stripe-change-payment-form').visible()) {
        $('stripe-change-payment-form').hide();
        $('stripe-payment-details').show();
        $('stripe_create_stripe_customer').value = '0';
        toggleValidation(false);
        $(element).update("Change payment information");
    } else {
        toggleValidation(true);
        $(element).update("Use existing payment information");
        $('stripe-change-payment-form').show();
        $('stripe-payment-details').hide();
        $('stripe_create_stripe_customer').value = "1";
    }
}
