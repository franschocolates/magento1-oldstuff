<?php
$type='TrueType';
$name='TradeGothicNo-18-Condensed';
$desc=array('Ascent'=>722,'Descent'=>170,'CapHeight'=>722,'Flags'=>32,'FontBBox'=>'[-30 -216 1000 950]','ItalicAngle'=>0,'StemV'=>70,'MissingWidth'=>502);
$up=-75;
$ut=50;
$cw=array(
	chr(0)=>502,chr(1)=>502,chr(2)=>502,chr(3)=>502,chr(4)=>502,chr(5)=>502,chr(6)=>502,chr(7)=>502,chr(8)=>502,chr(9)=>502,chr(10)=>502,chr(11)=>502,chr(12)=>502,chr(13)=>502,chr(14)=>502,chr(15)=>502,chr(16)=>502,chr(17)=>502,chr(18)=>502,chr(19)=>502,chr(20)=>502,chr(21)=>502,
	chr(22)=>502,chr(23)=>502,chr(24)=>502,chr(25)=>502,chr(26)=>502,chr(27)=>502,chr(28)=>502,chr(29)=>502,chr(30)=>502,chr(31)=>502,' '=>502,'!'=>333,'"'=>333,'#'=>444,'$'=>444,'%'=>778,'&'=>556,'\''=>222,'('=>278,')'=>278,'*'=>444,'+'=>600,
	','=>222,'-'=>333,'.'=>222,'/'=>278,'0'=>444,'1'=>444,'2'=>444,'3'=>444,'4'=>444,'5'=>444,'6'=>444,'7'=>444,'8'=>444,'9'=>444,':'=>222,';'=>222,'<'=>600,'='=>600,'>'=>600,'?'=>389,'@'=>800,'A'=>444,
	'B'=>500,'C'=>500,'D'=>500,'E'=>444,'F'=>389,'G'=>500,'H'=>500,'I'=>222,'J'=>278,'K'=>444,'L'=>389,'M'=>611,'N'=>500,'O'=>500,'P'=>444,'Q'=>500,'R'=>500,'S'=>444,'T'=>389,'U'=>500,'V'=>444,'W'=>611,
	'X'=>444,'Y'=>444,'Z'=>444,'['=>278,'\\'=>278,']'=>278,'^'=>600,'_'=>500,'`'=>222,'a'=>444,'b'=>444,'c'=>389,'d'=>444,'e'=>389,'f'=>278,'g'=>444,'h'=>444,'i'=>222,'j'=>222,'k'=>389,'l'=>222,'m'=>667,
	'n'=>444,'o'=>389,'p'=>444,'q'=>444,'r'=>278,'s'=>389,'t'=>278,'u'=>444,'v'=>389,'w'=>556,'x'=>333,'y'=>333,'z'=>333,'{'=>278,'|'=>222,'}'=>278,'~'=>600,chr(127)=>502,chr(128)=>502,chr(129)=>502,chr(130)=>222,chr(131)=>444,
	chr(132)=>389,chr(133)=>1000,chr(134)=>500,chr(135)=>500,chr(136)=>222,chr(137)=>1000,chr(138)=>444,chr(139)=>333,chr(140)=>722,chr(141)=>502,chr(142)=>502,chr(143)=>502,chr(144)=>502,chr(145)=>222,chr(146)=>222,chr(147)=>389,chr(148)=>389,chr(149)=>500,chr(150)=>500,chr(151)=>1000,chr(152)=>222,chr(153)=>990,
	chr(154)=>389,chr(155)=>333,chr(156)=>611,chr(157)=>502,chr(158)=>502,chr(159)=>444,chr(160)=>502,chr(161)=>333,chr(162)=>444,chr(163)=>444,chr(164)=>444,chr(165)=>444,chr(166)=>222,chr(167)=>500,chr(168)=>222,chr(169)=>800,chr(170)=>266,chr(171)=>500,chr(172)=>600,chr(173)=>333,chr(174)=>800,chr(175)=>222,
	chr(176)=>400,chr(177)=>600,chr(178)=>266,chr(179)=>266,chr(180)=>222,chr(181)=>444,chr(182)=>500,chr(183)=>222,chr(184)=>222,chr(185)=>266,chr(186)=>233,chr(187)=>500,chr(188)=>666,chr(189)=>666,chr(190)=>666,chr(191)=>389,chr(192)=>444,chr(193)=>444,chr(194)=>444,chr(195)=>444,chr(196)=>444,chr(197)=>444,
	chr(198)=>722,chr(199)=>500,chr(200)=>444,chr(201)=>444,chr(202)=>444,chr(203)=>444,chr(204)=>222,chr(205)=>222,chr(206)=>222,chr(207)=>222,chr(208)=>500,chr(209)=>500,chr(210)=>500,chr(211)=>500,chr(212)=>500,chr(213)=>500,chr(214)=>500,chr(215)=>600,chr(216)=>500,chr(217)=>500,chr(218)=>500,chr(219)=>500,
	chr(220)=>500,chr(221)=>444,chr(222)=>444,chr(223)=>444,chr(224)=>444,chr(225)=>444,chr(226)=>444,chr(227)=>444,chr(228)=>444,chr(229)=>444,chr(230)=>611,chr(231)=>389,chr(232)=>389,chr(233)=>389,chr(234)=>389,chr(235)=>389,chr(236)=>222,chr(237)=>222,chr(238)=>222,chr(239)=>222,chr(240)=>389,chr(241)=>444,
	chr(242)=>389,chr(243)=>389,chr(244)=>389,chr(245)=>389,chr(246)=>389,chr(247)=>600,chr(248)=>389,chr(249)=>444,chr(250)=>444,chr(251)=>444,chr(252)=>444,chr(253)=>333,chr(254)=>444,chr(255)=>333);
$enc='cp1252';
$diff='';
$file='a91d684541f41ebbc53790628f1c9234_trade-gothic.z';
$originalsize=41652;
?>
