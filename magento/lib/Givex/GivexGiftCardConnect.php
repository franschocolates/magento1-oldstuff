<?php
/** 
 * Adapted from Engine's Givex code API, but with config pulled out
 * and some test data removed
 */
class GivexGiftCardConnect 
{
	var $Product; //the master product that is the giftcard
	var $Givex; //givex api class
	var $args; //args for givex action
	var $reference;
	//givex vars
	var $url1;
	var $url2;
	var $url3;
	var $token;	
	var $cardNum;
	var $userId;
	var $userPass;
	var $creds;
	var $amount;
	var $platform;
	/*		
	The user that is setup under 001-Frans Chocolates Office:
	User ID:358827
	Host 1: https://gapi-ca2.givex.com:50081/1.0/trans/
	Host 2: https://gapi-ca1.givex.com:50081/1.0/trans/
	Port: 50081
	*/				
	//default params	
	function __construct(){
		$this->reference = 'new';
		$this->amount = '0';
		$this->port = '50081';		
	}
	
	//init givex api with credentials				
	function init($token, $userId, $userPass, $url){				
		$this->creds = array (
			'token' => $token,
			'user' => $userId,
			'userPasswd' => $userPass,
			'oper' => '1',
			'operPasswd' => '1',	
			'language' => 'en'	
		);
		
		$this->url1 = $url;
		# based on the original code, url2/url3 should be the path without the WSDL file name
		$slashLocation = strrpos($url, '/');
		$this->url2 = substr($url, 0, $slashLocation);
		$this->url3 = substr($url, 0, $slashLocation);
		
		$this->args = array (
			'id' => $this->creds,
			'reference' => $this->reference,
			'givexNumber' => $this->cardNum
		);
		
		try{	
			$this->Givex = new SoapClientTimeout(
			$this->url1,
			array(
				'trace' => 1,
				'location' => $this->url2,
				'uri' => $this->url3,
				'exceptions' => true
			));
		} catch (SoapFault $exception) {
			# TODO: special logging? We don't exactly have access to Magento to log
			$this->connectionError = true;
			# can't throw, it will cause problems.
			return false;
		}
		
		$this->connectionError = false;
		$this->Givex->__setTimeout(10000);
		return true;
	}
	
	function setArgsAmount(){
		$this->args['currency'] = 'USD';
		$this->args['amount'] = $this->amount;
	}
	
	function setRegisterArgs(){
		$this->args = array (
			'id' => $this->creds,
			'reference' => $this->reference,			
			'amount' => $this->amount
		);
	}		
	
	function setAmount($s){
		$this->amount = $s;
	}
	
	function setUrl1($s){
		$this->url1 = $s;
	}
	
	function setUrl2($s){
		$this->url2 = $s;
	}	
	
	function setUrl3($s){
		$this->url3 = $s;
	}		
	
	function setUserId($s){
		$this->userId = $s;
	}
	
	function setUserPass($s){
		$this->userPass = $s;
	}
	
	function setCardNum($s){
		$this->cardNum = $s;
	}	
	
	function setReference($s){
		$s = $s . ' @ ' . date( 'd-m-Y' , time() );
		$this->reference = $s;
	}
	
	function setReferenceCode($s){
		$this->reference = $s;
	}	
	
	/*
	* run a givex action
	*/
	function runAction($action){
		if(empty($this->Givex) || $this->connectionError){
			$res = new stdClass();
			$res->connectionError = true;
			$res->reply = 'noservice.';
			$res->detail = 'Our gift card processor is unavailable at this time. Please try again later, or call us at 206.322.0233.';
			$res->title = 'Error No GiveEx';
					$note = 'Givex was unavailable to run an action.' . "\r\n";
					$note .= 'Date: ' . date("F j, Y, g:i a") . "\r\n";
					$note .= 'Action: ' . $action . "\r\n";
					//send_admin_alert($note);			
			return $res;
		}
		try{
			switch($action){
				case 'RedeemForced':
					$res = $this->Givex->RedeemForced( $this->args );
				break;
				case 'Activate':
					$res = $this->Givex->Activate( $this->args );
				break;
				case 'GetBalance':
					$res = $this->Givex->GetBalance( $this->args );
				break;
				case 'Register':
					$this->setRegisterArgs();
					$res = $this->Givex->Register( $this->args );				
					$this->setCardNum($res->givexNumber);
				break;										
			}		
			return $res;
		} catch (Exception $exception) {
			$code = NULL;
			$res = new stdClass();
			$code = $exception->getMessage();			
			if($code == "Empty reply from server" || is_int( strpos($code , 'Operation timed out after' ) ) ){
				switch($action){
					case 'RedeemForced':
						$res = $this->Givex->Reversal( $this->args );
					break;
					case 'Activate':
						$res = $this->Givex->Reversal( $this->args );
					break;															
				}
				$res->reply = 'Our gift card processor is unavailable at this time. Please try again later, or call us at 206.322.0233.';	

				return $res;
			} 
			else if ($code == 'TransErr' && $action == 'GetBalance') /* transaction error */
			{
				$res->notFound = true;
			}
			
			else{
				$res->reply = $code;
			}
			
			# TODO: should we log somewhere.
			return $res;
		} 
	}
	
	function runTest($test){
		$cardA = '603628174542000056559';
		$cardB = '603628415112000056562';
		$cardC = '603628672332000056573';
		$cardD = '603628442252000056580';
		$cardE = '603628384922000056598';
		$cardF = '603628511272000056605';		
		switch($test){
			case 2:
				# Change the Givex User ID to 98659865.
				# Send an $80.00 forced redemption request to the Givex host using card "A".
				$this->setAmount('80.00');
				$this->setUserId('98659865');
				$this->setCardNum($cardA);
				$this->init();
				$this->setArgsAmount();
				return $this->runAction('RedeemForced');
			break;
			case 3:
				# Change the Givex User Password to 12451245. 
				# Send a $10.00 Activation request to the Givex host using card "A".
				$this->setAmount('10.00');
				$this->setUserPass('12451245');
				$this->setCardNum($cardA);
				$this->init();
				$this->setArgsAmount();
				return $this->runAction('Activate');
			break;
			case 4:
				# Send a $50.00 forced redemption request using Givex number 8295310131.
				$this->setAmount('50.00');
				$this->setCardNum('8295310131');
				$this->init();
				$this->setArgsAmount();
				return $this->runAction('RedeemForced');
			break;
			case 5:
				# Send a $50.00 redemption request using Givex number 6036285698895779763
				$this->setAmount('50.00');
				$this->setCardNum('6036285698895779763');
				$this->init();
				$this->setArgsAmount();
				return $this->runAction('RedeemForced');
			break;
			case 6:
				# Send a $50.00 redemption request using Givex number 60362895184999999994
				$this->setAmount('50.00');
				$this->setCardNum('60362895184999999994');
				$this->init();
				$this->setArgsAmount();
				return $this->runAction('RedeemForced');
			break;
			case 7:
				# Send a $50.00 redemption request using Givex number 6036289518412345678909
				$this->setAmount('50.00');
				$this->setCardNum('6036289518412345678909');
				$this->init();
				$this->setArgsAmount();
				return $this->runAction('RedeemForced');
			break;
			case 8:
				# Send a $10.00 activation request using Givex number  8888881196910438
				$this->setAmount('10.00');
				$this->setCardNum('8888881196910438');
				$this->init();
				$this->setArgsAmount();
				return $this->runAction('Activate');
			break;
			case 9:
				# Send a $20.00 request using Card G.  The card should be swiped
				# NA
				print "<h1>Running " . $this->reference . " " . $action . "</h1>";
				print "NA";
			break;
			case 10:
				# Send a Balance Inquiry request using Card number 'A234-57Q'.		
				$this->setCardNum('A234-57Q');
				$this->init();
				return $this->runAction('GetBalance');
			break;
			case 11:
			# Send a $75.00 Forced Redemption request to the Givex host using card "A".
				$this->setAmount('75.00');
				$this->setCardNum($cardA);
				$this->init();
				$this->setArgsAmount();
				return $this->runAction('RedeemForced');
			break;
			case 12:
				# Send an Activation request on card B for $100.00 
				$this->setAmount('100.00');
				$this->setCardNum($cardB);
				$this->init();
				$this->setArgsAmount();
				$this->runAction('Activate');
				# and on card "D" for $54.00.
				$this->setAmount('54.00');
				$this->setCardNum($cardD);
				$this->init();
				$this->setArgsAmount();
				return $this->runAction('Activate');		
			break;
			case 13:
				# Send a $100 Activation request to the Givex host using card "B".
				$this->setAmount('100.00');
				$this->setCardNum($cardB);
				$this->init();
				$this->setArgsAmount();
				return $this->runAction('Activate');
			break;
			case 14:
				# Send a Balance request to the Givex host using card "A".		
				$this->setCardNum($cardA);
				$this->init();
				return $this->runAction('GetBalance');
			break;
			case 15:
				# Send $20.00 Register request to the Givex host.
				# return card number (givexNumber)
				$this->setAmount('20.00');
				$this->setCardNum(NULL);
				$this->init();
				$this->setArgsAmount();
				return $this->runAction('Register');
			break;
			case 16:
				# Send $10.00 Redemption request to the Givex host with card number from RGT #1.
				# card number should be set in this OBJ from last Register.
				$this->setAmount('10.00');		
				$this->init();
				$this->setArgsAmount();
				return $this->runAction('RedeemForced');
			break;
			case 17:
				# Set the Host 1 in the software to use 149.99.39.157 and the port to 50099. 
				# Set the Host 2 in the software to use 149.99.39.146 using port  A
				# Send a $15.00 redemption request to the Givex host using card "C".	
				# NA
				print "<h1>Running " . $this->reference . " " . $action . "</h1>";
				print "NA";
			break;
			case 18:
				# Configure software to use port B for this test.
				# Send a $ 6.00 redemption request to the Givex host using card "C".
				//$this->setUrl1("https://dev-gapi.givex.com:55081/1.0/trans/gapi_trans.wsdl");
				$this->setUrl2("https://dev-gapi.givex.com:55081/1.0/trans");	
				$this->setUrl3("https://dev-gapi.givex.com:55081/1.0/trans");		
				$this->setAmount('6.00');
				$this->setCardNum($cardC);
				$this->init();
				$this->setArgsAmount();
				return $this->runAction('RedeemForced');
			break;
			case 19:
				# Configure software to use port C for this test.
				# Send a $7.00 Redemption request to the Givex host using card "C".
				//$this->setUrl1("https://dev-gapi.givex.com:56081/1.0/trans/gapi_trans.wsdl"); //was comment out
				$this->setUrl2("https://dev-gapi.givex.com:56081/1.0/trans");			
				$this->setUrl3("https://dev-gapi.givex.com:56081/1.0/trans");						
				$this->setAmount('9.00');
				$this->setCardNum($cardC);
				$this->init();
				$this->setArgsAmount();
				return $this->runAction('RedeemForced');
			break;
			default:
				die('no case!');
			break;
		}
	}		
}


//Drop-in replacement for PHP's SoapClient class supporting connect and response/transfer timeout
//Usage: Exactly as PHP's SoapClient class, except that 3 new options are available:
//	timeout			The response/transfer timeout in milliseconds; 0 == default SoapClient / CURL timeout
//	connecttimeout	The connection timeout; 0 == default SoapClient / CURL timeout
//	sslverifypeer	FALSE to stop SoapClient from verifying the peer's certificate
class SoapClientTimeout extends SoapClient
{
	private $timeout = 0;
	private $connecttimeout = 0;
	private $sslverifypeer = false;	
	public function __construct($wsdl, $options) {
		//"POP" our own defined options from the $options array before we call our parent constructor
		//to ensure we don't pass unknown/invalid options to our parent
		if (isset($options['timeout'])) {
			$this->__setTimeout($options['timeout']);
			unset($options['timeout']);
		}
		if (isset($options['connecttimeout'])) {
			$this->__setConnectTimeout($options['connecttimeout']);
			unset($options['connecttimeout']);
		}
		if (isset($options['sslverifypeer'])) {
			$this->__setSSLVerifyPeer($options['sslverifypeer']);
			unset($options['sslverifypeer']);
		}
		//Now call parent constructor
		parent::__construct($wsdl, $options);
	}

	public function __setTimeout($timeoutms)
	{
		if (!is_int($timeoutms) && !is_null($timeoutms) || $timeoutms<0)
			throw new Exception("Invalid timeout value");

		$this->timeout = $timeoutms;
	}

	public function __getTimeout()
	{
		return $this->timeout;
	}

	public function __setConnectTimeout($connecttimeoutms)
	{
		if (!is_int($connecttimeoutms) && !is_null($connecttimeoutms) || $connecttimeoutms<0)
			throw new Exception("Invalid connecttimeout value");

		$this->connecttimeout = $connecttimeoutms;
	}

	public function __getConnectTimeout()
	{
		return $this->connecttimeout;
	}

	public function __setSSLVerifyPeer($sslverifypeer)
	{
		if (!is_bool($sslverifypeer))
			throw new Exception("Invalid sslverifypeer value");

		$this->sslverifypeer = $sslverifypeer;
	}

	public function __getSSLVerifyPeer()
	{
		return $this->sslverifypeer;
	}

	public function __doRequest($request, $location, $action, $version, $one_way = FALSE)
	{
		if (($this->timeout===0) && ($this->connecttimeout===0))
		{
			// Call via parent because we require no timeout
			$response = parent::__doRequest($request, $location, $action, $version, $one_way);
		}
		else
		{
			// Call via Curl and use the timeout
			$curl = curl_init($location);
			if ($curl === false)
				throw new Exception('Curl initialisation failed');

			$options = array(
				CURLOPT_VERBOSE => false,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_POST => true,
				CURLOPT_POSTFIELDS => $request,
				CURLOPT_HEADER => false,
				CURLOPT_NOSIGNAL => true,	//http://www.php.net/manual/en/function.curl-setopt.php#104597
				CURLOPT_HTTPHEADER => array(sprintf('Content-Type: %s', $version == 2 ? 'application/soap+xml' : 'text/xml'), sprintf('SOAPAction: %s', $action)),
				CURLOPT_SSL_VERIFYPEER => $this->sslverifypeer
				);

			if ($this->timeout>0) {
				if (defined('CURLOPT_TIMEOUT_MS')) {	//Timeout in MS supported? 
					$options[CURLOPT_TIMEOUT_MS] = $this->timeout;		
				} else	{ //Round(up) to second precision
					$options[CURLOPT_TIMEOUT] = ceil($this->timeout/1000);	
				}
			}

			if ($this->connecttimeout>0) {
				if (defined('CURLOPT_CONNECTTIMEOUT_MS')) {	//ConnectTimeout in MS supported? 
					$options[CURLOPT_CONNECTTIMEOUT_MS] = $this->connecttimeout;	
				} else { //Round(up) to second precision
					$options[CURLOPT_CONNECTTIMEOUT] = ceil($this->connecttimeout/1000);	
				}
			}

			if (curl_setopt_array($curl, $options) === false)
				throw new Exception('Failed setting CURL options');

			$response = curl_exec($curl);

			if (curl_errno($curl))
				throw new Exception(curl_error($curl));

			curl_close($curl);
		}

		// Return?
		if (!$one_way)
			return ($response);
	}
}