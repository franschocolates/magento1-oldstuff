var StripeForm = Class.create();
StripeForm.prototype = {
    initialize: function(code, payment) {
        this.code = code;
        this.payment = payment;
    },
    save: function() {
        checkout.setLoadWaiting('payment');
        Stripe.createToken({
            name: $(this.code + '_cc_owner').value,
            number: $(this.code + '_cc_number').value,
            cvc: $(this.code + '_cc_cvc').value,
            exp_month: $(this.code + '_cc_expiration_month').value,
            exp_year: $(this.code + '_cc_expiration_year').value
        }, this.stripeResponse.bind(this));
    },
    stripeResponse: function(status, response) {
        if (response.error) {
            checkout.setLoadWaiting(false);
            alert(response.error.message);
        } else {
            $(this.code + '_token').value = response['id'];
            var request = new Ajax.Request(this.payment.saveUrl, {
                method:'post',
                onComplete: this.payment.onComplete.bind(this.payment),
                onSuccess: this.payment.onSave.bind(this.payment),
                onFailure: checkout.ajaxFailure.bind(checkout),
                parameters: Form.serialize(this.payment.form)
            });
        }
    }
};
