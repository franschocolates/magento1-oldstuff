// automatically set custom price to be '0' if
// an admin / CS rep sets the order type on an item
// to be 'no charge'
if(window.AdminOrder){
    AdminOrder.prototype.setCustomPrice = function(selectBox, id){

        var priceElementId = 'item_custom_price_' + id;
        var useCustomPriceId = 'item_use_custom_price_' + id;
        var categoryId = 'item_' + id + '_category';

        $(useCustomPriceId).checked = true;

        //hmmm...this doesn't seem great...
        if (selectBox.value == 'NO CHARGE'){
            $(priceElementId).show().value = '0.00';
            $(priceElementId).show().disabled = false;
        } else {
            $(priceElementId).show().value = $(priceElementId).defaultValue;
            $(priceElementId).show().disabled = true;
        }
    };
}

productNotes = {
    showForm: function(id){
        $('item_notes_' + id).hide();
        $('item_notes_' + id + '_form_wrapper').show();
    },
    hideForm: function(id){
        $('item_notes_' + id).show();
        $('item_notes_' + id + '_form_wrapper').hide();
    },
    saveNotes: function(container) {
        new Ajax.Request($(this.getFieldId(container, 'form')).action, {
            parameters: Form.serialize($(this.getFieldId(container, 'form')), true),
            loaderArea: container
        });
    },
    getFieldId: function(container, name) {
        return container + '_' + name;
    }
};