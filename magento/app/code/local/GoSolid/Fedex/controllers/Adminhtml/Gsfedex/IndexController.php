<?php
class GoSolid_Fedex_Adminhtml_Gsfedex_IndexController extends Mage_Adminhtml_Controller_action
{
	protected function _initAction() {
		$this->loadLayout();
		return $this;
	}   
 
	public function indexAction() {
		
		$this->_initAction();
		$this->renderLayout();
	}
}