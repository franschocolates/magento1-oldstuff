<?php
class GoSolid_Fedex_IndexController extends Mage_Core_Controller_Front_Action
{
	protected function _initAction() {
		$this->loadLayout();
		return $this;
	}   
 
	public function indexAction() {
		
		$this->_initAction();
		$this->renderLayout();
	}
	
	public function queryCustomerAddressScoreAction()
	{
		$addressId = $this->getRequest()->getParam('id');
		$address = Mage::getModel('sales/order_address')->load($addressId);
		$validator = Mage::getModel('fedex/addressValidator');
		$score = $validator->getAddressScore($address);
		
		echo "Score: $score";
		
		die;
	}
	/* Test timeout function 
       This function simulates a lack of response from a WSDL service so we can
       see if timeouts are functioning as expected */
	public function timeoutAction() {
		$sleepTime = 300;
		Mage::log("setting a timeout for $sleepTime seconds", null, 'fedex-addr-validation.log', true);
		sleep($sleepTime);
		die;
	}
}