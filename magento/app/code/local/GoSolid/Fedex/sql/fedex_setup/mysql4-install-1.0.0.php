<?php

# This script adds the necessary storage for the shipping score on the order address table

$installer = $this; 
$installer->startSetup(); 

$installer->run("ALTER TABLE `{$installer->getTable('sales/order_address')}`
  ADD COLUMN `shipping_score` VARCHAR(10) NULL;
");

$installer->endSetup();
