<?php
class GoSolid_HealthTrack_Model_HealthTrack extends Mage_Core_Model_Abstract 
{
	private $_filesBadPerms = array();
	private $_filesCrc = array();
	
    public function _construct()
    {
        parent::_construct(); 
        $this->_init('healthTrack/healthTrack');
    }
    
    public function getReport()
    {
    	
    	//$this->_scanDisk(); 
    	
    	$report = array(
    		"magento_version" => $this->_getMagentoVersion(),
    		"disk_space_total" => $this->_getDiskSpaceTotal(),
    		"disk_space_free" => $this->_getDiskSpaceFree(),
    		"php_errors_suppressed" => $this->_getPhpErrorsSupressed(),
    		"peak_memory_usage" => $this->_getPeakUsage(),
    		"is_email_working" => $this->_getIsEmailWorking(),
    		"products_with_no_images" => $this->_getProductsWithNoImage(),
    		"cdn_enabled" => $this->_getCDNEnabled(),
    		"file_security_risks" => $this->getFileSecurityRisks(),
    		"file_crc_errors" => $this->getFileCrcErrors(),
    		"visible_product_errors" => $this->_getVisibleProductErrors(),
    	);
    	
    	return $report;
    	
    }
    
    private function _getMagentoVersion()
    {
    	return Mage::getVersion();
    }
	
    
    //TODO: need to work on this one.
    private function _getVisibleProductErrors()
    {
    	//gets all of the products that are visible, in stock assigned to the site, but not in a category.
    	
    }
    
    private function _getDiskSpaceTotal()
    {
    	return disk_total_space("/");
    }
    
	private function _getDiskSpaceFree()
    {
    	return disk_free_space ("/");
    }
    
	private function _getPhpErrorsSupressed()
    {
    	$displayErrors = (ini_get("display_errors") == 1 ? false : true) ;
    	return $displayErrors;
    }
    
    private function _getPeakUsage()
    {
    	return memory_get_peak_usage();
    }
    
    private function _getIsEmailWorking()
    {
	    $mail = Mage::getModel('core/email');
		$mail->setToName('gosolid');
		$mail->setToEmail('mac@gosolid.net');
		$mail->setBody('Test');
		$mail->setSubject('Test');
		$mail->setFromEmail(Mage::getStoreConfig('trans_email/ident_general/email'));
		$mail->setFromName("Test");
		$mail->setType('html');// You can use Html or text as Mail format
		
		try 
		{
			$mail->send();
			return true;
		}
		catch (Exception $e) 
		{
			return false;
		}
    }
    
    private function _getProductsWithNoImage()
    {
    	$collection = Mage::getModel('catalog/product')
			->getCollection()
			->addAttributeToSelect('*')
			->addAttributeToFilter('image', 'no_selection');

		$skus = array();
		foreach ($collection as $c)
		{
			$skus[] = $c->getSku();
		}		
			
		$return = array("count" => $collection->count(), "skus" => implode(",",$skus) );	
			
		return $return;
    }
    
    
    private function _getCDNEnabled()
    {
    	$serverIp = $_SERVER['REMOTE_ADDR'];
    	$cdnEnabled = false;
    	


    	
    	$mediaUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
    	$urlProps = parse_url($mediaUrl);

    	$imageIp = gethostbyname($urlProps["host"]);
    	if($imageIp != $serverIp)
    	{
    		$cdnEnabled = true;
    	}
		
			
    	return $cdnEnabled;
    	
    }
    
    private function _scanDisk()
    {
    	
    	
    	/*
    	 * loop for permissions.
    	 */
    	
    	$path = realpath('.');
	    $objects = new RecursiveIteratorIterator(new WaxRecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
		foreach($objects as $name => $object)
		{
			$pathInfo = pathinfo($name); 
		
			if(substr($pathInfo['basename'],0,1) != ".")
			{
				//echo $name . ":" . $perms . "<br>";
				
		    	$correctPerms = true;
				
				//echo "$name\n";
		    	$perms = substr(sprintf('%o', fileperms($name)), -4);
		   		
		    	if(is_dir($name))
		    	{
		    		if($perms != "0755")
		    		{
		    			$correctPerms = false;
		    		}
		    	}
				if(is_file($name))
		    	{
		    		if($perms != "0644")
		    		{
		    			$correctPerms = false;
		    		}
		    	}
		    	
		    	//append to the bad file list.
		    	if($correctPerms == false)
		    	{
		    		$this->_filesBadPerms[] = array("file" => $name, "perms" => $perms);
		    	}
			}
	    	
		} //done looping for permissions
		
		
		
		/*
		 * Loop for CRC check
		 */
		$dirs = array(
			"app" . DS . "code" . DS . "core",
		);
		
		foreach($dirs as $dir)
		{
			$path = realpath('.') . DS . $dir;
			
			
			
			$checksum = 0;
		    $objects = new RecursiveIteratorIterator(new WaxRecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
			foreach($objects as $name => $object)
			{
				$pathInfo = pathinfo($name); 
			
				if(substr($pathInfo['basename'],0,1) != "." && is_file($name))
				{
					
					
					$file_string = file_get_contents($name);
      				$crc = crc32($file_string);
      				
      				
      				
      				$checksum += $crc;
				}
		    	
			} 
			
			$this->_filesCrc[] = array("dir" => $dir, "crc" => $checksum);
		}//done looping for crc check
		
		//var_dump($this->_filesCrc); die;
		
    }
    
    
    private function getFileSecurityRisks()
    {
    	$atRiskFiles = array();
    	$dirs = array("app","downloader","errors","includes","lib","pkginfo","shell","var");
    	
    	

    	
    	$risks = array(
    		"count" => count($this->_filesBadPerms), "risks" => implode(",", $atRiskFiles)
    	);
    	
    	return $risks;
    	
    }
    private function getFileCrcErrors()
    {
    	$allErrors = array();
    	
    	
    	//loop through the available CRC records. Compare to defaults.
    	foreach($this->_filesCrc as $key => $value)
    	{
    		$sourceCrc = $this->_getCrcValidatorByPath( $value["dir"] );
    		
    		if($sourceCrc != $value["crc"])
    		{
    			$allErrors[] = $value["dir"];
    		}
    	}
    	
    	return array("count" => count($allErrors), "dirs" => implode(",", $allErrors) );
    	
    }
    

    
    private function _getCrcVaidators()
    {
    	return array(
    		array( "dir" => "app\code\core" , "crc" => -89598985960 )
    	);
    }
    
    private function _getCrcValidatorByPath($path)
    {
    	foreach($this->_getCrcVaidators() as $key => $value)
    	{
    		if($value["dir"] == $path)
    		{
    			return $value["crc"];
    		}
    	}
    	
    	return null;
    }
    
}

class WaxRecursiveDirectoryIterator extends RecursiveDirectoryIterator {

	public function hasChildren() { 
		if(substr($this->getFilename(),0,1)== ".") 
		{
			return false; 
		}
		else
		{
			return parent::hasChildren(); 
		}
	}
}