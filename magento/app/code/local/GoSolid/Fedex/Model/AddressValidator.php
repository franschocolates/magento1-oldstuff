<?php
class GoSolid_Fedex_Model_AddressValidator extends Mage_Core_Model_Abstract 
{
	const SCORE_UNKNOWN = "Unknown";
    const DISABLED = "Address validation is disabled.";
	const ADDR_VALIDATION_PREFIX = "Address Validation: ";
	private $_validationServiceWsdl;
	
    public function _construct()
    {
        parent::_construct(); 
        $this->_init('fedex/addressValidator');

        $wsdlBasePath = Mage::getModuleDir('etc', 'GoSolid_Fedex')  . DS . 'wsdl' . DS . 'FedEx' . DS;
        $this->_validationServiceWsdl = $wsdlBasePath . 'AddressValidationService_v4.wsdl';
    }

    public function getAddressScore($address)
    {
        if($this->_getFedexConfigData('validation'))
        {
            try
            {
                /* Test to see if validation is enabled, and evaluate a score
                   based on validation. Otherwise, throw an error */
                $score = $this->_requestAddressValidation($address);
                return $score;
            }
            catch (Exception $e)
            {
                Mage::log($e);
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('fedex')->__(self::ADDR_VALIDATION_PREFIX).$e->getMessage());
            }
        }
        else{
            //check for admin to set message so the user knows it is disable.
            if (Mage::app()->getStore()->isAdmin()) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('fedex')->__(self::DISABLED));
            }
        }


        return self::SCORE_UNKNOWN;
    }
    
    /**
     * Makes request for a specific address
     *
     * @param string $purpose
     * @return mixed
     */
    private function _requestAddressValidation($address)
    {
        $validationRequest = $this->_createValidationRequest($address);
        $requestString = serialize($validationRequest);
        $debugData = array('request' => $validationRequest);
		$foundScore = false;

		try 
        {
			$client = $this->_createSoapClient($this->_validationServiceWsdl);
            $response = $client->addressValidation($validationRequest);
            $debugData['result'] = $response;
			$this->_fedexDebug($debugData);
            return Mage::helper('frans')->getScoreFromAddressValidationResult($address, $response);
        } 
        catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('fedex')->__(self::ADDR_VALIDATION_PREFIX).$e->getMessage());
        	$debugData['result'] = array('error' => $e->getMessage(), 'code' => $e->getCode());
			Mage::logException($e);
		}

		$this->_fedexDebug($debugData);
		
        return self::SCORE_UNKNOWN;
    }

    /**
     * Create soap client with selected wsdl
     *
     * @param string $wsdl
     * @param bool|int $trace
     * @return SoapClient
     */
    protected function _createSoapClient($wsdl, $trace = false)
    {
        try {
            if ($this->_getFedexConfigData('timeout') == 1) {
                $location = Mage::getUrl('gsfedex/index/timeout');
            } else {
                $location = $this->_getFedexConfigFlag('sandbox_mode')
                    ? 'https://wsbeta.fedex.com/web-services/addressvalidation'
                    : 'https://ws.fedex.com/web-services/addressvalidation';
            }
            $timeout = 5;
            ini_set('default_socket_timeout', $timeout);
            $client = new SoapClient($wsdl, array(
                'trace' => $trace,
                'connection_timeout' => $timeout,
                'keep_alive' => false,
            ));
            $client->__setLocation($location);
            return $client;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

	/**
     * Retrieve information from fedex carrier configuration
     *
     * @param   string $field
     * @return  mixed
     */
    private function _getFedexConfigData($field)
    {
        $path = 'carriers/fedex/'.$field;
        return Mage::getStoreConfig($path, $this->getStore());
    }

    /**
     * Retrieve fedex config flag for store by field
     *
     * @param string $field
     * @return bool
     */
    private function _getFedexConfigFlag($field)
    {
        $path = 'carriers/fedex/' . $field;
        return Mage::getStoreConfigFlag($path, $this->getStore());
    }
    
    /**
     * Create a validation request
     *
     * @param $address	address we are validating
     * @return array
     */
    protected function _createValidationRequest($address)
    {
		$regionCode = Mage::getModel('directory/region')->load($address->getRegionId())->getCode();
        
        $validationRequest = array(
            'WebAuthenticationDetail' => array(
                'UserCredential' => array(
                    'Key'      => $this->_getFedexConfigData('key'),
                    'Password' => $this->_getFedexConfigData('password')
        )
            ),
            'ClientDetail' => array(
                'AccountNumber' => $this->_getFedexConfigData('account'),
                'MeterNumber'   => $this->_getFedexConfigData('meter_number')
            ),
            'TransactionDetail' => array('CustomerTransactionId' => $address->getId()),
            'Version' => $this->_getVersionInfo(),
            'InEffectAsOfTimestamp' => date('c'),
         //    'Options' =>			array(
									// 	'CheckResidentialStatus' => false,
									// 	'MaximumNumberOfMatches' => 5,
									// 	'StreetAccuracy' => 'LOOSE',
									// 	'DirectionalAccuracy' => 'LOOSE',
									// 	'CompanyNameAccuracy' => 'LOOSE',
									// 	'ConvertToUpperCase' => 1,
									// 	'RecognizeAlternateCityNames' => 1,
									// 	'ReturnParsedElements' => 1
									// ),
			'AddressesToValidate' => array(
									0 => array(
										'AddressId' => $address->getId(), # for now we are just using our ID
								     	'Address' => array(
								     		'StreetLines' => $address->getStreetFull(),
								           	'PostalCode' => $address->getPostcode(),
								           	'City' => $address->getCity(),
											'Company' => $address->getCompany(), 
											'StateOrProvinceCode' => $regionCode, 
											'CountryCode' => $address->getCountryId(),
										)
									)
									),
									
        );

        #var_dump($validationRequest);
        return $validationRequest;
    }

    private function _getVersionInfo()
    {
		return array(
			'ServiceId' => 'aval', 
			'Major' => '4', 
			'Intermediate' => '0', 
			'Minor' => '0'
		);
    }
    
    /**
     * Log debug data to fedex file
     * Stolen from 
     *
     * @param mixed $debugData
     */
    protected function _fedexDebug($debugData)
    {
        if ($this->_getFedexConfigData('debug')) {
            Mage::getModel('core/log_adapter', 'shipping_fedex.log')
               #->setFilterDataKeys($this->_debugReplacePrivateDataKeys)	# couldn't find any for fedex
               ->log($debugData);
        }
    }
}