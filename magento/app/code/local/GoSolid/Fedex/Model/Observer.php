<?php

class GoSolid_Fedex_Model_Observer 
{
	public function onAddressSave($observer)
	{
		try
		{
			$orderAddress = $observer->getAddress();

			# there is a possibility save would be called multiple times
			# which unfortunately means we would call to fedex multiple times
			# however, there is no good way to avoid this since we may not have an ID
			# to track with to see that we were called before.
			if ($orderAddress 
				&& $orderAddress->getAddressType() == Mage_Sales_Model_Order_Address::TYPE_SHIPPING)
			{
				Mage::log("Attempting to look up fedex score for order.");
				$addressId = $orderAddress->getId();
				
				$validator = Mage::getModel('fedex/addressValidator');
				$score = $validator->getAddressScore($orderAddress);
				Mage::log('Score from observer: ' . $score);
				$orderAddress->setShippingScore($score);
			}
			
		}
		catch (Exception $e)
		{
			Mage::logException($e);
		}
		
		return $observer;
	}
}

?>