<?php

class GoSolid_Banner_Adminhtml_Banner_BannerController extends Mage_Adminhtml_Controller_action
{
	protected function _initAction() {
		$this->loadLayout();
		
		//Optional
		//$this->_setActiveMenu('system/pstadmin');
		//$this->_title($this->__('banner'))->_title($this->__('PST Admin'))->_title($this->__('Event Type Management'));
		
		return $this;
	}   
 
	public function indexAction() {

		$this->_initAction()
			->renderLayout();

	}

	public function editAction() {

		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('banner/banner')->load($id);
		
		Mage::register('banner_data', $model);
		
		if ($model->getId() || $id == 0) {
				$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
				if (!empty($data)) {
					$model->setData($data);
				}

				$this->loadLayout();
				
				//Optional for WYSIWYG must change loading blocks to XML
				//$this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
				//$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
				
				$this->_addContent($this->getLayout()->createBlock('banner/adminhtml_banner_edit'))
					->_addLeft($this->getLayout()->createBlock('banner/adminhtml_banner_edit_tabs'));
					
				$this->renderLayout();

			}
			else
			{
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('banner')->__('Record does not exist'));
				$this->_redirect('*/*/');
			}

	}
	
	
	public function newAction() {
		$this->_forward('edit');

	}
	
	public function deleteAction() {
		$model = Mage::getModel('banner/banner')->load($this->getRequest()->getParam('id'));
		$model->delete();
		$model->save();
		
		Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('banner')->__('Record was successfully deleted'));
		Mage::getSingleton('adminhtml/session')->setFormData(false);

	
		$this->_redirect('*/*/');
		return;
		
	}
	
	public function saveAction() {
		
		
		$session = Mage::getSingleton('adminhtml/session');
		

		
		
		if ($data = $this->getRequest()->getPost()) {
			
			//image.
			$folder = "banners";
			$filename = "image";
			if(isset($_FILES[$filename]['name']) and (file_exists($_FILES[$filename]['tmp_name']))) {
				//save the flyer images
				$path = Mage::getBaseDir('media') . DS . $folder . DS ;
				$uploader = new Varien_File_Uploader($filename);
			    $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png')); // or pdf or anything
			    $uploader->setAllowRenameFiles(false);
			    // setAllowRenameFiles(true) -> move your file in a folder the magento way
			    // setAllowRenameFiles(true) -> move your file directly in the $path folder
			    $uploader->setFilesDispersion(false);   
			 
			   // $uploader->save($path, $_FILES[$filename]['name']);
			    $saveArr = $uploader->save($path, $_FILES[$filename]['name']);
			   
			    $data[$filename] = $folder . '/' .  $saveArr['file'];
			}
			elseif (isset($data[$filename]["delete"]) & $data[$filename]["delete"] == "1")
			{
				$path = Mage::getBaseDir('media') . DS ;
				$path = $path . $data[$filename]["value"] ; 
				unlink($path);  
				$data[$filename] = null;
			}
			else
			{
				unset($data[$filename]);
			}
			
			
			$model = Mage::getModel('banner/banner');
			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));

			try {

				
				
				

				$model->save();

				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('banner')->__('Record was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
				
			}
			catch(Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
			}
			
		}
		
		//on save and edit
		 if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('page_id' => $model->getId(), '_current'=>true));
                    return;
                }
		
		
		//no agency to save.
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('banner')->__('Record not found.'));
        $this->_redirect('*/*/');
		
	}

	
}