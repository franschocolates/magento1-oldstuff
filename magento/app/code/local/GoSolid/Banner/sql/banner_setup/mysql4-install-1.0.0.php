<?php

$installer = $this;
$installer->startSetup();

$sql = <<<EOSQL
CREATE TABLE `gosolid_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `subtitle` varchar(255) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `target` varchar(20) DEFAULT NULL,
  `tooltip` varchar(255) DEFAULT NULL,
  `position` int(10) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `banner_color` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1
EOSQL;

$installer->run($sql);

$installer->endSetup();

