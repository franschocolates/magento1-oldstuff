<?php
// create initial banners

$installer = $this;
$installer->startSetup();

$banner = Mage::getModel('banner/banner');
$banner->setType('homepage_tile')
	->setName('Drinking Chocolate')
	->setTitle('Decadent Drinking Chocolate')
	->setSubtitle('Sip happiness')
	->setImage('banners/iced-chocolate.jpg')
	->setBannerColor(1)
	->setUrl(Mage::getUrl(''))
	->setTarget('_self')
	->setTooltip('Try our drinking chocolate')
	->setPosition(1)
	->setStatus('Enabled');
$banner->save();

$banner = Mage::getModel('banner/banner');
$banner->setType('homepage_tile')
	->setName('Wedding Favors')
	->setTitle('Share Joy')
	->setSubtitle('Elegant favors for your beautiful day')
	->setImage('banners/wedding-favors.jpg')
	->setBannerColor(1)
	->setUrl(Mage::getUrl(''))
	->setTarget('_self')
	->setTooltip('Celebrate with chocolate favors')
	->setPosition(2)
	->setStatus('Enabled');
$banner->save();

$banner = Mage::getModel('banner/banner');
$banner->setType('homepage_tile')
	->setName('Salted Caramels')
	->setTitle('A Legend Is Born')
	->setSubtitle('The Fran\'s salted caramel story')
	->setImage('banners/salted-caramels.jpg')
	->setBannerColor(1)
	->setUrl(Mage::getUrl(''))
	->setTarget('_self')
	->setTooltip('Learn the history of our most popular product')
	->setPosition(3)
	->setStatus('Enabled');
$banner->save();

$banner = Mage::getModel('banner/banner');
$banner->setType('homepage_tile')
	->setName('Business Accounts')
	->setTitle('Beautiful Business')
	->setSubtitle('Gifts to celebrate your business relationships')
	->setImage('banners/business-accounts.jpg')
	->setBannerColor(0)
	->setUrl(Mage::getUrl(''))
	->setTarget('_self')
	->setTooltip('Create a business account for large orders')
	->setPosition(4)
	->setStatus('Enabled');
$banner->save();

$installer->endSetup();