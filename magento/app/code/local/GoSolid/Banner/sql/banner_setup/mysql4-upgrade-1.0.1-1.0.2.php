<?php

$installer = $this;
$installer->startSetup();

// disable existing banners in this position
$previousBanners = Mage::getModel('banner/banner')->getCollection()
	->addFieldToFilter('type', array('eq', 'homepage_tile'))
	->addFieldToFilter('position', array('eq', 3));

foreach($previousBanners as $banner)
{
	$banner->setStatus('Disabled')->save();
}

// create new banner
$banner = Mage::getModel('banner/banner');
$banner->setType('homepage_tile')
	->setName('New Website')
	->setTitle('A Tasty New Website')
	->setSubtitle('New features &amp; online treats')
	->setImage('banners/new-website.jpg')
	->setBannerColor(1)
	->setUrl(Mage::getUrl('new-website'))
	->setTarget('_self')
	->setTooltip('New features &amp; online treats')
	->setPosition(3)
	->setStatus('Enabled');
$banner->save();

$installer->endSetup();