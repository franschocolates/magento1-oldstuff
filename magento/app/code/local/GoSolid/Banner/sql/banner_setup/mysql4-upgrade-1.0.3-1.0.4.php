<?php

$installer = $this;
$installer->startSetup();

// Add HTML to homepage hero banner title
$banner = Mage::getModel('banner/banner')->load('Homepage Hero -- Fall Collection', 'name');
$newTitle = <<<EOHTML
<span class="line-1">Something</span>
<span class="line-2">delicious</span>
<span class="line-3">this way comes</span>
<span class="line-4">and it's time to celebrate Fall</span>
EOHTML;
$banner->setTitle($newTitle)->save();

$installer->endSetup();