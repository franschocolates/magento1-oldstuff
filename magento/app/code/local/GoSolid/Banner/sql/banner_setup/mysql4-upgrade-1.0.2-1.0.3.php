<?php

$installer = $this;
$installer->startSetup();

// create new banner for New Website CMS page
$banner = Mage::getModel('banner/banner');
$banner->setType('new-website')
	->setName('CMS: New Website')
	->setImage('banners/cms-new-website-backdrop.jpg')
	->setBannerColor(1)
	->setTarget('_self')
	->setPosition(1)
	->setStatus('Enabled');
$banner->save();

$installer->endSetup();