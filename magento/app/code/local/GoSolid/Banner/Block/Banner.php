<?php
class GoSolid_Banner_Block_Banner extends Mage_Core_Block_Template
{
	public function getFirstBanner($bannerId)
	{
		$banners =  Mage::getModel('banner/banner')->getBanners($bannerId);

		return $banners->getFirstItem();
	}
	
	public function getBanners($bannerId)
	{
		$banners =  Mage::getModel('banner/banner')->getBanners($bannerId);

		return $banners;
	}
}