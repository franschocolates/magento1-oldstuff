<?php

class GoSolid_Banner_Block_Adminhtml_Banner_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{  
	public function __construct()
  	{
		$this->setId('banner_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('banner')->__('Manage'));
		parent::__construct();
  	}
	

	protected function _beforeToHtml()
  	{

	 $this->addTab('form_section', array(
          	'label'     => Mage::helper('banner')->__('General'),
         	'title'     => Mage::helper('banner')->__('General'),
          	'content'   =>  $this->getLayout()->createBlock('banner/adminhtml_banner_edit_tab_form')->toHtml(),
			'active'    => true
      ));


      return parent::_beforeToHtml();
  }
  
}