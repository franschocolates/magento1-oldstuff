<?php
class GoSolid_Banner_Block_Adminhtml_Banner_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{

		parent::__construct();
		$this->setId('bannerGrid');
		
		//TODO: Set default sort
		//$this->setDefaultSort('id');
		//$this->setDefaultDir('ASC');
		
		//TODO: Optional
		//$this->setSaveParametersInSession(true);
		//$this->setDefaultFilter(array('status' => 'Enabled'));
	}
 
	protected function _prepareCollection()
	{
		$collection = Mage::getModel('banner/banner')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}	
		
		
	protected function _prepareColumns()
  	{

  		$this->addColumn('id', array(
			  'header'    => Mage::helper('banner')->__('ID'),
			  'align'     =>'right',
			  'width'     => '50px',
			  'index'     => 'id',
		  ));
		  
		  $this->addColumn('name', array(
			  'header'    => Mage::helper('banner')->__('Name'),
			  'align'     =>'left',
			  'index'     => 'name',
		  ));
		  
		  
		  $types = Mage::getModel('banner/banner')->getTypes();
		  $this->addColumn('type', array(
			  'header'    => Mage::helper('banner')->__('Type'),
			  'align'     => 'left',
			  'index'     => 'type',
			  'type'  => 'options',
			  'options'	=> $types, //example of how to get options.
		  )); 
		  
		  
		  //text plain
		  $this->addColumn('position', array(
			  'header'    => Mage::helper('banner')->__('Position'),
			  'align'     =>'left',
		      'type'      => 'number',
			  'index'     => 'position',
		  ));
		  
		  //text plain
		  $this->addColumn('url', array(
			  'header'    => Mage::helper('banner')->__('Url'),
			  'align'     =>'left',
			  'index'     => 'url',
		  ));
		  
		  
		  $targets = Mage::getModel('banner/banner')->getTargets();
		  $this->addColumn('target', array(
			  'header'    => Mage::helper('banner')->__('Target'),
			  'align'     => 'left',
			  'index'     => 'target',
			  'type'  => 'options',
			  'options'	=> $targets, //example of how to get options.
		  )); 
		  
		  //status
		  $this->addColumn('status', array(
			  'header'    => Mage::helper('banner')->__('Status'),
			  'align'     => 'left',
			  'width'     => '150px',
			  'index'     => 'status',
			  'type'	  => 'options',
			  'options'   => array(
				  'Enabled' => 'Enabled',
				  'Disabled' => 'Disabled',
			  ),
		  ));
		  
		  /*
		   * EXAMPLE CODE. CAN BE REMOVED.
		   *
		  
		  //text plain
		  $this->addColumn('myname', array(
			  'header'    => Mage::helper('banner')->__('MyName'),
			  'align'     =>'left',
			  'index'     => 'name',
		  ));
		  
		  //date
		  $this->addColumn('mydate', array(
	            'header'    => Mage::helper('banner')->__('MyDate'),
	            'type'      => 'date',
	            'align'     => 'left',
	            'index'     => 'mydate', //change me
	            'gmtoffset' => false
	        ));
	        
	      //options
	      $options = Mage::getModel('banner/[model_lcase]')->getCollection()->setOrder("title", "asc")->toGridOptionArray("id", "title");
		  $this->addColumn('category_id', array(
			  'header'    => Mage::helper('banner')->__('Category'),
			  'align'     => 'left',
			  'index'     => 'category_id',
			  'type'  => 'options',
			  'options'	=> $options, //example of how to get options.
		  )); 
		  
		  //status
		  $this->addColumn('status', array(
			  'header'    => Mage::helper('banner')->__('Status'),
			  'align'     => 'left',
			  'width'     => '150px',
			  'index'     => 'status',
			  'type'	  => 'options',
			  'options'   => array(
				  'Enabled' => 'Enabled',
				  'Disabled' => 'Disabled',
			  ),
		  ));
		  
		  END EXAMPLE CODE
		  */ 
		  

		  return parent::_prepareColumns();
	}
	
	public function getRowUrl($row)
  	{
    	return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  	}
}