<?php

class GoSolid_Banner_Block_Adminhtml_Banner_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

	protected function _prepareForm()
	{

		$form = new Varien_Data_Form(); 
		$this->setForm($form);
		$fieldset = $form->addFieldset('banner_fs', array('legend'=>Mage::helper('banner')->__('Banner')));
	
		
		
		//status
		$types = Mage::getModel("banner/banner")->getTypes();
		$fieldset->addField('type', 'select',array(
			'label'     => Mage::helper('banner')->__('Banner Position'),
			'required'  => true,
			'name'      => 'type',
			'width'     => '600px',
			'options'   => $types,
		));
		
		$fieldset->addField('name', 'text', array(
	          'title'     => Mage::helper('banner')->__('Name'),
			  'label'     => Mage::helper('banner')->__('Name'),
	          'name'      => 'name',
			  'width'     => '300px',
			  'class'     => 'required-entry', 
	          'required'  => true,
			  'note'	  => 'Used as a reference, not displayed on the website'
	      	));

		$fieldset->addField('title', 'text', array(
			'title'     => Mage::helper('banner')->__('Title'),
			'label'     => Mage::helper('banner')->__('Title'),
			'name'      => 'title',
			'width'     => '300px',
			'note'	  => 'Top line of text displayed in the banner (large)'
		));

		$fieldset->addField('subtitle', 'text', array(
			'title'     => Mage::helper('banner')->__('Subtitle'),
			'label'     => Mage::helper('banner')->__('Subtitle'),
			'name'      => 'subtitle',
			'width'     => '300px',
			'note'	  => 'Second line of text displayed in the banner (small)'
		));

		$fieldset->addField('image', 'image', array(
	          'title'     => Mage::helper('banner')->__('Image'),
			  'label'     => Mage::helper('banner')->__('Image'),
	          'name'      => 'image',
			
	      	));

		//banner color
		$bannerColors = Mage::getModel("banner/banner")->getBannerColors();
		$fieldset->addField('banner_color', 'select',array(
			'label'     => Mage::helper('banner')->__('Banner Color'),
			'required'  => true,
			'name'      => 'banner_color',
			'options'   => $bannerColors,
			'note'      => 'Light banner: makes header text dark, Dark banner: makes header text lighter'
		));

	    $fieldset->addField('url', 'text', array(
	          'title'     => Mage::helper('banner')->__('Url'),
			  'label'     => Mage::helper('banner')->__('Url'),
	          'name'      => 'url',
			  'width'     => '300px',
			  //'class'     => 'required-entry', 
	          //'required'  => true,
	      	));  	
	   	
	    //target
	    $targets = Mage::getModel("banner/banner")->getTargets();
		$fieldset->addField('target', 'select',array(
			'label'     => Mage::helper('banner')->__('Target'),
			'required'  => true,
			'name'      => 'target',
			'options'   => $targets,
		));
		
	
		$fieldset->addField('tooltip', 'text', array(
	          'title'     => Mage::helper('banner')->__('Tool Tip'),
			  'label'     => Mage::helper('banner')->__('Tool Tip'),
	          'name'      => 'tooltip',
			  'width'     => '300px',
			  //'class'     => 'required-entry', 
	          //'required'  => true,
	          'note'	  => 'Displayed on mouse over.'
	      	));  
		
	   	$fieldset->addField('position', 'text', array(
          'title'     => Mage::helper('banner')->__('Position'),
		  'label'     => Mage::helper('banner')->__('Position'),
       	  'name'      => 'position',   
	   	  'class'     => 'required-entry validate-zero-or-greater',
          'required'  => true,
		  //'note' => 	 Mage::helper('banner')->__("My Note Here.")
      		));   	
	      	
	      	
		//status
		$fieldset->addField('status', 'select',array(
			'label'     => Mage::helper('banner')->__('Status'),
			'required'  => true,
			'name'      => 'status',
			'options'   => array(
					  		'Enabled' => 'Enabled',
					  		'Disabled' => 'Disabled',
				  ),
		));
		
		
		
		
		/*
	    * EXAMPLE CODE. CAN BE REMOVED.
	    *
		
		//text
		$fieldset->addField('mytext', 'text', array(
	          'title'     => Mage::helper('banner')->__('MyText'),
			  'label'     => Mage::helper('banner')->__('MyText'),
	          'name'      => 'name',
			  //'class'     => 'required-entry', 
	          //'required'  => true,
			  //'note'	  => 'My Note Here',
	      	));
		
	    //yes no
	    $fieldset->addField('myyesno', 'select', array(
		          'title'    	 => Mage::helper('banner')->__('Add To Cart'),
				  'label'    	 => Mage::helper('banner')->__('Add To Cart'),
		          //'class'     => 'required-entry', 
				  //'required'  	=> true,
		          'name'     	 => 'myyesno',
		    	  'checked'=> false,
		    	  'name'  => 'myyesno',
		    	  'options'   => array(
									'1' => 'Yes',
									'0' => 'No',
						  ),
				  'note'	=> Mage::helper('banner')->__("My Note."),

      		));   	
	   
		//money
	   	$fieldset->addField('myprice', 'text', array(
          'title'     => Mage::helper('banner')->__('MyPrice'),
		  'label'     => Mage::helper('banner')->__('MyPrice'),
       	  'name'      => 'myprice',   
	   	  //'class'     => 'required-entry validate-zero-or-greater',
          //'required'  => true,
		  //'note' => 	 Mage::helper('banner')->__("My Note Here.")
      		));
	  
      	//options
      	$options = Mage::getSingleton('event/facility')->getCollection()->setOrder("name", "asc")->toOptionArray("id", "name", "Select");
      	$fieldset->addField('myoptions', 'select', array(
          'label'     => Mage::helper('banner')->__('MyOptions'),
		  'title'     => Mage::helper('banner')->__('MyOptions'),
          //'class'     => 'required-entry',
          //'required'  => true,
          'name'      => 'myoptions',
		  'values'   => $options
      	));
		
		//status
		$fieldset->addField('status', 'select',array(
			'label'     => Mage::helper('banner')->__('Status'),
			'required'  => true,
			'name'      => 'status',
			'options'   => array(
					  		'Enabled' => 'Enabled',
					  		'Disabled' => 'Disabled',
				  ),
		));
		
		// END EXAMPLE CODE
		*/
		
	
	
		if ( Mage::getSingleton('adminhtml/session')->getBannerData() )
		{
	    	$form->setValues(Mage::getSingleton('adminhtml/session')->getBannerData());
	    	Mage::getSingleton('adminhtml/session')->getBannerData(null);
		}
		elseif(Mage::registry('banner_data'))
		{
	    	$form->setValues(Mage::registry('banner_data')->getData());
		}
		
		return parent::_prepareForm();
	}
  
}