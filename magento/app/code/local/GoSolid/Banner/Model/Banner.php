<?php
class GoSolid_Banner_Model_Banner extends Mage_Core_Model_Abstract 
{

    protected $_eventPrefix = 'banner_banner';

    public function _construct()
    {
        parent::_construct(); 
        $this->_init('banner/banner');
    }
	
    
    public function getTypes()
    {
    	$types = array(
		    'businessaccounts_backdrop' => 'Business Accounts Backdrop - 1210W x 600H (id: businessaccounts_backdrop)',
		    'businessaccounts_tile'     => 'Business Accounts Tile Link - 590W x 350H (id: businessaccounts_tile)',
		    'cms_backdrop_default'      => 'CMS Backdrop: DEFAULT - 1210W x 600H (id: cms_backdrop_default)',
		    'contact_backdrop'          => 'Contact Us Backdrop - 1210W x 600H (id: contact_backdrop)',
		    'eventfavors_backdrop'      => 'Event Favors Backdrop - 1210W x 600H (id: eventfavors_backdrop)',
		    'eventfavors_tile'          => 'Event Favors Tile Link - 590W x 350H (id: eventfavors_tile)',
		    'giftcards_backdrop'        => 'Gift Cards Backdrop - 1210W x 600H (id: giftcards_backdrop)',
		    'giftcardbalance_backdrop'  => 'Gift Card Check Balance Backdrop - 1210W x 600H (id: giftcardbalance_backdrop)',
		    'homepage_tile'             => 'Homepage Tile - 590W x 350H (id: homepage_tile)',
    		'homepage_retail'           => 'Homepage Retail Stores Tile - 1190W x 350H (id: homepage_retail)'
    	);

	    // dynamically create banner types based on CMS pages
	    $cmsPages = Mage::getModel('cms/page')->getCollection();
	    foreach($cmsPages as $cmsPage)
	    {
		    $identifier = $cmsPage->getIdentifier();
		    $types[$identifier] = 'CMS Backdrop: ' . $cmsPage->getTitle() . ' - 1210W x 600H (id: ' . $identifier .')';
	    }

	    // alphabetize the array by value before returning it
    	asort($types);

    	return $types;
    }

	public function getTargets()
	{
		$types = array(
			'_self' => 'Same Window',
			'_blank' => 'New Window / Tab',
		);

		return $types;
	}

	public function getBannerColors()
	{
		$bannerColors = array(
			1 => 'Light-Tile',
			0 => 'Dark',
		);

		return $bannerColors;
	}

	public function getBannerColorClass(){
		$color = $this->getBannerColor();
		$allColors = $this->getBannerColors();
		$className = strtolower($allColors[$color]);
		return $className;
	}

    public function getBanners($type)
    {
    	return $this->getCollection()
	    		->addFieldToFilter("type", $type)
	    		->addFieldToFilter("status", "Enabled")
	    		->setOrder("position", "ASC");
    }
    
    public function getImageUrl()
    {
    	return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . str_replace("\\", "/", $this->getImage());
    }
    
}