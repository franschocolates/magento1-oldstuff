<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 10/20/2015
 * Time: 1:57 PM
 *
 * Add verified address from pre-verified mailing address into initial data set's DB table.
 *
 */

$installer = $this;

$installer->startSetup();

$tblCatalogMailingList = $installer->getTable('fransCatalog/fransCatalogMailingList');
$tblTemp = "frans_catalog_mailing_list_verified_address";
$lastRecordDate = "2015-10-21 20:39:18";

$sqlQuery = "
  UPDATE `" . $tblCatalogMailingList . "` AS fcml
      INNER JOIN `" . $tblTemp . "` AS fmit
      ON fcml.address_hash = fmit.address_hash
  SET
      fcml.validated_on = fmit.validated_on,
      fcml.score = fmit.score,
      fcml.verified_street = fmit.verified_street,
      fcml.verified_city = fmit.verified_city,
      fcml.verified_region = fmit.verified_region,
      fcml.verified_region_id = fmit.verified_region_id,
      fcml.verified_postcode = fmit.verified_postcode,
      fcml.verified_country_code = fmit.verified_country_code,
      fcml.verified_address_hash = fmit.verified_address_hash;


  DELETE FROM `" . $tblCatalogMailingList . "` WHERE validated_on IS NULL AND created_at <= '" . $lastRecordDate . "';


  DELETE fcml
    FROM `" . $tblCatalogMailingList . "` fcml JOIN
    (
        SELECT COUNT(*), MAX(id) AS id, MAX(created_at) AS created_at, validated_on, address_hash
        FROM `" . $tblCatalogMailingList . "`
        GROUP BY address_hash
        HAVING validated_on IS NOT NULL
    ) nodelete
    ON fcml.address_hash = nodelete.address_hash
    AND fcml.id <> nodelete.id
    AND fcml.created_at <= '" . $lastRecordDate . "';

  DROP TABLE `" . $tblTemp . "`
  ";


try {
  $installer->run($sqlQuery);
} catch (exception $e ) {
    // do nothing if the table already exist
    Mage::log("Error populating frans_catalog_mailing_list DB table with initial verified address: " . $e->getMessage(), null, 'customerAddress.log');
}

$installer->endSetup();