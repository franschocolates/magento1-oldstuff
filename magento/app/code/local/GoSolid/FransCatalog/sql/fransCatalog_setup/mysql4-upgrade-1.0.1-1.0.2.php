<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 10/20/2015
 * Time: 2:48 PM
 *
 * Create the SQL Report.
 *
 */

/*  Create Category */
$categoryTitle = 'Marketing';
$sqlReportCategoryModel = Mage::getModel('sqlReports/sqlReportCategory');

$sqlReportCategoryModel->load($categoryTitle, 'title');

// Category doesn't exist, create category
if(!$sqlReportCategoryModel->getId()){
    $sqlReportCategoryModel->setTitle($categoryTitle)
            ->setPosition(9);
    
    $sqlReportCategoryModel->save();

    Mage::log("Created SQL Report Category" . $categoryTitle);
}


/* Create Report */
$reportTitle = 'Catalog Mailing List';

$sqlReportModel = Mage::getModel('sqlReports/sqlReport')
                        ->getCollection()
                        ->addFieldToFilter('category_id', $sqlReportCategoryModel->getId())
                        ->addFieldToFilter('title', $reportTitle)
                        ->load();

// Report doesn't exist, create report
if($sqlReportModel->count() == 0){
    $sql = <<<SQLQUERY
        SELECT
            firstname AS 'First Name',
            lastname AS 'Last Name',
            company AS Company,
            verified_street AS Street,
            verified_city AS City,
            verified_region AS Region,
            verified_postcode AS 'Zip / Postal Code',
            verified_country_code AS 'Country Code',
            MAX(score) AS Score
        FROM
            frans_catalog_mailing_list
        WHERE
            CONVERT_TZ(created_at, 'GMT', 'America/Los_Angeles') BETWEEN '{Start Date:date} 00:00:00' AND '{End Date:date} 23:59:59'
            AND score >= {Valid Score:string}
        GROUP BY
            verified_address_hash
        ORDER BY
            region, city, street
SQLQUERY;

    $sqlReport = Mage::getModel('sqlReports/sqlReport')
                        ->setSql($sql)
                        ->setCategoryId($sqlReportCategoryModel->getId())
                        ->setTitle($reportTitle)
                        ->setPosition(2);
    $sqlReport->save();

    Mage::log('Created ' . $reportTitle . ' report with ID of ' . $sqlReport->getId());

} else {
    Mage::log("Could not create as the '$reportByItemTitle' report already exists.");
}