<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 10/9/2015
 * Time: 3:38 PM
 *
 * Create DB table for holding mailing addresses.
 *
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$tblName = $installer->getTable('fransCatalog/fransCatalogMailingList');

try {
    $installer->run(
        "CREATE TABLE `" . $tblName . "` (
            `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
            `entity_type` varchar(255) DEFAULT NULL,
            `entity_id` int(10) unsigned DEFAULT NULL,
            `customer_id` int(10) unsigned DEFAULT NULL,
            `firstname` varchar(255) DEFAULT NULL,
            `lastname` varchar(255) DEFAULT NULL,
            `company` varchar(255) DEFAULT NULL,
            `street` varchar(255) DEFAULT NULL,
            `city` varchar(255) DEFAULT NULL,
            `region` varchar(32) DEFAULT NULL,
            `region_id` int(11) DEFAULT NULL,
            `postcode` varchar(16) DEFAULT NULL,
            `country_code` varchar(3) DEFAULT NULL,
            `address_hash` varchar(255) DEFAULT NULL,
            `created_at` timestamp NULL DEFAULT NULL,
            `validated_on` timestamp NULL DEFAULT NULL,
            `score` int(10) unsigned DEFAULT NULL,
            `verified_street` varchar(255) DEFAULT NULL,
            `verified_city` varchar(255) DEFAULT NULL,
            `verified_region` varchar(3) DEFAULT NULL,
            `verified_region_id` varchar(3) DEFAULT NULL,
            `verified_postcode` varchar(16) DEFAULT NULL,
            `verified_country_code` varchar(3) DEFAULT NULL,
            `verified_address_hash` varchar(255) DEFAULT NULL,
            PRIMARY KEY (`id`),
            INDEX (`address_hash`, `created_at`, `validated_on`)
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1;"
    );

    Mage::log($tblName . " table added.");
} catch (exception $e) {
    // do nothing if the table already exist
    Mage::log("Error adding " . $tblName . ": " . $e->getMessage());
}

$installer->endSetup();