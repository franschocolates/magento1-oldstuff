<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 10/20/2015
 * Time: 1:57 PM
 *
 * Create initial data set from xcart orders, Magento's customers' and orders' billing address.
 *
 */

$installer = $this;

$installer->startSetup();

$tblCatalogMailingList = $installer->getTable('fransCatalog/fransCatalogMailingList');
$tblTemp = "frans_mailinglist_temp";

try {
  $installer->run(
  "CREATE TEMPORARY TABLE IF NOT EXISTS `" . $tblTemp . "`

  SELECT
    'xcart_address' AS entity_type,
    TRIM(orderid) AS entity_id,
    NULL AS customer_id,
    TRIM(b_firstname) AS firstname,
    TRIM(b_lastname) AS lastname,
    TRIM(b_company) AS company,
    TRIM(b_address) AS street,
    TRIM(b_city) AS city,
    TRIM(b_state) AS region,
    NULL AS region_id,
    TRIM(b_zipcode) AS postcode,
    TRIM(b_country) AS country_code,
    created_at,
    MD5(UPPER(CONCAT(TRIM(REPLACE(b_address, '\r\n', '')), TRIM(b_city), TRIM(b_state), TRIM(b_zipcode), TRIM(b_country)))) AS address_hash
  FROM
    `xcart_orders`
  WHERE
    created_at > '2013-9-1'
    AND CHAR_LENGTH(b_address) > 2

  UNION

  SELECT
    'sales_order_address' AS entity_type,
    TRIM(oa.entity_id) AS entity_id,
    TRIM(oa.customer_id) AS customer_id,
    TRIM(oa.firstname) AS firstname,
    TRIM(oa.lastname) AS lastname,
    TRIM(oa.company) AS company,
    TRIM(oa.street) AS street,
    TRIM(oa.city) AS city,
    TRIM(oa.region) AS region,
    TRIM(oa.region_id) AS region_id,
    TRIM(oa.postcode) AS postcode,
    TRIM(oa.country_id) AS country_code,
    o.created_at AS `created_at`,
    MD5(UPPER(CONCAT(TRIM(REPLACE(oa.street, '\r\n', '')), TRIM(oa.city), TRIM(oa.region), TRIM(oa.postcode), TRIM(oa.country_id)))) AS address_hash
  FROM
    `sales_flat_order_address`  oa
  JOIN
    `sales_flat_order` o ON o.entity_id = parent_id
  WHERE
    address_type = 'billing'

  UNION

  SELECT
    'customer_address' AS entity_type,
    TRIM(e.entity_id) AS entity_id,
    TRIM(e.entity_id) AS customer_id,
    TRIM(`firstname`.`value`) AS firstname,
    TRIM(`lastname`.`value`) AS lastname,
    TRIM(`company`.`value`) AS company,
    TRIM(`street`.`value`) AS street,
    TRIM(`city`.`value`) AS city,
    r.code AS region,
    TRIM(`region`.`value`) AS region_id,
    TRIM(`postcode`.`value`) AS postcode,
    TRIM(`country`.`value`) AS country_code,
    billing_entity.created_at AS created_at,
    MD5(UPPER(CONCAT(TRIM(REPLACE(`street`.`value`, '\r\n', '')), TRIM(`city`.`value`), TRIM(r.code), TRIM(`postcode`.`value`), TRIM(`country`.`value`)))) AS address_hash
  FROM

    `customer_entity` AS `e`

    LEFT JOIN `customer_entity_varchar` AS `at_prefix` ON (`at_prefix`.`entity_id` = `e`.`entity_id`) AND (`at_prefix`.`attribute_id` = '4')
    LEFT JOIN `customer_entity_varchar` AS `firstname` ON (`firstname`.`entity_id` = `e`.`entity_id`) AND (`firstname`.`attribute_id` = '5')
    LEFT JOIN `customer_entity_varchar` AS `lastname` ON (`lastname`.`entity_id` = `e`.`entity_id`) AND (`lastname`.`attribute_id` = '7')
    LEFT JOIN `customer_address_entity` AS `billing_entity` ON (`billing_entity`.`parent_id` = `e`.`entity_id`)
    LEFT JOIN `customer_entity_int` AS `default_billing` ON (`default_billing`.`entity_id` = `e`.`entity_id`) AND (`default_billing`.`attribute_id` = '13')

    #company
    LEFT JOIN `customer_address_entity_varchar` AS `company` ON (`company`.`entity_id` = `default_billing`.`value`) AND (`company`.`attribute_id` = '24')

    #address line 1
    LEFT JOIN `customer_address_entity_text` AS `street` ON (`street`.`entity_id` = `default_billing`.`value`) AND (`street`.`attribute_id` = '25')

    #postal code
    LEFT JOIN `customer_address_entity_varchar` AS `postcode` ON (`postcode`.`entity_id` = `default_billing`.`value`) AND (`postcode`.`attribute_id` = '30')

    #city
    LEFT JOIN `customer_address_entity_varchar` AS `city` ON (`city`.`entity_id` = `default_billing`.`value`) AND (`city`.`attribute_id` = '26')

    #state / region
    LEFT JOIN `customer_address_entity_int` AS `region` ON (`region`.`entity_id` = `default_billing`.`value`) AND (`region`.`attribute_id` = '29')

    #country
    LEFT JOIN `customer_address_entity_varchar` AS `country` ON (`country`.`entity_id` = `default_billing`.`value`) AND (`country`.`attribute_id` = '27')

    LEFT JOIN `directory_country_region` r ON r.region_id = `region`.`value`

    WHERE
    (`e`.`entity_type_id` = '1')
    AND `street`.`value` IS NOT NULL

  ;


  INSERT INTO `" . $tblCatalogMailingList . "` (entity_type, entity_id, customer_id, firstname, lastname , company, street, city, region, region_id, postcode, country_code, created_at, address_hash)
  SELECT * FROM `" . $tblTemp . "`;


  DROP TABLE `" . $tblTemp . "`;

  ");
} catch (exception $e ) {
    // do nothing if the table already exist
    Mage::log("Error with initial Catalog Mailing List import: " . $e->getMessage());
}

$installer->endSetup();