<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 10/14/2015
 * Time: 11:51 AM
 */

class GoSolid_FransCatalog_IndexController extends Mage_Core_Controller_Front_Action {

    public function indexAction() {
        // nothing to do here.
    }

    /**
     * Verified not yet verified addresses in the database table
     */
    public function verifyAddressAction() {
        Mage::getModel('fransCatalog/address')->verify();
    }
}