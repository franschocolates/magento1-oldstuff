<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 10/13/2015
 * Time: 1:55 PM
 */

class GoSolid_FransCatalog_Model_FransCatalogMailingList extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('fransCatalog/fransCatalogMailingList');
    }
}