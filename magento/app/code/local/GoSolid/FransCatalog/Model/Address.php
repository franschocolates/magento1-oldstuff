<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 10/15/2015
 * Time: 9:43 AM
 */

class GoSolid_FransCatalog_Model_Address {

    const LOG_FILENAME = 'customerAddress.log';

    private $_mailingListModel;
    private $_addressValidator;
    private $_numberRecordsToProcess;


    function __construct() {
        $this->_mailingListModel = Mage::getModel('fransCatalog/fransCatalogMailingList');
        $this->_addressValidator = Mage::getModel('frans/shipping_addressValidator');

        $this->_numberRecordsToProcess = Mage::getStoreConfig('catalogMailingList/general/numberRecordProcess');
    }

    /**
     * Verify x number of address, that's not yet verify.
     */
    public function verify() {
        $startTime = time();

        $addresses = $this->_getAddressToProcess();

        if($addresses) {    // if there are address to process

            foreach($addresses AS $address) {
                // set address hash
                $address->setAddressHash($this->_hashAddress($address));

                // verify if this address is already validated
                $validatedId = $this->_isExistingValidAddress($address->getAddressHash());

                // if there's an existing valid verified address record, copy the verified address contents to this row.
                if($validatedId) {

                    $validatedAddress = $this->_getValidatedAddress($validatedId);

                    // copy over the verified address
                    $result = $this->_copyVerifiedAddress($address->getId(), $validatedAddress);

                    if($result){
                        $this->_deleteOlderDuplicateRecord($validatedAddress->getId());
                    }

                } else {    // no valid verified address, proceed to validate address with web service like FedEx
                    // get serialized FedEx proposed address with score
                    $this->_addressValidator->storeAddressTypeAndProposedAddress($address);

                    // if there is proposedAddress
                    if(!is_null($address->getProposedAddress())){

                        $this->_setScore($address);
                        $this->_setVerifiedAddress($address);
                        $this->_setVerifiedAddressHash($address);

                        // add verified address to db
                        $this->_addVerifiedAddress($address);
                    } else {
                        // delete invalid address where FedEx doesn't even return a possible proposed address
                        $this->_deleteInvalidAddress($address->getId());
                    }
                }
            }
        }

        echo ("Duration: " . date("i:s", time() - $startTime));
    }

    /**
     * Get the address to process
     *
     * @return bool|Varien_Data_Collection
     */
    private function _getAddressToProcess() {
        $addresses = $this->_mailingListModel
            ->getCollection()
            ->addFieldToFilter('validated_on', array('null' => true))
            ->setOrder('created_at', 'ASC')
            ->setPageSize($this->_numberRecordsToProcess);

        if($addresses->count())
            return $addresses;

        return false;
    }

    /**
     * @param $hash
     * @return bool
     */
    private function _isExistingValidAddress($hash) {
        $existingAddress = $this->_mailingListModel
            ->getCollection()
            ->addFieldToSelect('id')
            ->addFieldToFilter('address_hash', array('eq' => $hash))
            ->addFieldToFilter('validated_on', array('notnull' => '')) // IS NOT NULL
            ->setOrder('validated_on', 'DESC')
            ->getFirstItem();

        if($existingAddress->getId()) {
            return $existingAddress->getId();
        }

        return false;
    }

    /**
     * @param $address
     * @return bool
     */
    private function _addVerifiedAddress($address){

        $id = $address->getId();
        $proposedAddress = $this->_getProposedAddress($address);

        $model = $this->_mailingListModel->load($id);

        $model->setAddressHash($address->getAddressHash())
            ->setValidatedOn(time())
            ->setScore($address->getScore())
            ->setVerifiedStreet($proposedAddress['StreetLines'])
            ->setVerifiedCity($proposedAddress['City'])
            ->setVerifiedRegion($proposedAddress['StateOrProvinceCode'])
            ->setVerifiedRegionId(trim($address->getRegionId()))
            ->setVerifiedPostcode($proposedAddress['PostalCode'])
            ->setVerifiedCountryCode($proposedAddress['CountryCode'])
            ->setVerifiedAddressHash($address->getverifiedAddressHash());

        $verifyAddress = $proposedAddress['StreetLines']
            . " " . $proposedAddress['City']
            . " " . $proposedAddress['StateOrProvinceCode']
            . " " . $proposedAddress['PostalCode']
            . " " . $proposedAddress['CountryCode'];

        try {

            $model->save();

            Mage::log("ID: " . $id . " - Verified address added. " . $verifyAddress, null, self::LOG_FILENAME, true);

            return true;

        } catch (Exception $e) {

            Mage::log("ID: " . $id . " - Failed adding verified address./n" . $e->getMessage(), null, self::LOG_FILENAME, true);

        }

        return false;
    }

    /**
     * @param $id
     * @param $verifiedAddress
     * @return bool
     */
    private function _copyVerifiedAddress($id, $verifiedAddress){
        $model = $this->_mailingListModel->load($id);


        $model->setAddressHash($verifiedAddress->getAddressHash())
            ->setValidatedOn($verifiedAddress->getValidatedOn())
            ->setScore($verifiedAddress->getScore())
            ->setVerifiedStreet($verifiedAddress->getVerifiedStreet())
            ->setVerifiedCity($verifiedAddress->getVerifiedCity())
            ->setVerifiedRegion($verifiedAddress->getVerifiedRegion())
            ->setVerifiedRegionId($verifiedAddress->getVerifiedRegionId())
            ->setVerifiedPostcode($verifiedAddress->getVerifiedPostcode())
            ->setVerifiedCountryCode($verifiedAddress->getVerifiedCountryCode())
            ->setVerifiedAddressHash($verifiedAddress->getVerifiedAddressHash());

            $copiedAddress = $verifiedAddress->getVerifiedStreet()
                            . " " . $verifiedAddress->getVerifiedCity()
                            . " " . $verifiedAddress->getVerifiedRegion()
                            . " " . $verifiedAddress->getVerifiedRegionId()
                            . " " . $verifiedAddress->getVerifiedPostcode()
                            . " " . $verifiedAddress->getVerifiedCountryCode();

        try {

            $model->save();

            Mage::log("ID: " . $id . " - Verified address copied. " . $copiedAddress, null, self::LOG_FILENAME, true);

            return true;

        } catch (Exception $e) {

            Mage::log("ID: " . $id . " - Failed copying address./n" . $e->getMessage(), null, self::LOG_FILENAME, true);

        }

        return false;
    }

    /**
     * @param $id
     * @return bool
     */
    private function _deleteOlderDuplicateRecord($id){
        try {
            $this->_mailingListModel->setId($id)->delete();

            Mage::log("ID: " . $id . " - Delete old address (duplicated) row record with id " . $id, null, self::LOG_FILENAME, true);

            return true;
        } catch (Exception $e) {
            Mage::log("ID: " . $id . " - Failed deleting old address (duplicated) row record with id " . $id . "./n" . $e->getMessage(), null, self::LOG_FILENAME, true);
        }

        return false;
    }

    /**
     * @param $id
     * @return bool
     */
    private function _deleteInvalidAddress($id){
        try {
            $this->_mailingListModel->setId($id)->delete();

            Mage::log("ID: " . $id . " - Deleted invalid address.", null, self::LOG_FILENAME, true);

            return true;
        } catch (Exception $e) {
            Mage::log("ID: " . $id . " - Failed deleting invalid address.", null, self::LOG_FILENAME, true);
        }

        return false;
    }

    /**
     * @param $address
     */
    private function _setVerifiedAddress($address){
        $proposedAddress = $this->_getProposedAddress($address);

        $address->setVerifiedStreet($proposedAddress['StreetLines']);
        $address->setVerifiedCity($proposedAddress['City']);
        $address->setVerifiedRegion($proposedAddress['StateOrProvinceCode']);
        $address->setVerifiedPostcode($proposedAddress['PostalCode']);
        $address->setVerifiedCountryCode($proposedAddress['CountryCode']);
        $address->setVerifiedRegionId($address->getRegionId());

    }

    /**
     * Get proposedAddress' score.
     *
     * Must have a proposed address in $address.
     *
     * @param $address
     * @return mixed
     */
    private function _setScore($address) {
        $proposedAddress = json_decode($address->getProposedAddress(), true);

        $address->setScore($proposedAddress['Score']);
    }

    /**
     * Return proposed address as associate array
     *
     * @param $address
     * @return mixed
     */
    private function _getProposedAddress($address) {
        $proposedAddress = json_decode($address->getProposedAddress(), true);

        return $proposedAddress['Address'];
    }


    /**
     * @param $id
     * @return Varien_Object
     */
    private function _getValidatedAddress($id){
        $address = $this->_mailingListModel->getCollection()
            ->addFieldToFilter('id', array('eq' => $id))
            ->getFirstItem();

        return $address;
    }

    /**
     * @param $address
     * @return string
     */
    private function _setVerifiedAddressHash($address){

        $hashStr = strtoupper(
            trim($address->getVerifiedStreet())
            . trim($address->getVerifiedCity())
            . trim($address->getVerifiedRegion())
            . trim($address->getVerifiedPostcode())
            . trim($address->getVerifiedCountryCode())
        );

        $address->setVerifiedAddressHash(md5($hashStr));

    }


    /**
     * Generate hash based on mailing address
     *
     * @param $data
     * @return string
     */
    private function _hashAddress($data){

        $hashStr = strtoupper(
            trim($data->getStreet())
            . trim($data->getCity())
            . trim($data->getRegion())
            . trim($data->getPostcode())
            . trim($data->getCountryCode())
        );

Mage::log("String to hash: " . $hashStr, null, self::LOG_FILENAME, true);

        return md5($hashStr);

    }
}