<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 10/12/2015
 * Time: 12:08 PM
 */

class GoSolid_FransCatalog_Model_Observer {

    const LOG_FILENAME = 'customerAddress.log';

    private $_mailingListModel;

    function __construct() {
        $this->_mailingListModel = Mage::getModel('fransCatalog/fransCatalogMailingList');
    }

    /**
     * Capture the billing address
     *
     * @param $data
     */
    public function captureAddress($data){

        // If it's not an order, process the billing address.
        // If it's an order, do not process duplicate billing address submission from the same order
        if($data->getEntityId() !== NULL && (!$data->getIsOrder() || !$this->_isExistEntityId($data->getEntityId()))) {
            // convert region id to region code if needed
            if ($data->getRegionId()) {
                $data->setRegion(Mage::getModel('directory/region')->load($data->getRegionId())->getCode());
            }

            // put address into database
            $this->_addAddress($data);
        }
    }


    /**
     * Add new address to table
     *
     * @param $data
     */
    private function _addAddress($data){
        $addressData = array(
            'firstname' => trim($data->getFirstname()),
            'lastname' => trim($data->getLastname()),
            'company' => trim($data->getCompany()),
            'street' => trim($data->getStreet()),
            'city' => trim($data->getCity()),
            'region' => trim($data->getRegion()),
            'region_id' => trim($data->getRegionId()),
            'postcode' => trim($data->getPostcode()),
            'country_code' => trim($data->getCountryId()),
            'created_at' => trim($data->getUpdatedAt()),
            'entity_type' => trim($data->getEntityType()),
            'entity_id' => trim($data->getEntityId()),
            'customer_id' => trim($data->getCustomerId()),
        );

        $mailingAddress = $data->getFirstname()
            . " " . $data->getLastname()
            . " " . $data->getCompany()
            . " " . $data->getStreet()
            . " " . $data->getCity()
            . " " . $data->getRegion()
            . " " . $data->getCountryId();

        $this->_mailingListModel->setData($addressData);

        try {

            $insertId = $this->_mailingListModel->save()->getId();

            Mage::log("Address added to row id " . $insertId . ": " . $mailingAddress, null, self::LOG_FILENAME, true);

            return true;

        } catch (Exception $e){

            Mage::log("Address not added: " . $mailingAddress, null, self::LOG_FILENAME, true);

        }

        return false;
    }

    /**
     * @param $entityId
     * @return bool|mixed
     */
    private function _isExistEntityId($entityId){
        $existingEntityId = $this->_mailingListModel
                ->getCollection()
                ->addFieldToSelect('id')
                ->addFieldToFilter('entity_id', array('eq' => $entityId))
                ->setOrder('validated_on', 'DESC')
                ->getFirstItem();

        if($existingEntityId->getId()) {
            return $existingEntityId->getId();
        }

        return false;
    }

}