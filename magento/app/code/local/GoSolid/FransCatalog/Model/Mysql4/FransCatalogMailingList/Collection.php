<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 10/13/2015
 * Time: 1:59 PM
 */

class GoSolid_FransCatalog_Model_Mysql4_FransCatalogMailingList_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('fransCatalog/fransCatalogMailingList');
    }

    public function toOptionArray($value = "id", $label = "name", $selectText = null)
    {
        $array =  $this->_toOptionArray($value, $label);

        if($selectText != null)
        {
            $array = array('' =>  Mage::helper('extendedOrder')->__("-- %s --", $selectText)) + $array;
        }

        return $array;

    }


    public function toGridOptionArray($value = "id", $label = "name")
    {
        $list = array();


        foreach($this as $item)
        {
            $list[$item->getData($value)] = $item->getData($label);

        }
        return $list;
    }

}