<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 10/13/2015
 * Time: 1:58 PM
 */

class GoSolid_FransCatalog_Model_Mysql4_FransCatalogMailingList extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('fransCatalog/fransCatalogMailingList', 'id');
    }

}