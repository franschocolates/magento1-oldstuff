<?php

class GoSolid_ShippingRules_Adminhtml_ShippingrulesController extends Mage_Adminhtml_Controller_action
{
	protected function _initAction() {
		$this->loadLayout();
		
		//Optional
		//$this->_setActiveMenu('system/pstadmin');
		//$this->_title($this->__('shippingRules'))->_title($this->__('PST Admin'))->_title($this->__('Event Type Management'));
		
		return $this;
	}   
 
	public function indexAction() {

		$this->_initAction()
			->renderLayout();

	}

	public function editAction() {

		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('shippingRules/rules')->load($id);
		
		Mage::register('shippingrules_data', $model);
		
		if ($model->getId() || $id == 0) {
				$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
				if (!empty($data)) {
					$model->setData($data);
				}

				$this->loadLayout();
				
				//Optional for WYSIWYG must change loading blocks to XML
				//$this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
				//$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
				
				$this->_addContent($this->getLayout()->createBlock('shippingRules/adminhtml_shippingrules_edit'))
					->_addLeft($this->getLayout()->createBlock('shippingRules/adminhtml_shippingrules_edit_tabs'));
					
				$this->renderLayout();
			}
			else
			{
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('shippingRules')->__('Record does not exist'));
				$this->_redirect('*/*/');
			}

	}
	
	
	public function newAction() {
		$this->_forward('edit');

	}
	
	public function saveAction() {
		$session = Mage::getSingleton('adminhtml/session');
		if ($data = $this->getRequest()->getPost()) {
		
			$model = Mage::getModel('shippingRules/rules');
			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));

			$allowedModifiers = array("==", "<=", ">=", ">", "<");
				
			//Prep qty rule data for validation
			preg_match_all('/(\d)|(\D)/', $data["qty"], $qtyMatches);
			$ruleQty = implode($qtyMatches[1]);
			$qtyModifier = implode($qtyMatches[2]);

			//Prep weight rule data for validation
			preg_match_all('/(\d)|(\D)/', $data["weight"], $weightMatches);
			$ruleWeight = implode($weightMatches[1]);
			$weightModifier = implode($weightMatches[2]);
				
			//var_dump($qtyModifier, $weightModifier); die;
			
			try {
				
				if($ruleQty == "" | $qtyModifier == "" | !in_array($qtyModifier, $allowedModifiers))
				{
					Mage::throwException(Mage::helper('shippingRules')->__('The quantity is formatted incorrectly.  Please make sure it uses one of the following ( %s ) followed by the desired number.', implode(', ', $allowedModifiers)));
				}
			
				if($ruleWeight == "" | $weightModifier == "" | !in_array($weightModifier, $allowedModifiers))
				{
					Mage::throwException(Mage::helper('shippingRules')->__('The weight is formatted incorrectly.  Please make sure it uses one of the following ( %s ) followed by the desired number.', implode(', ', $allowedModifiers)));
				}
			

				$model->save();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('shippingRules')->__('Record was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
				
			}
			catch(Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
			}
			
		}
		
		//on save and edit
		 if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('page_id' => $model->getId(), '_current'=>true));
                    return;
                }
		
		
		//no agency to save.
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('shippingRules')->__('Record not found.'));
        $this->_redirect('*/*/');
		
	}

	public function deleteAction() {
	
		$session = Mage::getSingleton('adminhtml/session');
		if ($data = $this->getRequest()->getParams()) {

			$rule = Mage::getModel('shippingRules/rules')->load($data['id']);
			
			try {
				$rule->delete();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('shippingRules')->__('The rule %s was successfully deleted.', $rule->getTitle() ));
        		$this->_redirect('*/*/');
        		return;
			}
			catch(Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
			}
		}
		//no agency to save.
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('shippingRules')->__('Record not found.'));
        $this->_redirect('*/*/');
        return;
	
	}
}