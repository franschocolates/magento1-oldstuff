<?php
class GoSolid_ShippingRules_Adminhtml_IndexController extends Mage_Adminhtml_Controller_action
{
	protected function _initAction() {
		$this->loadLayout();
		return $this;
	}   
 
	public function indexAction() {
		
		$this->_initAction();
		$this->renderLayout();
	}
}