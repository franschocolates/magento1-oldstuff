<?php
/**
 * Provides a an option list with all carriers (both active and inactive)
 */
class GoSolid_ShippingRules_Model_CarrierOptions
{
    public function toOptionArray()
    {
        # we do all carriers, so that even if something is added in admin
        # they can filter it out ahead of time
        $carriers = Mage::getSingleton('shipping/config')->getAllCarriers();
        $options = array(
            // need a blank value or else we won't have the ability to clear it.
            array('value' => '', 'label' => 'None')
        );

        foreach ($carriers as $code => $carrier) {
            if (!($carrier instanceof Mage_Shipping_Model_Carrier_Interface))
            {
                Mage::log("Carrier with code $code is not a an instance of Mage_Shipping_Model_Carrier_Interface, can't get methods", Zend_Log::WARN);
                continue;
            }

            $carrierTitle = $carrier->getConfigData('title');

            $carrierName = ($carrierTitle) ? $carrierTitle : $code;

            $options[] = array('value' => $carrier->getCarrierCode(), 'label' => $carrierName);
        }

        return $options;
    }
}