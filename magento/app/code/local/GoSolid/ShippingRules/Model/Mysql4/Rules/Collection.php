<?php
class GoSolid_ShippingRules_Model_Mysql4_Rules_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct() 
    {
        parent::_construct();
        $this->_init('shippingRules/rules');
    }

	public function toOptionArray($value = "id", $label = "name", $selectText = null)
    {
		$array =  $this->_toOptionArray($value, $label);
		
		if($selectText != null)
		{
			$array = array('' =>  Mage::helper('shippingRules')->__("-- %s --", $selectText)) + $array;
		}
		
		return $array;
		
    }
    
    
	public function toGridOptionArray($value = "id", $label = "name")
    {
    	$list = array();
		

    	foreach($this as $item)
    	{
    		$list[$item->getData($value)] = $item->getData($label);	
    	
    	}       
		return $list;
    }
    
}