<?php
class GoSolid_ShippingRules_Model_Shipping extends Mage_Shipping_Model_Shipping
{
    protected $_fixedRatesCarriers = array();
    protected $_remoteIp;
    protected $_testIp;


    function __construct()
    {
        $this->_remoteIp = Mage::helper('core/http')->getRemoteAddr();
        $this->_testIp = Mage::getStoreConfig('shipping/rules/test_ip');
        // store our fixed rate carriers
        // so we don't look them up multiple times

        if ($fixedRateCarrierList = $this->_getConfig('fixed_rate_carriers'))
        {
            $this->_fixedRatesCarriers = explode(',', $fixedRateCarrierList);
            Mage::log("Fixed rate carriers: " . print_r($this->_fixedRatesCarriers, true));
        }
    }

    /**
     * Collect rates of given carrier
     *
     * @param string $carrierCode
     * @param Mage_Shipping_Model_Rate_Request $request
     * @return Mage_Shipping_Model_Shipping
     */
    public function collectCarrierRates($carrierCode, $request)
    {
        $carrier = $this->getCarrierByCode($carrierCode, $request->getStoreId());
        if (!$carrier) {
            return $this;
        }
        
        $carrier->setActiveFlag($this->_availabilityConfigField);
        $result = $carrier->checkAvailableShipCountries($request);
        if (false !== $result && !($result instanceof Mage_Shipping_Model_Rate_Result_Error)) {
            $result = $carrier->proccessAdditionalValidation($request);
        }
        /*
        * Result will be false if the admin set not to show the shipping module
        * if the devliery country is not within specific countries
        */
        if (false !== $result){
            if (!$result instanceof Mage_Shipping_Model_Rate_Result_Error) {
                // goSolid - begin changes for fixed rate carriers
                if (in_array($carrierCode, $this->_fixedRatesCarriers))
                {
                    Mage::log("Carrier $carrierCode is fixed rate, assuming enabled methods.");
                    $this->_outputTestMessage("Carrier %s is fixed rate, assuming enabled methods.", $carrierCode);
                    $result = $this->_getFixedRateResponse($carrier);
                }
                else
                {
                    // default Magento
                    $result = $carrier->collectRates($request);
                }
                // end goSolid changes for fixed rates

                if (!$result) {
                    return $this;
                }
            }
            if ($carrier->getConfigData('showmethod') == 0 && $result->getError()) {
                return $this;
            }
            // sort rates by price
            if (method_exists($result, 'sortRatesByPrice')) {
                $result->sortRatesByPrice();
            }
            
			//Run the GoSolid Shipping Rules
			try
			{
                $result = $this->_filterOutAdminMethods($carrierCode, $result);

      			$result = Mage::getModel('shippingRules/rules')->validateRules($carrierCode, $request, $result);
			}
			catch (Exception $e)
			{
				Mage::log(
                    sprintf('Shipping Rules Error: %s had a problem processing it\'s rules for country %s, region, %s and postcode %s.'
                    , $carrierCode, $request->getDestCountryId(),$request->getDestRegionId(),$request->getDestPostcode())
                );
			}
       		$this->getResult()->append($result);
        }
        
        return $this;
    }

    /**
     * @param $carrier Mage_Shipping_Model_Carrier_Interface
     *
     * @result Mage_Shipping_Model_Rate_Result
     */
    protected function _getFixedRateResponse($carrier)
    {
        $result = Mage::getModel('shipping/rate_result');

        // make separate request for Smart Post method
        $allowedMethods = $carrier->getAllowedMethods();

        foreach ($allowedMethods as $methodName => $methodTitle)
        {
            $rate = Mage::getModel('shipping/rate_result_method');
            $rate->setCarrier($carrier->getCarrierCode());
            $rate->setCarrierTitle($carrier->getConfigData('title'));
            $rate->setMethod($methodName);
            $rate->setMethodTitle($methodTitle);
            $rate->setCost(0); // we trust our shipping rules will work
            $rate->setPrice(0); // also trust the price to be set.
            $result->append($rate);
        }

        return $result;
    }

    /**
     * @param string $carrierCode
     * @param Mage_Shipping_Model_Rate_Result $result
     */
    private function _filterOutAdminMethods($carrierCode, $result)
    {
        $isAdmin = Mage::app()->getStore()->isAdmin();
        if ($isAdmin || !($adminOnlyConfig = $this->_getConfig('adminOnlyMethods')))
        {
            return $result;
        }

        // split up our admin rates.
        $adminOnlyMethods = explode(',', strtolower($adminOnlyConfig));

        if (count($adminOnlyMethods) == 0)
        {
            return $result;
        }

        // now filter against this carrier code / method
        $allRates = $result->getAllRates();
        $result->reset();

        foreach ($allRates as $rate)
        {
            $fullMethod = strtolower($carrierCode . '_' . $rate->getMethod());
            if (!in_array($fullMethod, $adminOnlyMethods))
            {
                $result->append($rate);
            }
            else
            {
                $this->_outputTestMessage("Method %s filtered to admin only, removing", $fullMethod);
            }
        }

        return $result;
    }

    protected function _outputTestMessage($format, $additional = '')
    {
        if ($this->_isTestMode())
        {
            echo " <hr><h2>". Mage::helper('shippingRules')->__($format, $additional)."</h2>";
        }
    }

    protected function _isTestMode()
    {
        return ($this->_remoteIp == $this->_testIp)
                && (Mage::app()->getRequest()->getControllerName() == 'cart');
    }

    protected function _getConfig($key)
    {
        return Mage::getStoreConfig("shipping/rules/$key");
    }
}