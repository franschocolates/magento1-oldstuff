<?php

abstract class GoSolid_ShippingRules_Model_Rule_Abstract extends Varien_Object
{
	public function init($params)
	{
		if ($params)
		{
			$decodedParams = json_decode($params);
			
			if ($decodedParams != null)
			{
				foreach ($decodedParams as $fieldName => $value)
				{
					$this->setData($fieldName, $value);
				}
			}
		}
	}
	
	public abstract function validateRule($request, $result);
	
	public abstract function getDisplayName();
	
}