<?php

class GoSolid_ShippingRules_Model_Rule_Options
{
	const PREFIX = "GoSolid_ShippingRules_Model_Rule_";
	
	public function getOptionArray()
	{
		$dirSearch = __DIR__ . '/*.php';
		
        $options = array();
        
        $options[''] = 'None';
        
        // find all files in the same directory as our class
        // that aren't either the abstract or this class
        foreach(glob($dirSearch) as $fileName) {
            $pathParts = pathinfo($fileName);
            $className = $pathParts['filename'];
            
            if ($className != 'Abstract' && $className != 'Options')
            {
            	$fullClassName = self::PREFIX . $className;
            	$instance = new $fullClassName();
				
            	if ($instance)
            	{
            		$options[get_class($instance)] = $instance->getDisplayName();
            	}
            	
            }
        }

        return $options;
		
	}
}