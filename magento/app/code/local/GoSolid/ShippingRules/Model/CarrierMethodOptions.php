<?php

/*
 * Helper class to build the options for selecting which carriers are admin only
 * 
 */

class GoSolid_ShippingRules_Model_CarrierMethodOptions
{
    public function toOptionArray()
    {
        # we do all carriers, so that even if something is added in admin
        # they can filter it out ahead of time
        $carriers = Mage::getSingleton('shipping/config')->getAllCarriers();
        $options = array();

        // we want it as an optiongroup
        // so we have an array of arrays with the values
        foreach ($carriers as $code => $carrier) {
            if (!($carrier instanceof Mage_Shipping_Model_Carrier_Interface))
            {
                Mage::log("Carrier with code $code is not a an instance of Mage_Shipping_Model_Carrier_Interface, can't get methods", Zend_Log::WARN);
                continue;
            }

            $carrierMethods = array();
            $carrierTitle = $carrier->getConfigData('title');

            $carrierName = ($carrierTitle) ? $carrierTitle : $code;

            foreach ($carrier->getAllowedMethods() as $methodName => $methodLabel)
            {
                $value = implode('_', array($code, $methodName));
                $label = "$carrierName - $methodLabel";
                $carrierMethods[] = array('value' => $value, 'label' => $label);
            }


            $options[] = array('value' => $carrierMethods, 'label' => $carrierName);
        }

        return $options;
    }
}
