<?php
/**
* @method string getQty()
* @method GoSolid_ShippingRules_Model_Rules setQty(string $value)
* @method string getSubtotal()
* @method GoSolid_ShippingRules_Model_Rules setSubtotal(string $value)
 *
 */
class GoSolid_ShippingRules_Model_Rules extends Mage_Core_Model_Abstract
{
	const REMOVE_RATE_YES	= 1;
	const REMOME_RATE_NO 	= 2;

    const VALUE_ANY = '*';

    public function _construct()
    {
        parent::_construct(); 
        $this->_init('shippingRules/rules');
    }
	
    
    public function validateRules($carrier, $request, $result)
    {
    	$rules = $this->_getRules();	
    	$rates = $result->getAllRates();
    	$result->reset();
    	
    	$customerGroup = $this->_getCurrentCustomerGroup();
    	
    	$remoteIp = Mage::helper('core/http')->getRemoteAddr();
 		$testIp = Mage::getStoreConfig('shipping/rules/test_ip');
		$isTest = false;
		$hasRule = false;
    	$controllerName = Mage::app()->getRequest()->getControllerName();
	   	//Live debugging logic
		if($remoteIp == $testIp && $controllerName == 'cart')
		{
    		$isTest = true;
    		echo " <hr><h2>". Mage::helper('shippingRules')->__("Testing Rules for carrier %s", $carrier)."</h2>";
 		}
    	
    	foreach ($rates as $rate)
    	{
    		$removeRate = false;
    		$method = $rate->getMethod();
			
    		//Loop rules against $carrier
            /* @var GoSolid_ShippingRules_Model_Rules $rule */
    		foreach ($rules as $rule) {
    			
    			//Validate Customer Group
    			if($rule->_validateCustomerGroup($customerGroup) == false)
    			{
    				continue;
    			}
    			
    			//Validate Carrier
    			if($rule->_validateCarrier($carrier) == false)
    			{
    				continue;
    			}
    			
    			//Validaete Carrier Method
    			if($rule->_validateCarrierMethod($method) == false)
    			{
    				continue;
    			}
  
    			//Validate Allowed Countries
    			if($rule->_validateCountry($request->getDestCountryId()) == false)
    			{
    				continue;
    			}
 
    			//Validate Allowed Region
    			if($rule->_validateRegion($request->getDestRegionId()) == false)
    			{
    				continue;
    			}
    			
    			//Validate Allowed Postal Code
    			if($rule->_validatePostalCode($request->getDestPostcode()) == false)
    			{
    				continue;
    			}

    			//Validate Qty
    			if($rule->_validateQty($request->getPackageQty()) == false)
    			{
    				continue;
    			}
    			
    			//Validate Weight
    			if($rule->_validateWeight($request->getPackageWeight()) == false)
    			{
    				continue;
    			}

                //Validate subtotal
                // see Quote_Address requestShippingRates; it sets PackageValue to be subtotal
                if($rule->_validateSubtotal($request->getPackageValue()) == false)
                {
                    continue;
                }

    			if ($rule->_validateRuleCode($request, $result) == false)
    			{
    				continue;
    			}
    		
    			//set the first rules value for this method and stop
    			$rulePrice = $rule->_getRulePrice($rate->getPrice());
    			
    			//Live debugging logic
    			if($isTest == true)
    			{
	    			echo Mage::helper('shippingRules')->__('<br><strong>Shipping Rule:</strong> %s',$rule->getTitle() );
	    			echo Mage::helper('shippingRules')->__('<br><strong>Carrier:</strong> %s', $carrier );
	    			echo Mage::helper('shippingRules')->__('<br><strong>Carrier Method:</strong> %s', $method );
	    			echo Mage::helper('shippingRules')->__('<br><strong>Position:</strong> %s', $rule->getPosition() );
	    			echo Mage::helper('shippingRules')->__('<br><strong>Remove Rate:</strong> %s <br><br> ',( $rule->getRemoveRate() == 2 ? Mage::helper('shippingRules')->__('NO') : Mage::helper('shippingRules')->__('YES') ) );
	    			echo Mage::helper('shippingRules')->__('<br><strong>Original Price</strong> $%s', $rate->getPrice() );
	    			echo Mage::helper('shippingRules')->__('<br><strong>Adjusted Price:</strong> $%s <br><br> ',$rulePrice );
    			}
    			
    			$hasRule = true;
    			if($rule->getRemoveRate() == GoSolid_ShippingRules_Model_Rules::REMOVE_RATE_YES)
    			{
    				$removeRate = true;
    			}
    			else
    			{
    				$rate->setPrice($rulePrice);
    			}
    			
    			break;
    		}
    		if($removeRate == false)
    		{
    			$result->append($rate);
    		}
    	}
    	
    	if($hasRule == false && $isTest == true)
    	{
    		echo "<br><strong>".Mage::helper('shippingRules')->__('No rules match this carrier shipping request.')."</strong>";
    	}

    	return $result;

    }
    
    
    private function _validateCustomerGroup($customerGroup)
    {
    	$allowedCustomerGroups = strtolower( $this->getCusomerGroups() );
    	//var_dump($customerGroup, $allowedCustomerGroups );
    	
    	$isCustomerGroupAllowed = true;
    	if($allowedCustomerGroups != '*')
    	{
    		$allowedCustomerGroups = explode(',', $allowedCustomerGroups);
    		
    		if(in_array( strtolower( $customerGroup ), $allowedCustomerGroups) == false)
    		{
    			$isCustomerGroupAllowed = false;
    		}
    	}
    	//var_dump($isCustomerGroupAllowed); echo "<hr>";
    	return $isCustomerGroupAllowed;
    }
    
    private function _validateCarrier($carrier)
    {
    	$allowedCarriers =  strtolower( $this->getCarrier() );
    	//var_dump($allowedCarriers);
    	
    	$isCarrierAllowed = true;
    	if($allowedCarriers != '*')
    	{
    		$allowedCarriers = explode(',', $allowedCarriers);
    		
    		if(in_array( strtolower( $carrier ), $allowedCarriers) == false)
    		{
    			$isCarrierAllowed = false;
    		}
    	}
    	//var_dump($isCarrierAllowed); echo "<hr>";
    	return $isCarrierAllowed;
    }
    
    
    private function _validateCarrierMethod($method)
    {
    	$allowedCarriersMethods =  strtolower( $this->getCarrierShipCode() );
    	//var_dump($allowedCarriersMethods, strtolower( $method ));
    	
    	$isCarrierMethodAllowed = true;
    	if($allowedCarriersMethods != '*')
    	{
    		$allowedCarriersMethods = explode(',', $allowedCarriersMethods);
    		
    		if(in_array( strtolower( $method ), $allowedCarriersMethods) == false)
    		{
    			$isCarrierMethodAllowed = false;
    		}
    	}
    	//var_dump($isCarrierMethodAllowed); echo "<hr>";
    	return $isCarrierMethodAllowed;
    }
    
    
    private function _validateCountry($countryId)
    {
    	$allowedCountries =  strtolower( $this->getAllowedCountries() );
    	//var_dump($allowedCountries, $countryId);
    	
    	$isCountriesAllowed = true;
    	if($allowedCountries != '*')
    	{
    		$allowedCountries = explode(',', $allowedCountries);
    		
    		if(in_array( strtolower( $countryId ), $allowedCountries) == false)
    		{
    			$isCountriesAllowed = false;
    		}
    	}
    	//var_dump($isCountriesAllowed); echo "<hr>";
    	return $isCountriesAllowed;
    }
    
    
    private function _validateRegion($regionId)
    {
    	$isRegionAllowed = true;
    	
    	//This is the case for most international address
    	if($regionId != null)
    	{
    		$regionCode =   strtolower( Mage::getModel('directory/region')->load($regionId)->getCode() );
	    	$allowedRegions =  strtolower( $this->getAllowedRegions() );
	    	//var_dump($allowedRegions, $regionCode);

	    	if($allowedRegions != '*')
	    	{
	    		$allowedRegions = explode(',', $allowedRegions);
	    		
	    		if(in_array($regionCode, $allowedRegions) == false)
	    		{
	    			$isRegionAllowed = false;
	    		}
	    	}
	    	//var_dump($isRegionAllowed); echo "<hr>";
    	}
	    return $isRegionAllowed;
    }
    
    
    private function _validatePostalCode($postalCode)
    {
    	$allowedPostalCode =  strtolower( $this->getAllowedPostalCodes() );
    	//var_dump($allowedPostalCode, $postalCode);
    	
    	$isPostalCodeAllowed = true;
    	if($allowedPostalCode != '*')
    	{
    		$allowedPostalCode = explode(',', $allowedPostalCode);
    		
    		if(in_array( strtolower( $postalCode ), $allowedPostalCode) == false)
    		{
    			$isPostalCodeAllowed = false;
    		}
    	}
    	//var_dump($isPostalCodeAllowed); echo "<hr>";
    	return $isPostalCodeAllowed;
    }
    
    private function _getCurrentCustomerGroup()
    {
    	//Could move to shipping override to call only once
    	
    	$customer = Mage::getSingleton('customer/session')->getCustomer();
    	
    	if($customer == null)
    	{
    		$customerId = Mage::getSingleton('adminhtml/session_quote')->getCustomerId();
    		$customer = Mage::getModel('customer/cusomter')->load($customerId);
    	}
    	
    	$customerGroup  =  Mage::getModel('customer/group')->load($customer->getGroupId() )->getCustomerGroupCode();

    	return $customerGroup;
    }


    private function _validateSubtotal($subtotal)
    {
        $subtotalFilter = $this->getSubtotal();

        // if we don't have a subtotal filter or it is set to the value of Any
        // used for backwards compatibility since we added this later
        if (!$subtotalFilter || $subtotalFilter == self::VALUE_ANY)
        {
            return true;
        }


        preg_match_all('/(\d)|(\D)/', $subtotalFilter, $matches);

        $ruleSubtotal = implode($matches[1]);
        $modifier = implode($matches[2]);

        $mathString = "return ($subtotal $modifier $ruleSubtotal);";
        return eval($mathString);
    }

    private function _validateQty($qty)
    {
    	preg_match_all('/(\d)|(\D)/', $this->getQty(), $matches);

		$ruleQty = implode($matches[1]);
		$modifier = implode($matches[2]);
		
		$mathString = "if(".$qty." ".$modifier." ".$ruleQty."){ return true; }else{return false;};";
		//var_dump($mathString); var_dump(eval($mathString));
		return eval($mathString);
    }
    
    private function _validateWeight($weight)
    {
    	preg_match_all('/(\d)|(\D)/', $this->getWeight(), $matches);

		$ruleWeight = implode($matches[1]);
		$modifier = implode($matches[2]);
		
		$mathString = "if(".$weight." ".$modifier." ".$ruleWeight."){ return true; }else{return false;};";
		//var_dump($mathString, eval($mathString));
		return eval($mathString);
    }
    
    private function _validateRuleCode($request, $result)
    {
    	
    	if ($ruleClassName = $this->getRuleClassName())
    	{
    		if (class_exists($ruleClassName))
    		{
    			$ruleParams = $this->getRuleClassParams();
    			$shippingRule = new $ruleClassName();
    			$shippingRule->init($ruleParams);

    			return $shippingRule->validateRule($request, $result);
    		}
    		else
    		{
    			Mage::log("Shipping rule is configured to use a class that does not exist! Class name: $ruleClassName", 
    						Zend_Log::WARN);
    		}
    	}
    	
    	// no rule specified, it should pass (so that we don't affect the other rules)
    	return true;
    }
    
    private function _getRules()
    {
    	$rules = $this->getCollection()->setOrder('position','ASC');
    	
    	return $rules;
    }
    
    private function _getRulePrice($price)
    {
    	$rulePrice = $this->getFlatRate();
    	
    	if($rulePrice == null)
    	{
    		$rulePercent = $this->getPercentRate();
    		if ($rulePercent != null)
    		{
    			$rulePrice =  $price * ($rulePercent / 100);
    		}
    		else
    		{
    			$rulePrice = $price;
    		}
    	}
    	return $rulePrice;
    }
    
    
    public function save()
    {
    	if($this->getFlatRate() == "")
    	{
    		$this->setFlatRate(null);
    	}
    	
    	if($this->getPercentRate() == "")
    	{
    		$this->setPercentRate(null);
    	}
    	
    	return parent::save();
    }
}






