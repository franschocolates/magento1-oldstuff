<?php
 
class GoSolid_ShippingRules_Block_Adminhtml_Shippingrules extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {

	$this->_controller = 'adminhtml_shippingrules';
	$this->_blockGroup = 'shippingRules';
	$this->_headerText = Mage::helper('shippingRules')->__("Shipping Rules");
	$this->_addButtonLabel = Mage::helper('shippingRules')->__("Add New Rule");
	
    parent::__construct();
    
  }
}