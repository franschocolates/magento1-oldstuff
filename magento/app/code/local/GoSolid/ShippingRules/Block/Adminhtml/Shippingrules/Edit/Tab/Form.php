<?php

class GoSolid_ShippingRules_Block_Adminhtml_Shippingrules_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	
	protected function _prepareForm()
	{

		$form = new Varien_Data_Form(); 
		$this->setForm($form);
		
		$fieldset = $form->addFieldset('shippingRules_fs', array('legend'=>Mage::helper('shippingRules')->__('General')));
		$ruleFieldset = $form->addFieldset('shippingRules_fsRule', array('legend'=>Mage::helper('shippingRules')->__('Rule')));
		$actionFieldset = $form->addFieldset('shippingRules_fsAction', array('legend'=>Mage::helper('shippingRules')->__('Action')));
		
	
		//General Fields
	    $fieldset->addField('title', 'text', array(
			'title'     => Mage::helper('shippingRules')->__('Title'),
			'label'     => Mage::helper('shippingRules')->__('Title'),
			'class'     => 'required-entry',
			'required'  => true,
	     	'name'      => 'title'
		));
		
		$fieldset->addField('position', 'text', array(
			'title'     => Mage::helper('shippingRules')->__('Rule Position'),
			'label'     => Mage::helper('shippingRules')->__('Rule Position'),
			'class'     => 'required-entry',
			'required'  => true,
	     	'name'      => 'position',
	    	'note'		=> Mage::helper('shippingRules')->__('The order in which the rules are checked for a match.')
		));
		 
		//Rule Fields
		$ruleFieldset->addField('carrier', 'text', array(
			'title'     => Mage::helper('shippingRules')->__('Carrier'),
			'label'     => Mage::helper('shippingRules')->__('Carrier'),
			'class'     => 'required-entry',
			'required'  => true,
	     	'name'      => 'carrier',
			'note'		=> Mage::helper('shipping')->__('* is a wildcard for any carrier. Separate multiple with a comma.')
		));
		
		$ruleFieldset->addField('carrier_ship_code', 'text', array(
			'title'     => Mage::helper('shippingRules')->__('Carrier Code'),
			'label'     => Mage::helper('shippingRules')->__('Carrier Code'),
			'class'     => 'required-entry',
			'required'  => true,
	     	'name'      => 'carrier_ship_code',
			'note'		=> Mage::helper('shipping')->__('* is a wildcard for any Carrier Code(ship method.). Separate multiple with a comma.')
		));
		
		$ruleFieldset->addField('allowed_countries', 'text', array(
			'title'     => Mage::helper('shippingRules')->__('Allowed Countries'),
			'label'     => Mage::helper('shippingRules')->__('Allowed Countries'),
			'class'     => 'required-entry',
			'required'  => true,
	     	'name'      => 'allowed_countries',
			'note'		=> Mage::helper('shippingRules')->__('Countries need to be entered by code (US).  * Is a wildcard for all countries. Separate multiple country codes with a comma. Allowed <a href="http://en.wikipedia.org/wiki/ISO_3166-2" target="_blank">country codes</a>.')
		));
		
		$ruleFieldset->addField('allowed_regions', 'text', array(
			'title'     => Mage::helper('shippingRules')->__('Allowed Regions'),
			'label'     => Mage::helper('shippingRules')->__('Allowed Regions'),
			'class'     => 'required-entry',
			'required'  => true,
	     	'name'      => 'allowed_regions',
			'note'		=> Mage::helper('shippingRules')->__('Regions need to be entered by code (WA).  * Is a wildcard for all regions. Separate multiple regions codes with a comma. Allowed <a href="https://www.usps.com/send/official-abbreviations.htm" target="_blank">US regions codes</a> and Allowed <a href="http://en.wikipedia.org/wiki/Canadian_postal_abbreviations_for_provinces_and_territories" target="_blank">CA regions codes</a>.')
		));
		
		$ruleFieldset->addField('allowed_postal_codes', 'text', array(
			'title'     => Mage::helper('shippingRules')->__('Allowed Postal Codes'),
			'label'     => Mage::helper('shippingRules')->__('Allowed Postal Codes'),
			'class'     => 'required-entry',
			'required'  => true,
	     	'name'      => 'allowed_postal_codes',
			'note'		=> Mage::helper('shippingRules')->__('* Is a wildcard for all postal codes. Separate multiple with a comma.')
		));
		
		$ruleFieldset->addField('cusomer_groups', 'text', array(
			'title'     => Mage::helper('shippingRules')->__('Cusomer Groups'),
			'label'     => Mage::helper('shippingRules')->__('Cusomer Groups'),
			'class'     => 'required-entry',
			'required'  => true,
	     	'name'      => 'cusomer_groups',
			'note'		=> Mage::helper('shippingRules')->__('* Is a wildcard for all customer groups. Separate multiple with a comma.')
		));

		
		$ruleFieldset->addField('qty', 'text', array(
			'title'     => Mage::helper('shippingRules')->__('Qty'),
			'label'     => Mage::helper('shippingRules')->__('Qty'),
			'class'     => 'required-entry',
			'required'  => true,
	     	'name'      => 'qty',
	    	'note'		=> Mage::helper('shippingRules')->__('The number of items in the cart. Prefix with the following, otherwise it is an exact match. "<" Less than, "<=" Less than or Equal to, ">" Greater than, ">=" Greater than or Equal to, "==" Equal to. Example: "<=5" would match a cart with 5 items or less (Use whole numbers only)  .')
		));
		
		$ruleFieldset->addField('weight', 'text', array(
			'title'     => Mage::helper('shippingRules')->__('Weight'),
			'label'     => Mage::helper('shippingRules')->__('Weight'),
			'class'     => 'required-entry',
			'required'  => true,
	     	'name'      => 'weight',
	    	'note'		=> Mage::helper('shippingRules')->__('The total weight(lbs) of all items in the cart. Prefix with the following, otherwise it is an exact match. "<" Less than, "<=" Less than or Equal to, ">" Greater than, ">=" Greater than or Equal to, "==" Equal to. Example: ">15" would match any cart with a total of more than 15 lbs (Use whole numbers only)  .')
		));

        $ruleFieldset->addField('subtotal', 'text', array(
            'title'     => Mage::helper('shippingRules')->__('Subtotal'),
            'label'     => Mage::helper('shippingRules')->__('Subtotal'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'subtotal',
            'note'		=> Mage::helper('shippingRules')->__('The subtotal of all items for the address (excluding discounts). Can be * for any subtotal, or prefix with the following, otherwise it is an exact match. "<" Less than, "<=" Less than or Equal to, ">" Greater than, ">=" Greater than or Equal to, "==" Equal to. Example: ">30" would match any address with a subtotal of more than $30.')
        ));


        $ruleFieldset->addField('rule_class_name', 'select', array(
			'title'     => Mage::helper('shippingRules')->__('Rule Class [Advanced]'),
			'label'     => Mage::helper('shippingRules')->__('Rule Class [Advanced]'),
			'required'  => false,
	     	'name'      => 'rule_class_name',
	    	'note'		=> Mage::helper('shippingRules')->__('(Advanced Users Only) Class to use for extended shipping rule validation.'),
			'options' 	=> Mage::getModel('shippingRules/rule_options')->getOptionArray()		
		));

		$ruleFieldset->addField('rule_class_params', 'textarea', array(
			'title'     => Mage::helper('shippingRules')->__('Rule Params [Advanced]'),
			'label'     => Mage::helper('shippingRules')->__('Rule Params [Advanced]'),
			'required'  => false,
	     	'name'      => 'rule_class_params',
	    	'note'		=> Mage::helper('shippingRules')->__('(Advanced Users Only) Parameters for extended rule specified above. If no rule is selected, this will be ignored.'),
		));

		//Action fields
		$actionFieldset->addField('flat_rate', 'text', array(
			'title'     => Mage::helper('shippingRules')->__('Flat Rate'),
			'label'     => Mage::helper('shippingRules')->__('Flat Rate'),
			'note'		=> Mage::helper('shippingRules')->__('When a rule is matched, if there is a value in the flat rate column, the flat rate is returned. A 0 (zero) will result in free shipping. A blank entry will be ignored.'),
	     	'name'      => 'flat_rate'
		));
		
		 $actionFieldset->addField('percent_rate', 'text', array(
			'title'     => Mage::helper('shippingRules')->__('Percent Rate'),
			'label'     => Mage::helper('shippingRules')->__('Percent Rate'),
			'note'		=> Mage::helper('shippingRules')->__('When a rule is matched, if there is not flat rate this percent value will be applied to the original carrier rate.  A blank entry will be ignored.'),
	     	'name'      => 'percent_rate'
		));
		
		$actionFieldset->addField('remove_rate', 'select', array(
			'title'     => Mage::helper('shippingRules')->__('Remove Rate'),
			'label'     => Mage::helper('shippingRules')->__('Remove Rate'),
			'note'		=> Mage::helper('shippingRules')->__('When a rule is matched, the returned rate will be removed from the list of available shipping rates.'),
	     	'name'      => 'remove_rate',
	     	'options'	=> array(GoSolid_ShippingRules_Model_Rules::REMOME_RATE_NO=>Mage::helper('shippingRules')->__('No'),GoSolid_ShippingRules_Model_Rules::REMOVE_RATE_YES=>Mage::helper('shippingRules')->__('Yes'))
		));
		
	
		if ( Mage::getSingleton('adminhtml/session')->getShippingrulesData() )
		{
	    	$form->setValues(Mage::getSingleton('adminhtml/session')->getShippingrulesData());
	    	Mage::getSingleton('adminhtml/session')->getShippingrulesData(null);
		}
		elseif(Mage::registry('shippingrules_data'))
		{
	    	$form->setValues(Mage::registry('shippingrules_data')->getData());
		}
		
		return parent::_prepareForm();
	}
  
}