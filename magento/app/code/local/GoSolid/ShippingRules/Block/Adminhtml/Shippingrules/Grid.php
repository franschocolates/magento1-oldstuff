<?php
class GoSolid_ShippingRules_Block_Adminhtml_Shippingrules_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{

		parent::__construct();
		$this->setId('shippingRulesGrid');
		
		//TODO: Set default sort
		$this->setDefaultSort('position');
		$this->setDefaultDir('ASC');
		
		//TODO: Optional
		//$this->setSaveParametersInSession(true);
		//$this->setDefaultFilter(array('status' => 'Enabled'));
	}
 
	protected function _prepareCollection()
	{
		$collection = Mage::getModel('shippingRules/rules')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}	
		
		
	protected function _prepareColumns()
  	{

		$this->addColumn('title', array(
			  'header'    => Mage::helper('shippingRules')->__('Title'),
			  'align'     =>'left',
			  'index'     => 'title',
		  ));

		$this->addColumn('flat_rate', array(
			  'header'    => Mage::helper('shippingRules')->__('Flat Rate'),
			  'align'     =>'right',
			  'width'     => '35px',
			  'index'     => 'flat_rate',
		  ));
		  
		$this->addColumn('percent_rate', array(
			  'header'    => Mage::helper('shippingRules')->__('Percent Rate'),
			  'align'     =>'right',
			  'width'     => '35px',
			  'index'     => 'percent_rate',
		  ));
		  
		$this->addColumn('remove_rate', array(
			  'header'    => Mage::helper('shippingRules')->__('Remove Rate'),
			  'align'     =>'right',
			  'width'     => '35px',
			  'index'     => 'remove_rate',
			  'type'  	  => 'options',
              'options'    => array(GoSolid_ShippingRules_Model_Rules::REMOME_RATE_NO=>Mage::helper('shippingRules')->__('No'),GoSolid_ShippingRules_Model_Rules::REMOVE_RATE_YES=>Mage::helper('shippingRules')->__('Yes')),
		  ));
		  
		$this->addColumn('carrier', array(
			  'header'    => Mage::helper('shippingRules')->__('Carrier'),
			  'align'     =>'right',
			  'index'     => 'carrier',
		  ));
		 
		$this->addColumn('carrier_ship_code', array(
			  'header'    => Mage::helper('shippingRules')->__('Carrier Code'),
			  'align'     =>'right',
			  'index'     => 'carrier_ship_code',
		  ));
		  
		$this->addColumn('allowed_countries', array(
			  'header'    => Mage::helper('shippingRules')->__('Allowed Countries'),
			  'align'     =>'right',
			  'index'     => 'allowed_countries',
		  ));
		  
		$this->addColumn('allowed_regions', array(
			  'header'    => Mage::helper('shippingRules')->__('Allowed Regions'),
			  'align'     =>'right',
			  'index'     => 'allowed_regions',
		  )); 
		
		$this->addColumn('allowed_postal_codes', array(
			  'header'    => Mage::helper('shippingRules')->__('Allowed Postal Codes'),
			  'align'     =>'right',
			  'index'     => 'allowed_postal_codes',
		  ));  

		$this->addColumn('cusomer_groups', array(
			  'header'    => Mage::helper('shippingRules')->__('Cusomer Groups'),
			  'align'     =>'right',
			  'index'     => 'cusomer_groups',
		  ));
		 
		$this->addColumn('qty', array(
			  'header'    => Mage::helper('shippingRules')->__('Qty'),
			  'align'     =>'right',
			  'width'     => '20px',
			  'index'     => 'qty',
		  ));
		$this->addColumn('weight', array(
			  'header'    => Mage::helper('shippingRules')->__('Weight'),
			  'align'     =>'right',
			  'width'     => '20px',
			  'index'     => 'weight',
		  ));
        $this->addColumn('subtotal', array(
            'header'    => Mage::helper('shippingRules')->__('Subtotal'),
            'align'     =>'right',
            'width'     => '20px',
            'index'     => 'subtotal',
        ));
		 $this->addColumn('position', array(
			  'header'    => Mage::helper('shippingRules')->__('Rule Position'),
			  'align'     =>'right',
			  'width'     => '35px',
			  'index'     => 'position',
		  ));
		  /*
		   * EXAMPLE CODE. CAN BE REMOVED.
		   *
		  
		  //text plain
		  $this->addColumn('myname', array(
			  'header'    => Mage::helper('shippingRules')->__('MyName'),
			  'align'     =>'left',
			  'index'     => 'name',
		  ));
		  
		  //date
		  $this->addColumn('mydate', array(
	            'header'    => Mage::helper('shippingRules')->__('MyDate'),
	            'type'      => 'date',
	            'align'     => 'left',
	            'index'     => 'mydate', //change me
	            'gmtoffset' => false
	        ));
	        
	      //options
	      $options = Mage::getModel('shippingRules/[model_lcase]')->getCollection()->setOrder("title", "asc")->toGridOptionArray("id", "title");
		  $this->addColumn('category_id', array(
			  'header'    => Mage::helper('shippingRules')->__('Category'),
			  'align'     => 'left',
			  'index'     => 'category_id',
			  'type'  => 'options',
			  'options'	=> $options, //example of how to get options.
		  )); 
		  
		  //status
		  $this->addColumn('status', array(
			  'header'    => Mage::helper('shippingRules')->__('Status'),
			  'align'     => 'left',
			  'width'     => '150px',
			  'index'     => 'status',
			  'type'	  => 'options',
			  'options'   => array(
				  'Enabled' => 'Enabled',
				  'Disabled' => 'Disabled',
			  ),
		  ));
		  
		  END EXAMPLE CODE
		  */ 
		  

		  return parent::_prepareColumns();
	}
	
	public function getRowUrl($row)
  	{
    	return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  	}
}