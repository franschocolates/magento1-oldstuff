<?php

class GoSolid_ShippingRules_Block_Adminhtml_Shippingrules_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'shippingRules';
        $this->_controller = 'adminhtml_shippingrules';
        
        // Optional
        //$this->_updateButton('save', 'label', Mage::helper('event')->__('Save'));
		//$this->_removeButton('delete');
		//$this->_removeButton('reset');
		
		$this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('shippingRules')->__('Save and Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);
		
        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
			
        ";
    }

	public function getHeaderText()
    {  
        if( Mage::registry('shippingrules_data') && Mage::registry('shippingrules_data')->getId() ) {
            return Mage::helper('shippingRules')->__("Edit %s Rule", Mage::registry('shippingrules_data')->getTitle());
		} else {
            return Mage::helper('shippingRules')->__('Add New Rule');
        }
    }

}