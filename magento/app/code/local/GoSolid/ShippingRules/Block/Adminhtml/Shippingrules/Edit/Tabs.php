<?php

class GoSolid_ShippingRules_Block_Adminhtml_Shippingrules_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{  
	public function __construct()
  	{
		$this->setId('shippingRules_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('shippingRules')->__('General'));
		parent::__construct();
  	}
	

	protected function _beforeToHtml()
  	{

	 $this->addTab('form_section', array(
          	'label'     => Mage::helper('shippingRules')->__('General'),
         	'title'     => Mage::helper('shippingRules')->__('General'),
          	'content'   =>  $this->getLayout()->createBlock('shippingRules/adminhtml_shippingrules_edit_tab_form')->toHtml(),
			'active'    => true
      ));


      return parent::_beforeToHtml();
  }
  
}