<?php

$installer = $this; 
$installer->startSetup(); 

$installer->run("ALTER TABLE gosolid_shipping_rules ADD COLUMN rule_class_name VARCHAR(100) DEFAULT NULL;");
$installer->run("ALTER TABLE gosolid_shipping_rules ADD COLUMN rule_class_params TEXT DEFAULT NULL;");

$installer->endSetup();
