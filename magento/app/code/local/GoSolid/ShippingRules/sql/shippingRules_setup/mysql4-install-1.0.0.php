<?php

$installer = $this; 
$installer->startSetup(); 

$installer->run(
	"DROP TABLE IF EXISTS  gosolid_shipping_rules;
	CREATE TABLE gosolid_shipping_rules (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `title` varchar(75) DEFAULT NULL,
	  `carrier` varchar(75) DEFAULT NULL,
	  `carrier_ship_code` varchar(255) DEFAULT NULL,
	  `allowed_countries` text,
	  `allowed_regions` text,
	  `allowed_postal_codes` text,
	  `cusomer_groups` varchar(255) DEFAULT NULL,
	  `qty` varchar(4) DEFAULT NULL,
	  `weight` varchar(10) DEFAULT NULL,
	  `position` int(5) DEFAULT NULL,
	  `flat_rate` float(10,2) DEFAULT NULL,
	  `percent_rate` int(3) DEFAULT NULL,
	  PRIMARY KEY (`id`)
	) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8"
);

$installer->endSetup();
