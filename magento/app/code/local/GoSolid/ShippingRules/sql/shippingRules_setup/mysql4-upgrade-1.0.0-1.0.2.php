<?php

$installer = $this; 
$installer->startSetup(); 

$installer->run("ALTER TABLE gosolid_shipping_rules ADD COLUMN remove_rate INT(0) DEFAULT NULL;");

$installer->endSetup();
