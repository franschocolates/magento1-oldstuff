<?php

$installer = $this; 
$installer->startSetup();

/* We can't use a normal upgrade script because some clients are on 1.5 or lower which don't have upgrade scripts */
$installer->run("ALTER TABLE {$installer->getTable('shippingRules/rules')} ADD COLUMN subtotal VARCHAR(20) DEFAULT NULL;");

$installer->endSetup();
