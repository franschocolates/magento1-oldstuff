<?php

$installer = $this;

$installer->startSetup();

$connection = $installer->getConnection();

/**
 *****************************************************************************
 * add columns to quote and order to indicate the giftcard info
 *****************************************************************************
 */

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote'), 'giftcard_number', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'LENGTH'    => 50,
        'COMMENT'   => 'Giftcard number',
        'required' => 0
    ));
$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote'), 'base_giftcard_amount', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'scale'     => 4,
        'precision' => 12,
        'COMMENT'   => 'Amount of giftcard used on quote',
        'required' => 0
    ));
$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote'), 'giftcard_amount', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'scale'     => 4,
        'precision' => 12,
        'COMMENT'   => 'Amount of giftcard used on quote',
        'required' => 0
    ));

// now create some more on orders
$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'giftcard_number', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'LENGTH'    => 50,
        'COMMENT'   => 'Giftcard number used with order',
        'required' => 0
    ));
$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'base_giftcard_amount', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'scale'     => 4,
        'precision' => 12,
        'COMMENT'   => 'Amount of giftcard used on order',
        'required' => 0
    ));
$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'giftcard_amount', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'scale'     => 4,
        'precision' => 12,
        'COMMENT'   => 'Amount of giftcard used on order',
        'required' => 0
    ));
$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'base_giftcard_invoiced_amount', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'scale'     => 4,
        'precision' => 12,
        'COMMENT'   => 'Amount of giftcard invoiced on order',
        'required' => 0
    ));
$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'giftcard_invoiced_amount', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'scale'     => 4,
        'precision' => 12,
        'COMMENT'   => 'Amount of giftcard invoiced on order',
        'required' => 0
    ));

$installer->endSetup();