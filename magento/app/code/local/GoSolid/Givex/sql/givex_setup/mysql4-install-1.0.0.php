<?php

$installer = $this;

$installer->startSetup();

$connection = $installer->getConnection();

/**
 *****************************************************************************
 * new table for giftcard
 *****************************************************************************
 */

$giftcardTable = $connection->newTable($installer->getTable('givex/giftcard'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Id')
    ->addColumn('order_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => true,
        ), 'Order the giftcard was purchased in')
    ->addColumn('order_item_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => true,
    ), 'Order item associated with the giftcard')
    ->addColumn('card_number', Varien_Db_Ddl_Table::TYPE_TEXT, 50, array(
        'nullable'  => true,
    ), 'Giftcard number')
    ->addColumn('card_type', Varien_Db_Ddl_Table::TYPE_CHAR, 2, array(
        'nullable'  => false,
    ), 'Type of card')
    ->addColumn('status', Varien_Db_Ddl_Table::TYPE_CHAR, 1, array(
        'nullable'  => true,
    ), 'Status of card')
    ->addColumn('amount', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', array(
        'nullable'  => false,
    ), 'Amount of card when purchased')
    ->addColumn('notes', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => true,
    ), 'Notes about giftcard.')
;

$connection->createTable($giftcardTable);

$installer->endSetup();