<?php

$installer = $this;

$installer->startSetup();

$connection = $installer->getConnection();

/**
 *****************************************************************************
 * add columns to giftcard to indicate when/who activated
 *****************************************************************************
 */

$installer->getConnection()
    ->addColumn($installer->getTable('givex/giftcard'), 'activated_at', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        'COMMENT'   => 'When giftcard was activated',
        'required' => 0
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('givex/giftcard'), 'activated_by', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'LENGTH'    => 50,
        'COMMENT'   => 'User that activated giftcard',
        'required' => 0
    ));

$installer->endSetup();