<?php
class GoSolid_Givex_IndexController extends Mage_Core_Controller_Front_Action
{
	protected function _initAction() {
		$this->loadLayout();
		return $this;
	}   
 
	public function codeAction() {

        $barcode        = '';
        $orderId        = $this->getRequest()->getParam(GoSolid_Givex_Model_Giftcard::BARCODE_PARAM_ORDER_ID);
        $itemOrderId    = $this->getRequest()->getParam(GoSolid_Givex_Model_Giftcard::BARCODE_PARAM_ORDER_ITEM_ID);
        $hash           = $this->getRequest()->getParam(GoSolid_Givex_Model_Giftcard::BARCODE_PARAM_ORDER_ID_ORDER_ITEM_ID_HASH);

        if($orderId != null && $itemOrderId != null && $hash !=null)
        {
            $barcode = MAge::getModel('givex/giftcard')->getBarCode($orderId, $itemOrderId,$hash);
        }

        //This is not the Magento way to send a response without headers but I can remember how they do it right now
        echo $barcode;
        die;
    }

}