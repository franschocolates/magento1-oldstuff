<?php

class GoSolid_Givex_Adminhtml_GiftcardController extends Mage_Adminhtml_Controller_Action
{
	protected function _initAction() {
		$this->loadLayout();
		
		//Optional
		$this->_setActiveMenu('sales/giftcards');
		//$this->_title($this->__('givex'))->_title($this->__('PST Admin'))->_title($this->__('Event Type Management'));
		
		return $this;
	}   
 
	public function indexAction() {

		$this->_initAction()
			->renderLayout();

	}

	public function editAction() {

		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('givex/giftcard')->load($id);
		
		Mage::register('giftcard_data', $model);
		
		if ($model->getId() || $id == 0) {
				$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
				if (!empty($data)) {
					$model->setData($data);
				}

				$this->loadLayout();
				
				//Optional for WYSIWYG must change loading blocks to XML
				//$this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
				//$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
				
				$this->_addContent($this->getLayout()->createBlock('givex/adminhtml_giftcard_edit'))
					->_addLeft($this->getLayout()->createBlock('givex/adminhtml_giftcard_edit_tabs'));
					
				$this->renderLayout();

			}
			else
			{
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('givex')->__('Record does not exist'));
				$this->_redirect('*/*/');
			}

	}
	
	
	public function newAction() {
		$this->_forward('edit');

	}
	
	public function saveAction() {
		$session = Mage::getSingleton('adminhtml/session');
		if ($data = $this->getRequest()->getPost()) {
			
			$model = Mage::getModel('givex/giftcard')
                        ->load($this->getRequest()->getParam('id'));
			$model->addData($data);
            if(isset($data['order_increment_id']))
            {
                $order = Mage::getModel('sales/order')->loadByIncrementId($data['order_increment_id']);
                if ($order->getId())
                {
                    $model->setOrderId($order->getId());
                }
            }

			try {

                // try and activate it
                if ($data['send_activation'])
                {
                    $model->activate();
                }

				$model->save();

				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('givex')->__('Record was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
				
			}
			catch(Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
			}
			
		}
		
		//on save and edit
		 if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('page_id' => $model->getId(), '_current'=>true));
                    return;
                }
		
		
		//no agency to save.
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('givex')->__('Record not found.'));
        $this->_redirect('*/*/');
		
	}

	public function applyToQuoteAction()
    {
        $return = array();

        try
        {
            if ($cardNumber = $this->getRequest()->getParam('giftcard_number'))
            {
                Mage::log("REceived card number in controller: $cardNumber");
                /* @var GoSolid_Givex_Model_Giftcard $giftcard */
                $giftcard = Mage::getModel('givex/giftcard');
                $quote = Mage::getSingleton('adminhtml/session_quote')->getQuote();

                $result = $giftcard->applyToQuote($cardNumber, $quote);

                if ($result > 0)
                {
                    $return['status'] = 'success';
                }
                else if ($result == GoSolid_Givex_Model_Giftcard::RETURN_INVALID_CARD)
                {
                    $return['status'] = 'fail';
                    $return['message'] = Mage::helper('givex')->__('Invalid gift card number.');
                }
                else if ($result == GoSolid_Givex_Model_Giftcard::RETURN_NO_BALANCE)
                {
                    $return['status'] = 'fail';
                    $return['message'] = Mage::helper('givex')->__('Gift card has no balance.');
                }
                else
                {
                    $return['status'] = 'fail';
                    $return['message'] = Mage::helper('givex')->__('Unknown error applying giftcard.');
                }
            }
        }
        catch (Exception $e)
        {
            Mage::logException($e);
            $return['status'] = 'fail';
            $return['message'] = Mage::helper('givex')->__('Unknown error applying giftcard.');

        }

        die(json_encode($return));
    }

    public function removeFromQuoteAction()
    {
        $return = array();
        // no params....
        try
        {
            $quote = Mage::getSingleton('adminhtml/session_quote')->getQuote();

            Mage::getModel("givex/giftcard")->removeFromQuote($quote);

            $return['status'] = 'success';
        }
        catch (Exception $e)
        {
            Mage::logException($e);
            $return['status'] = 'fail';
            $return['message'] = Mage::helper('givex')->__('Unknown error removing giftcard.');
        }

        die(json_encode($return));
    }
}