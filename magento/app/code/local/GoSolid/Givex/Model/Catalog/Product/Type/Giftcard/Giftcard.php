<?php
/**
 * Encapsulates a Physical Gift Card (Classic)
 * Inherits from Simple product, since that's what it is.
 */
class GoSolid_Givex_Model_Catalog_Product_Type_Giftcard_Giftcard
        extends Mage_Catalog_Model_Product_Type_Simple
        implements GoSolid_Givex_Model_Catalog_Product_Type_Giftcard
{
    /**
     * Whether product quantity is fractional number or not
     *
     * @var bool
     */
    protected $_canUseQtyDecimals  = false;

    /**
     * Process custom defined options for product
     *
     * @param Varien_Object $buyRequest
     * @param Mage_Catalog_Model_Product $product
     * @param string $processMode
     * @return array
     */
    protected function _prepareOptions(Varien_Object $buyRequest, $product, $processMode)
    {
        Mage::log("Mode of $processMode for product ID of {$product->getId()} for object " . print_r($buyRequest, true));
        return parent::_prepareOptions($buyRequest, $product, $processMode);
    }

    public function getGiftcardType()
    {
        return GoSolid_Givex_Model_Giftcard::CARD_TYPE_PHYSICAL;
    }

}
