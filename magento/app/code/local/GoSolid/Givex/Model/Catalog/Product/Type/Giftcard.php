<?php
/**
 * Interface to indicate that a product is a giftcard
 * and to force certain methods
 */
interface GoSolid_Givex_Model_Catalog_Product_Type_Giftcard
{
    public function getGiftcardType();
}