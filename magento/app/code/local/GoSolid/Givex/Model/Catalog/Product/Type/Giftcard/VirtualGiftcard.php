<?php
/**
 * Any logic for virtual gift cards...
 */
class GoSolid_Givex_Model_Catalog_Product_Type_Giftcard_VirtualGiftcard
    extends Mage_Catalog_Model_Product_Type_Virtual
    implements GoSolid_Givex_Model_Catalog_Product_Type_Giftcard
{
    const OPTION_RECIPIENT_NAME = 'gc_recipient_name';
    const OPTION_RECIPIENT_EMAIL = 'gc_recipient_email';
    const OPTION_MESSAGE = 'gc_message';
    const OPTION_SENDER_NAME = 'gc_sender_name';
    const OPTION_SENDER_EMAIL = 'gc_sender_email';
    const OPTION_AMOUNT = 'gc_amount';
    const EMAIL_TEMPLATE ='virtual_gift_card';
    /**
     * Whether product quantity is fractional number or not
     *
     * @var bool
     */
    protected $_canUseQtyDecimals  = false;

    public function getGiftcardType()
    {
        return GoSolid_Givex_Model_Giftcard::CARD_TYPE_VIRTUAL;
    }

    public function getRecipientName($orderItem)
    {
        return $this->_getOptionValueByLabel($orderItem, self::OPTION_RECIPIENT_NAME);
    }

    public function getRecipientEmail($orderItem)
    {
        return $this->_getOptionValueByLabel($orderItem, self::OPTION_RECIPIENT_EMAIL);
    }

    public function getMessage($orderItem)
    {
        return $this->_getOptionValueByLabel($orderItem, self::OPTION_MESSAGE);
    }

    public function getSenderName($orderItem)
    {
        return $this->_getOptionValueByLabel($orderItem, self::OPTION_SENDER_NAME);
    }

    public function getSenderEmail($orderItem)
    {
        return $this->_getOptionValueByLabel($orderItem, self::OPTION_SENDER_EMAIL);
    }

    /**
     * @param Mage_Sales_Model_Order_Item $orderItem
     * @param $label
     */
    protected function _getOptionValueByLabel($orderItem, $label)
    {
        // this code bears some explanation. The options that will come through will look something like this:
        /*
         * Array
(
    [info_buyRequest] => Array
        (
            [uenc] => somerandomstring...,
            [product] => 227
            [related_product] =>
            [options] => Array
                (
                    [3] => 8
                    [8] => Barrett!
                    [7] => barrett@gosolid.net
                    [6] => The gift message.
                    [5] => My Sender
                    [4] => sender@gosolid.net
                )

            [qty] => 1
        )

    [options] => Array
        (
            [0] => Array
                (
                    [label] => gc_amount
                    [value] => $75
                    [print_value] => $75
                    [option_id] => 3
                    [option_type] => radio
                    [option_value] => 8
                    [custom_view] =>
                )

            [1] => Array
                (
                    [label] => gc_recipient_name
                    [value] => Barrett!
                    [print_value] => Barrett!
                    [option_id] => 8
                    [option_type] => field
                    [option_value] => Barrett!
                    [custom_view] =>
                )

            [2] => Array
                (
                    [label] => gc_recipient_email
                    [value] => barrett@gosolid.net
                    [print_value] => barrett@gosolid.net
                    [option_id] => 7
                    [option_type] => field
                    [option_value] => barrett@gosolid.net
                    [custom_view] =>
                )

            [3] => Array
                (
                    [label] => gc_message
                    [value] => The gift message.
                    [print_value] => The gift message.
                    [option_id] => 6
                    [option_type] => area
                    [option_value] => The gift message.
                    [custom_view] =>
                )

            [4] => Array
                (
                    [label] => Sender's Name
                    [value] => My Sender
                    [print_value] => My Sender
                    [option_id] => 5
                    [option_type] => field
                    [option_value] => My Sender
                    [custom_view] =>
                )

            [5] => Array
                (
                    [label] => Sender's Email Address
                    [value] => sender@gosolid.net
                    [print_value] => sender@gosolid.net
                    [option_id] => 4
                    [option_type] => field
                    [option_value] => sender@gosolid.net
                    [custom_view] =>
                )

        )
)
         */

        if (($options = $orderItem->getProductOptions()) && isset($options['options']))
        {
            // find the option that has the matching label.
            $optionsByLabel = array_filter($options['options'], function ($o) use ($label) { return $o['label'] == $label; });

            if ($optionsByLabel)
            {
                // should now have a single option like:
                /*
                 *
                [1] => Array
                (
                    [label] => gc_recipient_name
                    [value] => Barrett!
                    [print_value] => Barrett!
                    [option_id] => 8
                    [option_type] => field
                    [option_value] => Barrett!
                    [custom_view] =>
                )

                 */
                $optionValueArray = current($optionsByLabel);
                return $optionValueArray['value'];
            }
        }

        // assume it wasn't present, and skip it.
        return '';
    }
}
