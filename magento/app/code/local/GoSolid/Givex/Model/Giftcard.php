<?php
/**
 * Giftcard model
 *
 * @method int getOrderId()
 * @method GoSolid_Givex_Model_Giftcard setOrderId(int $value)
 * @method string getCardNumber()
 * @method GoSolid_Givex_Model_Giftcard setCardNumber(string $value)
 * @method float getAmount()
 * @method GoSolid_Givex_Model_Giftcard setAmount(float $value)
 * @method string getStatus()
 * @method GoSolid_Givex_Model_Giftcard setStatus(string $value)
 * @method string getActivatedAt()
 * @method GoSolid_Givex_Model_Giftcard setActivatedAt(string $value)
 * @method string getActivatedBy()
 * @method GoSolid_Givex_Model_Giftcard setActivatedBy(string $value)
 * @method string getCardType()
 * @method GoSolid_Givex_Model_Giftcard setCardType(string $value)
 * @method int getOrderItemId()
 * @method GoSolid_Givex_Model_Giftcard setOrderItemId(int $value)
 *
 *
 * @package     GoSolid_Givex
 * @author      goSolid
 */
class GoSolid_Givex_Model_Giftcard extends Mage_Core_Model_Abstract
{
    const CARD_TYPE_PHYSICAL = 'P';
    const CARD_TYPE_VIRTUAL  = 'V';

    const CARD_STATUS_ACTIVE = 'A';
    const CARD_STATUS_INACTIVE = 'I';

    const RETURN_INVALID_CARD = -1;
    const RETURN_NO_BALANCE = -2;
    const RETURN_SUCCESS = 1;
    const RETURN_UNKNOWN_ERROR = -10;

    const BARCODE_HASH_TYPE = 'MD5';
    //TODO: Validate this is the correct code.
    const BARCODE_CODE_TYPE = 'code39';
    const BARCODE_OUTPUT_TYPE = 'image';
    const BARCODE_IMAGE_TYPE = 'gif';
    const BARCODE_PARAM_ORDER_ID = 'oid';
    const BARCODE_PARAM_ORDER_ITEM_ID = 'iid';
    const BARCODE_PARAM_ORDER_ID_ORDER_ITEM_ID_HASH = 'sd';

    const ACTIVATED_BY_DEFAULT = 'System';

    private static $_errorMappings = array(
                                        GoSolid_Givex_Model_Giftcard_Adapter_Interface::ERROR_DOES_NOT_EXIST => self::RETURN_INVALID_CARD
                                        , GoSolid_Givex_Model_Giftcard_Adapter_Interface::ERROR_NO_BALANCE => self::RETURN_NO_BALANCE
    );

    /** @var Mage_Sales_Model_Order_Item $_orderItem */
    private $_orderItem = null;

    public function _construct()
    {
        parent::_construct(); 
        $this->_init('givex/giftcard');
    }

    public function getOrderIncrementId()
    {
        if ($orderId = $this->getOrderId())
        {
            return Mage::getModel('sales/order')->load($orderId)->getIncrementId();
        }
        return false;
    }

    public function loadByCardNumber($cardNumber)
    {
        return $this->load($cardNumber, 'card_number');
    }

    /**
     * @param int $orderItemId
     * @return GoSolid_Givex_Model_Giftcard
     */
    public function loadByOrderItemId($orderItemId)
    {
        return $this->load($orderItemId, 'order_item_id');
    }

    public function isTestMode()
    {
        return Mage::getStoreConfig('givex/general/test_mode');
    }

    public function getBalance($cardNumber)
    {
        $balance = $this->_getAdapter()->getBalance($cardNumber);
        $cardLast4 = substr($cardNumber, -4);
        // DH switch from Mage log to custom givex log
        Mage::helper('givex')->logError("Gift card balance for card number '$cardNumber' has a balance of '$balance'", Zend_Log::INFO);

        if ($balance > 0)
        {
            return $balance;
        }

        if ($balance == 0)
        {
            return self::RETURN_NO_BALANCE;
        }

        if (array_key_exists($balance, GoSolid_Givex_Model_Giftcard::$_errorMappings))
        {
            return GoSolid_Givex_Model_Giftcard::$_errorMappings[$balance];
        }

        // DH add custom givex log
        // there was a issue getting the gift card balance
        Mage::Helper('givex')->logError("There was a problem retreiving the balance for card number '$cardNumber'", Zend_Log::ERR, true);

        return self::RETURN_UNKNOWN_ERROR;
    }

    public function applyToQuote($cardNumber, $quote)
    {
        $result = $this->getBalance($cardNumber);

        if ($result > 0)
        {
            $quote->setGiftcardNumber($cardNumber);
            $quote->setGiftcardBalance($result);
            $quote->collectTotals();
            $quote->save();

            // DH add custom givex log
            Mage::Helper('givex')->logError("Gift card balance applied to quote. Card #: $cardNumber.", Zend_Log::INFO);

            return self::RETURN_SUCCESS;
        }

        // DH add custom givex log
        Mage::Helper('givex')->logError("Gift card balance NOT applied to quote. Card #: $cardNumber.", Zend_Log::ERR, true);

        // should already be mapped
        return $result;
    }

    public function removeFromQuote($quote)
    {
        $quote->setGiftcardNumber(null);
        $quote->setGiftcardBalance(null);
        $quote->setGiftcardAmount(null);
        $quote->collectTotals();
        $quote->save();

        // DH add custom givex log
        Mage::Helper('givex')->logError("Gift card balance removed from quote.", Zend_Log::INFO);

        return self::RETURN_SUCCESS;
    }

    /**
     * @return bool
     * @throws Exception
     * @throws Mage_Core_Exception
     */
    public function activate()
    {
        // DH add custom givex log
        Mage::Helper('givex')->logError("Activate gift card.", Zend_Log::INFO);

        if ($this->getStatus() != self::CARD_STATUS_ACTIVE)
        {
            return false;
        }

        if ($this->getCardNumber() && $this->getAmount())
        {
            try
            {
                $result = $this->_getAdapter()->activate($this->getCardNumber(), $this->getAmount());

                if ($result > 0)
                {
                    $this->_setActivationFields();
                    return true;
                }

                // DH add custom givex log
                Mage::Helper('givex')->logError("Activation failed. Card #: $this->getCardNumber() -- Amount: $this->getAmount()", Zend_Log::ERR, true);

                // Should try and make this a better message somehow.
                Mage::throwException("Unable to activate giftcard. Error code: $result");
            }
            catch (Exception $exception)
            {
                // DH add custom givex log
                Mage::Helper('givex')->logError("Tried activation and failed. $exception->__toString()", Zend_Log::ERR, true);

                Mage::logException($exception);
                throw $exception;
            }
        }

        // DH add custom givex log
        Mage::Helper('givex')->logError("Can't activate. No gift card number.", Zend_Log::ERR, true);

        Mage::throwException('Cannot activate giftcard with no giftcard number!');
    }

    /**
     * @param Mage_Sales_Model_Quote $quote
     * @param Mage_Sales_Model_Order $order
     */
    public function redeem($order)
    {
        // DH add custom givex log
        Mage::Helper('givex')->logError("Start redemption process", Zend_Log::INFO);

        Mage::log("Giftcard number on order: {$order->getGiftcardNumber()}");

        if ($cardNumber = $order->getGiftcardNumber())
        {
            try
            {
                $authCode = $this->_getAdapter()->redeem($cardNumber, $order->getBaseGiftcardAmount());

                // DH switch from Mage log to custom givex log
                Mage::Helper('givex')->logError("have an authcode of $authCode, will try and log a history comment about it.", Zend_Log::INFO);

                $order->addStatusHistoryComment("Redeemed amount of {$order->getBaseGiftcardAmount()} via GiveX. Auth code: $authCode");
            }
            catch (Exception $exception)
            {
                Mage::logException($exception);

                $message = "Unable to redeem giftcard $cardNumber for order {$order->getIncrementId()}. Will allow order to process.";
                // DH changed to force logging
                Mage::helper('givex')->logError($message, Zend_Log::ERR, true);

                // we just need to continue as though nothing happened
                // and notify them that it happened

                // i don't like that we are calling into fran's from givex
                // but hard to avoid
                Mage::helper('frans')->sendAdminNotificationEmail('Error redeeming giftcard for order', $message);
            }

            // set that we invoiced it
            // currently base/actual are the same since they are USD only
            $giftcardAmount = $order->getBaseGiftcardAmount();
            $order->setBaseGiftcardInvoicedAmount($giftcardAmount);
            $order->setGiftcardInvoicedAmount($giftcardAmount);

            // try and adjust total paid
            $order->setBaseTotalPaid($giftcardAmount + $order->getBaseTotalPaid());
            $order->setTotalPaid($giftcardAmount + $order->getTotalPaid());

            return true;
        }

        // DH add custom givex log
        Mage::Helper('givex')->logError("Error redeeming gift card: '$cardNumber'.", Zend_Log::ERR, true);
    }

    public function importOrderItem(
        Mage_Sales_Model_Order_Item $orderItem
    )
    {
        // DH add custom givex log
        Mage::Helper('givex')->logError("Start of importOrderItem function.", Zend_Log::INFO);

        $giftcardType = $orderItem->getProduct()->getTypeInstance(true);
        $this->_orderItem = $orderItem;

        if ($giftcardType->getGiftcardType() == GoSolid_Givex_Model_Giftcard::CARD_TYPE_PHYSICAL)
        {
            return $this->_importPhysicalGiftCard($orderItem);
        }

        if ($giftcardType->getGiftcardType() == GoSolid_Givex_Model_Giftcard::CARD_TYPE_VIRTUAL)
        {
            return $this->_importVirtualGiftcard($orderItem);
        }

        // DH add custom givex log
        Mage::Helper('givex')->logError("No valid gift card type found.", Zend_Log::ERR, true);
    }

    /**
     * @return Mage_Sales_Model_Order_Item
     */
    public function getOrderItem()
    {
        // DH add custom givex log
        Mage::Helper('givex')->logError("Start of getOrderItem function.", Zend_Log::INFO);

        if (is_null($this->_orderItem))
        {
            if ($this->getOrderItemId())
            {
                $orderItem = Mage::getModel('sales/order_item')->load($this->getOrderItemId());

                // make sure we really got one.
                if ($orderItem->getId())
                {
                    $this->_orderItem = $orderItem;
                }
            }
        }

        return $this->_orderItem;
    }

    /**
     * Two key pieces of logic for before save:
     *  - Store Order Item ID if we have an order item
     *  - If virtual and status is now active (and wasn't previously),
     *      raise an event so that we can send emails as necessary
     *
     * @return $this|Mage_Core_Model_Abstract
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();

        // DH add custom givex log
        Mage::Helper('givex')->logError("Start of _beforeSave function.", Zend_Log::INFO);

        // store our Order Item Id if we have one
        if (!$this->getOrderItemId() && ($orderItem = $this->getOrderItem()))
        {
            $this->setOrderItemId($orderItem->getId());
        }

        // also raise an event if we are virtual and we are now active
        if ($this->_isVirtual())
        {
            $originalStatus = $this->getOrigData('status');

            Mage::log("Original status was $originalStatus, new status is {$this->getStatus()}");

            // DH add custom givex log
            Mage::Helper('givex')->logError("Original status was '$originalStatus', new status is '{$this->getStatus()}'", Zend_Log::INFO);

            if ($this->getStatus() == self::CARD_STATUS_ACTIVE &&
                $this->getStatus() != $originalStatus)
            {
                // need to raise an event
                Mage::log("I should now be raising an event....");
                Mage::dispatchEvent('givex_virtual_giftcard_register_after', array('giftcard' => $this));

                // DH add custom givex log
                Mage::Helper('givex')->logError("Status raise for virtual type.", Zend_Log::INFO);
            }
        }

        return $this;
    }

    /**
     *
     * @return GoSolid_Givex_Model_Giftcard_Adapter_Interface
     */
    protected function _getAdapter()
    {
        if ($this->isTestMode())
        {
            // DH add custom givex log
            Mage::Helper('givex')->logError("Returning givex/giftcard_adapter_local", Zend_Log::INFO);

            return Mage::getModel('givex/giftcard_adapter_local');
        }

        // DH add custom givex log
        Mage::Helper('givex')->logError("Returning givex/giftcard_adapter_givex", Zend_Log::INFO);

        return Mage::getModel('givex/giftcard_adapter_givex');
    }

    /**
     * Sets the activation fields (who/when)
     */
    protected function _setActivationFields()
    {
        // DH add custom givex log
        Mage::Helper('givex')->logError("Starting _setActivationFields function.", Zend_Log::INFO);

        $this->setActivatedAt(Mage::getModel('core/date')->gmtTimestamp());
        $adminUser = Mage::getSingleton('admin/session')->getUser();
        $this->setActivatedBy($adminUser->getFirstname() . ' ' . $adminUser->getLastname());
    }

    protected function _importPhysicalGiftCard($orderItem)
    {
        // DH add custom givex log
        Mage::Helper('givex')->logError("Starting _importPhysicalGiftCard function.", Zend_Log::INFO);

        // theoretically this should be right, don't need to pull from options
        $cardAmount = $orderItem->getPrice();

        return $this->setOrderId($orderItem->getOrderId())
            ->setStatus(GoSolid_Givex_Model_Giftcard::CARD_STATUS_INACTIVE)
            ->setCardNumber('') // will need to be entered for physical cards
            ->setAmount($cardAmount)
            ->setCardType(GoSolid_Givex_Model_Giftcard::CARD_TYPE_PHYSICAL);
    }

    /**
     * Logic to create virtual gift cards
     * Will need to call to Givex and get a new number and activate it.
     * Will also need to send an email somehow....
     *
     *
     * @param Mage_Sales_Model_Order_Item $orderItem
     */
    protected function _importVirtualGiftcard(Mage_Sales_Model_Order_Item $orderItem)
    {
        // DH add custom givex log
        Mage::Helper('givex')->logError("Starting _importVirtualGiftcard function.", Zend_Log::INFO);

        $cardAmount = $orderItem->getOriginalPrice();

        $newGiftcardNum = '';
        $cardStatus = GoSolid_Givex_Model_Giftcard::CARD_STATUS_INACTIVE;

        $successful = false;
        // try and create
        try
        {
            $newGiftcardNum = $this->_getAdapter()->register($cardAmount);

            if ($newGiftcardNum < 0)
            {
                // an error code from GiveX, didn't work
                $errorCode = $newGiftcardNum;
                $newGiftcardNum = '';

                // DH switch from Mage log to custom givex log
                Mage::Helper('givex')->logError("Received a GiveX error code $errorCode creating for order item {$orderItem->getId()} for Order #{$orderItem->getOrder()->getIncrementId()}"
                    , Zend_Log::WARN, 'givex-error.log', true);
            }
            else
            {
                // we were successful, mark it active.
                $successful = true;
                $cardStatus = GoSolid_Givex_Model_Giftcard::CARD_STATUS_ACTIVE;
            }
        }
        catch (Exception $exception)
        {
            // DH add custom givex log
            Mage::Helper('givex')->logError("Exception thrown while trying to activate new virtual gift card.", Zend_Log::ERR, true);

            Mage::logException($exception);
        }

        $this->setOrderId($orderItem->getOrderId())
            ->setStatus($cardStatus)
            ->setCardNumber($newGiftcardNum) // will need to be entered for physical cards
            ->setAmount($cardAmount)
            ->setCardType(GoSolid_Givex_Model_Giftcard::CARD_TYPE_VIRTUAL);

        // if it's active, set the activation fields.
        if ($cardStatus == GoSolid_Givex_Model_Giftcard::CARD_STATUS_ACTIVE)
        {
            $this->setActivatedAt(Mage::getModel('core/date')->gmtTimestamp())
                ->setActivatedBy(self::ACTIVATED_BY_DEFAULT);
        }

        // whether we are successful dictates what sort of event to raise
        if (!$successful)
        {
            // DH add custom givex log
            Mage::Helper('givex')->logError("Virtual gift card activation NOT successful.", Zend_Log::INFO);

            Mage::dispatchEvent('givex_virtual_giftcard_register_fail', array('giftcard' => $this));
       }

        // DH add custom givex log
        Mage::Helper('givex')->logError("Virtual gift card activation successful.", Zend_Log::INFO);

        return $this;
    }

    public function getIsVirtual()
    {
        return $this->_isVirtual();
    }

    protected function _isVirtual()
    {
        return $this->getCardType() == self::CARD_TYPE_VIRTUAL;
    }

    public function getBarCodeUrl()
    {
        // DH add custom givex log
        Mage::Helper('givex')->logError("Starting getBarCodeUrl function.", Zend_Log::INFO);

        $orderId        = $this->getOrderId();
        $orderItemId    = $this->getOrderItemId();
        $salt           = $this->_getBarCodeHash($orderItemId,$orderId);

        //TODO: make these to model constants
	    $barcodeUrl = Mage::getUrl('givex/index/code', array(
				            '_store'=>'default',
						    self::BARCODE_PARAM_ORDER_ID=>$orderId,
						    self::BARCODE_PARAM_ORDER_ITEM_ID=>$orderItemId,
						    self::BARCODE_PARAM_ORDER_ID_ORDER_ITEM_ID_HASH=>$salt)
				    ).'barcode.'.self::BARCODE_IMAGE_TYPE;

        return $barcodeUrl;
    }

    public function getBarCode($orderId, $orderItemId, $salt)
    {
        // DH add custom givex log
        Mage::Helper('givex')->logError("Starting getBarCode function.", Zend_Log::INFO);

        $barcodeImage = '';
        $barcodeHash = $this->_getBarCodeHash($orderItemId,$orderId);

        //Validate salt
        if($barcodeHash ==  $salt)
        {
            //Load Code
            $barcode = $this->load($orderItemId, 'order_item_id');

            // Only the text to draw is required
            $barcodeOptions = array(
                'TEXT'              => $barcode->getCardNumber(),
                'backgroundColor'   => '#F0F0F0',
                'barHeight'         => 60,
                'font'              => '/skin/frontend/frans/frans/css/fonts/tradegothic/25B5BF_B_0.ttf'
            );

            // No required options
            $rendererOptions = array('imageType' => self::BARCODE_IMAGE_TYPE);

            // Draw the barcode in a new image,
            // send the headers and the image
            $barcodeImage = Zend_Barcode::render(
                self::BARCODE_CODE_TYPE, self::BARCODE_OUTPUT_TYPE, $barcodeOptions, $rendererOptions
            );
        }

        // DH add custom givex log
        Mage::Helper('givex')->logError("Bar Code image generated.", Zend_Log::INFO);

        return $barcodeImage;

    }

    protected function _getBarCodeHash($orderItemId,$orderId)
    {
        return hash(self::BARCODE_HASH_TYPE, $orderItemId.$orderId);
    }

}