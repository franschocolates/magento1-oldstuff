<?php

class GoSolid_Givex_Model_Config_Source_GiftcardStatus
{
    public function toOptionArray()
    {
        return array(
            array('value' => GoSolid_Givex_Model_Giftcard::CARD_STATUS_ACTIVE, 'label'=>Mage::helper('givex')->__('Active')),
            array('value' => GoSolid_Givex_Model_Giftcard::CARD_STATUS_INACTIVE, 'label'=>Mage::helper('givex')->__('Inactive')),
        );
    }

    public function toGridOptionArray()
    {
        $list = array();


        foreach($this->toOptionArray() as $entry)
        {
            $list[$entry['value']] = $entry['label'];

        }
        return $list;
    }

}