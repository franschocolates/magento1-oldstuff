<?php

class GoSolid_Givex_Model_Config_Source_GiftcardType
{
    public function toOptionArray()
    {
        return array(
            array('value' => GoSolid_Givex_Model_Giftcard::CARD_TYPE_PHYSICAL, 'label'=>Mage::helper('givex')->__('Physical')),
            array('value' => GoSolid_Givex_Model_Giftcard::CARD_TYPE_VIRTUAL, 'label'=>Mage::helper('givex')->__('Virtual')),
        );
    }

    public function toGridOptionArray()
    {
        $list = array();


        foreach($this->toOptionArray() as $entry)
        {
            $list[$entry['value']] = $entry['label'];

        }
        return $list;
    }

}