<?php
/*
 * We inherit from Mage_Sales_Model_Resource_Order_Abstract in order to use virtual columns on the order
 */
class GoSolid_Givex_Model_Mysql4_Giftcard extends Mage_Sales_Model_Resource_Order_Abstract
{
    /**
     * Is grid available
     *
     * @var bool
     */
    protected $_grid                         = true;

    public function _construct()
    {    
        $this->_init('givex/giftcard', 'id');
    }

    /**
     * Init virtual grid records for entity
     *
     * @return Mage_Sales_Model_Resource_Order_Invoice
     */
    protected function _initVirtualGridColumns()
    {
        parent::_initVirtualGridColumns();
        Mage::log("Adding virtual column");
        $this->addVirtualGridColumn(
                'order_increment_id',
                'sales/order',
                array('order_id' => 'entity_id'),
                'increment_id'
            );

        return $this;
    }

}