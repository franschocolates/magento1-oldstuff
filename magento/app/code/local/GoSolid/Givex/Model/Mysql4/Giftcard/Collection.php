<?php
class GoSolid_Givex_Model_Mysql4_Giftcard_Collection extends Mage_Sales_Model_Resource_Order_Collection_Abstract
{
    public function _construct() 
    {
        parent::_construct();
        $this->_init('givex/giftcard');
    }

	public function toOptionArray($value = "id", $label = "name", $selectText = null)
    {
		$array =  $this->_toOptionArray($value, $label);
		
		if($selectText != null)
		{
			$array = array('' =>  Mage::helper('givex')->__("-- %s --", $selectText)) + $array;
		}

		return $array;
		
    }

    protected function _initSelect()
    {
        parent::_initSelect();

        $this->getSelect()
                ->joinLeft(
                array('order' => $this->getTable('sales/order'))
                , 'order.entity_id = main_table.order_id'
                , array('increment_id')
            );

        // both tables have status, need to disambiguate or else it crashes
        $this->addFilterToMap('status', 'main_table.status');

        return $this;
    }

    
	public function toGridOptionArray($value = "id", $label = "name")
    {
    	$list = array();
		

    	foreach($this as $item)
    	{
    		$list[$item->getData($value)] = $item->getData($label);	
    	
    	}       
		return $list;
    }
    
}