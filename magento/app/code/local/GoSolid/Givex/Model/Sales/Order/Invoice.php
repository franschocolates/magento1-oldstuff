<?php
/**
 * Created by PhpStorm.
 * User: Barrett
 * Date: 3/7/14
 * Time: 4:45 PM
 */ 
class GoSolid_Givex_Model_Sales_Order_Invoice extends Mage_Sales_Model_Order_Invoice
{
    /**
     *
     * Overriding to prevent emails from ever being sent
     * @param bool $notifyCustomer
     * @param string $comment
     * @return $this|Mage_Sales_Model_Order_Invoice
     */
    public function sendEmail($notifyCustomer = true, $comment = '')
    {
        // Fran's does not want to send new invoice emails
        // Shouldn't really be in this module, but oh well.
        return $this;
    }

    public function collectTotals()
    {
        parent::collectTotals();

        $this->_adjustTotalsForGiftcard();

        return $this;
    }

    private function _adjustTotalsForGiftcard()
    {
        if ($giftcardAmount = $this->getOrder()->getGiftcardInvoicedAmount())
        {
            Mage::log("Adjusting invoice totals base on giftcard amount of $giftcardAmount ");

            // for now, assume we are only doing this once
            $this->setGrandTotal($this->getGrandTotal() - $giftcardAmount);
            $this->setBaseGrandTotal($this->getBaseGrandTotal() - $giftcardAmount);
        }
    }
}