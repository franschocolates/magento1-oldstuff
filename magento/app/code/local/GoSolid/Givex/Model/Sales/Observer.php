<?php

class GoSolid_Givex_Model_Sales_Observer extends Mage_Core_Model_Abstract
{
    // for floating point calculations
    const EPSILON = 0.00001;

    const DISCOUNT_DESCRIPTION = 'Gift Card';

    // whether or not we applied the giftcard
    private $_giftcardApplied = false;

    // how much we've applied in total
    private $_totalAppliedBalance = 0;

    public function applyGiftcardDiscount($observer)
    {
        // DH add custom givex log
        Mage::Helper('givex')->logError("Starting applyGiftcardDiscount function.", Zend_Log::INFO);

        /* @var Mage_Sales_Model_Quote $quote */
        $quote = $observer->getQuote();

        if ($cardNumber = $quote->getGiftcardNumber())
        {
            if ($quote->hasGiftcardBalance())
            {
                $balance = $quote->getGiftcardBalance();
            }
            else
            {
                // something changed with the quote,  need to re-get the balance
                // DH switch from Mage log to custom givex log
                Mage::Helper('givex')->logError("Does not have giftcard balance, looking it up.", Zend_Log::INFO);

                $balance = Mage::getModel("givex/giftcard")->getBalance($cardNumber);
                $quote->setGiftcardBalance($balance);
            }

            // DH switch from Mage log to custom givex log
            Mage::Helper('givex')->logError("Have a giftcard number '$cardNumber' with balance of '$balance'.", Zend_Log::INFO);

            // now do the actual calculation
            if ($balance > 0)
            {
                $this->_giftcardApplied = true;
                $availableBalance = $balance;

                $baseGrandTotal = $quote->getBaseGrandTotal();
                $this->_totalAppliedBalance = min($baseGrandTotal, $availableBalance);

                // for multishipping quotes, we don't really want to apply it to the
                // addresses, so we just apply it to the quote.
                if (!$quote->getIsMultiShipping())
                {
                    // also need to apply to each address, or else it gets converted wrong.
                    // we do both since it can be either shipping or billing
                    foreach ($quote->getAllAddresses() as $address)
                    {
                        $this->_applyGiftCardAmountToAddress($address);
                    }
                }

                // hey, we applied it, add info to the quote as well
                $this->_addDiscountInfoToQuote($quote, $this->_totalAppliedBalance);

                // DH add custom givex log
                Mage::Helper('givex')->logError("A balance of '$this->_totalAppliedBalance' applied to quote.", Zend_Log::INFO);
            }

        }
    }

    /**
     * Called just before payment is placed
     * Need to check the balance on the gift card.
     *
     * @param Varien_Event $event
     */
    public function onPaymentPlaceStart($event)
    {
        // DH add custom givex log
        Mage::Helper('givex')->logError("Starting onPaymentPlaceStart function.", Zend_Log::INFO);

        /** @var Mage_Sales_Model_Order_Payment $payment */
        $payment = $event->getPayment();
        $order = $payment->getOrder();
        $quote = $order->getQuote();

        if ($cardNumber = $order->getGiftcardNumber())
        {
            if (!($balance = $quote->getGiftcardBalance()))
            {
                $balance = Mage::getModel('givex/giftcard')->getBalance($cardNumber);

                if ($balance < 0 && $balance != GoSolid_Givex_Model_Giftcard::RETURN_NO_BALANCE)
                {
                    // DH changed to force logging
                    Mage::helper('givex')->logError("Error returned from GiveX during payment place start. Error code: $balance, Giftcard: $cardNumber", Zend_Log::ERR, true);
                    Mage::throwException("Unable to deduct Gift Card balance.");
                }

                $quote->setGiftcardBalance($balance);
            }

            if ($order->getGiftcardAmount() > 0 && $balance < $order->getGiftcardAmount())
            {
                // DH add custom givex log
                Mage::Helper('givex')->logError("Gift Card does not contain enough funds.", Zend_Log::ERR, true);

                // not enough, throw an exception
                Mage::throwException("Gift Card does not contain enough funds!");
            }

            // nothing else to check
        }

        return $this;
    }

    /**
     * Called when payment has been placed
     * At this point, we can adjust back to the original order amount
     *
     * @param Varien_Event $event
     */
    public function onPaymentPlaceEnd($event)
    {
        // DH add custom givex log
        Mage::Helper('givex')->logError("Starting onPaymentPlaceEnd function.", Zend_Log::INFO);

        /** @var Mage_Sales_Model_Order_Payment $payment */
        $payment = $event->getPayment();
        $order = $payment->getOrder();

        if ($baseGiftcardAmount = $order->getBaseGiftcardAmount())
        {
            // first we redeem
            Mage::getModel('givex/giftcard')->redeem($order);

            $order->setBaseGrandTotal($order->getBaseGrandTotal() + $baseGiftcardAmount);
            $order->setGrandTotal($order->getGrandTotal() + $order->getGiftcardAmount());
        }
    }

    /**
     * Called when an order is placed;
     * used to figure out what giftcard records to create for an order
     *
     * @param $event
     */
    public function onOrderPlaceAfter($event)
    {
        // DH add custom givex log
        Mage::Helper('givex')->logError("Starting onOrderPlaceAfter function.", Zend_Log::INFO);

        /** @var GoSolid_Frans_Model_Sales_Order $order */
        $order = $event->getOrder();

        /** @var Mage_Sales_Model_Order_Item $orderItem */
        foreach ($order->getAllItems() as $orderItem)
        {
            // we want to make sure we have an Order set
            // otherwise we can't store it
            // we store a GiftcardHandled attribute just to make sure we don't double process.
            if ($orderItem->getOrder() && $orderItem->getOrder()->getId() && !$orderItem->getGiftcardHandled())
            {
                // DH switch from Mage log to custom givex log
                Mage::Helper('givex')->logError("Processing order item '{$orderItem->getId()}' in order '{$orderItem->getOrder()->getIncrementId()}'' (ID: '{$orderItem->getOrder()->getId()}')", Zend_Log::INFO);

                // only gift card items should come back
                $productType = $orderItem->getProduct()->getTypeInstance(false);
                if ($productType instanceof GoSolid_Givex_Model_Catalog_Product_Type_Giftcard)
                {
                    $this->_handleGiftcardPurchase($orderItem);
                    $orderItem->setGiftcardHandled(true);
                }
            }
        }
    }

    /**
     * @param Mage_Sales_Model_Quote $quote
     * @param float $appliedAmount
     */
    private function _addDiscountInfoToQuote($quote, $appliedAmount)
    {
        // DH add custom givex log
        Mage::Helper('givex')->logError("Starting _addDiscountInfoToQuote function.", Zend_Log::INFO);

        // DH switch from Mage log to custom givex log
        Mage::Helper('givex')->logError("This amount was applied from the giftcard: $appliedAmount.", Zend_Log::INFO);

        $grandTotal = $quote->getBaseGrandTotal();

        $newGrandTotal = $grandTotal - $appliedAmount;

        $newGrandTotal = ($this->_isZero($newGrandTotal)) ? 0 : $newGrandTotal;
        $quote->setBaseGrandTotal($newGrandTotal);
        $quote->setGrandTotal($newGrandTotal);

        $quote->setBaseGiftcardAmount($appliedAmount);
        $quote->setGiftcardAmount($appliedAmount);
    }

    /**
     * @param Mage_Sales_Model_Quote_Address $address
     * @param float $availableBalance
     * @return float    used balance
     */
    private function _applyGiftCardAmountToAddress($address)
    {
        // DH add custom givex log
        Mage::Helper('givex')->logError("Starting _applyGiftCardAmountToAddress function.", Zend_Log::INFO);

        if ($address->getBaseGrandTotal() > 0)
        {
            // DH switch from Mage log to custom givex log
            Mage::Helper('givex')->logError("Address total: '{$address->getBaseGrandTotal()}'", Zend_Log::INFO);

            $grandTotal = $address->getBaseGrandTotal();

            $appliedAmount = $this->_totalAppliedBalance;
            $newGrandTotal = $grandTotal - $appliedAmount;

            $newGrandTotal = ($this->_isZero($newGrandTotal)) ? 0 : $newGrandTotal;
            $address->setBaseGrandTotal($newGrandTotal);
            $address->setGrandTotal($newGrandTotal);

        }
    }

    /**
     * Helper function to determine if our float value is 0
     * Necessary since floats are imprecise
     *
     * @param $floatValue
     * @return bool
     */
    private function _isZero($floatValue)
    {
        return abs($floatValue) < self::EPSILON;
    }

    protected function _areEqual($val1, $val2)
    {
        return _isZero($val1 - $val2);
    }

    /**
     * Handle the purchase of a giftcard
     *
     * @param Mage_Sales_Model_Order_Item $orderItem
     */
    protected function _handleGiftcardPurchase(
        Mage_Sales_Model_Order_Item $orderItem
    )
    {
        // DH add custom givex log
        Mage::Helper('givex')->logError("Starting _handleGiftcardPurchase function.", Zend_Log::INFO);

        // create a giftcard instance for each qty (there could be multiple
        $qty = $orderItem->getQtyOrdered();

        for ($i = 1; $i <= $qty; $i++)
        {
            // theoretically this should be right, don't need to pull from options
            $giftcard = Mage::getModel('givex/giftcard')
                ->importOrderItem($orderItem);

            $giftcard->save();
        }
    }

}