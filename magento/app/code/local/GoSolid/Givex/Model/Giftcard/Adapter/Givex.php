<?php

require_once 'lib/Givex/GivexGiftCardConnect.php';

class GoSolid_Givex_Model_Giftcard_Adapter_Givex implements GoSolid_Givex_Model_Giftcard_Adapter_Interface
{

    public function getBalance($cardNumber)
    {
        // DH add custom givex log
        Mage::Helper('givex')->logError("Starting getBalance function.", Zend_Log::INFO);

        if ($giveX = $this->_getGiveXConnect($cardNumber))
        {
            $result = $giveX->runAction('GetBalance');

            // DH switch from Mage log to custom givex log
            Mage::Helper('givex')->logError("GiveX Result: " . print_r($result, true), Zend_Log::INFO);

            if (isset($result->certBalance))
            {
                // DH add custom givex log
                Mage::Helper('givex')->logError("Return gift card number $cardNumber's balance of $result->certBalance.", Zend_Log::INFO);

                return $result->certBalance;
            }
            else if (isset($result->notFound) && $result->notFound)
            {
                // DH add custom givex log
                Mage::Helper('givex')->logError("Gift card does NOT exist.", Zend_Log::ERR, true);

                return GoSolid_Givex_Model_Giftcard_Adapter_Interface::ERROR_DOES_NOT_EXIST;
            }
        }
        else
        {
            // DH add custom givex log
            Mage::Helper('givex')->logError("Error connecting to Givex module.", Zend_Log::ERR, true);

            return GoSolid_Givex_Model_Giftcard_Adapter_Interface::ERROR_CONNECTION_ERROR;
        }
    }

    public function redeem($cardNumber, $amount)
    {
        // DH add custom givex log
        Mage::Helper('givex')->logError("Starting redeem function.", Zend_Log::INFO);

        if ($giveX = $this->_getGiveXConnect($cardNumber))
        {
            // DH add custom givex log
            Mage::Helper('givex')->logError("Starting redemption process for gift card number '$cardNumber'.", Zend_Log::INFO);

            // need to set the amount
            $giveX->args['amount'] = $amount;
            $result = $giveX->runAction('RedeemForced');

            // DH switch from Mage log to custom givex log
            Mage::Helper('givex')->logError("Result of redeem forced for card $cardNumber: ". print_r($result, true), Zend_Log::INFO);

            if (isset($result->authCode))
            {
                // DH switch from Mage log to custom givex log
                Mage::Helper('givex')->logError("Received this AuthCode from GiveX: {$result->authCode}.", Zend_Log::INFO);

                return $result->authCode;
            }
            else if (isset($result->notFound) && $result->notFound)
            {
                // DH add custom givex log
                Mage::Helper('givex')->logError("Giftcard {$cardNumber} does not exist.", Zend_Log::ERR, true);

                Mage::throwException("Giftcard {$cardNumber} does not exist!");
            }
        }
        else
        {
            // DH add custom givex log
            Mage::Helper('givex')->logError("Error connecting to GiveX.", Zend_Log::ERR, true);

            Mage::throwException("Error connecting to GiveX!");
        }

        // DH add custom givex log
        Mage::Helper('givex')->logError("Unknown error in Givex response; did not redeem gift card.", Zend_Log::ERR, true);

        Mage::throwException("Unknown error in Givex response; did not redeem gift card.");
    }

    public function activate($cardNumber, $amount)
    {
        // DH add custom givex log
        Mage::Helper('givex')->logError("Starting activate function.", Zend_Log::INFO);

        if ($giveX = $this->_getGiveXConnect($cardNumber))
        {
            // DH add custom givex log
            Mage::Helper('givex')->logError("Starting activation process.", Zend_Log::INFO);

            // need to set the amount
            $giveX->args['amount'] = $amount;
            $result = $giveX->runAction('Activate');

            // DH switch from Mage log to custom givex log
            Mage::Helper('givex')->logError("Result of activation: ". print_r($result, true), Zend_Log::INFO);


            if (isset($result->authCode))
            {
                // DH switch from Mage log to custom givex log
                Mage::Helper('givex')->logError("Received this AuthCode from GiveX for activation: {$result->authCode}", Zend_Log::INFO);

                return $result->certBalance;
            }
            else if (isset($result->notFound) && $result->notFound)
            {
                // DH add custom givex log
                Mage::Helper('givex')->logError("Does not exist.", Zend_Log::ERR, true);

                return GoSolid_Givex_Model_Giftcard_Adapter_Interface::ERROR_DOES_NOT_EXIST;
            }
            else
            {
                // DH add custom givex log
                Mage::Helper('givex')->logError("Unknown error.", Zend_Log::ERR, true);

                return GoSolid_Givex_Model_Giftcard_Adapter_Interface::ERROR_UNKNOWN;
            }
        }
        else
        {
            // DH add custom givex log
            Mage::Helper('givex')->logError("Connection error.", Zend_Log::ERR, true);

            return GoSolid_Givex_Model_Giftcard_Adapter_Interface::ERROR_CONNECTION_ERROR;
        }

        // DH add custom givex log
        Mage::Helper('givex')->logError("Gift card number $cardNumber not activated.", Zend_Log::ERR, true);

        return false;
    }

    public function register($amount)
    {
        // DH add custom givex log
        Mage::Helper('givex')->logError("Starting register function.", Zend_Log::INFO);

        if ($giveX = $this->_getGiveXConnect())
        {
            // need to set the amount
            $giveX->args['amount'] = $amount;
            $giveX->amount = $amount;
            $result = $giveX->runAction('Register');

            // DH switch from Mage log to custom givex log
            Mage::Helper('givex')->logError("Result of registration: ". print_r($result, true), Zend_Log::INFO);

            if (isset($giveX->cardNum))
            {
                // DH switch from Mage log to custom givex log
                Mage::Helper('givex')->logError("Received this Card number back from GiveX for registration: {$giveX->cardNum}", Zend_Log::INFO);

                return $giveX->cardNum;
            }
            else
            {
                // DH add custom givex log
                Mage::Helper('givex')->logError("Unknown error.", Zend_Log::ERR, true);

                return GoSolid_Givex_Model_Giftcard_Adapter_Interface::ERROR_UNKNOWN;
            }
        }
        else
        {
            // DH add custom givex log
            Mage::Helper('givex')->logError("Connection error.", Zend_Log::ERR, true);

            return GoSolid_Givex_Model_Giftcard_Adapter_Interface::ERROR_CONNECTION_ERROR;
        }

        return false;
    }

    private function _getGiveXConnect($giftCardNumber = '')
    {
        // DH add custom givex log
        Mage::Helper('givex')->logError("Starting _getGiveXConnect function.", Zend_Log::INFO);

        $giveX = new GivexGiftCardConnect();

        // don't always need card number
        if ($giftCardNumber)
        {
            $giveX->setCardNum($giftCardNumber);
        }

        $giveX->init(
            $this->_getConfig('token'),
            $this->_getConfig('userid'),
            $this->_getConfig('userpass'),
            $this->_getConfig('url')
        );

        if ($giveX->connectionError)
        {
            // DH add custom givex log
            Mage::Helper('givex')->logError("Connection error.", Zend_Log::ERR, true);

            return false;
        }

        return $giveX;
    }

    private function _getConfig($name)
    {
        // DH add custom givex log
        Mage::Helper('givex')->logError("Starting _getConfig function.", Zend_Log::INFO);

        if ($name == 'token' || $name == 'userpass')
        {
            return Mage::helper('core')->decrypt(Mage::getStoreConfig('givex/credentials/' . $name));
        }

        return Mage::getStoreConfig('givex/credentials/' . $name);
    }

}
