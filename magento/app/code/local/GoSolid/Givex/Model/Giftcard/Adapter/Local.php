<?php

class GoSolid_Givex_Model_Giftcard_Adapter_Local implements GoSolid_Givex_Model_Giftcard_Adapter_Interface
{

    public function getBalance($cardNumber)
    {
        // DH add custom givex log
        Mage::Helper('givex')->logError("Starting getBalance function.", Zend_Log::INFO);

        // load it from the local DB
        $card = Mage::getModel('givex/giftcard')->loadByCardNumber($cardNumber);

        if ($card->getId() && $card->getAmount() > 0)
        {
            // DH add custom givex log
            Mage::Helper('givex')->logError("Returning gift card amount: ". $card->getAmount(), Zend_Log::INFO);

            return $card->getAmount();
        }
        else if ($card->getId())
        {
            // DH add custom givex log
            Mage::Helper('givex')->logError("No balance in gift card '$cardNumber'.", Zend_Log::ERR, true);

            return GoSolid_Givex_Model_Giftcard_Adapter_Interface::ERROR_NO_BALANCE;
        }

        // DH add custom givex log
        Mage::Helper('givex')->logError("Gift card '$cardNumber' does not exist.", Zend_Log::ERR, true);

        return GoSolid_Givex_Model_Giftcard_Adapter_Interface::ERROR_DOES_NOT_EXIST;
    }

    public function redeem($cardNumber, $amount)
    {
        // nothing to do here
        // DH switch from Mage log to custom givex log
        Mage::Helper('givex')->logError("Redeem called for card '$cardNumber' for amount of '$amount'.", Zend_Log::INFO);

        return 'GIVEX-TESTAUTH-' . substr($cardNumber, -4);
    }

    public function activate($cardNumber, $amount)
    {
        // for our local functionality, we don't need to do anything
        // DH switch from Mage log to custom givex log
        Mage::Helper('givex')->logError("Activating giftcard $cardNumber for amount of $amount.", Zend_Log::INFO);

        return $amount;
    }

    public function register($amount)
    {
        // for our local functionality, we just need a random number
        // timestamp should do
        $cardNumber = time();
        // DH switch from Mage log to custom givex log
        Mage::Helper('givex')->logError("Registering new giftcard for amount of '$amount' with new card number of '$cardNumber'.", Zend_Log::INFO);

        return $cardNumber;
    }
}