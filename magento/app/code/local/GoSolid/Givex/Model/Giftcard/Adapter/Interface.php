<?php

interface GoSolid_Givex_Model_Giftcard_Adapter_Interface
{
    const ERROR_DOES_NOT_EXIST    = -1;
    const ERROR_NO_BALANCE = -2;
    const ERROR_CONNECTION_ERROR = -3;
    const ERROR_UNKNOWN = -4;

    public function getBalance($cardNumber);

    public function redeem($cardNumber, $amount);

    public function activate($cardNumber, $amount);

    /** Registers a new card (requests a card be created and gets the number */
    public function register($amount);
}