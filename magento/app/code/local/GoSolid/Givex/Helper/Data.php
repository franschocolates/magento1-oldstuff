<?php
class GoSolid_Givex_Helper_Data extends Mage_Core_Helper_Abstract
{
	const LOG_FILE = 'givex-giftcard-error.log';

    public function logError($message, $logLevel = null, $forceLog = false)
    {
        Mage::log($message, $logLevel, self::LOG_FILE, $forceLog);
    }
}