<?php
/**
 *
 * Necessary to put in the total for the Giftcard
 */
class GoSolid_Givex_Block_Adminhtml_Sales_Order_Invoice_Totals extends Mage_Adminhtml_Block_Sales_Order_Invoice_Totals
{
    protected function _initTotals()
    {
        parent::_initTotals();

        // only show the section if they have a giftcard amount
        if ($giftcardAmount = $this->getOrder()->getGiftcardInvoicedAmount())
        {
            // to insert it second to last, we chunk the array into everything but the last one
            // (which should be grand total)
            // and then insert ours in.
            $totalsCount = count($this->_totals);
            list($first, $last) = array_chunk($this->_totals, $totalsCount - 1, true);

            $first['giftcard_total'] = new Varien_Object(array(
                'code'      => 'giftcard_total',
                'value'     => -$giftcardAmount,
                'base_value'=> -$giftcardAmount,
                'label'     => $this->helper('givex')->__('Gift Card'),
            ));

            $this->_totals = $first + $last;
        }

        return $this;
    }
}