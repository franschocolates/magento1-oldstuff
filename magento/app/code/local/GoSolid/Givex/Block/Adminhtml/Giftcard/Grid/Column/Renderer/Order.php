<?php

class GoSolid_Givex_Block_Adminhtml_Giftcard_Grid_Column_Renderer_Order extends
    Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        if ($orderId = $row->getOrderId())
        {
            return sprintf("<a title='View Order' target='_blank' href='%s'>%s</a>",
                $this->getUrl('*/sales_order/view', array('order_id' => $orderId)),
                $row->getIncrementId());
        }
        return '';
    }
}