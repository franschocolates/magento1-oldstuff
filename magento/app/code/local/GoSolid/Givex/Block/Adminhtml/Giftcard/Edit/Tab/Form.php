<?php

class GoSolid_Givex_Block_Adminhtml_Giftcard_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

	protected function _prepareForm()
	{

		$form = new Varien_Data_Form(); 
		$this->setForm($form);
		$fieldset = $form->addFieldset('givex_fs', array('legend'=>Mage::helper('givex')->__('Giftcard')));

        // we might need to pull some stuff from the model
        $existingCard = Mage::registry('giftcard_data');

        $fieldset->addField('order_increment_id', 'text', array(
            'title'     => Mage::helper('givex')->__('Order ID'),
            'label'     => Mage::helper('givex')->__('Order ID'),
            'name'      => 'order_increment_id',
            'class'     => 'required-entry',
            //'required'  => true,
            'note'	  => 'ID of order in which this gift card was purchased',
        ));

        $fieldset->addField('card_number', 'text', array(
            'title'     => Mage::helper('givex')->__('Card number'),
            'label'     => Mage::helper('givex')->__('Card number'),
            'name'      => 'card_number',
            'class'     => 'required-entry',
            'required'  => true,
            'note'	  => 'Number of gift card',
        ));

        $fieldset->addField('amount', 'text', array(
            'title'     => Mage::helper('givex')->__('Amount'),
            'label'     => Mage::helper('givex')->__('Amount'),
            'name'      => 'amount',
            'class'     => 'required-entry validate-zero-or-greater',
            'required'  => true,
            'note' => 	 Mage::helper('givex')->__("Amount of gift card.")
        ));

        $options = Mage::getModel('givex/config_source_giftcardStatus')->toOptionArray();
        $fieldset->addField('status', 'select', array(
            'label'     => Mage::helper('givex')->__('Card Status'),
            'title'     => Mage::helper('givex')->__('Card Status'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'status',
            'values'   => $options
        ));

        $activationCheckbox = $fieldset->addField('send_activation', 'checkbox', array(
            'label'     => Mage::helper('givex')->__('Send Activation'),
            'title'     => Mage::helper('givex')->__('Send Activation'),
            'name'      => 'send_activation',
            'value'     => 1,
            'note'      => 'Check this to activate & set the amount with givex. Will be ignored if status not active.',
            'id'        => 'send_activation'
        ));
        // if they already have a card number, we don't want to activate
        if ($existingCard && $existingCard->getCardNumber())
        {
            // have to use disabled; readonly doesn't seem to work for some reason without disabled
            $activationCheckbox->setReadonly(true, true);
        }
        else
        {
            // if they don't, default it to activate it
            $activationCheckbox->setData('checked', 'checked');
        }

        $options = Mage::getModel('givex/config_source_giftcardType')->toOptionArray();
        $fieldset->addField('card_type', 'select', array(
            'label'     => Mage::helper('givex')->__('Card Type'),
            'title'     => Mage::helper('givex')->__('Card Type'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'card_type',
            'values'   => $options
        ));

        $fieldset->addField('notes', 'textarea', array(
            'title'     => Mage::helper('givex')->__('Notes'),
            'label'     => Mage::helper('givex')->__('Notes'),
            'name'      => 'notes',
            //'class'     => 'required-entry',
            //'required'  => true,
            'note'	  => 'Notes about this gift card purchase',
        ));

		if ( Mage::getSingleton('adminhtml/session')->getGiftcardData() )
		{

	    	$form->setValues(Mage::getSingleton('adminhtml/session')->getGiftcardData());

	    	Mage::getSingleton('adminhtml/session')->getGiftcardData(null);
		}
		elseif(Mage::registry('giftcard_data'))
		{
            $model = Mage::registry('giftcard_data');
            $model->setData('order_increment_id', $model->getOrderIncrementid());
	    	$form->setValues(Mage::registry('giftcard_data')->getData());
		}
		
		return parent::_prepareForm();
	}
  
}