<?php
 
class GoSolid_Givex_Block_Adminhtml_Giftcard extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {

	$this->_controller = 'adminhtml_giftcard';
	$this->_blockGroup = 'givex';
	$this->_headerText = Mage::helper('givex')->__("Gift Cards");
	$this->_addButtonLabel = Mage::helper('givex')->__("Add Gift Card");
	
    parent::__construct();
    
  }
}