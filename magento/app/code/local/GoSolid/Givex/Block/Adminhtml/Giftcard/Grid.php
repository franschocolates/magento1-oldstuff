<?php
class GoSolid_Givex_Block_Adminhtml_Giftcard_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{

		parent::__construct();
		$this->setId('givexGrid');
		
		$this->setDefaultSort('id');
		$this->setDefaultDir('DESC');
		
		//TODO: Optional
		//$this->setSaveParametersInSession(true);
		//$this->setDefaultFilter(array('status' => 'Enabled'));
	}
 
	protected function _prepareCollection()
	{
		$collection = Mage::getModel('givex/giftcard')->getCollection();

		$this->setCollection($collection);
		return parent::_prepareCollection();
	}	
		
		
	protected function _prepareColumns()
  	{

  		$this->addColumn('id', array(
			  'header'    => Mage::helper('givex')->__('ID'),
			  'align'     =>'right',
			  'width'     => '50px',
			  'index'     => 'id',
		  ));

        $this->addColumn('order_increment_id', array(
            'header'    => Mage::helper('givex')->__('Order ID'),
            'align'     =>'right',
            'width'     => '100px',
            'index'     => 'increment_id',
            'renderer'  => 'GoSolid_Givex_Block_Adminhtml_Giftcard_Grid_Column_Renderer_Order'
        ));

        $this->addColumn('card_number', array(
            'header'    => Mage::helper('givex')->__('Card Number'),
            'align'     =>'right',
            'width'     => '100px',
            'index'     => 'card_number',
        ));

        $this->addColumn('amount', array(
            'header'    => Mage::helper('givex')->__('Card Amount'),
            'align'     =>'right',
            'width'     => '150px',
            'index'     => 'amount',
            'type'     => 'currency'
        ));

        $this->addColumn('status', array(
            'header'    => Mage::helper('givex')->__('status'),
            'align'     =>'right',
            'width'     => '100px',
            'index'     => 'status',
            'type'      => 'options',
            'options'    => Mage::getModel('givex/config_source_giftcardStatus')->toGridOptionArray()
        ));

        $this->addColumn('card_type', array(
            'header'    => Mage::helper('givex')->__('Card Type'),
            'align'     =>'right',
            'width'     => '100px',
            'type'  => 'options',
            'index'     => 'card_type',
            'options'    => Mage::getModel('givex/config_source_giftcardType')->toGridOptionArray()
        ));

        $this->addColumn('notes', array(
            'header'    => Mage::helper('givex')->__('Notes'),
            'align'     =>'right',
            'width'     => '250px',
            'index'     => 'notes',
        ));

		  return parent::_prepareColumns();
	}
	
	public function getRowUrl($row)
  	{
    	return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  	}
}