<?php

class GoSolid_Givex_Block_Adminhtml_Sales_Order_Create_Form_Giftcard
    extends Mage_Adminhtml_Block_Sales_Order_Create_Form_Abstract
{
    public function getSaveUrl()
    {
        return $this->getUrl("adminhtml/giftcard/applyToQuote");
    }

    public function getRemoveUrl()
    {
        return $this->getUrl("adminhtml/giftcard/removeFromQuote");
    }

    public function getHeaderCssClass()
    {
        return 'head-account';
    }

    public function getHeaderText()
    {
        return Mage::helper('sales')->__('Gift card');
    }

    public function getGiftcardNumber()
    {
        return $this->getQuote()->getGiftcardNumber();
    }

    protected function _prepareLayout()
    {
        if ($this->getGiftcardNumber())
        {
            $onclick = "removeGiftcard('{$this->getRemoveUrl()}')";
            $button = $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'   => Mage::helper('sales')->__('Remove Gift Card'),
                    'class'   => 'save progress-undo',
                    'onclick' => $onclick
                ));
            $this->setChild('remove_button', $button);
        }
        else
        {
            //$onclick = "submitAndReloadArea($('giftcard_fields').parentNode, '".$this->getSaveUrl()."')";
            $onclick = "applyGiftcard('{$this->getSaveUrl()}')";
            $button = $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'   => Mage::helper('sales')->__('Done'),
                    'class'   => 'save progress-done',
                    'onclick' => $onclick
                ));
            $this->setChild('save_button', $button);
        }

        return parent::_prepareLayout();
    }

    protected function _prepareForm()
    {
        // don't actually need to do anything at this point
        return parent::_prepareForm();
    }

}