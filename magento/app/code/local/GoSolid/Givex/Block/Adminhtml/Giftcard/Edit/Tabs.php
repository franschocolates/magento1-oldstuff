<?php

class GoSolid_Givex_Block_Adminhtml_Giftcard_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{  
	public function __construct()
  	{
		$this->setId('givex_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('givex')->__('General'));
		parent::__construct();
  	}
	

	protected function _beforeToHtml()
  	{

	 $this->addTab('form_section', array(
          	'label'     => Mage::helper('givex')->__('General'),
         	'title'     => Mage::helper('givex')->__('General'),
          	'content'   =>  $this->getLayout()->createBlock('givex/adminhtml_giftcard_edit_tab_form')->toHtml(),
			'active'    => true
      ));


      return parent::_beforeToHtml();
  }
  
}