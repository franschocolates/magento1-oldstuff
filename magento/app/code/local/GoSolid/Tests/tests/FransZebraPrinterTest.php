<?php
require_once Mage::getBaseDir('lib') . DS . 'Stripe' . DS . 'Stripe.php';

class FransZibraPrinterTest extends PHPUnit_Framework_TestCase
{



    public function setUp()
    {
        # this is required in every test to make sure the magento stuff is built up.
        Mage::app('default');
        Mage::app()->setCurrentStore('default');


    }

    public function test_add_zebra_print_job()
    {
        //make sure there is a print station.

        //create a print job.
        $printJob = $this->_createPrintJob();


        $this->assertTrue($printJob->getId() > 0, 'Zebra print job could not be created.');


    }


    public function test_update_zebra_print_job_to_failed()
    {
        //create a print job.
        $printJob = $this->_createPrintJob();

        $printJobUpdated = Mage::getModel("frans/printJobManager")->updatePrintJob($printJob->getId(), GoSolid_Frans_Model_PrintJob::JOB_STATUS_FAILED);

        $this->assertTrue($printJobUpdated->getPrintStatus() == GoSolid_Frans_Model_PrintJob::JOB_STATUS_FAILED, 'Zebra print job could not be created.');
    }

    private function _createPrintJob()
    {
        $random = $this->_getrandomstring(5);

        //build a print job.
        $printStationId = 1;
        $printJobType = GoSolid_Frans_Model_PrintJob::JOB_TYPE_ZPLII;
        $document = "^XA^CF,0,0,0^PR12^MD30^PW800^PON^CI13^LH0,20
            ^FO0,147^GB800,4,4^FS
            ^FO0,401^GB800,4,4^FS
            ^FO0,736^GB800,4,4^FS
            ^FO35,12^AdN,0,0^FWN^FH^FDFrom:^FS
            ^FO35,31^AdN,0,0^FWN^FH^FD^FS
            ^FO35,51^AdN,0,0^FWN^FH^FDFRAN'S CHOCOLATES^FS
            ^FO35,71^AdN,0,0^FWN^FH^FD1300 EAST PIKE STREET^FS
            ^FO35,92^AdN,0,0^FWN^FH^FD^FS
            ^FO35,132^AdN,0,0^FWN^FH^FD(206) 322-0233^FS
            ^FO490,72^AdN,0,0^FWN^FH^FDCAD: 101218323/WSXI2600^FS
            ^FO43,188^A0N,25,27^FWN^FH^FD^FS
            ^FO43,218^A0N,25,27^FWN^FH^FD^FS
            ^FO43,248^A0N,25,27^FWN^FH^FD5628 Airport Way S #220^FS
            ^FO43,278^A0N,25,27^FWN^FH^FD^FS
            ^FO35,112^AdN,0,0^FWN^FH^FDSEATTLE, WA 98122^FS
            ^FO490,31^AdN,0,0^FWN^FH^FDShip Date: 29APR14^FS
            ^FO490,51^AdN,0,0^FWN^FH^FDActWgt: 1.0 LB^FS
            ^FO43,158^A0N,25,27^FWN^FH^FDMac #220^FS
            ^FO615,156^AdN,0,0^FWN^FH^FD(206) 850-6299^FS
            ^FO43,306^A0N,30,30^FWN^FH^FDSeattle, WA 98108^FS
            ^FO530,306^A0N,35,45^FWN^FH^FD(US)^FS
            ^FO725,216^AdN,0,0^FWN^FH^FDGround^FS
            ^FO670,238^GB105,10,10^FS
            ^FO670,248^GB10,112,10^FS
            ^FO765,248^GB10,112,10^FS
            ^FO670,360^GB105,10,10^FS
            ^FO476,3^GB4,145,4^FS
            ^FO650,173^A0N,50,55^FWN^FH^FDFedEx^FS
            ^FO690,256^A0N,130,130^FWN^FH^FDG^FS
            ^FO80,771^BY4,2^BCN,290,N,N,N,N^FWN^FD>;>89612019012818215315414^FS
            ^FO135,1083^A0N,25,27^FWN^FH^FD(9612019) 0128182  15315414^FS
            ^FO783,258^A0N,15,15^FWB^FH^FDJ14101402072126^FS
            ^FO10,162^A0N,20,18^FWN^FH^FDTO^FS
            ^FO25,1108^A0N,50,55^FWN^FH^FDGND^FS
            ^FO25,1150^A0N,35,45^FWN^FH^FDPrepaid^FS
            ^FO300,1115^A0N,35,45^FWN^FH^FD^FS
            ^FO300,1149^A0N,35,45^FWN^FH^FD^FS
            ^FO30,428^BY2,2^B7N,10,5,12^FH^FWN^FH^FD[)>_1E01_1D0298108_1D840_1D019_1D012818215315414_1DFDEB_1D0128182_1D119_1D_1D1/1_1D1.0LB_1DN_1D5628 Airport Way S #220_1DSeattle_1DWA_1DMac #220_1E06_1D10ZGD007_1D12Z2068506299_1D23ZN_1D22Z_1CN_1D20Z _1C0_1D31Z                                  _1D33Z  _1D34Z01_1D9K$random_1D26Z5e9d_1C_1D_1E_04^FS
            ^FO250,1219^A0N,20,18^FWN^FH^FDTRCK: 012818215315414^FS
            ^FO150,1239^A0N,20,18^FWN^FH^FDSvcs: GND PPD^FS
            ^FO16,358^A0N,15,15^FWN^FH^FDRef: $random^FS
            ^FO16,373^A0N,15,15^FWN^FH^FDINV: ^FS
            ^FO16,388^A0N,15,15^FWN^FH^FDPO: ^FS
            ^FO406,388^A0N,15,15^FWN^FH^FDDept: ^FS
            ^FO625,495^A0N,20,35^FWN^FH^FD1^FS
            ^FO640,535^A0N,20,35^FWN^FH^FDof^FS
            ^FO625,575^A0N,20,35^FWN^FH^FD1^FS
            ^FO10,1263^AdN,0,0^FWN^FH^FDBilled Weight: 1.0^FS
            ^FO10,1285^AdN,0,0^FWN^FH^FDShipping: ^FS
            ^FO10,1307^AdN,0,0^FWN^FH^FDRef: $random^FS
            ^FO10,1328^AdN,0,0^FWN^FH^FDDeli: 2014-04-29T14:2^FS
            ^FO270,1263^AdN,0,0^FWN^FH^FDSrvc: FEDEX_5FGROUND^FS
            ^PQ1
            ^XZ
            ";

        $entityType = "sales/order";
        $entityId = 1;


        $printJob = Mage::getModel("frans/printJobManager")->addPrintJob($printStationId, $printJobType, $document, $entityType, $entityId);

        return $printJob;
    }

    private function _getrandomstring($length) {

        global $template;
        settype($template, "string");

        $template = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        /* this line can include numbers or not */

        settype($length, "integer");
        settype($rndstring, "string");
        settype($a, "integer");
        settype($b, "integer");

        for ($a = 0; $a <= $length; $a++) {
            $b = rand(0, strlen($template) - 1);
            $rndstring .= $template[$b];
        }

        return $rndstring;
    }

}
