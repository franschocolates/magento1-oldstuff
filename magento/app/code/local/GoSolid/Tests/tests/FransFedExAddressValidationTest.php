<?php

class FransFedExAddressValidationTest extends PHPUnit_Framework_TestCase 
{
	
    var $coreSession;
    var $checkoutSession;
		
	public function setUp()
	{
		# this is required in every test to make sure the magento stuff is built up.
		Mage::app('default');
		Mage::app()->setCurrentStore('default');
		
		$this->coreSession = Mage::getSingleton('core/session');
		$this->checkoutSession = Mage::getSingleton('checkout/session');
	}
	
	public function test_fedex_called_to_get_address_type_on_save()
	{
		// use a known residential address
		$helper = Mage::helper('tests');
		$addressData = $helper->buildAddressArray('Test', 'Person', '1422 18th Avenue', '206.466.6194', '98122');
		$customer = $helper->createCustomer($addressData);
		
		// verify that it now has an address with the address_delivery_type
		$customerAddresses = $customer->getAddressesCollection();
		$this->assertTrue($customerAddresses->count() > 0, "Customer should have an address.");
		$firstAddress = $customerAddresses->getFirstitem();
		$this->assertTrue($firstAddress->hasAddressDeliveryType(), "Should have the address delivery type.");
		$this->assertTrue(strlen($firstAddress->getAddressDeliveryType()) > 0, "Should have the address delivery type set.");
		
		// we only have the is residential logic on a quote address, we can create one and not save it
		$quoteAddress = Mage::getModel('sales/quote_address');
		$quoteAddress->importCustomerAddress($firstAddress);
		$this->assertTrue($quoteAddress->getIsResidential(), "Should now be set as residential");
		
		echo "test_fedex_called_to_get_address_type_on_save passed.";
	}
	
}