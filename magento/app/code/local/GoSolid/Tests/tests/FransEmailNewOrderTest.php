<?php

class FransEmailNewOrderTest extends PHPUnit_Framework_TestCase {
	
	public function setUp()
	{
		# this is required in every test to make sure the magento stuff is built up.
		Mage::app('default');
		
	}
	
	public function test_GoSolid_Frans_email_regular_order()
	{
		$order = Mage::getModel('sales/order')->load(66);
		$this->assertEquals(66, $order->getId(), 'No order loaded.');
		$this->assertEquals(1, $order->sendNewOrderEmail()->getEmailSent(), 'Order email sending failed.');
	}
	
}