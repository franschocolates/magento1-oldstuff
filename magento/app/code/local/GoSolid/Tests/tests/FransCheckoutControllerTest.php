<?php

require_once Mage::getBaseDir('app') . DS. implode(DS, array('code', 'local', 'GoSolid', 'Frans', 'controllers', 'CheckoutController.php'));

class FransCheckoutControllerTest extends PHPUnit_Framework_TestCase 
{
	const SHIP_METHOD_HOME = 'fedex_GROUND_HOME_DELIVERY';
	const SHIP_METHOD_2DAY = 'fedex_FEDEX_2_DAY';
	
    var $coreSession;
    var $checkoutSession;
    var $customerSession;
		
	public function setUp()
	{
		# this is required in every test to make sure the magento stuff is built up.
		Mage::app('default');
		
		$this->coreSession = Mage::getSingleton('core/session');
		$this->checkoutSession = Mage::getSingleton('checkout/session');
		$this->customerSession = Mage::getSingleton('customer/session');
	}
	
	/**
     * Ensures that adding by an existing address works
     * 
     */
	public function test_multiship_addAddress_adds_existing_customer_address()
	{
		# product IDs (as of this test) - 
		# 	188 - 7 pc - 233000-07 - 12.75
		# 	19 - 20 pc - 220240 - 28.50
		$product1 = $this->_getProduct(188);
		$product2 = $this->_getProduct(19);
		
		# construct a quote that we will add an address to 
		$helper = Mage::helper('tests');
		$customer = $helper->createCustomer(2);
		$quote = $helper->createQuote($customer
					, array( array ( $product1->getId() => 1 ) ), self::SHIP_METHOD_2DAY);
					
		$allAddresses = $customer->getAddressesCollection();
		$address2 = $allAddresses->getLastitem();
					
		# try and add the address via the controller
		$controller = $this->_getController($customer, $quote);
		$controller->getRequest()->setParam('customer_address_id', $address2->getId());
		# need the shipping data there, if empty
		$controller->getRequest()->setParam('shipping_address', array( 'empty' => false));
		$controller->addMultishipAddressAction();
		
		# verify there are now 2 shipping addresses.
		$reloadedQuote = Mage::getModel('sales/quote')->setStoreId(1)->load($quote->getId());
		
		# verify the addresses
		$shippingAddresses = $reloadedQuote->getAllShippingAddresses();
		
		$this->assertEquals(2, count($shippingAddresses), "should now have 2 shipping addresses.");
		
		$this->_verifyJsonResponse($controller, $quote);
		
		echo PHP_EOL . "test_multiship_addAddress_adds_existing_customer_address PASSED " . PHP_EOL;
	}
	
	/**
     * Ensures that adding a new address (no customer address) works
     * 
     */
	public function test_multiship_addAddress_adds_new_address()
	{
		# product IDs (as of this test) - 
		# 	188 - 7 pc (Ivory) - 233000-07 - 12.75
		# 	19 - 20 pc - 220240 - 28.50
		$product1 = $this->_getProduct(188);
		$product2 = $this->_getProduct(19);
		
		# construct a quote that we will add an address to 
		$helper = Mage::helper('tests');
		$customer = $helper->createCustomer(2);
		$quote = $helper->createQuote($customer
					, array( array ( $product1->getId() => 1 ) ), self::SHIP_METHOD_2DAY);
		# try and add the address via the controller
		$controller = $this->_getController($customer, $quote);
		# need the shipping data there with our fields
		$shippingInfo = array(
			'firstname' => 'Barrett',
			'lastname' => 'Hogue',
			'street' => array('1422 Airport Way S', 'Line 2'),
			'city' => 'Seattle',
			'region_id' => 62,
			'postcode' => '98122',
			'telephone' => '2067778888',
			'country_id' => 'US',
		);
		$controller->getRequest()->setParam('shipping_address', $shippingInfo);
		$controller->addMultishipAddressAction();
		
		# verify there are now 2 shipping addresses.
		$reloadedQuote = Mage::getModel('sales/quote')->setStoreId(1)->load($quote->getId());
		
		# verify the addresses
		$shippingAddresses = $reloadedQuote->getAllShippingAddresses();
		
		$this->assertEquals(2, count($shippingAddresses), "should now have 2 shipping addresses.");
		
		# now verify it has the data from above
		$quoteAddress2 = $shippingAddresses[1];
		
		$this->assertEquals($shippingInfo['firstname'], $quoteAddress2->getFirstname(), "First name should match input.");
		$this->assertEquals($shippingInfo['lastname'], $quoteAddress2->getLastname(), "Last name should match input.");
		$this->assertEquals($shippingInfo['postcode'], $quoteAddress2->getPostcode(), "Postcode should match input.");
		$this->assertEquals($shippingInfo['telephone'], $quoteAddress2->getTelephone(), "Telephone should match input.");
		
		$this->_verifyJsonResponse($controller, $quote);
		
		echo PHP_EOL . "test_multiship_addAddress_adds_new_address PASSED " . PHP_EOL;
	}
	
	/**
     * Checks that you can set the quantities for items within a multiship order
     * Intention: 
     *	- add multiship guest address
     * 	- build a matrix of 2 addresses & 2 items
     * 	- first item will have a qty of 1 for both addresses
     *  - second item will have a qty of 2 for both addresses
     * 
     * @depends test_multiship_addAddress_adds_new_address
     */
	public function test_unregistered_address_multiship_set_quantities_all_addresses()
	{
		# product IDs (as of this test) - 
		# 	188 - 7 pc (Ivory) - 233000-07 - 12.75
		# 	19 - 20 pc - 220240 - 28.50
		$product1 = $this->_getProduct(188);
		$product2 = $this->_getProduct(19);
		
		# construct a quote that we will add an address to 
		$helper = Mage::helper('tests');
		$customer = $helper->createCustomer(2);
		# we put both items in the quote initially
		$quote = $helper->createQuote($customer
					, array( array ( $product1->getId() => 1, $product2->getId() => 1 ) ), self::SHIP_METHOD_2DAY);
		# try and add the address via the controller
		$controller = $this->_getController($customer, $quote);
		# need the shipping data there with our fields
		$shippingInfo = array(
			'firstname' => 'Two',
			'lastname' => 'Tarts',
			'street' => array('5629 Airport Way S', 'Tarty Tart'),
			'city' => 'Seattle',
			'region_id' => 62,
			'postcode' => '98108',
			'telephone' => '206-767-8012',
			'country_id' => 'US',
		);
		$controller->getRequest()->setParam('shipping_address', $shippingInfo);
		$controller->addMultishipAddressAction();
		
		# verify there are now 2 shipping addresses.
		$reloadedQuote = Mage::getModel('sales/quote')->setStoreId(1)->load($quote->getId());
		
		# verify the addresses
		$shippingAddresses = $reloadedQuote->getAllShippingAddresses();
		
		$this->assertEquals(2, count($shippingAddresses), "should now have 2 shipping addresses.");
		
		$quoteItems = $reloadedQuote->getAllItems();
		$quoteItem1 = $quoteItems[0];
		$quoteItemId1 = $quoteItem1->getId();
		$quoteItem2 = $quoteItems[1];
		$quoteItemId2 = $quoteItem2->getId();
		
		# we assume previous tests have verified the data, in this case, we 
		# will just try and set the quote items
		$quoteAddress1 = $shippingAddresses[0];
		$quoteAddress2 = $shippingAddresses[1];
		
		# now lets add quantity for all items
		$shippingQty = array(
			$quoteAddress1->getId() => array(
				$quoteItemId1 => array( 'qty' => 1),
				$quoteItemId2 => array( 'qty' => 2),
			),
			$quoteAddress2->getId() => array(
				$quoteItemId1 => array( 'qty' => 1),
				$quoteItemId2 => array( 'qty' => 2),
			),
		);
		
		# get the controller again, because that is what would happen in real life
		$controller = $this->_getController($customer, $reloadedQuote);
		$controller->getRequest()->setParam('shipping_info', $shippingQty);
		$controller->saveShippingInfoAction();
		
		# also need to verify the quantities of everything now.
		# reload the quote, just to make sure
		$reloadedQuote = Mage::getModel('sales/quote')->setStoreId(1)->load($quote->getId());
		
		$this->_verifyJsonResponse($controller, $reloadedQuote);
		
		
		# make sure we have the correct quantities
		foreach ($reloadedQuote->getAllItems() as $quoteItem)
		{
			$expectedQty = ($quoteItem->getId() == $quoteItemId1) ? 2 : 4;
			
			$this->assertEquals($expectedQty, $quoteItem->getQty(), "Quantity should match for quote item " . $quoteItem->getId());
			
			# make sure that the addresses have half the quantity
			foreach ($reloadedQuote->getAllShippingAddresses() as $shippingAddress)
			{
				foreach ($shippingAddress->getAllItems() as $addressItem)
				{
					if ($addressItem->getQuoteItemId() == $quoteItem->getId())
					{
						$this->assertEquals($expectedQty / 2, $addressItem->getQty(), "Addresss should have correct quantity for item.");
					}
				}
			}
		}
		
		echo PHP_EOL . "test_unregistered_address_multiship_set_quantities_all_addresses PASSED " . PHP_EOL;
	}
		
	/**
     * Checks that you can set the quantities for items within a multiship order, but with uneven quantities
     * Intention: 
     *	- add multiship guest address
     * 	- build a matrix of 2 addresses & 3 items
     * 	- first item will have a qty of 1 for one address, 3 for another
     *  - second item will have a qty of 0 for first address, 1 for the other
     *  - third item will have a qty of 6 for first address, 0 for the other
     * 
     * @depends test_multiship_addAddress_adds_new_address
     */
	public function test_unregistered_address_multiship_set_uneven_quantities_all_addresses()
	{
		# product IDs (as of this test) - 
		# 	188 - 7 pc (Ivory) - 233000-07 - 12.75
		# 	19 - 20 pc - 220240 - 28.50
		#	142 - 20 pc - classic milk/dark - 220570 - 28.50
		$product1 = $this->_getProduct(188);
		$product2 = $this->_getProduct(19);
		$product3 = $this->_getProduct(142);
		
		# construct a quote that we will add an address to 
		$helper = Mage::helper('tests');
		$customer = $helper->createCustomer(2);
		# we put all items in the quote initially
		$quote = $helper->createQuote($customer
					, array( array ( $product1->getId() => 1, $product2->getId() => 1, $product3->getId() => 1 ) ), self::SHIP_METHOD_2DAY);
		# try and add the address via the controller
		$controller = $this->_getController($customer, $quote);
		# need the shipping data there with our fields
		$shippingInfo = array(
			'firstname' => 'Two',
			'lastname' => 'Tarts',
			'street' => array('5629 Airport Way S', 'Tarty Tart'),
			'city' => 'Seattle',
			'region_id' => 62,
			'postcode' => '98108',
			'telephone' => '206-767-8012',
			'country_id' => 'US',
		);
		$controller->getRequest()->setParam('shipping_address', $shippingInfo);
		$controller->addMultishipAddressAction();
		
		# verify there are now 2 shipping addresses.
		$reloadedQuote = Mage::getModel('sales/quote')->setStoreId(1)->load($quote->getId());
		
		# verify the addresses
		$shippingAddresses = $reloadedQuote->getAllShippingAddresses();
		
		$this->assertEquals(2, count($shippingAddresses), "should now have 2 shipping addresses.");
		
		$quoteItems = $reloadedQuote->getAllItems();
		$quoteItem1 = $quoteItems[0];
		$quoteItemId1 = $quoteItem1->getId();
		$quoteItem2 = $quoteItems[1];
		$quoteItemId2 = $quoteItem2->getId();
		$quoteItem3 = $quoteItems[2];
		$quoteItemId3 = $quoteItem3->getId();
		
		# we assume previous tests have verified the data, in this case, we 
		# will just try and set the quote items
		$quoteAddress1 = $shippingAddresses[0];
		$quoteAddress2 = $shippingAddresses[1];
		
		# now lets add quantity for all items
		$shippingQty = array(
			$quoteAddress1->getId() => array(
				$quoteItemId1 => array( 'qty' => 1),
				$quoteItemId2 => array( 'qty' => 0),
				$quoteItemId3 => array( 'qty' => 6)
			),
			$quoteAddress2->getId() => array(
				$quoteItemId1 => array( 'qty' => 3),
				$quoteItemId2 => array( 'qty' => 1),
				$quoteItemId3 => array( 'qty' => 0)
			),
		);
		
		# get the controller again, because that is what would happen in real life
		$controller = $this->_getController($customer, $reloadedQuote);
		$controller->getRequest()->setParam('shipping_info', $shippingQty);
		$controller->saveShippingInfoAction();
		
		# also need to verify the quantities of everything now.
		# reload the quote, just to make sure
		$reloadedQuote = Mage::getModel('sales/quote')->setStoreId(1)->load($quote->getId());
		
		$this->_verifyJsonResponse($controller, $reloadedQuote);
		
		# make sure we have the correct quantities
		foreach ($reloadedQuote->getAllItems() as $quoteItem)
		{
			switch ($quoteItem->getId())
			{
				case $quoteItemId1:
					$expectedTotalQty = 4;
					$expectedAddressQty = array(1, 3);
					break;
				case $quoteItemId2:
					$expectedTotalQty = 1;
					$expectedAddressQty = array(0, 1);
					break;
				case $quoteItemId3:
					$expectedTotalQty = 6;
					$expectedAddressQty = array(6, 0);
					break;
				default:
					throwException(new Exception('Unknown quote item in all items.'));
			}

			$this->assertEquals($expectedTotalQty, $quoteItem->getQty(), "Quantity should match for quote item " . $quoteItem->getId());
			
			# make sure that the addresses have half the quantity
			# we'll assume they are in the correct order to get the qty from our array
			foreach ($reloadedQuote->getAllShippingAddresses() as $addressIndex => $shippingAddress)
			{
				foreach ($shippingAddress->getAllItems() as $addressItem)
				{
					if ($addressItem->getQuoteItemId() == $quoteItem->getId())
					{
						$this->assertEquals($expectedAddressQty[$addressIndex], $addressItem->getQty(), "Addresss should have correct quantity for item.");
					}
				}
				
			}
		}
		
		echo PHP_EOL . "test_unregistered_address_multiship_set_uneven_quantities_all_addresses PASSED " . PHP_EOL;
	}
		
	/**
     * Checks that you can remove an address and it will remove the address and decrement quantities
     * Intention: 
     *	- add multiship guest address x2
     * 	- build a matrix of 3 addresses & 2 items
     * 	- first item will have a qty of 1, 3, and 1
     *  - second item will have a qty of 0, 1, and 2
     *  - remove last address
     *  - should update quantities/addresses accordingly
     * 
     * @depends test_unregistered_address_multiship_set_uneven_quantities_all_addresses
     */
	public function test_unregistered_address_multiship_remove_address()
	{
		# product IDs (as of this test) - 
		# 	188 - 7 pc (Ivory) - 233000-07 - 12.75
		# 	19 - 20 pc - 220240 - 28.50
		$product1 = $this->_getProduct(188);
		$product2 = $this->_getProduct(19);
		
		# construct a quote that we will add an address to 
		$helper = Mage::helper('tests');
		$customer = $helper->createCustomer(2);
		# we put all items in the quote initially
		$quote = $helper->createQuote($customer
					, array( array ( $product1->getId() => 1, $product2->getId() => 1) ), self::SHIP_METHOD_2DAY);
		# try and add the addresses via the controller
		$controller = $this->_getController($customer, $quote);
		
		# first, add from a customer address (#2)
		$allAddresses = $customer->getAddressesCollection();
		$address2 = $allAddresses->getLastitem();
					
		# try and add the address via the controller
		$controller->getRequest()->setParam('customer_address_id', $address2->getId());
		# need the shipping data there, if empty
		$controller->getRequest()->setParam('shipping_address', array( 'empty' => false));
		$controller->addMultishipAddressAction();
		
		$quote = Mage::getModel('sales/quote')->load($quote->getId());
		
		# make sure it now has 2 addresses
		$this->assertEquals(2, count($quote->getAllShippingAddresses()), "Should now have 2 shipping addresses.");
		
		
		$controller = $this->_getController($customer, $quote);
		
		# now do another random address
		# need the shipping data there with our fields
		$shippingInfoAddr = array(
			'firstname' => 'Two',
			'lastname' => 'Tarts',
			'street' => array('5629 Airport Way S', 'Tarty Tart'),
			'city' => 'Seattle',
			'region_id' => 62,
			'postcode' => '98108',
			'telephone' => '206-767-8012',
			'country_id' => 'US',
		);
		$controller->getRequest()->setParam('shipping_address', $shippingInfoAddr);
		$controller->addMultishipAddressAction();
		
		# verify there are now 3 shipping addresses.
		$reloadedQuote = Mage::getModel('sales/quote')->setStoreId(1)->load($quote->getId());
		
		# verify the addresses
		$shippingAddresses = $reloadedQuote->getAllShippingAddresses();

		$shippingAddress1 = $shippingAddresses[0];
		$shippingAddress2 = $shippingAddresses[1];
		$shippingAddress3 = $shippingAddresses[2];
		
		$this->assertEquals(3, count($shippingAddresses), "should now have 3 shipping addresses.");
		
		$quoteItems = $reloadedQuote->getAllItems();
		$quoteItem1 = $quoteItems[0];
		$quoteItemId1 = $quoteItem1->getId();
		$quoteItem2 = $quoteItems[1];
		$quoteItemId2 = $quoteItem2->getId();
		
		# we assume previous tests have verified the data, in this case, we 
		# will just try and set the quote items
		
		# now lets add quantity for all items
		$shippingQty = array(
			$shippingAddress1->getId() => array(
				$quoteItemId1 => array( 'qty' => 1),
				$quoteItemId2 => array( 'qty' => 0),
			),
			$shippingAddress2->getId() => array(
				$quoteItemId1 => array( 'qty' => 3),
				$quoteItemId2 => array( 'qty' => 1),
			),
			$shippingAddress3->getId() => array(
				$quoteItemId1 => array( 'qty' => 1),
				$quoteItemId2 => array( 'qty' => 2),
			),
		);
		
		# get the controller again, because that is what would happen in real life
		$controller = $this->_getController($customer, $reloadedQuote);
		$controller->getRequest()->setParam('shipping_info', $shippingQty);
		$controller->saveShippingInfoAction();
		
		# also need to verify the quantities of everything now.
		# reload the quote, just to make sure
		$reloadedQuote = Mage::getModel('sales/quote')->setStoreId(1)->load($quote->getId());
		
		$this->_verifyJsonResponse($controller, $reloadedQuote);
		
		# finally, we remove one
		# and then verify the quantities
		$reloadedQuote = Mage::getModel('sales/quote')->setStoreId(1)->load($quote->getId());
		$controller = $this->_getController($customer, $reloadedQuote);
		$controller->getRequest()->setParam('address_id', $shippingAddress3->getId());
		$controller->removeMultishipAddressAction();
		
		$reloadedQuote = Mage::getModel('sales/quote')->setStoreId(1)->load($quote->getId());
		
		# make sure it has the right number of shipping addresses
		$shippingAddresses = $reloadedQuote->getAllShippingAddresses();
		$this->assertEquals(2, count($shippingAddresses), "should be back to 2 addresses.");
		
		# make sure we have the correct quantities
		foreach ($reloadedQuote->getAllItems() as $quoteItem)
		{
			switch ($quoteItem->getId())
			{
				case $quoteItemId1:
					$expectedTotalQty = 4;
					$expectedAddressQty = array(1, 3);
					break;
				case $quoteItemId2:
					$expectedTotalQty = 1;
					$expectedAddressQty = array(0, 1);
					break;
				default:
					throwException(new Exception('Unknown quote item in all items.'));
			}

			$this->assertEquals($expectedTotalQty, $quoteItem->getQty(), "Quantity should match for quote item " . $quoteItem->getId());
			
			# make sure that the addresses have half the quantity
			# we'll assume they are in the correct order to get the qty from our array
			foreach ($reloadedQuote->getAllShippingAddresses() as $addressIndex => $shippingAddress)
			{
				foreach ($shippingAddress->getAllItems() as $addressItem)
				{
					if ($addressItem->getQuoteItemId() == $quoteItem->getId())
					{
						$this->assertEquals($expectedAddressQty[$addressIndex], $addressItem->getQty(), "Addresss should have correct quantity for item.");
					}
				}
				
			}
		}
		
		echo PHP_EOL . "test_unregistered_address_multiship_remove_address PASSED " . PHP_EOL;
	}
		
	private function _verifyJsonResponse($controller, $quote)
	{
		$json = $controller->getResponse();
		$responseArray = Mage::helper('core')->jsonDecode($json);
		
		$data = $responseArray['data'];
		
		$this->_verifyItemData($quote, $data['items']);
		$this->_verifyAddressData($quote, $data['addresses']);
	}
	
	private function _verifyItemData($quote, $itemData)
	{
		foreach ($quote->getAllItems() as $quoteItem)
		{
			$this->assertArrayHasKey($quoteItem->getId(), $itemData, "Should have array item with quote item ID as key.");
			
			$quoteItemData = $itemData[$quoteItem->getId()];
			
			$this->assertEquals($quoteItem->getProductId(), $quoteItemData['product_id'], 'Product ID should be in quote item data.');
			$price = $quoteItem->getPrice();
			$qty = $quoteItem->getQty();
			$this->assertEquals($price, $quoteItemData['price'], 'Price should be in quote item data.');
			$this->assertEquals($qty, $quoteItemData['qty'], 'Qty should be in quote item data.');
			$this->assertEquals($quoteItem->getRowTotal(), $quoteItemData['row_total'], 'Row total should be in quote item data.');
		}
	}
	
	private function _verifyAddressData($quote, $addressData)
	{
		foreach ($quote->getAllShippingAddresses() as $quoteAddress)
		{
			$this->assertArrayHasKey($quoteAddress->getId(), $addressData, "Address should be in address data");
			
			$quoteAddressData = $addressData[$quoteAddress->getId()];
			
			$this->assertEquals($quoteAddress->getFirstname(), $quoteAddressData['firstname'], 'Firstname should match the quote address.');
			$this->assertEquals($quoteAddress->getLastname(), $quoteAddressData['lastname'], 'Lastname should match the quote quote address.');
			$this->assertEquals($quoteAddress->getPostcode(), $quoteAddressData['postcode'], 'Postcode should match the quote address.');
			$this->assertEquals($quoteAddress->getCountryId(), $quoteAddressData['country_id'], 'Country ID should match the quote address.');
		}
	}
	
	private function _getProduct($id)
	{
		return Mage::getModel('catalog/product')->load($id);
	}
	
	private function _getController($customer, $quote)
	{
		# have to set store or ela
		$store = Mage::app()->getStore(1);
		Mage::app()->setCurrentStore($store);
		
		$this->customerSession->setCustomer($customer);
		# if we don't clear the checkout session, it will keep using the same first quote
		$this->checkoutSession->clear();
		$this->checkoutSession->setQuoteId($quote->getId());
		
		# need to build request/response
		$request = new Zend_Controller_Request_Http();
		$response = new Zend_Controller_Response_Http(); 
		
		Mage::getModel('checkout/cart')->setQuote($quote);
		
		return new GoSolid_Frans_CheckoutController($request, $response);
	}
}