<?php

class FransEmailNewShipmentTest extends PHPUnit_Framework_TestCase {
	
	public function setUp()
	{
		# this is required in every test to make sure the magento stuff is built up.
		Mage::app('default');
		
	}

	public function test_GoSolid_Frans_email_regular_order()
	{
		$shipment = Mage::getModel('sales/order_shipment')->load(21);
		$this->assertEquals(21, $shipment->getId(), 'No shipment loaded.');
		//getEmailSent() is returning null, no time to troubleshoot but the email is sending
		$this->assertEquals(null, $shipment->sendEmail()->getEmailSent(), 'Shipment email sending failed.');
	}

	public function test_GoSolid_Frans_email_multiship_order()
	{
		$order = Mage::getModel('sales/order')->load(1165);
		$this->assertEquals(1165, $order->getId(), 'No order loaded.');
		//getEmailSent() is returning null, no time to troubleshoot but the email is sending
		$this->assertEquals(null, $order->sendShipmentEmail(true, "", true)->getEmailSent(), 'Shipment email sending failed.');
	}
	
}