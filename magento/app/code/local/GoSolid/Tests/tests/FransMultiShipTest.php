<?php
require_once Mage::getBaseDir('lib') . DS . 'Stripe' . DS . 'Stripe.php';

class FransMultiShipTest extends PHPUnit_Framework_TestCase 
{
	
	const SHIP_METHOD_HOME = 'fedex_GROUND_HOME_DELIVERY';
	const SHIP_METHOD_2DAY = 'fedex_FEDEX_2_DAY';
	
    var $coreSession;
    var $checkoutSession;
		
	public function setUp()
	{
		# this is required in every test to make sure the magento stuff is built up.
		Mage::app('default');
		Mage::app()->setCurrentStore('default');
		
		$this->coreSession = Mage::getSingleton('core/session');
		$this->checkoutSession = Mage::getSingleton('checkout/session');
	}
	
	public function test_GoSolid_Frans_checkout_type_registered()
	{
		$this->assertTrue(class_exists('GoSolid_Frans_Model_Checkout_Type_Multiship'), 'Checkout type multiship class should exist.');
		
		$multishipModel = Mage::getModel('frans/checkout_type_multiship');
		
		$this->assertInstanceOf('GoSolid_Frans_Model_Checkout_Type_Multiship', $multishipModel, 'Should have a multship model type');
	}
	
	public function test_frans_order_registered_as_order()
	{
		$orderModel = Mage::getModel('sales/order');
		
		$this->assertInstanceOf('GoSolid_Frans_Model_Sales_Order', $orderModel, 'Order should be a frans multiship order.');
	}
	
	public function test_multiship_checkout_type_overwrites_stock_multishipping_checkout()
	{
		$multishippingModel = Mage::getModel('checkout/type_multishipping');
		
		$this->assertInstanceOf('GoSolid_Frans_Model_Checkout_Type_Multiship', $multishippingModel, 'Should override multshipping model type');
	}
	
	/**
	 * @depends test_frans_order_registered_as_order
	 */
	public function test_multiship_child_order_property_correct()
	{
		$order = Mage::getModel('sales/order');
		
		# make it think it's a child
		$order->setMultishipParentId(72);
		
		$this->assertTrue($order->getIsMultishipChildOrder(), "Should be a multiship child order.");
	}
	
	/**
	 * @depends test_frans_order_registered_as_order
	 * @expectedException  GoSolid_Frans_Exception
	 */
	public function test_child_order_send_email_throws_exception()
	{
		$order = Mage::getModel('sales/order');
		
		# make it think it's a child
		$order->setMultishipParentId(72);
		
		# try and send an email - it should fail
		$order->sendNewOrderEmail();
	}
	
	/**
	 * @depends test_frans_order_registered_as_order
	 */
	public function test_child_order_cant_send_email()
	{
		$order = Mage::getModel('sales/order');
		
		# make it think it's a child
		$order->setMultishipParentId(72);
		
		$this->assertFalse($order->getCanSendNewEmailFlag(), "Should not be able to send new email for child order.");
	}
	
	/**
	 * @depends test_frans_order_registered_as_order
	 */
	public function test_parent_order_cancels_all_children()
	{
		$parentOrder = Mage::getModel('sales/order');
		
		$parentId = 1;
		$parentOrder->setIsMultishipParent(true);
		$parentOrder->setId($parentId); // for testing only, should never actually save
		
		$childOrder1 = Mage::getModel('sales/order');
		$childOrder1->setMultishipParentId($parentId);
		$childOrder1->setState(Mage_Sales_Model_Order::STATE_PROCESSING);
		
		$childOrder2 = Mage::getModel('sales/order');
		$childOrder2->setMultishipParentId($parentId);
		$childOrder2->setState(Mage_Sales_Model_Order::STATE_PROCESSING);
		
		$childOrders = array( $childOrder1, $childOrder2 );
		
		$parentOrder->setChildOrders($childOrders);
		
		$parentOrder->cancel();
		
		# make sure child orders were canceled
		$this->assertEquals(Mage_Sales_Model_Order::STATE_CANCELED, $parentOrder->getState()
				, "Parent should be canceled.");
		$this->assertEquals(Mage_Sales_Model_Order::STATE_CANCELED, $childOrder1->getState()
				, "Child 1 should be canceled.");
		$this->assertEquals(Mage_Sales_Model_Order::STATE_CANCELED, $childOrder2->getState()
				, "Child 2 should be canceled.");
		
	}
	
	/**
	 * @depends test_frans_order_registered_as_order
	 */
	public function test_parent_stays_new_when_all_child_orders_processing()
	{
		$parentOrder = Mage::getModel('sales/order');
		
		$parentId = 8;
		$parentOrder->setIsMultishipParent(true);
		$parentOrder->setState(Mage_Sales_Model_Order::STATE_NEW);
		$parentOrder->setId($parentId); // for testing only, should never actually save
		
		$childOrder1 = Mage::getModel('sales/order');
		$childOrder1->setMultishipParentId($parentId);
		$childOrder1->setState(Mage_Sales_Model_Order::STATE_PROCESSING);
		
		$childOrder2 = Mage::getModel('sales/order');
		$childOrder2->setMultishipParentId($parentId);
		$childOrder2->setState(Mage_Sales_Model_Order::STATE_PROCESSING);
		
		$childOrders = array( $childOrder1, $childOrder2 );
		
		$parentOrder->setChildOrders($childOrders);
		
		$parentOrder->update();
		
		# make sure parent is still new, and children remain in their expected state
		$this->assertEquals(Mage_Sales_Model_Order::STATE_NEW, $parentOrder->getState()
				, "Parent should still be new.");
		$this->assertEquals(Mage_Sales_Model_Order::STATE_PROCESSING, $childOrder1->getState()
				, "Child 1 should be processing.");
		$this->assertEquals(Mage_Sales_Model_Order::STATE_PROCESSING, $childOrder2->getState()
				, "Child 2 should be processing.");
	}
	
	/**
	 * @depends test_frans_order_registered_as_order
	 */
	public function test_parent_moves_to_processing_when_one_child_complete()
	{
		$parentOrder = Mage::getModel('sales/order');
		
		$parentId = 8;
		$parentOrder->setIsMultishipParent(true);
		$parentOrder->setState(Mage_Sales_Model_Order::STATE_NEW);
		$parentOrder->setId($parentId); // for testing only, should never actually save
		
		$childOrder1 = Mage::getModel('sales/order');
		$childOrder1->setMultishipParentId($parentId);
		# we can't directly set state to complete, but we can set data.
		$childOrder1->setData('state', Mage_Sales_Model_Order::STATE_COMPLETE);
		
		$childOrder2 = Mage::getModel('sales/order');
		$childOrder2->setMultishipParentId($parentId);
		$childOrder2->setState(Mage_Sales_Model_Order::STATE_PROCESSING);
		
		$childOrders = array( $childOrder1, $childOrder2 );
		
		$parentOrder->setChildOrders($childOrders);
		
		$parentOrder->update();
		
		# make sure parent is now processing, and children remain in their expected state
		$this->assertEquals(Mage_Sales_Model_Order::STATE_PROCESSING, $parentOrder->getState()
				, "Parent should now be processing.");
		$this->assertEquals(Mage_Sales_Model_Order::STATE_COMPLETE, $childOrder1->getState()
				, "Child 1 should still be complete.");
		$this->assertEquals(Mage_Sales_Model_Order::STATE_PROCESSING, $childOrder2->getState()
				, "Child 2 should be processing.");
	}
	
	/**
	 * @depends test_frans_order_registered_as_order
	 */
	public function test_parent_moves_to_canceled_when_all_children_canceled()
	{
		$parentOrder = Mage::getModel('sales/order');
		
		// ID for testing only, should never actually save
		$parentId = 10;
		$parentOrder->setIsMultishipParent(true);
		$parentOrder->setState(Mage_Sales_Model_Order::STATE_PROCESSING);
		$parentOrder->setId($parentId); 
		
		$childOrder1 = Mage::getModel('sales/order');
		$childOrder1->setMultishipParentId($parentId);
		$childOrder1->setState(Mage_Sales_Model_Order::STATE_CANCELED);
		
		$childOrder2 = Mage::getModel('sales/order');
		$childOrder2->setMultishipParentId($parentId);
		$childOrder2->setState(Mage_Sales_Model_Order::STATE_CANCELED);
		
		$childOrders = array( $childOrder1, $childOrder2 );
		
		$parentOrder->setChildOrders($childOrders);
		
		$parentOrder->update();
		
		# make sure parent is now canceled, and children remain in canceled
		$this->assertEquals(Mage_Sales_Model_Order::STATE_CANCELED, $parentOrder->getState()
				, "Parent should now be canceled.");
		$this->assertEquals(Mage_Sales_Model_Order::STATE_CANCELED, $childOrder1->getState()
				, "Child 1 should be canceled.");
		$this->assertEquals(Mage_Sales_Model_Order::STATE_CANCELED, $childOrder2->getState()
				, "Child 2 should be canceled.");
	}
	
	/**
	 * @depends test_frans_order_registered_as_order
	 */
	public function test_parent_moves_to_ready_payment_when_all_children_complete()
	{
		$parentOrder = Mage::getModel('sales/order');
		
		// ID for testing only, should never actually save
		$parentId = 10;
		$parentOrder->setIsMultishipParent(true);
		$parentOrder->setState(Mage_Sales_Model_Order::STATE_PROCESSING);
		$parentOrder->setId($parentId); 
		
		$childOrder1 = Mage::getModel('sales/order');
		$childOrder1->setMultishipParentId($parentId);
		# use set data to trick magento
		$childOrder1->setData('state', Mage_Sales_Model_Order::STATE_COMPLETE);
		
		$childOrder2 = Mage::getModel('sales/order');
		$childOrder2->setMultishipParentId($parentId);
		$childOrder2->setData('state', Mage_Sales_Model_Order::STATE_COMPLETE);
		
		$childOrders = array( $childOrder1, $childOrder2 );
		
		$parentOrder->setChildOrders($childOrders);
		
		$parentOrder->update();
		
		# make sure parent has new status of ready for payment
		$this->assertEquals(Mage_Sales_Model_Order::STATE_PROCESSING, $parentOrder->getState()
				, "Parent should still be processing.");
		$this->assertEquals('ready_for_payment', $parentOrder->getStatus()
				, "Parent should have status of ready for payment.");
		$this->assertEquals(Mage_Sales_Model_Order::STATE_COMPLETE, $childOrder1->getState()
				, "Child 1 should still be complete.");
		$this->assertEquals(Mage_Sales_Model_Order::STATE_COMPLETE, $childOrder2->getState()
				, "Child 2 should still be complete.");
	}
	
	/**
	 * @depends test_frans_order_registered_as_order
	 */
	public function test_parent_moves_to_complete_when_all_children_canceled_or_complete()
	{
		$parentOrder = Mage::getModel('sales/order');
		
		// ID for testing only, should never actually save
		$parentId = 13;
		$parentOrder->setIsMultishipParent(true);
		$parentOrder->setState(Mage_Sales_Model_Order::STATE_PROCESSING);
		$parentOrder->setId($parentId); 
		
		$childOrder1 = Mage::getModel('sales/order');
		$childOrder1->setMultishipParentId($parentId);
		# use set data to trick magento
		$childOrder1->setData('state', Mage_Sales_Model_Order::STATE_COMPLETE);
		
		$childOrder2 = Mage::getModel('sales/order');
		$childOrder2->setMultishipParentId($parentId);
		$childOrder2->setState(Mage_Sales_Model_Order::STATE_CANCELED);
		
		$childOrders = array( $childOrder1, $childOrder2 );
		
		$parentOrder->setChildOrders($childOrders);
		
		$parentOrder->update();
		
		# make sure parent has new status of ready for payment
		$this->assertEquals(Mage_Sales_Model_Order::STATE_PROCESSING, $parentOrder->getState()
				, "Parent should still be processing.");
		$this->assertEquals('ready_for_payment', $parentOrder->getStatus()
				, "Parent should have status of ready for payment.");
				$this->assertEquals(Mage_Sales_Model_Order::STATE_COMPLETE, $childOrder1->getState()
				, "Child 1 should still be complete.");
		$this->assertEquals(Mage_Sales_Model_Order::STATE_CANCELED, $childOrder2->getState()
				, "Child 2 should still be canceled.");
	}
	
	/**
     * @depends test_multiship_checkout_type_overwrites_stock_multishipping_checkout
     */
	public function test_multiship_checkout_type_creates_multiship_parent_and_kids()
	{
		# product IDs (as of this test) - 
		# 	188 - 7 pc - 233000-7 - 12.75
		# 	19 - 20 pc - 220240 - 28.50
		$product1 = $this->_getProduct(188);
		$product2 = $this->_getProduct(19);
		
		$subtotal = $product1->getPrice() + $product2->getPrice();
		
		$helper = Mage::helper('tests');
		$customer = $helper->createCustomer(2);
		$quote = $helper->createQuote($customer
					, array( array ( $product1->getId() => 1 ) ), self::SHIP_METHOD_2DAY, date('Y-m-d', strtotime('+ 5 day')));
		$multishipCheckout = Mage::getModel('checkout/type_multishipping');
		
		# now add the second item in another address
		$allAddresses = $customer->getAddressesCollection();
		$address2 = $allAddresses->getLastitem();
		$helper->addAddressAndItemsToQuote($quote, $address2
					, array ( array ($product2->getId() => 1 )), self::SHIP_METHOD_2DAY, date('Y-m-d', strtotime('+ 5 day')));
		$this->_setSessionQuote($quote);
		
		$payment = Mage::getModel('sales/quote_payment');

		// have to reload the quote for the payment to work properly
		$newQuote = Mage::getModel('sales/quote')->setStoreId(1);
		$newQuote = $newQuote->load($quote->getId());
		$payment->setStoreId(1);
		$payment->setQuote($newQuote);
		$payment->importData(array ('method' => 'checkmo'));
		$payment->save();
		$newQuote->setPayment($payment);
		
		$multishipCheckout = Mage::getModel('checkout/type_multishipping');
		$multishipCheckout->init($newQuote);
		
		$orders = $multishipCheckout->createOrders($this->coreSession, $this->checkoutSession);
		
		# quote should no longer be active
		$currentQuote = Mage::getModel('sales/quote')->setStoreId(1)->load($quote->getId());
		$this->assertEquals(0, $currentQuote->getIsActive(), 'Quote should no longer be active.'); 
		
		# there should be 3 orders (parent and two children) for the quote
		$orders = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('quote_id', $currentQuote->getId())->load();
		$this->assertEquals(3, $orders->count(), "Should be 3 orders for this quote.");
		
		# parent should be the sum of the children
		# let's start out with subtotals
		$ordersArray = $orders->getItems();
		$orderIds = array_keys($ordersArray);
		$parentOrder = $ordersArray[$orderIds[0]];
		$childOrder1 = $ordersArray[$orderIds[1]];
		$childOrder2 = $ordersArray[$orderIds[2]];
		
		# add a gift message to the parent order
		$giftMessage1 = Mage::getModel('giftmessage/message');
		$giftMessage2 = Mage::getModel('giftmessage/message');
		$giftMessage1->setMessage('Testing gift message for first shipment')->save();
		$giftMessage2->setMessage('Testing gift message for second shipment')->save();
		$childOrder1->setGiftMessageId($giftMessage1->getId())->save();
		$childOrder2->setGiftMessageId($giftMessage2->getId())->save();
		
		# make sure the child order costs the same as the product
		$this->assertEquals($product1->getPrice(), $childOrder1->getBaseSubtotal()
								, 'Subtotal of child order 1 should match.');
		$this->assertEquals($product2->getPrice(), $childOrder2->getBaseSubtotal()
								, 'Subtotal of child order 2 should match.');
								
		# make sure child orders are set to processing (not new)
		$this->assertEquals(Mage_Sales_Model_Order::STATE_PROCESSING, $childOrder1->getState(), 
					'Child order 1 should be set to processing by default.');
		$this->assertEquals(Mage_Sales_Model_Order::STATE_PROCESSING, $childOrder2->getState(), 
					'Child order 2 should be set to processing by default.');
		
		# make sure that the shipping total on the order matches the child quote address shipping costs
		# unfortunately we don't have a way to force a specific shipping cost, so we just have to add
		# what was in the quotes, which should have called to the shipping method to get calculated
		$totalShipping = 0;
		foreach ($currentQuote->getAllAddresses() as $address)
		{
			if ($address->getAddressType() == 'shipping')
			{
				$totalShipping += $address->getShippingAmount();
			}
		}
		
		$this->assertEquals($totalShipping, $parentOrder->getShippingAmount(), 
				"Shipping amount on parent should match sum of child orders.");
		
		# grand total should be the product costs + shipping (no tax)
		$expectedGrandTotal = $product1->getPrice() + $product2->getPrice() + $totalShipping;
		$this->assertEquals($expectedGrandTotal, $parentOrder->getGrandTotal(),
				"Grand total should be sum of child products and shipping.");
		
		echo PHP_EOL . "test_multiship_checkout_type_creates_multiship_parent_and_kids PASSED " . PHP_EOL;
	}
	
	public function test_multiship_checkout_type_multiple_one_address_single_other()
	{
		# product IDs (as of this test) - 
		# 	188 - 7 pc - 233000-7 - 12.75
		# 	4 - 20 pc - 220525 - 28.50
		# 	
		$product1 = $this->_getProduct(4);
		$product2 = $this->_getProduct(188);
		
		$product1price = $product1->getPrice();
		$product2price = $product2->getPrice();
		
		// Address #1 will have the 20 pc (product 1)
		// Address #2 will have a 20 pc (product 1) and 3x 7 pc (product 2)
		
		$subtotal = $product1->getPrice() + $product2->getPrice();
		
		$helper = Mage::helper('tests');
		$customer = $helper->createCustomer(2);
		$quote = $helper->createQuote($customer
					, array( array ( $product1->getId() => 1 ) ), self::SHIP_METHOD_2DAY);
		
		$this->_setSessionQuote($quote);
		
		# now add the second item in another address
		$allAddresses = $customer->getAddressesCollection();
		$address2 = $allAddresses->getLastitem();
		$helper->addAddressAndItemsToQuote($quote, $address2
					, array ( array ($product1->getId() => 1, $product2->getId() => 3 )), self::SHIP_METHOD_2DAY);

					
		$payment = Mage::getModel('sales/quote_payment');

		// have to reload the quote for the payment to work properly
		$newQuote = Mage::getModel('sales/quote')->setStoreId(1);
		$newQuote = $newQuote->load($quote->getId());
		$payment->setStoreId(1);
		$payment->setQuote($newQuote);
		$payment->importData(array ('method' => 'checkmo'));
		$payment->save();
		$newQuote->setPayment($payment);
		
		$multishipCheckout = Mage::getModel('checkout/type_multishipping');
		$multishipCheckout->init($newQuote);
		
		$orders = $multishipCheckout->createOrders($this->coreSession, $this->checkoutSession);
		
		# quote should no longer be active
		$currentQuote = Mage::getModel('sales/quote')->setStoreId(1)->load($quote->getId());
		$this->assertEquals(0, $currentQuote->getIsActive(), 'Quote should no longer be active.'); 
		
		# there should be 3 orders (parent and two children) for the quote
		$orders = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('quote_id', $currentQuote->getId())->load();
		$this->assertEquals(3, $orders->count(), "Should be 3 orders for this quote.");
		
		# parent should be the sum of the children
		# let's start out with subtotals
		$ordersArray = $orders->getItems();
		$orderIds = array_keys($ordersArray);
		$parentOrder = $ordersArray[$orderIds[0]];
		$childOrder1 = $ordersArray[$orderIds[1]];
		$childOrder2 = $ordersArray[$orderIds[2]];
				
		# make sure the child order costs the same as the product
		$this->assertEquals($product1->getPrice(), $childOrder1->getBaseSubtotal()
								, 'Subtotal of child order 1 should match.');
		$expectedSubtotalOrder2 = $product1price + ($product2price * 3);
		$this->assertEquals($expectedSubtotalOrder2, $childOrder2->getBaseSubtotal()
								, 'Subtotal of child order 2 should match.');
								
		# make sure that the shipping total on the order matches the child quote address shipping costs
		# unfortunately we don't have a way to force a specific shipping cost, so we just have to add
		# what was in the quotes, which should have called to the shipping method to get calculated
		$totalShipping = 0;
		foreach ($currentQuote->getAllAddresses() as $address)
		{
			if ($address->getAddressType() == 'shipping')
			{
				$totalShipping += $address->getShippingAmount();
			}
		}
		
		$this->assertEquals($totalShipping, $parentOrder->getShippingAmount(), 
				"Shipping amount on parent should match sum of child orders.");
		
		# grand total should be the product costs + shipping (no tax)
		$expectedGrandTotal = $product1->getPrice() + $expectedSubtotalOrder2 + $totalShipping;
		$this->assertEquals($expectedGrandTotal, $parentOrder->getGrandTotal(),
				"Grand total should be sum of child products and shipping.");

		echo PHP_EOL . "test_multiship_checkout_type_multiple_one_address_single_other PASSED " . PHP_EOL;
	}
	
	public function test_multiship_checkout_type_multiple_items_multiple_addressess()
	{
		# product IDs (as of this test) - 
		# 	188 - 7 pc - 233000-7 - 12.75
		# 	4 - 20 pc - 220525 - 28.50
		# 	5 - 40 pc gray & smoked - 220268 - 54.00
		$product1 = $this->_getProduct(4);
		$product2 = $this->_getProduct(188);
		$product3 = $this->_getProduct(5);
		
		$product1price = $product1->getPrice();
		$product2price = $product2->getPrice();
		$product3price = $product3->getPrice();
		
		// Address #1 will have the 20 pc (product 1) & 2x 40 pc (product 3)
		// Address #2 will have a 20 pc (product 1) and 3x 7 pc (product 2)
		
		$helper = Mage::helper('tests');
		$customer = $helper->createCustomer(2);
		$quote = $helper->createQuote($customer
					, array( array ( $product1->getId() => 1, $product3->getId() => 2 ) ), self::SHIP_METHOD_2DAY);
		$this->_setSessionQuote($quote);
					
		# now add the second item in another address
		$allAddresses = $customer->getAddressesCollection();
		$address2 = $allAddresses->getLastitem();
		$helper->addAddressAndItemsToQuote($quote, $address2
					, array ( array ($product1->getId() => 1, $product2->getId() => 3 )), self::SHIP_METHOD_2DAY);

					
		$payment = Mage::getModel('sales/quote_payment');

		// have to reload the quote for the payment to work properly
		$newQuote = Mage::getModel('sales/quote')->setStoreId(1);
		$newQuote = $newQuote->load($quote->getId());
		$payment->setStoreId(1);
		$payment->setQuote($newQuote);
		$payment->importData(array ('method' => 'checkmo'));
		$payment->save();
		$newQuote->setPayment($payment);
		
		$multishipCheckout = Mage::getModel('checkout/type_multishipping');
		$multishipCheckout->init($newQuote);
		
		$orders = $multishipCheckout->createOrders($this->coreSession, $this->checkoutSession);
		
		# quote should no longer be active
		$currentQuote = Mage::getModel('sales/quote')->setStoreId(1)->load($quote->getId());
		$this->assertEquals(0, $currentQuote->getIsActive(), 'Quote should no longer be active.'); 
		
		# there should be 3 orders (parent and two children) for the quote
		$orders = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('quote_id', $currentQuote->getId())->load();
		$this->assertEquals(3, $orders->count(), "Should be 3 orders for this quote.");
		
		# parent should be the sum of the children
		# let's start out with subtotals
		$ordersArray = $orders->getItems();
		$orderIds = array_keys($ordersArray);
		$parentOrder = $ordersArray[$orderIds[0]];
		$childOrder1 = $ordersArray[$orderIds[1]];
		$childOrder2 = $ordersArray[$orderIds[2]];
				
		# make sure the child order costs the same as the product
		$expectedSubtotalOrder1 = $product1price + ($product3price * 2);
		$this->assertEquals($expectedSubtotalOrder1, $childOrder1->getBaseSubtotal()
								, 'Subtotal of child order 1 should match.');
		$expectedSubtotalOrder2 = $product1price + ($product2price * 3);
		$this->assertEquals($expectedSubtotalOrder2, $childOrder2->getBaseSubtotal()
								, 'Subtotal of child order 2 should match.');
								
		# make sure that the shipping total on the order matches the child quote address shipping costs
		# unfortunately we don't have a way to force a specific shipping cost, so we just have to add
		# what was in the quotes, which should have called to the shipping method to get calculated
		$totalShipping = 0;
		foreach ($currentQuote->getAllAddresses() as $address)
		{
			if ($address->getAddressType() == 'shipping')
			{
				$totalShipping += $address->getShippingAmount();
			}
		}
		
		$this->assertEquals($totalShipping, $parentOrder->getShippingAmount(), 
				"Shipping amount on parent should match sum of child orders.");
		
		# grand total should be the product costs + shipping (no tax)
		$expectedGrandTotal = $expectedSubtotalOrder1 + $expectedSubtotalOrder2 + $totalShipping;
		$this->assertEquals($expectedGrandTotal, $parentOrder->getGrandTotal(),
				"Grand total should be sum of child products and shipping.");

		echo PHP_EOL . "test_multiship_checkout_type_multiple_items_multiple_addessess PASSED " . PHP_EOL;
	}
	
	public function test_multiship_checkout_type_multiple_address_single_cancelation()
	{
		# product IDs (as of this test) - 
		# 	4 - 20 pc - 220525 - 28.50
		# 	161 - Grand Duo (red) - 221830 - $80
		# 	126 - 34 pc red box - 232600 - $50
		$product1 = $this->_getProduct(4);
		$product2 = $this->_getProduct(161);
		$product3 = $this->_getProduct(126);
		
		$product1price = $product1->getPrice();
		$product2price = $product2->getPrice();
		$product3price = $product3->getPrice();
		
		# Address #1 will have the 20 pc (product 1)
		# Address #2 will have a Grand Duo
		# Address #3 will have a 34 pc
		
		$helper = Mage::helper('tests');
		$customer = $helper->createCustomer(3);
		$quote = $helper->createQuote($customer
					, array( array ( $product1->getId() => 1 ) ), self::SHIP_METHOD_2DAY);
		$this->_setSessionQuote($quote);
					
		# now add the second item in another address
		$allAddresses = $customer->getAddressesCollection();
		$addressArray = array_values($allAddresses->getItems());
				
		$address2 = $addressArray[2];
		echo "Address2 ID: " . $address2->getId() . PHP_EOL;
		$helper->addAddressAndItemsToQuote($quote, $address2
					, array ( array ($product2->getId() => 1 )), self::SHIP_METHOD_2DAY);

		# and finally the 3rd item in another address
		$address3 = $allAddresses->getLastitem();
		$helper->addAddressAndItemsToQuote($quote, $address3
					, array ( array ($product3->getId() => 1 )), self::SHIP_METHOD_2DAY);

					
		$payment = Mage::getModel('sales/quote_payment');

		// have to reload the quote for the payment to work properly
		$newQuote = Mage::getModel('sales/quote')->setStoreId(1);
		$newQuote = $newQuote->load($quote->getId());
		$payment->setStoreId(1);
		$payment->setQuote($newQuote);
		$payment->importData(array ('method' => 'checkmo'));
		$payment->save();
		$newQuote->setPayment($payment);
		
		$multishipCheckout = Mage::getModel('checkout/type_multishipping');
		$multishipCheckout->init($newQuote);
		
		$orders = $multishipCheckout->createOrders($this->coreSession, $this->checkoutSession);
		
		# quote should no longer be active
		$currentQuote = Mage::getModel('sales/quote')->setStoreId(1)->load($quote->getId());
		$this->assertEquals(0, $currentQuote->getIsActive(), 'Quote should no longer be active.'); 
		
		# there should be 3 orders (parent and two children) for the quote
		$orders = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('quote_id', $currentQuote->getId())->load();
		$this->assertEquals(4, $orders->count(), "Should be 4 orders for this quote.");
		
		# parent should be the sum of the children
		# let's start out with subtotals
		$ordersArray = $orders->getItems();
		$orderIds = array_keys($ordersArray);
		$parentOrder = $ordersArray[$orderIds[0]];
		$childOrder1 = $ordersArray[$orderIds[1]];
		$childOrder2 = $ordersArray[$orderIds[2]];
		$childOrder3 = $ordersArray[$orderIds[3]];
				
		# make sure the child order costs the same as the product
		$expectedSubtotalOrder1 = $product1price ;
		$this->assertEquals($expectedSubtotalOrder1, $childOrder1->getBaseSubtotal()
								, 'Subtotal of child order 1 should match.');
		$expectedSubtotalOrder2 = $product2price;
		$this->assertEquals($expectedSubtotalOrder2, $childOrder2->getBaseSubtotal()
								, 'Subtotal of child order 2 should match.');
		$expectedSubtotalOrder3 = $product3price;
		$this->assertEquals($expectedSubtotalOrder3, $childOrder3->getBaseSubtotal()
								, 'Subtotal of child order 3 should match.');
								
								
		# make sure that the shipping total on the order matches the child quote address shipping costs
		# unfortunately we don't have a way to force a specific shipping cost, so we just have to add
		# what was in the quotes, which should have called to the shipping method to get calculated
		$totalShipping = 0;
		$canceledShipping = 0;
		foreach ($currentQuote->getAllAddresses() as $address)
		{
			if ($address->getAddressType() == 'shipping')
			{
				$totalShipping += $address->getShippingAmount();
				
				echo "Address ID: " . $address->getId() . PHP_EOL;
				if ($address->getCustomerAddressId() == $address2->getId())
				{
					$canceledShipping = $address->getShippingAmount();
					echo "Canceled shipping amount is $canceledShipping" . PHP_EOL;
				}
			}
		}
		
		$this->assertEquals($totalShipping, $parentOrder->getShippingAmount(), 
				"Shipping amount on parent should match sum of child orders.");
		
		# grand total should be the product costs + shipping (no tax)
		$expectedGrandTotal = $expectedSubtotalOrder1 + $expectedSubtotalOrder2 + $expectedSubtotalOrder3  
								+ $totalShipping;
		$this->assertEquals($expectedGrandTotal, $parentOrder->getGrandTotal(),
				"Grand total should be sum of child products and shipping.");

		# now try canceling one of the sub orders (#2)
		# to make sure everything is recalculated on the parent correctly
		echo "Child order ID: " . $childOrder2->getId() . PHP_EOL;
		$childOrder2->cancel()->save();
		
		# reload both orders to make sure they saved correctly
		$canceledOrder = Mage::getModel('sales/order')->load($childOrder2->getId());
		$parentOrder = Mage::getModel('sales/order')->load($parentOrder->getId());

		$this->assertTrue($canceledOrder->isCanceled(), "Should be marked as canceled.");
		
		$this->assertEquals($canceledShipping, $canceledOrder->getShippingCanceled(), 'Should have the correct amount of shipping canceled.');
		
		$expectedNewGrandTotal = $expectedGrandTotal - $expectedSubtotalOrder2 - $canceledShipping;
		$this->assertEquals($canceledShipping, $parentOrder->getShippingCanceled(), 'Parent order should have correct amount of canceled shipping.');
		$this->assertEquals($expectedNewGrandTotal, $parentOrder->getGrandTotal(), "Should have the correct grand total.");
		
		echo PHP_EOL . "test_multiship_checkout_type_multiple_address_single_cancelation PASSED " . PHP_EOL;
	}

	/**
	 * @depends test_multiship_checkout_type_creates_multiship_parent_and_kids
	 */
	public function test_multiship_order_charged_once_stripe()
	{
		# product IDs (as of this test) - 
		# 	188 - 7 pc - 233000-7 - 12.75
		# 	19 - 20 pc - 220240 - 28.50
		$product1 = $this->_getProduct(188);
		$product2 = $this->_getProduct(19);
		
		$subtotal = $product1->getPrice() + $product2->getPrice();
		
		$helper = Mage::helper('tests');
		$customer = $helper->createCustomer(2);
		$quote = $helper->createQuote($customer
					, array( array ( $product1->getId() => 1 ) ), self::SHIP_METHOD_2DAY);
		$this->_setSessionQuote($quote);
		$multishipCheckout = Mage::getModel('checkout/type_multishipping');
		
		# now add the second item in another address
		$allAddresses = $customer->getAddressesCollection();
		$address2 = $allAddresses->getLastitem();
		$helper->addAddressAndItemsToQuote($quote, $address2
					, array ( array ($product2->getId() => 1 )), self::SHIP_METHOD_2DAY);
		#Mage::getSingleton('checkout/session')->setQuote($quote);
		
		$payment = Mage::getModel('sales/quote_payment');

		// have to reload the quote for the payment to work properly
		$newQuote = Mage::getModel('sales/quote')->setStoreId(1);
		$newQuote = $newQuote->load($quote->getId());
		
		# create a stripe token for the payment
		$this->_setStripeConfig();
		
		$token = Stripe_Token::create(array(
  			"card" => array(
    		"number" => "4242424242424242",
    		"exp_month" => 11,
    		"exp_year" => 2014,
    		"cvc" => "314"
  			)
		));
		
		$payment->setStoreId(1);
		$payment->setQuote($newQuote);
		$payment->importData(array ('method' => 'stripe', 'stripe_token' => $token->id, 'exp_month' => 11, 'exp_year' => 2019));
		$payment->save();
		$newQuote->setPayment($payment);
		
		$multishipCheckout = Mage::getModel('checkout/type_multishipping');
		$multishipCheckout->init($newQuote);
		
		$orders = $multishipCheckout->createOrders($this->coreSession, $this->checkoutSession);
		
		# quote should no longer be active
		$currentQuote = Mage::getModel('sales/quote')->setStoreId(1)->load($quote->getId());
		$this->assertEquals(0, $currentQuote->getIsActive(), 'Quote should no longer be active.'); 
		
		# there should be 3 orders (parent and two children) for the quote
		$orders = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('quote_id', $currentQuote->getId())->load();
		$this->assertEquals(3, $orders->count(), "Should be 3 orders for this quote.");
		
		# parent should be the sum of the children
		# let's start out with subtotals
		$ordersArray = $orders->getItems();
		$orderIds = array_keys($ordersArray);
		$parentOrder = $ordersArray[$orderIds[0]];
		$childOrder1 = $ordersArray[$orderIds[1]];
		$childOrder2 = $ordersArray[$orderIds[2]];
				
		# make sure the child order costs the same as the product
		$this->assertEquals($product1->getPrice(), $childOrder1->getBaseSubtotal()
								, 'Subtotal of child order 1 should match.');
		$this->assertEquals($product2->getPrice(), $childOrder2->getBaseSubtotal()
								, 'Subtotal of child order 2 should match.');
								
		# make sure child orders are set to processing (not new)
		$this->assertEquals(Mage_Sales_Model_Order::STATE_PROCESSING, $childOrder1->getState(), 
					'Child order 1 should be set to processing by default.');
		$this->assertEquals(Mage_Sales_Model_Order::STATE_PROCESSING, $childOrder2->getState(), 
					'Child order 2 should be set to processing by default.');
		
		# make sure that the shipping total on the order matches the child quote address shipping costs
		# unfortunately we don't have a way to force a specific shipping cost, so we just have to add
		# what was in the quotes, which should have called to the shipping method to get calculated
		$totalShipping = 0;
		foreach ($currentQuote->getAllAddresses() as $address)
		{
			if ($address->getAddressType() == 'shipping')
			{
				$totalShipping += $address->getShippingAmount();
			}
		}
		
		$this->assertEquals($totalShipping, $parentOrder->getShippingAmount(), 
				"Shipping amount on parent should match sum of child orders.");
		
		# grand total should be the product costs + shipping (no tax)
		$expectedGrandTotal = $product1->getPrice() + $product2->getPrice() + $totalShipping;
		$this->assertEquals($expectedGrandTotal, $parentOrder->getGrandTotal(),
				"Grand total should be sum of child products and shipping.");
		
		echo PHP_EOL . "test_multiship_order_charged_once_stripe PASSED " . PHP_EOL;
	}
	
	/**
	 * @depends test_multiship_checkout_type_creates_multiship_parent_and_kids
	 */
	public function test_multiship_order_stripe_multiple_qty()
	{
		# product IDs (as of this test) - 
		# 	188 - 7 pc - 233000-7 - 12.75
		# 	19 - 20 pc - 220240 - 28.50
		$product1 = $this->_getProduct(188);
		$product2 = $this->_getProduct(19);
		
		$subtotal = $product1->getPrice() * 2 + $product2->getPrice() * 5;
		
		$helper = Mage::helper('tests');
		$customer = $helper->createCustomer(2);
		$quote = $helper->createQuote($customer
					, array( array ( $product1->getId() => 2 ), array( $product2->getId() => 3) ), self::SHIP_METHOD_2DAY);
		$this->_setSessionQuote($quote);
		$multishipCheckout = Mage::getModel('checkout/type_multishipping');
		
		# now add the second item in another address
		$allAddresses = $customer->getAddressesCollection();
		$address2 = $allAddresses->getLastitem();
		$helper->addAddressAndItemsToQuote($quote, $address2
					, array ( array ($product2->getId() => 2 )), self::SHIP_METHOD_2DAY);
		#Mage::getSingleton('checkout/session')->setQuote($quote);
		
		$payment = Mage::getModel('sales/quote_payment');

		// have to reload the quote for the payment to work properly
		$newQuote = Mage::getModel('sales/quote')->setStoreId(1);
		$newQuote = $newQuote->load($quote->getId());
		
		# create a stripe token for the payment
		$this->_setStripeConfig();
		
		$token = Stripe_Token::create(array(
  			"card" => array(
    		"number" => "4242424242424242",
    		"exp_month" => 11,
    		"exp_year" => 2014,
    		"cvc" => "314"
  			)
		));
		
		$payment->setStoreId(1);
		$payment->setQuote($newQuote);
		$payment->importData(array ('method' => 'stripe', 'stripe_token' => $token->id, 'exp_month' => 11, 'exp_year' => 2019));
		$payment->save();
		$newQuote->setPayment($payment);
		
		$multishipCheckout = Mage::getModel('checkout/type_multishipping');
		$multishipCheckout->init($newQuote);
		
		$orders = $multishipCheckout->createOrders($this->coreSession, $this->checkoutSession);
		
		# quote should no longer be active
		$currentQuote = Mage::getModel('sales/quote')->setStoreId(1)->load($quote->getId());
		$this->assertEquals(0, $currentQuote->getIsActive(), 'Quote should no longer be active.'); 
		
		# there should be 3 orders (parent and two children) for the quote
		$orders = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('quote_id', $currentQuote->getId())->load();
		$this->assertEquals(3, $orders->count(), "Should be 3 orders for this quote.");
		
		# parent should be the sum of the children
		# let's start out with subtotals
		$ordersArray = $orders->getItems();
		$orderIds = array_keys($ordersArray);
		$parentOrder = $ordersArray[$orderIds[0]];
		$childOrder1 = $ordersArray[$orderIds[1]];
		$childOrder2 = $ordersArray[$orderIds[2]];
				
		# make sure the child order costs the same as the product
		$this->assertEquals($product1->getPrice() * 2 + $product2->getPrice() * 3, $childOrder1->getBaseSubtotal()
								, 'Subtotal of child order 1 should match.');
		$this->assertEquals($product2->getPrice() * 2, $childOrder2->getBaseSubtotal()
								, 'Subtotal of child order 2 should match.');
								
		# make sure child orders are set to processing (not new)
		$this->assertEquals(Mage_Sales_Model_Order::STATE_PROCESSING, $childOrder1->getState(), 
					'Child order 1 should be set to processing by default.');
		$this->assertEquals(Mage_Sales_Model_Order::STATE_PROCESSING, $childOrder2->getState(), 
					'Child order 2 should be set to processing by default.');
		
		# make sure that the shipping total on the order matches the child quote address shipping costs
		# unfortunately we don't have a way to force a specific shipping cost, so we just have to add
		# what was in the quotes, which should have called to the shipping method to get calculated
		$totalShipping = 0;
		foreach ($currentQuote->getAllAddresses() as $address)
		{
			if ($address->getAddressType() == 'shipping')
			{
				$totalShipping += $address->getShippingAmount();
			}
		}
		
		$this->assertEquals($totalShipping, $parentOrder->getShippingAmount(), 
				"Shipping amount on parent should match sum of child orders.");
		
		# grand total should be the product costs + shipping (no tax)
		$expectedGrandTotal = $product1->getPrice() * 2 + $product2->getPrice() * 5 + $totalShipping;
		$this->assertEquals($expectedGrandTotal, $parentOrder->getGrandTotal(),
				"Grand total should be sum of child products and shipping.");
		
		echo PHP_EOL . "test_multiship_order_stripe_multiple_qty PASSED " . PHP_EOL;
	}
	
	/**
	 * @depends test_multiship_order_charged_once_stripe
	 */
	public function test_multiship_order_not_created_when_payment_fails()
	{
		# product IDs (as of this test) - 
		# 	188 - 7 pc - 233000-7 - 12.75
		# 	19 - 20 pc - 220240 - 28.50
		$product1 = $this->_getProduct(188);
		$product2 = $this->_getProduct(19);
		
		$subtotal = $product1->getPrice() + $product2->getPrice();
		
		$helper = Mage::helper('tests');
		$customer = $helper->createCustomer(2);
		$quote = $helper->createQuote($customer
					, array( array ( $product1->getId() => 1 ) ), self::SHIP_METHOD_2DAY);
		$this->_setSessionQuote($quote);
		$multishipCheckout = Mage::getModel('checkout/type_multishipping');
		
		# now add the second item in another address
		$allAddresses = $customer->getAddressesCollection();
		$address2 = $allAddresses->getLastitem();
		$helper->addAddressAndItemsToQuote($quote, $address2
					, array ( array ($product2->getId() => 1 )), self::SHIP_METHOD_2DAY);
		#Mage::getSingleton('checkout/session')->setQuote($quote);
		
		$payment = Mage::getModel('sales/quote_payment');

		// have to reload the quote for the payment to work properly
		$newQuote = Mage::getModel('sales/quote')->setStoreId(1);
		$newQuote = $newQuote->load($quote->getId());
		
		# create a stripe token for the payment
		$this->_setStripeConfig();
		
		$token = Stripe_Token::create(array(
  			"card" => array(
    		"number" => "4000000000000002", # this card will be declined as per https://stripe.com/docs/testing
    		"exp_month" => 11,
    		"exp_year" => 2019,
    		"cvc" => "314"
  			)
		));
		
		$payment->setStoreId(1);
		$payment->setQuote($newQuote);
		$payment->importData(array ('method' => 'stripe', 'stripe_token' => $token->id, 'exp_month' => 11, 'exp_year' => 2019));
		$payment->save();
		$newQuote->setPayment($payment);
		
		$multishipCheckout = Mage::getModel('checkout/type_multishipping');
		$multishipCheckout->init($newQuote);
		
		$exception = false;
		try {
			$orders = $multishipCheckout->createOrders($this->coreSession, $this->checkoutSession);
		}
		catch (Exception $e)
		{
			$exception = true;
		} 

		$this->assertTrue($exception, 'Should have thrown an exception when creating orders.');
		
		# quote should still be active
		$currentQuote = Mage::getModel('sales/quote')->setStoreId(1)->load($quote->getId());
		$this->assertEquals(1, $currentQuote->getIsActive(), 'Quote should still be active.');
	
		# make sure there are no orders for this quote
		$nonExistentOrder = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('quote_id', $quote->getId());
		$orderCount = $nonExistentOrder->count();
		$this->assertEquals(0, $orderCount, "Should be no orders for quote {$quote->getId()}");
		
		echo PHP_EOL . "test_multiship_order_not_created_when_payment_fails PASSED " . PHP_EOL;
	}
	
	public function test_multiship_3_child_orders_cancel_one_send_two_stripe()
	{
		# product IDs (as of this test) - 
		# 	4 - 20 pc - 220525 - 28.50
		# 	161 - Grand Duo (red) - 221830 - $80
		# 	126 - 34 pc red box - 232600 - $50
		$product1 = $this->_getProduct(4);
		$product2 = $this->_getProduct(161);
		$product3 = $this->_getProduct(126);
		
		$product1price = $product1->getPrice();
		$product2price = $product2->getPrice();
		$product3price = $product3->getPrice();
		
		# Address #1 will have the 20 pc (product 1)
		# Address #2 will have a Grand Duo
		# Address #3 will have a 34 pc
		
		$helper = Mage::helper('tests');
		$customer = $helper->createCustomer(3);
		$quote = $helper->createQuote($customer
					, array( array ( $product1->getId() => 1 ) ), self::SHIP_METHOD_2DAY);
		$this->_setSessionQuote($quote);
					
		# now add the second item in another address
		$allAddresses = $customer->getAddressesCollection();
		$addressArray = array_values($allAddresses->getItems());
				
		$address2 = $addressArray[2];
		echo "Address2 ID: " . $address2->getId() . PHP_EOL;
		$helper->addAddressAndItemsToQuote($quote, $address2
					, array ( array ($product2->getId() => 1 )), self::SHIP_METHOD_2DAY);

		# and finally the 3rd item in another address
		$address3 = $allAddresses->getLastitem();
		$helper->addAddressAndItemsToQuote($quote, $address3
					, array ( array ($product3->getId() => 1 )), self::SHIP_METHOD_2DAY);

		$payment = Mage::getModel('sales/quote_payment');

		// have to reload the quote for the payment to work properly
		$newQuote = Mage::getModel('sales/quote')->setStoreId(1);
		$newQuote = $newQuote->load($quote->getId());
		
		# create a stripe token for the payment
		$this->_setStripeConfig();
		
		$token = Stripe_Token::create(array(
  			"card" => array(
    		"number" => "4242424242424242",
    		"exp_month" => 11,
    		"exp_year" => 2014,
    		"cvc" => "314"
  			)
		));
		
		$payment->setStoreId(1);
		$payment->setQuote($newQuote);
		$payment->importData(array ('method' => 'stripe', 'stripe_token' => $token->id, 'exp_month' => 11, 'exp_year' => 2019));
		$payment->save();
		$newQuote->setPayment($payment);
							
		$multishipCheckout = Mage::getModel('checkout/type_multishipping');
		$multishipCheckout->init($newQuote);
		
		$orders = $multishipCheckout->createOrders($this->coreSession, $this->checkoutSession);
		
		# quote should no longer be active
		$currentQuote = Mage::getModel('sales/quote')->setStoreId(1)->load($quote->getId());
		$this->assertEquals(0, $currentQuote->getIsActive(), 'Quote should no longer be active.'); 
		
		# there should be 3 orders (parent and two children) for the quote
		$orders = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('quote_id', $currentQuote->getId())->load();
		$this->assertEquals(4, $orders->count(), "Should be 4 orders for this quote.");
		
		# parent should be the sum of the children
		# let's start out with subtotals
		$ordersArray = $orders->getItems();
		$orderIds = array_keys($ordersArray);
		$parentOrder = $ordersArray[$orderIds[0]];
		$childOrder1 = $ordersArray[$orderIds[1]];
		$childOrder2 = $ordersArray[$orderIds[2]];
		$childOrder3 = $ordersArray[$orderIds[3]];
				
		# make sure the child order costs the same as the product
		$expectedSubtotalOrder1 = $product1price ;
		$this->assertEquals($expectedSubtotalOrder1, $childOrder1->getBaseSubtotal()
								, 'Subtotal of child order 1 should match.');
		$expectedSubtotalOrder2 = $product2price;
		$this->assertEquals($expectedSubtotalOrder2, $childOrder2->getBaseSubtotal()
								, 'Subtotal of child order 2 should match.');
		$expectedSubtotalOrder3 = $product3price;
		$this->assertEquals($expectedSubtotalOrder3, $childOrder3->getBaseSubtotal()
								, 'Subtotal of child order 3 should match.');
								
								
		# make sure that the shipping total on the order matches the child quote address shipping costs
		# unfortunately we don't have a way to force a specific shipping cost, so we just have to add
		# what was in the quotes, which should have called to the shipping method to get calculated
		$totalShipping = 0;
		$canceledShipping = 0;
		foreach ($currentQuote->getAllAddresses() as $address)
		{
			if ($address->getAddressType() == 'shipping')
			{
				$totalShipping += $address->getShippingAmount();
				
				if ($address->getCustomerAddressId() == $address2->getId())
				{
					$canceledShipping = $address->getShippingAmount();
					echo "Canceled shipping amount is $canceledShipping" . PHP_EOL;
				}
			}
		}
		
		$this->assertEquals($totalShipping, $parentOrder->getShippingAmount(), 
				"Shipping amount on parent should match sum of child orders.");
		
		# grand total should be the product costs + shipping (no tax)
		$expectedGrandTotal = $expectedSubtotalOrder1 + $expectedSubtotalOrder2 + $expectedSubtotalOrder3  
								+ $totalShipping;
		$this->assertEquals($expectedGrandTotal, $parentOrder->getGrandTotal(),
				"Grand total should be sum of child products and shipping.");

		# now try canceling one of the sub orders (#2)
		# to make sure everything is recalculated on the parent correctly
		$childOrder2->cancel()->save();
		
		# reload both orders to make sure they saved correctly
		$canceledOrder = Mage::getModel('sales/order')->load($childOrder2->getId());
		$parentOrder = Mage::getModel('sales/order')->load($parentOrder->getId());

		$this->assertTrue($canceledOrder->isCanceled(), "Should be marked as canceled.");
		
		$this->assertEquals($canceledShipping, $canceledOrder->getShippingCanceled(), 'Should have the correct amount of shipping canceled.');
		
		$expectedNewGrandTotal = $expectedGrandTotal - $expectedSubtotalOrder2 - $canceledShipping;
		$this->assertEquals($canceledShipping, $parentOrder->getShippingCanceled(), 'Parent order should have correct amount of canceled shipping.');
		$this->assertEquals($expectedNewGrandTotal, $parentOrder->getGrandTotal(), "Should have the correct grand total.");
		
		# now send shipments 1 and 3
		$shipment1 = $this->_createShipmentForChildOrder($childOrder1);
		$shipment3 = $this->_createShipmentForChildOrder($childOrder3);
		
		# send the email
		$parentOrder->sendShipmentEmail(true);
		
		echo PHP_EOL . "test_multiship_3_child_orders_cancel_one_send_two_stripe PASSED " . PHP_EOL;
	}

	/**
     * @depends test_multiship_checkout_type_creates_multiship_parent_and_kids
     */
	public function test_guest_multiship_creates_multiship_parent_kids_with_stripe()
	{
		# product IDs (as of this test) - 
		# 	188 - 7 pc - 233000-7 - 12.75
		# 	19 - 20 pc - 220240 - 28.50
		$product1 = $this->_getProduct(188);
		$product2 = $this->_getProduct(19);
		
		$subtotal = $product1->getPrice() * 2 + $product2->getPrice() * 5;
		
		$helper = Mage::helper('tests');
		
		$billingAddressData = $helper->buildAddressArray('Multiship', 'Guest', '123 Pine ST Suite ' . time());
		
		$quote = $helper->createGuestQuote($billingAddressData
					, array( array ( $product1->getId() => 2 ), array( $product2->getId() => 3) ), self::SHIP_METHOD_2DAY);
		$this->_setSessionQuote($quote);
		$multishipCheckout = Mage::getModel('checkout/type_multishipping');
		
		# now add another address, where we will add two more of the second item
		$otherShippingAddress = $helper->buildAddressArray(
			'Multiship Guest'
			, 'Other Address'
			, '5628 Airport Way S #' . time());
		$helper->addAddressAndItemsToQuote($quote, $otherShippingAddress
					, array ( array ($product2->getId() => 2 )), self::SHIP_METHOD_2DAY);
		#Mage::getSingleton('checkout/session')->setQuote($quote);
		
		$payment = Mage::getModel('sales/quote_payment');

		// have to reload the quote for the payment to work properly
		$newQuote = Mage::getModel('sales/quote')->setStoreId(1);
		$newQuote = $newQuote->load($quote->getId());
		
		# create a stripe token for the payment
		$this->_setStripeConfig();
		
		$token = Stripe_Token::create(array(
  			"card" => array(
    		"number" => "4242424242424242",
    		"exp_month" => 11,
    		"exp_year" => 2014,
    		"cvc" => "314"
  			)
		));
		
		$payment->setStoreId(1);
		$payment->setQuote($newQuote);
		$payment->importData(array ('method' => 'stripe', 'stripe_token' => $token->id, 'exp_month' => 11, 'exp_year' => 2019));
		$payment->save();
		$newQuote->setPayment($payment);
		
		$multishipCheckout = Mage::getModel('checkout/type_multishipping');
		$multishipCheckout->init($newQuote);
		
		$orders = $multishipCheckout->createOrders($this->coreSession, $this->checkoutSession);
		
		# quote should no longer be active
		$currentQuote = Mage::getModel('sales/quote')->setStoreId(1)->load($quote->getId());
		$this->assertEquals(0, $currentQuote->getIsActive(), 'Quote should no longer be active.'); 
		
		# there should be 3 orders (parent and two children) for the quote
		$orders = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('quote_id', $currentQuote->getId())->load();
		$this->assertEquals(3, $orders->count(), "Should be 3 orders for this quote.");
		
		# parent should be the sum of the children
		# let's start out with subtotals
		$ordersArray = $orders->getItems();
		$orderIds = array_keys($ordersArray);
		$parentOrder = $ordersArray[$orderIds[0]];
		$childOrder1 = $ordersArray[$orderIds[1]];
		$childOrder2 = $ordersArray[$orderIds[2]];
				
		# make sure the child order costs the same as the product
		$this->assertEquals($product1->getPrice() * 2 + $product2->getPrice() * 3, $childOrder1->getBaseSubtotal()
								, 'Subtotal of child order 1 should match.');
		$this->assertEquals($product2->getPrice() * 2, $childOrder2->getBaseSubtotal()
								, 'Subtotal of child order 2 should match.');
								
		# make sure child orders are set to processing (not new)
		$this->assertEquals(Mage_Sales_Model_Order::STATE_PROCESSING, $childOrder1->getState(), 
					'Child order 1 should be set to processing by default.');
		$this->assertEquals(Mage_Sales_Model_Order::STATE_PROCESSING, $childOrder2->getState(), 
					'Child order 2 should be set to processing by default.');
		
		# make sure that the shipping total on the order matches the child quote address shipping costs
		# unfortunately we don't have a way to force a specific shipping cost, so we just have to add
		# what was in the quotes, which should have called to the shipping method to get calculated
		$totalShipping = 0;
		foreach ($currentQuote->getAllAddresses() as $address)
		{
			if ($address->getAddressType() == 'shipping')
			{
				$totalShipping += $address->getShippingAmount();
			}
		}
		
		$this->assertEquals($totalShipping, $parentOrder->getShippingAmount(), 
				"Shipping amount on parent should match sum of child orders.");
		
		# grand total should be the product costs + shipping (no tax)
		$expectedGrandTotal = $product1->getPrice() * 2 + $product2->getPrice() * 5 + $totalShipping;
		$this->assertEquals($expectedGrandTotal, $parentOrder->getGrandTotal(),
				"Grand total should be sum of child products and shipping.");
		
		echo PHP_EOL . "test_guest_multiship_creates_multiship_parent_kids PASSED with quote ID of {$currentQuote->getId()}" . PHP_EOL;
	}
	
	private function _setStripeConfig()
	{
		$key = Mage::getStoreConfig('payment/stripe/secret_key_test', 1);
		Stripe::setApiKey($key);
	}
			
	private function _getProduct($id)
	{
		return Mage::getModel('catalog/product')->load($id);
	}
	
	private function _setSessionQuote($quote)
	{
		# so it will cache the quote, so we need to clear it
		# and then set the ID.
		# now that we can't just set the quote, because then it will cache its state from
		# when we set it.
		Mage::getSingleton('checkout/session')->clear();
		Mage::getSingleton('checkout/session')->setQuoteId($quote->getId());
	}
	
	private function _createShipmentForChildOrder($childOrder)
	{
		$orderItemQuantities = array();
		foreach ($childOrder->getAllItems() as $item)
		{
			$orderItemQuantities[$item->getId()] = $item->getQtyOrdered();
		}
		$shipment = Mage::getModel('sales/service_order', $childOrder)
						->prepareShipment($orderItemQuantities);	

		// create a fake tracking number
		$data['carrier_code'] = 'fedex';
		$data['title'] = 'Fedex';		
		$data['number'] = $this->_createFakeTrackingNumber();

		$track = Mage::getModel('sales/order_shipment_track')->addData($data);
		$shipment->addTrack($track);

		$shipment->save();
		
		return $shipment;
	}
	
	private function _createFakeTrackingNumber()
	{
		$trackingNumber = '01';
		$valid_chars = range(0, 9);
		$valid_count = count($valid_chars);
		
		$length = 13;
		
		for ($i = 0; $i < $length; $i++)
		{
			$randomNumber = mt_rand(1, $valid_count);
			$trackingNumber .= $randomNumber; 		
		}
		
		return $trackingNumber;
	}
}
