<?php
class GoSolid_Tests_Helper_Data extends Mage_Core_Helper_Abstract
{
	// shippingAddress can be a number (which indicates how many to create)
	// or an array with data in it
	public function createCustomer($shippingAddress = 0)
	{
		$customer = Mage::getModel('customer/customer');
		
		# according to php docs, DateTime supports microseconds
		# but i was unable to get that to work, so using a hack
		$now = new DateTime();
		$testDate = $now->format('M-d-Y');
		$testTime = $now->format('H-i-s-') . $this->_getMillis();

		$customer->setFirstname("Test $testDate");
		$customer->setLastname("customer $testTime");
		$email = "testcustomer-$testDate-$testTime@gosolid.net";
		$customer->setEmail($email);
		
		# assume default as store for now
		$customer->setStoreId(1);
		$customer->setWebsiteId(1);
		
		$customer->save();
		
		if (is_array($shippingAddress))
		{
			$address = $this->_addAddressForCustomer($customer, true, 0, $shippingAddress);			
		}
		else 
		{
			$address = $this->_addAddressForCustomer($customer);
		}
		
		$customer->setDefaultBilling($address->getId());
		$customer->setDefaultShipping($address->getId());
		$customer->save();
		
		// we can pass an array that contains the address to use
		// or have a number that indicates how many default addresses to use.
		if (!is_array($shippingAddress) && $shippingAddress > 0)
		{
			$shippingAddressCount = $shippingAddress;
			for ($i = 1; $i <= $shippingAddressCount; $i++)
			{
				$this->_addAddressForCustomer($customer, false, $i + 1);
			}
			
		}
		
		$newCustomer = Mage::getModel('customer/customer')->load($customer->getId());
		$session = Mage::getSingleton('customer/session');
		$session->setCustomer($newCustomer);
		
		return $newCustomer;
	}
	
	public function createQuote($customer, $productData, $shippingMethod = '')
	{
		if (!isset($productData) || !is_array($productData))
		{
			throw new Exception('You must specify product data in an array to create a quote!');
		}
		
		$quote = Mage::getModel('sales/quote');
		$quote->setStoreId(1);
		$quote->setWebsiteId(1);
		$quote->setIsMultiShipping(true);

		$quote->assignCustomerWithAddressChange($customer);
		
		$quote->save();
		
		/*
		$checkout = Mage::getSingleton('checkout/session');
		$checkout->setCheckoutState(Mage_Checkout_Model_Session::CHECKOUT_STATE_BEGIN);
		$checkout->setQuoteId($quote->getId());
		*/
		
		# add the shipping price, if present
			$shippingAddress = $quote->getShippingAddress();
		$this->_productsToQuoteItems($shippingAddress, $productData, $quote);
			if (strlen($shippingMethod) > 0)
		{
			$this->_addShippingMethodToAddress($shippingAddress, $shippingMethod);
		}
		
		
		$quote->save();
		
		#$quote->collectTotals();
		
		// need to attach to the session
		#$checkoutSession = new Mage_Checkout_Model_Session();
		#$checkoutSession->setCustomer($customer);
		#$checkoutSession->setQuoteId($quote->getId());
		#Mage::unregister('_singleton/' . 'checkout/session');
		#Mage::register('_singleton/' . 'checkout/session', $checkoutSession, false);
	
		return $quote;
	}
	
	public function createGuestQuote($billingAddressData, $productData, $shippingMethod)
	{
		$quote = Mage::getModel('sales/quote');
		$quote->setStoreId(1);
		$quote->setWebsiteId(1);
		$quote->setIsMultiShipping(true);
		$quote->setCustomerIsGuest(true);

		$emailAddress = 'multishipguest-' . time() . "@gosolid.net";
		
        $billingAddress = $quote->getBillingAddress();
        
        $billingAddress = $this->_convertToQuoteAddress($billingAddressData, $billingAddress);
        
        $billingAddress->setCustomerAddressId(null);

        # at this point, our billing is configured correctly
        # but we are going to use it as a shipping address, too
        # which requires cloning and replacing certain properties
		$billing = clone $billingAddress;
		$billing->unsAddressId()->unsAddressType();
        $shippingAddress = $quote->getShippingAddress();

		$shippingAddress->addData($billing->getData())
        	->setSameAsBilling(1)
            ->setSaveInAddressBook(0)
            ->setShippingMethod($shippingMethod)
            ->setCollectShippingRates(true);
        
        # set some display fields on the quote from the addresses
        $quote->setCustomerFirstname($billingAddress->getFirstname());
        $quote->setCustomerLastname($billingAddress->getLastname());
        $quote->setCustomerEmail($emailAddress);
            
		$quote->save();
		
		# add the shipping price, if present
		$this->_productsToQuoteItems($shippingAddress, $productData, $quote);
		if (strlen($shippingMethod) > 0)
		{
			$this->_addShippingMethodToAddress($shippingAddress, $shippingMethod);
		}
		
		$quote->save();
		
		return $quote;
	}
	
	/***
	 * 
	 * $address 	Mage_Customer_Model_Address | array	Address object or array to import
	 */
	public function addAddressAndItemsToQuote($quote, $address, $productData, $shippingMethod, $preferredArrivalDate = '')
	{
		$quoteAddress = $this->_convertToQuoteAddress($address);
		$quoteAddress->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_SHIPPING);

        if ($preferredArrivalDate)
        {
            $quoteAddress->setPreferredArrivalDate($preferredArrivalDate);
        }

		$quote->addAddress($quoteAddress);
		$quote->setIsMultiShipping(true);
		
		$quoteAddress->save();
		$quote->save();
		
		$this->_productsToQuoteItems($quoteAddress, $productData, $quote);
		$quoteAddress->save();
		$this->_addShippingMethodToAddress($quoteAddress, $shippingMethod);
		
	}
	
	public function buildAddressArray(
		$firstname, $lastname
		, $streetAddress
		, $telephone = '206.466.6194'
		, $postcode = '98108'
		, $city = 'Seattle'
		, $region = 'Washington'
		, $regionId = '62'
		, $countryId = 'US'
	)
	{
		return array(
				"firstname" => $firstname,
				"lastname" => $lastname,
				"street" => $streetAddress,
				"telephone" => $telephone,
				"postcode" => $postcode,
				"city" => $city,
				"region" => $region,
				"region_id" => $regionId,
				"country_id" => $countryId,
		);	
	}
	
	private function _convertToQuoteAddress($quoteAddressData, $quoteAddress = null)
	{
		
		if ($quoteAddress == null)
		{
			$quoteAddress = Mage::getModel('sales/quote_address');
		}
		
		if ($quoteAddressData instanceof Mage_Customer_Model_Address)
		{
			$quoteAddress->importCustomerAddress($quoteAddressData);
		}
		else
		{
			# should be an array, more steps
        	/* @var $addressForm Mage_Customer_Model_Form */
        	$addressForm = Mage::getModel('customer/form');
        	$addressForm->setFormCode('customer_address_edit')
            	->setEntityType('customer_address')
            	->setIsAjaxRequest(Mage::app()->getRequest()->isAjax());
		
        	# the following code adopted from Core Onepage checkout for guest
			$addressForm->setEntity($quoteAddress);
        
			// 	emulate request object
    	    $addressData    = $addressForm->extractData($addressForm->prepareRequest($quoteAddressData));
        	$addressErrors  = $addressForm->validateData($addressData);
			$addressForm->compactData($addressData);
	        //	unset billing address attributes which were not shown in form
        	foreach ($addressForm->getAttributes() as $attribute) {
				if (!isset($quoteAddressData[$attribute->getAttributeCode()])) {
            		$quoteAddress->setData($attribute->getAttributeCode(), NULL);
				}
			}

			# for some reason there is an issue with setting the street
			# this is a hack shortcut to force it
			$quoteAddress->setStreet($quoteAddressData['street']);
			#$quoteAddress->implodeStreetAddress();
		}
		

		return $quoteAddress;
	}
	
	private function _addShippingMethodToAddress($address, $shippingMethod)
	{
			#echo "Adding shipping method of $shippingMethod";
			$address->setCollectShippingRates(true);
			$addressRate = Mage::getModel('sales/quote_address_rate');
			$flatrateShipping = $this->_getFlatRateShippingMethod($shippingPrice);
			$addressRate->importShippingRate($addressRate);
			$addressRate->save();
			
			# we need to apply the method or else it will think it doesn't have one
			$address->addShippingRate($addressRate);
			$address->save();
			$address->setShippingMethod($shippingMethod);
			# this is what really sets the shipping
			$address->setShippingAmount($shippingPrice);
			$address->save();			
		
	}
	
	/*
	 * $quoteContainer can be a quote or a quoteAddress - both offer the addItem method
	 */
	private function _productsToQuoteItems($quoteAddress, $productData, $quote)
	{
		foreach ($productData as $productEntry)
		{
			foreach ($productEntry as $productId => $qty)
			{
				# TODO: allow for the key to be a product, rather than an ID
				$product = Mage::getModel('catalog/product')->load($productId);
				
				if (!$product->getId())
				{
					throw new Exception("Product $productId could not be found.");
				}
				
				$quoteItem = Mage::getModel("sales/quote_item");
				$quoteItem->setProduct($product);
				$quoteItem->setQty($qty);
				$quoteItem->setPrice($product->getPrice());
				$quoteItem->setRowTotal($qty * $quoteItem->getPrice());
				
				$quote->addItem($quoteItem);
				$quoteItem->save();

				$quoteAddress->addItem($quoteItem);
				$quoteAddress->save();
				$quoteItem->save();
			}
		}
	}
	
	private function _addAddressForCustomer($customer, $isDefault = true, $addressNumber = 0, $addressData = null)
	{
		$address = Mage::getModel('customer/address');
		
		if ($addressData == null)
		{
			$addressData =  $this->buildAddressArray(
									$customer->getFirstname(), $customer->getLastname(),
									'Test Address' . (($addressNumber > 0) ? (PHP_EOL . "Address #$addressNumber") : '')
									);	
		}
		if ($isDefault)
		{
			$addressData["is_default_billing"] = 1;
			$addressData["is_default_shipping"] = 1; 
		}
		
		$address->setData($addressData);
		
		$customer->addAddress($address);
		
		if (!$isDefault)
		{
			$address->setCustomer($customer);
			$address->save();
		}
		
		return $address;
	}
	
	private function _getFlatRateShippingMethod($shippingPrice)
	{
		// this code ripped off from the Mage_Shipping_Model_Carrier_Flatrate
		// class, since this is what it does to construct a response
        $method = Mage::getModel('shipping/rate_result_method');

        $method->setCarrier('flatrate');
        $method->setCarrierTitle('Flatrate test');

        $method->setMethod('flatrate');
        $method->setMethodTitle('flatrate method test');

        $method->setPrice($shippingPrice);
        $method->setCost($shippingPrice);
        
        return $method;
	}
	
	private function _getMillis()
	{
        return substr((string)microtime(), 2, 8);
	}
}