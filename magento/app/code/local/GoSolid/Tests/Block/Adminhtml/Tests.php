<?php
class GoSolid_Tests_Block_Adminhtml_Tests extends Mage_Adminhtml_Block_Abstract
{
	public function __construct()
  	{
  		$this->setTemplate("gosolid_tests/tests.phtml");
		parent::_construct();
  	}
  	
  	public function getTestNames()
  	{
  		# look in the tests folder for any test files
  		$path = implode(DS, array( __DIR__ , '..', '..', 'tests'));

  		$files = scandir($path);
  		
  		$testFiles = array();
  		
  		foreach ($files as $file)
  		{
  			if (preg_match("/.*\.php$/i", $file))
  			{
  				$testFiles []= substr($file, 0, strlen($file) - 4);
  			}
  		}
  		
  		
  		return $testFiles;
  	}
	
}