<?php
class GoSolid_Tests_Adminhtml_Gstests_IndexController extends Mage_Adminhtml_Controller_Action
{
	protected function _initAction() {
		$this->loadLayout();
		return $this;
	}   
 
	public function indexAction() {
		
		$this->_initAction();		
		$this->renderLayout();
	}
}