<?php

class GoSolid_Tests_Adminhtml_Gstests_TestController extends Mage_Adminhtml_Controller_Action
{
	protected function _initAction() {
		$this->_title($this->__('darwin test runner'));
		
		return $this;
	}   
 
	public function runAction() {

		$this->_initAction();

		$testNames = $this->getRequest()->getParam('testNames');
		
		if (!is_array($testNames) || count($testNames) == 0)
		{
			echo "No test names specified!";
			die;
		}

		$localGoSolid = realpath(__DIR__ . '/../../..');
		$testsLibPath = $localGoSolid . '/Tests/Lib'; 
		if (!class_exists('PHPUnit_TextUI_Command'))
		{
			# PHPUnit is not defined, include it from the Lib
			# $testLibPath = '/../../../Tests/Lib/PHPUnit/Autoload.php';
			set_include_path(get_include_path() . PATH_SEPARATOR . $testsLibPath);
			include_once $testLibPath . '/PHPUnit/Autoload.php';
		}
		
		# hacky way to walk up to the GoSolid directory
		$testDir ='/../../tests/';
		
		foreach ($testNames as $name)
		{
			$testPath = $testDir . $name . ".php";
			$testPath = __DIR__ . $testPath;
			echo "Running $testPath" . PHP_EOL;
		
			error_reporting(E_ALL ^ E_NOTICE);
			$argv = array(
    			# we need at least one log file or else the runner fails
				'--log-junit', 'test-ouptut.xml',
    			$testPath
			);
			$_SERVER['argv'] = $argv;
		
			PHPUnit_TextUI_Command::main(false);
		}
		 
	}
	
}