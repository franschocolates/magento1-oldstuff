<?php
/**
 * Created by PhpStorm.
 * User: Steve
 * Date: 3/10/2015
 * Time: 2:48 PM
 */

class GoSolid_AccPac_Model_Observer
{

    public function salesQuoteItemSetAccPac($observer)
    {
        $quoteItem = $observer->getQuoteItem();
        $product = $observer->getProduct();
        $quoteItem->setAccpacSku($product->getAccpacSku());
    }
}