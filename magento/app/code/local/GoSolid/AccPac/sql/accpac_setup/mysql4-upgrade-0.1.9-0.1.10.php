<?php
/**
 * Created by PhpStorm.
 * User: Steve
 * Date: 3/9/2015
 * Time: 12:17 PM
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

//add the accpac sku column to the quote address item table so it's availble for the order item.
$installer->getConnection()->addColumn($installer->getTable('sales/quote_address_item'), 'accpac_sku', array(
    'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'LENGTH'    => 64,
    'NULLABLE'  => true,
    'COMMENT'   => 'ACCPAC SKU'
));

$installer->getConnection()->addColumn($installer->getTable('sales/quote_address_item'), 'configurable_product_id', array(
    'TYPE'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
    'LENGTH'    => 11,
    'NULLABLE'  => true,
    'COMMENT'   => 'ID of the configurable product.'
));


$installer->endSetup();