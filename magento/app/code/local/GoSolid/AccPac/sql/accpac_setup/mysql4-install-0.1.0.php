<?php
/**
 * Created by PhpStorm.
 * User: Steve
 * Date: 3/9/2015
 * Time: 12:17 PM
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();


$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'accpac_sku', array(
    'group'         => 'General',
    'backend'       => '',
    'frontend'      => '',
    'label'         => 'ACCPAC SKU',
    'type'          => 'static',
    'input'         => 'text',
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
    'visible'       => false,
    'required'      => true,
    'user_defined'  => true,
    'visible_on_front' => false,
    'used_in_product_listing' => false,
    'unique'                     => false,
    'sort_order'                 => 6,
    'searchable'                 => false,
    'comparable'                 => false,
    'visible_in_advanced_search' => false,
    'note'                      => 'This is the shared SKU for ACCPAC as the Magento SKUs have to be unique, this field is required.',
    'apply_to'          => 'simple'
));


    $installer->getConnection()->addColumn($installer->getTable('catalog/product'), 'accpac_sku', array(
            'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
            'LENGTH'    => 64,
            'NULLABLE'  => true,
            'COMMENT'   => 'ACCPAC SKU'
        ));//addColumn($tableName, $columnName, $definition, $schemaName = null);

$installer->getConnection()->addColumn($installer->getTable('sales/quote_item'), 'accpac_sku', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'LENGTH'    => 64,
        'NULLABLE'  => true,
        'COMMENT'   => 'ACCPAC SKU'
    ));

$installer->getConnection()->addColumn($installer->getTable('sales/order_item'), 'accpac_sku', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'LENGTH'    => 64,
        'NULLABLE'  => true,
        'COMMENT'   => 'ACCPAC SKU'
    ));

$installer->getConnection()->addColumn($installer->getTable('sales/invoice_item'), 'accpac_sku', array(
    'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'LENGTH'    => 64,
    'NULLABLE'  => true,
    'COMMENT'   => 'ACCPAC SKU'
));

$installer->endSetup();