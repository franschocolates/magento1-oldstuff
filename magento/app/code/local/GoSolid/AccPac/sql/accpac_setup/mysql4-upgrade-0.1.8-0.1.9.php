<?php
/**
 * Created by PhpStorm.
 * User: Steve
 * Date: 3/9/2015
 * Time: 12:17 PM
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

//Set the product types the ACCPAC SKU if for
$installer->run("UPDATE catalog_eav_attribute SET apply_to = 'simple,virtualgiftcard,giftcard' WHERE attribute_id = (SELECT attribute_id FROM eav_attribute WHERE attribute_code = 'accpac_sku');");

//Now update the gift cards product types
$installer->run(<<<MySQL
UPDATE catalog_product_entity
SET accpac_sku = sku
WHERE type_id IN ('giftcard','virtualgiftcard');
MySQL
);

///Let update the orders and invoices again because some test orders are missing the data
//Add SKUs to orders in the system
$installer->run("UPDATE sales_flat_order_item SET accpac_sku = sku WHERE accpac_sku IS NULL;");

//Add SKUs to invoices in the system
$installer->run("UPDATE sales_flat_invoice_item SET accpac_sku = sku WHERE accpac_sku IS NULL;");

$installer->endSetup();