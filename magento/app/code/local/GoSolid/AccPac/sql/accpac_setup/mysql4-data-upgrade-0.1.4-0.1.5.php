<?php
/**
 * Created by PhpStorm.
 * User: Steve
 * Date: 3/9/2015
 * Time: 12:59 PM
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$reportTitle = 'Items on Order - By Ship Date';

$report = Mage::getModel('sqlReports/sqlReport')->load($reportTitle,'title');


$report->setPreSql(
    <<<MySQL
MySQL

);
$report->setSql(
    <<<MySQL
SELECT
	`sku`
	, `name` AS 'Product Name'
	, SUM(sfoi.qty_ordered - (sfoi.qty_shipped + sfoi.qty_canceled)) AS 'Quantity'
	, DATE_FORMAT(sfo.planned_ship_date, '%m/%d/%Y') AS 'ship date'
	, IF (COUNT(DISTINCT sfo.increment_id) > 1, CONCAT(COUNT(DISTINCT sfo.increment_id), ' shipments'), GROUP_CONCAT(DISTINCT sfo.increment_id SEPARATOR '')) AS 'Order #'
	, sfoi.notes AS 'Product Notes'
	, `accpac_sku`
FROM
	sales_flat_order_item AS sfoi
INNER JOIN
	sales_flat_order AS sfo
ON
	sfo.entity_id = sfoi.order_id
WHERE
	(sfoi.qty_ordered - (sfoi.qty_shipped + sfoi.qty_canceled)) > 0
AND
	sfo.`status` NOT IN ('ready_for_pickup', 'picked_up')
AND
	DATE(sfo.planned_ship_date) BETWEEN '{Start Date:date}' AND '{End Date:date}'
GROUP BY
	sfoi.`sku`, sfoi.`name`, sfo.planned_ship_date, sfoi.notes, sfoi.`accpac_sku`
ORDER BY
	planned_ship_date ASC, sku ASC;
MySQL

);

$report->save();


$installer->endSetup();