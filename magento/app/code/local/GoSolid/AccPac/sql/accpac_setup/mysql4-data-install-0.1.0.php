<?php
/**
 * Created by PhpStorm.
 * User: Steve
 * Date: 3/9/2015
 * Time: 12:59 PM
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

//Add SKUs to orders in the system
$installer->run("UPDATE sales_flat_order_item SET accpac_sku = sku WHERE accpac_sku IS NULL;");

//Add SKUs to invoices in the system
$installer->run("UPDATE sales_flat_invoice_item SET accpac_sku = sku WHERE accpac_sku IS NULL;");

//Set the product types the ACCPAC SKU if for
$installer->run("UPDATE catalog_eav_attribute SET apply_to = 'simple' WHERE attribute_id = (SELECT attribute_id FROM eav_attribute WHERE attribute_code = 'accpac_sku');");


$installer->endSetup();