<?php
/**
 * Created by goSolid.
 * Date: 11/14/14
 * Time: 9:56 AM
 */ 
class GoSolid_Frans_Helper_Checkout_Data extends Mage_Checkout_Helper_Data {

	public function formatPrice($price)
	{
		return str_replace('.00', '', parent::formatPrice($price)); // remove ".00"
	}

}