<?php
/**
 * Rewritten to point to our custom logic that redirects to a page if they aren't logged in
 */ 
class GoSolid_Frans_Helper_Checkout_Url extends Mage_Checkout_Helper_Url
{
    /**
     * Retrieve checkout url
     *
     * @return string
     */
    public function getCheckoutUrl()
    {
            return $this->_getUrl('frans/checkout');
    }

    // onepage should use the same logic as normal checkout URL
    public function getOPCheckoutUrl()
    {
        return $this->getCheckoutUrl();
    }

    /**
     * Retrieve multishipping checkout url
     *
     * @return string
     */
    public function getMSCheckoutUrl()
    {
        return $this->_getUrl('frans/checkout_multiship');
    }

}