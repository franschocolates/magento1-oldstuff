<?php

require_once 'lib/Givex/GivexGiftCardConnect.php';

class GoSolid_Frans_Helper_GiftCard extends Mage_Core_Helper_Abstract
{
	const GC_NOT_FOUND = -1;
	const GC_CONNECTION_ERROR = -2;
	
	public function getBalance($giftCardNumber)
	{
        return Mage::getModel('givex/giftcard')->getBalance($giftCardNumber);
	}

    /**
     * Sends the email for a virtual gift card
     *
     * @param GoSolid_Givex_Model_Giftcard $giftcard
     */
    public function sendVirtualGiftCardEmail(GoSolid_Givex_Model_Giftcard $giftcard, $emails = false)
    {
        $orderItem = $giftcard->getOrderItem();

        if ($orderItem)
        {
            // get the options
            /** @var GoSolid_Givex_Model_Catalog_Product_Type_Giftcard_VirtualGiftcard $virtualGiftcardInstance */
            $virtualGiftcardInstance = $orderItem->getProduct()->getTypeInstance(false);

            $recipientName = $virtualGiftcardInstance->getRecipientName($orderItem);
            $recipientEmail = $virtualGiftcardInstance->getRecipientEmail($orderItem);
            $message = $virtualGiftcardInstance->getMessage($orderItem);
            $senderName = $virtualGiftcardInstance->getSenderName($orderItem);
            $senderEmail = $virtualGiftcardInstance->getSenderEmail($orderItem);
            $barcodeUrl = $giftcard->getBarCodeUrl();

            // now populate in our template and send....
            $mailTemplate = Mage::getModel("core/email_template")->loadDefault('virtual_gift_card',  Mage::getStoreConfig('general/locale/code'));
            $vars = array(
                GoSolid_Givex_Model_Catalog_Product_Type_Giftcard_VirtualGiftcard::OPTION_RECIPIENT_NAME => $recipientName
            , GoSolid_Givex_Model_Catalog_Product_Type_Giftcard_VirtualGiftcard::OPTION_RECIPIENT_EMAIL  => $recipientEmail
            , GoSolid_Givex_Model_Catalog_Product_Type_Giftcard_VirtualGiftcard::OPTION_MESSAGE          => $message
            , GoSolid_Givex_Model_Catalog_Product_Type_Giftcard_VirtualGiftcard::OPTION_SENDER_NAME      => $senderName
            , GoSolid_Givex_Model_Catalog_Product_Type_Giftcard_VirtualGiftcard::OPTION_SENDER_EMAIL     => $senderEmail
            , 'card_number' => $giftcard->getCardNumber()
            , GoSolid_Givex_Model_Catalog_Product_Type_Giftcard_VirtualGiftcard::OPTION_AMOUNT => Mage::helper('core')->currency($orderItem->getOriginalPrice(),true,false)
            , 'url' => $barcodeUrl
            );

            $mailTemplate->setSenderEmail(Mage::getStoreConfig('trans_email/ident_sales/email'));
            $mailTemplate->setSenderName(Mage::getStoreConfig('trans_email/ident_sales/name'));

            // we may have specific emails, in which case we don't read from the original
            if ($emails)
            {
                // use the recipient name for everyone
                $names = array_fill(0, count($emails), $recipientName);
            }
            else
            {
                // use what was provided
                $emails = array($recipientEmail);
                $names = array($recipientName);
            }


            // handle the case that it's not doing
            $order = $orderItem->getOrder();
            $storeId = $order->getStore()->getId();
            $appEmulation = Mage::getSingleton('core/app_emulation');
            $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);

            try
            {
                $mailTemplate->addBcc(Mage::getStoreConfig('frans/notifications/email_address'));
                $mailTemplate->send($emails, $names, $vars);

                $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
            }
            catch (Exception $exception)
            {
                Mage::logException($exception);
                $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
            }
        }
        else
        {
            Mage::log("Unable to send email; no order item specified for giftcard!", Zend_Log::ERR, 'frans-giftcard.log', true);
        }
    }

	private function _getGiveXConnect($giftCardNumber)
	{
		$giveX = new GivexGiftCardConnect();
		$giveX->setCardNum($giftCardNumber);
		
		$giveX->init(
			$this->_getConfig('token'),
			$this->_getConfig('userid'),
			$this->_getConfig('userpass'),
			$this->_getConfig('url')
		);
		
		if ($giveX->connectionError)
		{
			return false;
		}
		
		return $giveX;
	}
	
	private function _getConfig($name)
	{
		if ($name == 'token' || $name == 'userpass')
		{
			return Mage::helper('core')->decrypt(Mage::getStoreConfig('givex/credentials/' . $name));
		}
		
		return Mage::getStoreConfig('givex/credentials/' . $name);
	}
}