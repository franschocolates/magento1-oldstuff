<?php
class GoSolid_Frans_Helper_Data extends Mage_Core_Helper_Abstract
{
	const FORCED_EXCEPTION_LOG = "frans-exceptions.log";

    const PRETTY_DATE_FORMAT = 'l M j, Y';
    const CALENDAR_DISPLAY_DATE_FORMAT = 'm/d/Y';
    // zend dates have different formatting; the below matches our calendar display date format
    // in the zend specification (m = M, Y = YYYY)
    const CALENDAR_ZEND_DATE_FORMAT = 'M/d/YYYY';

    public static $ADDRESS_VAILDATION_ATTRIBUTES = array(
            'Resolved' => array(true, -80),
            'DPV' => array(true, -80),
            'CountrySupported' => array(true, -80),
            'SuiteRequiredButMissing' => array(false, -30),
            'InvalidSuiteNumber' => array(false, -10),
            'MultipleMatches' => array(false, -10),
            'POBox' => array(false, -80),
            'StreetAddress' => array(true, -20)
        );

    public static $ADDRESS_VALIDATION_MESSAGES = array(
            'DPV' => 'This place doesn’t exist, or if it does, no one can deliver anything to it.',
            'CountrySupported' => 'We do not ship to this country.',
            'SuiteRequiredButMissing' => 'Suite number is required, but missing.',
            'InvalidSuiteNumber' => 'Suite number is invalid.',
            'MultipleMatches' => 'Multiple matches for this address.',
            'POBox' => 'We do not ship to PO Boxes.',
            'StreetAddress' => 'Could not find a match of this address against reference data.'
        );

    public function todaysDate(){
        return Mage::app()->getLocale()->date(null, Zend_Date::DATE_SHORT, null, false);
    }

    public function prettyDate($dateValue)
    {
        return $this->_formatDate($dateValue, self::PRETTY_DATE_FORMAT);
    }

    public function calendarDisplayDate($dateValue)
    {
        return $this->_formatDate($dateValue, self::CALENDAR_DISPLAY_DATE_FORMAT);
    }

	// format any string for use as a friendly URL
	public function friendlyUrl($string)
	{
		// remove apostrophes from string so they don't get converted to dashes (especially important for "Fran's")
		$string = str_replace("'", '', $string);
		// use Magento helper to replace invalid characters with ones that work in URLs
		$string = Mage::helper('catalog/product_url')->format($string);
		// replace remaining non-alphanumeric characters with dashes and make it lowercase
		$string = preg_replace('#[^0-9a-z]+#i', '-', $string);
		// remove trailing or preceding dashes
		$string = trim($string, '-');
		// make it lowercase
		$string = strtolower($string);
		return $string;
	}

    /**
     * Sends a notification email to the configured admin email address
     *
     * @param string $subject
     * @param string $message
     */
    public function sendAdminNotificationEmail($subject, $message)
    {
        // figure out who to send notifications to...
        $toAddress = Mage::getStoreConfig('frans/notifications/email_address');

        $mail = Mage::getModel('core/email')
                ->setBody($message)
                ->setType('text')
                ->setFromEmail($toAddress)
                ->setToEmail($toAddress)
                ->setSubject($subject);

        $mail->send();
    }

    /**
     *
     * Gets string to display for payment terms
     *
     * @param GoSolid_Frans_Model_Sales_Order_Payment $payment
     */
    public function getTermsDisplay($payment)
    {
        $helper = Mage::helper('frans');
        $baseDisplay = array(
            'free' => $this->__('NO CHARGE')
            , 'stripe' => $payment->getCcType()
            , 'paybyinvoice' => $this->__('INV')
            , 'paid_at_register' => $this->__('Paid at Register')
            , 'pay_at_pickup' => $this->__('Pay at Pickup')
            , 'checkmo' => $this->__('Cash / Check')
            );

        $termsDisplay = $baseDisplay[$payment->getMethod()];

        if ($payment->getOrder()->getBaseGiftcardInvoicedAmount() > 0)
        {
            // if it was free, it was paid entirely by giftcard
            // otherwise, append
            if ($payment->getMethod() != 'free')
            {
                $termsDisplay .= $this->__(' / Gift Card');
            }
            else
            {
                $termsDisplay = $this->__('Gift Card');
            }
        }

        return $termsDisplay;
    }

    protected function _formatDate($dateValue, $format)
    {
        if (!$dateValue)
        {
            return '';
        }

        if (!($dateValue instanceof DateTime))
        {
            $dateValue = new DateTime($dateValue);
        }

        return $dateValue->format($format);
    }

    public function maskedShippingMethod($shippingMethod, $default)
    {
        $fransShippingMethod = Mage::getModel('frans/shippingMethod')->loadByMethod($shippingMethod);

        if ($fransShippingMethod->getId())
        {
            return $fransShippingMethod->getLabel();
        }

        return $default;
    }

	/**
	 * Builds a hash of the location elements of an address
	 * (street, city, state, zip, etc)
	 * 
	 * @param Mage_Customer_Model_Address $address
	 */
	public function calculateLocationHash($address)
	{
		// easy implementation for now - just append all of them together and then md5 it.
		$combined = implode(' ', $address->getStreet()) . $address->getCity() . 
						$address->getRegionId() . $address->getPostcode() . $address->getCountryId();
		return md5($combined);
	}
	
	/**
	 * 
	 * Forces a log entry for an exception we deem critical
	 * @param Exception $exception
	 * @param string $source
	 */
	public function logCriticalException($exception, $source)
	{
		Mage::log("Critical exception in $source: " . $exception, Zend_Log::CRIT, self::FORCED_EXCEPTION_LOG, true);
	}

    public function getShipDateForMethodAndPostcode($arrivalDate, $method, $postcode)
    {
        if (!($arrivalDate instanceof DateTime))
        {
            $arrivalDate = new DateTime($arrivalDate);
        }

        Mage::log("Calculating the ship date for arrival date of {$arrivalDate->format('Y-m-d')} using method of $method to postcode $postcode");

        // first try and find the shippingzonemethodtime
        if ($shippingZones = Mage::getModel('frans/shippingZone')->loadByPostcode($postcode))
        {
            foreach($shippingZones as $shippingZone) {


                $foundInvalid = false;
                // find the matching method time
                foreach ($shippingZone->getShippingMethodTimes() as $methodTime) {
                    if ($methodTime->getShippingMethod() == $method) {
                        Mage::log("Found matching ShippingZoneMethodTime with ID of {$methodTime->getId()}, calculating ship date from it");
                        $checkDate = clone $arrivalDate;
                        $shipDate = $checkDate
                            ->add(DateInterval::createFromDateString('-' . $methodTime->getTimeInDays() . ' days'))
                            ->format('Y-m-d');
                        Mage::log("Determined ship date of $shipDate, checking to make sure it is valid for shipping.");

                        $shippingBlackoutDate = Mage::getModel('frans/shippingBlackoutDate')->load($shipDate, 'blackout_date');

                        if ($shippingBlackoutDate->getId()) {
                            Mage::log("not valid for shipping, trying to look for others.");
                            $foundInvalid = true;
                            continue;

                        } else {
                            return $shipDate;
                        }
                    }
                }
            }

            if ($foundInvalid && $shipDate)
            {
                // this means we found one but it is invalid
                // we should at least pretend that we have on, so use the last invalid one.
                return $shipDate;
            }
        }

        return '';
    }

    /**
     * Applies suggested address as stored in proposed address field
     * No return value, applied directly to object that was passed in
     * It will be on the caller to save the object.
     *
     * @param Mage_Customer_Model_Address $address
     * @param array $addressData
     * @internal param $Mage_Customer_Model_Address_Abstract
     */
    public function applyChangesFromAddressValidation($address, $addressData)
    {
        $address->addData($addressData);
        $address->implodeStreetAddress();
    }

    /**
     *
     * Logs activity (if necessary) for an object
     *
     * @param Mage_Core_Model_Abstract $entity
     * @param string    $activityType
     */
    public function addActivity($entity, $entityType, $activityType, $entityId = false, $description = '')
    {
        if ($entity->getId())
        {
            // can specify an entity ID so that we don't have issues
            // with child objects
            $entityId = ($entityId === false) ? $entity->getId() : $entityId;

            $details = $this->_buildActivityDetails($entity);

            if ($description)
            {
                $details[GoSolid_Frans_Model_Activity::FIELD_DESCRIPTION ] = $description;
            }


            $this->_saveActivity($entityId, $entityType, $activityType, $details);
        }
    }

    /**
     * Saves activity for a changed object. Filters out unchanged objects
     * by excluding things with no changed fields.
     *
     * @param $entity
     * @param $entityType
     * @param $activityType
     * @param bool $entityId
     */
    public function addChangeActivity($entity, $entityType, $activityType, $entityId = false, $description = '')
    {
        if ($entity->getId())
        {
            $details = $this->_buildActivityDetails($entity);

            // for changes, nothing may actually have changed, so skip it there are no details
            if ($details)
            {
                // can specify an entity ID so that we don't have issues
                // with child objects
                $entityId = ($entityId === false) ? $entity->getId() : $entityId;

                // we can sometimes have a description field for cases where it's not the actual item being saved
                // in those cases, we append the array with our description
                if ($description)
                {
                    $details[GoSolid_Frans_Model_Activity::FIELD_DESCRIPTION ] = $description;
                }

                $this->_saveActivity($entityId, $entityType, $activityType, $details);
            }
        }
    }

    /**
     * Saves activity for a changed object. Requires you to supply specific
     * before/after values, rather than figuring it out from the object
     *
     * @param string $entityType Type of entity that we log
     * @param $activityType Type of activity that happened
     * @param bool $entityId ID of entity to log for
     * @param string $description description of item that changed
     * @param array $oldData old data to log
     * @param array $newData new data that was set
     */
    public function addSpecificChangeActivity(
        $entityType
        , $activityType
        , $entityId
        , $oldData
        , $newData
        , $description = ''
    )
    {
        $details = $this->_buildDataChanges($oldData, $newData);

        // for changes, nothing may actually have changed, so skip it there are no details
        if ($details)
        {
            // we can sometimes have a description field for cases where it's not the actual item being saved
            // in those cases, we append the array with our description
            if ($description)
            {
                $details[GoSolid_Frans_Model_Activity::FIELD_DESCRIPTION ] = $description;
            }

            $this->_saveActivity($entityId, $entityType, $activityType, $details);
        }
    }

	// returns a formatted string for displaying a product's price (or price range for configurable products)
	public function getCatalogPrices($product){
		$prices = $product->getPricesAsArray();
		$priceCount = count($prices);
		$helper = Mage::helper('core');
		$output = '';
		if($priceCount > 0){
			$output = $helper->currency($prices[0], true, false);
		}
		if($priceCount > 1){
			$output .= ' - ' . $helper->currency(end($prices), true, false);
		}
		// remove decimals from exact-dollar prices
		return str_replace('.00', '', $output);
	}

	// takes a number from 1-10 and returns a string representation of that number (e.g. "three")
	// used for generating Fran's tile class names
	public function getNumberText($number){
		$numbers = array(
			1   => 'one',
			2   => 'two',
			3   => 'three',
			4   => 'four',
			5   => 'five',
			6   => 'six',
			7   => 'seven',
			8   => 'eight',
			9   => 'nine',
			10  => 'ten',
		);
		return ($numbers[$number] == null) ? '' : $numbers[$number];
	}

    /**
     *
     * Persists activity record
     *
     * @param integer $entityId
     * @param string $entityType
     * @param string $activityType
     * @param array $details
     */

    protected function _saveActivity($entityId, $entityType, $activityType, $details)
    {
        $activity = Mage::getModel('frans/activity')
            ->setCreatedAt(Mage::getModel('core/date')->gmtDate())
            ->setCreatedBy($this->_getCurrentUserName())
            ->setEntityType($entityType)
            ->setActivityType($activityType)
            ->setEntityId( $entityId)
            ->setDetails(json_encode($details));
        $activity->save();
    }

    private function _getCurrentUserName()
    {
        $user = new Varien_Object();

        // TODO: make sure the customer session & cron work
        // cannot currently test with orders
        if (Mage::getSingleton('admin/session')->isLoggedIn())
        {
            $user = Mage::getSingleton('admin/session')->getUser();
        }
        else if (Mage::getSingleton('customer/session')->isLoggedIn())
        {
            $user = Mage::getSingleton('customer/session')->getCustomer();
        }

        if ($user->getId())
        {
            return $user->getFirstname() . ' ' . $user->getLastname();
        }

        // TODO: figure out cron jobs
        return '';
    }


    /**
     *  Builds activity details based on entity changes
     *
     * @param Mage_Core_Model_Abstract $entity
     * @param string    $activityType
     * @return array
     */

    private function _buildActivityDetails($entity)
    {
        // only care about changes if not new
        // note that there is an issue here in the case of new-ish objects (just saved)
        // they won't have an origData for the ID, but they may also not have changes
        // as an example, an order is saved several times after placement
        // so you see several empty update records
        if ($entity->getOrigData($entity->getIdFieldName()))
        {

            $origData = $entity->getOrigData();
            $newData = $this->_getFilteredNewData($entity->getData());

            return $this->_buildDataChanges($origData, $newData);
        }

        return '';
    }

    /**
     * Builds an array with the data changes between two arrays
     * Loosely equal values will be filtered out
     *
     * @param $oldData
     * @param $newData
     */
    private function _buildDataChanges($origData, $newData)
    {
        $changes = array();

        // need to compare both ways to find missing values from both
        $allChanges = array_merge(array_diff_assoc($origData, $newData), array_diff_assoc($newData, $origData));

        foreach ($allChanges as $key => $value)
        {
            $origDataValue = isset($origData[$key]) ? $origData[$key] : '';
            $newDataValue = isset($newData[$key]) ? $newData[$key] : '';

            // sometimes things are equal, but not strictly, which we don't want to log
            if ($origDataValue == $newDataValue ||
                ( (!$origDataValue && is_numeric($newDataValue) && floatval($newDataValue) == 0)
                    || (is_numeric($origDataValue) && (floatval($origDataValue) == 0) && !$newDataValue) )
            )
            {
                continue;
            }

            $changes[$key] = array(
                'old' => $origDataValue
            , 'new' => $newDataValue
            );
        }

        return $changes;
    }

    /**
     * Filters out things that happen occasionally that we don't want
     * like post data that inadvertently gets entered
     *
     * @param $data
     */
    private function _getFilteredNewData($data)
    {
        $filterKeys = array('form_key','is_in_process', 'qty_shipped');
        return array_diff_key( $data, array_flip($filterKeys));
    }

    function cleanUrlForDisplay($url) {
        $disallowed = array('http://www.', 'https://www.', 'http://dev.', 'http:');
        foreach($disallowed as $d) {
            if(strpos($url, $d) === 0) {
                return str_replace($d, '', $url);
            }
        }
        return $url;
    }

    public function replaceStringInFile($filename, $string_to_replace, $replace_with){
        $content = file_get_contents($filename);
        $contentChunks = explode($string_to_replace, $content);
        $content = implode($replace_with, $contentChunks);
        $content = $this->all_ascii($content);
        file_put_contents($filename, $content);
    }


    public function all_ascii($stringIn){
        $final = '';
        $search = array(
            chr(145),
            chr(146),
            chr(147),
            chr(148),
            chr(150),
            chr(151),
            chr(180),
        );

        $replace = array(
            "'",
            "'",
            '&quot;',
            '&quot;',
            '&ndash;',
            '&ndash;',
            "'",
            );

        $hold = str_replace($search,$replace,$stringIn);

        if(!function_exists('str_split')){
            function str_split($string,$split_length=1){
                $count = strlen($string);
                if (($split_length < 1) || ($count  > 1)) {
                    return array($string);
                }
                else
                {
                    $num = (int)ceil($count/$split_length);
                    $ret = array();
                    for($i=0;$i<$num;$i++){
                        $ret[] = substr($string,$i*$split_length,$split_length);
                    }
                    return $ret;
                }
            }
        }

        $holdarr = str_split($hold);
        foreach ($holdarr as $val) {
            if (ord($val) < 128) $final .= $val;
        }
        return $final;
    }

    function getScoreFromAddressValidationResult($address, $result){

        $attributes = array_reduce($result->AddressResults->Attributes, function($r, $i){
            $r[$i->Name] = filter_var($i->Value, FILTER_VALIDATE_BOOLEAN);
            return $r;
        });

        $score = 100;

        foreach(self::$ADDRESS_VAILDATION_ATTRIBUTES as $key => $vals){
            if(array_key_exists($key, $attributes) && $attributes[$key] != $vals[0]){
                $score += $vals[1];
            }
        }

        // three more checks: city, state and zip
        if(strtoupper(rtrim($address->getCity())) !== strtoupper(rtrim($result->AddressResults->EffectiveAddress->City))){
            $score += -25;
        }

        if(strtoupper(rtrim($address->getRegionCode())) !== strtoupper(rtrim($result->AddressResults->EffectiveAddress->StateOrProvinceCode))){
            $score += -50;
        }

        try{
            if(strlen($address->getPostcode()) > 5){
              if($address->getPostcode() !== $result->AddressResults->EffectiveAddress->PostalCode){
                $score += -50;
              }
            }
            else
            {
              $zip_code_base = explode('-', $result->AddressResults->EffectiveAddress->PostalCode)[0];
              if($address->getPostcode() !== $zip_code_base){
                $score += -50;
              }
            }
        } catch (Exception $e) { }

        return max($score, 0);

    }

    function getScoreDescription($address, $result){

        $attributes = array_reduce($result->AddressResults->Attributes, function($r, $i){
            $r[$i->Name] = filter_var($i->Value, FILTER_VALIDATE_BOOLEAN);
            return $r;
        });

        $description = array();

        foreach(self::$ADDRESS_VAILDATION_ATTRIBUTES as $key => $vals){
            if(array_key_exists($key, $attributes) && $attributes[$key] != $vals[0] && array_key_exists($key, self::$ADDRESS_VALIDATION_MESSAGES)){
                $description[] = self::$ADDRESS_VALIDATION_MESSAGES[$key];
            }
        }

        if(strtoupper(rtrim($address->getCity())) !== strtoupper(rtrim($result->AddressResults->EffectiveAddress->City))){
            $description[] = "City does not match.";
        }

        if(strtoupper(rtrim($address->getRegionCode())) !== strtoupper(rtrim($result->AddressResults->EffectiveAddress->StateOrProvinceCode))){
            $description[] = "State does not match.";
        }

        try{
            if(strlen($address->getPostcode()) > 5){
              if($address->getPostcode() !== $result->AddressResults->EffectiveAddress->PostalCode){
                $description[] = "Zip code does not match.";
              }
            }
            else
            {
              $zip_code_base = explode('-', $result->AddressResults->EffectiveAddress->PostalCode)[0];
              if($address->getPostcode() !== $zip_code_base){
                $description[] = "Zip code does not match.";
              }
            }
        } catch (Exception $e) { }

        return $description;

    }


}
