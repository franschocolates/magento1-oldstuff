<?php
class GoSolid_Frans_Helper_Image extends Mage_Core_Helper_Abstract
{

	private $_cacheBasePath = null;
	private	$_cacheBaseUrl = null;

	public function __construct()
	{
		$this->_cacheBasePath = Mage::getBaseDir('media') . DS . "catalog" . DS . "product" . DS . "cache";
		$this->_cacheBaseUrl = Mage::getBaseUrl('media') . "catalog/product/cache";
	}

	private function _getFilepathDirectories($directories)
	{
		return str_replace(array("/","\\"), DS, $directories);
	}

	private function _getUrlDirectories($directories)
	{
		return str_replace(array("/","\\"), "/", $directories);
	}

	private function _createCachedImage($filePath, $width = null, $height = null, $quality = 85)
	{
		//attempt to create the passed image as cached image,.
		$path_parts = pathinfo($filePath);
		$extension = $path_parts['extension'];
		$image = $path_parts['filename'];
		// get the directory structure starting with the media folder, and ending with the parent of the file
		$directories = str_replace(Mage::getBaseDir('media'), '', $path_parts['dirname']);

		// create name for cached file
		$widthLabel = ($width ? $width : 'auto');
		$heightLabel = ($height ? $height : 'auto');
		if($widthLabel == 'auto' && $heightLabel == 'auto'){
			$heightLabel = 'orig';
			$widthLabel = 'orig';
		}

		//create the cached image filename
		$destinationFilename = $image . "_" . $widthLabel . "x" . $heightLabel . '.' . $extension;

		//combine cache path with cache filename
		$destinationFilePath = $this->_cacheBasePath . $this->_getFilepathDirectories($directories) . DS . $destinationFilename;
		$destinationUrl = $this->_cacheBaseUrl . $this->_getUrlDirectories($directories) . '/' . $destinationFilename;

		// if cached image doesn't exist, create it
		if (file_exists($destinationFilePath) == false)
		{
			// create folder
			if(!file_exists($this->_cacheBasePath)){
				mkdir($this->_cacheBasePath, 0777);
			}

			$imageObj = new Varien_Image($filePath);
			$imageObj->constrainOnly(true);
			$imageObj->keepAspectRatio(true);
			$imageObj->keepTransparency(true);
			$imageObj->keepFrame(false);
			$imageObj->quality($quality);

			// proportional scaling, if only one dimension is provided
			$originalWidth = $imageObj->getOriginalWidth();
			$originalHeight = $imageObj->getOriginalHeight();
			if($width && !$height){
				$height = $width * ($originalWidth / $originalHeight);
			} elseif($height && !$width){
				$width = $height * ($originalHeight / $originalWidth);
			}

			// we should have width and height now, but double-check in case neither was provided
			if($width && $height){
				$imageObj->resize($width, $height);
			}

			$imageObj->save($destinationFilePath);
		}

		return $destinationUrl;
	}

	// Cache (and optionally resize) any image; uses placeholder image if file specified doesn't exist
	public function getImageResized($filePath, $width = null, $height = null, $quality = 85)
	{

		//set the placeholder image path
		$placeholderDirectories = 'catalog/product/placeholder';
		$placeholderFilePath = Mage::getBaseDir('media') . DS  . $this->_getFilepathDirectories($placeholderDirectories) . DS . Mage::getStoreConfig('catalog/placeholder/image_placeholder');
		$placeholderUrl = Mage::getBaseUrl('media') . $this->_getUrlDirectories($placeholderDirectories) . Mage::getStoreConfig('catalog/placeholder/image_placeholder');

		//finalPath
		$destinationUrl = null;

		try
		{
			//attempt to resize the original image.
			$destinationUrl = $this->_createCachedImage($filePath, $width, $height, $quality);
		}
		catch (Exception $ex)
		{
			//attempt to resize the placeholder image.
			try{
				$destinationUrl = $this->_createCachedImage($placeholderFilePath, $width, $height, $quality);
			} catch (Exception $ex2)
			{
				//just pass back the original image.
				$destinationUrl = $placeholderUrl;
			}
		}

		//convert the provided filePath to a URL



		return $destinationUrl;
	}

	// Cache (and optionally resize) a product's image (specify attribute code, e.g. "thumbnail", in param)
	public function getImageResizedByProduct($product, $attributes = array('thumbnail'), $width = null, $height = null, $quality = 85)
	{
		$path = '';
		$storeId = Mage::app()->getStore()->getId();
		$filename = '';

		// $attributes should be an array of attributes listed in priority order; if a string is provided, convert to array
		if(!is_array($attributes))
		{
			$attributes = array($attributes);
		}

		// loop through attributes until an image is found
		foreach($attributes as $attribute)
		{
			$filename = Mage::getResourceModel('catalog/product')->getAttributeRawValue($product->getId(), $attribute, $storeId);
			if($filename !== 'no_selection')
			{
				break; // image found, don't loop any more
			}
		}

		if($product instanceof GoSolid_Frans_Model_Catalog_Product){
			$path = Mage::getBaseDir('media') . DS . "catalog" . DS . "product" . $filename;
		}
		return $this->getImageResized($path, $width, $height, $quality);
	}



}