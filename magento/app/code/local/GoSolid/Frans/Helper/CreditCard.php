<?php
class GoSolid_Frans_Helper_CreditCard extends Mage_Core_Helper_Abstract
{

	// format the last 4 digits to look like a full-length, masked number with optional digit placeholders $p and separators $s
	public function formatLast4ByCardType($last4, $cardType, $p = 'X', $s = '-')
	{
		$formattedCc = '';
		switch($cardType):
			case "AE":
				$formattedCc = $p.$p.$p.$p . $s . $p.$p.$p.$p.$p.$p . $s . $p . $last4; // XXXX-XXXXXX-X3333
				break;
			case "DC":
				$formattedCc = $p.$p.$p.$p . $s . $p.$p.$p.$p . $s . $p.$p.$p.$p . $s . $last4; // XXXX-XXXX-XXXX-3333
				break;
			case "MC":
				$formattedCc = $p.$p.$p.$p . $s . $p.$p.$p.$p . $s . $p.$p.$p.$p . $s . $last4; // XXXX-XXXX-XXXX-3333
				break;
			case "Visa":
				$formattedCc = $p.$p.$p.$p . $s . $p.$p.$p.$p . $s . $p.$p.$p.$p . $s . $last4; // XXXX-XXXX-XXXX-3333
				break;
			default:
				$formattedCc = $p.$p.$p.$p . $s . $p.$p.$p.$p . $s . $p.$p.$p.$p . $s . $last4; // XXXX-XXXX-XXXX-3333
				break;
		endswitch;
		return $formattedCc;
	}

	// get the human-readable CC type label from the two-character type code
	public function getCardTypeLabel($cardType)
	{
		$cardTypeLabel = '';
		switch($cardType):
			case "AE":
				$ccTypeLabel = $this->__('American Express');
				break;
			case "DC":
				$ccTypeLabel = $this->__('Discover');
				break;
			case "MC":
				$ccTypeLabel = $this->__('MasterCard');
				break;
			case "Visa":
				$ccTypeLabel = $this->__('Visa');
				break;
			default:
				$ccTypeLabel = $this->__('Card Information');
				break;
		endswitch;
		return $ccTypeLabel;
	}

}