<?php

/*
 * Uses lib/html2pdf to generate pdf files.
 */
require_once("lib/html2pdf/html2pdf.class.php");
class GoSolid_Frans_Helper_Pdf extends Mage_Core_Helper_Abstract
{
    const PDF_FILE_EXT = "pdf";

    const OUTPUT_BROWSER = 1;
    const OUTPUT_DOWNLOAD = 2;
    const OUTPUT_SERVER = 3;

    const PAGESIZE_A4 = "A4";
    const PAGESIZE_A7 = "A7";
    const PAGESIZE_LETTER = "LETTER";

    //convert the passed html into the correct output file.
    public function htmlToPdf($html, $outputType, $filename = "", $size = self::PAGESIZE_LETTER)
    {
        $html2pdf = new HTML2PDF('P',$size,'en', true, 'UTF-8', array(0, 0, 0, 0));

        //$html2pdf->addFont('Gothic', '', 'lib/html2pdf/_tcpdf_5.0.002/fonts/gothic.php');
        //$html2pdf->addTTFfont('/lib/html2pdf/_tcpdf_5.0.002/fonts/gothic.ttf', 'TrueTypeUnicode', '', 32);


        $html2pdf->WriteHTML($html);



        //auto generate a default file name
        $fn = $this->generateFilename();
        if($filename != "")
        {
            $fn = $this->appendFileExt($filename);
        }

        //generate the PDF file.
        switch($outputType)
        {
            case self::OUTPUT_BROWSER:
                //to browser
                $html2pdf->Output($fn);
                exit; //otherwise we write stuff to after the PDF and that will cause issues.
                break;
            case self::OUTPUT_DOWNLOAD:
                //to a file download
                $html2pdf->Output($fn, 'D');
                break;
            case self::OUTPUT_SERVER:
                $html2pdf->Output($fn, 'F');
                break;
            default:
                //to browser
                $html2pdf->Output($fn);
                exit;  //otherwise we write stuff to after the PDF and that will cause issues.
                break;
        }

        return $fn;

    }

    public  function generateFilename($prefix = "")
    {
        return $prefix . date('m-d-Y_His').'.' . GoSolid_Frans_Helper_Pdf::PDF_FILE_EXT;
    }

    /*
     * Appends the file extension, if it does not exist already.
     */
    private function appendFileExt($filename)
    {
        $currentExt =  pathinfo($filename, PATHINFO_EXTENSION);
        if($currentExt == "")
        {
            $filename .= "." . self::PDF_FILE_EXT;
        }

        return $filename;
    }

}