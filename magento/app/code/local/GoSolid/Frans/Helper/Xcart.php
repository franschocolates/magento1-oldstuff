<?php

class GoSolid_Frans_Helper_Xcart extends Mage_Core_Helper_Abstract
{
    const XCART_CONFIG_SECTION = 'frans/xcart/';
    const XCART_CONFIG_BASE_ADMIN_URL = 'base_admin_url';

    // note the %s token for the ship ID
    const ORDER_SEARCH_URL =
        'search_shipments.php?order_ship_id=%s&search_action=search_by_order_ship_id&page_action=search_results';

    public function getOrderSearchUrl($orderNumber)
    {
        return sprintf($this->_getBaseAdminUrl() . self::ORDER_SEARCH_URL, $orderNumber);
    }

    protected function _getBaseAdminUrl()
    {
        return $this->_getConfig(self::XCART_CONFIG_BASE_ADMIN_URL);
    }

    protected function _getConfig($settingName)
    {
        return Mage::getStoreConfig(self::XCART_CONFIG_SECTION . $settingName);
    }
}