<?php
/**
 * Created by goSolid.
 * Date: 9/29/14
 * Time: 11:31 AM
 */

class GoSolid_Frans_Helper_Catalog_Category extends Mage_Catalog_Helper_Category
{

	const FILE_EXTENSION_ICON = '.png'; // file extension for images displayed below admin dropdowns

	public function getProductGridLayouts(){

		// omit file extensions because each layout has a .phtml template and a .png icon for selection in backend
		// we cannot easily reassign array keys for existing layouts, but space is provided for inserting new ones
		$layouts = array(
			200     => array("name" => "One Up, Two Down (Anchored)", "file" => "one-up-two-down-anchored"),
			300     => array("name" => "One Up, Two Mid, Two Down", "file" => "one-up-two-mid-two-down"),
			500     => array("name" => "Two Up", "file" => "two-up"),
			550     => array("name" => "Two Up, One Down", "file" => "two-up-one-down"),
			600     => array("name" => "Two Up, One Mid, One Down", "file" => "two-up-one-mid-one-down"),
			700     => array("name" => "Two Up, Two Mid, One Down", "file" => "two-up-two-mid-one-down"),
			800     => array("name" => "Two Up, Three Down", "file" => "two-up-three-down"),
			1000    => array("name" => "Three Up", "file" => "three-up"),
			8000    => array("name" => "Collection: Seasonal", "file" => "collection-seasonal"),
			12000   => array("name" => "Gifts: Parties and Events", "file" => "gifts-parties-events")
		);
		return $layouts;

	}

	public function getProductGridLayoutTemplates(){
		$layouts = $this->getProductGridLayouts();
		foreach($layouts as $key => $value){
			$layouts[$key] = 'catalog/product/list/layout/' . $value['file'] . '.phtml';
		}
		return $layouts;
	}

	// master icon template file is located at /docs/assets/product-grid-layout-icons.psd (outside of Magento root)
	public function getProductGridLayoutIcons(){
		$layouts = $this->getProductGridLayouts();
		foreach($layouts as $key => $value){
			$layouts[$key] = Mage::getDesign()->getSkinUrl('images/product-grid-layout-icons/' . $value['file'] . self::FILE_EXTENSION_ICON);
		}
		return $layouts;
	}

	public function getChildCategoryLayouts(){

		// omit file extensions because each layout has a .phtml template and a .png icon for selection in backend
		// we cannot easily reassign array keys for existing layouts, but space is provided for inserting new ones
		$layouts = array(
			0       => array("name" => "Do Not Display Child Categories"), // special case omits a layout file
			300     => array("name" => "Alternating Details, Right First", "file" => "alternating-details-right-first"),
			390     => array("name" => "Alternating Inset, Left First", "file" => "alternating-inset-left-first"),
			400     => array("name" => "Alternating Inset, Left First, No Margin", "file" => "alternating-inset-left-first-no-margin"),
			410     => array("name" => "Alternating Inset, Right First", "file" => "alternating-inset-right-first"),
			420     => array("name" => "Alternating Inset, Right First, No Margin", "file" => "alternating-inset-right-first-no-margin"),
			550     => array("name" => "Two Up Plus Right Feature", "file" => "two-up-right-feature"),
			1000    => array("name" => "Three Up Plus Left Feature", "file" => "three-up-left-feature"),
			5000    => array("name" => "Gifts", "file" => "gifts"),
			6000    => array("name" => "Gifts 2", "file" => "gifts-2")
		);
		return $layouts;

	}

	public function getChildCategoryLayoutTemplates(){
		$layouts = $this->getChildCategoryLayouts();
		foreach($layouts as $key => $value){
			if(array_key_exists('file', $value))
			{
				$layouts[$key] = 'catalog/category/list/layout/' . $value['file'] . '.phtml';
			}
			else
			{
				unset($layouts[$key]); // if there's no file set for this layout, remove it
			}
		}
		return $layouts;
	}

	// master icon template file is located at /docs/assets/child-category-layout-icons.psd (outside of Magento root)
	public function getChildCategoryLayoutIcons(){
		$layouts = $this->getChildCategoryLayouts();
		foreach($layouts as $key => $value){
			if(array_key_exists('file', $value))
			{
				$layouts[$key] = Mage::getDesign()->getSkinUrl('images/child-category-layout-icons/' . $value['file'] . self::FILE_EXTENSION_ICON);
			}
			else
			{
				unset($layouts[$key]); // if there's no file set for this layout, remove it
			}
		}
		return $layouts;
	}

	// for products; returns the HTML class to apply, making text over the image either light or dark
	public function getProductImageClass($product){
		$productImageStyle = $product->getProductImageStyle();
		$class = '';
		switch($productImageStyle){
			case '1': // dark
				$class = 'dark';
				break;
			default: // default to light;
				$class = 'light-tile';
				break;
		}
		return $class;
	}

	// for categories; returns the HTML class to apply, making text over the image either light or dark
	public function getTileImageClass($category){
		$tileImageStyle = $category->getTileImageStyle();
		$class = '';
		switch($tileImageStyle){
			case '2': // dark
				$class = 'dark';
				break;
			default: // default to light;
				$class = 'light-tile';
				break;
		}
		return $class;
	}

	// return array of a category's child categories
	public function getChildCategories($parentCategory)
	{
		if($parentCategory->hasChildren()){
			$childCategoryIds = $parentCategory->getChildren(); // a comma-delimited string of category IDs
			$categories = explode(',', $childCategoryIds);
			$sortedCategories = array();
			foreach($categories as $childId){
				// sort the categories by position
				$category = Mage::getModel('catalog/category')->load($childId);
				// filter out cross-sell categories, those don't belong in this set
				if(!$category->getIsCrossSell()){
					$sortedCategories[($category->getPosition())] = $category;
				}
			}
			// actually perform the sort to put the categories in the right order, and make array keys sequential
			ksort($sortedCategories);
			$categories = array_values($sortedCategories);
		} else {
			$categories = array();
		}
		return $categories;
	}

	// compares categories with reserved list, returns array separated into "reserved" and "available" subsets
	public function separateReservedCategories($categories, $reservedCategories)
	{
		$separatedCategories = array(
			'available' => $categories,
			'reserved'  => array()
		);
		foreach($categories as $index => $category){
			foreach($reservedCategories as $reservedKey){
				if($category->getUrlKey() == $reservedKey){
					// add the category to the reserved array and remove it from the available one
					$separatedCategories['reserved'][$reservedKey] = $category;
					unset($separatedCategories['available'][$index]);
					break;
				}
			}
		}
		return $separatedCategories;
	}

	public function getDisplaySections()
	{
		$displaySections = array(
			1   => array("name" => "Section 1"),
			2   => array("name" => "Section 2"),
			3   => array("name" => "Section 3")
		);
		return $displaySections;
	}

	public function sortCategoriesByDisplaySection($categories)
	{
		$categorySections = array();
		$allDisplaySectionOptions = Mage::getModel('frans/attribute_source_displaysection')->getAllOptions();
		foreach($allDisplaySectionOptions as $displaySectionOption)
		{
			$displaySectionNumber = $displaySectionOption['value'];
			$categorySections[$displaySectionNumber] = array();
			foreach($categories as $category)
			{
				if($category->getDisplaySection() == $displaySectionOption['value'])
				{
					$categorySections[$displaySectionNumber][] = $category;
				}
			}
		}
		return $categorySections;
	}

	/* Returns an array of categories from a comma-delimited list of category IDs in a config setting */
	public function getAllProductsCategoriesArray()
	{
		$categoryIdList = Mage::getStoreConfig('frans/all_products_page/category_ids');
		$categoryIds = explode(',', $categoryIdList);
		$categories = Mage::getModel('catalog/category')->getCollection()
			->setStoreId(Mage::app()->getStore()->getStoreId())
			->addAttributeToFilter('entity_id', array(
				'in' => $categoryIds
			))
			->addAttributeToFilter('is_active', array(
				'eq' => 1
			))
			->addAttributeToSelect(array(
				'name', 'children_count', 'all_products_tile_image', 'url_path'
			));
        if($categories instanceof Mage_Catalog_Model_Resource_Category_Flat_Collection){            
$categories->getSelect()->order(new Zend_Db_Expr("FIELD(entity_id,".$categoryIdList.")")); //for flat tables
        } else {
            $categories->getSelect()->order(new Zend_Db_Expr("FIELD(`e`.entity_id,".$categoryIdList.")")); // for non-flat tables
        }
		$categoryArray = array();
		foreach($categories as $category){
			$categoryArray[] = $category;
		}
		return $categoryArray;
	}

}
