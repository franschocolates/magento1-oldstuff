<?php
$installer = $this;
$installer->startSetup();



$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'is_on_hold', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
        'COMMENT'   => 'Flags if the order is on hold',
        'nullable'  => false,
    ));

// also need on sales order grid
$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_grid'), 'is_on_hold', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
        'COMMENT'   => 'Flags if the order is on hold',
        'nullable'  => false,
    ));


$installer->endSetup();