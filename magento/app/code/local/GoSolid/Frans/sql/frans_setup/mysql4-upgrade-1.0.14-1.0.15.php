<?php
$installer = $this;
$installer->startSetup();

$setup = Mage::getModel('customer/entity_setup', 'core_setup');

// Remove the old attribute
$setup->removeAttribute('customer' , 'customer_number');

// Add modified attribute 
$setup->addAttribute('customer', 'customer_number', array(
	'type' => Varien_Db_Ddl_Table::TYPE_VARCHAR,
	'input' => 'text',
	'label' => 'Customer Number',
	'global' => 1,
	'visible' => 1,
	'required' => 0,
	'default' => '',
	'visible_on_front' => 0,
	'source' =>   NULL,
));


Mage::getSingleton('eav/config')
	->getAttribute('customer', 'customer_number')
	->setData('used_in_forms', array('adminhtml_customer'))
	->setData('sort_order', 125)
	->save();

$installer->endSetup();