<?php

# This script removes the requirements for first/last name when registering a new account
$installer = $this; 
$installer->startSetup(); 

$installer->run("UPDATE `eav_attribute` SET is_required = 0 WHERE attribute_code IN ('firstname', 'lastname') AND entity_type_id = 1");

$installer->endSetup();