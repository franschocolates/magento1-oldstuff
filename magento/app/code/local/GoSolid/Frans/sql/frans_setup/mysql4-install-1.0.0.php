<?php
$installer = $this;
$installer->startSetup();
 
$setup = Mage::getModel('customer/entity_setup', 'core_setup');
 
$setup->addAttribute('customer', 'company_name', array(
	'type' => 'varchar',
	'input' => 'text',
	'label' => 'Company Name',
	'global' => 1,
	'visible' => 1,
	'required' => 0,
	'position' => 70,
	'user_defined' => 0,
	'default' => '',
	'visible_on_front' => 1,
	'source' =>   NULL,
));

Mage::getSingleton('eav/config')
	->getAttribute('customer', 'company_name')
	->setData('used_in_forms', array('adminhtml_customer','customer_account_edit','checkout_register','adminhtml_checkout'))
	->save();

$installer->endSetup();