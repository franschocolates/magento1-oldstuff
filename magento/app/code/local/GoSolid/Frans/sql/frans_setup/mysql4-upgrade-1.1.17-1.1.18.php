<?php
$installer = $this;
$installer->startSetup();

$sql = "

   DELETE FROM `{$this->getTable('helpdesk/status')}` WHERE `name` = 'Open';
   DELETE FROM `{$this->getTable('helpdesk/status')}` WHERE `name` = 'In Progress';
   DELETE FROM `{$this->getTable('helpdesk/status')}` WHERE `name` = 'Closed';

   INSERT INTO `{$this->getTable('helpdesk/status')}` (name,code,sort_order,color,status_id) VALUES ('New','new','10', 'green', '1');
   INSERT INTO `{$this->getTable('helpdesk/status')}` (name,code,sort_order,color,status_id) VALUES ('In Progress','in_progress','20', 'red','2');
   INSERT INTO `{$this->getTable('helpdesk/status')}` (name,code,sort_order,color,status_id) VALUES ('Complete ','complete','30', 'blue','3');
";

$installer->run($sql);

$installer->endSetup();
