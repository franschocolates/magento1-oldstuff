<?php

$installer = $this;
$installer->startSetup();

// Add new Google Shopping attributes
if(!$installer->getAttributeId('catalog_product', 'shop_on_google')){
    $this->addAttribute('catalog_product', 'shop_on_google', array(
        'type' => 'int',
        'backend' => '',
        'frontend' => '',
        'source' => 'eav/entity_attribute_source_boolean',
        'input' => 'boolean',
        'label' => 'Shop On Google',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'visible' => true,
        'required' => false,
        'user_defined' => false,
        'default' => '0',
        'visible_on_front' => false,
        'unique' => false,
        'group' => 'Google Shopping',
        'used_in_product_listing' => false
    ));
}

if(!$installer->getAttributeId('catalog_product', 'google_product_category')) {
    $this->addAttribute('catalog_product', 'google_product_category', array(
        'type' => 'text',
        'frontend' => '',
        'input' => 'text',
        'label' => 'Google Product Category',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'visible' => true,
        'required' => false,
        'user_defined' => false,
        'default' => '',
        'visible_on_front' => false,
        'unique' => false,
        'group' => 'Google Shopping',
        'used_in_product_listing' => false
    ));
}

$fieldList = array(
    'shop_on_google',
    'google_product_category'
    );

    // make these attributes applicable to simple products
    foreach ($fieldList as $field) {
        $applyTo = str_split(',', $installer->getAttribute('catalog_product', $field, 'apply_to'));
        if (!in_array('simple', $applyTo)) {
            $applyTo[] = 'simple';
            $installer->updateAttribute('catalog_product', $field, 'apply_to', join(',', $applyTo));
        }
    }

//create new attribute group and add two new attributes to it
$attributeId1  = $installer->getAttributeId('catalog_product', 'shop_on_google');
$attributeId2  = $installer->getAttributeId('catalog_product', 'google_product_category');

$entityTypeId = $installer->getEntityTypeId('catalog_product');
$attributeSets = $installer->_conn->fetchAll('select * from '.$this->getTable('eav/attribute_set').' where entity_type_id=?', $entityTypeId);
foreach ($attributeSets as $attributeSet) {
        $setId = $attributeSet['attribute_set_id'];
    if(!$installer->getAttributeGroupId($entityTypeId, $setId, 'Google Shopping')) {
        $installer->addAttributeGroup($entityTypeId, $setId, 'Google Shopping');
        $groupId = $installer->getAttributeGroupId($entityTypeId, $setId, 'Google Shopping');
        $installer->addAttributeToGroup($entityTypeId, $setId, $groupId, $attributeId1);
        $installer->addAttributeToGroup($entityTypeId, $setId, $groupId, $attributeId2);
    }
}

$installer->endSetup();