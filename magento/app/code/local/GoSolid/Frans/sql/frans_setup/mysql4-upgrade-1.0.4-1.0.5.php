<?php

# This script adds the columns needed to support our sales/order for multiship

$installer = $this; 
$installer->startSetup(); 

$installer->run("ALTER TABLE `{$installer->getTable('frans/retailStore')}` 
	ADD COLUMN `position` INT(11) AFTER `status`;");

$installer->endSetup();
