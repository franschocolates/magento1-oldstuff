<?php

# This script updates the attribute_id of the 'phone' number field from 155 to 188 which is the new attribute_id for the 'phone' number field

$installer = $this;

$installer->startSetup();

$installer->run("UPDATE IGNORE `customer_entity_varchar` SET attribute_id = 188 WHERE attribute_id = 155;");

$installer->endSetup();
