<?php

$installer = $this;
$installer->startSetup();

$sql = <<<MySQL
DROP TABLE IF EXISTS default_billing_tmp;

CREATE TEMPORARY TABLE

IF NOT EXISTS default_billing_tmp (INDEX (`entity_id`)) AS (
		SELECT `e`.`entity_id` AS `entity_id`
		FROM `customer_entity` AS `e`
		INNER JOIN `customer_entity_int` AS `at_default_shipping` ON (`at_default_shipping`.`entity_id` = `e`.`entity_id`)
			AND (`at_default_shipping`.`attribute_id` = '14')
		WHERE (`e`.`entity_type_id` = '1')
			AND (at_default_shipping.value IS NOT NULL)
		);
	DELETE
	FROM customer_entity_int
	WHERE attribute_id = (
			SELECT attribute_id
			FROM eav_attribute
			WHERE attribute_code = 'default_shipping'
			)
		AND entity_id IN (
			SELECT *
			FROM default_billing_tmp
			)
MySQL;

$installer->run($sql);

$installer->endSetup();