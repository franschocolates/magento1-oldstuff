<?php

$installer = $this;
$installer->startSetup();

$installer->run("ALTER TABLE sales_flat_order ADD COLUMN shipping_score SMALLINT NULL;");
$installer->run("ALTER TABLE sales_flat_order_grid ADD COLUMN shipping_score SMALLINT NULL;");

$installer->endSetup();

?>