<?php

# This script adds the columns needed to support our sales/order for multiship

$installer = $this; 
$installer->startSetup(); 

$installer->run("ALTER TABLE sales_flat_order ADD COLUMN is_multiship_parent SMALLINT NULL;");

$installer->run("ALTER TABLE sales_flat_order ADD COLUMN multiship_parent_id INT NULL;");

$installer->endSetup();
