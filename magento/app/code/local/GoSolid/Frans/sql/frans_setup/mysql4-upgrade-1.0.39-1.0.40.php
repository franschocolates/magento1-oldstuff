<?php



$installer = $this;
$installer->startSetup();

$installer->getConnection()

    ->addColumn('sales_order_status', 'status_frontend', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'LENGTH'    => 32,
        'COMMENT'   => 'The status that is assumed on the front end.',
        'nullable'  => false,
        'default'   => ''
    ));

$installer->getConnection()
    ->addColumn('sales_order_status', 'allowed_actions', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'LENGTH'    => 500,
        'COMMENT'   => 'The allowed actions when in this state. Actions = Status',
        'nullable'  => false,
        'default'   => ''
    ));


$installer->endSetup();