<?php

$installer = $this;
$installer->startSetup();

// fix position of customer telephone attribute in admin area
$installer->updateAttribute('customer', 'telephone', 'sort_order', 70);

$installer->endSetup();