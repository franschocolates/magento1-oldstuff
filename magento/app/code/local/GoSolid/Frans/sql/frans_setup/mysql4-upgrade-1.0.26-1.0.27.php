<?php
$installer = $this;
$installer->startSetup();


$installer->addAttribute("catalog_category", "category_image_style",  array(
    "type"     => "int",
    "backend"  => "",
    "frontend" => "",
    "label"    => "Category Image Style",
    "input"    => "select",
    "class"    => "",
    "source"   => "frans/catalog_category_attribute_source_imagestyle",
    "global"   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    "visible" => true,
    "required" => true,
    "user_defined" => true,
    "default" => "0",
    "visible_on_front" => true,
    "unique" => false,
    "group" => "General Information",
	"position" => 5,
	"note" => "Light Image Style: makes header text dark, Dark Image Style: makes header text lighter",
    "used_in_product_listing" => false
    ));
$installer->endSetup();