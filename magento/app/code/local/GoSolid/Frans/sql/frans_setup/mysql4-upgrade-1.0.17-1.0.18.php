<?php

$installer = $this;

$installer->startSetup();

$connection = $installer->getConnection();

/**
 *****************************************************************************
 * new tables for accounting originations & labels
 *****************************************************************************
 */

$originationTable = $connection->newTable($installer->getTable('frans/accounting_origination'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Id')
    ->addColumn('origination_name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
        'nullable'  => false,
        ), 'Name of origination');

$connection->createTable($originationTable);

$dateTable = $connection->newTable($installer->getTable('frans/accounting_label'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Id')
    ->addColumn('label_name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
        'nullable'  => false,
        ), 'Name of label')
    ->addColumn('accpac_customer_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
        'nullable'  => false,
        ), 'AccPac Customer ID');
        
$connection->createTable($dateTable);

$installer->endSetup();