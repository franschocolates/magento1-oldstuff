<?php

$installer = $this;
$installer->startSetup();

$setup = Mage::getModel('customer/entity_setup', 'core_setup');

$setup->addAttribute('customer', 'telephone', array(
                                                'type' => 'varchar',
                                                'input' => 'text',
                                                'label' => 'Phone',
                                                'global' => 1,
                                                'visible' => 0,
                                                'visible_on_front' => 0,
                                                'required' => 0,
                                                'position' => 70,
                                                'default' => '',
                                                'source' =>   NULL,
                                            ));

Mage::getSingleton('eav/config')
                    ->getAttribute('customer', 'telephone')
                    ->setData('used_in_forms', array('adminhtml_customer','adminhtml_checkout'))
                    ->save();

$installer->endSetup();