<?php

$installer = $this;
$installer->startSetup();

// Add attribute to customer
$installer->addAttribute('customer', 'subscription_origin', array(
        'label' => 'Subscription Origin',
        'input' => 'text',
        'required' => false,
        'user_defined' => 0,
        'source' => 'eav/entity_attribute_source_table',
        )
    );

Mage::getSingleton('eav/config')
    ->getAttribute('customer', 'subscription_origin')
    ->setData('used_in_forms', array('adminhtml_customer'))
    ->setData('sort_order', 105)
    ->save();


// Add column to newsletter
$installer->getConnection()
    ->addColumn(
        $installer->getTable('newsletter/subscriber'),
        'subscription_origin', array(
            'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
            'nullable'  => false,
            'length'    => 255,
            'after'     => null, // column name to insert new column after
            'default'   => '',
            'comment'   => 'From where the subscription came from'
        )
    );

$installer->endSetup();
