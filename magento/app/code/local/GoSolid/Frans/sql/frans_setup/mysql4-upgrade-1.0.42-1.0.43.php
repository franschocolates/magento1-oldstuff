<?php
$installer = $this;
$installer->startSetup();

// Add fields to track fedex info for order addresses
$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_address'), 'address_delivery_type', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'LENGTH'    => 50,
        'NULLABLE'  => true,
        'COMMENT'   => 'Delivery Type (residential, commercial, etc)'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_address'), 'proposed_address', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'NULLABLE'  => true,
        'COMMENT'   => 'Proposed alternative address'
    ));

$installer->endSetup();