<?php

/* @var $installer GoSolid_Frans_Model_Resource_Eav_Mysql4_Setup */

$installer = $this;
$installer->startSetup();

$installer->updateAttribute('catalog_product', 'sku', 'used_in_product_listing', 1);
$installer->endSetup();