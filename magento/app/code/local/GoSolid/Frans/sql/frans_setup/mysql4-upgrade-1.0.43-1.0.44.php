<?php
$installer = $this;
$installer->startSetup();

$updateEmailDateSql = <<<EOSQL
UPDATE %s
SET shipment_email_sent_at = CURRENT_TIMESTAMP()
EOSQL;

$fullSql = sprintf($updateEmailDateSql, $installer->getTable('sales/order'));
$installer->run($fullSql);

$installer->endSetup();