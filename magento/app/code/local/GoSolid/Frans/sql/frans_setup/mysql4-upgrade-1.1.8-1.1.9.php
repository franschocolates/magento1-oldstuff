<?php

$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();

$tableName = $installer->getTable('frans/customerNotes');
$this->addAttribute('customer', "customer_notes", array(
    'label' => "Customer Notes",
    'input' => "textarea",
    'type'  => "text",
    'required' => 0,
    'user_defined' => 1,
));

$table = $connection->newTable($tableName)
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Id')
    ->addColumn('customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => true,
        'unsigned'  => true,
    ), 'Customer ID')
    ->addColumn('email', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        'nullable'  => false,
    ), 'Email')
    ->addColumn('notes', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => true,
    ), 'Customer Notes');

if ($installer->getConnection()->isTableExists($tableName) != true) {
	$connection->createTable($table);
}

$installer->endSetup();