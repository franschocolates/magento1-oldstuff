<?php


/** @var $installer GoSolid_Frans_Model_Resource_Eav_Mysql4_Setup */
$installer = $this;
$installer->startSetup();

$installer->setConfigData('catalog/frontend/list_mode', 'grid-list');
$installer->setConfigData('catalog/frontend/grid_per_page_values', '10,15,25,50');
$installer->setConfigData('catalog/frontend/grid_per_page', '10');
$installer->setConfigData('catalog/frontend/list_per_page_values', '10,15,20,25');
$installer->setConfigData('catalog/frontend/list_per_page', '10');
$installer->setConfigData('catalog/frontend/list_allow_all', '0');
$installer->setConfigData('catalog/frontend/default_sort_by', 'position');

$installer->endSetup();