<?php

# This script is called once before the version of frans_setup is changed

$installer = $this;

$installer->startSetup(); 

$this->addAttribute('catalog_product', 'is_safe_crossings_product', array(
        'type' => 'int',
        'backend' => '',
        'frontend' => '',
		'source' => 'eav/entity_attribute_source_boolean',
		'input' => 'boolean',
        'label' => 'Safe Crossings Product',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'visible' => true,
        'required' => false,
        'user_defined' => true,
        'default' => '0',
        'visible_on_front' => false,
        'unique' => false,
		'group' => 'General',
		'used_in_product_listing' => false
    ));

$this->addAttribute('catalog_product', 'safe_crossings_donation', array(
        'type' => 'decimal',
        'frontend' => '',
		'input' => 'text',
        'label' => 'Safe Crossings Donation',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'visible' => true,
        'required' => false,
        'user_defined' => true,
        'default' => '0',
        'visible_on_front' => false,
        'unique' => false,
		'group' => 'General',
		'used_in_product_listing' => false
));
    
$installer->endSetup();