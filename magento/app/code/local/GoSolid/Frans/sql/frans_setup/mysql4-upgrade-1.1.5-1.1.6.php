<?php
$installer = $this;
$installer->startSetup();

/* insert DDL here */
$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_address'), 'location_hash', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'LENGTH'    => 255,
        'NULLABLE'  => true,
        'COMMENT'   => 'Used to determine whether the location described by the physical address has changed'
    ));

$installer->endSetup();