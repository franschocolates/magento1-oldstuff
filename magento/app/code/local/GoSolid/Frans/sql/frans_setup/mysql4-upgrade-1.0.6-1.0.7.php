<?php

$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('sales/shipment'), 'ice_packs', array(
    	'TYPE'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'NULLABLE'  => true,
    	'DEFAULT' 	=> 0,
        'COMMENT'   => 'Number of ice packs in shipment'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/shipment'), 'ship_date', array(
    	'TYPE'      => Varien_Db_Ddl_Table::TYPE_DATE,
        'NULLABLE'  => true,
        'COMMENT'   => 'Date shipment will be sent'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/shipment_track'), 'shipping_method', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'LENGTH'    => 255,
        'NULLABLE'  => true,
        'COMMENT'   => 'Shipping Method, both carrier and method'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/shipment_track'), 'shipping_description', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'NULLABLE'  => true,
        'LENGTH'    => 255,
    	'COMMENT'   => 'Shipping Description'
    ));  

$installer->endSetup(); 

?>