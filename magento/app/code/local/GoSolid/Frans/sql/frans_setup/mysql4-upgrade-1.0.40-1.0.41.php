<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address'), 'planned_ship_date', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_DATE,
        'COMMENT'   => 'Date we are planning to ship; quote values should only be used in the admin.',
        'required' => 0
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address'), 'base_shipping_amount_adjusted', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'scale'     => 4,
        'precision' => 12,
        'COMMENT'   => 'Adjusted base shipping amount for admin orders',
        'nullable'  => true,
    ));

// need planned ship date on grid for display
$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_grid'), 'planned_ship_date', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_DATE,
        'COMMENT'   => 'Date we are planning to ship, brought over from order table',
        'required' => 0
    ));

$updatePlannedShipDateSql = <<<EOSQL
UPDATE %s AS sfog
INNER JOIN %s AS sfo
ON sfo.entity_id = sfog.entity_id
SET sfog.planned_ship_date = sfo.planned_ship_date
EOSQL;

$fullSql = sprintf($updatePlannedShipDateSql, $installer->getTable('sales/order_grid'), $installer->getTable('sales/order'));
$installer->run($fullSql);

// set FedEx to be on, this appears to be ok and makes our admin methods show up in the backend.
Mage::getSingleton('core/config')->saveConfig('carriers/fedex/active', '1');

$installer->endSetup();