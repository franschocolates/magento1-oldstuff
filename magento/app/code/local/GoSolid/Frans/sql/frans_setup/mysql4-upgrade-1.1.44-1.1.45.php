<?php
/*
 * Remove Soczed Less compiler
 */
$installer = $this;
$installer->startSetup();

$statuses = ['printed', 'checked', 'ready_for_review', 'ready_to_print'];
foreach($statuses as $status){
    if(Mage::getModel("sales/order_status")->load($status, "status")->getStatus()){
        $fullStatus = Mage::getModel("sales/order_status")->load($status, "status");

        $fullStatus->setStatusFrontend('processing');
        $fullStatus->save();
    }
}
$installer->endSetup();













