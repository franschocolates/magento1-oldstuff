<?php
$installer = $this;
$installer->startSetup();

$table = $this->getTable('frans_color_tile');
$resource = Mage::getSingleton('core/resource');
$readConnection = $resource->getConnection('core_read');
$fields=$readConnection->describeTable($table);

if(!$fields['position']){

    //add position column
    //update column with ascending values and order
    $sql = <<<ENDOFSQL
    ALTER TABLE {$table}
	ADD COLUMN `position` INT(11) AFTER `status`;

	SET @counter = 0;

    UPDATE {$table} SET `position` = @counter := @counter + 1
    ORDER BY {$table}.`title` ASC
ENDOFSQL;

    $installer->run($sql);
}

$installer->endSetup();
