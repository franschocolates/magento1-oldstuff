<?php

# This script is called once before the version of frans_setup is changed

$installer = $this;

$installer->startSetup(); 

$installer->getConnection()
	->addColumn($installer->getTable('frans/accounting_label'), 'accpac_location_code', array(
	'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'LENGTH'    => 10,
	'COMMENT'   => 'Location code for Accpac',
	'required' => 0
));

$installer->getConnection()
	->addColumn($installer->getTable('frans/accounting_label'), 'accpac_freight_code', array(
	'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'LENGTH'    => 10,
	'COMMENT'   => 'Freigh code for Accpac',
	'required' => 0
));

$installer->endSetup();