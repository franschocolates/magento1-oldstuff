<?php

$installer = $this;
$installer->startSetup();

$permissionBlocks = $this->getTable('admin/permission_block');
$permissionVars = $this->getTable('admin/permission_variable');

$sql = "
   INSERT IGNORE INTO {$permissionBlocks} (block_name,is_allowed) VALUES ('frans/catalog_category_allproducts', '1');
   INSERT IGNORE INTO {$permissionBlocks} (block_name,is_allowed) VALUES ('frans/article_news','1');
   INSERT IGNORE INTO {$permissionBlocks} (block_name,is_allowed) VALUES ('helpdesk/email_satisfaction','1');

   INSERT IGNORE INTO {$permissionVars} (variable_name,is_allowed) VALUES ('general/store_information/tollfree_number', '1');
   INSERT IGNORE INTO {$permissionVars} (variable_name,is_allowed) VALUES ('shipping/origin/street_line1','1');
   INSERT IGNORE INTO {$permissionVars} (variable_name,is_allowed) VALUES ('shipping/origin/city','1');
   INSERT IGNORE INTO {$permissionVars} (variable_name,is_allowed) VALUES ('shipping/origin/postcode','1');
   INSERT IGNORE INTO {$permissionVars} (variable_name,is_allowed) VALUES ('design/footer/copyright','1');

";

$installer->run($sql);


$installer->endSetup();