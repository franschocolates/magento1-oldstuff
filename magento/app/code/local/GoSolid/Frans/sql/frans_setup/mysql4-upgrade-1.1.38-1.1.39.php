<?php
$installer = $this;
$installer->startSetup();

//$installer->removeAttribute('catalog_category', 'category_redirect_url');

if(!(Mage::getResourceModel('eav/entity_attribute')
    ->getIdByCode('catalog_category', 'category_redirect_url'))) {

    $installer->addAttribute("catalog_category", "category_redirect_url", array(
        'group' => 'General Information',
        'type' => 'varchar',
        'input' => 'text',
        "label" => "Category Redirect Url",
        "required" => false,
        "user_defined" => false,
        "default" => "",
        "unique" => false,
        'note' => 'Place a valid URL in this field to redirect users visiting this category\'s page to the URL.',
        "global" => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'visible' => true,
        'visible_on_front' => true,
        'used_in_product_listing' => false,
        'is_html_allowed_on_front' => false,
        'position' => '50',
        'sort_order' => 50
    ));
}

$installer->endSetup();