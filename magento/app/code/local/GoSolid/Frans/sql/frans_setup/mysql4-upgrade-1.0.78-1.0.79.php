<?php
$installer = $this;
$installer->startSetup();

//Insert business contact hours
$sql = "
    INSERT INTO `core_config_data` (`config_id`, `scope`, `scope_id`, `path`, `value`) VALUES(NULL,'default','0','general/store_information/business_contact_hours','Mon – Fri 8 AM – 4:30 PM (Pacific)');
";

$installer->run($sql);

$installer->endSetup();