<?php

$installer = $this;
$installer->startSetup();

// Remove the old attribute
$installer->removeAttribute('customer' , 'subscription_origin');

// Add attribute to customer
$installer->addAttribute('customer', 'subscription_origin', array(
        'label' => 'Subscription Origin',
        'input' => 'text',
        'required' => false,
        'user_defined' => 0,
        'source' => 'eav/entity_attribute_source_table',
        )
    );

$installer->endSetup();
