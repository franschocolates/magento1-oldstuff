<?php

# This script disables the wishlist module

$installer = $this;
$installer->startSetup();

$installer->run("DELETE FROM `core_config_data` WHERE `path` = 'wishlist/general/active';");
$installer->run("INSERT INTO `core_config_data` (`scope`, `scope_id`, `path`, `value`) VALUES ('default', 0, 'wishlist/general/active', '0');");

$installer->endSetup();
