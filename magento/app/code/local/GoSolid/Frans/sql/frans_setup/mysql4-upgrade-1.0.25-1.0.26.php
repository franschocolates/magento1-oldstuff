<?php

# This script updates the attribute front-end lables from Company to Business

$installer = $this; 
$installer->startSetup(); 

$installer->run("UPDATE eav_attribute SET frontend_label = 'Business' WHERE attribute_code = 'company';");

$installer->run("UPDATE eav_attribute SET frontend_label = 'Business Name' WHERE attribute_code = 'company_name';");

$installer->endSetup();
