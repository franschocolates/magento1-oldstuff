<?php
$installer = $this;
$installer->startSetup();

/* add new tables for tracking print jobs */
$printStationTable = $installer->getConnection()
    ->newTable($installer->getTable('frans/printStation'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Id')
    ->addColumn('station_name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        'nullable'  => false,
    ), 'Name of print station');
$installer->getConnection()->createTable($printStationTable);

$printJobTable = $installer->getConnection()
    ->newTable($installer->getTable('frans/printJob'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Id')
    ->addColumn('entity_type', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        'nullable'  => false,
    ), 'Type of entity being printed')
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false,
    ), 'ID of entity being printed')
    ->addColumn('print_station_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false,
    ), 'Print station to print to')
    ->addColumn('print_job_type', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
        'nullable'  => false,
    ), 'Type of print job')
    ->addColumn('print_status', Varien_Db_Ddl_Table::TYPE_CHAR, 1, array(
        'nullable'  => false,
    ), 'Status of job (queued, printed, failed)')
    ->addColumn('document', Varien_Db_Ddl_Table::TYPE_VARBINARY, '2m', array(
        'nullable'  => false,
        'comment' => 'Document Content',
    ), 'Document to print')
    ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable'  => false,
    ), 'Indicates when job updated')
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable'  => false,
    ), 'Indicates when job created');


$installer->getConnection()->createTable($printJobTable);
$installer->endSetup();