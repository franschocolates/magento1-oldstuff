<?php
$installer = $this;
$installer->startSetup();

/* insert DDL here */
$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'is_ready_for_capture', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
        'LENGTH'    => NULL,
        'COMMENT'   => 'Indicates whether an order is ready to be captured',
        'DEFAULT'   => '0',
        'nullable'  => false,
    ));


$installer->endSetup();