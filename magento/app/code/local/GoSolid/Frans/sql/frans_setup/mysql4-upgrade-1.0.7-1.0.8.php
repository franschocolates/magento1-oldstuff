<?php

# This script is called once before the version of frans_setup is changed

$installer = $this;
$installer->startSetup(); 

$attributesInfo = array(
    'dob' => array(
        'label'     => 'Date Of Birth',
        'type'      => 'datetime',
        'input'     => 'date',
		'backend'	=> 'eav/entity_attribute_backend_datetime',
		'frontend'	=> 'eav/entity_attribute_frontend_datetime',
        'sort'		=> 136,
		'order'		=> 136,
        'visible'   => true,
        'required'  => false
    ),
    'email' => array(
        'label'     => 'Email',
        'type'      => 'varchar',
        'input'     => 'text',
        'sort'		=> 53,
    	'order'		=> 53,
        'visible'   => true,
        'required'  => false
    )
);

foreach ($attributesInfo as $attributeCode => $attributeParams) {
    $installer->addAttribute('customer_address', $attributeCode, $attributeParams);
    $attribute = Mage::getSingleton('eav/config')->getAttribute('customer_address', $attributeCode);
	$attribute->setData('used_in_forms', array(
	     'adminhtml_customer_address',
	     'customer_address_edit'
	));
	$attribute->save();
	    
}

$installer->getConnection()
    ->query( "UPDATE eav_attribute AS ea JOIN customer_eav_attribute AS eaf ON ea.attribute_id = eaf.attribute_id SET sort_order = 53 WHERE ea.entity_type_id = 2 AND ea.attribute_code = 'email';");

$installer->getConnection()
    ->query( "UPDATE eav_attribute AS ea JOIN customer_eav_attribute AS eaf ON ea.attribute_id = eaf.attribute_id SET sort_order = 136 WHERE ea.entity_type_id = 2 AND ea.attribute_code = 'dob';");
    
$installer->endSetup();

