<?php

$installer = $this;
$installer->startSetup();

$setup = Mage::getModel('customer/entity_setup', 'core_setup');

Mage::getSingleton('eav/config')
    ->getAttribute('customer', 'customer_notes')
    ->setData('used_in_forms', array())
    ->save();

$installer->endSetup();