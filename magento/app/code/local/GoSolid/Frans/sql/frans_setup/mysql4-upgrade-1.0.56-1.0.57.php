<?php
$installer = $this;
$installer->startSetup();

$sql = "
    update " . $installer->getTable('sales/order_grid') . " g
    join " . $installer->getTable('sales/order') . " o on o.entity_id = g.entity_id
    set g.shipping_method = o.shipping_method
";



$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_grid'), 'shipping_method', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'LENGTH'    => 255,
        'COMMENT'   => 'Ship method for order',
        'nullable'  => false,
    ));


//move the existing ship methods over.
$installer->run($sql);





$installer->endSetup();