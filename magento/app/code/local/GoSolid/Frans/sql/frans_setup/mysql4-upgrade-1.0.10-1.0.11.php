<?php

$installer = $this;

$installer->startSetup();

$connection = $installer->getConnection();

/**
 *****************************************************************************
 * add columns for address type & proposed address details on customer address & quote address
 *****************************************************************************
 */

$installer->addAttribute('customer_address', 'address_delivery_type', array(
    'label' => "Address Delivery Type",
    'input' => 'text',
    'type'  => 'varchar',
    'required' => 0,
    'user_defined' => 1,
	'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
));

$installer->addAttribute('customer_address', 'proposed_address', array(
    'label' => "Proposed Address",
    'input' => 'textarea',
    'type'  => 'text',
    'required' => 0,
    'user_defined' => 1,
	'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
));

// Location Hash is a hash of the relevant fields of the address location
// this is used to determine whether we need to revalidate the address
// only fields that describe the actual location (city, state, etc)
// are included (as opposed to DOB, email, phone, which are not locative)
$installer->addAttribute('customer_address', 'location_hash', array(
    'label' => "Location Hash",
    'input' => 'text',
    'type'  => 'varchar',
    'required' => 0,
    'user_defined' => 1,
	'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
	'note' => 'Used to determine whether the location described by the physical address has changed'
));

// also need DOB on quote address
$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address'), 'dob', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_DATETIME,
        'NULLABLE'  => true,
        'COMMENT'   => 'Date of Birth (for import to customer address)'
));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address'), 'address_delivery_type', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'LENGTH'    => 50,
        'NULLABLE'  => true,
        'COMMENT'   => 'Delivery Type (residential, commercial, etc)'
));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address'), 'proposed_address', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'NULLABLE'  => true,
        'COMMENT'   => 'Proposed alternative address'
));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address'), 'location_hash', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'LENGTH'    => 255,
        'NULLABLE'  => true,
        'COMMENT'   => 'Used to determine whether the location described by the physical address has changed'
));

$installer->endSetup();