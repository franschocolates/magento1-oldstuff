<?php
$installer = $this;
$installer->startSetup();

$sql = "
    update {$installer->getTable('sales/order_grid')} AS g
    join {$installer->getTable('sales/order')} AS o on o.entity_id = g.entity_id
    set g.admin_notes = o.admin_notes
";



$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_grid'), 'admin_notes', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'LENGTH'    => NULL,
        'COMMENT'   => 'Admin notes for order',
        'nullable'  => true,
    ));


//move the existing ship methods over.
$installer->run($sql);

$installer->endSetup();