<?php
$installer = $this;
$installer->startSetup();

//Change all cancelled orders to have correlating statuses/states
$sql = "UPDATE sales_flat_order SET sales_flat_order.status = 'canceled' WHERE sales_flat_order.state = 'canceled';";

$installer->run($sql);

$installer->endSetup();