<?php

$installer = $this;

$installer->startSetup();

$setup = $installer->getConnection();

/**
 *****************************************************************************
 * multiship shipping address edit 
 *****************************************************************************
 */

$formCode = 'multiship_shipping_address_edit';
$setup->insert($installer->getTable('eav/form_type'), array(
    'code'      => $formCode,
    'label'     => $formCode,
    'is_system' => 0,
    'theme'     => '',
    'store_id'  => 0
));
$formTypeId   = $setup->lastInsertId();
$entityTypeId = $installer->getEntityTypeId('customer_address');

$setup->insert($installer->getTable('eav/form_type_entity'), array(
    'type_id'        => $formTypeId,
    'entity_type_id' => $entityTypeId
));

$installer->run("
	INSERT INTO customer_form_attribute
	(form_code, attribute_id)
	SELECT 	'$formCode', address_form.attribute_id
	FROM	customer_form_attribute AS address_form
	INNER JOIN
			eav_attribute AS eav
	ON
			eav.attribute_id = address_form.attribute_id
	WHERE
			address_form.form_code='customer_address_edit'
	AND
			eav.attribute_code NOT IN ('email', 'dob');
");

# base it on customer address edit, but without certain fields included
/* @var $baseForm Mage_Customer_Model_Form */
/*$baseForm = Mage::getModel('customer/form')
				->setFormCode('customer_address_edit')
				->setEntityType('customer_address');

foreach ($baseForm->getAttributes() as )

*/
$installer->endSetup();