<?php
$installer = $this; 
$installer->startSetup(); 

// only need estimated arrival date on the quote
$installer->getConnection()
	->addColumn($installer->getTable('sales/quote_address'), 'estimated_arrival_date', array(
	'TYPE'      => Varien_Db_Ddl_Table::TYPE_DATE,
	'COMMENT'   => 'Date the user has selected for the shipment to arrive',
	'required' => 0
));

// need estimated arrival date and planned ship date on orders
$installer->getConnection()
	->addColumn($installer->getTable('sales/order'), 'estimated_arrival_date', array(
	'TYPE'      => Varien_Db_Ddl_Table::TYPE_DATE,
	'COMMENT'   => 'Date the user has selected for the shipment to arrive',
	'required' => 0
));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'planned_ship_date', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_DATE,
        'COMMENT'   => 'Date we are planning to ship',
        'required' => 0
    ));


$installer->endSetup();