<?php
$installer = $this;
$installer->startSetup();

// need gift message and special instructions per address and order
$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address'), 'gift_message', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'LENGTH'    => 1000,
        'COMMENT'   => 'Gift message for the address',
        'nullable'  => false,
    ));
$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address'), 'special_instructions', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'LENGTH'    => 2000,
        'COMMENT'   => 'Special instructions message for the address',
        'nullable'  => false,
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'gift_message', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'LENGTH'    => 1000,
        'COMMENT'   => 'Gift message for the address',
        'nullable'  => false,
    ));
$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'special_instructions', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'LENGTH'    => 2000,
        'COMMENT'   => 'Special instructions message for the address',
        'nullable'  => false,
    ));

// also need on sales order grid
$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_grid'), 'gift_message', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'LENGTH'    => 1000,
        'COMMENT'   => 'Gift message for the address',
        'nullable'  => false,
    ));
$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_grid'), 'special_instructions', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'LENGTH'    => 2000,
        'COMMENT'   => 'Special instructions message for the address',
        'nullable'  => false,
    ));


$installer->endSetup();