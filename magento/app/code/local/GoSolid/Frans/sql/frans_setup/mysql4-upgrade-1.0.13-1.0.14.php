<?php
$installer = $this; 
$installer->startSetup(); 

$installer->run("RENAME TABLE `gosolid_retail_store` TO `frans_retail_store`");


$installer->getConnection()
	->addColumn($installer->getTable('frans/retailStore'), 'use_on_frontend', array(
	'TYPE'      => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
	'COMMENT'   => 'Adding the boolean use_on_frontend column'
));

$installer->getConnection()
	->addColumn($installer->getTable('frans/retailStore'), 'store_image', array(
	'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
	'LENGTH'    => 255,
	'COMMENT'   => 'Adding image path varchar',
	'required' => 1
));

$installer->getConnection()
	->addColumn($installer->getTable('frans/retailStore'), 'maps_url', array(
	'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
	'LENGTH'    => 255,
	'COMMENT'   => 'Adding maps url field',
));

$installer->getConnection()
	->addColumn($installer->getTable('frans/retailStore'), 'subtitle', array(
	'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
	'LENGTH'    => 255,
	'COMMENT'   => 'Adding store subtitle',
));

$installer->endSetup();