<?php
/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;

$entityTypeId     = $installer->getEntityTypeId('customer_address');

$countryIdAttribute = Mage::getModel('eav/entity_attribute')->loadByCode($entityTypeId, 'country_id');
$regionAttribute = Mage::getModel('eav/entity_attribute')->loadByCode($entityTypeId, 'region');
$regionIdAttribute = Mage::getModel('eav/entity_attribute')->loadByCode($entityTypeId, 'region_id');
$postcodeAttribute = Mage::getModel('eav/entity_attribute')->loadByCode($entityTypeId, 'postcode');

// revert back to standard so we can handle the fact that backend region is unhappy if they aren't in the correct order.
$attributeOrders = array(
    'country_id' => 90
, 'region' => 100
, 'region_id' => 100
, 'postcode' => 110
);

$customerEavAttributeTable = $this->getTable('customer/eav_attribute');
$adapter = $this->getConnection();
foreach ($attributeOrders as $attributeCode => $sortOrder)
{
    $attribute = Mage::getModel('eav/entity_attribute')->loadByCode($entityTypeId, $attributeCode);
    $where = array($adapter->quoteIdentifier('attribute_id') . '=?' => $attribute->getId());
    $adapter->update($customerEavAttributeTable, array('sort_order' => $sortOrder), $where);
}

$installer->endSetup();