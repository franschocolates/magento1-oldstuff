<?php
$installer = $this;
$installer->startSetup();

// have to set pad length to match our expected length, and save that
$orderEntityType = Mage::getModel('eav/entity_type')->loadByCode('order');
$orderEntityType->setIncrementPadLength(6)
                ->save();

// set increment for order to start with 400000
$entityTypeStore = Mage::getModel('eav/entity_store')->loadByEntityStore($orderEntityType->getId(), /* store ID */ 1);

$entityTypeStore
    ->setIncrementLastId('400000')
    ->setIncrementPrefix('') // if we have a prefix it won't go above 400000 correctly
    ->save();
