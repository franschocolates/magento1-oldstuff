<?php
$installer = $this;
$installer->startSetup();

$installer->updateAttribute(
	'customer_address', 
	'telephone',
	'is_required',
	0
);

$installer->endSetup();