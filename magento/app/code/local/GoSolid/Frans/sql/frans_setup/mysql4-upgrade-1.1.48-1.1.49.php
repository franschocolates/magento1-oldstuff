<?php
/*
 * Remove Soczed Less compiler
 */
$installer = $this;
$installer->startSetup();

$reportTitle = 'Shipments - Billed';

$sql = <<<ENDOFSQL
SELECT
	COALESCE(sfp.increment_id, sfo.increment_id) AS shipmentgroupid
	, IF(INSTR(sfo.increment_id, '-') > 0, SUBSTRING(sfo.increment_id FROM 11), 1) AS shipmentnr
	, sfo.increment_id AS ship_id
	, sfo.ice_packs AS icepack_count
	, sfo.entity_id AS orderid
	, sfo.subtotal AS subtotal
	, sfo.discount_amount AS discount
	, sfo.tax_amount AS tax
	, sfo.shipping_amount AS shipping_cost
	, tn.tracking_numbers AS tracking
	, sfo.grand_total
	, COALESCE(sfp.grand_total, sfo.grand_total) AS order_grand_total
	, sfo.ship_date
	, DATE(sfo.created_at) AS order_date
	, DATE(tbo.invoice_created_date) AS order_status_date
	, 'billed' AS order_status
	, sfoa.prefix AS s_title
	, sfoa.firstname AS s_firstname
	, sfoa.lastname AS s_lastname
	, sfoa.street AS s_address
	, sfoa.city AS s_city
	, sfoa.region AS s_state
	, sfoa.postcode AS s_zipcode
	, sfoa.country_id AS s_country
	, sfob.prefix AS b_title
	, sfob.firstname AS b_firstname
	, sfob.lastname AS b_lastname
	, sfob.street AS b_address
	, sfob.city AS b_city
	, sfob.region AS b_state
	, sfob.postcode AS b_zipcode
	, sfob.country_id AS b_country
	, sfob.telephone AS phone
	, COALESCE(sfp.customer_email, sfo.customer_email) AS email
	, sn.note AS cutomer_notes
	, gm.message AS gift_message
	, sfo.admin_notes AS admi_note
	, sfo.shipping_description AS shipping_description
	, sfo.origination AS origination_level
	, sfo.accounting_label AS accounting_label
	, sfop.cc_type AS creditcard_type
	, sfo.weight AS total_weight
	, tp.products AS 'Products'
FROM
	sales_flat_order AS sfo
INNER JOIN
	tmp_BilledOrders AS tbo
ON
	tbo.order_id = sfo.entity_id
INNER JOIN
	sales_flat_order_address AS sfoa
ON
	sfoa.entity_id = sfo.shipping_address_id
LEFT OUTER JOIN
	sales_flat_order AS sfp
ON
	sfp.entity_id = tbo.parent_id
INNER JOIN
	sales_flat_order_address AS sfob
ON
	sfob.entity_id = COALESCE(sfp.billing_address_id, sfo.billing_address_id)
LEFT OUTER JOIN
	tmp_TrackingNumbers AS tn
ON
	tn.order_id = sfo.entity_id
LEFT OUTER JOIN
	tmp_Products AS tp
ON
	tp.order_id = sfo.entity_id
LEFT OUTER JOIN
	shipnote_note AS sn
ON
	sn.note_Id = sfo.ship_note_id
LEFT OUTER JOIN
	gift_message AS gm
ON
	gm.gift_message_id = sfo.gift_message_id
LEFT OUTER JOIN
	sales_flat_order_payment AS sfop
ON
	sfop.parent_id = IFNULL(sfp.entity_id, sfo.entity_id)

WHERE sfo.state != 'canceled' AND sfo.status != 'canceled';
ENDOFSQL;

if (Mage::getModel("sqlReports/sqlReport")->load($reportTitle ,'title')->getId()) {
    $report = Mage::getModel("sqlReports/sqlReport")->load($reportTitle, 'title')
        ->setSql($sql);
    $report->save();
}
$installer->endSetup();













