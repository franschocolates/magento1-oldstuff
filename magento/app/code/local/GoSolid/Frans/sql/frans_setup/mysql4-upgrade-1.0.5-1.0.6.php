<?php

# This script is called once before the version of frans_setup is changed

$installer = $this;

$installer->startSetup(); 

$this->addAttribute(4, 'call_to_order', array(
        'type' => 'int',
        'backend' => '',
        'frontend' => '',
		'source' => 'eav/entity_attribute_source_boolean',
		'input' => 'boolean',
        'label' => 'Call to order',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'visible' => true,
        'required' => true,
        'user_defined' => true,
        'default' => '0',
        'visible_on_front' => true,
        'unique' => false,
		'group' => 'General',
		'used_in_product_listing' => true
    ));

$installer->endSetup();