<?php

$installer = $this;
$installer->startSetup();

$permissionBlocks = $this->getTable('admin/permission_block');

$sql = <<<EOSQL
   INSERT IGNORE INTO {$permissionBlocks} (block_name,is_allowed) VALUES ('cms/block', '1');
   INSERT IGNORE INTO {$permissionBlocks} (block_name,is_allowed) VALUES ('frans/catalog_product_widget_giftservices','1');
   INSERT IGNORE INTO {$permissionBlocks} (block_name,is_allowed) VALUES ('frans/catalog_product_widget_insidethebox','1');
EOSQL;

$installer->run($sql);

$installer->endSetup();