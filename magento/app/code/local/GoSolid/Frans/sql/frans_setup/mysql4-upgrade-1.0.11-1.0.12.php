<?php
$installer = $this;
$installer->startSetup();

$installer->addAttribute('customer', 'subscriptions', array(
    'type'      =>  'varchar',
    'label'     =>  'Subscriptions',
    'input'     =>  'multiselect',
	'visible'   => true,
    'required'  => false,
    'source'    =>  'frans/customer_subscriptions'
));







$customerattrubute = Mage::getModel('customer/attribute')->loadByCode('customer', 'subscriptions');
$forms=array('adminhtml_customer');
$customerattrubute->setData('used_in_forms', $forms);
$customerattrubute->save();



$installer->getConnection()
    ->query( "UPDATE eav_attribute AS ea JOIN customer_eav_attribute AS eaf ON ea.attribute_id = eaf.attribute_id SET sort_order = 100 WHERE ea.entity_type_id = 1 AND ea.attribute_code = 'subscriptions';");
    


$installer->endSetup();