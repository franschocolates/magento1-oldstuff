<?php

$installer = $this;
$installer->startSetup();

$getRewriteModel  = Mage::getModel('core/url_rewrite')->load('businessgifting', 'request_path');
$getRewriteModel
    ->setIdPath('businessgifting-to-about')
    ->setRequestPath('businessgifting')
    ->setStoreId('1')
    ->setTargetPath('frans/businessgifting')
    ->setIsSystem('0')
    ->save();

$installer->endSetup();