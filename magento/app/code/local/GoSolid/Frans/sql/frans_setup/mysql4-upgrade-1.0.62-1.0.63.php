<?php
// add an index for the activity table on entity ID
$installer = $this;
$installer->startSetup();

$connection = $installer->getConnection()->addIndex(
    $installer->getTable('frans/activity'),
    $installer->getIdxName('frans/activity', array('entity_id')),
    array('entity_id')
);
