<?php
$installer = $this;
$installer->startSetup();


$installer->addAttribute("catalog_category", "include_in_submenu",  array(
    "type"     => "int",
    "backend"  => "",
    "frontend" => "",
    "label"    => "Include in Navigation Side Menu",
    "input"    => "select",
    "class"    => "",
    "source"   => "eav/entity_attribute_source_boolean",
    "global"   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    "visible" => true,
    "required" => true,
    "user_defined" => true,
    "default" => "0",
    "visible_on_front" => true,
    "unique" => false,
    "group" => "General Information",
	"position" => 5,
	"note" => ""
    ));
$installer->endSetup();