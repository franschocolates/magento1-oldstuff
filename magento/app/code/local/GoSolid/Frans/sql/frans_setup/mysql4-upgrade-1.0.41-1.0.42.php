<?php
$installer = $this;
$installer->startSetup();

// Add field to track when we sent the shipment email
$installer->getConnection()->addColumn($installer->getTable('sales/order'), 'shipment_email_sent_at', array(
    'type' => Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
    'nullable' => true,
    'default' => null,
    'comment' => 'Shipment Email Sent Date'
));

$installer->endSetup();