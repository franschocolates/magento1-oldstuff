<?php

/** @var $installer Mage_ImportExport_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

/**
 * Create table 'frans/importexport_importquoteaddressdata'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('frans/importexport_import_quoteaddressdata'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Id')
    ->addColumn('quote_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false,
    ), 'Id')
    ->addColumn('entity', Varien_Db_Ddl_Table::TYPE_TEXT, 50, array(
        'nullable'  => false,
    ), 'Entity')
    ->addColumn('behavior', Varien_Db_Ddl_Table::TYPE_TEXT, 10, array(
        'nullable'  => false,
        'default'   => Mage_ImportExport_Model_Import::BEHAVIOR_APPEND,
    ), 'Behavior')
    ->addColumn('data', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(
        'default'   => '',
    ), 'Data')
    ->setComment('Quote Address Import Data Table');
$installer->getConnection()->createTable($table);
