<?php

$installer = $this;

$installer->startSetup();

$connection = $installer->getConnection();

/**
 *****************************************************************************
 * new tables for blackout dates & messages
 *****************************************************************************
 */

$messageTable = $connection->newTable($installer->getTable('frans/futureShipMessage'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Id')
    ->addColumn('message_title', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        'nullable'  => true,
        ), 'Title to display when message is shown')
    ->addColumn('image', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable'  => true,
        ), 'Image to display when message is selected')
    ->addColumn('message_text', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable'  => false,
        ), 'Message text');

$connection->createTable($messageTable);

$dateTable = $connection->newTable($installer->getTable('frans/futureShipDate'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Id')
    ->addColumn('message_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false,
        ), 'Message text')
    ->addColumn('calendar_date', Varien_Db_Ddl_Table::TYPE_DATE, null, array(
        'nullable'  => false,
        ), 'Actual date')
    ->addColumn('date_type', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false,
        ), 'Indicates if a blackout date');

$connection->createTable($dateTable);

// Make the date reference the message
$connection->addConstraint(
	'FK_FUTURE_SHIP_DATE_FUTURE_SHIP_MESSAGE',
	$installer->getTable('frans/futureShipDate'),
	'message_id',
	$installer->getTable('frans/futureShipMessage'),
	'id',
	Varien_Db_Adapter_Interface::FK_ACTION_NO_ACTION,
	Varien_Db_Adapter_Interface::FK_ACTION_NO_ACTION
);

// also need tables for shipping blackout, zone, and duration 
$dateTable = $connection->newTable($installer->getTable('frans/shippingBlackoutDate'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Id')
    ->addColumn('blackout_date', Varien_Db_Ddl_Table::TYPE_DATE, null, array(
        'nullable'  => false,
        ), 'Shipping blackout date');
        
$connection->createTable($dateTable);

$zoneTable = $connection->newTable($installer->getTable('frans/shippingZone'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Id')
    ->addColumn('zone_name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable'  => false,
        ), 'Zone name')
    ->addColumn('postcode_list', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
        ), 'List of postcodes, comma separated')
    ->addColumn('position', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        ), 'Position of zone');
$connection->createTable($zoneTable);

$timeTable = $connection->newTable($installer->getTable('frans/shippingZoneMethodTime'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Id')
    ->addColumn('zone_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false,
        ), 'ID of Zone to tie back to')
    ->addColumn('shipping_method', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        'nullable'  => false,
        ), 'Shipping Method')
    ->addColumn('time_in_days', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false,
        ), 'Number of days it will take something to arrive');
$connection->createTable($timeTable);


$installer->endSetup();