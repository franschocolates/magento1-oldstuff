<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote'), 'progress', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'COMMENT'   => 'Progress steps for quote creation',
        'nullable'  => true,
    ));



$installer->endSetup();