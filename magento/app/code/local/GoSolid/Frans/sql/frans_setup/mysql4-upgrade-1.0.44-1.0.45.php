<?php
$installer = $this;
$installer->startSetup();

$connection = $installer->getConnection();

// need to change name of date column on both address and order
$connection->changeColumn(
    $installer->getTable('sales/quote_address')
        , 'estimated_arrival_date'
        , 'preferred_arrival_date'
        , array(
            'TYPE'      => Varien_Db_Ddl_Table::TYPE_DATE,
            'COMMENT'   => 'Date the user has selected for the shipment to arrive',
            'required' => 0
        )
    );

$connection->changeColumn(
    $installer->getTable('sales/order')
    , 'estimated_arrival_date'
    , 'preferred_arrival_date'
    , array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_DATE,
        'COMMENT'   => 'Date the user has selected for the shipment to arrive',
        'required' => 0
    )
);

$installer->endSetup();