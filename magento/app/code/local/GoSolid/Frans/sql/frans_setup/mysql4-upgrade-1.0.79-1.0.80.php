<?php
$installer = $this;
$installer->startSetup();

// technically this could be a data script
// but since it is affecting customer groups, seems like it's more DDL
// like an EAV attribute
$noDiscountBizGroup = Mage::getModel('customer/group')
                        ->load('Business Customer - No Discount', 'customer_group_code');

if (!$noDiscountBizGroup->getId())
{
    $taxClass = Mage::getModel('tax/class')->load('Retail Customer', 'class_name');
    $noDiscountBizGroup
        ->setCustomerGroupCode('Business Customer - No Discount')
        ->setTaxClassId($taxClass->getId())
        ->save();
}


// set it to be the default customer group for business customers
Mage::getModel('core/config')->saveConfig('frans/business_customers/default_customer_group_id', $noDiscountBizGroup->getId());

$installer->endSetup();