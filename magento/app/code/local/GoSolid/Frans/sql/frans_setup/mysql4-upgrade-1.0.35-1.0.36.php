<?php

$installer = $this;

$installer->startSetup();

$connection = $installer->getConnection();

$connection->changeColumn(
    $installer->getTable('frans/futureShipDate')
    , 'date_type'
    , 'availability_type'
    , array(
        'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'length'    => null,
        'comment'   => 'Availability Type',
        'required'  => true
    )
);

$installer->endSetup();