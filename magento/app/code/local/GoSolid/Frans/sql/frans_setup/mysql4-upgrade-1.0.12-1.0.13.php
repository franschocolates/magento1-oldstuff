<?php

# This script adds the columns needed to support our sales/order for multiship

$installer = $this; 
$installer->startSetup(); 

$installer->run("CREATE TABLE `frans_article` (
				  `id` INT(11) NOT NULL AUTO_INCREMENT,
				  `type_id` INT(11) DEFAULT NULL,
				  `title` VARCHAR(255) DEFAULT NULL,
				  `sub_title` VARCHAR(255) DEFAULT NULL,
				  `date` DATETIME DEFAULT NULL,
				  `description` TEXT,
				  `status` TINYINT(1) DEFAULT NULL,
				  `url` VARCHAR(334) DEFAULT NULL,
				  PRIMARY KEY (`id`)
				) ENGINE=INNODB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1");

$installer->endSetup();
