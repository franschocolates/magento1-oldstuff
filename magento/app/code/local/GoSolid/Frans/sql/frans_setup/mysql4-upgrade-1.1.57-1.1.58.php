<?php

$installer = $this;
$installer->startSetup();

$installer->run("ALTER TABLE sales_flat_quote ADD COLUMN shipping_score SMALLINT NULL;");

$installer->endSetup();

?>