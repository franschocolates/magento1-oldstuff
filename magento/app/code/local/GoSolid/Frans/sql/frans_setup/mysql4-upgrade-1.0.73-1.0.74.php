<?php
/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;

$entityTypeId     = $installer->getEntityTypeId('customer_address');

$countryIdAttribute = Mage::getModel('eav/entity_attribute')->loadByCode($entityTypeId, 'country_id');
$regionAttribute = Mage::getModel('eav/entity_attribute')->loadByCode($entityTypeId, 'region');
$regionIdAttribute = Mage::getModel('eav/entity_attribute')->loadByCode($entityTypeId, 'region_id');
$postcodeAttribute = Mage::getModel('eav/entity_attribute')->loadByCode($entityTypeId, 'postcode');

// make country ID into 110, with the others moving up 10
$attributeOrders = array(
    'country_id' => 110
    , 'region' => 90
    , 'region_id' => 90
    , 'postcode' => 100
);

$customerEavAttributeTable = $this->getTable('customer/eav_attribute');
$adapter = $this->getConnection();
foreach ($attributeOrders as $attributeCode => $sortOrder)
{
    $attribute = Mage::getModel('eav/entity_attribute')->loadByCode($entityTypeId, $attributeCode);
    $where = array($adapter->quoteIdentifier('attribute_id') . '=?' => $attribute->getId());
    $adapter->update($customerEavAttributeTable, array('sort_order' => $sortOrder), $where);
}

$installer->endSetup();