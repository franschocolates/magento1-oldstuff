<?php
$installer = $this; 
$installer->startSetup(); 

$installer->getConnection()
	->addColumn($installer->getTable('sales/quote'), 'admin_notes', array(
	'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
	'COMMENT'   => 'Admin notes for the quote',
	'required' => 0
));
$installer->getConnection()
	->addColumn($installer->getTable('sales/order'), 'admin_notes', array(
	'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
	'COMMENT'   => 'Admin notes for the order',
	'required' => 0
));

$installer->endSetup();