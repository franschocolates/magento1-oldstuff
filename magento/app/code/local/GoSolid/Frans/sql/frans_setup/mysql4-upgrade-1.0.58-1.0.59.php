<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote'), 'saved_quote', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'LENGTH'    => 1,
        'COMMENT'   => 'Indicates whether quote was saved',
        'nullable'  => true,
    ));



$installer->endSetup();