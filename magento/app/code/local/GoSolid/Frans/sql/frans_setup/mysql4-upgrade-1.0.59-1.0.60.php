<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()->changeColumn(
    $installer->getTable('sales/quote'),
    'saved_quote',
    'saved_quote',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
        'length'    => 1,
        'COMMENT'   => 'Indicates whether quote was saved',
        'nullable'  => false,
        'DEFAULT'   => '0'
    )
);


$installer->endSetup();