<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'captured_at', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_DATETIME,
        'COMMENT'   => 'Date/time the order was captured at',
    ));


$installer->endSetup();