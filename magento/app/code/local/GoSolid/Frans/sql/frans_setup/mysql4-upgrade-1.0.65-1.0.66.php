<?php
/**
 * Add a column to track whether an order was printed.
 */
$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'is_printed', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
        'LENGTH'    => NULL,
        'COMMENT'   => 'Indicates whether an order was printed',
        'DEFAULT'   => '0',
        'nullable'  => false,
    ));
