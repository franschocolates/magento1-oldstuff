<?php
$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();

$orderDeptLabel = 'Orders';
$donationsDeptLabel = 'Donations';

$sql = "
   DELETE FROM `{$this->getTable('helpdesk/department')}` WHERE `name` = 'Support';
   DELETE FROM `{$this->getTable('helpdesk/department')}` WHERE `name` = 'Sales';
   INSERT INTO `{$this->getTable('helpdesk/department')}` (name,sort_order,sender_email,is_notification_enabled,notification_email,is_active) VALUES ('Orders','10','general','1','','1');
   INSERT INTO `{$this->getTable('helpdesk/department')}` (name,sort_order,sender_email,is_notification_enabled,notification_email,is_active) VALUES ('Donations','20','general','1','','1');
";

$installer->run($sql);

$orderDept = Mage::getModel('helpdesk/department')->load($orderDeptLabel, 'name');
$donationsDept = Mage::getModel('helpdesk/department')->load($donationsDeptLabel, 'name');

$defaultOrderDeptId = $orderDept->getId().'_0';
$defaultDonationsDeptId = $donationsDept->getId().'_0';

$configModel = Mage::getModel('core/config');

$configModel->saveConfig('contacts/department/default', $defaultOrderDeptId);
$configModel->saveConfig('contacts/department/orderassistance', $defaultOrderDeptId);
$configModel->saveConfig('contacts/department/catalogrequest', $defaultOrderDeptId);
$configModel->saveConfig('contacts/department/pressinquiries', $defaultOrderDeptId);
$configModel->saveConfig('contacts/department/donations', $defaultDonationsDeptId);
$configModel->saveConfig('contacts/department/jobs', $defaultOrderDeptId);
$configModel->saveConfig('contacts/department/wholesale', $defaultOrderDeptId);
$configModel->saveConfig('contacts/department/international', $defaultOrderDeptId);
$configModel->saveConfig('contacts/department/other', $defaultOrderDeptId);

$installer->endSetup();
