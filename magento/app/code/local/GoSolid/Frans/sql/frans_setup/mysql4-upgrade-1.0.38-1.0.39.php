<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('frans/shippingZoneMethodTime'), 'delivery_days', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'LENGTH'    => 20,
        'COMMENT'   => 'Days of the week that the shipping zone can be delivered to with this method',
        'nullable'  => false,
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('frans/shippingZoneMethodTime'), 'delivery_type', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'SIZE'      => 1,
        'COMMENT'   => 'type of delivery (all/res/commercial) that this method represents',
        'nullable'  => false,
        'DEFAULT'    => '0'
    ));


$installer->endSetup();