<?php

$installer = $this;

$installer->startSetup();

$connection = $installer->getConnection();

/**
 *****************************************************************************
 * new table for shipping method display labels
 *****************************************************************************
 */

$shippingMethodTable = $connection->newTable($installer->getTable('frans/shippingMethod'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Id')
    ->addColumn('shipping_method', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        'nullable'  => false,
    ), 'Shipping Method Name')
    ->addColumn('label', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        'nullable'  => false,
    ), 'Display label for method')
    ->addColumn('color', Varien_Db_Ddl_Table::TYPE_VARCHAR, 10, array(
        'nullable'  => false,
    ), 'Display color for calendar');

$connection->createTable($shippingMethodTable);

$installer->endSetup();