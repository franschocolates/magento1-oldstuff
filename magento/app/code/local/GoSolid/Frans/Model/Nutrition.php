<?php
/**
 * Created by goSolid.
 * Date: 3/2/15
 * Time: 2:37 PM
 */ 
class GoSolid_Frans_Model_Nutrition extends Mage_Core_Model_Abstract
{
    protected $_mainTable = 'frans_catalog_product_nutrition';

    /**
     * Errors in import process
     *
     * @var array
     */
    protected $_importErrors        = array();


    protected function _construct()
    {
        $this->_init('frans/nutrition', 'pk');
    }

    public function uploadAndImport(Varien_Object $object)
    {

        if (empty($_FILES['groups']['tmp_name']['nutrition']['fields']['nutrition_import']['value'])) {
            return $this;
        }

        $csvFile = $_FILES['groups']['tmp_name']['nutrition']['fields']['nutrition_import']['value'];

        $this->_importErrors        = array();

        $io     = new Varien_Io_File();
        $info   = pathinfo($csvFile);
        $io->open(array('path' => $info['dirname']));
        $io->streamOpen($info['basename'], 'r');

        // check and skip headers
        $headers = $io->streamReadCsv();
        if ($headers === false || count($headers) < 5) {
            $io->streamClose();
            Mage::throwException(Mage::helper('frans')->__('Invalid Nutrition Information File Format'));
        }

        $adapter = $this->_getWriteAdapter();
        $adapter->beginTransaction();

        try {

            $rowNumber  = 1;
            $importData = array();


//             delete old data by website and condition name

            $condition = array(
                '1 = 1'
            );
            $adapter->delete($this->getMainTable(), $condition);

            while (false !== ($csvLine = $io->streamReadCsv())) {
                $rowNumber ++;

                if (empty($csvLine)) {
                    continue;
                }
                $row = $this->_getImportRow($csvLine, $rowNumber);

                if ($row !== false) {
                    $importData[] = $row;
                }

                if (count($importData) == 5000) {
                    $this->_saveImportData($importData);
                    $importData = array();
                }
            }
            $this->_saveImportData($importData);

            $io->streamClose();
        } catch (Mage_Core_Exception $e) {
            $adapter->rollback();
            $io->streamClose();
            Mage::throwException($e->getMessage());
        } catch (Exception $e) {
            $adapter->rollback();
            $io->streamClose();
            Mage::logException($e);
            Mage::throwException(Mage::helper('frans')->__('An error occurred while importing nutrition information.'));
        }

        $adapter->commit();

        if ($this->_importErrors) {
            $error = Mage::helper('frans')->__('File has not been imported. See the following list of errors:</br> %s', implode(" \n", $this->_importErrors));
            Mage::throwException($error);
        }

        return $this;
    }

    protected function _getImportRow($row, $rowNumber = 0)
    {
        // validate row
        if (count($row) < 38) {
            $this->_importErrors[] = Mage::helper('shipping')->__('Invalid Nutrition Information format in the Row #%s', $rowNumber);
            return false;
        }
        // strip whitespace from the beginning and end of each row
        foreach ($row as $k => $v) {
            $row[$k] = trim($v);
        }

        $id = $row[0];
        $code = $row[1];
        $accpac_sku = $row[2];
        $name = $row[3];
        $ingredients = $row[4];
        $servingSize = $row[5];
        $weight = $row[6];
        $netWeight = $row[7];
        $calories = $row[8];
        $caloriesFromFat = $row[9];
        $totalFat = $row[10];
        $totalFatDV = $row[11];
        $saturatedFat = $row[12];
        $saturatedFatDV = $row[13];
        $transFat = $row[14];
        $transFatDV = $row[15];
        $cholesterol = $row[16];
        $cholesterolDV = $row[17];
        $sodium = $row[18];
        $sodiumDV = $row[19];
        $totalCarbs = $row[20];
        $totalCarbsDV = $row[21];
        $dietaryFiber = $row[22];
        $dietaryFiberDV = $row[23];
        $sugars = $row[24];
        $protein = $row[25];
        $vitaminA = $row[26];
        $vitaminC = $row[27];
        $calcium = $row[28];
        $iron = $row[29];
        $isGlutenFree = $row[30];
        $isNonGmo = $row[31];
        $isVegan = $row[32];
        $containsAlcohol = $row[33];
        $containsNuts = $row[34];
        $containsMilkEggs = $row[35];
        $isOrganic = $row[36];
        $containsSoy = $row[37];
        $isFairTrade = $row[38];
        $backgroundImage = $row[39];

        // validate id
        if ($id == '0' || $id == null) {
            $this->_importErrors[] = Mage::helper('frans')->__('No ID in Row #' . $rowNumber, $id, $rowNumber);
            return false;
        }

        // validate code
        if ($code == '0' || $code == null) {
            $this->_importErrors[] = Mage::helper('frans')->__('No Code in Row #' . $rowNumber, $code, $rowNumber);
            return false;
        }

        // validate accpac_sku
        if ($accpac_sku == '0' || $accpac_sku == null) {
            $this->_importErrors[] = Mage::helper('frans')->__('No AccPac Sku in Row #' . $rowNumber, $accpac_sku, $rowNumber);
            return false;
        }

        // validate name
        if ($name == '0' || $name == null) {
            $this->_importErrors[] = Mage::helper('frans')->__('No Name in Row #' . $rowNumber, $name, $rowNumber);
            return false;
        }

        // validate is_gluten_free
        if ($isGlutenFree == null) {
            $this->_importErrors[] = Mage::helper('frans')->__('No value for Is Gluten Free in Row #' . $rowNumber, $isGlutenFree, $rowNumber);
            return false;
        }

        // validate is_non_gmo
        if ($isNonGmo == null) {
            $this->_importErrors[] = Mage::helper('frans')->__('No value for Is Non GMO in Row #' . $rowNumber, $isNonGmo, $rowNumber);
            return false;
        }

        // validate is_vegan
        if ($isVegan == null) {
            $this->_importErrors[] = Mage::helper('frans')->__('No value for Is Vegan in Row #' . $rowNumber, $isVegan, $rowNumber);
            return false;
        }

        // validate is_vegan
        if ($isVegan == null) {
            $this->_importErrors[] = Mage::helper('frans')->__('No value for Is Vegan in Row #' . $rowNumber, $isVegan, $rowNumber);
            return false;
        }

        // validate contains_alcohol
        if ($containsAlcohol == null) {
            $this->_importErrors[] = Mage::helper('frans')->__('No value for Contains Alcohol in Row #' . $rowNumber, $containsAlcohol, $rowNumber);
            return false;
        }

        // validate contains_nuts
        if ($containsNuts == null) {
            $this->_importErrors[] = Mage::helper('frans')->__('No value for Contains Nuts in Row #' . $rowNumber, $containsNuts, $rowNumber);
            return false;
        }

        // validate contains_milk_eggs
        if ($containsMilkEggs == null) {
            $this->_importErrors[] = Mage::helper('frans')->__('No value for Contains Milk/Eggs in Row #' . $rowNumber, $containsMilkEggs, $rowNumber);
            return false;
        }

        // validate is_organic
        if ($isOrganic == null) {
            $this->_importErrors[] = Mage::helper('frans')->__('No value for Is Organic in Row #' . $rowNumber, $isOrganic, $rowNumber);
            return false;
        }

        // validate contains_soy
        if ($containsSoy == null) {
            $this->_importErrors[] = Mage::helper('frans')->__('No value for Contains Soy in Row #' . $rowNumber, $containsSoy, $rowNumber);
            return false;
        }

        // validate is_fair_trade
        if ($isFairTrade == null) {
            $this->_importErrors[] = Mage::helper('frans')->__('No value for Is Fair Trade in Row #' . $rowNumber, $isFairTrade, $rowNumber);
            return false;
        }


        return array(
            htmlspecialchars_decode($id),                    // id
            htmlspecialchars_decode($code),                  // code
            htmlspecialchars_decode($accpac_sku),            // accpac_sku
            htmlspecialchars_decode($name),                  // name
            htmlspecialchars_decode($ingredients),           // ingredients
            htmlspecialchars_decode($servingSize),           // serving_size
            htmlspecialchars_decode($weight),                // weight
            htmlspecialchars_decode($netWeight),             // net_weight
            htmlspecialchars_decode($calories),              // calories
            htmlspecialchars_decode($caloriesFromFat),       // calories_from_fat
            htmlspecialchars_decode($totalFat),              // total_fat
            htmlspecialchars_decode($totalFatDV),            // total_fat_dv
            htmlspecialchars_decode($saturatedFat),          // saturated_fat
            htmlspecialchars_decode($saturatedFatDV),        // saturated_fat_dv
            htmlspecialchars_decode($transFat),              // trans_fat
            htmlspecialchars_decode($transFatDV),            // trans_fat_dv
            htmlspecialchars_decode($cholesterol),           // cholesterol
            htmlspecialchars_decode($cholesterolDV),         // cholesterol_dv
            htmlspecialchars_decode($sodium),                // sodium
            htmlspecialchars_decode($sodiumDV),              // sodium_dv
            htmlspecialchars_decode($totalCarbs),            // total_carbohydrates
            htmlspecialchars_decode($totalCarbsDV),          // total_carbohydrates_dv
            htmlspecialchars_decode($dietaryFiber),          // dietary_fiber
            htmlspecialchars_decode($dietaryFiberDV),        // dietary_fiber_dv
            htmlspecialchars_decode($sugars),                // sugars
            htmlspecialchars_decode($protein),               // protein
            htmlspecialchars_decode($vitaminA),              // vitamin_a
            htmlspecialchars_decode($vitaminC),              // vitamin_c
            htmlspecialchars_decode($calcium),               // calcium
            htmlspecialchars_decode($iron),                  // iron
            htmlspecialchars_decode($isGlutenFree),          // is_gluten_free
            htmlspecialchars_decode($isNonGmo),              // is_non_gmo
            htmlspecialchars_decode($isVegan),               // is_vegan
            htmlspecialchars_decode($containsAlcohol),       // contains_alcohol
            htmlspecialchars_decode($containsNuts),          // contains_nuts
            htmlspecialchars_decode($containsMilkEggs),      // contains_milk_eggs
            htmlspecialchars_decode($isOrganic),             // is_organic
            htmlspecialchars_decode($containsSoy),           // contains_soy
            htmlspecialchars_decode($isFairTrade),           // is_fair_trade
            htmlspecialchars_decode($backgroundImage)        // background_image
        );


    }

    protected function _saveImportData(array $data)
    {
        if (!empty($data)) {
            $columns =
                array(
                    'id',
                    'code',
                    'accpac_sku',
                    'name',
                    'ingredients',
                    'serving_size',
                    'weight',
                    'net_weight',
                    'calories',
                    'calories_from_fat',
                    'total_fat',
                    'total_fat_dv',
                    'saturated_fat',
                    'saturated_fat_dv',
                    'trans_fat',
                    'trans_fat_dv',
                    'cholesterol',
                    'cholesterol_dv',
                    'sodium',
                    'sodium_dv',
                    'total_carbohydrate',
                    'total_carbohydrate_dv',
                    'dietary_fiber',
                    'dietary_fiber_dv',
                    'sugars',
                    'protein',
                    'vitamin_a',
                    'vitamin_c',
                    'calcium',
                    'iron',
                    'is_gluten_free',
                    'is_non_gmo',
                    'is_vegan',
                    'contains_alcohol',
                    'contains_nuts',
                    'contains_milk_eggs',
                    'is_organic',
                    'contains_soy',
                    'is_fair_trade',
                    'background_image'
                );
            $this->_getWriteAdapter()->insertArray($this->getMainTable(), $columns, $data);
            $this->_importedRows += count($data);
        }

        return $this;
    }

    /**
     * Parse and validate positive decimal value
     * Return false if value is not decimal or is not positive
     *
     * @param string $value
     * @return bool|float
     */
    protected function _parseDecimalValue($value)
    {
        if (!is_numeric($value)) {
            return false;
        }
        $value = (float)sprintf('%.4F', $value);
        if ($value < 0.0000) {
            return false;
        }
        return $value;
    }

    /**
     * Parse and validate positive decimal value
     *
     * @see self::_parseDecimalValue()
     * @deprecated since 1.4.1.0
     * @param string $value
     * @return bool|float
     */
    protected function _isPositiveDecimalNumber($value)
    {
        return $this->_parseDecimalValue($value);
    }

    protected function _getWriteAdapter()
    {
        return Mage::getSingleton("core/resource")->getConnection("core_write");
    }

    public function getMainTable()
    {
        return $this->_mainTable;
    }
}