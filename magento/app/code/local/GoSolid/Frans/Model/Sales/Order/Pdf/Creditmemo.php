<?php
/**
 * This class rewritten to allow us to insert the credit memo date
 *
 */ 
class GoSolid_Frans_Model_Sales_Order_Pdf_Creditmemo
    extends Mage_Sales_Model_Order_Pdf_Creditmemo
{
    /**
     * Return PDF document
     *  Overridden to allow us to insert the credit memo date.
     *  Minimal changes required, but no clean way to make this change, so have to override the whole method
     *  goSolid changes marked by // START / END GS
     *
     * @param  array $creditmemos
     * @return Zend_Pdf
     */
    public function getPdf($creditmemos = array())
    {
        $this->_beforeGetPdf();
        $this->_initRenderer('creditmemo');

        $pdf = new Zend_Pdf();
        $this->_setPdf($pdf);
        $style = new Zend_Pdf_Style();
        $this->_setFontBold($style, 10);

        foreach ($creditmemos as $creditmemo) {
            if ($creditmemo->getStoreId()) {
                Mage::app()->getLocale()->emulate($creditmemo->getStoreId());
                Mage::app()->setCurrentStore($creditmemo->getStoreId());
            }
            $page  = $this->newPage();
            $order = $creditmemo->getOrder();
            /* Add image */
            $this->insertLogo($page, $creditmemo->getStore());
            /* Add address */
            $this->insertAddress($page, $creditmemo->getStore());
            /* Add head */
            $this->insertOrder(
                $page,
                $order,
                Mage::getStoreConfigFlag(self::XML_PATH_SALES_PDF_CREDITMEMO_PUT_ORDER_ID, $order->getStoreId())
            );
            /* Add document text and number */
            // START GS - just adding the Created on Info
            $this->insertDocumentNumber(
                $page,
                Mage::helper('sales')->__('Credit Memo # %s (Created on %s)'
                    , $creditmemo->getIncrementId()
                    , Mage::helper('core')->formatDate($creditmemo->getCreatedAtStoreDate(), 'medium', false)
                )
            );
            // END GS

            /* Add table head */
            $this->_drawHeader($page);
            /* Add body */
            foreach ($creditmemo->getAllItems() as $item){
                if ($item->getOrderItem()->getParentItem()) {
                    continue;
                }
                /* Draw item */
                $this->_drawItem($item, $page, $order);
                $page = end($pdf->pages);
            }
            /* Add totals */
            $this->insertTotals($page, $creditmemo);
        }
        $this->_afterGetPdf();
        if ($creditmemo->getStoreId()) {
            Mage::app()->getLocale()->revert();
        }
        return $pdf;
    }

}