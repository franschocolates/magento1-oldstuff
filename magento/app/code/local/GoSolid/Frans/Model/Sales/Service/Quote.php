<?php
/**
 * Rewritten to allow submission of multiship quotes
 */ 
class GoSolid_Frans_Model_Sales_Service_Quote extends Mage_Sales_Model_Service_Quote
{
    /**
     * Submit the quote. Quote submit process will create the order based on quote data
     * Note that we just have special logic for multishipping
     *
     * @return Mage_Sales_Model_Order
     */
    public function submitOrder()
    {
        if ($this->_quote->getIsMultiShipping())
        {
            return $this->submitMultishipOrder();
        }

        return parent::submitOrder();
    }

    public function submitMultishipOrder()
    {
        $this->_deleteNominalItems();
        $this->_validateMultiship();
        $quote = $this->_quote;

        $transaction = Mage::getModel('core/resource_transaction');
        if ($quote->getCustomerId()) {
            $transaction->addObject($quote->getCustomer());
        }
        $transaction->addObject($quote);

        $quote->reserveOrderId();

        $order = $this->_prepareParentOrder();
        $order->setSuppressActivity(true); //otherwise it will start logging updates when children are saved

        if ($quote->getBillingAddress()->getCustomerAddress()) {
            $order->getBillingAddress()->setCustomerAddress($quote->getBillingAddress()->getCustomerAddress());
        }

        $order->setQuote($quote);

        $transaction->addObject($order);
        $transaction->addCommitCallback(array($order, 'place'));

        $childOrderNum = 1;
        foreach ($this->getQuote()->getAllShippingAddresses() as $address) {
            $childOrder = $this->_prepareChildOrder($address, $order, $childOrderNum);

            $childOrders[] = $childOrder;
            Mage::dispatchEvent(
                'checkout_type_multishipping_create_orders_single',
                array('order'=>$childOrder, 'address'=>$address)
            );

            $transaction->addObject($childOrder);
            // we do not call place on the child
            // because it is already saved and covered when the parent is placed.
            $childOrderNum++;
        }

        // update parent order totals
        $order->setChildOrders($childOrders);

        $transaction->addCommitCallback(array($order, 'save'));

        /**
         * We can use configuration data for declare new order status
         */
        Mage::dispatchEvent('checkout_type_multiship_save_order', array('order'=>$order, 'quote'=>$quote));
        Mage::dispatchEvent('sales_model_service_quote_submit_before', array('order'=>$order, 'quote'=>$quote));
        try {
            $order->update();

            $transaction->save();
            $this->_inactivateQuote();

            Mage::dispatchEvent('sales_model_service_quote_submit_success', array('order'=>$order, 'quote'=>$quote));
        } catch (Exception $e) {

            if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
                // reset customer ID's on exception, because customer not saved
                $quote->getCustomer()->setId(null);
            }

            //reset order ID's on exception, because order not saved
            $order->setId(null);
            /** @var $item Mage_Sales_Model_Order_Item */
            foreach ($order->getItemsCollection() as $item) {
                $item->setOrderId(null);
                $item->setItemId(null);
            }

            Mage::dispatchEvent('sales_model_service_quote_submit_failure', array('order'=>$order, 'quote'=>$quote));
            throw $e;
        }
        Mage::dispatchEvent('sales_model_service_quote_submit_after', array('order'=>$order, 'quote'=>$quote));
        $this->_order = $order;
        return $order;
    }

    /**
     * Validate quote data before converting to order
     *
     * @return Mage_Sales_Model_Service_Quote
     */
    protected function _validateMultiship()
    {
        foreach ($this->getQuote()->getAllShippingAddresses() as $shippingAddress)
        {
            $addressValidation = $shippingAddress->validate();
            if ($addressValidation !== true) {
                Mage::throwException(
                    Mage::helper('sales')->__('Please check shipping address information for address %d: %s', $shippingAddress->getId(), implode(' ', $addressValidation))
                );
            }
            $method= $shippingAddress->getShippingMethod();
            $rate  = $shippingAddress->getShippingRateByCode($method);
            if (!$this->getQuote()->isVirtual() && (!$method || !$rate)) {
                Mage::throwException(Mage::helper('sales')->__('Please specify a shipping method for address %d.', $shippingAddress->getId()));
            }
        }

        $addressValidation = $this->getQuote()->getBillingAddress()->validate();
        if ($addressValidation !== true) {
            Mage::throwException(
                Mage::helper('sales')->__('Please check billing address information. %s', implode(' ', $addressValidation))
            );
        }

        if (!($this->getQuote()->getPayment()->getMethod())) {
            Mage::throwException(Mage::helper('sales')->__('Please select a valid payment method.'));
        }

        return $this;
    }

    /**
     * @return GoSolid_Frans_Model_Sales_Order
     */
    protected function _prepareParentOrder()
    {
        $quote = $this->getQuote();
        $quote->unsReservedOrderId();
        $quote->reserveOrderId();
        $reservedOrderId = $quote->getReservedOrderId() . "-0";
        $quote->setReservedOrderId($reservedOrderId);
        $quote->collectTotals();

        $convertQuote = Mage::getSingleton('sales/convert_quote');
        $order = $convertQuote->toOrder($quote);
        $order->setQuote($quote);
        $order->setBillingAddress(
            $convertQuote->addressToOrderAddress($quote->getBillingAddress())
        );

        $order->setIsVirtual(1);
        $order->setIsMultishipParent(true);

        $order->setPayment($convertQuote->paymentToOrderPayment($quote->getPayment()));
        return $order;
    }

    /**
     * Prepare order based on quote address
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  Mage_Sales_Model_Order
     * @throws  Mage_Checkout_Exception
     */
    protected function _prepareChildOrder(Mage_Sales_Model_Quote_Address $address,
                                          Mage_Sales_Model_Order $parentOrder, $childOrderNum)
    {
        $quote = $this->getQuote();

        $incrementId = str_replace('-0', '-' . $childOrderNum, $parentOrder->getIncrementId());

        $quote->setReservedOrderId($incrementId);
        $quote->collectTotals();


        $convertQuote = Mage::getSingleton('sales/convert_quote');
        $order = $convertQuote->addressToOrder($address);
        $order->setQuote($quote);
        $order->setIsMultishipParent(false);


        $order->setPayment($this->_getChildOrderPayment($quote));
        // child orders are processing by default.
        $order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true);

        if ($address->getAddressType() == 'billing') {
            $order->setIsVirtual(1);
        } else {
            $order->setShippingAddress($convertQuote->addressToOrderAddress($address));
        }

        foreach ($address->getAllItems() as $item) {
            $_quoteItem = $item->getQuoteItem();
            if (!$_quoteItem) {
                throw new Mage_Checkout_Exception(Mage::helper('checkout')->__('Item not found or already ordered'));
            }
            $item->setProductType($_quoteItem->getProductType())
                ->setProductOptions(
                    $_quoteItem->getProduct()->getTypeInstance(true)->getOrderOptions($_quoteItem->getProduct())
                )
                ->setNotes($_quoteItem->getNotes())
                ->setCategory($_quoteItem->getCategory())
                ->setOrderType($_quoteItem->getOrderType());
            $orderItem = $convertQuote->itemToOrderItem($item);
            if ($item->getParentItem()) {
                $orderItem->setParentItem($order->getItemByQuoteItemId($item->getParentItem()->getId()));
            }
            $order->addItem($orderItem);
        }

        $order->setMultishipParentOrder($parentOrder);

        return $order;
    }

    protected function _getChildOrderPayment($quote)
    {
        $payment = Mage::getModel('sales/quote_payment');

        // have to reload the quote for the payment to work properly
        $payment->setStoreId($this->getQuote()->getStoreId());
        $payment = $payment->importMultishipData();
        $payment->setQuoteId($quote->getId());
        $payment->save();

        $orderPayment = Mage::getModel('sales/order_payment')
            ->setStoreId($payment->getStoreId())
            ->setCustomerPaymentId($payment->getCustomerPaymentId());
        $orderPayment->setMethod('multiship');
        return $orderPayment;
    }

}