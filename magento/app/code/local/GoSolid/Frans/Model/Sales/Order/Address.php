<?php
/*
 * Rewrite of Order Address, to allow us to get quotes on shipping
 */
class GoSolid_Frans_Model_Sales_Order_Address extends Mage_Sales_Model_Order_Address
{
	/**
	 * 
	 * Quote rates is a collection of Sales_Model_Quote_Address_Rate, 
	 * Since that was easy to re-use, even though we aren't actually a quote
	 * @var array Sales_Model_Quote_Address_Rate
	 */
	private $_quoteAddressRates;
	
    /**
     * Request shipping rates for entire address
     * 	Note that this based on the code from Mage_Sales_Model_Quote_Address
     *  In the future, if we need to support getting a quote for just one item,
     *  Then we can re-reference that code, since it also exists there
     *
     * @return bool
     */
    public function requestShippingRates()
    {
        /** @var $request Mage_Shipping_Model_Rate_Request */
        $request = Mage::getModel('shipping/rate_request');
        # NOTE: this should probably be a quote item list
        $request->setAllItems($this->getAllItems());
        $request->setDestCountryId($this->getCountryId());
        $request->setDestRegionId($this->getRegionId());
        $request->setDestRegionCode($this->getRegionCode());
        /**
         * need to call getStreet with -1
         * to get data in string instead of array
         */
        $request->setDestStreet($this->getStreet(-1));
        $request->setDestCity($this->getCity());
        $request->setDestPostcode($this->getPostcode());
        $request->setPackageValue($this->getOrder()->getBaseSubtotal());
        $packageValueWithDiscount = $this->getOrder()->getBaseSubtotalWithDiscount();
        $request->setPackageValueWithDiscount($packageValueWithDiscount);
        $request->setPackageWeight($this->getOrder()->getWeight());
        $request->setPackageQty($this->getItemQty());

        /**
         * Need for shipping methods that use insurance based on price of physical products
         */
        $packagePhysicalValue = $this->getOrder()->getBaseSubtotal() - $this->getOrder()->getBaseVirtualAmount();
        $request->setPackagePhysicalValue($packagePhysicalValue);

        // not sure what this should really be.
        $request->setFreeMethodWeight(0);

        /**
         * Store and website identifiers need specify from quote
         */
        /*$request->setStoreId(Mage::app()->getStore()->getId());
        $request->setWebsiteId(Mage::app()->getStore()->getWebsiteId());*/

        $request->setStoreId($this->getOrder()->getStore()->getId());
        $request->setWebsiteId($this->getOrder()->getStore()->getWebsiteId());
        //$request->setFreeShipping($this->getFreeShipping());;
        /**
         * Currencies need to convert in free shipping
         */
        $request->setBaseCurrency($this->getOrder()->getStore()->getBaseCurrency());
        $request->setPackageCurrency($this->getOrder()->getStore()->getCurrentCurrency());
        $request->setLimitCarrier($this->getLimitCarrier());

        $request->setBaseSubtotalInclTax($this->getOrder()->getBaseSubtotalInclTax());

        $result = Mage::getModel('shipping/shipping')->collectRates($request)->getResult();

        if ($result) 
        {
			# quotes would import& save shipping rates
			# we can't save, but we can import and persist for the request
			$quoteAddressRates = array();
			
			foreach ($result->getAllRates() as $rate)
			{
                $quoteAddressRate = Mage::getModel('sales/quote_address_rate')
                    ->importShippingRate($rate);
                $quoteAddressRates[] = $quoteAddressRate;
			}

			$this->_quoteAddressRates = $quoteAddressRates;
			
			return count($this->_quoteAddressRates) > 0;
        }
        return false;
    }

    public function getShippingMethod()
    {
    	return $this->getOrder()->getShippingMethod();
    }

    public function getAddressScore()
    {
        return Mage::getModel('frans/shipping_addressValidator')->getStoredScore($this);
    }

    public function getAddressScoreDescription()
    {
        return Mage::getModel('frans/shipping_addressValidator')->getScoreDescription($this);
    }

    public function getValidationMessages()
    {
        return Mage::getModel('frans/shipping_addressValidator')->getChangeMessages($this);
    }

    public function getEffectiveAddressHtmlFormatted()
    {
        try{

            $proposed_address = json_decode($this->getProposedAddress());
            $street_lines = is_array($proposed_address->StreetLines) ? implode("<br />", $proposed_address->StreetLines) : $proposed_address->StreetLines;
            $city_state_zip = $proposed_address->City . ", " . $proposed_address->StateOrProvinceCode . " " . $proposed_address->PostalCode;
            return $street_lines . "<br />" . $city_state_zip;
        }
        catch (Exception $e)
        {
            return '';
        }
    }

    /**
     * Retrieve all grouped shipping rates
     * (Adapted from Quote Address)
     *
     * @return array
     */
    public function getGroupedAllShippingRates()
    {
        $rates = array();
        $handledMethods = array();
        if($this->_quoteAddressRates) {
            foreach ($this->_quoteAddressRates as $rate) {
                if (!$rate->isDeleted() && $rate->getCarrierInstance()) {
                    if (!isset($rates[$rate->getCarrier()])) {
                        $rates[$rate->getCarrier()] = array();
                    }

                    // may have duplicates, filter them out.
                    // dupes happen because we have the same method for multiple days
                    if (!in_array($rate->getCode(), $handledMethods)) {
                        $rates[$rate->getCarrier()][] = $rate;
                        $rates[$rate->getCarrier()][0]->carrier_sort_order = $rate->getCarrierInstance()->getSortOrder();
                        $handledMethods[] = $rate->getCode();
                    }
                }
            }
        }
        uasort($rates, array($this, '_sortRates'));
        return $rates;
    }

    /**
     * Retrieve item quantity 
     *
     * @param int $itemId
     * @return float|int
     */
    public function getItemQty()
    {
        if ($this->hasData('item_qty')) {
            return $this->getData('item_qty');
        }

        $qty = 0;
		foreach ($this->getOrder()->getAllItems() as $item) 
		{
                // not sure if this should adjust for things like canceled, etc.
                // tough to say
                $qty += $item->getQtyOrdered() - max(0, $item->getQtyCanceled());
        }
        return $qty;
    }

    /**
     * Sort rates recursive callback
     *
     * @param array $a
     * @param array $b
     * @return int
     */
    protected function _sortRates($a, $b)
    {
        if ((int)$a[0]->carrier_sort_order < (int)$b[0]->carrier_sort_order) {
            return -1;
        }
        elseif ((int)$a[0]->carrier_sort_order > (int)$b[0]->carrier_sort_order) {
            return 1;
        }
        else {
            return 0;
        }
    }

    /**
     * Returns the quote address for an order address.
     * This only works if the observer is hooked up that sets the quote address ID during conversion
     *
     * @return Mage_Sales_Model_Quote_Address
     */
    public function getQuoteAddress()
    {
        if ($this->getQuoteAddressId())
        {
            if ($quoteAddress = Mage::getModel('sales/quote_address')->load($this->getQuoteAddressId()))
            {
                return $quoteAddress;
            }
        }

        return false;
    }

    /**
     * Helper method to convert this order address into a customer address
     *
     * @param Mage_Customer_Model_Customer $customer
     * @param bool $defaultBilling
     * @return Mage_Customer_Model_Address
     */
    public function convertToCustomerAddress($customer, $defaultBilling = false)
    {
        // can't do this if we already have an ID
        if ($this->getCustomerAddressId())
        {
            return false;
        }

        if ($quoteAddress = $this->getQuoteAddress())
        {
            $customerAddress = $quoteAddress->exportCustomerAddress();

            // set the necessary properties on everything
            $customer->addAddress($customerAddress);

            if ($defaultBilling)
            {
                $customerAddress->setIsDefaultBilling(true);
            }

            // add it to the order address
            // they do not have the property explicitly, but in _beforeSave it will
            // set the customer address ID if the customer address is present.
            $this->setCustomerAddress($customerAddress);

            return $customerAddress;
        }

        return false;
    }

    /**
     *
     * Returns whether the address is a known residential address
     */
    public function getIsResidential()
    {
        // only residential if the property is populated and
        // if it matches our residential value. Note we do a case insensitive compare
        return $this->hasData('address_delivery_type') &&
        (strcasecmp($this->getAddressDeliveryType(), GoSolid_Frans_Model_Sales_Quote_Address::RESIDENTIAL_ADDRESS_TYPE) == 0);
    }


    /**
     * Makes sure that if we add a customer address, it sets the customer ID (if present)
     *
     * @return $this|Mage_Sales_Model_Order_Address
     */
    protected function _beforeSave()
    {
        // we call setRegion before our parent beforeSave so that we have a region when we are edited
        // otherwise activity logging thinks our region changed when maybe it didn't
        // only do it when we have an ID (ie, we aren't new)
        if ($this->getId())
        {
            // this usually gets called in Customer_Address_Abstract _beforeSave
            // so we'll end up calling it twice, but the result is cached, so seems ok.
            $this->getRegion();
        }

        /* need to track if we changed location */
        $newHash = Mage::helper('frans')->calculateLocationHash($this);
        $currentHash = $this->getLocationHash();

        if ($newHash != $currentHash)
        {
            $this->setLocationHasChanged(true);
            $this->setLocationHash($newHash);
        }

         parent::_beforeSave();

        // also try and set the customer ID
        if ($this->getCustomerAddress()
            && $this->getCustomerAddress()->getId()
            && !$this->getCustomerId())
        {
            $this->setCustomerId($this->getCustomerAddress()->getCustomer()->getCustomerId());
        }

        return $this;
    }


    public function getGiftMessage()
    {
        return $this->getOrder()->getGiftMessage();
    }

    public function getSpecialInstructions()
    {
        return $this->getOrder()->getSpecialInstructions();
    }

    public function getBaseShippingAmount()
    {
        return $this->getOrder()->getBaseShippingAmount();
    }

}