<?php
class GoSolid_Frans_Model_Sales_Order_Item extends Mage_Sales_Model_Order_Item
{

	public function isVirtualGiftCard(){
		return $this->getProductType() == 'virtualgiftcard';
	}

    public function isGiftCard()
    {
        return in_array($this->getProductType(), array('virtualgiftcard', 'giftcard'));
    }

	public function getGiftCardNumber(){
		$giftCard = Mage::getModel('givex/giftcard')->loadByOrderItemId($this->getId());
		return $giftCard->getCardNumber();
	}

}