<?php
/**
 * Created by PhpStorm.
 * User: Steve
 * Date: 8/10/2015
 * Time: 10:48 AM
 */ 
class GoSolid_Frans_Model_Sales_Quote_Address_Item extends Mage_Sales_Model_Quote_Address_Item {

    public function importQuoteItem(Mage_Sales_Model_Quote_Item $quoteItem)
    {
        $this->_quote = $quoteItem->getQuote();
        $this->setQuoteItem($quoteItem)
            ->setQuoteItemId($quoteItem->getId())
            ->setProductId($quoteItem->getProductId())
            ->setProduct($quoteItem->getProduct())
            ->setSku($quoteItem->getSku())
            ->setName($quoteItem->getName())
            ->setDescription($quoteItem->getDescription())
            ->setWeight($quoteItem->getWeight())
            ->setPrice($quoteItem->getPrice())
            ->setCost($quoteItem->getCost())
            ->setConfigurableProductId($quoteItem->getConfigurableProductId())
            ->setAccpacSku($quoteItem->getAccpacSku());

        if (!$this->hasQty()) {
            $this->setQty($quoteItem->getQty());
        }
        $this->setQuoteItemImported(true);
        return $this;
    }

}