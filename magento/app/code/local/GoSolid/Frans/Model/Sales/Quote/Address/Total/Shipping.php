<?php
/**
 * Rewritten to allow us to do a custom shipping amount during the order taking.
 */ 
class GoSolid_Frans_Model_Sales_Quote_Address_Total_Shipping
    extends Mage_Sales_Model_Quote_Address_Total_Shipping
{
    /**
     * Collects the totals. Takes care of adjusting the amount based on
     * the adjusted amount, if that amount is present.
     *
     * @param Mage_Sales_Model_Quote_Address $address
     * @return $this|Mage_Sales_Model_Quote_Address_Total_Shipping
     */
    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        parent::collect($address);

        $method = $address->getShippingMethod();

        // only need special logic if they have a method
        // and if we have an adjusted amount.
        if ($method && $address->hasBaseShippingAmountAdjusted())
        {
            // we use has/get so that we can accept a 0 value.
            $adjustedAmount = $address->getBaseShippingAmountAdjusted();
            if ($adjustedAmount != $address->getBaseShippingAmount())
            {
                // just have to adjust the amounts, nothing else.
                $this->_setAmount($adjustedAmount);
                $this->_setBaseAmount($adjustedAmount);
            }
        }

        if (!$method)
        {
            // unset this so we don't persist it.
            $address->unsBaseShippingAmountAdjusted();
        }

        return $this;
    }

}