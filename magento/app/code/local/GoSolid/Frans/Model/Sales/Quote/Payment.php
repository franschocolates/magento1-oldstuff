<?php

class GoSolid_Frans_Model_Sales_Quote_Payment extends Mage_Sales_Model_Quote_Payment
{
	public function __construct()
	{
		parent::_construct();
	}

    /**
     * Import data array to payment method object,
     * Method calls quote totals collect because payment method availability
     * can be related to quote totals
     * REWRITTEN so that we can conditionally not do things if editing payment
     *
     * @param   array $data
     * @param   GoSolid_Frans_Model_Sales_Order $editOrder
     * @throws  Mage_Core_Exception
     * @return  Mage_Sales_Model_Quote_Payment
     */
    public function importData(array $data, $editOrder = false)
    {
        // fall back to parent if not in edit mode
        if (!$editOrder)
        {
            return parent::importData($data);
        }

        $data = new Varien_Object($data);

        $this->setMethod($data->getMethod());
        $method = $this->getMethodInstance(true);

        /**
         * Payment availability related with quote totals.
         * We have recollect quote totals before checking
         */
        // REMOVED because we don't have a real quote
        //$this->getQuote()->collectTotals();

        $method->assignData($data);
        /*
        * validating the payment data
        */
        $method->validate($editOrder);
        return $this;
    }

    /**
     * Retrieve payment method model object
     *
     * @return Mage_Payment_Model_Method_Abstract
     */
    public function getMethodInstance($useDefaultStore = false)
    {
        $method = Mage_Payment_Model_Info::getMethodInstance();
        
        if ($method->getCode() == 'multiship' || $useDefaultStore)
        {
        	#echo "Multiship method instance" . PHP_EOL;
        	return $method->setStoreId(Mage::app()->getStore());
        }
        
        #echo "Using Parent method instance" . PHP_EOL;
        return $method->setStore($this->getQuote()->getStore());
    }
	
    public function importMultishipData()
    {
        $data = new Varien_Object();

        $this->setMethod('multiship');
        $data['method'] = 'multiship';
        $method = $this->getMethodInstance();

        if (!$method->isAvailable($this->getQuote())) {
            Mage::throwException(Mage::helper('sales')->__('The Multiship Payment Method is not available.'));
        }

        #echo "assigning data.";
        $method->assignData($data);
        /*
        * validating the payment data
        */
        return $this;
    }
    
}