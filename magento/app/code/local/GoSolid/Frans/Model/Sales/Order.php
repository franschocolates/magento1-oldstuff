<?php

/**
 * Order model
 * 	Custom logic to support multiship orders
 *
 * @method int getIsMultishipParent()
 * @method GoSolid_Frans_Model_Sales_Order setIsMultishipParent(int $value)
 * @method DateTime getPreferredArrivalDate()
 * @method GoSolid_Frans_Model_Sales_Quote_Address setPreferredArrivalDate(DateTime $value)
 * @method int getIcePacks()
 * @method GoSolid_Frans_Model_Sales_Order setIcePacks(int $value)
 * @method string getAdminNotes()
 * @method GoSolid_Frans_Model_Sales_Order setAdminNotes(string $value)
 * @method int getIsReadyForCapture()
 * @method GoSolid_Frans_Model_Sales_Order setIsReadyForCapture(int $value)
 *
 */
class GoSolid_Frans_Model_Sales_Order extends Mage_Sales_Model_Order
{
    const STATE_READY_FOR_PICKUP    = 'ready_for_pickup';
    const STATE_PICKED_UP           = 'picked_up';

    const STATUS_PRINTED            = 'Printed';

    private $_childOrders = null;
    private $_parentOrder = null;

    private $_childItems = null;

    private $_needSaveChildOrders = false;
    private $_skipSaveParentOrder = false;


    public function getIsMultishipChildOrder()
    {
        return ($this->getMultishipParentId() > 0);
    }

    /**
     * @return GoSolid_Frans_Model_Sales_Order|null
     */
    public function getMultishipParentOrder()
    {
        if ($this->getIsMultishipChildOrder())
        {
            if (!isset($this->_parentOrder))
            {
                $this->_parentOrder = Mage::getModel('sales/order')->load($this->getMultishipParentId());
            }

            return $this->_parentOrder;
        }

        return null;
    }

    /**
     * @param GoSolid_Frans_Model_Sales_Order $order
     */
    public function setMultishipParentOrder($order)
    {
        if ($this->getId() || $this->getMultishipParentId())
        {
            Mage::throwException('Cannot set parent of order that already has an ID/parent.');
        }

        $this->_parentOrder = $order;

        if ($order->getId())
        {
            $this->setMultishipParentId($order->getId());
        }
        return $this;
    }

    public function getCanSendNewEmailFlag()
    {
        if ($this->getIsMultishipChildOrder())
        {
            return false;
        }

        return parent::getCanSendNewEmailFlag();
    }

    public function sendNewOrderEmail()
    {
        $alert_template = Mage::getStoreConfig('frans/email_alert_options/email_alert_type');
        if ($alert_template == null){
            $alert_template = 'alert_default.phtml';
        }

        if ($this->getIsMultishipChildOrder())
        {
            throw new GoSolid_Frans_Exception('Cannot send email for child multiship order');
        }

        $storeId = $this->getStore()->getId();

        if (!Mage::helper('sales')->canSendNewOrderEmail($storeId)) {
            return $this;
        }
        // Get the destination email addresses to send copies to
        $copyTo = $this->_getEmails(self::XML_PATH_EMAIL_COPY_TO);
        $copyMethod = Mage::getStoreConfig(self::XML_PATH_EMAIL_COPY_METHOD, $storeId);

        // Start store emulation process
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);

        try {
            // Retrieve specified view block from appropriate design package (depends on emulated store)
            $paymentBlock = Mage::helper('payment')->getInfoBlock($this->getPayment())
                ->setIsSecureMode(true);
            $paymentBlock->getMethod()->setStore($storeId);
            $paymentBlockHtml = $paymentBlock->toHtml();
        } catch (Exception $exception) {
            // Stop store emulation process
            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
            throw $exception;
        }

        // Retrieve corresponding email template id and customer name
        if ($this->getCustomerIsGuest()) {
            $templateId = Mage::getStoreConfig(self::XML_PATH_EMAIL_GUEST_TEMPLATE, $storeId);
            $customerName = $this->getBillingAddress()->getName();
            $orderSummaryHtml		= Mage::app()->getLayout()->createBlock('frans/fransemail_order')->setTemplate('fransemail/order/summary/order_summary.phtml')->setOrder($this)->setIsGuest(true)->setParentTemplate($templateId)->toHtml();
        } else {
            $templateId = Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE, $storeId);
            $customerName = $this->getCustomerName();
            $orderSummaryHtml		= Mage::app()->getLayout()->createBlock('frans/fransemail_order')->setTemplate('fransemail/order/summary/order_summary.phtml')->setOrder($this)->setIsGuest(false)->setParentTemplate($templateId)->toHtml();
        }

        // Prepare Fran's email components
        $shippingAlertHtml		= Mage::app()->getLayout()->createBlock('frans/fransemail_order')->setTemplate('fransemail/order/shippingalerts/' . $alert_template)->setOrder($this)->setParentTemplate($templateId)->toHtml();
        $shipmentDetailsHtml	= Mage::app()->getLayout()->createBlock('frans/fransemail_order')->setTemplate('fransemail/order/shipment/shipment_details.phtml')->setOrder($this)->setEmailType('order')->setParentTemplate($templateId)->toHtml();
        $contactDetailsHtml     = Mage::app()->getLayout()->createBlock('frans/page_contactdetails')->setTemplate('page/contact-details.phtml')->setUseEmailFormatting(true)->toHtml();

        // Stop store emulation process
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);

        $mailer = Mage::getModel('core/email_template_mailer');
        $emailInfo = Mage::getModel('core/email_info');
        $emailInfo->addTo($this->getCustomerEmail(), $customerName);
        if ($copyTo && $copyMethod == 'bcc') {
            // Add bcc to customer email
            foreach ($copyTo as $email) {
                $emailInfo->addBcc($email);
            }
        }
        $mailer->addEmailInfo($emailInfo);

        // Email copies are sent as separated emails if their copy method is 'copy'
        if ($copyTo && $copyMethod == 'copy') {
            foreach ($copyTo as $email) {
                $emailInfo = Mage::getModel('core/email_info');
                $emailInfo->addTo($email);
                $mailer->addEmailInfo($emailInfo);
            }
        }

        // Set all required params and send emails
        $mailer->setSender(Mage::getStoreConfig(self::XML_PATH_EMAIL_IDENTITY, $storeId));
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId($templateId);

        $templateParams = array(
            'order'					=> $this,
            'billing'				=> $this->getBillingAddress(),
            'payment_html'			=> $paymentBlockHtml,
            'order_summary'			=> $orderSummaryHtml,
            'shipment_details'		=> $shipmentDetailsHtml,
            'contact_details'       => $contactDetailsHtml
        );

        // only include shipping alert for non-virtual, non-pickup orders
        if(($this->getIsVirtual() != 1 && !$this->isPickupAtStore()) || $this->getIsMultishipParent()){
            $templateParams['shipping_alert'] = $shippingAlertHtml;
        }

        $mailer->setTemplateParams($templateParams);
        $mailer->send();

        $this->setEmailSent(true);
        $this->_getResource()->saveAttribute($this, 'email_sent');

        return $this;
    }

    public function sendOrderUpdateEmail($notifyCustomer = true, $comment = '')
    {
        $alert_template = Mage::getStoreConfig('frans/email_alert_options/email_alert_type');
        if ($alert_template == null){
            $alert_template = 'alert_default.phtml';
        }

        $storeId = $this->getStore()->getId();

        if (!Mage::helper('sales')->canSendOrderCommentEmail($storeId)) {
            return $this;
        }
        // Get the destination email addresses to send copies to
        $copyTo = $this->_getEmails(self::XML_PATH_UPDATE_EMAIL_COPY_TO);
        $copyMethod = Mage::getStoreConfig(self::XML_PATH_UPDATE_EMAIL_COPY_METHOD, $storeId);
        // Check if at least one recepient is found
        if (!$notifyCustomer && !$copyTo) {
            return $this;
        }

        if ($this->getIsMultishipChildOrder())
        {
            throw new GoSolid_Frans_Exception('Cannot send email for child multiship order');
        }

        // Start store emulation process
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);

        try {
            // Retrieve specified view block from appropriate design package (depends on emulated store)
            $paymentBlock = Mage::helper('payment')->getInfoBlock($this->getPayment())
                ->setIsSecureMode(true);
            $paymentBlock->getMethod()->setStore($storeId);
            $paymentBlockHtml = $paymentBlock->toHtml();
        } catch (Exception $exception) {
            // Stop store emulation process
            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
            throw $exception;
        }

        // Retrieve corresponding email template id and customer name
        if ($this->getCustomerIsGuest()) {
            $templateId = Mage::getStoreConfig(self::XML_PATH_UPDATE_EMAIL_GUEST_TEMPLATE, $storeId);
            $customerName = $this->getBillingAddress()->getName();
            $orderSummaryHtml = Mage::app()->getLayout()->createBlock('frans/fransemail_order')->setTemplate('fransemail/order/summary/order_summary.phtml')->setOrder($this)->setIsGuest(true)->setParentTemplate($templateId)->toHtml();
        } else {
            $templateId = Mage::getStoreConfig(self::XML_PATH_UPDATE_EMAIL_TEMPLATE, $storeId);
            $customerName = $this->getCustomerName();
            $orderSummaryHtml = Mage::app()->getLayout()->createBlock('frans/fransemail_order')->setTemplate('fransemail/order/summary/order_summary.phtml')->setOrder($this)->setIsGuest(false)->setParentTemplate($templateId)->toHtml();
        }

        // Prepare Fran's email components
        $shippingAlertHtml		= Mage::app()->getLayout()->createBlock('frans/fransemail_order')->setTemplate('fransemail/order/shippingalerts/' . $alert_template)->setOrder($this)->setParentTemplate($templateId)->toHtml();
        $shipmentDetailsHtml	= Mage::app()->getLayout()->createBlock('frans/fransemail_order')->setTemplate('fransemail/order/shipment/shipment_details.phtml')->setOrder($this)->setParentTemplate($templateId)->toHtml();
        $contactDetailsHtml     = Mage::app()->getLayout()->createBlock('frans/page_contactdetails')->setTemplate('page/contact-details.phtml')->setUseEmailFormatting(true)->toHtml();

        // Stop store emulation process
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);

        $mailer = Mage::getModel('core/email_template_mailer');
        if ($notifyCustomer) {
            $emailInfo = Mage::getModel('core/email_info');
            $emailInfo->addTo($this->getCustomerEmail(), $customerName);
            if ($copyTo && $copyMethod == 'bcc') {
                // Add bcc to customer email
                foreach ($copyTo as $email) {
                    $emailInfo->addBcc($email);
                }
            }
            $mailer->addEmailInfo($emailInfo);
        }

        // Email copies are sent as separated emails if their copy method is
        // 'copy' or a customer should not be notified
        if ($copyTo && ($copyMethod == 'copy' || !$notifyCustomer)) {
            foreach ($copyTo as $email) {
                $emailInfo = Mage::getModel('core/email_info');
                $emailInfo->addTo($email);
                $mailer->addEmailInfo($emailInfo);
            }
        }

        // Set all required params and send emails
        $mailer->setSender(Mage::getStoreConfig(self::XML_PATH_UPDATE_EMAIL_IDENTITY, $storeId));
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId($templateId);

        $templateParams = array(
            'order'   => $this,
            'comment' => $comment,
            'billing' => $this->getBillingAddress(),
            'order_summary'			=> $orderSummaryHtml,
            'shipment_details'		=> $shipmentDetailsHtml,
            'contact_details'       => $contactDetailsHtml
        );

        // only include shipping alert for non-virtual, non-pickup orders
        if(($this->getIsVirtual() != 1 && !$this->isPickupAtStore()) || $this->getIsMultishipParent()){
            $templateParams['shipping_alert'] = $shippingAlertHtml;
        }

        $mailer->setTemplateParams($templateParams);
        $mailer->send();

        return $this;
    }

    public function getKeyedChildOrders()
    {
        if (!$this->getIsMultishipParent())
        {
            return false;
        }

        if ($this->getId() > 0)
        {
            $childCollection = Mage::getModel('sales/order')->getCollection();
            $childCollection = $childCollection->addFieldToFilter('multiship_parent_id', $this->getId());
            return $childCollection->getItems();
        }
        else
        {
            return false;
        }

        return false;
    }

    public function getChildOrders()
    {
        if (!$this->getIsMultishipParent())
        {
            return false;
        }

        if ($this->_childOrders != null)
        {
            return $this->_childOrders;
        }
        else if ($this->getId() > 0)
        {
            $childCollection = Mage::getModel('sales/order')->getCollection();
            $childCollection = $childCollection->addFieldToFilter('multiship_parent_id', $this->getId());
            $this->_childOrders = array_values($childCollection->getItems());

            return $this->_childOrders;
        }
        else
        {
            return false;
        }

        return false;
    }

    /*
     * This method is used when the order is first created.
     *
     */
    public function setChildOrders($childOrders)
    {
        $this->_childOrders = $childOrders;
    }

    /*
     * Cancels the order.
     * 	$parentCanceled indicates that the parent is being cancelled, so that we know whether or not
     * 		to call to our parent to notify them.
     */
    public function cancel($parentCanceled = false)
    {
        if ($this->getIsMultishipParent())
        {
            return $this->_cancelParentOrder();
        }
        else if ($this->getIsMultishipChildOrder())
        {
            return $this->_cancelChildOrder($parentCanceled);
        }

        return parent::cancel();

    }

    /*
     * We should never be able to invoice child orders
     */
    public function canInvoice()
    {
        if ($this->getIsMultishipChildOrder())
        {
            return false;
        }

        return parent::canInvoice();
    }

    public function canCancel()
    {

        //check for one or more shipped items.
        if($this->hasShipments() == true)
        {
            return false;
        }

        //if it has one or more invoices, no cancel either.
        if($this->hasInvoices() == true)
        {
            return false;
        }





        if ($this->getIsMultishipParent())
        {
            return $this->_canCancelParentOrder();
        }
        else if ($this->getIsMultishipChildOrder())
        {
            return $this->_canCancelChildOrder();
        }
        else
        {
            return parent::canCancel();
        }
    }

    /***
     * Indicates whether the order can ship.
     */
    public function canShip()
    {
        # Parent orders can never ship,
        if ($this->getIsMultishipParent())
        {
            return false;
        }

        # for other orders, we delegate to base functionality
        return parent::canShip();
    }

    public function update()
    {
        // do nothing if we aren't a parent order.
        if (!$this->getIsMultishipParent())
        {
            return;
        }

        if ($childOrders = $this->getChildOrders())
        {
            // zero everything out
            $this->_updateTotals($childOrders);

            $readyToCapture = $this->_checkIsReadyForCapture($childOrders);
            $this->setIsReadyForCapture($readyToCapture);

            // update our status
            /* Mac: We are not going to o this on save. Try and do this on cron cleanup.
             * $this->_updateParentStateOrStatus($childOrders);
             */
        }

        return $this;
    }

    public function getAllItems()
    {
        if ($this->getIsMultishipParent())
        {
            $allItems = array();

            # need to combine items from all child orders
            foreach ($this->getChildOrders() as $childOrder)
            {
                $allItems = array_merge($allItems, $childOrder->getAllItems());
            }

            return $allItems;
        }

        return parent::getAllItems();
    }

    /*
     * Looks up the billing address. Necessary to override for child orders, which will not have an
     * address collection, which is what the base implementation does by default.
     */
    public function getBillingAddress()
    {
        if ($this->getIsMultishipChildOrder())
        {
            return Mage::getModel('sales/order_address')->load($this->getBillingAddressId());
        }

        return parent::getBillingAddress();
    }

    public function getAllShippingAddresses()
    {
        $addresses = array();

        if ($this->getIsMultishipParent())
        {
            foreach ($this->getChildOrders() as $childOrder)
            {
                if ($childShippingAddress = $childOrder->getShippingAddress())
                {
                    $addresses[] = $childShippingAddress;
                }
            }
        }
        // virtual orders may not have a shipping address
        // so make sure we get something back
        else if ($shippingAddress = $this->getShippingAddress())
        {
            $addresses[] = $shippingAddress;
        }

        return $addresses;
    }




    public function save()
    {
        parent::save();

        # Once save completes (ie, it commits), then we go ahead and update
        # the parent (if applicable)
        if ($this->getIsMultishipChildOrder() && !$this->_skipSaveParentOrder)
        {
            try
            {
                $parentOrder = $this->getMultishipParentOrder();
                $parentOrder->update()->save();
                $this->_skipSaveParentOrder = true;
            }
            catch (Exception $ex)
            {
                Mage::logException($ex);
                # TODO: give some sort of error that we couldn't update the parent
            }
        }

        return $this;
    }

    public function updateShipMethod($shipMethod, $shippingAmount, $preferredArrivalDate, $plannedShipDate)
    {
        if ($this->getIsMultishipParent()
            || $this->isCanceled()
            || $this->getState() == self::STATE_COMPLETE)
        {
            return;
        }

        $oldArrivalDate = new DateTime($this->getPreferredArrivalDate());
        $oldShipDate = new DateTime($this->getPlannedShipDate());
        $newArrivalDate = new DateTime($preferredArrivalDate);
        $newPlannedDate = new DateTime($plannedShipDate);
        if ($this->getShippingMethod() != $shipMethod
            || $this->getBaseShippingAmount() != $shippingAmount
            || ($oldArrivalDate != $newArrivalDate)
            ||  ($oldShipDate != $newPlannedDate)
        )
        {
            $this->setShippingMethod($shipMethod);
            $this->setPreferredArrivalDate($preferredArrivalDate);
            $this->setPlannedShipDate($plannedShipDate);
            $this->_updateShippingTotals($shippingAmount);
            $this->setShippingDescription($this->_getShipMethodDescription($shipMethod));
            $this->save();
        }
    }

    /***
     * Returns the order increment ID with the accounting label prefix
     * Centralized here so we don't sprinkle it all over the place
     */
    public function getOrderNumberWithPrefix()
    {
        return $this->getAccountingLabel() . $this->getIncrementId();
    }

    /**
     * Override to make the parent order look up the child items for things that
     * want to get items for a parent order. Similar to getAllItems (but used in different places)
     * @see Mage_Sales_Model_Order::getItemsCollection()
     */
    public function getItemsCollection($filterByTypes = array(), $nonChildrenOnly = false)
    {
        if ($this->getIsMultishipParent())
        {
            if (is_null($this->_childItems)) {
                # NOTE: in the base order, it will set _items to the collection it builds
                # However, if you set _items, then it will save them every time you save the order
                # which could be a *lot* of items.
                # so we are using a new variable
                $this->_childItems = Mage::getResourceModel('sales/order_item_collection')
                    ->setParentOrderFilter($this);

                if ($filterByTypes) {
                    $this->_childItems->filterByTypes($filterByTypes);
                }
                if ($nonChildrenOnly) {
                    $this->_childItems->filterByParent();
                }
            }
            return $this->_childItems;
        }

        return parent::getItemsCollection($filterByTypes, $nonChildrenOnly);
    }

    /**
     * @return GoSolid_Frans_Model_Resource_Sales_Order_Collection
     */
    public function getCollection()
    {
        return parent::getCollection();
    }

    /**
     *
     * Performs a check to see if all items have been refunded when a creditmemo is created
     * This allows us to update the state on a child order even though the credit memo is not against it
     * @param array $itemsRefunded - array with order item ID as the key and actual item as value
     * @return bool Indicates whether it needs to be saved
     */
    public function checkStateOnRefund($affectedItems)
    {
        if (!$this->getIsMultishipChildOrder())
        {
            Mage::throwException("Order {$this->getId()} is not a child order, cannot check state.");
        }

        # we'll need to check all the items
        # first check the ones we have locally before we load the rest
        $allRefunded = true;
        foreach ($affectedItems as $orderItemId => $orderItem)
        {
            # just find the first case they aren't all refunded
            if ($orderItem->getQtyRefunded() < $orderItem->getQtyOrdered())
            {
                $allRefunded = false;
                break;
            }
        }

        # only continue if all of those are refunded.
        # if not, not everything is refunded
        if ($allRefunded)
        {
            # now we need to get all of our items and make sure everything is refunded
            foreach ($this->getItemsCollection() as $item)
            {
                # check and see if already processed; if so, then we know it's fully refunded
                if (!isset($affectedItems[$item->getId()]))
                {
                    # check and see if alreayd fully refunded; if not, we're done
                    if ($item->getQtyRefunded() < $item->getQtyOrdered())
                    {
                        $allRefunded = false;
                        break;
                    }
                }
            }

            if ($allRefunded)
            {
                # hey, everything really was refunded. update our state.
                $this->_setState(Mage_Sales_Model_Order::STATE_CLOSED, true
                    , 'Moved to closed since all items refunded.', false, false);
                # we can assume the parent is also being updated
                # because it will be saved with the creditmemo
                $this->_skipSaveParentOrder = true;
                $this->save();
            }
        }

        return $allRefunded;
    }

    public function isPickupAtStore()
    {
        return $this->getShippingMethod(true)->getCarrierCode() == 'pickupatstore';
    }

    /**
     * Send email with shipment data, if multiship order
     * If not, return false.
     *
     * @param boolean $notifyCustomer
     * @param string $comment
     * @return GoSolid_Frans_Model_Sales_Order
     */
    public function sendShipmentEmail($notifyCustomer, $comment = "", $batch = false )
    {
        /*
         *   We only send shipment notification emails from parent orders and regular shipments.
         *   If this is a child shipment, we should get the parent and send it from there.
         */

        $order = $this; //default to this order.
        Mage::log( "Starting to send shipment email for order " . $order->getIncrementId() );


        //check to see if the order is batch.
        if($batch == true)
        {
            //if this is a child order, we should log and exit. We do not send child orders in batch.
            if ( $this->getIsMultishipChildOrder() )
            {
                Mage::log( "Tried to send child order shipment email in batch. No mail sent order " . $order->getIncrementId() );
                return;
            }
        }
        else if($this->getIsMultishipParent() == true)
        {
            Mage::log( "Tried to send an order on a multiship parent" . $order->getIncrementId() );
            return;
        }

        //now send the message
        $storeId = $order->getStore()->getId();

        //Mac: not sure what this thing does.. but it was here.
        if (!Mage::helper('sales')->canSendNewShipmentEmail($storeId)) {
            return $this;
        }



        // Get the destination email addresses to send copies to
        $copyTo = $this->_getEmails(Mage_Sales_Model_Order_Shipment::XML_PATH_EMAIL_COPY_TO);
        $copyMethod = Mage::getStoreConfig(Mage_Sales_Model_Order_Shipment::XML_PATH_EMAIL_COPY_METHOD, $storeId);
        // Check if at least one recepient is found
        if (!$notifyCustomer && !$copyTo) {
            return $this;
        }

        // Start store emulation process
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);

        try {
            // Retrieve specified view block from appropriate design package (depends on emulated store)
            $paymentBlock = Mage::helper('payment')->getInfoBlock($order->getPayment())
                ->setIsSecureMode(true);
            $paymentBlock->getMethod()->setStore($storeId);
            $paymentBlockHtml = $paymentBlock->toHtml();
        } catch (Exception $exception) {
            // Stop store emulation process
            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
            throw $exception;
        }

        $mailer = Mage::getModel('core/email_template_mailer');
        if ($notifyCustomer) {
            $emailInfo = Mage::getModel('core/email_info');
            $customerName = $order->getBillingAddress()->getName();
            $emailInfo->addTo($order->getCustomerEmail(), $customerName);
            if ($copyTo && $copyMethod == 'bcc') {
                // Add bcc to customer email
                foreach ($copyTo as $email) {
                    $emailInfo->addBcc($email);
                }
            }
            $mailer->addEmailInfo($emailInfo);
        }

        // Email copies are sent as separated emails if their copy method is 'copy' or a customer should not be notified
        if ($copyTo && ($copyMethod == 'copy' || !$notifyCustomer)) {
            foreach ($copyTo as $email) {
                $emailInfo = Mage::getModel('core/email_info');
                $emailInfo->addTo($email);
                $mailer->addEmailInfo($emailInfo);
            }
        }

        // Set all required params and send emails
        $mailer->setSender(Mage::getStoreConfig(Mage_Sales_Model_Order_Shipment::XML_PATH_EMAIL_IDENTITY, $storeId));
        $mailer->setStoreId($storeId);


        /*
         * Prepare the content of the email.
         */

        $shippingMethod = $order->getShippingMethod(true);


        //set the email template
        if($order->isPickupAtStore())
        {
            //pickup order; choose which template to use based on the order status
            switch($order->getStatus()){
                case self::STATE_PICKED_UP:
                    $path = "sales_email/shipment/email_template_picked_up";
                    $isPickedUp = true; // also set a flag to pass to the store details template
                    break;
                default: // "ready_for_pickup" is the expected alternative to "picked_up" but catch anything just in case
                    $path = "sales_email/shipment/email_template_ready_for_pickup";
                    $isPickedUp = false;
            }
            $templateId = Mage::getStoreConfig($path, $storeId);

            //get the store assigned.
            $retailStore = Mage::getModel("frans/retailStore")->load($shippingMethod["method"], "store_code");

            //included templates.

            $shipmentDetailsHtml = Mage::app()->getLayout()->createBlock('frans/fransemail_order')->setTemplate('fransemail/order/shipment/shipment_details.phtml')->setOrder($order)->setIsGuest($order->getCustomerIsGuest())->setEmailType('pickup')->setHideShipmentSummary(true)->setHideArrivalDate(true)->setParentTemplate($templateId)->toHtml();
            $storeDetailsHtml = Mage::app()->getLayout()->createBlock('core/template')->setTemplate('fransemail/order/shipment/store_pickup_info.phtml')->setRetailStore($retailStore)->setIsPickedUp($isPickedUp)->toHtml();

            $mailer->setTemplateSubject("pickup");
            $mailer->setTemplateId($templateId);
            $mailer->setTemplateParams(array(
                    'order'					=> $order,
                    'comment'				=> $comment,
                    'shipment_details'		=> $shipmentDetailsHtml,
                    'store_details'         => $storeDetailsHtml,
                    'retail_store_name'     => $retailStore->getName(),
                )
            );

        }
        else
        {
            // figure out if the customer is a guest and set the email template accordingly
            if($this->getCustomerId()){
                $templateId = Mage::getStoreConfig(Mage_Sales_Model_Order_Shipment::XML_PATH_EMAIL_TEMPLATE, $storeId);
            } else {
                $templateId = Mage::getStoreConfig(Mage_Sales_Model_Order_Shipment::XML_PATH_EMAIL_GUEST_TEMPLATE, $storeId);
            }

            //included templates.

            $orderSummaryHtml = Mage::app()->getLayout()->createBlock('frans/fransemail_order')->setTemplate('fransemail/order/summary/order_summary.phtml')->setOrder($order)->setIsGuest($order->getCustomerIsGuest())->setParentTemplate($templateId)->toHtml();
            $shipmentDetailsHtml = Mage::app()->getLayout()->createBlock('frans/fransemail_order')->setTemplate('fransemail/order/shipment/shipment_details.phtml')->setOrder($order)->setIsGuest($order->getCustomerIsGuest())->setEmailType('shipment')->setHideArrivalDate(true)->setParentTemplate($templateId)->toHtml();
            $contactDetailsHtml     = Mage::app()->getLayout()->createBlock('frans/page_contactdetails')->setTemplate('page/contact-details.phtml')->setUseEmailFormatting(true)->toHtml();


            $mailer->setTemplateId($templateId);
            $mailer->setTemplateParams(array(
                    'order'					=> $order,
                    'comment'				=> $comment,
                    'billing'				=> $order->getBillingAddress(),
                    'payment_html'			=> $paymentBlockHtml,
                    'order_summary'			=> $orderSummaryHtml,
                    'shipment_details'		=> $shipmentDetailsHtml,
                    'contact_details'       => $contactDetailsHtml,

                )
            );

        }

        // Stop store emulation process
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);

        try{
            //send the email.
            $mailer->send();
            Mage::log( "Sent email for order " . $order->getIncrementId() );
            //set the send date.
            $sentAt = Mage::getModel('core/date')->gmtTimestamp();
            $order->setShipmentEmailSentAt($sentAt);
            $order->save();
            Mage::log( "Updated order shipment email sent at order " . $order->getIncrementId() );
            if ($order->getIsMultishipParent())
            {
                foreach ($order->getChildOrders() as $childOrder)
                {
                    // if the child order has a ship date
                    // but has not been marked that a shipment email was sent
                    // then mark it as sent
                    if ($childOrder->getShipDate() && !$childOrder->getShipmentEmailSentAt())
                    {
                        $childOrder->setShipmentEmailSentAt($sentAt)
                            ->setSkipSaveParentOrder()
                            ->save();
                        Mage::log( "Updated child order shipment email sent at order " . $childOrder->getIncrementId() );
                    }
                }
            }
        }
        catch(Exception $ex)
        {
            //log the exception.
            Mage::log("Unable to send shipment notification email for order #" . $order->getIncrementId() . ". " . $ex->getMessage() );
        }

        return true;
    }

    public function sendShipmentUpdateEmail($notifyCustomer, $comment)
    {
        if (!$this->getIsMultishipParent())
        {
            return false;
        }

        $order = $this;
        $storeId = $order->getStore()->getId();

        if (!Mage::helper('sales')->canSendShipmentCommentEmail($storeId)) {
            return $this;
        }
        // Get the destination email addresses to send copies to
        $copyTo = $this->_getEmails(Mage_Sales_Model_Order_Shipment::XML_PATH_UPDATE_EMAIL_COPY_TO);
        $copyMethod = Mage::getStoreConfig(Mage_Sales_Model_Order_Shipment::XML_PATH_UPDATE_EMAIL_COPY_METHOD, $storeId);
        // Check if at least one recepient is found
        if (!$notifyCustomer && !$copyTo) {
            return $this;
        }

        // Start store emulation process
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);

        // Retrieve corresponding email template id and customer name
        if ($order->getCustomerIsGuest()) {
            $templateId = Mage::getStoreConfig(Mage_Sales_Model_Order_Shipment::XML_PATH_UPDATE_EMAIL_GUEST_TEMPLATE, $storeId);
            $customerName = $order->getBillingAddress()->getName();
            $orderSummaryHtml = Mage::app()->getLayout()->createBlock('frans/fransemail_order')->setTemplate('fransemail/order/summary/order_summary.phtml')->setOrder($order)->setIsGuest(true)->setParentTemplate($templateId)->toHtml();
            $shipmentDetailsHtml = Mage::app()->getLayout()->createBlock('frans/fransemail_order')->setTemplate('fransemail/order/shipment/shipment_details.phtml')->setOrder($order)->setIsGuest(true)->setEmailType('shipment')->setParentTemplate($templateId)->toHtml();
        } else {
            $templateId = Mage::getStoreConfig(Mage_Sales_Model_Order_Shipment::XML_PATH_UPDATE_EMAIL_TEMPLATE, $storeId);
            $customerName = $order->getCustomerName();
            $orderSummaryHtml = Mage::app()->getLayout()->createBlock('frans/fransemail_order')->setTemplate('fransemail/order/summary/order_summary.phtml')->setOrder($order)->setIsGuest(false)->setParentTemplate($templateId)->toHtml();
            $shipmentDetailsHtml = Mage::app()->getLayout()->createBlock('frans/fransemail_order')->setTemplate('fransemail/order/shipment/shipment_details.phtml')->setOrder($order)->setIsGuest(false)->setEmailType('shipment')->setParentTemplate($templateId)->toHtml();
        }

        $contactDetailsHtml = Mage::app()->getLayout()->createBlock('frans/page_contactdetails')->setTemplate('page/contact-details.phtml')->setUseEmailFormatting(true)->toHtml();

        // Stop store emulation process
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);

        $mailer = Mage::getModel('core/email_template_mailer');
        if ($notifyCustomer) {
            $emailInfo = Mage::getModel('core/email_info');
            $emailInfo->addTo($order->getCustomerEmail(), $customerName);
            if ($copyTo && $copyMethod == 'bcc') {
                // Add bcc to customer email
                foreach ($copyTo as $email) {
                    $emailInfo->addBcc($email);
                }
            }
            $mailer->addEmailInfo($emailInfo);
        }

        // Email copies are sent as separated emails if their copy method is 'copy' or a customer should not be notified
        if ($copyTo && ($copyMethod == 'copy' || !$notifyCustomer)) {
            foreach ($copyTo as $email) {
                $emailInfo = Mage::getModel('core/email_info');
                $emailInfo->addTo($email);
                $mailer->addEmailInfo($emailInfo);
            }
        }

        // Set all required params and send emails
        $mailer->setSender(Mage::getStoreConfig(Mage_Sales_Model_Order_Shipment::XML_PATH_UPDATE_EMAIL_IDENTITY, $storeId));
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId($templateId);
        $mailer->setTemplateParams(array(
                'order'    => $this,
                'comment'  => $comment,
                'billing'  => $order->getBillingAddress(),
                'order_summary'			=> $orderSummaryHtml,
                'shipment_details'		=> $shipmentDetailsHtml,
                'contact_details'       => $contactDetailsHtml
            )
        );
        $mailer->send();

        return $this;
    }

    public function canEditPayment()
    {
        // can only edit if nothing invoiced
        // and if not a child order
        return !$this->getBaseInvoicedAmount()
        && !$this->getIsMultishipChildOrder();
    }

    public function changePayment($newPaymentData)
    {
        if (!$this->canEditPayment())
        {
            Mage::throwException("Cannot edit payment for order {$this->getIncrementId()}");
        }

        // we need to use some quote objects, even though our order already exists
        $quotePayment = Mage::getModel('sales/quote_payment')
            ->setStoreId($this->getStoreId())
            ->importData($newPaymentData, $this);

        $convertor = Mage::getModel('sales/convert_quote');
        /* @var Mage_Sales_Model_Order_Payment $newPayment */
        $newPayment = $convertor->paymentToOrderPayment($quotePayment);

        // set old payment as removed
        $oldPayment = $this->getPayment();
        $oldPayment->isDeleted(true);
        $this->addPayment($newPayment);

        // pass that we are placing in edit mode
        $newPayment->place(true);

        Mage::dispatchEvent('sales_order_change_payment', array(
            'order'=>$this
        , 'old_payment' => $oldPayment
        , 'new_payment' => $newPayment
        ));

        // have to change some sort of attribute or else it thinks we haven't changed
        // so it doesn't do anything.
        // so just set updated at, which should really get set anyway.
        return $this->setUpdatedAt(Mage::getSingleton('core/date')->gmtDate());

    }

    /**
     * @return GoSolid_Frans_Model_Sales_Order_Payment
     */
    public function getPayment()
    {
        if ($this->getIsMultishipChildOrder())
        {
            return $this->getMultishipParentOrder()->getPayment();
        }

        return parent::getPayment();
    }

    public function getAllowedStatusActions()
    {
        $status = $this->getStatus();

        $s = Mage::getModel("sales/order_status")->load($status, "status");

        $allowedActions = $s->getAllowedActions();

        return array_map('trim', explode(',', $allowedActions));
    }


    public function getAllowedStatusActionOptions()
    {
        //get the available status actions
        $allowedActions = $this->getAllowedStatusActions();

        $actions = array();

        if($this->getIsMultishipParent())
        {
            return $actions;
        }



        if(count($allowedActions) > 0)
        {
            $allActions = $allowedActions;


            //add labels
            $statuses = Mage::getModel("sales/order_status")->getCollection()->toOptionArray();


            foreach($allActions as $action)
            {
                //look for it in the key value array.
                foreach($statuses as $key => $value)
                {
                    if($action == $value["value"])
                    {
                        $actions[] = array("status" => $value["value"], "label" => $value["label"]);
                    }
                }
            }


        }






        return $actions;

    }

    public function getSalesperson(){
        // if "salesperson" data is set against the order, return that, otherwise return default text
        $salesperson = $this->getData('salesperson');
        if(!$salesperson){
            $salesperson = Mage::helper('sales')->__('Web Order');
        }
        return $salesperson;
    }

    public function getFrontendStatus()
    {
        //get the frontend status by finding the current status of the order and grabbing the front end column.
        $status = $this->getStatus();

        $frontendStatus = Mage::getModel("sales/order_status")->load($status, "status")->getStatusFrontend();

        if(strlen($frontendStatus) > 0 )
        {
            $status = $frontendStatus;
        }

        return $status;
    }

    public function getFrontendStatusLabel()
    {
        $status = $this->getFrontendStatus();
        $label = Mage::getModel("sales/order_status")->load($status, "status")->getLabel();

        return $label;
    }

    public function getWeightWithIcePacks()
    {
        return $this->getWeight() + Mage::getModel('frans/icePack')->getTotalIcePackWeight($this->getIcePacks());
    }

    protected function _afterSave()
    {
        $addresses = $this->getAddressesCollection()->getItems();
        $billingAddress = $this->_getBillingAddress($addresses);


        if($billingAddress) {
            $isMultiShip = false;
            if(is_object($billingAddress->getOrder()->getData('quote'))) {
                $isMultiShip = (int)($billingAddress->getOrder()->getData('quote')->getData('is_multi_shipping'));
            }

            // Mark this as an order's billing address
            $billingAddress->setIsOrder(1);

            // If it's a multi-ship order,
            //      only process billing address if there's an entity_id.
            //      If there isn't an entity_id, it's not the final billing address
            // If NOT a multi-ship order, record billing address
            if (!$isMultiShip || ($billingAddress->getEntityId() && $isMultiShip)) {
                $billingAddress->setEntityType("sales_order_address");
                // Record billing address for Mailing List
                Mage::dispatchEvent('mailing_list_capture_billing_address', $billingAddress->getData());
            }
        }

        if ($this->getIsMultishipParent() && $this->_needSaveChildOrders)
        {
            foreach ($this->getChildOrders() as $childOrder)
            {
                $childOrder->save();
            }

            $this->_needSaveChildOrders = false;
        }

        return parent::_afterSave();
    }




    /**
     * Check order state before saving
     */
    protected function _checkState()
    {
        if (!$this->getId()) {
            return $this;
        }



        $userNotification = $this->hasCustomerNoteNotify() ? $this->getCustomerNoteNotify() : null;



        if (!$this->isCanceled()
            && !$this->canUnhold()
            && !$this->canInvoice()
            && !$this->canShip()) {
            if (0 == $this->getBaseGrandTotal() || $this->canCreditmemo()) {

                /* MAC: turned off the setting to complete whe the invoice amount is zero.
                if ($this->getState() !== self::STATE_COMPLETE) {
                   $this->_setState(self::STATE_COMPLETE, true, '', $userNotification);
                }
                */
            }
            /**
             * Order can be closed just in case when we have refunded amount.
             * In case of "0" grand total order checking ForcedCanCreditmemo flag
             */
            elseif (floatval($this->getTotalRefunded()) || (!$this->getTotalRefunded()
                    && $this->hasForcedCanCreditmemo())
            ) {
                if ($this->getState() !== self::STATE_CLOSED) {
                    $this->_setState(self::STATE_CLOSED, true, '', $userNotification);
                }
            }
        }

        if ($this->getState() == self::STATE_NEW && $this->getIsInProcess()) {
            $this->setState(self::STATE_PROCESSING, true, '', $userNotification);
        }
        return $this;
    }


    private function _updateTotals($childOrders)
    {
        # NOTE: we do not include refunds, as those are only handled on the parent.
        $totalColumns = array(
            'base_discount_amount' => 0 ,
            'base_discount_canceled' => 0 ,
            'base_discount_invoiced' => 0 ,
            'base_grand_total' => 0 ,
            'base_shipping_amount' => 0 ,
            'base_shipping_canceled' => 0 ,
            'base_shipping_invoiced' => 0 ,
            'base_shipping_tax_amount' => 0 ,
            'base_subtotal' => 0 ,
            'base_subtotal_canceled' => 0 ,
            'base_subtotal_invoiced' => 0 ,
            'base_tax_amount' => 0 ,
            'base_tax_canceled' => 0 ,
            'base_tax_invoiced' => 0 ,
            'base_total_canceled' => 0 ,
            'base_total_invoiced' => 0 ,
            'base_total_invoiced_cost' => 0 ,
            'base_total_paid' => 0 ,
            'base_total_qty_ordered' => 0 ,
            'discount_amount' => 0 ,
            'discount_canceled' => 0 ,
            'discount_invoiced' => 0 ,
            'grand_total' => 0 ,
            'shipping_amount' => 0 ,
            'shipping_canceled' => 0 ,
            'shipping_invoiced' => 0 ,
            'shipping_tax_amount' => 0 ,
            'subtotal' => 0 ,
            'tax_amount' => 0 ,
            'tax_canceled' => 0 ,
            'tax_invoiced' => 0 ,
            'base_adjustment_negative' => 0 ,
            'base_adjustment_positive' => 0 ,
            'base_shipping_discount_amount' => 0 ,
            'base_subtotal_incl_tax' => 0 ,
            'base_total_due' => 0 ,
            'shipping_discount_amount' => 0 ,
            'subtotal_incl_tax' => 0 ,
            'hidden_tax_amount' => 0 ,
            'base_hidden_tax_amount' => 0 ,
            'shipping_hidden_tax_amount' => 0 ,
            'base_shipping_hidden_tax_amnt' => 0 ,
            'hidden_tax_invoiced' => 0 ,
            'base_hidden_tax_invoiced' => 0 ,
            'shipping_incl_tax' => 0 ,
            'base_shipping_incl_tax' => 0 ,
            'total_canceled' => 0,
        );

        foreach ($childOrders as $child)
        {
            foreach ($totalColumns as $fieldName => $value)
            {
                $methodName = 'get' . $this->_camelize($fieldName);
                $newValue = $value + $child->$methodName();
                $totalColumns[$fieldName] = $newValue;
            }
        }

        foreach ($totalColumns as $fieldName => $finalValue)
        {
            $methodName = 'set' . $this->_camelize($fieldName);
            $this->$methodName($finalValue);
        }

        # certain fields we need to calculate by hand, as they don't properly take into
        # account cancellations. We calculated them above, but now subtract out cancelled values
        $newBaseSubtotal = $this->getBaseSubtotal() - $this->getBaseSubtotalCanceled();
        $newBaseShippingAmount = $this->getBaseShippingAmount() - $this->getBaseShippingCanceled();

        //ToDo: Remove
        Mage::log("Calculating newBaseDiscountAmount: getBaseDiscountAmount - getBaseDiscountCanceled");
        Mage::log("getBaseDiscountAmount: " . $this->getBaseDiscountAmount());
        Mage::log("getBaseDiscountCanceled: " . $this->getBaseDiscountCanceled());
        /*
         * baseDiscountCanceled amount is stored as a positive number and baseDiscountAmount is stored as a negative number.
         *
         * Since the baseDiscountAmount is a negative value, the baseDiscountCanceled amount should be added to the baseDiscountAmount
         * to deduct it from the discount.
         */
        $newBaseDiscountAmount = $this->getBaseDiscountAmount() + $this->getBaseDiscountCanceled();

        $giftcardAmount = max(0, $this->getBaseGiftcardAmount());

        $this->setBaseSubtotal($newBaseSubtotal);
        $this->setBaseShippingAmount($newBaseShippingAmount);
        //ToDo: Remove
        Mage::log("Setting newBaseDiscountAmount: " . $newBaseDiscountAmount);
		$this->setBaseDiscountAmount($newBaseDiscountAmount);
		$this->setBaseGrandTotal($newBaseSubtotal + $newBaseShippingAmount + $newBaseDiscountAmount);

		# research whether there is some converter we can use for the following:
		$this->setSubtotal($newBaseSubtotal);
		$this->setShippingAmount($newBaseShippingAmount);
		$this->setDiscountAmount($newBaseDiscountAmount);
		$this->setGrandTotal($this->getBaseGrandTotal());

	}
	
	private function _canCancelParentOrder()
	{
		# can cancel if all child orders are either cancelled, or can be cancelled
		$cancellable  = true;
		
		foreach ($this->getChildOrders() as $childOrder)
		{
			# they can either already be canceled
			# or they can be cancellable
			if (!$childOrder->isCanceled() && !$childOrder->canCancel())
			{
				$cancellable = false;
				break;
			}
		}
		
		return $cancellable;
	}



	private function _canCancelChildOrder()
	{
		# can cancel assuming it hasn't shipped
		if ($shipments = $this->getShipmentsCollection())
		{
			$size = $shipments->getSize();
			return $size < 1;
		}

        # can cancel assuming it hasn't been invoiced.



        return true;

    }

    private function _cancelChildOrder($fromParent)
    {


        if (!$this->getIsMultishipChildOrder())
        {
            return;
        }


        if ($this->_canCancelChildOrder())
        {
            // no payment necessary since we're a child order.
            $this->registerCancellation($cancelReason);

            # if our parent wasn't canceled (so we were canceled individually)
            # then we need to let it know to update it's totals
            # after we save
            $this->_skipSaveParentOrder = $fromParent;
        }

        return $this;
    }


    private function _cancelParentOrder()
    {
        if (!$this->getIsMultishipParent())
        {
            return;
        }

        if ($this->_canCancelParentOrder())
        {
            $this->_needSaveChildOrders = true;

            // cancel each child
            foreach ($this->getChildOrders() as $childOrder)
            {
                $childOrder->cancel('Parent multi ship order was canceled.');
            }

            $this->getPayment()->cancel();
            $this->registerCancellation();

            # we only raise the event against the parent
            Mage::dispatchEvent('order_cancel_after', array('order' => $this));
        }
        else
        {
            throw new GoSolid_Frans_Exception('Invalid call to cancel parent order, it is not cancellable.');
        }

        return $this;
    }

    private function _updateParentStateOrStatus($childOrders)
    {
        # we would only update for certain statuses....
        $currentState = $this->getState();

        # don't care if already canceled/completed/closed
        if (in_array($currentState, array( self::STATE_CANCELED , self::STATE_COMPLETE, self::STATE_CLOSED)))
        {
            return;
        }

        /*
            ideally we'd have different methods for different transitions we'd expect
             e.g. New->Processing, Processing->Ready for Payment
            However, there is always the chance that all were canceled, which could happen in any state
            so really we just go through and figure it out based on all children
        */
        $totalCount = count($childOrders);

        if ($totalCount == 0)
        {
            # nothing valid if we have no child orders.
            return;
        }

        $canceledCount = $completedCount = 0;

        foreach ($childOrders as $childOrder)
        {
            $childState = $childOrder->getState();

            # only 3 valid states for child orders are complete & canceled
            if ($childState == self::STATE_COMPLETE)
            {
                $completedCount++;
            }
            else if ($childState == self::STATE_CANCELED)
            {
                $canceledCount++;
            }
        }

        # if we have some completed (shipped) and not all are completed/canceled
        if ($canceledCount == $totalCount)
        {
            # we must not have been canceled, since we would have exited above. Mark canceled
            $this->_setState(Mage_Sales_Model_Order::STATE_CANCELED, true
                , 'Moved to canceled since all child orders canceled.', false, false);
        }
        else if ($totalCount == ($canceledCount + $completedCount) )
        {
            # all shipped or canceled, make sure we are now ready for payment
            if ($this->getStatus() != 'ready_for_payment')
            {
                $this->_setState(self::STATE_PROCESSING, 'ready_for_payment'
                    , 'Moved to ready for payment.', false, false);
            }
        }
        else if ($completedCount > 0)
        {
            # at least some have shippped, make sure we update the status appropriately
            if ($currentState != self::STATE_PROCESSING)
            {
                # "true" means to use the default for the status
                # (which is processing in this case, which is what we want)
                $this->_setState(self::STATE_PROCESSING, true
                    , 'Moved to processing since at least one child order complete.', false, false);
            }
        }

        # all updates should have been made above.
    }



    private function _getShipMethodDescription($shipMethod)
    {
        # method is in the format carrer_[method]
        # so we load the carrier and then find correct method
        $pieces = explode('_', $shipMethod, 2);
        $carrier = Mage::getModel('shipping/config')->getCarrierInstance($pieces[0]);

        if ($carrier)
        {
            foreach ($carrier->getAllowedMethods() as $key => $value)
            {
                if ($pieces[1] == $key)
                {
                    return $carrier->getConfigData('title') . ' - ' .  $value;
                }
            }
        }
        else
        {
            Mage::log("Could not find carrier for ship method $shipMethod", Zend_Log::WARN);
        }

    }

    /**
     *
     * Updates the base shipping amount ot the specified total
     * And then recalculate the rest of the shipping totals based on that
     * @param unknown_type $baseShippingAmount
     */
    private function _updateShippingTotals($baseShippingAmount)
    {
        $this->setBaseShippingAmount($baseShippingAmount);
        $this->setBaseGrandTotal(
            $this->getBaseSubtotal() + $this->getBaseShippingAmount() + $this->getBaseDiscountAmount()
        );

        # the following are based on the base
        # and should ideally use currency conversion
        $this->setShippingAmount($baseShippingAmount);
        $this->setShippingInclTax($baseShippingAmount);
        $this->setBaseShippingInclTax($baseShippingAmount);
        $this->setGrandTotal($this->getBaseGrandTotal());
    }

    /**
     * Gets the emails for a specific type of email. Copied from Shipment class for use in parent orders
     * @see Mage_Sales_Model_Order::_getEmails()
     */
    protected function _getEmails($configPath)
    {
        $data = Mage::getStoreConfig($configPath, $this->getStoreId());
        if (!empty($data)) {
            return explode(',', $data);
        }
        return false;
    }

    public function hold()
    {
        if (!$this->canHold()) {
            Mage::throwException(Mage::helper('sales')->__('Hold action is not available.'));
        }

        //set the hold flag.
        $this->setIsOnHold(true);
        return $this;


    }

    public function unhold()
    {
        if (!$this->canUnhold()) {
            Mage::throwException(Mage::helper('sales')->__('Unhold action is not available.'));
        }

        $this->setIsOnHold(false);
        return $this;
    }

    public function canHold()
    {
        $state = $this->getState();
        if ($this->isCanceled() || $this->isPaymentReview()
            || $state === self::STATE_COMPLETE || $state === self::STATE_CLOSED || $state === self::STATE_HOLDED) {
            return false;
        }

        if ($this->getActionFlag(self::ACTION_FLAG_HOLD) === false) {
            return false;
        }

        if($this->getIsOnHold() == true)
        {
            return false;
        }

        return true;
    }

    /**
     * Retrieve order unhold availability
     *
     * @return bool
     */
    public function canUnhold()
    {
        return ($this->getIsOnHold() == true ? true : false);
    }

    /**
     * Does necessary steps to mark an order printed.
     */
    public function setPrinted()
    {
        return $this->setIsPrinted(true)
            ->setStatus(self::STATUS_PRINTED);
    }

    /**
     * force skip update for updating child orders for parent orders
     */

    public function setSkipSaveParentOrder()
    {
        $this->_skipSaveParentOrder = true;

        return $this;
    }

    protected function _beforeSave()
    {
        if ($this->_parentOrder && !$this->getMultishipParentId())
        {
            $this->setMultishipParentId($this->_parentOrder->getId());
            $this->setBillingAddressId($this->_parentOrder->getBillingAddressId());
        }

        if ($billingAddress = $this->getBillingAddress())
        {
            // populate our firstname/lastname from billing address
            // if we don't have them
            if (!$this->getCustomerFirstname())
            {
                $this->setCustomerFirstname($billingAddress->getFirstname());
            }

            if (!$this->getCustomerLastname())
            {
                $this->setCustomerLastname($billingAddress->getLastname());
            }

            if (!$this->getCustomerMiddlename())
            {
                $this->setCustomerMiddlename($billingAddress->getMiddlename());
            }

            if (!$this->getCustomerPrefix())
            {
                $this->setCustomerPrefix($billingAddress->getPrefix());
            }

            if (!$this->getCustomerSuffix())
            {
                $this->setCustomerSuffix($billingAddress->getSuffix());
            }
        }

        // need beforeSave to run first because it changes data we need to change.
        // and we don't want our changes to be lost.
        parent::_beforeSave();

        // build total qty ordered
        // this is copied from the quote but wrong in the case of multiship
        if (!$this->getId())
        {
            // for child orders, the total qty ordered is wrong, since it's copied from the quote
            $totalQtyOrdered = 0;
            foreach ($this->getAllItems() as $item) {
                $parent = $item->getQuoteParentItemId();
                if ($parent && !$item->getParentItem()) {
                    $item->setParentItem($this->getItemByQuoteItemId($parent));
                } elseif (!$parent) {
                    $totalQtyOrdered += $item->getQtyOrdered();
                }
            }
            $this->setTotalQtyOrdered($totalQtyOrdered);

            // for parents, the total item count is wrong, because it includes each SKU
            // for each child order, not just once
            if ($this->getIsMultishipParent())
            {
                $skuList = array();
                foreach ($this->getAllItems() as $item) {
                    $parent = $item->getQuoteParentItemId();
                    if ($parent && !$item->getParentItem()) {
                        $item->setParentItem($this->getItemByQuoteItemId($parent));
                    } elseif (!$parent) {
                        // just need an entry, we'll count the items later

                        $skuList[$item->getSku()] = true;
                    }
                }

                $this->setTotalItemCount(count($skuList));
            }
        }

        return $this;
    }

    /**
     * Place order payments
     *  Overridden to handle placing of child orders
     *
     * @return Mage_Sales_Model_Order
     */
    protected function _placePayment()
    {
        // don't place payment if a child
        if (!$this->getIsMultishipChildOrder())
        {
            $this->getPayment()->place();
        }

        // also need to handle the ship date of virtual orders
        // they will never have anything shipped, but we need to be able to invoice them.
        if ($this->getIsVirtual() && !$this->getIsMultishipParent())
        {
            $this->setShipDate(Mage::getModel('core/date')->date('Y-m-d'));
            $this->setIsReadyForCapture(true);
            $this->setStatus('shipped');
        }

        return $this;
    }

    /**
     * @param $childOrders
     */
    protected function _checkIsReadyForCapture($childOrders)
    {
        Mage::log("Checking if {$this->getIncrementId()} is ready for capture.");
        /** @var GoSolid_Frans_Model_Sales_Order $childOrder */
        foreach ($childOrders as $childOrder)
        {
            Mage::log("{$childOrder->getIncrementId()} has state of {$childOrder->getState()}");
            Mage::log("{$childOrder->getIncrementId()} has ship date {$childOrder->hasShipDate()}");
            if (!($childOrder->getState() == self::STATE_CANCELED || $childOrder->hasShipDate()))
            {
                return false;
            }
        }

        // all child orders are either canceled, or have a ship date.
        // we should be ready to capture
        return true;
    }

    /**
     * @param $addresses
     * @return bool
     */
    private function _getBillingAddress($addresses){
        foreach($addresses AS $address){
            if(strtoupper($address->getAddressType()) == strtoupper("billing")){
                return $address;
            }
        }

        return false;
    }

    public function getCustomerEmail(){
        if(!$this->hasData('customer_email') || !$this->getData('customer_email')){
            if($this->getData('customer_id')){

                // manually fetch customer; it may not exist on the order
                $customer = Mage::getModel('customer/customer')->load($this->getData('customer_id'));
                $this->setData('customer_email', $customer->getEmail());

            }
            // todo: Is there any 'else'? Where should we find this email
            // if it's not on the order, and there's no customer to pull from?
        }
        return $this->getData('customer_email');
    }

}
