<?php

class GoSolid_Frans_Model_Sales_Observer
{

    /**
     * This method is called whenever the payment captured method is called, and other scenarios where
     * payment is theoretically captured (e.g. offline). So there should be multiple references to this
     * in the events in config.xml
     *
     * @param Varien_Object $observer
     */
    public function onPaymentCapture($observer)
    {
        $invoice = $observer->getInvoice();

        $order = $invoice->getOrder();

        if (!$order->getCapturedAt())
        {
            // get when they are capturing, in GMT
            $capturedInGmt = Mage::getModel('core/date')->gmtDate();
            $order->setCapturedAt($capturedInGmt);
        }
    }

    /**
     *
     * Sets the order status to be Processing as a default
     * That way we don't dig into the normal magento order processing flow.
     *
     * @param Varien_Object $event
     */
    public function onOrderPaymentPlaceEnd($event)
    {
        /** @var Mage_Sales_Model_Order_Payment $payment */
        $payment = $event->getPayment();
        $order = $payment->getOrder();

        // everything should be in order state of Processing when order placed
        $order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, $order->getStatus());
    }

	public function convertConfigurableProductId($observer){

	}

    /**
     * Handles any post-place logic
     * Mainly this is populating the customer name
     *
     * @param Varien_Event_Observer $observer
     */
    public function onOrderPlaceAfter($observer)
    {
        $order = $observer->getOrder();
        $this->_ensureCustomerHasName($order);

        $customer = $order->getCustomer();
        $request = Mage::app()->getRequest()->getParams();
        if(isset($request['order']['customer_notes'])){
            $notes = Mage::getModel('frans/customerNotes')
                ->loadByCustomer($customer->getId())
                ->setNotes($request['order']['customer_notes'])
                ->save();
    }
    }

	public function onShipmentSave($observer)
	{
		//Mage::log("Storing ship date on order if present.");
		# need to save the ship date on the order
		# we only care about new shipments, so abort if we have an ID
        /** @var GoSolid_Frans_Model_Sales_Order_Shipment $shipment */
		$shipment = $observer->getShipment();
		if ($shipment->getId())
		{
			return $observer;
		}

		if (!$shipDate = $shipment->getShipDate())
		{
            // a ship date wasn't explicitly set - that means it was sent manually
            // and not via the ShipmentManager
            // so set it to today
            // magento core/date takes care of timezone issues.
            $shipDate = Mage::getModel('core/date')->date('Y-m-d');
            $shipment->setShipDate($shipDate);
		}

        // we are trusting that the order is being saved with the shipment
        // which should always happen
        /** @var GoSolid_Frans_Model_Sales_Order $order */
        $order = $shipment->getOrder();
        $order->setShipDate($shipment->getShipDate());
        $order->setStatus("shipped");

        // single shipment orders are now ready for capture
        if (!$order->getIsMultishipChildOrder())
        {
            $order->setIsReadyForCapture(true);
        }

        return $observer;
	}
	
	/**
	 * 
	 * Called when a quote is being saved. Necessary to determine whether or not to validate the
	 * address, or if it has been validated already.
	 * @param unknown_type $observer
	 */
	public function onQuoteAddressBeforeSave($observer)
	{
		/* var GoSolid_Frans_Model_Sales_Quote_Address $address  */
		$address = $observer->getQuoteAddress();
		
		if ($address->getLocationHasChanged())
		{
			//Mage::log("Location is new or has changed, or no delivery type, for quote address {$address->getId()} :: Performing validation.");
			$this->_performAddressValidation($address);
		}
		else
		{
			//Mage::log("No location change for quote address {$address->getId()}, nothing to do. Current status:{$address->getAddressDeliveryType()} ");
		}
	}

    /**
     * Called when an order address is being updated.
     * Necessary to determine whether or not to validate the
     * address, or if it has been validated already.
     * @param unknown_type $observer
     */
    public function onOrderAddressBeforeSave($observer)
    {
        /* var GoSolid_Frans_Model_Sales_Order_Address $address  */
        $address = $observer->getAddress();

        if ($address->getLocationHasChanged())
        {
            //Mage::log("Location is new or has changed, or no delivery type, for order address {$address->getId()} :: Performing validation.");
            $this->_performAddressValidation($address);
        }
        else
        {
            //Mage::log("No location change for order address {$address->getId()}, nothing to do. Current status:{$address->getAddressDeliveryType()} ");
        }
    }

    /**
	 * 
	 * Takes care of adding any fields we have in the admin create order screen
	 * Necessary to put them on quote so that they get pulled over
	 * @param Varien_Object $observer
	 */
	public function onAdminOrderProcessData($observer)
	{
		$this->_setAdminNotes($observer);
		$this->_captureAccountingFields($observer);
        $this->_captureOrderItemFields($observer);
	}
	
	/**
	 * 
	 * Sets accounting fields on the quote/order
	 * Originally from Engine, with modifications for accounting originations/labels
	 * @param unknown_type $observer
	 */
    public function setAccountingFields($observer){


        $event = $observer->getEvent();
        $quote = $event->getQuote();
        $order = $event->getOrder();


        // has this already been set (e.g. in the admin)? dip out.
        if(!is_null($order->getAccountingLabel()) || !is_null($order->getOrigination())):
            return $this;
        endif;

        $origination = Mage::getModel('frans/accounting_origination')->getDefaultOrigination();
        $accounting_label = $origination;

        $quote->setOrigination($origination);
        $quote->setAccountingLabel($accounting_label);

        $order->setOrigination($origination);
        $order->setAccountingLabel($accounting_label);

        return $this;
    }


    public function setShippingScore($observer){

        $event = $observer->getEvent();
        $order = $event->getOrder();

        try{

          $shippingAddress = $order->getShippingAddress();

          if(!$shippingAddress){
            return $this;
          }

          $shippingScore = $shippingAddress->getAddressScore();

          Mage::log('shippingScore: ' . $shippingScore);

          if(is_numeric($shippingScore)){
            $order->setShippingScore($shippingScore);
          }

        } catch (Exception $e) {

        }


        return $this;
    }

    /**
     * @param Varien_Event_Observer $observer
     */
    public function setPlannedShipDate($observer)
    {
        // don't always need to set it
        $order = $observer->getOrder();

        // only bother if it has an preferred arrival date
        if ($order->getPreferredArrivalDate())
        {
            // for now, only do it if not already set
            if (!$order->getPlannedShipDate())
            {
                $shippingAddress = $order->getShippingAddress();
                $shipDate = Mage::helper('frans')->getShipDateForMethodAndPostcode(
                                $order->getPreferredArrivalDate(), $order->getShippingMethod(), $shippingAddress->getPostcode());
                if ($shipDate)
                {
                    $order->setPlannedShipDate($shipDate);
                }
                else
                {
                    //Mage::log("Could not determine ship date for order {$order->getIncrementId()}!", Zend_Log::WARN);
                }
            }
        }
    }

    public function setQuoteAddressIdOnOrderAddress($observer)
    {
        $quoteAddress = $observer->getAddress();
        $orderAddress = $observer->getOrderAddress();

        $orderAddress->setQuoteAddressId($quoteAddress->getId());
    }

	public function setConfigurableProductId($observer)
	{
		$params = Mage::app()->getRequest()->getParams();
		$productId = $params['product'];
		$configurableProductId = $params['configurable_product_id'];

		if($configurableProductId){

			$items = $observer->getItems();
			foreach($items as $item){
				// make sure we're updating the right item
				if($item->getProductId() == $productId){
					// load the quote item and set the configurable product ID
					$item->setConfigurableProductId($configurableProductId);
				}
			}

		}
	}

	private function _performAddressValidation($address)
	{
		try
		{
			$validator = Mage::getModel('frans/shipping_addressValidator');
			$validator->storeAddressTypeAndProposedAddress($address);
		}
		catch (Exception $exception)
		{
			Mage::helper('frans')->logCriticalException($exception, "Customer_Observer address validation");
			//Mage::logException($exception);
		}
	}
	
	private function _setAdminNotes($observer)
	{
		$request = $observer->getRequest();
		
		$orderCreateModel = $observer->getOrderCreateModel();

		if ($orderCreateModel)
		{
			if (isset($request['order']) && isset($request['order']['admin_notes']))
			{
				$orderCreateModel->getQuote()->setAdminNotes($request['order']['admin_notes']);
			}
		}
		
		return $this;
	}
	
	/**
	 * 
	 * Captures accounting fields
	 * Some originally from Engine observer
	 * @param unknown_type $observer
	 */
    public function _captureAccountingFields($observer){
        
        $event = $observer->getEvent();
        $orderCreateModel = $event->getOrderCreateModel();
        $quote = $orderCreateModel->getQuote();
        $request = $event->getRequest();

        // this captures order-level details
        // currently, (origination and accounting label)
        foreach(array('accounting_label', 'origination') as $field):
            if(isset($request['order'][$field])):
                $quote->setData($field, $request['order'][$field]);
            endif;
        endforeach;

        return $this;
    }

    /**
     * Sends out shipment notification emails
     *
     * @param $schedule
     */
    public function sendShipmentNotificationEmails($schedule)
    {

        //First we are going to set the max runtime to 12 minutes as php max X time on prod is 15 minutes
        $maxRunTime = strtotime('+12 minutes', strtotime(date('Y-m-d H:i:s')));

        //Set the single shipment Query first
        $singleShipmentQuery = 'SELECT entity_id
                                FROM   sales_flat_order
                                WHERE  ship_date IS NOT NULL
                                       AND shipment_email_sent_at IS NULL
                                       AND multiship_parent_id IS NULL
                                       AND Ifnull(is_multiship_parent, 0) = 0
                                ORDER BY ship_date ASC
                                LIMIT 1';

        //Set the multi shipment Query second
        $multiShipmentQuery = 'SELECT multiship_parent_id
                                FROM   sales_flat_order
                                WHERE  ship_date IS NOT NULL
                                       AND shipment_email_sent_at IS NULL
                                       AND multiship_parent_id IS NOT NULL
                                ORDER BY ship_date ASC
                                LIMIT 1';

        /**
         * Get the resource model
         * Retrieve the read connection
         */
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        //while in the 20 minute window lets try and make some shipments
        do
        {
          //We will throttle our logic to even seconds this is kinda wack but will give a break incase another connection is try to access the db
          if(date('s')%2 == 1)
          {

               /**
               * Execute the query and store the results in $results
               */
              $readConnection->getConnection();
              $results = $readConnection->fetchAll($singleShipmentQuery);
              $readConnection->closeConnection();
              if(is_array($results[0]))
              {
                  try{
                      Mage::getModel('sales/order')->load($results[0]["entity_id"])->sendShipmentEmail(true, '', true);
                  }catch (Exception $e)
                  {
                      Mage::log('Cron Error Sending Shipment Email: '.$e->getMessage(),Zend_Log::ERR);
                  }

              }

          }

          //We will throttle our logic to even seconds
          if(date('s')% 2 == 1)
          {
                /**
                 * Execute the query and store the results in $results
                 */
                $readConnection->getConnection();
                $multiresults = $readConnection->fetchAll($multiShipmentQuery);
                $readConnection->closeConnection();
                if(is_array($multiresults[0]))
                {
                    try{
                        Mage::getModel('sales/order')->load($multiresults[0]["multiship_parent_id"])->sendShipmentEmail(true, '', true);
                    }catch (Exception $e)
                    {
                        Mage::log('Cron Error Sending Shipment Email: '.$e->getMessage(),Zend_Log::ERR);
                    }
                }

          }

        }
        while ( $maxRunTime > strtotime(date('Y-m-d H:i:s')) );
    }

    /**
     * Handle processing of cancelled orders.
     * This one is called only from adminhtml events (as of this writing)
     * So that we don't pop up a message in a non admin session
     *
     * @param Varien_Event_Observer $observer
     */
    public function onOrderCancelAfter($observer)
    {
        $order = $observer->getOrder();
        //Mage::log("Processing order cancel for order {$order->getIncrementId()}.");

        if ($order->getBaseGiftcardInvoicedAmount())
        {
            // add a message that it has a giftcard amount
            $adminSession = Mage::getSingleton('adminhtml/session');
            $adminSession->addWarning(
                sprintf(Mage::helper('core')->__('Order %s had payment by gift card. You will need to manually return the gift card amount.')
                    , $order->getIncrementId()));
        }

        return $this;
    }

    protected function _captureOrderItemFields($observer){

        $event = $observer->getEvent();
        $orderCreateModel = $event->getOrderCreateModel();
        $quote = $orderCreateModel->getQuote();
        $request = $event->getRequest();

        // this captures item-level notes on the order
        // as set by admins.
        if(isset($request['item'])):
            foreach($request['item'] as $id => $params):
                $item = $quote->getItemById($id);
                if(!$item):
                    continue;
                endif;
                foreach(array('notes', 'order_type', 'category') as $cat):
                    if(isset($params[$cat])):
                        $item->setData($cat, $params[$cat]);
                    endif;
                endforeach;
            endforeach;
        endif;

        return $this;
    }


    //moves the order item notes to the quote.
    public function onOrderItemEdit($observer)
    {
        $orderItem = $observer->getOrderItem();
        $quoteItem = $observer->getQuoteItem();


        $quoteItem->setOrderType($orderItem->getOrderType());
        $quoteItem->setCategory($orderItem->getCategory());
        $quoteItem->setNotes($orderItem->getNotes());


        $quoteItem->setOriginalCustomPrice($orderItem->getPrice());
        $quoteItem->setCustomPrice($orderItem->getPrice());



        return $this;
    }

    /**
     * Handles code to run before a credit memo is "registered"
     * Unfortunately this event is actually sort of inaccurate -
     * this happens whether or not the credit memo is being registered
     * (it is raised from the CreditMemoController in the _initCreditMemo function)
     * so we need to check and see if the action is a new credit memo
     * as opposed to saving.
     *
     * @param $observer
     */
    public function onCreditMemoBeforeRegister($observer)
    {
        $request = $observer->getRequest();
        /* @var Mage_Sales_Model_Order_Creditmemo $creditmemo */
        $creditmemo = $observer->getCreditmemo();

        // only bother if new and if the order has a giftcard amount
        if ($request->getActionName() == 'new'
            && $creditmemo->getOrder()->getBaseGiftcardInvoicedAmount() > 0)
        {
            $giftcardAmount = $creditmemo->getOrder()->getBaseGiftcardInvoicedAmount();
            // they are trying to creditmemo one that has a giftcard, which
            // will lead to wacky results, since the totals don't calculate correctly
            // give them a message and pre-populate the amount
            $creditmemo->setBaseAdjustmentFeeNegative($giftcardAmount);
            $baseGrandTotal = $creditmemo->getBaseGrandTotal();
            // update the grand total for display only
            $creditmemo->setBaseGrandTotal($baseGrandTotal - $giftcardAmount);
            $creditmemo->setGrandTotal($creditmemo->getBaseGrandTotal());

            // finally, give them a warning message
            Mage::getSingleton('adminhtml/session')->addNotice(
                Mage::helper('core')->__('This order was placed with a gift card. The adjustment fee below has been automatically populated with the gift card amount so the refund can be properly processed.')
            );

        }
    }

    /**
     * This method will prevent our normal order controller from setting the status for an order
     * It should only set a comment.
     * Prevents us having to override the controller action itself, which could get messy
     *
     * @param Varien_Event_Observer $observer
     */
    public function onOrderAddCommentPreDispatch(Varien_Event_Observer $observer)
    {
        $request = $observer->getControllerAction()->getRequest();
        if ($orderId = $request->getParam('order_id'))
        {
            //Mage::log('Will attempt to maintain status on order when adding comment.');
            $order = Mage::getModel('sales/order')->load($orderId);

            // make sure we have an order
            if ($order->getId())
            {
                // we have it - override the status parameter to be the order's current status
                // since we never want to set the status via comment.
                $data = $request->getPost('history');
                $data['status'] = $order->getStatus();
                $request->setPost('history', $data);
            }
        }
    }

    /**
     *
     * Handles any logic for updating the sales observer prior to clearing expired quotes
     *
     * @param Varien_Object $observer
     */
    public function onClearExpiredQuotesBefore(Varien_Object $observer)
    {
        $salesObserver = $observer->getSalesObserver();

        // add a filter so it doesn't touch saved quotes, since they might be useful
        $salesObserver->setExpireQuotesAdditionalFilterFields(array(
            'saved_quote' => 0
        ));
    }

    /**
     * Handles the fact that a virtual gift card was created - should send an email
     *
     * @param Varien_Object $event
     */
    public function onVirtualGiftcardRegisterAfter(Varien_Object $event)
    {
        /** @var GoSolid_Givex_Model_Giftcard $giftcard */
        $giftcard = $event->getGiftcard();

        Mage::helper('frans/giftCard')->sendVirtualGiftCardEmail($giftcard);
    }

    /**
     * Handles the case that a virtual gift card couldn't be created
     *
     * @param Varien_Object $event
     */
    public function onVirtualGiftcardFailure(Varien_Object $event)
    {
        /** @var GoSolid_Givex_Model_Giftcard $giftcard */
        $giftcard = $event->getGiftcard();

        $orderItem = $giftcard->getOrderItem();
        if ($orderItem && ($order = $orderItem->getOrder()))
        {
            $message = "Order #{$order->getIncrementId()} was placed and included a virtual gift card. However, the virtual gift card could not be created. The card will need to be created manually in the site via the Gift Card admin interface";

            Mage::helper('frans')->sendAdminNotificationEmail('Error creating Virtual Gift Card', $message);
        }

    }

    /**
     * Makes sure that a customer has a firstname/lastname
     * To cover cases where they don't (due to no requirement)
     *
     * @param GoSolid_Frans_Model_Sales_Order $order
     */
    protected function _ensureCustomerHasName($order)
    {
        if ($order && $order->getCustomerId())
        {
            /** @var Mage_Customer_Model_Customer $customer */
            $customer = $order->getCustomer();

            // in case it's not cached on the order for some reason
            if (!$customer)
            {
                $customer = Mage::getModel('customer/customer')->load($order->getCustomerId());
            }

            $changed = false;
            $billingAddress = $order->getBillingAddress();

            foreach (array('firstname', 'lastname', 'middlename', 'suffix', 'prefix') as $attributeName)
            {
                if (!$customer->hasData($attributeName) && $billingAddress->hasData($attributeName))
                {
                    $customer->setData($attributeName, $billingAddress->getData($attributeName));
                    $changed = true;
                }
            }

            if ($changed)
            {
                //Mage::log("Updating firstname/lastname for customer {$customer->getId()} because they were missing.");
                $customer->save();
            }
        }
    }
}