<?php

class GoSolid_Frans_Model_Sales_Quote extends Mage_Sales_Model_Quote
{
	
    /**
     * Collect totals
     * 	- overridden to make multishipping quotes recalculate totals from addresses
     *  - since otherwise row totals do not match individual item totals
     *  (i.e. the row total for the item in the order does not match the sum of the row totals in addresses)
     *
     * @return GoSolid_Frans_Model_Sales_Quote
     */
    public function collectTotals()
    {
        /**
         * Protect double totals collection
         */
        if ($this->getTotalsCollectedFlag()) {
            return $this;
        }
        Mage::dispatchEvent($this->_eventPrefix . '_collect_totals_before', array($this->_eventObject => $this));

        $this->setSubtotal(0);
        $this->setBaseSubtotal(0);

        $this->setSubtotalWithDiscount(0);
        $this->setBaseSubtotalWithDiscount(0);

        $this->setGrandTotal(0);
        $this->setBaseGrandTotal(0);

		$itemsInAddresses = array();

        /* @var $address Mage_Sales_Model_Quote_Address */
        foreach ($this->getAllAddresses() as $address) {
            $address->setSubtotal(0);
            $address->setBaseSubtotal(0);

            $address->setGrandTotal(0);
            $address->setBaseGrandTotal(0);

            $address->collectTotals();

            $this->setSubtotal((float) $this->getSubtotal() + $address->getSubtotal());
            //Mage::log("Current subtotal: " . $this->getSubtotal());
            $this->setBaseSubtotal((float) $this->getBaseSubtotal() + $address->getBaseSubtotal());

            $this->setSubtotalWithDiscount(
                (float) $this->getSubtotalWithDiscount() + $address->getSubtotalWithDiscount()
            );
            $this->setBaseSubtotalWithDiscount(
                (float) $this->getBaseSubtotalWithDiscount() + $address->getBaseSubtotalWithDiscount()
            );

            $this->setGrandTotal((float) $this->getGrandTotal() + $address->getGrandTotal());
            $this->setBaseGrandTotal((float) $this->getBaseGrandTotal() + $address->getBaseGrandTotal());
            
            if ($this->getIsMultishipping())
            {
            	foreach ($address->getItems() as $addressItem)
            	{
					$itemsInAddresses[$addressItem->geQuoteItemId()] = true;
	            }
   			}
            /*
            # need to do extra logic for multishipping ot make sure we collect totals
            # at the item level
            if ($this->getIsMultiShipping())
            {
            	// @var $addressItem Mage_Sales_Model_Quote_Address_Item 
            	foreach ($address->getAllItems() as $addressItem)
            	{
					if (($quoteItemId = $addressItem->getQuoteItemId())
						&& isset($allItemTotals[$quoteItemId]))
					{
						$itemTotals = $allItemTotals[$quoteItemId];
					}
					else
					{
						# these are the fields we want to total
						$itemTotals = array (
							'discount_amount' => 0,
							'base_discount_amount' => 0,
							'tax_amount' => 0,
							'base_tax_amount' => 0,
							'row_total' => 0,
							'base_row_total' => 0,
							'row_total_with_discount' => 0,
							'base_tax_before_discount' => 0,
							'tax_before_discount' => 0,
							'price_incl_tax' => 0,
							'base_price_incl_tax' => 0,
							'price_incl_tax' => 0,
							'row_total_incl_tax' => 0,
							'base_row_total_incl_tax' => 0,
						);
					}
            		Mage::log("I have this quote item: {$addressItem->getQuoteItemId()}");
					
					foreach($itemTotals as $key => $value)
					{
						$itemTotals[$key] += $addressItem->getData($key);
					}
					
					$allItemTotals[$quoteItemId] = $itemTotals;
            	}
            	
            }*/
        }

        Mage::helper('sales')->checkQuoteAmount($this, $this->getGrandTotal());
        Mage::helper('sales')->checkQuoteAmount($this, $this->getBaseGrandTotal());

        $this->setItemsCount(0);
        $this->setItemsQty(0);
        $this->setVirtualItemsQty(0);

        /* @var $item Mage_Sales_Model_Quote_Item */
        //Mage::log("Count of all : " . count($this->getAllVisibleItems()));
        foreach ($this->getAllVisibleItems() as $item) {
            if ($item->getParentItem()) {
                continue;
            }
            $children = $item->getChildren();
            if ($children && $item->isShipSeparately()) {
                foreach ($children as $child) {
                    if ($child->getProduct()->getIsVirtual()) {
                        $this->setVirtualItemsQty($this->getVirtualItemsQty() + $child->getQty()*$item->getQty());
                    }
                }
            }

            if ($item->getProduct()->getIsVirtual()) {
                $this->setVirtualItemsQty($this->getVirtualItemsQty() + $item->getQty());
            }

            $this->setItemsCount($this->getItemsCount()+1);
            $this->setItemsQty((float) $this->getItemsQty()+$item->getQty());
        }

        if ($this->getIsMultiShipping())
        {
        	$calculateAddress = Mage::getModel('frans/sales_quote_multishipCalculationAddress')
        								->setQuote($this);
        								
        	$calculateAddress->collectTotals();
        }
        
        $this->setData('trigger_recollect', 0);
        $this->_validateCouponCode();

        Mage::dispatchEvent($this->_eventPrefix . '_collect_totals_after', array($this->_eventObject => $this));

        $this->setTotalsCollectedFlag(true);
        return $this;
    }

    /**
     * Get all quote totals (sorted by priority)
     * Rewritten to allow us to customize the totals display for multiship quotes
     *
     * @return array
     */
    public function getTotals()
    {
        $sortedTotals = parent::getTotals();

        if ($this->getIsMultiShipping() && isset($sortedTotals['shipping']))
        {
            // reset the totals title
            // based on how many shipping addresses we have
            $sortedTotals['shipping']['title'] =
                Mage::helper('frans')->__('Shipping (%d shipments)', count($this->getAllShippingAddresses()));
        }

        return $sortedTotals;
    }

	
    /**
     * Remove quote item by item identifier
     * 	- Overridden in order to not set mulitshipping mode = false
     *
     * @param   int $itemId
     * @return  Mage_Sales_Model_Quote
     */
    public function removeItem($itemId)
    {
        $item = $this->getItemById($itemId);

        if ($item) {
            $item->setQuote($this);

			# also remove from any shipping addresses if multiship
			if ($this->getIsMultiShipping())
			{
				/* @var $address Mage_Sales_Model_Quote_Address */
				foreach ($this->getAllAddresses() as $address)
				{
					if ($addressItem = $address->getItemByQuoteItemId($item->getId()))
					{
						$address->removeItem($addressItem->getId());
					}
				}
			}
            
            $item->isDeleted(true);
            if ($item->getHasChildren()) {
                foreach ($item->getChildren() as $child) {
                    $child->isDeleted(true);
                }
            }

            $parent = $item->getParentItem();
            if ($parent) {
                $parent->isDeleted(true);
            }

            Mage::dispatchEvent('sales_quote_remove_item', array('quote_item' => $item));
            
            # make sure we recollect
            $this->setTotalsCollectedFlag(false);
        }

        return $this;
    }

    /**
     * When you add an item and it doesn't have an ID, the items collection cached it with no ID
     * Which makes other functions not work (this specifically happens when bulk uploading items
     * This method clears the items so the next call to them will refresh them and they will have IDs
     */
    public function refreshItems()
    {
        $this->_items = null;
    }


    /* returns the best value and fastest dates for each address */
    public function getBestArrivalDatesAndMethods()
    {

        $allAddresses = array();

        $addresses = $this->getAllShippingAddresses();

        for ($i = 0; $i < count($addresses); $i++)
        {
            /* @var GoSolid_Frans_Model_Sales_Quote_Address $address */
            $address = $addresses[$i];

            $datesAndMethods = $address->getAvailableArrivalDatesAndMethods();
            $fastest = array_filter($datesAndMethods, function($v) { return isset($v['fastest']) && $v['fastest']; });
            $cheapest = array_filter($datesAndMethods, function($v) { return isset($v['cheapest']) && $v['cheapest']; });

            $allAddresses[$address->getId()]["cheapest"] = $cheapest;
            $allAddresses[$address->getId()]["fastest"] = $fastest;

        }


        return $allAddresses;

    }

    /* builds a range 'from' and 'to' for date and price. */
    public function getBestArrivalDatesAndMethodsAsRange(){
        $addresses = $this->getBestArrivalDatesAndMethods();

        $cheapest["dates"] = array();
        $fastest["dates"] = array();

        $cheapest["price"] = array();
        $fastest["price"] = array();


        foreach($addresses as $address)
        {
            $cO = $address["cheapest"];
            $fO = $address["fastest"];

            $cheapest["dates"][] = strtotime( key($cO) );
            $fastest["dates"][] = strtotime( key($fO) );


            $cheapest["prices"][] = $cO[key($cO)]["price"];
            $fastest["prices"][] = $fO[key($fO)]["price"];


        }


        //define the fastest and cheapest ranges.
        $cheapestFinal = array();

        $cheapestFinal["date"]["from"] = min($cheapest["dates"]);
        $cheapestFinal["date"]["to"] = max($cheapest["dates"]);

        $cheapestFinal["price"]["from"] = min($cheapest["prices"]);
        $cheapestFinal["price"]["to"] = max($cheapest["prices"]);




        $fastestFinal = array();

        $fastestFinal["date"]["from"] = min($fastest["dates"]);
        $fastestFinal["date"]["to"] = max($fastest["dates"]);

        $fastestFinal["price"]["from"] = min($fastest["prices"]);
        $fastestFinal["price"]["to"] = max($fastest["prices"]);


        /*
        echo "<pre>";
        var_dump($fastest);
        */

        return array(
          "fastest" => $fastestFinal,
          "cheapest" => $cheapestFinal
        );


    }





    /**
     * Provides a combined view of the available arrival dates/methods for all the shipping addresses
     * in the quote
     */
    public function getAvailableArrivalDatesAndMethods()
    {
    	$addresses = $this->getAllShippingAddresses();
    	
    	// take the first (which should copy it)
    	// and then update that with changes based on the others
    	// really we would just remove any dates, not make dates available
    	$firstAddress = $addresses[0];
    	$combinedDatesAndMethods = $firstAddress->getAvailableArrivalDatesAndMethods();
    	
    	// start at next address
    	for ($i = 1; $i < count($addresses); $i++)
    	{
    		$compareAddress = $addresses[$i];
    		$compareDates = $compareAddress->getAvailableArrivalDatesAndMethods();
    		
    		// we would expect them to have the same dates...
    		$keys = array_keys($combinedDatesAndMethods);
    		
    		foreach ($keys as $key)
    		{
                $combinedAvailable = $combinedDatesAndMethods[$key]['available'];
                $compareAvailable = $compareDates[$key]['available'];
    			if ($combinedAvailable && !$compareAvailable)
				{
					// replace with what's in the other address
					$combinedDatesAndMethods[$key] = $compareAddress[$key];    					
    			}
                else if ($combinedAvailable && $compareAvailable &&
                    ($compareDates[$key]['price'] > $combinedDatesAndMethods[$key]['price']))
                {
                    // make sure we show the most expensive option
                    $combinedDatesAndMethods[$key] = $compareDates[$key];
                }
    		}
    	}

    	return $combinedDatesAndMethods;
    }

    /*
     * Figures out the allocated quantities for each item in multiship
     *
     * @param bool $reset
     */
    public function doMultishipAllocations($reset = false)
    {
        if (!$this->getIsMultiShipping())
        {
            return $this;
        }

        if ($reset)
        {
            foreach ($this->getAllItems() as $item)
            {
                $item->setMultishippingQty(0);
            }
        }

        // loop through all items and figure out the actual allocations
        /* @var Mage_Sales_Model_Quote_Address $address */
        foreach ($this->getAllShippingAddresses() as $address)
        {
            /* @var Mage_Sales_Model_Quote_Address_Item $addressItem */
            foreach ($address->getAllItems() as $addressItem)
            {
                if ($addressItem->getQty() > 0)
                {
                    $quoteItem = $this->getItemById($addressItem->getQuoteItemId());
                    $quoteItemQty = max(0, $quoteItem->getMultishippingQty());
                    $quoteItemQty += $addressItem->getQty();
                    $quoteItem->setMultishippingQty($quoteItemQty);
                    //Mage::log("Set multishipping qty to $quoteItemQty for {$quoteItem->getId()}");
                }
            }
        }

        // set the new qtys in case some are now higher
        foreach ($this->getAllItems() as $item)
        {
            if ($item->getMultishippingQty() > $item->getQty())
            {
                // just set the QTY
                // we don't necessarily need to save; we may just be displaying
                // worst case, we set it once, and then later that gets save assuming the quote was initialized.
                $item->setQty($item->getMultishippingQty());
            }
        }

        return $this;
    }

    /**
     * Rewritten to allow us to pass the check when multiship
     * and no items allocated
     *
     * @param bool $multishipping
     * @return bool
     */
    public function validateMinimumAmount($multishipping = false)
    {
        $storeId = $this->getStoreId();
        $minOrderActive = Mage::getStoreConfigFlag('sales/minimum_order/active', $storeId);
        $minOrderMulti  = Mage::getStoreConfigFlag('sales/minimum_order/multi_address', $storeId);
        $minAmount      = Mage::getStoreConfig('sales/minimum_order/amount', $storeId);

        $multiship = $this->getIsMultiShipping();

        if (!$minOrderActive) {
            return true;
        }

        if (!$multiship)
        {
            return parent::validateMinimumAmount(false);
        }

        $effectiveMinAmount = max($minAmount, $minOrderMulti);

        $total = 0;
        /* @var Mage_Sales_Model_Quote_Item $item */
        foreach ($this->getAllItems() as $item)
        {
            $total += $item->getRowTotal();
        }

        return $total >= $effectiveMinAmount;
    }


    public function getShippingAmount()
    {
        $amount = 0;

        foreach ($this->getAllShippingAddresses() as $shippingAddress)
        {
            $amount += $shippingAddress->getShippingAmount();
        }

        return $amount;
    }

    public function getSavedQuotes($customerId = false, $includeQuotesWithOrders =  false)
    {
        $savedQuotes = Mage::getModel('sales/quote')->getCollection()
            ->addFilter('saved_quote', true);

        if($customerId)
        {
            $savedQuotes->addFilter('main_table.customer_id', $customerId);
        }

        // see if we should include ones with orders
        // we really just have to filter them out with an outer join
        if (!$includeQuotesWithOrders)
        {
            $select = $savedQuotes->getSelect();

            $select->joinLeft(
                array( 'sfo' => Mage::getSingleton('core/resource')->getTableName('sales/order'))
                , 'main_table.entity_id = sfo.quote_id'
                , array( 'created_order_id' => 'sfo.entity_id')
            );

            $select->joinLeft(
                array( 'sfqab' => Mage::getSingleton('core/resource')->getTableName('sales/quote_address'))
                , 'main_table.entity_id = sfqab.quote_id AND sfqab.address_type = "billing"'
                , array(
                    'billto_name' => 'CONCAT(sfqab.firstname, " ", sfqab.lastname)',
                    'billto_company' => 'sfqab.company'
                )
            );

            // Note that a quote could have multiple shipping records (multi-ship), we want the earliest date
            // Retrieving the earliest date in a multi-ship order has been complicated, leaving results blank
            // for multi-ship orders for now.
            $select->joinLeft(
                array( 'sfqas' => Mage::getSingleton('core/resource')->getTableName('sales/quote_address'))
                , 'main_table.entity_id = sfqas.quote_id AND main_table.is_multi_shipping = 0 AND sfqas.address_type = "shipping"'
                , array( 'planned_ship_date_shipping' => 'sfqas.planned_ship_date')
            );

            $select->joinLeft(
                array( 'cg' => Mage::getSingleton('core/resource')->getTableName('customer/customer_group'))
                , 'main_table.customer_group_id = cg.customer_group_id'
                , array( 'customer_group_code' => 'cg.customer_group_code')
            );

            $select->joinLeft(
                array( 'cn' => Mage::getSingleton('core/resource')->getTableName('frans/customerNotes'))
                , 'main_table.customer_id = cn.customer_id'
                , array( 'customer_notes' => 'cn.notes')
            );

            // To use when quote tracking is added
//            $select->joinLeft(
//                array( 'fa' => 'frans_activity')
//                , 'main_table.entity_id = fa.entity_id AND fa.entity_type = "sales/quote" AND activity_type = "quote_created" '
//                , array( 'created_by' => 'fa.created_by')
//            );

            $select->where('sfo.entity_id IS NULL');
//          $select->group('main_table.entity_id');
        }

        // for now, always order by most recent
        $savedQuotes->setOrder('updated_at');

        return $savedQuotes;
    }

    public function isLargerOrder(){
        if($this->getSubtotal() >=  Mage::getStoreConfig('frans/checkout_options/large_order_amount')){
            return true;
        } else {
            return false;
        }
    }

    public function assignCustomerWithAddressChange(
        Mage_Customer_Model_Customer    $customer,
        Mage_Sales_Model_Quote_Address  $billingAddress  = null,
        Mage_Sales_Model_Quote_Address  $shippingAddress = null
    )
    {
        if ($customer->getId()) {
            $this->setCustomer($customer);

            if (!is_null($billingAddress)) {
                $this->setBillingAddress($billingAddress);
            } else {
                $defaultBillingAddress = $customer->getDefaultBillingAddress();
                if ($defaultBillingAddress && $defaultBillingAddress->getId()) {
                    $billingAddress = Mage::getModel('sales/quote_address')
                        ->importCustomerAddress($defaultBillingAddress);
                    $this->setBillingAddress($billingAddress);
                }
            }

            $shippingAddress = Mage::getModel('sales/quote_address');

            $this->setShippingAddress($shippingAddress);
        }

        return $this;
    }


}