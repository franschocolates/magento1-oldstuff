<?php

/**
 * Extension of base Magento class to allow us to have items with quantity of 0 in multiship quotes 
 * @author goSolid
 *
 */
class GoSolid_Frans_Model_Sales_Quote_Address_Total_Subtotal
		extends Mage_Sales_Model_Quote_Address_Total_Subtotal
{
    /**
     * Collect address subtotal
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  GoSolid_Frans_Model_Sales_Quote_Address_Total_Subtotal
     */
    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
    	# we call the abstract method and not the parent,
    	# since the parent does things we don't want (remove)
        Mage_Sales_Model_Quote_Address_Total_Abstract::collect($address);
        $address->setTotalQty(0);

        $baseVirtualAmount = $virtualAmount = 0;

        /**
         * Process address items
         */
        $items = $this->_getAddressItems($address);
        foreach ($items as $item) {
            if ($this->_initItem($address, $item) && $item->getQty() > 0) {
                /**
                 * Separatly calculate subtotal only for virtual products
                 */
                if ($item->getProduct()->isVirtual()) {
                    $virtualAmount += $item->getRowTotal();
                    $baseVirtualAmount += $item->getBaseRowTotal();
                }
            }
            else
            {
            	# special logic for multishipping
            	# allow 0 in the cart
            	# so only remove if not multishipping
            	if (!$address->getQuote()->getIsMultiShipping())
            	{
            		$this->_removeItem($address, $item);
            	}
            }
        }

        $address->setBaseVirtualAmount($baseVirtualAmount);
        $address->setVirtualAmount($virtualAmount);

        /**
         * Initialize grand totals
         */
        Mage::helper('sales')->checkQuoteAmount($address->getQuote(), $address->getSubtotal());
        Mage::helper('sales')->checkQuoteAmount($address->getQuote(), $address->getBaseSubtotal());
        return $this;
    }

    protected function _initItem($address, $item)
    {

        if ($item instanceof Mage_Sales_Model_Quote_Address_Item) {
            $quoteItem = $item->getAddress()->getQuote()->getItemById($item->getQuoteItemId());
        }
        else {
            $quoteItem = $item;
        }
        $product = $quoteItem->getProduct();
        $product->setCustomerGroupId($quoteItem->getQuote()->getCustomerGroupId());

        /**
         * Quote super mode flag mean what we work with quote without restriction
         */
        if ($item->getQuote()->getIsSuperMode()) {
            if (!$product) {
                return false;
            }
        }
        else {
            if (!$product || !$product->isVisibleInCatalog()) {
                return false;
            }
        }

        $currentItem = Mage::getModel('sales/quote_item')->load($item->getQuoteItemId());

        if ($quoteItem->getParentItem() && $quoteItem->isChildrenCalculated()) {
            if($currentItem->getCustomPrice() == null) {
                $finalPrice = $quoteItem->getParentItem()->getProduct()->getPriceModel()->getChildFinalPrice(
                    $quoteItem->getParentItem()->getProduct(),
                    $quoteItem->getParentItem()->getQty(),
                    $quoteItem->getProduct(),
                    $quoteItem->getQty()
                );
                $item->setPrice($finalPrice)
                    ->setBaseOriginalPrice($finalPrice);
            }
            $item->calcRowTotal();
        } else if (!$quoteItem->getParentItem()) {

            if($currentItem->getCustomPrice() == null) {
                $finalPrice = $product->getFinalPrice($quoteItem->getQty());
                $item->setPrice($finalPrice)
                    ->setBaseOriginalPrice($finalPrice);
            }

            $item->calcRowTotal();
            $this->_addAmount($item->getRowTotal());
            $this->_addBaseAmount($item->getBaseRowTotal());
            $address->setTotalQty($address->getTotalQty() + $item->getQty());
        }

        return true;
    }

	
}