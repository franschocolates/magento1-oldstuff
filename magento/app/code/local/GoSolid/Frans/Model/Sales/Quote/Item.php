<?php

/**
 * 
 * Override of Quote Item to allow us to do 0 quantities
 * @author goSolid
 *
 */
class GoSolid_Frans_Model_Sales_Quote_Item extends Mage_Sales_Model_Quote_Item
{
	protected function _prepareQty($qty)
	{
		$qty = Mage::app()->getLocale()->getNumber($qty);
        # changed to >= 0, so we allow 0
		$qty = ($qty >= 0) ? $qty : 1;
		
		return $qty;
	}
}