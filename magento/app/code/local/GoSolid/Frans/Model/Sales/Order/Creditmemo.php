<?php

/**
 * Overrides to credit memo for Fran's
 * @author goSolid
 *
 */
class GoSolid_Frans_Model_Sales_Order_Creditmemo extends Mage_Sales_Model_Order_Creditmemo
{
	public function sendEmail($notifyCustomer = true, $comment = '')
    {
        $order = $this->getOrder();
        $storeId = $order->getStore()->getId();

        if (!Mage::helper('sales')->canSendNewCreditmemoEmail($storeId)) {
            return $this;
        }
        // Get the destination email addresses to send copies to
        $copyTo = $this->_getEmails(self::XML_PATH_EMAIL_COPY_TO);
        $copyMethod = Mage::getStoreConfig(self::XML_PATH_EMAIL_COPY_METHOD, $storeId);
        // Check if at least one recepient is found
        if (!$notifyCustomer && !$copyTo) {
            return $this;
        }

        // Start store emulation process
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);

        try {
            // Retrieve specified view block from appropriate design package (depends on emulated store)
            $paymentBlock = Mage::helper('payment')->getInfoBlock($order->getPayment())
                ->setIsSecureMode(true);
            $paymentBlock->getMethod()->setStore($storeId);
            $paymentBlockHtml = $paymentBlock->toHtml();
        } catch (Exception $exception) {
            // Stop store emulation process
            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
            throw $exception;
        }
        
        // Retrieve corresponding email template id and customer name
        if ($order->getCustomerIsGuest()) {
            $templateId = Mage::getStoreConfig(self::XML_PATH_EMAIL_GUEST_TEMPLATE, $storeId);
            $customerName = $order->getBillingAddress()->getName();
        	$orderSummaryHtml		= Mage::app()->getLayout()->createBlock('frans/fransemail_order')->setTemplate('fransemail/order/summary/order_summary.phtml')->setOrder($order)->setIsGuest(true)->setParentTemplate($templateId)->toHtml();
        } else {
            $templateId = Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE, $storeId);
            $customerName = $order->getCustomerName();
        	$orderSummaryHtml		= Mage::app()->getLayout()->createBlock('frans/fransemail_order')->setTemplate('fransemail/order/summary/order_summary.phtml')->setOrder($order)->setIsGuest(false)->setParentTemplate($templateId)->toHtml();
        }

	    $contactDetailsHtml = Mage::app()->getLayout()->createBlock('frans/page_contactdetails')->setTemplate('page/contact-details.phtml')->setUseEmailFormatting(true)->toHtml();
        
        // Stop store emulation process
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);

        $mailer = Mage::getModel('core/email_template_mailer');
        if ($notifyCustomer) {
            $emailInfo = Mage::getModel('core/email_info');
            $emailInfo->addTo($order->getCustomerEmail(), $customerName);
            if ($copyTo && $copyMethod == 'bcc') {
                // Add bcc to customer email
                foreach ($copyTo as $email) {
                    $emailInfo->addBcc($email);
                }
            }
            $mailer->addEmailInfo($emailInfo);
        }

        // Email copies are sent as separated emails if their copy method is 'copy' or a customer should not be notified
        if ($copyTo && ($copyMethod == 'copy' || !$notifyCustomer)) {
            foreach ($copyTo as $email) {
                $emailInfo = Mage::getModel('core/email_info');
                $emailInfo->addTo($email);
                $mailer->addEmailInfo($emailInfo);
            }
        }

        // Set all required params and send emails
        $mailer->setSender(Mage::getStoreConfig(self::XML_PATH_EMAIL_IDENTITY, $storeId));
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId($templateId);
        $mailer->setTemplateParams(array(
                'order'        	    => $order,
                'creditmemo'   	    => $this,
                'comment'      	    => $comment,
                'billing'      	    => $order->getBillingAddress(),
                'payment_html'	    => $paymentBlockHtml,
        		'order_summary'     => $orderSummaryHtml,
		        'contact_details'   => $contactDetailsHtml
            )
        );
        $mailer->send();
        $this->setEmailSent(true);
        $this->_getResource()->saveAttribute($this, 'email_sent');

        return $this;
    }
    
     /**
     * Send email with creditmemo update information
     *
     * @param boolean $notifyCustomer
     * @param string $comment
     * @return Mage_Sales_Model_Order_Creditmemo
     */
    public function sendUpdateEmail($notifyCustomer = true, $comment = '')
    {
        $order = $this->getOrder();
        $storeId = $order->getStore()->getId();

        if (!Mage::helper('sales')->canSendCreditmemoCommentEmail($storeId)) {
            return $this;
        }
        // Get the destination email addresses to send copies to
        $copyTo = $this->_getEmails(self::XML_PATH_UPDATE_EMAIL_COPY_TO);
        $copyMethod = Mage::getStoreConfig(self::XML_PATH_UPDATE_EMAIL_COPY_METHOD, $storeId);
        // Check if at least one recepient is found
        if (!$notifyCustomer && !$copyTo) {
            return $this;
        }

        // Start store emulation process
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);

        // Retrieve corresponding email template id and customer name
        if ($order->getCustomerIsGuest()) {
            $templateId = Mage::getStoreConfig(self::XML_PATH_UPDATE_EMAIL_GUEST_TEMPLATE, $storeId);
            $customerName = $order->getBillingAddress()->getName();
        	$orderSummaryHtml		= Mage::app()->getLayout()->createBlock('frans/fransemail_order')->setTemplate('fransemail/order/summary/order_summary.phtml')->setOrder($order)->setIsGuest(true)->setParentTemplate($templateId)->toHtml();
        } else {
            $templateId = Mage::getStoreConfig(self::XML_PATH_UPDATE_EMAIL_TEMPLATE, $storeId);
            $customerName = $order->getCustomerName();
        	$orderSummaryHtml		= Mage::app()->getLayout()->createBlock('frans/fransemail_order')->setTemplate('fransemail/order/summary/order_summary.phtml')->setOrder($order)->setIsGuest(false)->setParentTemplate($templateId)->toHtml();
        }

	    $contactDetailsHtml = Mage::app()->getLayout()->createBlock('frans/page_contactdetails')->setTemplate('page/contact-details.phtml')->setUseEmailFormatting(true)->toHtml();
        
        // Stop store emulation process
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);

        $mailer = Mage::getModel('core/email_template_mailer');
        if ($notifyCustomer) {
            $emailInfo = Mage::getModel('core/email_info');
            $emailInfo->addTo($order->getCustomerEmail(), $customerName);
            if ($copyTo && $copyMethod == 'bcc') {
                // Add bcc to customer email
                foreach ($copyTo as $email) {
                    $emailInfo->addBcc($email);
                }
            }
            $mailer->addEmailInfo($emailInfo);
        }

        // Email copies are sent as separated emails if their copy method is 'copy' or a customer should not be notified
        if ($copyTo && ($copyMethod == 'copy' || !$notifyCustomer)) {
            foreach ($copyTo as $email) {
                $emailInfo = Mage::getModel('core/email_info');
                $emailInfo->addTo($email);
                $mailer->addEmailInfo($emailInfo);
            }
        }

        // Set all required params and send emails
        $mailer->setSender(Mage::getStoreConfig(self::XML_PATH_UPDATE_EMAIL_IDENTITY, $storeId));
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId($templateId);
        $mailer->setTemplateParams(array(
                'order'             => $order,
                'creditmemo'        => $this,
                'comment'           => $comment,
                'billing'           => $order->getBillingAddress(),
        		'order_summary'	    => $orderSummaryHtml,
		        'contact_details'   => $contactDetailsHtml
            )
        );
        $mailer->send();

        return $this;
    }
    
	/**
	 * Figures out whether we need to update a shipment to set to closed
	 * once all of its items have been refunded
	 * @see Mage_Sales_Model_Order_Creditmemo::_beforeSave()
	 */
	protected function _beforeSave()
	{
		parent::_beforeSave();
		
		# only care if it's a parent and this is a new Creditmemo
		# if it's not new or not multiship, doesn't matter
		if (!$this->getId() && $this->getOrder()->getIsMultishipParent())
		{
			# store an array of order ids/items to check
			$refundedOrderItems = array();
			
			# first check all of the items we have attached
			# since they are loaded
	        foreach ($this->getAllItems() as $item) {
	            if ($item->getQty()>0 && $orderItem = $item->getOrderItem()) {
	            	$childOrderId = $orderItem->getOrderId();
	            	
	            	$itemsForOrder = isset($refundedOrderItems[$childOrderId]) ? 
	            						$refundedOrderItems[$childOrderId] : array();
	            	
	            	# prior to save the items would have been updated with the new counts
	            	# so we just use those versions
					$itemsForOrder[$orderItem->getId()] = $orderItem;
					$refundedOrderItems[$childOrderId] = $itemsForOrder;
	            }
	        }
	        
	        # now loop through these by order ID and figure out what to do
	        $childOrders = $this->getOrder()->getKeyedChildOrders();
	        foreach ($refundedOrderItems as $childOrderId => $refundedItems)
	        {
	        	if ($childOrder = $childOrders[$childOrderId])
	        	{
	        		$childOrder->checkStateOnRefund($refundedItems);
	        	}
	        	else
	        	{
	        		Mage::throwException('Cannot find child order for ID ' . $childOrderId . '; unable to determine order state, aborting credit memo save.');
	        	}
	        }
		}
	
		return $this;
	}
    
}
