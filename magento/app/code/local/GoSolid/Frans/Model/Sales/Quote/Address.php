<?php

/**
 * Class GoSolid_Frans_Model_Sales_Quote_Address
 *
 * @method DateTime getPreferredArrivalDate()
 * @method GoSolid_Frans_Model_Sales_Quote_Address setPreferredArrivalDate(DateTime $value)
 * @method string getSpecialInstructions()
 * @method GoSolid_Frans_Model_Sales_Quote_Address setSpecialInstructions(string $value)
 * @method string getGiftMessage()
 * @method GoSolid_Frans_Model_Sales_Quote_Address setGiftMessage(string $value)
 * @method string getProposedAddress()
 * @method GoSolid_Frans_Model_Sales_Quote_Address setProposedAddress(string $value)
 */
class GoSolid_Frans_Model_Sales_Quote_Address extends Mage_Sales_Model_Quote_Address
{
	const DATE_FORMAT = 'Y-m-d';

    const COMMERCIAL_ADDRESS_TYPE = 'BUSINESS';
	const RESIDENTIAL_ADDRESS_TYPE = 'RESIDENTIAL';
    const UNKNOWN_ADDRESS_TYPE = 'UNDETERMINED';

	private $_arrivalDatesAndMethods = null;

	public function addItem(Mage_Sales_Model_Quote_Item_Abstract $item, $qty=null)
	{
		parent::addItem($item, $qty);

		$this->_clearCachedItems();

		return $this;
	}

	public function removeItem($itemId)
	{
		parent::removeItem($itemId);

		$this->_clearCachedItems();

		return $this;
	}

    /**
     * Get all available address items
     * 	- overridden to take into account having no items in multiship
     *
     * @return array
     */
    public function getAllItems()
    {
        // We calculate item list once and cache it in three arrays - all items, nominal, non-nominal
        $cachedItems = $this->_nominalOnly ? 'nominal' : ($this->_nominalOnly === false ? 'nonnominal' : 'all');
        $key = 'cached_items_' . $cachedItems;
        if (!$this->hasData($key)) {
            // For compatibility  we will use $this->_filterNominal to divide nominal items from non-nominal
            // (because it can be overloaded)
            // So keep current flag $this->_nominalOnly and restore it after cycle
            $wasNominal = $this->_nominalOnly;
            $this->_nominalOnly = true; // Now $this->_filterNominal() will return positive values for nominal items

            $quoteItems = $this->getQuote()->getItemsCollection();
            $addressItems = $this->getItemsCollection();

            $items = array();
            $nominalItems = array();
            $nonNominalItems = array();
            if ($this->getQuote()->getIsMultiShipping()) {
                /* @var Mage_Sales_Model_Quote_Address_Item $aItem */
                foreach ($addressItems as $aItem) {
                    if ($aItem->isDeleted()) {
                        continue;
                    }

                    if (!$aItem->getQuoteItemImported()) {
                        $qItem = $this->getQuote()->getItemById($aItem->getQuoteItemId());
                        if ($qItem) {
                            $aItem->importQuoteItem($qItem);
                        }
                    }
                    $items[] = $aItem;
                    if ($this->_filterNominal($aItem)) {
                        $nominalItems[] = $aItem;
                    } else {
                        $nonNominalItems[] = $aItem;
                    }
                }
            } else {
                /*
                * For virtual quote we assign items only to billing address, otherwise - only to shipping address
                */
                $addressType = $this->getAddressType();
                $canAddItems = $this->getQuote()->isVirtual()
                    ? ($addressType == self::TYPE_BILLING)
                    : ($addressType == self::TYPE_SHIPPING);

                if ($canAddItems) {
                    foreach ($quoteItems as $qItem) {
                        if ($qItem->isDeleted()) {
                            continue;
                        }
                        $items[] = $qItem;
                        if ($this->_filterNominal($qItem)) {
                            $nominalItems[] = $qItem;
                        } else {
                            $nonNominalItems[] = $qItem;
                        }
                    }
                }
            }

            // Cache calculated lists
            $this->setData('cached_items_all', $items);
            $this->setData('cached_items_nominal', $nominalItems);
            $this->setData('cached_items_nonnominal', $nonNominalItems);

            $this->_nominalOnly = $wasNominal; // Restore original value before we changed it
        }

        $items = $this->getData($key);
        return $items;
    }

    /**
     * Validate minimum amount
     *  Overridden so that we can evaluate against the subtotal with no discount
     *  Rather than with a discount
     *
     * @return bool
     */
    public function validateMinimumAmount($multishipping = false)
    {
        $storeId = $this->getQuote()->getStoreId();
        $storeConfigPath = 'sales/minimum_order/' . (($multishipping) ? 'multi_address' : 'active');
        if (!Mage::getStoreConfigFlag($storeConfigPath, $storeId)) {
            return true;
        }

        if ($this->getQuote()->getIsVirtual() && $this->getAddressType() == self::TYPE_SHIPPING) {
            return true;
        }
        elseif (!$this->getQuote()->getIsVirtual() && $this->getAddressType() != self::TYPE_SHIPPING) {
            return true;
        }

        $amount = Mage::getStoreConfig('sales/minimum_order/amount', $storeId);

        /* GS - only change here - use subtotal, not subtotal with discount */
        if ($this->getBaseSubtotal() < $amount) {
            return false;
        }
        return true;
    }

    /**
     *
     * Determines the available arrival dates/methods for an address
     */
    public function getAvailableArrivalDatesAndMethods($stopForCheapest = false)
    {
    	if ($this->_arrivalDatesAndMethods == null)
    	{
	    	$methodsAndTimesArray = $this->getShippingZoneMethodTimesArray();
            $cheapestMethodNames = array();
	    	$this->_appendPriceToShipMethodAndTimes($methodsAndTimesArray, $cheapestMethodNames);
	    	$shippingBlackoutDates = $this->_getBlackedOutShippingDates();

	    	$specialArrivalDates = $this->_getSpecialArrivalDates();
	    	$lastSpecialDate = key(array_slice($specialArrivalDates, -1, 1, TRUE));

            if (!$lastSpecialDate)
            {
                $lastSpecialDate = $this->_getDefaultBlackoutDate();
            }

	    	// get the effective ship date and default unavailable message from fran's shipping config
	    	$fransShipping = Mage::getModel('frans/shipping_carrier_frans');
	    	$earliestShipDate = $fransShipping->getEffectiveShipDate();
	    	Mage::log("Cutoff date/time: {$earliestShipDate->format('Y-m-d H:i')}");
	    	$defaultUnavailableMessage = $fransShipping->getDefaultShippingUnavailableMessage();
	    	$defaultMessage = $fransShipping->getDefaultMessage();

            // variables to track that we have the cheapest/fastest days identified
            $haveFastest = false;
            $haveCheapest = false;

	    	$arrivalDatesAndMethods = array();

			$todayTimestamp = Mage::getModel('core/date')->timestamp(time());
			// use effective ship date, so that we start with tomorrow.
			// but set the time to 8 pm, which should be late enough.
			$date = new DateTime();
			$date->setTimestamp($todayTimestamp);

	    	do
	    	{
	    		$date->modify('+1 day');

	    		$result = array();
	    		$dateString = $date->format(self::DATE_FORMAT);

	    		// see if manually excluded
	    		if (array_key_exists($dateString, $specialArrivalDates) && !$specialArrivalDates[$dateString]['available'])
	    		{
	    			$blackoutInfo = $specialArrivalDates[$dateString];
	    			$result = array(
	    						'available' => 0,
	    						'message_id' => $blackoutInfo['id'],
	    						'message_text' => $blackoutInfo['message']
	    			);
	    		}
	    		else
	    		{
	    			$cheapestMethod = $this->_cheapestShipMethod($shippingBlackoutDates, $methodsAndTimesArray, $date, $earliestShipDate);

	    			if ($cheapestMethod)
	    			{
                        // we could have a special message from the special arrival dates
                        $message_id = (isset($specialArrivalDates[$dateString])) ?
                                        $specialArrivalDates[$dateString]['id']
                                        : $defaultMessage->getId();
	    				$result = array(
	    						'available' => 1,
	    						'shipping_method' => $cheapestMethod['shipping_method'],
	    						'message_id' => $message_id,
	    						'price' => $cheapestMethod['price'],
                                'fastest' => false,
                                'cheapest' => false
	    				);

                        // fastest is really just first entry
                        if (!$haveFastest)
                        {
                            $haveFastest = true;
                            $result['fastest'] = true;
                        }

                        // if we haven't identified the cheapest yet and this method
                        // matches the method we know as the cheapest, then we have found our cheapest.
                        if (!$haveCheapest && in_array($cheapestMethod['shipping_method'], $cheapestMethodNames))
                        {
                            $haveCheapest = true;
                            $result['cheapest'] = true;
                        }
                    }
	    			else
	    			{
	    				$result = array( 'available' => 0,
	    								'message_id' => $defaultUnavailableMessage->getId(),
	    								'message_text' => $defaultUnavailableMessage->getMessageText()
	    				);
	    			}
	    		}

	    		$arrivalDatesAndMethods[$date->format(self::DATE_FORMAT)] = $result;

                if ($haveCheapest && $stopForCheapest)
                {
                    break;
                }


	    	}
	    	while ($date->format(self::DATE_FORMAT) != $lastSpecialDate);


	    	$this->_arrivalDatesAndMethods = $arrivalDatesAndMethods;

    	}

    	return $this->_arrivalDatesAndMethods;
    }

    /**
     * @return bool|GoSolid_Frans_Model_Mysql4_ShippingZoneMethodTime_Collection
     */
    public function getShippingZoneMethodTimesArray()
    {
        $arrayShippingZoneMethodTimes = array();

        /* @var GoSolid_Frans_Model_ShippingZone $shippingZone */
        $shippingZones = Mage::getModel('frans/shippingZone')->loadByPostcode($this->getPostcode());

        foreach($shippingZones as $shippingZone)
        {
            $arrayShippingZoneMethodTimes[] = $shippingZone->getShippingMethodTimes();
        }

        return $arrayShippingZoneMethodTimes;
    }

    public function setShippingMethodToCheapest()
    {
        $arrivalDatesAndMethods = $this->getAvailableArrivalDatesAndMethods(true);

        $cheapest = array_filter($arrivalDatesAndMethods, function($v) { return isset($v['cheapest']) && $v['cheapest']; });
        if (is_array($cheapest) && count($cheapest) > 0)
        {
            $cheapestDate = key($cheapest);
            $cheapestOption = current($cheapest);

            $this->setShippingMethod($cheapestOption['shipping_method']);

            // only set arrival date if not already present
            if (!$this->getPreferredArrivalDate())
            {
                $this->setPreferredArrivalDate($cheapestDate);
            }
        }
        else
        {
            Mage::log("No cheapest option, not setting dates.");
        }
    }

    /**
     * Import quote address data from customer address object
     *
     * @param   Mage_Customer_Model_Address $address
     * @return  GoSolid_Frans_Model_Sales_Quote_Address
     */
    public function importCustomerAddress(Mage_Customer_Model_Address $address)
    {
    	parent::importCustomerAddress($address);

    	// also set the address type/proposed address
    	// as well as the location hash, and the DOB
    	$this->setAddressDeliveryType($address->getAddressDeliveryType());
    	$this->setProposedAddress($address->getProposedAddress());
    	$this->setLocationHash($address->getLocationHash());
    	$this->setDob($address->getDob());

        // location has changed doesn't come over automatically
        // which causes issues in other logic
        if ($address->getLocationHasChanged())
        {
            $this->setLocationHasChanged($address->getLocationHasChanged());
        }

    	// note that our customer addresses also have an email on them;
    	// it will be attached in the parent method, which checks for an email property
    	// on the customer address.
        return $this;
    }


    public function convertToCustomerAddress()
    {
        $customerAddress = null;


        //confirm that this quote address has a customer.
        if($this->getCustomerId() != null)
        {
            //get a customer address from the quote address
            $customerAddressNew = $this->exportCustomerAddress();


            //if there is already a customer address attached, then update the customer address, don't make a new one.
            $customerAddress = Mage::getModel("customer/address")->load($this->getCustomerAddressId());


            if($customerAddress->getId() != null)
            {
                $customerAddress = Mage::getModel("customer/address")->load($this->getCustomerAddressId());

                //copy the quote address into the customer address.
                $customerAddress->setAddressType($customerAddressNew->getAddressType());
                $customerAddress->setPrefix($customerAddressNew->getPrefix());
                $customerAddress->setFirstname($customerAddressNew->getFirstname());
                $customerAddress->setMiddlename($customerAddressNew->getMiddlename());
                $customerAddress->setLastname($customerAddressNew->getLastname());



                $customerAddress->setSuffix($customerAddressNew->getSuffix());
                $customerAddress->setCompany($customerAddressNew->getCompany());
                $customerAddress->setStreet($customerAddressNew->getStreetFull());
                $customerAddress->setCity($customerAddressNew->getCity());
                $customerAddress->setRegion($customerAddressNew->getRegion());
                $customerAddress->setRegionId($customerAddressNew->getRegionId());


                $customerAddress->setPostcode($customerAddressNew->getPostcode());
                $customerAddress->setCountryId($customerAddressNew->getCountryId());
                $customerAddress->setTelephone($customerAddressNew->getTelephone());



            }
            else
            {
                //make this a new address.
                $customerAddress = $customerAddressNew;
                $customerAddress->setParentId($this->getCustomerId());
            }



            $customerAddress->save();

            //also save this customer addressid back to the quote.
            $this->setCustomerAddressId($customerAddress->getId());
            $this->save();
        }



        return $customerAddress;
    }

    /**
     *
     * Requests shipping rates. Overridden for two reasons:
     *  1) so that we don't set an incorrect total amount in the event that we have an adjusted amount
     *  2) we need to specify a different package value for use by shipping rules, since gift cards shouldn't count
     *
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @return bool|void
     */
    public function requestShippingRates(Mage_Sales_Model_Quote_Item_Abstract $item = null)
    {
        // first we delegate to the "original" request shipping rates (which we have made modifications to in this class)
        $found = $this->_originalRequestShippingRates($item);

        // only need to adjust our shipping if we found a match
        // and we don't have an item
        // and we have the field present
        if ($found && !$item && $this->hasBaseShippingAmountAdjusted())
        {
            if ($this->getShippingAmount() != $this->getBaseShippingAmountAdjusted())
            {
                $this->setShippingAmount($this->getBaseShippingAmountAdjusted());
            }
        }

        return $found;
    }

    /**
     * Request shipping rates for entire address or specified address item
     * Returns true if current selected shipping method code corresponds to one of the found rates
     *  NOTE: This is pretty much the same as requestShippingRates in our base class
     *          except with special logic for gift cards
     *
     * @see Mage_Sales_Model_Quote_Address::requestShippingRates
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @return bool
     */
    public function _originalRequestShippingRates(Mage_Sales_Model_Quote_Item_Abstract $item = null)
    {
        /** @var $request Mage_Shipping_Model_Rate_Request */
        $request = Mage::getModel('shipping/rate_request');
        $request->setAllItems($item ? array($item) : $this->getAllItems());
        $request->setDestCountryId($this->getCountryId());
        $request->setDestRegionId($this->getRegionId());
        $request->setDestRegionCode($this->getRegionCode());
        /**
         * need to call getStreet with -1
         * to get data in string instead of array
         */
        $request->setDestStreet($this->getStreet(-1));
        $request->setDestCity($this->getCity());
        $request->setDestPostcode($this->getPostcode());

        /* GS - Begin changes for gift cards */
        $giftcardTotal = $this->_getGiftCardTotals(false);
        $discountedGiftcardTotal = $this->_getGiftCardTotals(true);
        // adjust to not include gift card in amount
        $request->setPackageValue($item ? $item->getBaseRowTotal() : ($this->getBaseSubtotal() - $giftcardTotal));
        $packageValueWithDiscount = $item
            ? $item->getBaseRowTotal() - $item->getBaseDiscountAmount()
            : ($this->getBaseSubtotalWithDiscount() - $discountedGiftcardTotal);
        $request->setPackageValueWithDiscount($packageValueWithDiscount);
        /* END GS changes */

        $request->setPackageWeight($item ? $item->getRowWeight() : $this->getWeight());
        $request->setPackageQty($item ? $item->getQty() : $this->getItemQty());

        /**
         * Need for shipping methods that use insurance based on price of physical products
         */
        $packagePhysicalValue = $item
            ? $item->getBaseRowTotal()
            : $this->getBaseSubtotal() - $this->getBaseVirtualAmount();
        $request->setPackagePhysicalValue($packagePhysicalValue);

        $request->setFreeMethodWeight($item ? 0 : $this->getFreeMethodWeight());

        /**
         * Store and website identifiers need specify from quote
         */
        /*$request->setStoreId(Mage::app()->getStore()->getId());
        $request->setWebsiteId(Mage::app()->getStore()->getWebsiteId());*/

        $request->setStoreId($this->getQuote()->getStore()->getId());
        $request->setWebsiteId($this->getQuote()->getStore()->getWebsiteId());
        $request->setFreeShipping($this->getFreeShipping());
        /**
         * Currencies need to convert in free shipping
         */
        $request->setBaseCurrency($this->getQuote()->getStore()->getBaseCurrency());
        $request->setPackageCurrency($this->getQuote()->getStore()->getCurrentCurrency());
        $request->setLimitCarrier($this->getLimitCarrier());

        $request->setBaseSubtotalInclTax($this->getBaseSubtotalInclTax());

        $result = Mage::getModel('shipping/shipping')->collectRates($request)->getResult();

        $found = false;
        if ($result) {
            $shippingRates = $result->getAllRates();

            foreach ($shippingRates as $shippingRate) {
                $rate = Mage::getModel('sales/quote_address_rate')
                    ->importShippingRate($shippingRate);
                if (!$item) {
                    $this->addShippingRate($rate);
                }

                if ($this->getShippingMethod() == $rate->getCode()) {
                    if ($item) {
                        $item->setBaseShippingAmount($rate->getPrice());
                    } else {
                        /**
                         * possible bug: this should be setBaseShippingAmount(),
                         * see Mage_Sales_Model_Quote_Address_Total_Shipping::collect()
                         * where this value is set again from the current specified rate price
                         * (looks like a workaround for this bug)
                         */
                        $this->setShippingAmount($rate->getPrice());
                    }

                    $found = true;
                }
            }
        }
        return $found;
    }


    /**
     *
     * Returns whether the address is a known residential address
     */
    public function getIsResidential()
    {
    	// only residential if the property is populated and
    	// if it matches our residential value. Note we do a case insensitive compare
    	return $this->hasData('address_delivery_type') &&
    		   (strcasecmp($this->getAddressDeliveryType(), self::RESIDENTIAL_ADDRESS_TYPE) == 0);
    }

    public function hasSuggestedAddressChanges()
    {
        return Mage::getModel('frans/shipping_addressValidator')->doesAddressHaveActualChanges($this);
    }

	/**
	 * Called before the address is saved. Updates the location hash & notes whether
	 * it has changed
	 * @see Mage_Customer_Model_Address_Abstract::_beforeSave()
	 */
	protected function _beforeSave()
	{
		$newHash = Mage::helper('frans')->calculateLocationHash($this);
		$currentHash = $this->getLocationHash();

		if ($newHash != $currentHash)
		{
			$this->setLocationHasChanged(true);
			$this->setLocationHash($newHash);
		}

		parent::_beforeSave();

		// all quote addresses of type "shipping" for Fran's are marked NOT same as billing
		if ($this->getAddressType() == Mage_Sales_Model_Quote_Address::TYPE_SHIPPING){
			$this->setSameAsBilling(0);
		}

		return $this;
	}

	protected function _clearCachedItems()
	{
		# clear cache
		if ($this->hasData('cached_items_all'))
		{
        	$this->unsetData('cached_items_all');
        	$this->unsetData('cached_items_nominal');
        	$this->unsetData('cached_items_nonnominal');
		}
	}

	/**
	 *
	 * Returns the cheapest ship method for a specific date based on the
	 * shipping days that are blacked out and the available methods/prices
	 *
	 * @param $shippingBlackoutDates array	dates that shipping cannot take place
	 * @param $methodsAndTimes	GoSolid_Frans_Model_ShippingBlackoutDate_Mysql4_Collection
	 * @param $date	DateTime
	 * @param $cutoffDateTime DateTime
	 */
	protected function _cheapestShipMethod($shippingBlackoutDates, $methodsAndTimesArray, $date, $cutoffDateTime)
	{
		$availableMethods = array();

        foreach($methodsAndTimesArray as $methodsAndTimes ) {


            /* @var GoSolid_Frans_Model_ShippingZoneMethodTime $shipMethodTime */
            foreach ($methodsAndTimes as $shipMethodTime) {
                // first, check to make sure that we can deliver on that day with that method
                if (stripos($shipMethodTime->getDeliveryDays(), $date->format('w')) === false) {
                    continue;
                }

                // make sure we can deliver to this address.
                if (!$shipMethodTime->canDeliverTo($this)) {
                    continue;
                }

                // if we don't have a price, that means it shouldn't be here
                // exit.
                if (!$shipMethodTime->hasPrice()) {
                    continue;
                }

                $checkDate = clone $date;

                // go back in time the number of days it takes to send via this method
                $checkDate->modify('-' . $shipMethodTime->getTimeInDays() . ' day');
                // if not in the past, or in the shipping blackout date, can use it.
                if ($checkDate->getTimestamp() > $cutoffDateTime->getTimestamp() &&
                    !in_array($checkDate->format(self::DATE_FORMAT), $shippingBlackoutDates)
                ) {
                    // we put in the price as the value; this will be sorted below using asort
                    $availableMethods[$shipMethodTime->getShippingMethod()] =
                        $shipMethodTime->getPrice();
                }
            }
        }

		if (count($availableMethods) > 0)
		{
			asort($availableMethods);

			$method = key($availableMethods);
			$price = $availableMethods[$method];

			return array('shipping_method' => $method, 'price' => $price );
		}

		return false;
	}

	/**
	 *
	 * Determines the last available arrival date, based on what is blacked out
	 * Starts at the last blackout date and walks backward until it finds a date that is not blacked out.
	 * @param unknown_type $blackoutDates
	 */
	protected function _getLastAvailableArrivalDate($blackoutDates)
	{
		$dates = array_keys($blackoutDates);

		# start at last one.
		$index = count($dates) - 1;

		do
		{
			$date = new DateTime($dates[$index]);
			$date->modify('-1 day');
			$prevDay = $date->format(self::DATE_FORMAT);

			// we might not need this, but makes the loop work
			$index--;
		}
		while (in_array($prevDay, $dates));

		return $prevDay;
	}

	protected function _getBlackedOutShippingDates()
	{
		$futureDates = Mage::getModel('frans/shippingBlackoutDate')->getFutureDates();
		$blackoutDates = array();

		foreach ($futureDates as $futureDate)
		{
			$date = new DateTime($futureDate->getBlackoutDate());
			$blackoutDates[] = $date->format(self::DATE_FORMAT);
		}

		return $blackoutDates;
	}

	/**
	 *
	 * Builds an array with all applicable blackout dates. Key is the date
	 * in the DATE_FORMAT, for searching. value is an array with id/message
	 *
	 * return array
	 */
	protected function _getSpecialArrivalDates()
	{
		$allFutureDates = Mage::getModel('frans/futureShipDate')->getFutureDates($this);
		$futureDates = array();

        /* @var GoSolid_Frans_Model_FutureShipDate $futureDate */
		foreach ($allFutureDates as $futureDate)
		{
			$date = new DateTime($futureDate->getCalendarDate());
            $futureDates[$date->format(self::DATE_FORMAT)] =
				array(
                    'id' => $futureDate->getMessageId()
                    , 'message' => $futureDate->getMessage()
                    , 'available' => $futureDate->isAvailable()
                );
		}

		return $futureDates;
	}

	/**
	 *
	 * Iterates through the available ship method/times and adds the price from our rates
     *
     * Note that we pass back the cheapest method by reference
     * so that we know what it is when building the data
	 */
	protected function _appendPriceToShipMethodAndTimes($shipMethodAndTimesArray, &$cheapestMethod)
	{
        // we use an array in case we have a tie
        // otherwise we get really confused in cases where two things have the same rate
        $currentCheapestMethod = array();
        $currentCheapestPrice = 100000; // just a high number

        foreach($shipMethodAndTimesArray as $shipMethodAndTimes) {
            foreach ($shipMethodAndTimes as $shipMethodTime) {
                $rate = $this->getShippingRateByCode($shipMethodTime->getShippingMethod());

                if ($rate) {
                    // use magical getter.
                    $shipMethodTime->setPrice($rate->getPrice());

                    if ($rate->getPrice() < $currentCheapestPrice) {
                        // reset to only the cheapest.
                        $currentCheapestMethod = array($shipMethodTime->getShippingMethod());
                        $currentCheapestPrice = $rate->getPrice();
                    } else if ($rate->getPrice() == $currentCheapestPrice &&
                        !in_array($shipMethodTime->getShippingMethod(), $currentCheapestMethod)
                    ) {
                        // if it's just a tie, no price update, but add to our list of cheapest methods
                        $currentCheapestMethod[] = $shipMethodTime->getShippingMethod();
                    }
                } else {
                    // ok to be missing, indicates it's not valid at this point.
                    //Mage::log("Missing rate for Ship method time: " . print_r($shipMethodTime, true), Zend_Log::WARN);
                }
            }
        }

        $cheapestMethod = $currentCheapestMethod;
	}

    /**
     * Gets the default blackout date
     * Really, this should only be for testing, as the calendar needs to be built
     * but this is a sanity check to prevent it going too far out
     */
    private function _getDefaultBlackoutDate()
    {
        return date(self::DATE_FORMAT, strtotime("+6 months"));
    }

    protected function _getGiftCardTotals($withDiscount = false)
    {
        $giftcardTotal = 0;
        // sometimes need to get items from quote (but only if we don't have any directly on address)
        $items = $this->getItemsCollection()->count() > 0 ?
            $this->getItemsCollection() : $this->getQuote()->getItemsCollection();
        /** @var Mage_Sales_Model_Quote_Address_Item|Mage_Sales_Model_Quote_Item $item */
        foreach ($items as $item)
        {
            if ($item->getProduct()->getTypeInstance(true) instanceof GoSolid_Givex_Model_Catalog_Product_Type_Giftcard)
            {
                // shouldn't really be a discount on giftcards, but just in case...
                // note that there is no base on row total with discount for quote item
                $rowTotal = ($withDiscount) ? $item->getRowTotalWithDiscount() : $item->getBaseRowTotal();

                $giftcardTotal += $rowTotal;
            }
        }

        return $giftcardTotal;
    }


    /* reset all of the variables used to control the address validation screens */
    public function resetAddressValidation()
    {
        $this->setSuggestAddress(null); //reset suggestion.
        $this->setValidationAttemptCount(null);
        $this->setValidationMessage(null);
        $this->setIsValid(null);
        $this->save();
    }

}