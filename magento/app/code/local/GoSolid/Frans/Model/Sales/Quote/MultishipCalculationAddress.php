<?php

/**
 * Shell address used to calculate row totals for the order when in multiship mode
 * 	THIS IS NOT INTENDED TO OVERRIDE/REWRITE THE QUOTE ADDRESS MODEL
 * It is only for calculating totals
 * 
 * @author goSolid
 *
 */
class GoSolid_Frans_Model_Sales_Quote_MultishipCalculationAddress
		extends Mage_Sales_Model_Quote_Address
{
	public function getAllItems()
	{
		# assume we should just use all of the items
		return $this->getQuote()->getAllItems();
	}
} 