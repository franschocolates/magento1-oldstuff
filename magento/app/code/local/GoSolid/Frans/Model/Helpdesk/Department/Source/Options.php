<?php
class GoSolid_Frans_Model_Helpdesk_Department_Source_Options extends Mage_Core_Model_Abstract
{


    public function _construct()
    {
        parent::_construct();
        $this->_init('frans/helpdesk_department_source_options');
    }

    public function toOptionArray($emptyOption = false)
    {
        return Mage::helper('helpdesk')->getAdminOwnerOptionArray($emptyOption);
    }


}