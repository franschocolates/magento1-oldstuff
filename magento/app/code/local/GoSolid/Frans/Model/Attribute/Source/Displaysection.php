<?php
/**
 * Created by goSolid.
 * Date: 3/18/15
 * Time: 9:27 AM
 */

class GoSolid_Frans_Model_Attribute_Source_Displaysection extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{

	protected $_options = null;

	public function getAllOptions(){
		if(is_null($this->_options)){
			$this->_options = array();
			$layouts = Mage::helper("frans/catalog_category")->getDisplaySections();
			foreach($layouts as $key => $value){
				$this->_options[] = array('label' => $value["name"], 'value' => $key );
			}
		}
		$options = $this->_options;
		return $options;
	}

}