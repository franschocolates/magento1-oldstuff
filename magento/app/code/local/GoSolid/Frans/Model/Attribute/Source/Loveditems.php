<?php
/**
 * Created by goSolid.
 * Date: 4/27/15
 * Time: 4:38 PM
 */

class GoSolid_Frans_Model_Attribute_Source_Loveditems extends Mage_Eav_Model_Entity_Attribute_Source_Table
{

	protected $_options = null;

	// build an array of IDs for a given category and all its active children, recursively
	private function appendChildCategoryIdsRecursivelyToArray($category, $array)
	{
		$array[] = $category->getId();
		$childCategories = $category->getChildrenCategories();
		foreach($childCategories as $childCategory)
		{
			$array = $this->appendChildCategoryIdsRecursivelyToArray($childCategory, $array);
		}
		return $array;
	}

	public function getAllOptions()
	{

		$resourceModel = Mage::getResourceModel('catalog/product');
		$storeId = Mage::app()->getStore()->getId();

		if(is_null($this->_options))
		{

			$this->_options = array();
			$currentCategory = Mage::registry('current_category');

			// array of all categories to find products for, starting with the current category itself
			$categoryIds = $this->appendChildCategoryIdsRecursivelyToArray($currentCategory, array());

			$categoryProducts = Mage::getResourceModel('catalog/product_collection')
				->joinField('category_id', 'catalog/category_product', 'category_id', 'product_id = entity_id', null, 'inner')
				->addAttributeToFilter('category_id', array(
					'in' => $categoryIds
				))
				->addAttributeToFilter('status', 1)
				->addAttributeToSort('sku', 'asc')
				->groupByAttribute('entity_id');

			foreach($categoryProducts as $product){
				$this->_options[] = array(
					'label' => $resourceModel->getAttributeRawValue($product->getId(), 'sku', $storeId),
					'value' => $resourceModel->getAttributeRawValue($product->getId(), 'entity_id', $storeId)
				);
			}

		}
		$options = $this->_options;
		return $options;
	}

	public function toOptionArray($value = "id", $label = "name", $selectText = null)
	{
		$array = $this->getAllOptions();

		if($selectText != null)
		{
			$array = array('' =>  Mage::helper('frans')->__("-- %s --", $selectText)) + $array;
		}

		return $array;

	}

}