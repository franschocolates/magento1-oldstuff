<?php

/**
 * Class GoSolid_Frans_Model_RetailStore
 *
 * @method bool getUseOnFrontend()
 * @method GoSolid_Frans_Model_RetailStore setUseOnFrontend(bool $val)
 *
 */
class GoSolid_Frans_Model_RetailStore extends Mage_Core_Model_Abstract 
{
    public function _construct()
    {
        parent::_construct(); 
        $this->_init('frans/retailStore');
    }

    //returns the path relative to the media directory.
    public function getMediaPath()
    {
        return "stores";
    }

	public function getActiveStores(){
		$stores = $this->getCollection()
			->addFieldToFilter('status', 1);
		$stores->getSelect()->order('position ASC');
		return $stores;
	}
    
    public function getActiveFrontendStores(){

    	$stores = $this->getCollection()
    		->addFieldToFilter('status', 1)
    		->addFieldToFilter('use_on_frontend', 1);
    	
    	$stores->getSelect()->order('position ASC');
    	
    	return $stores;
    }
    
    // parse the store hours into an array and return it
    public function getStoreHours(){
    	$hours = $this->getData('store_hours');
    	$rows = explode("\n", $hours);

    	$output = array();
    	foreach($rows as $row){
			$cols = explode("|", $row);
			if(count($cols) == 2){
				$output[] = array(
					'days'	=> trim($cols[0]),
					'hours'	=> trim($cols[1])
				);
			}

		}
    	return $output;
    	
    }

	// return retail store address formatted like a customer address
	public function getAsCustomerAddress()
	{
		$countryId = 'US'; // hard-coded to US for now since retail store country data is saved as a string
		$region = Mage::getModel('directory/region')->loadByCode($this->getRegion(), $countryId);
		$address = Mage::getModel('customer/address')
					->setFirstname($this->getName()) // first and last name are required, so use store name as first name
					->setLastname(Mage::helper('core')->__('Retail Store')) // use hard-coded string as last name
					->setCompany('')
					->setStreet($this->getCombinedAddressLines())
					->setCity($this->getCity())
					->setCountryId($countryId)
					->setRegion($region->getName())
					->setRegionId($region->getId())
					->setPostcode($this->getPostalCode())
					->setTelephone($this->getPhone())
					->setFax('')
					->setDob('');
		return $address;
	}

	// return a string with Address 1 and Address 2 (if it exists), separated by a newline
	public function getCombinedAddressLines(){
		$output = $this->getAddress1();
		$address2 = $this->getAddress2();
		if($address2 != ''){
			$output .= PHP_EOL . $address2;
		}
		return $output;
	}

	public function getImagePath($image){
		return str_replace('\\', '/', $this->getData($image));
	}

	public function getStoreImageLarge(){
		return $this->getImagePath('store_image_large');
	}

	public function getStoreImageThumbnail(){
		return $this->getImagePath('store_image_thumbnail');
	}

	public function getStoreImageTile(){
		return $this->getImagePath('store_image_tile');
	}

	public function getStoreImageBanner(){
		return $this->getImagePath('store_image_banner');
	}

}