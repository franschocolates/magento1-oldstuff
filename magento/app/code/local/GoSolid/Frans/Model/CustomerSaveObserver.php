<?php
class GoSolid_Gstools_Model_CustomerSaveObserver extends Mage_Core_Model_Abstract
{
    public function syncCustomerNotes($observer)
    {
		try
		{
			$customer = $observer->getCustomer();
			
			# see if the customer exists in the notes
			$notesModel = Mage::getModel('frans/customerNotes');
			$notesRecordByCustomer = $notesModel->loadByCustomer($customer->getId());
			$notesRecordByEmail = $notesModel->loadByEmail($customer->getEmail());
			if ($notesRecordByCustomer && $notesRecordByCustomer->getId())
			{
				# see if the email addresses match.
				if ($notesRecordByCustomer->getEmail() != $customer->getEmail())
				{
					// update the notes record
					$notesRecordByCustomer->setEmail($customer->getEmail());
					$notesRecordByCustomer->save();
				}
			}
			else if ($notesRecordByEmail && $notesRecordByEmail->getId())
			{
				// need to set the customer ID On this one.
				$notesRecordByEmail->setCustomerId($customer->getId());
				$notesRecordByEmail->save();
			}
			else
			{
				// none exist, that's fine, do nothing.
			}
			
		}
		catch (Exception $ex)
		{
			Mage::logException($ex);
		}
    	
    }
}