<?php

class GoSolid_Frans_Model_IcePack
{
	public function getWeight()
	{
		return 0.5;		
	}

    public function getTotalIcePackWeight($count)
    {
        return $count * $this->getWeight();
    }
}