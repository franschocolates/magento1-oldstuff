<?php
/**
 * Created by goSolid.
 * Date: 9/15/14
 * Time: 1:55 PM
 */ 
class GoSolid_Frans_Model_Catalog_Observer extends Mage_Catalog_Model_Observer {

	/**
	 * Update child configurable product URLs on catalog_product_save_after event
	 *
	 * @param   Varien_Event_Observer $observer
	 * @return  GoSolid_Frans_Model_Catalog_Observer
	 */
	public function updateChildConfigurableProductUrls(Varien_Event_Observer $observer)
	{
		$product = $observer->getProduct();

        //Re-instantiate the object so that we can understand configurable-to-simple associations
        Mage::getModel('catalog/product')->load($product->getId())->updateConfigurableUrls();

		return $this;
	}

	/**
	 * Update child configurable product URLs on catalog_category_change_products event
	 *
	 * @param   Varien_Event_Observer $observer
	 * @return  GoSolid_Frans_Model_Catalog_Observer
	 */
	public function updateConfigurableProductUrlCategories(Varien_Event_Observer $observer)
	{
		// get the category and its products (collection will include new products and not removed ones)
		$category = $observer->getCategory();
		$categoryId = $category->getId();
		$categoryProducts = $category->getProductCollection();

		// get an array of product IDs that were previously assigned to the category, from our table
		$existingCategoryProductUrls = Mage::getModel('frans/configurableProductUrls')
			->getCollection()
			->addFieldToFilter('category_id', $categoryId);
		$existingCategoryProductUrls->getSelect()->group('configurable_id');
		$existingCategoryProductIds = $existingCategoryProductUrls->getColumnValues('configurable_id');

		// loop through the current category products
		foreach($categoryProducts as $product){

			// update the configurable URLs for this product
			$product->updateConfigurableUrls();

			// remove this product ID from the array of product IDs in this category before the update
			foreach($existingCategoryProductIds as $i => $existingProductId){
				if($product->getId() == $existingProductId){
					unset($existingCategoryProductIds[$i]);
				}
			}

		}

		// the remaining products IDs in the array are just the ones that were removed from this category
		foreach($existingCategoryProductIds as $existingCategoryProductId){
			// delete the configurable product URLs for this product, in only this category
			$product = Mage::getModel('catalog/product')->load($existingCategoryProductId);
			$product->deleteConfigurableUrls($categoryId);
		}

		return $this;
	}

	/**
	 * Delete child configurable product URLs on catalog_product_delete_after event
	 *
	 * @param   Varien_Event_Observer $observer
	 * @return  GoSolid_Frans_Model_Catalog_Observer
	 */
	public function deleteConfigurableProductUrlsByProduct(Varien_Event_Observer $observer)
	{
		$product = $observer->getProduct();
		$product->deleteConfigurableUrls();
		return $this;
	}

	/**
	 * Delete child configurable product URLs on catalog_category_delete_before event
	 *
	 * @param   Varien_Event_Observer $observer
	 * @return  GoSolid_Frans_Model_Catalog_Observer
	 */
	public function deleteConfigurableProductUrlsByCategory(Varien_Event_Observer $observer)
	{
		// get the category and its products
		$category = $observer->getCategory();
		$categoryId = $category->getId();
		$categoryProducts = $category->getProductCollection();

		// loop through the products and delete the configurable product URLs for each, in this category only
		foreach($categoryProducts as $categoryProduct){
			$categoryProduct->deleteConfigurableUrls($categoryId);
		}

		return $this;
	}

	/**
	 * Function override adds Tagline and Image URL to tree node data array
	 *
	 * @param Varien_Data_Tree_Node_Collection|array $categories
	 * @param Varien_Data_Tree_Node $parentCategoryNode
	 */
	protected function _addCategoriesToMenu($categories, $parentCategoryNode)
	{
		foreach ($categories as $category) {
			if (!$category->getIsActive()) {
				continue;
			}

			$categoryId = $category->getId();
			$loadedCategory = Mage::getModel('catalog/category')->load($categoryId);

			$nodeId = 'category-node-' . $categoryId;

			// check if the file exists in local media directory
			$thumbnailFile = $loadedCategory->getThumbnail();
			$thumbnailPath = Mage::getBaseDir().'/media/catalog/category/'.$thumbnailFile;
			if($thumbnailFile != '' &&  file_exists($thumbnailPath)){
				// if the file exists locally, build a URL with the base media URL (so CDN will work)
				$thumbnailUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'catalog/category/'.$thumbnailFile;
			} else {
				// TODO: Build a proper catalog thumbnail placeholder
				$thumbnailUrl = '';
			}

			$tree = $parentCategoryNode->getTree();
			$categoryData = array(
				'name'              => $category->getName(),
				'id'                => $nodeId,
				'url'               => Mage::helper('catalog/category')->getCategoryUrl($category),
				'is_active'         => $this->_isActiveMenuCategory($category),
				'tagline'           => $loadedCategory->getTagline(),
				'thumbnail'         => $thumbnailUrl,
				'display_section'   => $loadedCategory->getDisplaySection()
			);
			$categoryNode = new Varien_Data_Tree_Node($categoryData, 'id', $tree, $parentCategoryNode);
			$parentCategoryNode->addChild($categoryNode);

			// special handling for All Products category
			// don't show its children, instead use categories from list in config setting
			if($loadedCategory->getUrlKey() == 'all-products') {
				$subcategories = Mage::helper('catalog/category')->getAllProductsCategoriesArray();;
			} else {
				if (Mage::helper('catalog/category_flat')->isEnabled()) {
					$subcategories = (array)$category->getChildrenNodes();
				} else {
					$subcategories = $category->getChildren();
				}
			}

			$this->_addCategoriesToMenu($subcategories, $categoryNode);
		}
	}

	// add an error message to the session which contains the conflict details
	public function validateProductConfigurableUrlsAsMessage(Varien_Event_Observer $observer)
	{
		//get the product conflicting messages
		$conflictMessages = $this->_validateUrlsByProduct($observer->getProduct());

		if(!empty($conflictMessages))
		{
			//create messages
			$msg = Mage::helper('catalog')->__('This product has a conflicting URL. To resolve, rename this product or rename the conflicting products in the list below.');
			foreach($conflictMessages as $m)
			{
				$msg .= "<br/>" . $m;
			}

			//throw the error for the observer
			Mage::getSingleton('adminhtml/session')->addError($msg);
		}
	}

	// throw an exception containing the conflict details (this prevents the product from saving)
	public function validateProductConfigurableUrls(Varien_Event_Observer $observer)
	{

		//get the product conflicting messages
		$conflictMessages = $this->_validateUrlsByProduct($observer->getProduct());

		if(!empty($conflictMessages))
		{
			//create messages
			$msg = Mage::helper('catalog')->__('Unable to save product. This product has a conflicting URL. To resolve, rename this product or rename the conflicting products in the list below.');
			foreach($conflictMessages as $m)
			{
				$msg .= "<br/>" . $m;
			}

			//throw the error for the observer
			Mage::throwException(
				$msg
			);
		}

	}

	public function validateCategoryConfigurableUrls(Varien_Event_Observer $observer)
	{
		$category = $observer->getCategory();
		$categoryProducts = $category->getProductCollection();

		//messages array
		$conflictMessages = array();

		//get grouped messages by product and add to our messages array
		foreach($categoryProducts as $product)
		{
			array_merge($conflictMessages, $this->_validateUrlsByProduct($product));
		}

		if(!empty($conflictMessages))
		{
			//create messages
			$msg = Mage::helper('catalog')->__('Unable to save category. One or more configurable products in this category have conflicting URLs. To resolve, rename either of the products in each conflict below.');
			foreach($conflictMessages as $m)
			{
				$msg .= "<br/>" . $m;
			}

			//throw the error for the observer
			Mage::throwException(
				$msg
			);
		}
	}

	//returns an array of conflict messages
	protected function _validateUrlsByProduct($product)
	{
		//array of messages
		$conflictMessages = array();

		$urlDetails = $product->validateConfigurableUrls();
		if(!empty($urlDetails))
		{
			foreach($urlDetails as $url)
			{
				foreach($url['conflicts'][0]['items'] as $conflict)
				{

					// get ID of the conflicting product
					$conflictProductId = $conflict['configurable_id'];
					$thisProductSku = Mage::getModel('catalog/product')->load($url['product_id'])->getSku();
					$conflictProductSku = Mage::getModel('catalog/product')->load($conflictProductId)->getSku();
					$link1 = '<a target="_blank" href="' . Mage::helper('adminhtml')->getUrl('adminhtml/catalog_product/edit', array('id' => $url['product_id'])) . '">' . $thisProductSku . '</a>';
					$link2 = '<a target="_blank" href="' . Mage::helper('adminhtml')->getUrl('adminhtml/catalog_product/edit', array('id' => $conflictProductId)) . '">' . $conflictProductSku . '</a>';

					$msgLine = Mage::helper('catalog')->__('Configurable product %s has the same Product Name as %s.', $link1, $link2);

					//create duplicate keys to have assoc array auto group
					$conflictMessages[$conflictProductId . $thisProductSku] = $msgLine;

				}

			}
		}

		return $conflictMessages;
	}

}
