<?php

class GoSolid_Frans_Model_Catalog_Category extends Mage_Catalog_Model_Category
{
    /**
     * Order model
     * 	Custom logic to add large images to product collection for zoom feature
     *
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
	public function getProductCollection()
    {
        $collection = Mage::getResourceModel('catalog/product_collection')
            ->setStoreId($this->getStoreId())
            ->addCategoryFilter($this)
            ->addAttributeToSelect('image');
        return $collection;
    }

    //retrieves a list of child cross-sell categories for the current parent block
    public function getCrossellCategories($block){
        $category = $block->getCurrentCategory();
        $collection = $category->getCollection();
        /* @var $collection Mage_Catalog_Model_Resource_Category_Collection */
        $collection->addAttributeToSelect('url_key')
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('all_children')
            ->addAttributeToSelect('is_cross_sell')
            ->addAttributeToFilter('is_active', 1)
            ->addAttributeToFilter('is_cross_sell', 1)
            ->addIdFilter($category->getChildren())
            ->setOrder('position', Varien_Db_Select::SQL_ASC);

        return $collection;
    }

    /**
     * Retrieve categories by parent
     *
     * @param int $parent
     * @param int $recursionLevel
     * @param bool $sorted
     * @param bool $asCollection
     * @param bool $toLoad
     * @return mixed
     */
    public function getCategories($parent, $recursionLevel = 0, $sorted=false, $asCollection=false, $toLoad=true,$menuType='include_in_menu')
    {
        $categories = $this->getResource()
            ->getCategories($parent, $recursionLevel, $sorted, $asCollection, $toLoad,$menuType );
        return $categories;
    }

	public function getImagePath()
	{
		$imagePath = $this->hasImage() ? ($this->getAbsolutePath($this->getImage())) : null;
		return $imagePath;
	}

	public function getThumbnailPath()
	{
		$imagePath = $this->hasThumbnail() ? ($this->getAbsolutePath($this->getThumbnail())) : null;
		return $imagePath;
	}

	public function getTileImagePath()
	{
		$imagePath = $this->hasTileImage() ? ($this->getAbsolutePath($this->getTileImage())) : null;
		return $imagePath;
	}

	public function getActionImagePath()
	{
		$imagePath = $this->hasActionImage() ? ($this->getAbsolutePath($this->getActionImage())) : null;
		return $imagePath;
	}

	public function getAllProductsTileImagePath()
	{
		$imagePath = $this->hasAllProductsTileImage() ? ($this->getAbsolutePath($this->getAllProductsTileImage())) : null;
		return $imagePath;
	}

	public function getInsetBackground()
	{

		$defaultColor = 'ffffff';
		$color = '';
		$opacity = 90;
		if($this->hasInsetColor()){
			// get the hex value, convert to lowercase, and strip out invalid characters
			$color =  preg_replace("/[^0-9a-f]/", '', strtolower($this->getInsetColor()));
		}

		// validate the string is a 3- or 6-digit hexadecimal color
		$colorSize = strlen($color);
		if(ctype_xdigit($color) && ($colorSize == 6) || ($colorSize == 3)){
			// convert short-form 3-digit color to 6-digits
			if($colorSize == 3) {
				$color = str_repeat(substr($color, 0, 1), 2) . str_repeat(substr($color, 1, 1), 2) . str_repeat(substr($color, 2, 1), 2);
			}
		} else {
			// use default color
			$color = $defaultColor;
		}

		// convert hex color values to RGB array using bitwise operation
		$colorVal = hexdec($color);
		$red    = 0xFF & ($colorVal >> 0x10);
		$green  = 0xFF & ($colorVal >> 0x8);
		$blue   = 0xFF & $colorVal;

		if($this->hasInsetOpacity()){
			// get the opacity percentage as an integer
			$categoryOpacity = (int) preg_replace("/[^0-9]/", '', $this->getInsetOpacity());
			if($categoryOpacity >= 0 || $categoryOpacity <= 100){
				$opacity = $categoryOpacity;
			}
		}

		// change the percentage to a CSS-ready opacity value
		$opacity = ($opacity * 0.01);

        $IE8 = (preg_match('/MSIE 8/',$_SERVER['HTTP_USER_AGENT'])) ? true : false;
        $css = 'rgba(' . $red . ',' . $green . ',' . $blue . ',' . $opacity . ')';
        if ($IE8) {
            $opacity = ($opacity * 100);
            $css = '#'.$color.'; filter:alpha(opacity='.$opacity.')';
        }
		// return the CSS property value string
		return $css; //

	}

	// get an absolute path to a catalog category image file
	private function getAbsolutePath($filename)
	{
		return Mage::getBaseDir('media') . DS . 'catalog' . DS . 'category' . DS . $filename;
	}

}