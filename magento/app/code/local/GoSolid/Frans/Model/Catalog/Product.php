<?php
/**
 * Created by goSolid.
 * Date: 10/2/14
 * Time: 9:19 AM
 */ 
class GoSolid_Frans_Model_Catalog_Product extends Mage_Catalog_Model_Product {

	protected $_defaultChildProduct;

	public function generateConfigurableUrls()
	{

		// create the array for holding data about the URLs to be created/updated for all child products
		$urlDetails = array();

		// only proceed if product is configurable
		if($this->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE){

			// first get some info about this configurable product
			$productId = $this->getId();
			$productName = $this->getName();
			if(!$productName)
			{
				$productName = Mage::getResourceModel('catalog/product')->getAttributeRawValue($productId, 'name', Mage::app()->getDefaultStoreView());
			}

			$configurableAttributes = $this->getTypeInstance(true)->getConfigurableAttributesAsArray($this);

			// helper for generating friendly URLs
			$helper = Mage::helper('frans');

			// get an array of current product category assignments, to loop through later and create URLs
			$productCategoryIds = $this->getCategoryIds();

			// add an empty string to the array so one iteration can generate a URL without category info
			array_unshift($productCategoryIds, '');

			// get an array of this product's previous category assignments from frans_configurable_product_urls table
			$oldProductCategoryIds = Mage::getModel('frans/configurableProductUrls')
				->getCollection()
				->addFieldToFilter('configurable_id', $productId)
				->addFieldToFilter('category_id', array('notnull' => true));
			$oldProductCategoryIds->getSelect()->group('category_id');
			$oldProductCategoryIds = $oldProductCategoryIds->getColumnValues('category_id');

			// determine the category IDs that were previously assigned to this product, but are not now
			$removedCategoryIds = array_diff($oldProductCategoryIds, $productCategoryIds);

			// find all URL rewrites for this configurable product in categories it's no longer assigned to
			$rewritesToRemove = Mage::getModel('core/url_rewrite')
				->getCollection()
				->addFieldToFilter('product_id', $productId)
				->addFieldToFilter('category_id', array('in' => $removedCategoryIds));

			// delete the rewrites for unaffiliated categories, they're not needed any longer
			foreach($rewritesToRemove as $rewriteToRemove){
				$rewriteToRemove->delete();
			}

			// begin building out URL details data; loop through child products
			$childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null, $this);

			foreach($childProducts as $childProduct){

				$childProductId = $childProduct->getId();

				// loop through all stores this child product is assigned to
				$storeIds = $childProduct->getStoreIds();
				foreach($storeIds as $storeId){

					// loop through the category ID array, creating an URL detail for each
					foreach($productCategoryIds as $productCategoryId){

						// build the target path category info and request path category info for this child product, in this store
						$targetPathCategory = '';
						$requestPathCategory = '';

						// check if category ID is set (remember, an empty string indicates the new URL will contain no category info)
						if($productCategoryId){

							// look up the request path for the current category in the core_url_rewrite table
							$categoryRewrite = Mage::getModel('core/url_rewrite')->getCollection();
							$categoryRewrite->addFieldToFilter('target_path', array('eq' => 'catalog/category/view/id/' . $productCategoryId));
							$categoryRewrite = $categoryRewrite->getFirstItem();

							// set the category info for target path and request path
							$targetPathCategory = '/category/' . $productCategoryId;
							$requestPathCategory = $categoryRewrite->getRequestPath() . '/';

						}

						// build the target path query string and request path attributes for this child product, in this store
						$targetPathQueryString = '';
						$requestPathAttributes = '';

						// loop through configurable attributes to build the strings
						foreach($configurableAttributes as $attribute){

							$attributeCode = $attribute['attribute_code'];
							$value = Mage::getResourceModel('catalog/product')->getAttributeRawValue($childProductId, $attributeCode, $storeId);

							if($value){
								$delimiter = $targetPathQueryString ? '&' : '?'; // first param starts with "?" and all others use "&"
								$targetPathQueryString .= $delimiter . $attributeCode . '=' . $value;

								// loop through options for this attr to find the one used by this child product
								foreach($attribute['values'] as $option){
									if($option['value_index'] == $value){
										// get the option label, precede it with a dash, and exit the loop
										$requestPathAttributes .= '-' . $option['store_label'];
										break;
									}
								}
							}
						}

						// assemble the full target path for this child product, in this store
						$targetPath = 'catalog/product/view/id/' . $productId . $targetPathCategory . $targetPathQueryString;

						// assemble the full request path for this child product, in this store
						$requestPath =  $requestPathCategory . $helper->friendlyUrl($productName . $requestPathAttributes);

						// build the array of data needed for creating a new URL rewrite or updating an existing one
						$urlDetails[] = array(
							'store_id'      => $storeId,
							'id_path'       => uniqid() . '_' . uniqid(),
							'request_path'  => $requestPath,
							'target_path'   => $targetPath,
							'is_system'     => 0,
							'options'       => null,
							'category_id'   => $productCategoryId ? $productCategoryId : null,
							'product_id'    => $productId,
							'old_path'      => '',
							'simple_id'     => $childProductId
						);

					}

				}

			}
		}
		return $urlDetails;
	}

	// return an array of configurable product URLs that conflict with this one's name and child products
	public function validateConfigurableUrls()
	{
		$conflicts = array();
		$urlDetails = $this->generateConfigurableUrls();
		foreach($urlDetails as $url)
		{
			$existingUrls = Mage::getModel('frans/configurableProductUrls')
				->getCollection()
				->addFieldToFilter('path', $url['request_path'])
				->addFieldToFilter('configurable_id', array('neq' => $url['product_id']));
			if($existingUrls->count() > 0)
			{
				$url['conflicts'] = array($existingUrls->toArray());
				$conflicts[] = $url;
			}
		}
		return $conflicts;
	}

	/**
	 * Update configurable product URLs (does nothing if called on a simple product)
	 *
	 * @return  GoSolid_Frans_Model_Catalog_Product
	 */
	public function updateConfigurableUrls()
	{

		$productId = $this->getId();

		// only proceed if product is configurable
		if($this->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE){

			$urlDetails = $this->generateConfigurableUrls();

			// now that the array of URLs to create/update has been built, loop through it
			foreach($urlDetails as $url){

				$doCreateNew = true;

				// look for an existing URL in frans_configurable_product_urls table matching category, configurable ID and simple ID
				$categoryFilter = is_null($url['category_id']) ? array('null' => true) : $url['category_id'];

				$oldConfigurableUrl = Mage::getModel('frans/configurableProductUrls')
					->getCollection()
					->addFieldToFilter('configurable_id', $url['product_id'])
					->addFieldToFilter('simple_id', $url['simple_id'])
					->addFieldToFilter('category_id', $categoryFilter)
					->getFirstItem();

				// if an existing one was found, note its path
				if($oldConfigurableUrl->getId())
				{
					$url['old_path'] = $oldConfigurableUrl->getPath();
				}

				// load the existing rewrite if it already exists
				$urlRewrite = Mage::getModel('core/url_rewrite')
					->getCollection()
					->addFieldToFilter('request_path', array('eq' => $url['request_path']))
					->getFirstItem();

				// if an existing rewrite was found, compare it to the new one to see if it has changed
				if($urlRewrite->getUrlRewriteId()){
					$existingHash = md5($urlRewrite->getRequestPath() . $urlRewrite->getTargetPath());
					$newHash = md5($url['request_path'] . $url['target_path']);
					if($existingHash == $newHash){
						// no change; set the flag so a new rewrite will not be created
						$doCreateNew = false;

					}
				}

				if($doCreateNew){

					// if attempting to recreate a URL rewrite that existed once before, delete the old rewrite first
					$urlRewriteToDelete = Mage::getModel('core/url_rewrite')
						->getCollection()
						->addFieldToFilter('request_path', array('eq' => $url['request_path']))
						->getFirstItem();
					if($urlRewriteToDelete->getUrlRewriteId())
					{
						$urlRewriteToDelete->delete();
					}

					// create the new URL rewrite
					$urlRewrite->setData($url);
					$urlRewrite->save();

					// get the rewrite to change to a 301 redirect, if one exists
					$oldUrlRewrite = Mage::getModel('core/url_rewrite')
						->getCollection()
						->addFieldToFilter('request_path', $url['old_path'])
						->getFirstItem();

					// if that rewrite was found, change it to a 301 with a target path equal to the new URL request path
					if($oldUrlRewrite->getUrlRewriteId())
					{
						// quick sanity check to prevent creating a redirect loop
						if($oldUrlRewrite->getRequestPath() != $url['request_path'])
						{
							$oldUrlRewrite->setOptions('RP')
								->setTargetPath($url['request_path'])
								->save();
						}
					}

				}

			}

			// delete all existing records for this configurable product from the frans_configurable_product_urls table
			$productUrls = Mage::getModel('frans/configurableProductUrls')->getCollection();
			$productUrls->addFieldToFilter('configurable_id', $productId);
			foreach($productUrls as $productUrl){
				$productUrl->delete();
			}

			// save the configurable URLs in the frans_configurable_product_urls table
			foreach($urlDetails as $url){
				Mage::getModel('frans/configurableProductUrls')
					->setConfigurableId($url['product_id'])
					->setSimpleId($url['simple_id'])
					->setCategoryId($url['category_id'])
					->setPath($url['request_path'])
					->save();
			}

		}

		return $this;

	}

	/**
	 * Delete all configurable product URLs for this product (does nothing if called on a simple product).
	 * Provide a category ID to limit deletion to the URLs associated with that category.
	 *
	 * @param   int $categoryId
	 * @return  GoSolid_Frans_Model_Catalog_Product
	 */
	public function deleteConfigurableUrls($categoryId = null)
	{

		// only proceed if product is configurable
		if($this->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE){

			$productId = $this->getId();

			// get a collection of configurable product URLs from our table, for this product
			$configurableProductUrls = Mage::getModel('frans/configurableProductUrls')
				->getCollection()
				->addFieldToFilter('configurable_id', $productId);

			// apply the category filter to the configurable product URL collection if a category ID was provided
			if($categoryId){
				$configurableProductUrls->addFieldToFilter('category_id', $categoryId);
			}

			// delete those configurable product URLs
			foreach($configurableProductUrls as $configurableProductUrl){
				$configurableProductUrl->delete();
			}

			// get a collection of non-system core URL rewrites for this product
			$urlRewrites = Mage::getModel('core/url_rewrite')
				->getCollection()
				->addFieldToFilter('is_system', 0)
				->addFieldToFilter('product_id', $productId);

			// apply the category filter to the rewrites collection if a category ID was provided
			if($categoryId){
				$urlRewrites->addFieldToFilter('category_id', $categoryId);
			}

			// delete the rewrites
			foreach($urlRewrites as $urlRewrite){
				$urlRewrite->delete();
			}

		}

		return $this;

	}

	/**
	 * Get the path to the product details page view.
	 * Provide a configurable product ID to get a configurable product path.
	 *
	 * @param   int $configurableProductId
	 * @return  string
	 */
	public function getProductPath($configurableProductId = null)
	{

		$productPath = null;

		// don't do anything else unless a configurable product ID was provided
		if($configurableProductId){

			$categoryId = $this->getCategoryId();

			// get all the configurable product URLs for this simple/configurable product combination
			// there should only be one for each category, plus one without a category (category_id == null)
			$configurableProductUrls = Mage::getModel('frans/configurableProductUrls')
				->getCollection()
				->addFieldToFilter('configurable_id', $configurableProductId)
				->addFieldToFilter('simple_id', $this->getId());

			// loop through them
			foreach($configurableProductUrls as $configurableProductUrl){

				$configurableProductUrlCategoryId = $configurableProductUrl->getCategoryId();

				// look for either a matching category ID (preferred), or a null category ID (fallback)
				if(!$configurableProductUrlCategoryId || ($configurableProductUrlCategoryId == $categoryId)){

					// if either of those was found, remember its path
					$productPath = $configurableProductUrl->getPath();

					// if it was a category ID match, stop looping; that's the one we want to use
					if($configurableProductUrlCategoryId == $categoryId){
						break;
					}

				}

			}

		}

		if(is_null($productPath))
		{
			$productPath = str_replace(Mage::getUrl(), '', parent::getProductUrl());
		}

		return $productPath;

	}

	/**
	 * Get the URL to the product details page view.
	 * Provide a configurable product ID to get a configurable product URL.
	 *
	 * @param   int $configurableProductId
	 * @return  string
	 */
	public function getProductUrl($configurableProductId = null)
	{
		$productPath = $this->getProductPath($configurableProductId);
		return Mage::getUrl($productPath);
	}

	public function getPricesAsArray()
	{
		$prices = array();
		if($this->isConfigurable()){
			// configurable product, loop through all its simple products and get their prices
			$children = $this->getTypeInstance()->getUsedProducts();
			foreach($children as $child){
				$prices[] = (float)$child->getPrice();
			}
			// remove duplicates and order the prices from lowest to highest
			$prices = array_unique($prices, SORT_NUMERIC);
        } else {
			// simple product, just return its price as the only array element
			$prices[] = $this->getPrice();
		}
        sort($prices);
		return $prices;
	}

	// get the URL of the image to use for the product set (in product list view)
	public function getProductSetImageUrl()
	{
		$tileWidth = 1785; // maximum width (in pixels) of tile image, to be bottom-centered in tile
		$tileHeight = 350; // height (in pixels) of full tile
		$tileHeightPadded = 296; // height (in pixels) of tile, minus padding for text at the top of the tile
		$imageHelper = Mage::helper('frans/image');

		// default to showing the product's base image, scaled down to padded tile height
		$imageUrl = $imageHelper->getImageResizedByProduct($this, 'image', $tileWidth, $tileHeightPadded);

		if($this->isConfigurable())
		{
			// if the configurable product has no base image, check its default child product
			if($this->getImage() == 'no_selection')
			{
				$defaultChildProduct = $this->getDefaultChildProduct();
				// use the base image of the default child product
				$imageUrl = $imageHelper->getImageResizedByProduct($defaultChildProduct, 'image', $tileWidth, $tileHeightPadded);
			}
			else
			{
				// if the configurable product has a base image, use it
				$imageUrl = $imageHelper->getImageResizedByProduct($this, 'image', $tileWidth, $tileHeight);
			}
		}

		return $imageUrl;
	}

	public function getDefaultChildProduct()
	{

		if($this->_defaultChildProduct)
		{
			return $this->_defaultChildProduct;
		}

		// attempt to get the default child of this product, if it has one set in the default_child attribute
		$store = Mage::app()->getStore();
		$defaultChildProductId = Mage::getResourceModel('catalog/product')
			->getAttributeRawValue($this->getId(), 'default_child', $store->getId());
		$this->setDefaultChildProduct($defaultChildProductId);

		return $this->_defaultChildProduct;
	}

	public function setDefaultChildProduct($defaultChildProductId)
	{
		if($defaultChildProductId)
		{
			$defaultChildProduct = Mage::getModel('catalog/product')->load($defaultChildProductId);
			// validate the default child product before setting it
			if($defaultChildProduct->isValidDefaultChild())
			{
				$this->_defaultChildProduct = $defaultChildProduct;
			}
		}
		return $this;
	}

	// check if the product both is enabled and has an enabled color tile, making it acceptable as a default child
	public function isValidDefaultChild()
	{
		// default to fail condition
		$result = false;
		// check that the product has enabled status
		if($this->getStatus() == Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
		{
			// NOTE: we used to check product availability here too, but don't do this anymore
			// if(Mage::getModel('cataloginventory/stock_item')->loadByProduct($this)->getIsInStock()) { ... }

			// check that the product's color tile is enabled
			$storeId = Mage::app()->getStore()->getId();
			$productResourceModel = Mage::getResourceModel('catalog/product');
			$colorTileId = $productResourceModel->getAttributeRawValue($this->getId(), 'color_tile', $storeId);
			$colorTile = Mage::getModel('frans/colorTile')->load($colorTileId);
			if($colorTile->getStatus() || $colorTileId === false) // allow non-tiles through if product doesn't have one set at all
			{
				// all conditions passed, product is valid
				$result = true;
			}
		}
		return $result;
	}

}