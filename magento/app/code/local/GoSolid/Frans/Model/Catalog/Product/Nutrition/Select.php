<?php
/**
 * Created by goSolid.
 * Date: 3/2/15
 * Time: 2:37 PM
 */
class GoSolid_Frans_Model_Catalog_Product_Nutrition_Select extends Varien_Object
{

	/**
	 * Reference to the attribute instance
	 *
	 * @var Mage_Catalog_Model_Resource_Eav_Attribute
	 */
	protected $_attribute;

	/**
	 * Initialize object
	 *
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Retrieve option array
	 *
	 * @return array
	 */
	static public function getOptionArray()
	{
		//get the Nutrition Option Array
		$list = Mage::getModel('frans/nutrition')->getCollection()->setOrder('name', 'ASC')->toGridOptionArray('code', 'name');

		return $list;
	}

	/**
	 * Retrieve all options
	 *
	 * @return array
	 */
	static public function getAllOptions()
	{
		$res = array();
		$res[] = array('value'=>'', 'label'=> Mage::helper('catalog')->__('None'));
		foreach (self::getOptionArray() as $index => $value) {
			$res[] = array(
				'value' => $index,
				'label' => $value
			);
		}
		return $res;
	}

}