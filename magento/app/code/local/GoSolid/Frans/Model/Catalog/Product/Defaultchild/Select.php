<?php
/**
 * Created by goSolid.
 * Date: 4/15/15
 * Time: 10:08 AM
 */
class GoSolid_Frans_Model_Catalog_Product_Defaultchild_Select extends Varien_Object
{

	/**
	 * Reference to the attribute instance
	 *
	 * @var Mage_Catalog_Model_Resource_Eav_Attribute
	 */
	protected $_attribute;

	/**
	 * Initialize object
	 *
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Retrieve option array
	 *
	 * @return array
	 */
	static public function getOptionArray()
	{
		$product = Mage::registry('product');
        if($product) {
            $childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null, $product);
            $list = array();

            // if configurable product has children, add them to the list
            if (count($childProducts) > 0) {
                foreach ($childProducts as $childProduct) {
                    // only include child products with enabled status
                    if ($childProduct->getStatus() == Mage_Catalog_Model_Product_Status::STATUS_ENABLED) {
                        $childProductLabel = $childProduct->getSku();
                        $list[$childProduct->getId()] = $childProductLabel;
                    }
                }
            } else {
                // no child products, possibly indicating this is a new product still being created
                $list = array(0 => Mage::helper('catalog')->__('Please associate some child products'));
            }
        }else{
            //We do this because we cant have a value if we dont have a product yet...
            $list = array(0 => Mage::helper('catalog')->__('NA'));
        }

		//get the Nutrition Option Array
		//$list = Mage::getModel('frans/nutrition')->getCollection()->setOrder('name', 'ASC')->toGridOptionArray('code', 'name');

		return $list;
	}

	/**
	 * Retrieve all options
	 *
	 * @return array
	 */
	static public function getAllOptions()
	{
		$res = array();
		$res[] = array('value'=>'', 'label'=> Mage::helper('catalog')->__('-- Please Select --'));
		foreach (self::getOptionArray() as $index => $value) {
			$res[] = array(
				'value' => $index,
				'label' => $value
			);
		}
		return $res;
	}

}