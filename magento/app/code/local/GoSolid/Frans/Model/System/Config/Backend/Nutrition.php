<?php


class GoSolid_Frans_Model_System_Config_Backend_Nutrition extends Mage_Core_Model_Config_Data
{
    public function _afterSave()
    {
        Mage::getModel('frans/nutrition')->uploadAndImport($this);
    }
}
