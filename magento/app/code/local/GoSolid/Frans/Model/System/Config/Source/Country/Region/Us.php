<?php
/**
 * Created by PhpStorm.
 * User: Steve
 * Date: 6/13/14
 * Time: 9:25 AM
 */

class GoSolid_Frans_Model_System_Config_Source_Country_Region_Us
{
    protected $_options;

    public function toOptionArray()
    {
        if (!$this->_options) {
            $this->_options = Mage::getResourceModel('directory/region_collection')->addCountryFilter('US', false)->loadData()->toOptionArray(false);
        }

        //unset the please select
        unset($this->_options[0]);

        return $this->_options;
    }
}