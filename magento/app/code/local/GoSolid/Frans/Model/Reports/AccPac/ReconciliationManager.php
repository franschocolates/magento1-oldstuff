<?php

require_once "lib/aws-autoloader.php";

use Aws\S3\S3Client;

class GoSolid_Frans_Model_Reports_AccPac_ReconciliationManager extends Varien_Object
{
	// these columns should match the SQL template below
	const COL_LABEL = 'label_name';
	const COL_PO_NUMBER = 'po_number';
	const COL_SKU = 'item_sku';
	const COL_CUSTOMER_ID = 'accpac_customer_number';
	const COL_ITEM_QTY = 'item_total_qty';
	const COL_PRICE = 'item_price';
	const COL_ITEM_TOTAL = 'item_row_total';
	const COL_ITEM_DISCOUNT = 'item_discount_total';
	const COL_ITEM_TAX = 'item_tax_total';
	const COL_SHIPPING = 'shipping_total';
	const COL_TAX = 'tax_total';
	const COL_PAYMENT_METHOD = 'payment_method';

	const PAY_BY_INVOICE = 'paybyinvoice';

	// not sure of best way to capture this; MFG has different STDCAT values
	// and special logic
	const MFG_CODE = 'MFG';

	private $_lineDelimiter;
	private $_filePath;

	private $_freightShippingLineNum;
    private $_skuLineNum;

	private $reportLines;

	public function __construct()
	{
		// it seems possible that excel will get mad about the line ending (\r\n versus \n)
		// so making this a param in case we need to update it
		$this->_lineDelimiter = "\r\n";

		// we could possibly make this config, but then if someone changed it, it would break
		// given that they have a macro pull it from a specific path, seems like changing it in code
		// is not the worst thing
		$this->_filePath = Mage::getBaseDir('var') . DS . 'export' . DS . 'accpac' . DS . 'accpacimport.csv';

		// old system started with 50 for freight/shipping lines
		$this->_freightShippingLineNum = 50;

        // old system had some weird 200,000 value for the SKU linenum, mimic that
        $this->_skuLineNum = 220300;

		$this->reportLines = array();
	}

	public function generateReport($date)
	{
		$this->_logInfo("Generating AccPac report for $date");

		$reportDate = new DateTime($date);

		$this->reportLines = $this->_getHeaderLines();

		$reportType = array("others", "accpacCust");
		foreach($reportType AS $type) {
			$isAccPacCust = ($type == 'accpacCust') ? true : false;

            $data = $this->_getReportData($this->_getLabelLevelSql($date, $isAccPacCust));
			$this->_logInfo("Label level results: " . print_r($data, true));

			$this->_getReportItems($data, $reportDate, $date);
		}

		// make sure directory exists
		if (!file_exists(dirname($this->_filePath)))
		{
			// create it recursively
			mkdir(dirname($this->_filePath), 0777, true);
		}
		file_put_contents($this->_filePath, implode($this->_lineDelimiter, $this->reportLines));

		// now lets drop that onto S3
		// this should create an archive and a most recent.

		$s3 = S3Client::factory(array(
					'region' => 'us-west-2',
					'version' => '2006-03-01',
					'credentials' => array(
						'key' => getenv('AWS_ACCESS_KEY_ID'),
						'secret' => getenv('AWS_SECRET_ACCESS_KEY')
					)
				));

		// copy to 'latest' url
		$latest = $s3->putObject(array(
		    'Bucket'       => 'magento-accpac-export',
		    'Key'          => 'accpacimport.csv',
		    'SourceFile'   => $this->_filePath,
		    'ContentType'  => 'text/csv'
		));

		// copy to archival url
		$archival = $s3->putObject(array(
		    'Bucket'       => 'magento-accpac-export',
		    'Key'          => 'archive/' .  date('Ymd-Gis') . '-accpacimport.csv',
		    'SourceFile'   => $this->_filePath,
		    'ContentType'  => 'text/csv'
		));

	}

	private function _getReportItems($data, $reportDate, $date){
		// we will have an associative array in each row
		// we loop through and see if the label changed, in which case we have a new section
		foreach ($data as $row)
		{
			$labelName = $row[self::COL_LABEL];

			$this->_logInfo("Processing accounting label $labelName");

			$poNumber = $row[self::COL_PO_NUMBER];
			$customerId = $row[self::COL_CUSTOMER_ID];
			$paymentMethod = $row[self::COL_PAYMENT_METHOD];

			$accountingLabel = Mage::getModel('frans/accounting_label')->loadByName($labelName);

			$ordUniq = $this->_getOrdUniqueForCustomer($accountingLabel, $customerId, $paymentMethod);

			$this->reportLines[] = $this->_getLabelHeaderRow($reportDate, $accountingLabel, $poNumber, $customerId, $ordUniq, $paymentMethod);

			// do all the SKUs for this label/customer
			$skuSql = $this->_getSkuLevelSql($date, $accountingLabel, $customerId, $poNumber, $paymentMethod);

			$skuData = $this->_getReportData($skuSql);
			foreach ($skuData as $skuRow)
			{
				$this->reportLines[] = $this->_getSkuRow($ordUniq, $accountingLabel, $skuRow);
			}

			// we can also go ahead and do the tax & freight, we just don't add them yet
			$freightRow = $this->_getLabelFreightRow($ordUniq, $accountingLabel, $row);
			$taxRow = $this->_getLabelSalesTaxRow($ordUniq, $accountingLabel, $row);

			$this->reportLines[] = $freightRow;
			$this->reportLines[] = $taxRow;
		}
    }

	/**
	 *
	 * Gets the data for a part of the report, which is an associative array of

	 * @param string $date
	 * @return array
	 */
	private function _getReportData($sql)
	{
		$readConnection =  Mage::getSingleton('core/resource')->getConnection('core_read');

		return $readConnection->fetchAll($sql, array(), Zend_Db::FETCH_ASSOC);
	}

    /**
     *
     * Builds the SQL to get the totals at the label level (not yet SKU)
     * If a customer record (invoice customer) is present, that is also returned
     *
     * @param $date
     */
    private function _getLabelLevelSql($date, $accpacCust = false)
    {
        $sqlTemplate = <<<EOSQL
SELECT sfo.accounting_label AS label_name
	, cev_cust_number.value AS accpac_customer_number
	, IF(LENGTH(po_number) > 0, MIN(po_number), '') AS po_number
	, SUM(sfi.shipping_amount) AS shipping_total
	, SUM(sfi.tax_amount) AS tax_total
	, sfop.method AS payment_method
	#, GROUP_CONCAT(sfo.increment_id) AS order_id
FROM sales_flat_order AS sfo
INNER JOIN
	sales_flat_invoice AS sfi
ON
	sfi.order_id = sfo.entity_id
LEFT OUTER JOIN
	customer_entity_varchar AS cev_cust_number
ON
	cev_cust_number.entity_id = sfo.customer_id
AND
	cev_cust_number.attribute_id = (SELECT attribute_id FROM eav_attribute WHERE attribute_code='customer_number')
LEFT OUTER JOIN
	customer_entity_varchar AS cev_cust_name
ON
	cev_cust_name.entity_id = sfo.customer_id
AND
	cev_cust_name.attribute_id = (SELECT attribute_id FROM eav_attribute WHERE attribute_code='company_name')
/* get PO number if they are an invoice customer */
LEFT OUTER JOIN
    sales_flat_order_payment AS sfop
ON
    sfop.parent_id = sfo.entity_id
WHERE
	 DATE(CONVERT_TZ(sfi.created_at, 'UTC', '%s')) = '%s' AND %s
GROUP BY sfo.accounting_label, %s
ORDER BY sfo.accounting_label, cev_cust_number.value, cev_cust_name.value
EOSQL;


		$payByInvoice = <<<EOSQL
 sfop.method = 'paybyinvoice'
EOSQL;

		$otherPaymentType = <<<EOSQL
 (sfop.method IS NULL OR sfop.method <> 'paybyinvoice')
EOSQL;

		$groupByAccPacCustomer = <<<EOSQL
	IF(po_number IS NOT NULL AND cev_cust_number.value IS NOT NULL, cev_cust_name.value, '')
EOSQL;

        $localTimezoneName = Mage::getStoreConfig('general/locale/timezone');
        $dateObject = new DateTime($date);
		$wherePaymentMethod = $accpacCust ? $payByInvoice : $otherPaymentType;
		$groupBy = $accpacCust ? "accpac_customer_number" : $groupByAccPacCustomer;

			$sql = sprintf($sqlTemplate
            , $localTimezoneName
            , $dateObject->format('Y-m-d')
			, $wherePaymentMethod
			, $groupBy
        );

        $this->_logInfo('Final SQL to execute to get label level data: '. $sql);

        return $sql;
    }

	private function _getSkuLevelSql($date, $accountingLabel, $customerId, $poNumber = '', $paymentMethod)
    {
		$this->_logInfo("customerId passed to getSkuLevelSql: " . $customerId);
		$this->_logInfo("Is customerId empty: " . (empty($customerId) ? "true" : "false"));
		$this->_logInfo("poNumber: " . $poNumber);

        $sqlTemplate = <<<EOSQL
SELECT sfii.accpac_sku AS item_sku
	, SUM(sfii.qty) AS item_total_qty
	, SUM(sfii.price * sfii.qty) / SUM(sfii.qty)  AS item_price
	, SUM(sfii.row_total - COALESCE(sfii.discount_amount, 0)) AS item_row_total
	, SUM(COALESCE(sfii.discount_amount, 0)) AS item_discount_total
	, SUM(sfii.tax_amount) AS item_tax_total
FROM sales_flat_order AS sfo
LEFT OUTER JOIN
    sales_flat_order_payment AS sfop
ON
    sfop.parent_id = sfo.entity_id
INNER JOIN
	sales_flat_invoice AS sfi
ON
	sfi.order_id = sfo.entity_id
INNER JOIN
	sales_flat_invoice_item AS sfii
ON
	sfii.parent_id = sfi.entity_id
INNER JOIN
	sales_flat_order_item AS sfoi
ON
	sfoi.item_id = sfii.order_item_id
LEFT OUTER JOIN
	customer_entity_varchar AS cev_cust_number
ON
	cev_cust_number.entity_id = sfo.customer_id
AND
	cev_cust_number.attribute_id = (SELECT attribute_id FROM eav_attribute WHERE attribute_code='customer_number')
LEFT OUTER JOIN
	customer_entity_varchar AS cev_cust_name
ON
	cev_cust_name.entity_id = sfo.customer_id
AND
	cev_cust_name.attribute_id = (SELECT attribute_id FROM eav_attribute WHERE attribute_code='company_name')
WHERE
	DATE(CONVERT_TZ(sfi.created_at, 'UTC', '%s')) = '%s'
AND
	sfo.accounting_label = '%s'
/* additional PO number join */
%s
GROUP BY sfii.accpac_sku
EOSQL;

		$useCustomerNumberTemplate = <<<EOSQL
 AND cev_cust_number.value = '%s'
EOSQL;

		$paymentType = <<<EOSQL
 AND sfop.method = 'paybyinvoice'
EOSQL;

		$notPayByInvoice = <<<EOSQL
 AND (sfop.method IS NULL OR sfop.method <> "paybyinvoice")
EOSQL;


		$poCustomerNumberSql = ($paymentMethod == 'paybyinvoice') ? sprintf($useCustomerNumberTemplate, $customerId) : "";

		$poCustomerNumberSql .= (($paymentMethod == 'paybyinvoice') ? $paymentType : $notPayByInvoice);

		$dateObject = new DateTime($date);

		// have to use the label/customer number a few times
		// to handle cases where this and isn't one
		$sql = sprintf($sqlTemplate
			, Mage::getStoreConfig('general/locale/timezone')
			, $dateObject->format('Y-m-d')
			, $accountingLabel->getLabelName()
			, $poCustomerNumberSql
		);

        $this->_logInfo('Final SQL to execute to get SKU level details: '. $sql);

        return $sql;
    }

	/**
	 *
	 * Generates the header row for an accounting label
	 * This should exist in something like this format:
	 * 1,5503,UV020414,000,"","","","","","","","","",,,"","4",1,20140204,20140205,UVILL,0.00,0
	 *
	 * 1 = record type
	 * 5503 = unique identifier
	 * UV0202414 = UV for 02/02/14
	 *
	 *
	 * see also the get header rows
	 *
	 * @param DateTime $reportDate
	 * @param GoSolid_Frans_Model_Accounting_Label $accountingLabel
	 * @param string $companyName
	 * @param string $customerId
	 * @param string $ordUniq
	 */
	private function _getLabelHeaderRow($reportDate, $accountingLabel, $poNumber, $customerId, $ordUniq, $paymentMethod)
	{
		$nextDay = clone $reportDate;

		// for business customers, the customerId will be specified
		// otherwise fall back to the accounting label for it
		if ($paymentMethod != "paybyinvoice")
		{
			// standard, not a specific customer
			// 1,5503,UV020414,000,"","","","","","","","","",,,"","4",1,20140204,20140205,UVILL,0.00,0
			$customerId = $accountingLabel->getAccpacCustomerId();
			$ordNumber = $accountingLabel->getLabelName() . $reportDate->format('mdy');

			$format = '1,%s,%s,%s,"","","","","","","","","",,,"","%s",1,%s,%s,%s,0.00,0';
			$headerLine = sprintf($format, $ordUniq, $ordNumber, $customerId
                , $accountingLabel->getLabelName()
                , $reportDate->format('Ymd'), $nextDay->format('Ymd')
                , $accountingLabel->getAccpacLocationCode());
		}
		else
		{
			// for orders for customers, then we do orduniq = customer ID
			// and ordernumber = custID-{date}
			// 1,236369,236369-121013,236369,"","","","","","","","","",,,"","RIDDELL WILLIAMS",1,20131210,20140225,02CORP,0.00,0
			$ordNumber = "$customerId-{$reportDate->format('mdy')}";

			$format = '1,%s,%s,%s,"","","","","","","","","",,,"","%s",1,%s,%s,%s,0.00,0';
			$headerLine = sprintf($format, $ordUniq, $ordNumber, $customerId, $poNumber,
				$reportDate->format('Ymd'), $nextDay->format('Ymd'), $accountingLabel->getAccpacLocationCode());
		}

		return $headerLine;
	}

	private function _getOrdUniqueForCustomer($accountingLabel, $customerId, $paymentMethod)
	{
		if($customerId && $paymentMethod == self::PAY_BY_INVOICE){
			// for orders for customers, then we do orduniq = customer ID
			// and ordernumber = custID-{date}
			$ordUniq = $customerId;
		}
		else
		{
            // previous code was just using the numbers 550X where X seemed to change
            // do something similar and just use our ID
			$ordUniq = '500' . $accountingLabel->getId();
		}

		return $ordUniq;
	}

	/**
	 * Builds the row for a SKU
	 * Example:
	 *	2,236369,210749,1,220268,,STDCAT,02CORP,1,54.00,45.90,8.10,0,1
	 *
	 * @param string 	$ordUnique
	 * @param unknown_type $itemRow
	 */
	private function _getSkuRow($ordUnique, $accountingLabel, $itemRow)
	{
		$sku = $itemRow[self::COL_SKU];
		$locationCode = $accountingLabel->getAccpacLocationCode();
		$totalQty = intval($itemRow[self::COL_ITEM_QTY]);
        $skuLineNum = $this->_skuLineNum;
        $this->_skuLineNum++;

		if ($accountingLabel->getLabelName() != self::MFG_CODE)
		{
			$itemPrice = $itemRow[self::COL_PRICE];
			$rowTotal = $itemRow[self::COL_ITEM_TOTAL];
			$itemDiscountTotal = $itemRow[self::COL_ITEM_DISCOUNT];
			$itemTaxTotal = $itemRow[self::COL_ITEM_TAX];
			$category = 'STDCAT';
		}
		else
		{
			// MFG is no charge, everything is 0, and it has a different category
			$itemPrice = $rowTotal = $itemDiscountTotal = $itemTaxTotal = 0;
			$category = 'DONATE';
		}

		return "2,$ordUnique,$skuLineNum,1,$sku,,$category,$locationCode,$totalQty,$itemPrice,$rowTotal,$itemDiscountTotal,$itemTaxTotal,0";
	}

	/**
	 *
	 * Gets the freight row. Should be something like:
	 * 	2,5505,54,2,,MOFRT,,04INT,0,0,2695.00,0,,0
	 *
	 * @param string $ordUniq
	 * @param Frans_GoSolid_Model_Accounting_Label $accountingLabel
	 * @param array	row
	 */
	private function _getLabelFreightRow($ordUniq, $accountingLabel, $row)
	{
		$format = "2,%s,%d,2,,%s,,%s,0,0,%s,0,,0";
		$row = sprintf($format
					, $ordUniq
					, $this->_freightShippingLineNum
					, $accountingLabel->getAccpacFreightCode()
					, $accountingLabel->getAccpacLocationCode()
					, $row[self::COL_SHIPPING]
		);

		$this->_freightShippingLineNum++;

		return $row;
	}

	/**
	 *
	 * Gets the sales tax row. Template:
	 * 	2,240979,55,2,,SLSTAX,,02CORP,0,0,0.00,0,,0

	 * @param string $ordUniq
	 * @param Frans_GoSolid_Model_Accounting_Label $accountingLabel
	 * @param array	row
	 */
	private function _getLabelSalesTaxRow($ordUniq, $accountingLabel, $row)
	{
		$format = "2,%s,%s,2,,SLSTAX,,%s,0,0,%s,0,,0";
		$row = sprintf($format
					, $ordUniq
					, $this->_freightShippingLineNum
					, $accountingLabel->getAccpacLocationCode()
					, $row[self::COL_TAX]
		);

		$this->_freightShippingLineNum++;

		return $row;
	}

	private function _getHeaderLines()
	{
		$headerText = <<<EOH
RECTYPE,ORDUNIQ,ORDNUMBER,CUSTOMER,SHPNAME,SHPADDR1,SHPADDR2,SHPADDR3,SHPADDR4,SHPCITY,SHPSTATE,SHPZIP,SHPCOUNTRY,SHPPHONE,SHPFAX,SHPCONTACT,PONUMBER,TYPE,ORDDATE,EXPDATE,LOCATION,TAMOUNT1,AUTOTAXCAL
RECTYPE,ORDUNIQ,LINENUM,LINETYPE,ITEM,MISCCHARGE,CATEGORY,LOCATION,QTYORDERED,PRIUNTPRC,EXTINVMISC,INVDISC,TAMOUNT1,COMMINST
RECTYPE,ORDUNIQ,PAYMENT
RECTYPE,ORDUNIQ,UNIQUIFIER,DETAILNUM,COINTYPE,COIN
RECTYPE,ORDUNIQ,UNIQUIFIER
RECTYPE,ORDUNIQ,OPTFIELD
RECTYPE,ORDUNIQ,LINENUM,OPTFIELD
EOH;

		// returning as lines for now, so we can join with our line delimiter when necessary.
		return explode(PHP_EOL, $headerText);
	}

	private function _logInfo($message)
	{
		Mage::log($message, Zend_Log::INFO, 'accpac-reports.log');
	}
}