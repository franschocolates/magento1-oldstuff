<?php
class GoSolid_Frans_Model_FutureShipMessage extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('frans/futureShipMessage');
    }

    public function getImageUrl()
    {
        $url = "";

        if( strlen(trim($this->getImage())) > 0)
        {
            $url = Mage::getBaseUrl('media') . $this->getImage();
        }

    	return $url;
    }
}