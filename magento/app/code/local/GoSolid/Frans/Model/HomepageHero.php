<?php

/**
 * 
 * Provides option array for selecting homepage hero from config
 * @author goSolid
 *
 */
class GoSolid_Frans_Model_HomepageHero
{

	const TEMPLATE_DIRECTORY = 'homepagehero';
	const TEMPLATE_EXTENSION = '.phtml';
	
	public function toOptionArray()
	{
		// figure out where the template .phtml files live for the current theme
		// TODO: Make sure this works for multiple themes, inheriting from default automatically
		$designPackage = Mage::getDesign();
		$templatePath = Mage::getBaseDir('design') . DS . 'frontend' . DS . $designPackage->getTheme('frontend') . DS . $designPackage->getPackageName() . DS . 'template' . DS . self::TEMPLATE_DIRECTORY;
		// get an array of all the files in the template directory, in alphabetical order
		$files = scandir($templatePath);
		// begin options array with "None" option for no selected hero
		$options = array(
			array(
				'label' => '-- None --',
				'value' => ''
			)
		);
		foreach($files as $file)
		{
			// if the filename ends with ".phtml" we know it's a template
			if(substr($file, -6) === self::TEMPLATE_EXTENSION)
			{
				// add this file to the options array
				$options[] = array(
					'label' => $file,
					'value' => $file
				);
			}
		}
		return $options;
	}

}