<?php
/**
 * Fedex rewrite
 * Necessary because:
 *  - endpoints have changed
 *  - default Magento functionality around residential yes/no is dumb and is not address dependent.
 */ 
class GoSolid_Frans_Model_Usa_Shipping_Carrier_Fedex
    extends Mage_Usa_Model_Shipping_Carrier_Fedex
{
    /**
     * Create soap client with selected wsdl
     * Overridden with new endpoints due to FedEx change.
     *
     * @param string $wsdl
     * @param bool|int $trace
     * @return SoapClient
     */
    protected function _createSoapClient($wsdl, $trace = false)
    {
        $client = new SoapClient($wsdl, array('trace' => $trace));
        $client->__setLocation($this->getConfigFlag('sandbox_mode')
                ? 'https://wsbeta.fedex.com:443/web-services'
                : 'https://ws.fedex.com:443/web-services'
        );

        return $client;
    }

    /**
     * Form array with appropriate structure for shipment request
     *
     * @param Varien_Object $request
     * @return array
     */
    protected function _formShipmentRequest(Varien_Object $request)
    {
        /** @var GoSolid_Frans_Model_Sales_Order $order */
        $order = $request->getOrderShipment()->getOrder();

        // by default it will make the reference be something like "Order #{increment} - P1"
        // so we change the reference data and package ID to not do that
        $request->setReferenceData($order->getOrderNumberWithPrefix());
        $request->setPackageId('');

        $generatedRequest = parent::_formShipmentRequest($request);

        // since it's a PHP array, we need to pass it around
        $generatedRequest = $this->_updateZones($generatedRequest, $order);

        $generatedRequest = $this->_updatePaymentInfo($generatedRequest);

        $offset = (Mage::getModel('core/date')->getGmtOffset('hours') * -1);

        $generatedRequest['RequestedShipment']['ShipTimestamp'] = strtotime((($offset > 0)?'+':'') . $offset . ' hours',$request->getShipTimestamp());

        // now we need to override the residential field
        if (isset($generatedRequest['RequestedShipment']['Recipient']['Address']['Residential']))
        {

            // set based on address
            $generatedRequest['RequestedShipment']['Recipient']['Address']['Residential'] =
                $request->getIsResidential() ? 1 : 0;
        }

        return $generatedRequest;
    }

    /**
     * Updates payment info to use a third party when applicable
     *
     *
     * @param $generatedRequest
     */
    protected function _updatePaymentInfo($generatedRequest)
    {
        // only bother if payment is sender (ignore return shipments)
        // I don't love these constants, but they are hardcoded into our parent class
        /** @see Mage_Usa_Model_Shipping_Carrier_Fedex::_formShipmentRequest */
        if ($generatedRequest['RequestedShipment']['ShippingChargesPayment']['PaymentType'] == 'SENDER')
        {
            // see if it's set
            if ($thirdPartyAccountNumber = $this->getConfigData('third_party_account'))
            {
                $thirdPartyAccountNumber = Mage::helper('core')->decrypt($thirdPartyAccountNumber);
                // and if it's different from the main account
                // and we aren't in sandbox mode
                if ($thirdPartyAccountNumber != $this->getConfigData('account')
                    && !$this->getConfigFlag('sandbox_mode'))
                {
                    // we updated the paymenttype/payor
                    // but we're assuming the same country code as the store.
                    $generatedRequest['RequestedShipment']['ShippingChargesPayment']['PaymentType'] = 'THIRD_PARTY';
                    $generatedRequest['RequestedShipment']['ShippingChargesPayment']['Payor']['AccountNumber'] = $thirdPartyAccountNumber;
                }
            }
        }

        return $generatedRequest;
    }

    protected function _updateZones($generatedRequest, $order)
    {
        $zones = array();

        $zones[] = array(
            "ZoneNumber" => 1,
            "Header" => "Billed Weight" ,
            "DataField" => "REQUEST/SHIPMENT/RequestedPackageLineItems[1]/Weight/Value"
        );
        $zones[] = array(
            "ZoneNumber" => 2,
            "Header" => "Shipping" ,
            "DataField" => "UNKNOWN" //TODO: Not sure that this should pull?
        );
        $zones[] = array(
            "ZoneNumber" => 3,
            "Header" => "Ref" ,
            "DataField" => "TRANSACTION/CustomerTransactionId"
        );
        $zones[] = array(
            "ZoneNumber" => 4,
            "Header" => "Deliver" ,
            "DataField" => "REQUEST/SHIPMENT/ShipTimestamp"
        );
        $zones[] = array(
            "ZoneNumber" => 5,
            "Header" => "Srvc" ,
            "DataField" => "REQUEST/SHIPMENT/ServiceType"
        );

        $docTabContent = array();
        $docTabContent["DocTabContent"] = array(
            'DocTabContentType' => 'ZONE001',
            'Zone001' => array(
                'DocTabZoneSpecifications' => $zones
            )

        );

        $generatedRequest["TransactionDetail"]["CustomerTransactionId"] = $order->getOrderNumberWithPrefix();

        //set label
        $generatedRequest['RequestedShipment']['LabelSpecification'] = array(
            'LabelFormatType' => 'COMMON2D',
            'ImageType' => 'ZPLII',
            'LabelStockType' => 'STOCK_4X6.75_LEADING_DOC_TAB',
            'LabelPrintingOrientation' => 'BOTTOM_EDGE_OF_TEXT_FIRST',
            'CustomerSpecifiedDetail'   =>  $docTabContent
        );

        return $generatedRequest;
    }
    /**
     * Parse tracking response
     *
     * @param array $trackingValue
     * @param stdClass $response
     */
    protected function _parseTrackingResponse($trackingValue, $response)
    {
        if (is_object($response)) {
            if ($response->HighestSeverity == 'FAILURE' || $response->HighestSeverity == 'ERROR') {
                $errorTitle = (string)$response->Notifications->Message;
            } elseif (isset($response->TrackDetails)) {

                $trackInfo = $response->TrackDetails;
                $resultArray['status'] = (string)$trackInfo->StatusDescription;
                $resultArray['service'] = (string)$trackInfo->ServiceInfo;

                $timestamp = isset($trackInfo->EstimatedDeliveryTimestamp) ?
                $trackInfo->EstimatedDeliveryTimestamp : $trackInfo->ActualDeliveryTimestamp;
                $timestamp = str_split($timestamp, 19);
                $timestamp = $timestamp[0];

                $timestamp = strtotime((string)$timestamp);
                if ($timestamp) {
                    $resultArray['deliverydate'] = date('Y-m-d', $timestamp);
                    $resultArray['deliverytime'] = date('H:i:s', $timestamp);
                }
                $deliveryLocation = isset($trackInfo->EstimatedDeliveryAddress) ?
                    $trackInfo->EstimatedDeliveryAddress : $trackInfo->ActualDeliveryAddress;
                $deliveryLocationArray = array();
                if (isset($deliveryLocation->City)) {
                    $deliveryLocationArray[] = (string)$deliveryLocation->City;
                }
                if (isset($deliveryLocation->StateOrProvinceCode)) {
                    $deliveryLocationArray[] = (string)$deliveryLocation->StateOrProvinceCode;
                }
                if (isset($deliveryLocation->CountryCode)) {
                    $deliveryLocationArray[] = (string)$deliveryLocation->CountryCode;
                }
                if ($deliveryLocationArray) {
                    $resultArray['deliverylocation'] = implode(', ', $deliveryLocationArray);
                }

                $resultArray['signedby'] = (string)$trackInfo->DeliverySignatureName;
                $resultArray['shippeddate'] = date('Y-m-d', (int)$trackInfo->ShipTimestamp);
                if (isset($trackInfo->PackageWeight) && isset($trackInfo->Units)) {
                    $weight = (string)$trackInfo->PackageWeight;
                    $unit = (string)$trackInfo->Units;
                    $resultArray['weight'] = "{$weight} {$unit}";
                }

                $packageProgress = array();
                if (isset($trackInfo->Events)) {
                    $events = $trackInfo->Events;
                    if (isset($events->Address)) {
                        $events = array($events);
                    }
                    foreach ($events as $event) {
                        $tempArray = array();
                        $tempArray['activity'] = (string)$event->EventDescription;
                        $timestamp = str_split((string)$event->Timestamp, 19);
                        $timestamp = $timestamp[0];
                        $timestamp = strtotime((string)$timestamp);
                        if ($timestamp) {
                            $tempArray['deliverydate'] = date('Y-m-d', $timestamp);
                            $tempArray['deliverytime'] = date('H:i:s', $timestamp);
                        }
                        if (isset($event->Address)) {
                            $addressArray = array();
                            $address = $event->Address;
                            if (isset($address->City)) {
                                $addressArray[] = (string)$address->City;
                            }
                            if (isset($address->StateOrProvinceCode)) {
                                $addressArray[] = (string)$address->StateOrProvinceCode;
                            }
                            if (isset($address->CountryCode)) {
                                $addressArray[] = (string)$address->CountryCode;
                            }
                            if ($addressArray) {
                                $tempArray['deliverylocation'] = implode(', ', $addressArray);
                            }
                        }
                        $packageProgress[] = $tempArray;
                    }
                }

                $resultArray['progressdetail'] = $packageProgress;
            }
        }

        if (!$this->_result) {
            $this->_result = Mage::getModel('shipping/tracking_result');
        }

        if (isset($resultArray)) {
            $tracking = Mage::getModel('shipping/tracking_result_status');
            $tracking->setCarrier('fedex');
            $tracking->setCarrierTitle($this->getConfigData('title'));
            $tracking->setTracking($trackingValue);
            $tracking->addData($resultArray);
            $this->_result->append($tracking);
        } else {
            $error = Mage::getModel('shipping/tracking_result_error');
            $error->setCarrier('fedex');
            $error->setCarrierTitle($this->getConfigData('title'));
            $error->setTracking($trackingValue);
            $error->setErrorMessage($errorTitle ? $errorTitle : Mage::helper('usa')->__('Unable to retrieve tracking'));
            $this->_result->append($error);
        }
    }
}
