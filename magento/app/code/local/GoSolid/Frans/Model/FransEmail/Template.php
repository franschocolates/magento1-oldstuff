<?php
class GoSolid_Frans_Model_FransEmail_Template extends Aschroder_SMTPPro_Model_Email_Template
{
    
	public function setTemplateText($value)
    {    	
		// Start store emulation process
		$appEmulation = Mage::getSingleton('core/app_emulation');
		$initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation(1);
    	
    	// wrap provided template text with content from Fran's email wrapper phtml
    	$isHtmlTemplate = ($this->getTemplateType() == Mage_Core_Model_Template::TYPE_HTML);
    	$html = Mage::app()->setCurrentStore(1)->getLayout()
            ->createBlock("core/template", 'emailtemplate')
            ->setTemplate('fransemail/wrapper.phtml')
            ->setContent($value)
            ->setIsHtmlTemplate($isHtmlTemplate)
            ->toHtml();
    	$this->setData('template_text', $html);
    	
    	// End store emulation process
		$appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
		
    	return $this;
    }
}