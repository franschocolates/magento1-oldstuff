<?php
class GoSolid_Frans_Model_FransEmail_Mailalert
{
    public function toOptionArray(){
        return array(
            array('value' => 'alert_default.phtml', 'label'=>Mage::helper('core')->__('Warm Weather')),
            array('value' => 'alert_february.phtml', 'label'=>Mage::helper('core')->__('Holidays - February')),
            array('value' => 'alert_december.phtml', 'label'=>Mage::helper('core')->__('Holidays - December')),
            array('value' => 'alert_none.phtml', 'label'=>Mage::helper('core')->__('None')),
        );
    }

}