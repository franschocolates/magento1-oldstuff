<?php
/**
 * Created by PhpStorm.
 * User: Steve
 * Date: 4/9/2015
 * Time: 4:54 PM
 */

class GoSolid_Frans_Model_Shipping_Carrier_Saturday extends Mage_Shipping_Model_Carrier_Abstract
    implements Mage_Shipping_Model_Carrier_Interface
{
    protected $_code = 'saturday';
    protected $_tracking = true;
    /**
     * Check if carrier has shipping tracking option available
     *
     * @return boolean
     */
    public function isTrackingAvailable()
    {
        return $this->_tracking;
    }


    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        $rate = Mage::getModel('shipping/rate_result_method');
        $result = Mage::getModel('shipping/rate_result');
        /* @var $rate Mage_Shipping_Model_Rate_Result_Method */
        $rate->setCarrier($this->_code);
        $rate->setCarrierTitle($this->getConfigData('title'));//TODO: add config $this->getConfigData('title'));
        $rate->setMethod('priority_overnight');
        $rate->setMethodTitle('Priority Overnight');
        $rate->setPrice(110);
        $rate->setCost(0);
        $result->append($rate);
        return $result;
    }

    /**
     * Get allowed shipping methods
     *
     * @return array
     */
    public function getAllowedMethods()
    {
        return array(
            'priority_overnight'    =>  'Priority Overnight'
        );
    }
}