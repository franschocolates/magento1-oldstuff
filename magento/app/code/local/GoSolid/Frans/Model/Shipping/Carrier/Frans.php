<?php

class GoSolid_Frans_Model_Shipping_Carrier_Frans
    extends Mage_Shipping_Model_Carrier_Abstract
    implements Mage_Shipping_Model_Carrier_Interface
{
	protected $_code = "frans";

	/**
	 * 
	 * Returns whether this is an aggregate carrier, ie it wraps other carriers.
	 */
	public function getIsAggregateCarrier()
	{
		return true;
	}
	
	public function getAllowedMethods()
	{
		// this should just be used to show methods at various places in the admin
		// so we pull any methods that are configured 
		$shippingZoneMethodsTimes = Mage::getModel('frans/shippingZoneMethodTime')->getCollection();
		
		$methods = array();
		$carriers = Mage::getSingleton('shipping/config')->getAllCarriers();
		
		foreach ($shippingZoneMethodsTimes as $methodTime)
		{
			# load the original carrier
			$shippingMethod = $methodTime->getShippingMethod();
			list($carrierCode, $method) = explode('_', $shippingMethod, 2);
			$carrier = $carriers[$carrierCode];
			
			// we have to delegate to the carrier to get it's methods, with labels
			$carrierMethods = $carrier->getAllowedMethods();
			if (isset($carrierMethods[$method]))
			{
				$methods[$shippingMethod] = $carrier->getConfigData('title') . ' - ' . $carrierMethods[$method];
			} 
			
		}
		return $methods;
	}

	public function getEffectiveShipDate()
	{
		// cutoff would be today, at the cutoff time
		$cutoffTime = $this->getConfigData('cutoff_time');

		$todayTimestamp = Mage::getModel('core/date')->timestamp(time());
		$now = new DateTime();
		$now->setTimestamp($todayTimestamp);
		$tomorrow = clone $now;
		$tomorrow->modify('+1 day');
		
		# see if we already past the cutoff, in which case the cutoff is tomorrow
		$cutoffDateTime = new DateTime($now->format("Y-m-d ")  . $cutoffTime ); 
		
		$effectiveShipDateTime = ($cutoffDateTime->getTimeStamp() > $now->getTimestamp()) ?
									$cutoffDateTime : $tomorrow;
		
		// once we know which date to use, we just want the date, no time.
		$effectiveShipDateTime->setTime(0,0,0);
		return $effectiveShipDateTime;
	}
	
	public function getDefaultMessage()
	{
		if ($messageId = $this->getConfigData('default_message_id'))
		{
			return Mage::getModel('frans/futureShipMessage')->load($messageId);
		}
		
		Mage::throwException('There is no default message specified for Fran\'s shipping!');
	}
	
	public function getDefaultShippingUnavailableMessage()
	{
		if ($messageId = $this->getConfigData('unavailable_date_message_id'))
		{
			return Mage::getModel('frans/futureShipMessage')->load($messageId);
		}
		
		Mage::throwException('There is no default unavailable message specified for Fran\'s shipping!');
	}
	
	public function allowedMethodsToGridOptionArray()
	{
		$carriers = Mage::getSingleton('shipping/config')->getAllCarriers();
		$allowedMethods = array();

		foreach ($carriers as $code => $model)
		{
			// don't include ourselves...
			if ($code != $this->_code)
			{
				if(!$carrierTitle = Mage::getStoreConfig("carriers/$code/title"))
				{
            		$carrierTitle = $code;
				}

				foreach ($model->getAllowedMethods() as $allowedCode => $allowedTitle)
				{
					$allowedMethods[$code . '_' . $allowedCode] =
						$carrierTitle . ' - ' . $allowedTitle;
				}
			}
		}

		return $allowedMethods;
	}

	public function allowedMethodsToOptionArray($selectText = ' - Select - ')
	{
		$gridOptions = $this->allowedMethodsToGridOptionArray();

		$options = array();

		$options[] = array('' =>  Mage::helper('frans')->__("-- %s --", $selectText));

		foreach ($gridOptions as $code => $label)
		{
			$options[] = array(
				'label' => $label
				, 'value' => $code
			);
		}

		return $gridOptions;
	}

	public function isShippingLabelsAvailable()
	{
		return true;
	}
	
    /**
     * Builds a list of available rates for the request
     *
     * @param $purpose Mage_Shipping_Model_Rate_Request $request
     * @return Mage_Shipping_Model_Rate_Result
     */
	public function collectRates(Mage_Shipping_Model_Rate_Request $request)
	{
        $result = Mage::getModel('shipping/rate_result');

        // loop through any rates we find for the zip code
		$zones = Mage::getModel('frans/shippingZone')->loadByPostcode($request->getDestPostcode());

		foreach($zones as $zone) {
			if ($zone && $zone->getId()) {
				$shippingConfig = Mage::getSingleton('shipping/config');
				$carriers = $shippingConfig->getAllCarriers();
				$timesForZip = $zone->getShippingMethodTimes();

				foreach ($timesForZip as $shipMethodTime) {
					list($code, $method) = explode('_', $shipMethodTime->getShippingMethod(), 2);
					$carrierModel = $carriers[$code];

					$rate = Mage::getModel('shipping/rate_result_method');
					$carrierModel = $carriers[$code];
					$rate->setCarrier($code);
					$rate->setCarrierTitle($carrierModel->getConfigData('title'));
					$rate->setMethod($method);
					$rate->setMethodTitle($carrierModel->getCode('method', $method));

					// cost/price will be set by shipping rules module
					$rate->setCost(0);
					$rate->setPrice(0);
					$result->append($rate);
				}
			}
		}
        return $result;
	}
}