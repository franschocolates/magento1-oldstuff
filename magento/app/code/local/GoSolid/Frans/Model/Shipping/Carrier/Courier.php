<?php

class GoSolid_Frans_Model_Shipping_Carrier_Courier
	extends Mage_Shipping_Model_Carrier_Abstract
	implements Mage_Shipping_Model_Carrier_Interface
{
	protected $_code = 'courier';
	
	public function collectRates(Mage_Shipping_Model_Rate_Request $request)
	{

		$result = Mage::getModel('shipping/rate_result');

        if(Mage::app()->getStore()->isAdmin())
        {
            $method = Mage::getModel('shipping/rate_result_method')
                ->setCarrier($this->_code)
                ->setCarrierTitle($this->_getMethod())
                ->setMethod($this->_getMethod())
                ->setMethodTitle($this->_getTitle())
                ->setPrice($this->getConfigData('price'))
                ->setCost('0.00');

            $result->append($method);
        }

		return $result;
	}

	public function getAllowedMethods()
	{
	    return $methods[] = array($this->_getMethod() => $this->_getMethod());
	}

    private function _getMethod()
    {
        return $this->getConfigData('name');
    }

    private function _getTitle()
    {
        return $this->getConfigData('title');
    }
}