<?php
class GoSolid_Frans_Model_Shipping_AddressValidator extends Mage_Core_Model_Abstract
{
	const SCORE_UNKNOWN = "Unknown";

    const NO_CHANGES = 'NO_CHANGES';
    const MODIFIED = 'MODIFIED_TO_ACHIEVE_MATCH';
    const APARTMENT_NUMBER_REQUIRED = "APARTMENT_NUMBER_REQUIRED";

    const ADDR_VALIDATION_PREFIX = "Address Validation: ";
    const ADDR_VALIDATION_DISABLED = "Address validation is disabled.";

    private $_validationServiceWsdl;

    private $_friendlyMessages;

    private $_origStreetData;

    private function _prefixErrorMessage($msg){
        /* Prefix our error messages related to address validation */
        return Mage::helper('frans')->__(self::ADDR_VALIDATION_PREFIX).$msg;
    }
    public function _construct()
    {
        parent::_construct();
        $this->_init('frans/shipping_addressValidator');

        $wsdlBasePath = Mage::getModuleDir('etc', 'GoSolid_Frans')  . DS . 'wsdl' . DS . 'FedEx' . DS;
        $this->_validationServiceWsdl = $wsdlBasePath . 'AddressValidationService_v4.wsdl';

        $this->_loadFriendlyMessages();
    }


    /* Turning off function as it is unused and should be removed if disabling it doesn't
       break anything.

       TODO: Remove at a later date. Function is unneeded.
    public function getAddressScore($address)
    {
        /* If validation is enabled, then we check for the address score. Otherwise,
           return unknown.
        if($this->_getFedexConfigData('validation')) {
            $score = $this->_requestAddressValidation($address);
            return $score;
        } else {
            return self::SCORE_UNKNOWN;
        }
    }
    */
    /**
     *
     * Validate an address with FedEx and store the address type / proposed address details
     * 	Can be a customer address or a quote address
     * @param $address Mage_Customer_Model_Address_Abstract
     */
    public function storeAddressTypeAndProposedAddress($address)
    {
        // only bother if we have some key fields
        if (!$address->getStreet()
            || !$address->getPostcode()
            || !$address->getCity()
        )
        {
            return $this;
        }
        /* Let's try to create a SOAP request. If we can't, handle the errors gracefully
           and return them to the user. _createSoapClient will create the errors you
           will see returned here. */
        if ($this->_getFedexConfigData('validation')){
            try
            {

                Mage::log("About to try and store address type/proposed for address {$address->getId()}");

                $validationRequest = $this->_createValidationRequest($address);
                $requestString = serialize($validationRequest);
                $debugData = array('request' => $validationRequest);
                $client = $this->_createSoapClient($this->_validationServiceWsdl);
                /* A bit dirty, but the SOAP client throws fatal errors instead of
                   exceptions when WSDL-defined methods cannot be found */

                $response = $client->addressValidation($validationRequest);
                $debugData['result'] = $response;
                $this->_fedexDebug($debugData);
                # we just take the first score for now,
                # as we are only passing one

                foreach($response->AddressResults as $name => $value)
                {
                    if ($name == 'EffectiveAddress')
                    {
                        $address->setAddressDeliveryType($response->AddressResults->Classification);
                        $value->Score = Mage::helper('frans')->getScoreFromAddressValidationResult($address, $response);
                        $value->ScoreDescription = Mage::helper('frans')->getScoreDescription($address, $response);
                        $value->ValidationResponse = $response;
                        $address->setProposedAddress(json_encode($value));
                        Mage::log("Status: {$address->getAddressDeliveryType()}");

                        break;
                    }
                }
                $this->_fedexDebug($debugData);
            }
            catch (Exception $e)
            {
                /* If the SOAP client fails, this will capture its error and display it to the user, as well
                   as log the error. */
                Mage::getSingleton('adminhtml/session_quote')->addError($this->_prefixErrorMessage($e->getMessage()));
                $debugData['result'] = array('error' => $e->getMessage(), 'code' => $e->getCode());
                Mage::logException($e);
            }
        } else {
            /* If validation is disabled, we display an error message to the user */
            Mage::getSingleton('adminhtml/session_quote')->addError(Mage::helper('frans')->__(self::ADDR_VALIDATION_DISABLED));
        }

        return $this;
    }

    public function getStoredScore($address)
    {
        if ($fedexAddress = $this->getFedexAddress($address))
        {
            if (isset($fedexAddress->Score))
            {
                return $fedexAddress->Score;
            }
        }

        return self::SCORE_UNKNOWN;
    }

    public function getScoreDescription($address)
    {
        if ($fedexAddress = $this->getFedexAddress($address))
        {
            if (isset($fedexAddress->ScoreDescription))
            {
                return join('<br />', $fedexAddress->ScoreDescription);
            }
        }

        return '';
    }

    /**
     * Determines whether an address has actual changes
     *
     * @param Mage_Customer_Model_Address $address
     * @return bool
     */
    public function doesAddressHaveActualChanges($address)
    {
        /* This check will prevent the modal dialog from popping up
           when address validation is disabled. */
        if($this->_getFedexConfigData('validation')){
            if ($serializedProposed = $address->getProposedAddress()) {
                $proposedAddress = json_decode($serializedProposed);
            }

            if (!$proposedAddress)
            {
                return false;
            }

            $changeMessages = $this->getChangeMessages($address, $proposedAddress);

            $hasChanges = false;
            if (count($changeMessages) > 0)
            {
                // check and see if we have any actual data changes....
                if (array_key_exists(self::MODIFIED, $changeMessages))
                {
                    $changedFields = $this->getChangedAddressFields($address);

                    $hasChanges =  count($changedFields) > 0;
                }
                //currently no reason to throw error for Apartment Number Required 9-22-16
                elseif(array_key_exists(self::APARTMENT_NUMBER_REQUIRED, $changeMessages)){
                    unset($changeMessages[self::APARTMENT_NUMBER_REQUIRED]);
                }
                else
                {
                    $hasChanges = true;
                }
            }
        }
        else {
            $hasChanges = false;
        }

        return $hasChanges;
    }

    /**
     * Determines which fields changed between an address
     * and the proposed version
     *
     * @param Mage_Customer_Model_Address $address
     * @return array
     */
    public function getChangedAddressFields($address, $proposedAddress = false)
    {
        $changedFields = array();

        if (!$proposedAddress)
        {
            $proposedAddress = $this->convertChangesToCustomerAddress($address);
        }

        // if we have a proposed one, use it
        if ($proposedAddress)
        {
            // compare any relevant fields between the address passed in
            $simpleFields = array('street', 'company', 'city', 'country_id', 'postcode', 'region');

            foreach ($simpleFields as $fieldName)
            {
                if (strcasecmp($address->getData($fieldName), $proposedAddress->getData($fieldName)))
                {
                    $changedFields[] = $fieldName;
                }
            }

            // region is more complicated - depending on the country, it might be either region or region_id
            if ($address->getData('region_id') >= 0 && $proposedAddress->getData('region_id') >= 0 &&
                $address->getData('region_id') != $proposedAddress->getData('region_id'))
            {
                $changedFields[] = 'region_id';
            }
        }

        return $changedFields;
    }

    /**
     * Converts a proposed address into a customer address for easier data passing
     *
     * @param Mage_Customer_Model_Address $address
     * @return Mage_Customer_Model_Address
     */
    public function convertChangesToCustomerAddress($address)
    {
        $proposedAddress = $this->getFedexAddress($address);

        if (!$proposedAddress)
        {
            return false;
        }

        // convert the changed address json to an address object
        $addressDetails = $proposedAddress->Address;

        $countryId = $addressDetails->CountryCode;
        $postcode = $addressDetails->PostalCode;

        // for US addresses, we don't want the last 4
        if ($countryId == 'US' && strlen($postcode) > 5)
        {
            $postcode = substr($postcode, 0, 5);
        }

        // we can just use a customer address, it adheres to address abstract
        $address = Mage::getModel('customer/address')
            ->setStreet($addressDetails->StreetLines)
            ->setCity($addressDetails->City)
            ->setRegionCode($addressDetails->StateOrProvinceCode)
            ->setPostcode($postcode)
            ->setCountryId($countryId);

        // figure out the region/region ID
        $region = Mage::getModel('directory/region')->loadByCode($addressDetails->StateOrProvinceCode, $countryId);

        if ($region && $region->getId())
        {
            $address->setRegionId($region->getId())
                    ->setRegion($region->getName());
        }
        else
        {
            $address->setRegion($addressDetails->StateOrProvinceCode);
        }

        if (isset($addressDetails->CompanyName))
        {
            $address->setCompany($addressDetails->CompanyName);
        }

        return $address;
    }

    protected function getFedexAddress($address)
    {
        $proposedAddress = false;

        if ($serializedProposed = $address->getProposedAddress())
        {
            $proposedAddress = json_decode($serializedProposed);
        }

        return $proposedAddress;
    }

    public function getChangeMessages($address, $proposedAddress = false)
    {
        if ($proposedAddress == false)
        {
            $proposedAddress = $this->getFedexAddress($address);
        }

        $messages = array();

        if ($proposedAddress && isset($proposedAddress->Changes))
        {
            $changes = $proposedAddress->Changes;

            $changes = is_array($changes) ? $changes : array($changes);

            foreach ($changes as $change)
            {
                if ($change != self::NO_CHANGES)
                {
                    $messages[$change] = $this->getFriendlyMessage($change);;
                }
            }
        }
        return $messages;
    }

    /*
     * Tries to load a friendly message for a fedex message
     * Just returns message
     */
    public function getFriendlyMessage($message)
    {
        if (isset($this->_friendlyMessages[$message]))
        {
            return $this->_friendlyMessages[$message];
        }

        return $message;
    }

    /**
     * Makes request for a specific address
     *
     * @param string $purpose
     * @return mixed
     */
    /* This function is unused and is handled by storeAddressTypeAndProposedAddress. If
       commenting this out doesn't break functionality, we can remove this function.

      TODO: Remove at a later date. Function is unneeded.
    private function _requestAddressValidation($address)
    {
        $validationRequest = $this->_createValidationRequest($address);
        $debugData = array('request' => $validationRequest);
		$foundScore = false;

		try
        {
			$client = $this->_createSoapClient($this->_validationServiceWsdl);
            $response = $client->addressValidation($validationRequest);
            $debugData['result'] = $response;

			$this->_fedexDebug($debugData);

			# we just take the first score for now,
			# as we are only passing one
	        foreach($response->AddressResults as $name => $value){
	        	if ($name == 'ProposedAddressDetails')
	        	{
					$foundScore = true;
					$score = $value->Score;
					break;
	        	}

	        	#break;
	        }
        }
        catch (Exception $e)
        {
        	$debugData['result'] = array('error' => $e->getMessage(), 'code' => $e->getCode());
			Mage::logException($e);
		}

		$this->_fedexDebug($debugData);

        if ($foundScore)
        {
        	return $score;
        }

        return self::SCORE_UNKNOWN;
    }
    */
    /**
     * Create soap client with selected wsdl
     *
     * @param string $wsdl
     * @param bool|int $trace
     * @return SoapClient
     */
    protected function _createSoapClient($wsdl, $trace = false)
    {
        if ($this->_getFedexConfigData('timeout') == 1) {
            $location = Mage::getUrl('gsfedex/index/timeout');
        } else {
            $location = $this->_getFedexConfigFlag('sandbox_mode')
                ? 'https://wsbeta.fedex.com/web-services/addressvalidation'
                : 'https://ws.fedex.com/web-services/addressvalidation';
        }
        $timeout = 5;
        ini_set('default_socket_timeout', $timeout);
        $client = new SoapClient($wsdl, array(
            'trace' => $trace,
            'connection_timeout' => $timeout,
            'keep_alive' => false,
	        'exceptions' => false // instead of throwing SoapFault exceptions, fail silently and return nothing
        ));
        $client->__setLocation($location);
        return $client;
    }

	/**
     * Retrieve information from fedex carrier configuration
     *
     * @param   string $field
     * @return  mixed
     */
    private function _getFedexConfigData($field)
    {
        $path = 'carriers/fedex/'.$field;
        return Mage::getStoreConfig($path, $this->getStore());
    }

    /**
     * Retrieve fedex config flag for store by field
     *
     * @param string $field
     * @return bool
     */
    private function _getFedexConfigFlag($field)
    {
        $path = 'carriers/fedex/' . $field;
        return Mage::getStoreConfigFlag($path, $this->getStore());
    }
    
    /**
     * Create a validation request
     *
     * @param $address	address we are validating
     * @return array
     */
    protected function _createValidationRequest($address)
    {
		$regionCode = Mage::getModel('directory/region')->load($address->getRegionId())->getCode();
		// we need to supply an ID - fallback to 1 if this is a new address
		
		$addressId = ($address->getId()) ? $address->getId() : 1;

        $validationRequest = array(
            'WebAuthenticationDetail' => array(
                'UserCredential' => array(
                    'Key'      => $this->_getFedexConfigData('key'),
                    'Password' => $this->_getFedexConfigData('password')
        )
            ),
            'ClientDetail' => array(
                'AccountNumber' => $this->_getFedexConfigData('account'),
                'MeterNumber'   => $this->_getFedexConfigData('meter_number')
            ),
            'TransactionDetail' => array('CustomerTransactionId' => $address->getId()),
            'Version' => $this->_getVersionInfo(),
            'InEffectAsOfTimestamp' => date('c'),
         //    'Options' =>			array(
									// 	'CheckResidentialStatus' => false,
									// 	'MaximumNumberOfMatches' => 5,
									// 	'StreetAccuracy' => 'LOOSE',
									// 	'DirectionalAccuracy' => 'LOOSE',
									// 	'CompanyNameAccuracy' => 'LOOSE',
									// 	'ConvertToUpperCase' => 1,
									// 	'RecognizeAlternateCityNames' => 1,
									// 	'ReturnParsedElements' => 1
									// ),
			'AddressesToValidate' => array(
									0 => array(
										'AddressId' => $address->getId(), # for now we are just using our ID
								     	'Address' => array(
								     		'StreetLines' => $address->getStreetFull(),
								           	'PostalCode' => $address->getPostcode(),
								           	'City' => $address->getCity(),
											'Company' => $address->getCompany(), 
											'StateOrProvinceCode' => $regionCode, 
											'CountryCode' => $address->getCountryId(),
										)
									)
									),
									
        );


        return $validationRequest;
    }

    private function _getVersionInfo()
    {
		return array(
			'ServiceId' => 'aval', 
			'Major' => '4', 
			'Intermediate' => '0', 
			'Minor' => '0'
		);
    }
    
    /**
     * Log debug data to fedex file
     * Copied from base logging 
     *
     * @param mixed $debugData
     */
    protected function _fedexDebug($debugData)
    {
        if ($this->_getFedexConfigData('debug')) {
            Mage::getModel('core/log_adapter', 'shipping_fedex.log')
               //->setFilterDataKeys($this->_debugReplacePrivateDataKeys)	// couldn't find any for fedex
               ->log($debugData);
        }
    }

    protected function _loadFriendlyMessages()
    {
        $this->_friendlyMessages = array(
            self::MODIFIED => 'Changes Suggested'
            , 'APARTMENT_NUMBER_NOT_FOUND' => 'Apt/Unit Number Not Found'
            , 'APARTMENT_NUMBER_REQUIRED' =>  'Apt/Unit Number Required'
            , 'INSUFFICIENT_DATA' => 'Insufficient Data'
        );
    }
}
