<?php

class GoSolid_Frans_Model_Shipping_Carrier_Pickupatstore
	extends Mage_Shipping_Model_Carrier_Abstract
	implements Mage_Shipping_Model_Carrier_Interface
{
	protected $_code = 'pickupatstore';
	
	public function collectRates(Mage_Shipping_Model_Rate_Request $request)
	{
		$result = Mage::getModel('shipping/rate_result');
		
		
		$retailStores = $this->_getRetailStores();
		
		foreach($retailStores as $retailStore){
			$method = Mage::getModel('shipping/rate_result_method');
			$method->setCarrier($this->_code);
			$method->setCarrierTitle($this->getConfigData('title'));
			$method->setMethod($retailStore->getStoreCode());
			$method->setMethodTitle($retailStore->getName());
			$method->setPrice('0.00');
			$method->setCost('0.00');
			$result->append($method);
		}
		
		return $result;
	}
	
	public function getAllowedMethods()
	{
		$retailStores = $this->_getRetailStores();
		
		$methods = array();
		
		foreach ($retailStores as $retailStore)
		{
			$methods[$retailStore->getStoreCode()] = $retailStore->getName();
		}
		
		return $methods;
	}
	
	private function _getRetailStores()
	{
		$retailStores = Mage::getModel('frans/retailStore')
			->getCollection()
			->addFieldToFilter('status', array(
				'eq' => 1
			));
		$retailStores->getSelect()->order('position DESC');
		
		return $retailStores;
	}
}