<?php
/**
 * Created by goSolid.
 */
class GoSolid_Frans_Model_Api2_PrintJob extends Mage_Api2_Model_Resource
{
    public function _retrieve()
    {
        // in the case of looking up jobs, our ID is the station name, but the parameter is called "ID"
        $stationName = $this->getRequest()->getParam('id');

        Mage::log("Looking up print station with name of $stationName");

        $printStation = Mage::getModel('frans/printStation')->load($stationName, 'station_name');

        if ($printStation->getId())
        {
            $firstItem = Mage::getModel('frans/printJob')->getCollection()
                        ->addStationToFilter($printStation)
                        ->addStatusToFilter(GoSolid_Frans_Model_PrintJob::JOB_STATUS_QUEUED)
                        ->getFirstitem();

            return ($firstItem->getId()) ? $firstItem->toArray() : array();
        }

        return false;
    }

}