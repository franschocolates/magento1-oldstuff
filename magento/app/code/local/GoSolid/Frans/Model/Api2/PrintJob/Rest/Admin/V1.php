<?php
/**
 * Handles updates via the API
 */

class GoSolid_Frans_Model_Api2_PrintJob_Rest_Admin_V1 extends GoSolid_Frans_Model_Api2_PrintJob
{
    protected function _update(array $data)
    {
        if ($id = $this->getRequest()->getParam('id'))
        {
            $newStatus = $data['print_status'];

            Mage::getModel('frans/printJobManager')->updatePrintJob($id, $newStatus);
        }
    }

}