<?php
/**
 * Created by goSolid.
 */ 
class GoSolid_Frans_Model_Api2_Request extends Mage_Api2_Model_Request
{
    public function getHeader($header)
    {
        Mage::log("Using my overridden get header.");
        $headerValue = parent::getHeader($header);

        if ($headerValue === false)
        {
            // try and see if exists with "REDIRECT" because of annoying apache redirects
            $temp = 'REDIRECT_HTTP_' . strtoupper(str_replace('-', '_', $header));
            if (isset($_SERVER[$temp])) {
                return $_SERVER[$temp];
            }

            // alternatively, it may not be prefixed with HTTP
            $temp = strtoupper(str_replace('-', '_', $header));
            if (isset($_SERVER[$temp]))
            {
                return $_SERVER[$temp];
            }

        }

        return $headerValue;
    }

}