<?php

class GoSolid_Frans_Model_Resource_Sales_Order_Item_Collection extends Mage_Sales_Model_Resource_Order_Item_Collection
{
    /**
     * Add parent order filter
     * 	Overrides the basic to handle the case where we have a parent order
     *
     * @param int|Mage_Sales_Model_Order $order
     * @return Mage_Sales_Model_Resource_Order_Collection_Abstract
     */
    public function setParentOrderFilter($order)
    {
        if ($order instanceof GoSolid_Frans_Model_Sales_Order && $order->getIsMultishipParent()) {
            $this->setSalesOrder($order);
            $orderId = $order->getId();
            if ($orderId) {
            	// need to join based on the parent
            	$this->getSelect()
            		->joinInner(
            			array( 'co' => $this->getTable('sales/order')), 
            			'co.entity_id = ' . $this->_orderField
					);
                $this->addFieldToFilter('multiship_parent_id', $orderId);
            } else {
                $this->_totalRecords = 0;
                $this->_setIsLoaded(true);
            }
        	return $this;
        }
        
        throw new InvalidArgumentException('Order must be a multiship parent order.');
    }
	
}