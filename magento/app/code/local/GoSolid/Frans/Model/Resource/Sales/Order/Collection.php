<?php
/**
 * Added to have an easy way to filter ready for shipment emails
 */ 
class GoSolid_Frans_Model_Resource_Sales_Order_Collection
    extends Mage_Sales_Model_Resource_Order_Collection
{
    protected $_maintainJoins = false;

    public function setMaintainJoins($value)
    {
        $this->_maintainJoins = $value;

        return $this;
    }

    /**
     * Minimize usual count select
     *
     * @return Varien_Db_Select
     */
    public function getSelectCountSql()
    {
        if (!$this->_maintainJoins)
        {
            return parent::getSelectCountSql();
        }

        /* @var $countSelect Varien_Db_Select */
        // in this case, we don't reset the joins.
        $this->_renderFilters();

        $countSelect = clone $this->getSelect();
        $countSelect->reset(Zend_Db_Select::ORDER);
        $countSelect->reset(Zend_Db_Select::LIMIT_COUNT);
        $countSelect->reset(Zend_Db_Select::LIMIT_OFFSET);

        $countSelect->columns('COUNT(*)');
        Mage::log("Select would be: $countSelect");

        return $countSelect;
    }


    public function addReadyToCaptureFilter()
    {
        // add criteria so we only have orders with a ship date
        $this->addFieldToFilter('is_ready_for_capture', array('eq' => 1))
                ->addFieldToFilter('captured_at', array('null' => true))
                // and are not multiship children/shipments
                ->addFieldToFilter('multiship_parent_id', array('null' => true));

        return $this;
    }

    public function addPaymentMethodJoin($alias = 'sfop')
    {
        // need to get payment method
        $select = $this->getSelect();
        $select->joinLeft(
            array($alias => Mage::getSingleton('core/resource')->getTableName('sales/order_payment'))
            , "$alias.parent_id = main_table.entity_id"
            , array('method AS sfop.method')
        );

        return $this;
    }


    public function addReadyForShipmentEmailFilter()
    {
        // SQL isn't too complicated, when written
        // just find any child orders that are sent but have no email, and get their parent
        // and combine with any singleship who have no email but are sent.
        $readyForShipmentSql = <<<EOSQL
entity_id IN
(
	SELECT
	    multiship_parent_id
	FROM
	    sales_flat_order
	WHERE
	    ship_date IS NOT NULL
	AND
	    shipment_email_sent_at IS NULL
	AND
	    multiship_parent_id IS NOT NULL
	UNION
	SELECT
	    entity_id
	FROM
	    sales_flat_order
	WHERE
	    ship_date IS NOT NULL
	AND
	    shipment_email_sent_at IS NULL
	AND
	    multiship_parent_id IS NULL
	AND	IFNULL(is_multiship_parent, 0) = 0
)
EOSQL;

        $this->getSelect()
            ->where($readyForShipmentSql);

        return $this;
    }

}