<?php

/**
 * Class GoSolid_Frans_Model_Resource_Sales_Order_Grid_Collection
 *
 * Rewrite in order to add SKU filtering
 */
class GoSolid_Frans_Model_Resource_Sales_Order_Grid_Collection extends Mage_Sales_Model_Resource_Order_Grid_Collection
{
    /** Temporarily disabling for ticket Ticket #1032: Admin Sales Order is Slow
    protected function _initSelect()
    {
        $select = parent::_initSelect()->getSelect();

        $this->_addSkuToSelect($select);

        return $this;
    }

    public function getSelectCountSql()
    {
        if ($this->getIsCustomerMode()) {
            $countSelect = parent::getSelectCountSql();
        } else {
            $countSelect = $this->_addSkuToSelect(parent::getSelectCountSql());
        }

        return $countSelect;
    }

    private function _addSkuToSelect($select)
    {
        $skuSubquery = new Zend_Db_Expr(
            sprintf("( SELECT sfoi.order_id, GROUP_CONCAT(sku SEPARATOR '\\n') AS sku_list FROM %s AS sfoi GROUP BY sfoi.order_id )",
                Mage::getSingleton('core/resource')->getTableName('sales/order_item')
            )
        );
        $select->joinLeft(
            array('skus' => $skuSubquery)
            , 'main_table.entity_id = skus.order_id'
            , 'sku_list'
        );

        return $select;
    }
    **/
}