<?php
/**
 * Created by PhpStorm.
 * User: Steve
 * Date: 6/13/14
 * Time: 8:33 AM
 */ 
class GoSolid_Frans_Model_Resource_Directory_Region_Collection extends Mage_Directory_Model_Resource_Region_Collection {



    /**
     * Filter by country_id
     *
     * @param string|array $countryId
     * @return Mage_Directory_Model_Resource_Region_Collection
     */
    public function addCountryFilter($countryId, $filterRegions=true)
    {
        if (!empty($countryId)) {
            if (is_array($countryId)) {
                $this->addFieldToFilter('main_table.country_id', array('in' => $countryId));
            } else {
                $this->addFieldToFilter('main_table.country_id', $countryId);
            }

            if($filterRegions)
            {
                //Now filter the regions
                $this->addAllowedRegions($countryId);
            }
        }
        return $this;
    }

    public function addAllowedRegions($countryId)
    {
        if($countryId=='US' || in_array('US',$countryId))//Would like to not hard code this but cant find a way around it right now
        {
            $allowedRegions = Mage::getStoreConfig('general/region/allowed_regions_us');
            $this->addFieldToFilter('main_table.region_id', explode(',',$allowedRegions));
        }
        return $this;
    }

}