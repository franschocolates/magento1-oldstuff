<?php
/**
 * Created by PhpStorm.
 * User: Steve
 * Date: 6/20/14
 * Time: 11:23 AM
 */ 
class GoSolid_Frans_Model_Resource_Catalog_Category extends Mage_Catalog_Model_Resource_Category {

    /**
     * Retrieve categories
     *
     * @param integer $parent
     * @param integer $recursionLevel
     * @param boolean|string $sorted
     * @param boolean $asCollection
     * @param boolean $toLoad
     * @return Varien_Data_Tree_Node_Collection|Mage_Catalog_Model_Resource_Category_Collection
     */
    public function getCategories($parent, $recursionLevel = 0, $sorted = false, $asCollection = false, $toLoad = true, $menuType='include_in_menu')
    {
        $tree = Mage::getResourceModel('catalog/category_tree');
        /* @var $tree Mage_Catalog_Model_Resource_Category_Tree */
        $nodes = $tree->loadNode($parent)
            ->loadChildren($recursionLevel)
            ->getChildren();

        $tree->addCollectionData(null, $sorted, $parent, $toLoad, true, $menuType );

        if ($asCollection) {
            return $tree->getCollection();
        }
        return $nodes;
    }


}