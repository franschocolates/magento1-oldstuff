<?php
class GoSolid_Frans_Model_Mysql4_RetailStore_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct() 
    {
        parent::_construct();
        $this->_init('frans/retailStore');
    }

	public function toOptionArray($value = "id", $label = "name", $selectText = null)
    {
		$array =  $this->_toOptionArray($value, $label);
		
		if($selectText != null)
		{
			$array = array('' =>  Mage::helper('frans')->__("-- %s --", $selectText)) + $array;
		}
		
		return $array;
		
    }
    
    
	public function toGridOptionArray($value = "id", $label = "name")
    {
    	$list = array();
		

    	foreach($this as $item)
    	{
    		$list[$item->getData($value)] = $item->getData($label);	
    	
    	}       
		return $list;
    }


    /* returns an grid array of options for the store ship method */
    public function toShipMethodGridOptionArray()
    {
        //get all the stores (active or not)
        $collection = Mage::getModel("frans/retailStore")->getCollection();

        $list = array();

        //loop each item and create a proper string for the value. This combines the ship method and the store code.
        foreach($collection as $store)
        {
            $s = "pickupatstore_" . $store->getStoreCode();
            $list[$s] = $store->getName();
        }

        return $list;

    }
    
}