<?php
/**
 * Created by goSolid.
 * Date: 3/2/15
 * Time: 2:37 PM
 */ 
class GoSolid_Frans_Model_Mysql4_Nutrition_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{

    protected function _construct()
    {
	    parent::_construct();
        $this->_init('frans/nutrition');
    }

	public function toGridOptionArray($value = "id", $label = "label", $selectText = null)
	{
		$list = array();

		foreach($this as $item)
		{
			$list[$item->getData($value)] = $item->getData($label);

		}
		if($selectText != null)
		{
			$list[null] = Mage::helper('events')->__("-- %s --", $selectText);
		}

		return $list;
	}

}