<?php
class GoSolid_Frans_Model_Mysql4_ColorTile extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        $this->_init('frans/colorTile', 'id');
    }

	//returns the path relative to the media directory.
	public function getMediaPath()
	{
		return "colortiles";
	}
	
}