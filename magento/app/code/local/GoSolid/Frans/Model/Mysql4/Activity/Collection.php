<?php
class GoSolid_Frans_Model_Mysql4_Activity_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct() 
    {
        parent::_construct();
        $this->_init('frans/activity');
    }

    public function addOrderToFilter($order)
    {

        $orderIds = array($order->getId());

        if ($order->getIsMultishipParent())
        {
            // need to get all child IDs
            foreach($order->getChildOrders() as $childOrder)
            {
                $orderIds[] = $childOrder->getId();
            }

            // need to join so we have the increment IDs for the other orders
            $this->getSelect()->join(
                array('orders' => $this->getTable('sales/order'))
                , 'main_table.entity_id = orders.entity_id'
                , 'increment_id'
            );
        }

        return $this->addFieldToFilter('entity_type', 'sales/order')
                    ->addFieldToFilter('main_table.entity_id', array('in' => $orderIds));
    }

	public function toOptionArray($value = "id", $label = "name", $selectText = null)
    {
		$array =  $this->_toOptionArray($value, $label);
		
		if($selectText != null)
		{
			$array = array('' =>  Mage::helper('frans')->__("-- %s --", $selectText)) + $array;
		}
		
		return $array;
		
    }
    
    
	public function toGridOptionArray($value = "id", $label = "name")
    {
    	$list = array();
		

    	foreach($this as $item)
    	{
    		$list[$item->getData($value)] = $item->getData($label);	
    	
    	}       
		return $list;
    }
    
}