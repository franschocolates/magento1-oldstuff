<?php
/**
 * Model for interacting with quote address data
 * Adapted from Core ImportExport_Data
 *
 * Basically just copied and added an extra filter/column for the quote ID
 *
 * @see Mage_ImportExport_Model_Resource_Import_Data
 *
 */
class GoSolid_Frans_Model_Mysql4_ImportExport_Import_Quote_Address_Data
    extends Mage_Core_Model_Resource_Db_Abstract
    implements IteratorAggregate
{
    /**
     * @var IteratorIterator
     */
    protected $_iterator = null;

    protected $_quoteId = false;

    /**
     * Resource initialization
     */
    protected function _construct()
    {
        $this->_init('frans/importexport_import_quoteaddressdata', 'id');
    }

    /**
     * Set quote ID (don't have magical getters/setters
     *
     * @param int $quoteId
     */
    public function setQuoteId($quoteId)
    {
        $this->_quoteId = $quoteId;
    }

    public function getQuoteId()
    {
        return $this->_quoteId;
    }

    /**
     * Retrieve an external iterator
     *
     * @return IteratorIterator
     */
    public function getIterator()
    {
        $adapter = $this->_getWriteAdapter();
        $select = $adapter->select()
            ->from($this->getMainTable(), array('data'))
            ->where('quote_id = :quote_id')
            ->order('id ASC');
        $stmt = $adapter->query($select, array('quote_id' => $this->getQuoteId()));

        $stmt->setFetchMode(Zend_Db::FETCH_NUM);
        if ($stmt instanceof IteratorAggregate) {
            $iterator = $stmt->getIterator();
        } else {
            // Statement doesn't support iterating, so fetch all records and create iterator ourself
            $rows = $stmt->fetchAll();
            $iterator = new ArrayIterator($rows);
        }

        return $iterator;
    }

    /**
     * Clean all bunches from table.
     *
     * @return Varien_Db_Adapter_Interface
     */
    public function cleanBunches()
    {
        return $this->_getWriteAdapter()->delete($this->getMainTable()
            , array('quote_id = ?' => (int) $this->getQuoteId())
        );
    }

    /**
     * Return behavior from import data table.
     *
     * @throws Exception
     * @return string
     */
    public function getBehavior()
    {
        $adapter = $this->_getReadAdapter();
        $behaviors = array_unique($adapter->fetchCol(
            $adapter->select()
                ->from($this->getMainTable(), array('behavior'))
                ->where('quote_id = :quote_id')
            , array('quote_id' => $this->getQuoteId())
        ));
        if (count($behaviors) != 1) {
            Mage::throwException(Mage::helper('importexport')->__('Error in data structure: behaviors are mixed'));
        }
        return $behaviors[0];
    }

    /**
     * Return entity type code from import data table.
     *
     * @throws Exception
     * @return string
     */
    public function getEntityTypeCode()
    {

        // HACK:
        // perceptibly this should always be 'quote_address'
        // since this is a quote / address centric class
        // soooooo let's just cut out the middle man.

        // $adapter = $this->_getReadAdapter();
        // $entityCodes = array_unique($adapter->fetchCol(
        //     $adapter->select()
        //         ->from($this->getMainTable(), array('entity'))
        //         ->where('quote_id = :quote_id')
        //     , array('quote_id' => $this->getQuoteId())
        // ));
        // if (count($entityCodes) != 1) {
        //     Mage::throwException(Mage::helper('importexport')->__('Error in data structure: entity codes are mixed'));
        // }
        // return $entityCodes[0];

        return "quote_address";

    }

    /**
     * Get next bunch of validated rows.
     *
     * @return array|null
     */
    public function getNextBunch()
    {
        if (null === $this->_iterator) {
            $this->_iterator = $this->getIterator();
            $this->_iterator->rewind();
        }
        if ($this->_iterator->valid()) {
            $dataRow = $this->_iterator->current();
            $dataRow = Mage::helper('core')->jsonDecode($dataRow[0]);
            $this->_iterator->next();
        } else {
            $this->_iterator = null;
            $dataRow = null;
        }
        return $dataRow;
    }

    /**
     * Save import rows bunch.
     *
     * @param string $entity
     * @param string $behavior
     * @param array $data
     * @return int
     */
    public function saveBunch($entity, $behavior, array $data)
    {
        return $this->_getWriteAdapter()->insert(
            $this->getMainTable(),
            array('behavior' => $behavior, 'quote_id' => $this->getQuoteId(), 'entity' => $entity, 'data' => Mage::helper('core')->jsonEncode($data))
        );
    }
}
