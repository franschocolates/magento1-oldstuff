<?php
/* 
 * Payment method for paying at pickup
 */
class GoSolid_Frans_Model_Payment_Method_PayAtPickup extends Mage_Payment_Model_Method_Abstract
{
    const CODE = 'pay_at_pickup';

    protected $_code = self::CODE;
    protected $_canUseInternal = true;
    
    # this means frontend, so we can filter so that it's admin only
    protected $_canUseCheckout = false;
    protected $_canOrder       = true;

    /**
     * Validate payment method information object
     * Rewritten to use an order when supplied
     *
     * @return GoSolid_Frans_Model_Payment_Method_PayAtPickup
     */
    public function validate($order = false)
    {
        /**
         * to validate payment method is allowed for billing country or not
         */
        if ($order && ($order instanceof Mage_Sales_Model_Order))
        {
            $billingCountry = $order->getBillingAddress()->getCountryId();

            if (!$this->canUseForCountry($billingCountry)) {
                Mage::throwException(Mage::helper('payment')->__('Selected payment type is not allowed for billing country.'));
            }

            return $this;
        }

        return parent::validate();
    }
}
?>