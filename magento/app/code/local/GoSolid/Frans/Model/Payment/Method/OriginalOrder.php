<?php
/**
 * Used to create from a previous order payment method
 */
class GoSolid_Frans_Model_Payment_Method_OriginalOrder
    extends Mage_Payment_Model_Method_Abstract
{
    protected $_code = 'original_order';

    protected $_originalOrder = null;

    // make it so that we can't use it anywhere
    // so that it doesn't show up, but is used via code.
    protected $_canUseInternal              = true;
    protected $_canUseCheckout              = false;

    /**
     * Quote session object
     *
     * @var Mage_Adminhtml_Model_Session_Quote
     */
    protected $_session;

    public function __construct()
    {
        $this->_session = Mage::getSingleton('adminhtml/session_quote');
    }

    /**
     * Determines whether available.
     * Should be based on whether our quote has an original order
     *
     * @param Mage_Sales_Model_Quote $quote
     * @return bool
     */
    public function isAvailable($quote = null)
    {
        if (parent::isAvailable($quote) && $quote && ($order = $this->getOriginalOrder()))
        {
            return true;
        }

        return false;
    }

    public function getSession()
    {
        return $this->_session;
    }

    public function getTitle()
    {
        $title = parent::getTitle();

        /* @var Mage_Sales_Model_Order $originalOrder */
        if ($originalOrder = $this->getOriginalOrder())
        {
            $originalMethod = $this->_getHelper()->getMethodInstance($originalOrder->getPayment()->getMethod());
            $title .= " ({$originalMethod->getTitle()})";
        }

        return $title;
    }

    public function getOriginalOrder()
    {
        if ($this->_originalOrder == null)
        {
            // this is a little bit ugly - we need to get the order from the session
            if ($orderId = $this->getSession()->getOrderId())
            {
                $order = Mage::getModel('sales/order')->load($orderId);

                if ($order->getId())
                {
                    $this->_originalOrder = $order;
                }
            }

            // prevent us form trying again.
            if ($this->_originalOrder == null)
            {
                $this->_originalOrder = false;
            }

        }

        return $this->_originalOrder;
    }
}