<?php
/* 
 * All quotes need a payment method. For any child orders, we will use the multiship payment method
 * this will need to be disabled/hidden in the frontend.
 */
class GoSolid_Frans_Model_Payment_Method_Multiship extends Mage_Payment_Model_Method_Abstract
{
    protected $_code = 'multiship';

    // make it so that we can't use it anywhere
    // so that it doesn't show up, but is used via code.
    protected $_canUseInternal              = false;
    protected $_canUseCheckout              = false;

}
?>