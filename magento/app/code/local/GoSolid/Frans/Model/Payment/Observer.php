<?php
/**
 * Created to track payment events
 */

class GoSolid_Frans_Model_Payment_Observer
        extends Varien_Object
{
    /**
     *
     * Implemented to handle where we have our original order payment
     * and we need to copy it over
     *
     * @param Varien_Event_Observer $observer
     */
    public function onConvertQuotePayment($observer)
    {
        $quotePayment = $observer->getQuotePayment();
        /* @var GoSolid_Frans_Model_Sales_Order_Payment $orderPayment */
        $orderPayment = $observer->getOrderPayment();

        $methodInstance = $orderPayment->getMethodInstance();

        if ($methodInstance instanceof GoSolid_Frans_Model_Payment_Method_OriginalOrder)
        {
            // oops, we need to reconvert
            // the following code will look up the original order
            // try and clone the data via custom clonedata method, so we can implement as necessary
            // and if that fails, fall back to getData, which is what would have happened
            // when it first cloned the order
            $originalOrder = $methodInstance->getOriginalOrder();

            // see if there is clone data
            if (!($data = $originalOrder->getPayment()->getMethodInstance()->getCloneData()))
            {
                $data = $originalOrder->getPayment()->getData();
            }

            $quotePayment->addData($data);

            Mage::helper('core')->copyFieldset('sales_convert_quote_payment', 'to_order_payment', $quotePayment, $orderPayment);

            // clear instance so it places properly.
            $orderPayment->unsetData('method_instance');
        }

    }
}