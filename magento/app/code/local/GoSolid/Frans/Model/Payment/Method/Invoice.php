<?php


class GoSolid_Frans_Model_Payment_Method_Invoice extends Mage_Payment_Model_Method_Abstract
{
    protected $_code  = 'paybyinvoice';
    protected $_canUseCheckout = true;
    protected $_canOrder = true;

    protected $_formBlockType = 'frans/payment_form_invoice';
    protected $_infoBlockType = 'frans/payment_info_invoice';

    public function isAvailable($quote = null){
        if($quote && $quote->getCustomer() && $quote->getCustomer()->getCustomerNumber()):
            return true;
        endif;
        return false;
    }

    public function assignData($data)
    {
        if (!($data instanceof Varien_Object)) {
            $data = new Varien_Object($data);
        }
        $info = $this->getInfoInstance();
        $info->setPoNumber($data->getPoNumber());
        return $this;
    }

    public function getIsCentinelValidationEnabled()
    {
    	return false;
    }

    /**
     * Validate payment method information object
     * Rewritten to use an order when supplied
     *
     * @return GoSolid_Frans_Model_Payment_Method_PayAtPickup
     */
    public function validate($order = false)
    {
        /**
         * to validate payment method is allowed for billing country or not
         */
        if ($order && ($order instanceof Mage_Sales_Model_Order))
        {
            $billingCountry = $order->getBillingAddress()->getCountryId();

            if (!$this->canUseForCountry($billingCountry)) {
                Mage::throwException(Mage::helper('payment')->__('Selected payment type is not allowed for billing country.'));
            }

            return $this;
        }

        return parent::validate();
    }

}
