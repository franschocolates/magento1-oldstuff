<?php
/**
 * Representing an order that was paid for at the register
 * Should be backend only
 */

class GoSolid_Frans_Model_Payment_Method_PaidAtRegister
    extends Mage_Payment_Model_Method_Abstract
{
    const METHOD_CODE = 'paid_at_register';

    protected $_code = self::METHOD_CODE;
    protected $_canUseInternal = true;

    # this means frontend, so we can filter so that it's admin only
    protected $_canUseCheckout = false;
    protected $_canOrder       = true;

    /**
     * Validate payment method information object
     * Rewritten to use an order when supplied
     *
     * @return GoSolid_Frans_Model_Payment_Method_PayAtPickup
     */
    public function validate($order = false)
    {
        // no validation necessary
        // they already paid
        return true;
    }
}