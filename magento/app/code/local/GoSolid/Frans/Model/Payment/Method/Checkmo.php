<?php
/**
 * Rewritten to allow us to validate on an order rather than on a quote
 * when we are editing
 */ 
class GoSolid_Frans_Model_Payment_Method_Checkmo
    extends Mage_Payment_Model_Method_Checkmo
{
    /**
     * Validate payment method information object
     * Rewritten to use an order when supplied
     *
     * @return GoSolid_Frans_Model_Payment_Method_Checkmo
     */
    public function validate($order = false)
    {
        /**
         * to validate payment method is allowed for billing country or not
         */
        if ($order && ($order instanceof Mage_Sales_Model_Order))
        {
            $billingCountry = $order->getBillingAddress()->getCountryId();

            if (!$this->canUseForCountry($billingCountry)) {
                Mage::throwException(Mage::helper('payment')->__('Selected payment type is not allowed for billing country.'));
            }

            return $this;
        }

        return parent::validate();
    }
}