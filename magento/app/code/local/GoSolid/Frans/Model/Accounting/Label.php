<?php
class GoSolid_Frans_Model_Accounting_Label extends Mage_Core_Model_Abstract 
{
	const DEFAULT_BUSINESS = 'BUS';
	
    public function _construct()
    {
        parent::_construct(); 
        $this->_init('frans/accounting_label');
    }
	
    public function getDefaultBusinessLabel()
    {
    	return self::DEFAULT_BUSINESS;
    }
    
    public function loadByName($name)
    {
    	return $this->load($name, 'label_name');
    }
}