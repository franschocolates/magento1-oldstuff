<?php
class GoSolid_Frans_Model_Accounting_Origination extends Mage_Core_Model_Abstract 
{
	const DEFAULT_ADMIN = 'MFG';
	const DEFAULT_WEB = 'WEB';
	
    public function _construct()
    {
        parent::_construct(); 
        $this->_init('frans/accounting_origination');
    }
	
    public function getDefaultOrigination()
    {
    	return self::DEFAULT_WEB;
    }
    
    public function getDefaultAdminOrigination()
    {
    	return self::DEFAULT_ADMIN;
    }
}