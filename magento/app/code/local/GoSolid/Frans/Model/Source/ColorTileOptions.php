<?php

/**
 * 
 * Provides option array for selecting color tiles from config
 * @author goSolid
 *
 */
class GoSolid_Frans_Model_Source_ColorTileOptions extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
	
	public function toOptionArray()
	{
		$options = Mage::getModel('frans/colorTile')
							->getCollection()
							->toOptionArray('id', 'title', 'None');
	
		return $options;
	}

	public function getAllOptions() {
		return $this->toOptionArray();
	}

}