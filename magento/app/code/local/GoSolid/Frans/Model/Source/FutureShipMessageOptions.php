<?php

/**
 * 
 * Provides option array for selecting future ship messages from config
 * Necessary because default Collection toOptionArray not always in correct format for system config
 * @author goSolid
 *
 */
class GoSolid_Frans_Model_Source_FutureShipMessageOptions
{
	
	public function toOptionArray()
	{
		$originalOptions = Mage::getModel('frans/futureShipMessage')
							->getCollection()
							->toOptionArray('id', 'message_title', ' - Select - ');	 
	
		return $originalOptions;
	}
}