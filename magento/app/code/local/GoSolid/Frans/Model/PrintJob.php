<?php
class GoSolid_Frans_Model_PrintJob extends Mage_Core_Model_Abstract 
{
    const JOB_TYPE_ZPLII = "ZPLII";
    const JOB_TYPE_PDF = "PDF";

    const JOB_STATUS_QUEUED = "Q";
    const JOB_STATUS_PRINTED = "P";
    const JOB_STATUS_FAILED = "F";

    public function _construct()
    {
        parent::_construct(); 
        $this->_init('frans/printJob');
    }



	
}