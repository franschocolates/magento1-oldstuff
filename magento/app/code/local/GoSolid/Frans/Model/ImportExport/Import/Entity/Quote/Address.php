<?php
/**
 * Manages importing of quote addresses
 */
class GoSolid_Frans_Model_ImportExport_Import_Entity_Quote_Address
        // CAREFUL! we shouldn't really inherit this because we aren't an EAV Entity
        // however, the issue is that we want a lot of the logic
        // if we just suppress certain constructor functionality we should be ok.
       extends Mage_ImportExport_Model_Import_Entity_Abstract
{
    const DEFAULT_COUNTRY_ID = 'US';

    /**
     * Permanent column names.
     *
     */
    const COL_FIRSTNAME   = 'firstname';
    const COL_LASTNAME   = 'lastname';
    const COL_COMPANY = 'company';
    const COL_STREET1 = 'street1';
    const COL_STREET2 = 'street2';
    const COL_CITY = 'city';
    const COL_STATE = 'state';
    const COL_POSTCODE = 'postcode';
    const COL_TELEPHONE = 'telephone';
    // the rest are optional
    const COL_GIFT_MESSAGE = 'gift_message';
    const COL_PREFERRED_ARRIVAL = 'preferred_arrival_date';
    const COL_SHIPPING_METHOD = 'shipping_method';
    const COL_SPECIAL_INST = 'special_instructions';

    // things we fill out
    const FIELD_COUNTRY_ID = 'country_id';
    const FIELD_REGION_ID = 'region_id';
    const FIELD_REGION = 'region';

    /**
     * Error codes.
     */

    const ERROR_INVALID_FIRSTNAME = 'invalidFirstname';
    const ERROR_INVALID_LASTNAME = 'invalidLastname';
    const ERROR_INVALID_STREET1 = 'invalidStreet1';
    const ERROR_INVALID_CITY = 'invalidCity';

    const ERROR_INVALID_STATE      = 'invalidState';
    const ERROR_INVALID_ZIP         = 'invalidZip';
    const ERROR_INVALID_TELEPHONE   = 'invalidPhone';
    const ERROR_NO_QTY              = 'noQty';
    const ERROR_ARRIVAL_DATE_IN_PAST = 'arrivalDateInPast';
    const ERROR_METHOD_WITH_NO_DATE = 'shippingMethodNoDate';
    const ERROR_UNKNOWN_METHOD      ='unknownShippingMethod';

    /**
     * Permanent entity columns.
     *
     * @var array
     */
    protected $_permanentAttributes = array(
                                        self::COL_FIRSTNAME
                                        , self::COL_LASTNAME
                                        , self::COL_COMPANY
                                        , self::COL_STREET1
                                        , self::COL_CITY
                                        , self::COL_STATE
                                        , self::COL_POSTCODE
                                        , self::COL_TELEPHONE
    );

    // columns that are particular
    // note that we will also append SKUs
    protected $_particularAttributes = array(
        self::COL_GIFT_MESSAGE
        , self::COL_PREFERRED_ARRIVAL
        , self::COL_SPECIAL_INST
        , self::COL_SHIPPING_METHOD
    );

    protected $_messageTemplates = array(
        self::ERROR_INVALID_FIRSTNAME       => 'Invalid first name (must not be blank)'
        , self::ERROR_INVALID_LASTNAME      => 'Invalid last name (must not be blank)'
        , self::ERROR_INVALID_STREET1       => 'Invalid street1 (must not be blank)'
        , self::ERROR_INVALID_CITY      => 'Invalid city (must not be blank)'
        , self::ERROR_INVALID_STATE      => 'Invalid value in state column (state does not exist)'
        , self::ERROR_INVALID_ZIP       => 'Invalid value in zip/postcode column (must be at least 5 characters)'
        , self::ERROR_NO_QTY            => 'No quantity found for any SKU'
        , self::ERROR_ARRIVAL_DATE_IN_PAST => 'Arrival date must be in the future'
        , self::ERROR_METHOD_WITH_NO_DATE => 'A preferred arrival date must be supplied to set a specific shipping method'
        , self::ERROR_UNKNOWN_METHOD    => 'Unknown shipping method'
        , self::ERROR_INVALID_TELEPHONE => 'Invalid value in telephone column (must not be blank)'
    );

    protected $_importSkus = array();

    protected $_skuProducts = array();

    protected $_allShippingMethods = array();


    public function __construct()
    {
        // DO NOT CALL PARENT constructor
        // call the functions parent calls that are necessary
        // we use a different data model so we can have different imports per quote
        //$this->_dataSourceModel = Mage_ImportExport_Model_Import::getDataSourceModel();
        $this->_dataSourceModel = Mage::getResourceModel('frans/importExport_import_quote_address_data');
        $this->_dataSourceModel->setQuoteId($this->getQuote()->getId());
        $this->_connection      = Mage::getSingleton('core/resource')->getConnection('write');

        $this->_addSkusToParticularAttributes();

        $this->_loadShippingMethods();
    }

    /**
     * Import data rows.
     *
     * @return boolean
     */
    protected function _importData()
    {
        // TODO: Implement _importData() method.
        Mage::log("Beginning import of quote addresses (and quote data)");

        $quote = $this->getQuote();
        $this->_removePreviousAddresses();
        $skuProducts = array();
        $skuQuoteItems = array();

        $first = true;

        while ($bunch = $this->_dataSourceModel->getNextBunch()) {

            foreach ($bunch as $rowNum => $rowData)
            {
                if ($first)
                {
                    $skuProducts = $this->_getSkuProducts($rowData);
                    $skuQuoteItems = $this->_addProductsToQuote($skuProducts);

                    $first = false;
                }

                if (!$this->validateRow($rowData, $rowNum)) {
                    continue;
                }

                $this->_setRegionFields($rowData);
                $this->_removeEmptyFields($rowData);

                // now create a quote address
                /** @var GoSolid_Frans_Model_Sales_Quote_Address $address */
                $address = Mage::getModel('sales/quote_address')
                            ->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_SHIPPING)
                            ->setSameAsBilling(0)
                            ->addData($rowData)
                            ->setCollectShippingRates(1);

                // do the street rollup with the two lines into an array
                $street = array($rowData[self::COL_STREET1]);
                if (isset($rowData[self::COL_STREET2]) && $rowData[self::COL_STREET2])
                {
                    $street[] = $rowData[self::COL_STREET2];
                }
                $address->setStreet($street);

                // handle the case where there is a shipping method
                // this will be wiped out when we collect rates
                // so we stash it in a different place to be retrieved later on
                $address->setSpecificShippingMethod($address->getShippingMethod());

                // add all the items
                foreach ($skuQuoteItems as $sku => $quoteItem)
                {
                    if ($addressQty = $rowData[$sku])
                    {
                        //Mage::log("Should be adding sku $sku with qty of $addressQty (quote item ID: {$quoteItem->getId()}");
                        $address->addItem($quoteItem, $addressQty);
                    }
                    else
                    {
                        Mage::log("Supposedly no row data for $sku - ");
                        Mage::log($rowData);
                    }
                }

                $quote->addShippingAddress($address);
                $this->_processedEntitiesCount++;

            }

        }

        if ($this->_processedEntitiesCount > 0)
        {

            $quote->collectTotals();

            $quote->save();

            // now set the shipping method to the cheapest
            /** @var GoSolid_Frans_Model_Sales_Quote_Address $shippingAddress */
            foreach ($quote->getAllShippingAddresses() as $shippingAddress)
            {
                if ($shippingMethod = $shippingAddress->getSpecificShippingMethod())
                {
                    $shippingAddress->setShippingMethod($shippingMethod);
                }
                else
                {
                    $shippingAddress->setShippingMethodToCheapest();
                }

                //This object gets reloaded by a separate instance of the model and it needs to be saved for that...
                $shippingAddress->save();
            }


            // remove the data.
            $this->_dataSourceModel->cleanBunches();
        }

        return $this;

    }

    /**
     * EAV entity type code getter.
     *
     * @return string
     */
    public function getEntityTypeCode()
    {
        return 'quote_address';
    }

    /**
     * Validate data row.
     *
     * @param array $rowData
     * @param int $rowNum
     * @return boolean
     */
    public function validateRow(array $rowData, $rowNum)
    {
        // skip rows that are completely empty.
        if ($this->_hasAnyAddressFields($rowData, $this->_permanentAttributes))
        {
            return $this->_validateAddressData($rowData, $rowNum);
        }
        else
        {
            $this->_rowsToSkip[$rowNum] = true;
            return false;
        }
    }

    protected function _validateAddressData(array $rowData, $rowNum)
    {
        // always increment how many we looked at.
        $this->_processedEntitiesCount ++;

        if(!Zend_Validate::is($rowData[self::COL_FIRSTNAME], 'NotEmpty')){
            $this->addRowError(self::ERROR_INVALID_FIRSTNAME, $rowNum);
        }

        if(!Zend_Validate::is($rowData[self::COL_LASTNAME], 'NotEmpty')){
            $this->addRowError(self::ERROR_INVALID_LASTNAME, $rowNum);
        }

        if(!Zend_Validate::is($rowData[self::COL_STREET1], 'NotEmpty')){
            $this->addRowError(self::ERROR_INVALID_STREET1, $rowNum);
        }

        if(!Zend_Validate::is($rowData[self::COL_CITY], 'NotEmpty')){
            $this->addRowError(self::ERROR_INVALID_CITY, $rowNum);
        }

        // things to validate - does the state code exist?
        // does it have all the necessary address fields.
        if (!$this->_validateRegion($rowData))
        {
            $this->addRowError(self::ERROR_INVALID_STATE, $rowNum);
        }

        if (!$this->_checkPostCode($rowData))
        {
            $this->addRowError(self::ERROR_INVALID_ZIP, $rowNum);
        }

        if (!$this->_validatePhoneNumber($rowData))
        {
            $this->addRowError(self::ERROR_INVALID_TELEPHONE, $rowNum);
        }

        if (!$this->_checkQuantity($rowData))
        {
            $this->addRowError(self::ERROR_NO_QTY, $rowNum);
        }

        // dates/methods can add multiple errors, which it handles; just call the method
        $this->_validateArrivalDateAndMethod($rowData, $rowNum);

        return !isset($this->_invalidRows[$rowNum]);
    }

    /**
     *
     * Removes fields that are effectively empty (have no value at all or have a 0)
     * We do this so that we don't get random 0s in our input file
     *
     * @param array $rowData
     */
    protected function _removeEmptyFields(array &$rowData)
    {
        foreach ($rowData as $key => $value)
        {
            if (!$value)
            {
                unset($rowData[$key]);
            }
        }
    }

    protected function _hasAnyAddressFields(array $rowData, array $requiredFields)
    {
        foreach ($requiredFields as $fieldName)
        {
            if ($rowData[$fieldName])
            {
                return true;
            }
        }
    }

    protected function _validateRegion(array $rowData)
    {
        $region = $this->_lookupRegion($rowData);

        return $region->getId() > 0;
    }

    protected function _validatePhoneNumber(array $rowData)
    {
        return Zend_Validate::is($rowData[self::COL_TELEPHONE], 'NotEmpty');
    }

    protected function _checkQuantity(array $rowData)
    {
        $skus = $this->_getImportSkus($rowData);
        $hasQty = false;

        foreach ($skus as $sku)
        {
            if ($rowData[$sku])
            {
                $hasQty = true;
                break;
            }
        }

        return $hasQty;
    }

    /**
     *
     * Performs validation for the arrival date and method
     * Current rules:
     *  Arrival date must be blank or must be in the future
     *  Shipping method can only be set if arrival date is set
     *  Shipping method must exist.
     *
     *
     * @param array $rowData
     */
    protected function _validateArrivalDateAndMethod(array $rowData, $rowNum)
    {
        $preferredArrivalDate = empty($rowData[self::COL_PREFERRED_ARRIVAL]) ? false : $rowData[self::COL_PREFERRED_ARRIVAL];

        // make sure it's in the future
        if ($preferredArrivalDate)
        {
            $dateHelper = Mage::getModel('core/date');
            $arrivalTimestamp = $dateHelper->timestamp($preferredArrivalDate);
            $currentTimestamp = $dateHelper->timestamp();

            if ($arrivalTimestamp <= $currentTimestamp)
            {
                $this->addRowError(self::ERROR_ARRIVAL_DATE_IN_PAST, $rowNum);
            }
        }


        $shippingMethod = !empty($rowData[self::COL_SHIPPING_METHOD]) ? $rowData[self::COL_SHIPPING_METHOD] : '';

        if ($shippingMethod)
        {
            // make sure they also have an arrival date
            if (!$preferredArrivalDate)
            {
                $this->addRowError(self::ERROR_METHOD_WITH_NO_DATE, $rowNum);
            }

            // also validate it's a known shipping method
            if (!array_key_exists($shippingMethod, $this->_allShippingMethods))
            {
                Mage::log("Unknown shipping method: $shippingMethod");
                $this->addRowError(self::ERROR_UNKNOWN_METHOD, $rowNum);
            }

        }
    }

    protected function _setRegionFields(array &$rowData)
    {
        $region = $this->_lookupRegion($rowData);

        $rowData[self::FIELD_REGION_ID] = $region->getId();
        $rowData[self::FIELD_REGION] = $region->getName();
        $rowData[self::FIELD_COUNTRY_ID] = self::DEFAULT_COUNTRY_ID;
    }

    protected function _lookupRegion(array $rowData)
    {
        // for now, we assume the US
        $stateCode = $rowData[self::COL_STATE];
        return  Mage::getModel('directory/region')->loadByCode($stateCode, self::DEFAULT_COUNTRY_ID);

        if ($region->getId())
        {
            $rowData[self::FIELD_REGION_ID] = $region->getId();
            $rowData[self::FIELD_REGION] = $region->getName();
            $rowData[self::FIELD_COUNTRY_ID] = self::DEFAULT_COUNTRY_ID;

            return true;
        }

        return false;
    }

    protected function _checkPostCode(array $rowData)
    {
        // just make sure its 5 characters for now
        return strlen($rowData[self::COL_POSTCODE]) >= 5;
    }

    protected function _addSkusToParticularAttributes()
    {
        $productCollection = Mage::getModel('catalog/product')->getCollection();
        $productCollection->getSelect()
                            ->reset(Zend_Db_Select::COLUMNS)
                            ->columns('sku');

        foreach ($productCollection as $product)
        {
            $this->_particularAttributes[] = $product->getSku();
        }

    }

    protected function _removePreviousAddresses()
    {
        // determine if we need this.
        foreach ($this->getQuote()->getAllShippingAddresses() as $shippingAddress)
        {
            $shippingAddress->isDeleted(true);
        }
    }

    protected function _getImportSkus($rowData)
    {
        if (empty($this->_importSkus))
        {
            foreach (array_keys($rowData) as $colName)
            {
                if ($colName == self::COL_FIRSTNAME)
                {
                    break;
                }
                $this->_importSkus[] = $colName;
            }
        }

        return $this->_importSkus;
    }

    protected function _getSkuProducts($rowData)
    {
        $skuProducts = array();

        // move through columns until we find the name column
        foreach (array_keys($rowData) as $colName)
        {
            if ($colName == self::COL_FIRSTNAME)
            {
                break;
            }

            $productModel = Mage::getModel('catalog/product')->loadByAttribute('sku', $colName);

            if (!$productModel->getId())
            {
                Mage::throwException("Cannot find product for SKU $colName, abandoning import!");
            }

            $skuProducts[$colName] = $productModel;
        }

        return $skuProducts;
    }

    protected function _addProductsToQuote($skuProducts)
    {
        $quote = $this->getQuote();
        $skuQuoteItems = array();

        $quote->removeAllItems();

        foreach ($skuProducts as $sku => $productModel)
        {
            $quoteItem = $quote->addProduct($productModel);
            $skuQuoteItems[$sku] = $quoteItem;

        }

        // save to save the items, then refresh them so they have IDs
        $quote->save()
                ->refreshItems();

        return $skuQuoteItems;
    }

    protected function _loadShippingMethods()
    {
        $methods = array();
        $allMethods = Mage::getModel('adminhtml/system_config_source_shipping_allmethods')->toOptionArray(false);

        foreach ($allMethods as $carrierCode => $carrierMethods)
        {
            if (is_array($carrierMethods['value']))
            {
                foreach ($carrierMethods['value'] as $options)
                {
                    $methods[$options['value']] = $options['label'];
                }
            }
            else
            {
                Mage::log("Carrier with code $carrierCode does not have methods as 'value', skipping.");
            }
        }

        $this->_allShippingMethods = $methods;
    }

    /**
     * Retrieve quote session object
     *
     * @return Mage_Adminhtml_Model_Session_Quote
     */
    protected function _getSession()
    {
        return Mage::getSingleton('adminhtml/session_quote');
    }

    /**
     * Retrieve quote model object
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        return $this->_getSession()->getQuote();
    }
}