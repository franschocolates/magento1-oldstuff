<?php
class GoSolid_Frans_Model_Pickupmanager_Itemgrid extends Varien_Data_Form_Element_Abstract
{
    private $_data1 = null;

    public function __construct($data) {
        parent::__construct($data);
        $this->_data1 = $data;
    }

    public function getElementHtml() {


        $orderId = $this->_data1["value"]["entity_id"];
        $order = Mage::getModel("sales/order")->load($orderId);

        $layout = Mage::getSingleton('core/layout');


        //$html= $itemsBlock->toHtml();
        $parentBlock = $layout ->createBlock('adminhtml/sales_order');
        $parentBlock->setOrder($order);


        $blockItems = $layout->createBlock('adminhtml/sales_order_view_items')
            ->setParentBlock($parentBlock)
            ->setTemplate('pickupmanager/items.phtml');


        return $blockItems->toHtml();
    }

}