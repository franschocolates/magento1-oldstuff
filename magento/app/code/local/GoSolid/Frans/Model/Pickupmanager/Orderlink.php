<?php
class GoSolid_Frans_Model_Pickupmanager_Orderlink extends Varien_Data_Form_Element_Abstract
{
    private $_data1 = null;

    public function __construct($data) {
        parent::__construct($data);

        $this->_data1 = $data;

    }

    public function getElementHtml() {


        $incrementId = $this->_data1["value"]["increment_id"];
        $orderId = $this->_data1["value"]["entity_id"];

        $url = Mage::getUrl("*/sales_order/view", array("order_id" => $orderId));

        return "<a href='" . $url . "' target='_blank'>" . $incrementId . "</a>" ;
    }

}