<?php
class GoSolid_Frans_Model_ShippingZoneMethodTime extends Mage_Core_Model_Abstract 
{
    const DELIVERY_TYPE_ALL = 0;
    const DELIVERY_TYPE_COMMERCIAL = 1;
    const DELIVERY_TYPE_RESIDENTIAL = 2;

    public function _construct()
    {
        parent::_construct(); 
        $this->_init('frans/shippingZoneMethodTime');
    }

    public function setData($key, $value = null)
    {
        parent::setData($key, $value);
        if(is_array($key))
        {
            if (is_array($this->getDeliveryDays()))
            {
                $this->setDeliveryDays(implode(',', $this->getDeliveryDays()));
            }
        }

        return $this;
    }

    /**
     * Determines whether the specified address can be delivered to with this shipping zone method time
     * Main determinant is whether it is residential or not
     *
     * @param GoSolid_Frans_Model_Sales_Quote_Address $address
     * @return bool
     */
    public function canDeliverTo($address)
    {
        // if all, can always deliver, we are done.
        if ($this->getDeliveryType() == self::DELIVERY_TYPE_ALL)
        {
            return true;
        }
        $isResidential = $address->getIsResidential();

        // we can deliver if we are RES only and it's a RES address
        // or we are commercial only and it's a commercial address
        return
            ($isResidential && $this->getDeliveryType() == self::DELIVERY_TYPE_RESIDENTIAL)
            || (!$isResidential && $this->getDeliveryType() == self::DELIVERY_TYPE_COMMERCIAL)
            ;
    }

    public function deliveryTypesToOptionArray()
    {
        return array(
            array('value' => self::DELIVERY_TYPE_ALL, 'label' => Mage::helper('core')->__('All'))
            , array('value' => self::DELIVERY_TYPE_RESIDENTIAL, 'label' => Mage::helper('core')->__('Residential'))
            , array('value' => self::DELIVERY_TYPE_COMMERCIAL, 'label' => Mage::helper('core')->__('Commercial'))
        );

        /*
        $array =  $this->_toOptionArray($value, $label);

        if($selectText != null)
        {
            $array = array('' =>  Mage::helper('frans')->__("-- %s --", $selectText)) + $array;
        }

        return $array;
        */
    }


    public function deliveryTypesToGridOptionArray()
    {
        return array(
            self::DELIVERY_TYPE_ALL => Mage::helper('core')->__('All')
        , self::DELIVERY_TYPE_RESIDENTIAL => Mage::helper('core')->__('Residential')
        , self::DELIVERY_TYPE_COMMERCIAL => Mage::helper('core')->__('Commercial')
        );
        /*
        $list = array();


        foreach($this as $item)
        {
            $list[$item->getData($value)] = $item->getData($label);

        }
        return $list;
        */
    }
}