<?php
class GoSolid_Frans_Model_ColorTile extends Mage_Core_Model_Abstract 
{
    public function _construct()
    {
        parent::_construct(); 
        $this->_init('frans/colorTile');
    }

	public function getImagePath(){
		return str_replace('\\', '/', $this->getData('image'));
	}

	//returns the path relative to the media directory.
	public function getMediaPath()
	{
		return "color_tiles";
	}
	
}