<?php

class GoSolid_Frans_Model_Adminhtml_System_Config_Source_Cms_Page
{

    protected $_options;

    public function toOptionArray()
    {
        if (!$this->_options) {

            $options = Mage::getResourceModel('cms/page_collection')
                ->load()->toOptionIdArray();

            // add a special value to signify skipping the CMS system entirely
            array_unshift($options, array('label' => "Use Customized Homepage Instead of CMS Page", 'value' => -1));

            $this->_options = $options;
        }
        return $this->_options;
    }

}