<?php
/**
 * Rewritten to allow us to set defaults on a quote
 */ 
class GoSolid_Frans_Model_Adminhtml_Session_Quote
    extends Mage_Adminhtml_Model_Session_Quote
{
    /**
     * Special logic on top of the previous one to default the addresses to save in the address book
     *
     * @return Mage_Sales_Model_Quote|void
     */
    public function getQuote()
    {
        // we are new if we don't have a quote yet
        $isNew = is_null($this->_quote) && !$this->hasQuoteId() && $this->hasCustomerId();

        $quote = parent::getQuote();

        if ($quote && $isNew)
        {
            Mage::log("Setting save in address book for stuff.");
            // set the addresses to save in address book by default
            $quote->getBillingAddress()->setSaveInAddressBook(1);
            $quote->getShippingAddress()->setSaveInAddressBook(1);

        }

        return $quote;
    }
}