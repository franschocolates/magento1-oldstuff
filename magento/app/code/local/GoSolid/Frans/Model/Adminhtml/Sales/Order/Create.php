<?php
/**
 * Rewritten to add custom shipping logic.
 */ 
class GoSolid_Frans_Model_Adminhtml_Sales_Order_Create
    extends Mage_Adminhtml_Model_Sales_Order_Create
{
    protected $_savedQuotes = null;

    /**
     * Retrieve quote billing address
     *  Overridden to default to save in address book
     *
     * @return Mage_Sales_Model_Quote_Address
     */
    public function getBillingAddress()
    {
        $billingAddress = $this->getQuote()->getBillingAddress();
        if (!$billingAddress->getId())
        {
            $billingAddress->setSaveInAddressBook(1);
        }

        return $billingAddress;
    }


    /**
     * Overridden to allow us to import more shipping data.
     *
     * @param array $data
     * @return Mage_Adminhtml_Model_Sales_Order_Create|void
     */
    public function importPostData($data)
    {
        parent::importPostData($data);

        if (isset($data['quote_save']))
        {
            $this->getQuote()->setSavedQuote(1);
            $this->getSession()->addSuccess(Mage::helper('core')->__('Quote was successfully saved.'));
        }

        if (isset($data['use_validated_address']))
        {
            $shippingAddressId = $data['shipping_address_id'];

            if ($address = $this->getQuote()->getAddressById($shippingAddressId))
            {
                $addressData = Mage::app()->getRequest()->getParam('updated_address');
                Mage::helper('frans')->applyChangesFromAddressValidation($address, $addressData);
                $this->getQuote()->setRecollect(true);
            }
        }

        if (isset($data['remove_address_id']) && $data['remove_address_id'])
        {
            $this->_removeShippingAddress($data['remove_address_id']);
        }

        if (isset($data['shipments']) && isset($data['update_shipments']))
        {
            $this->_importShipmentData($data['shipments']);
        }

        if (isset($data['set_multiship']))
        {
            Mage::log("Switching admin quote to be multishipping.");
            $this->getQuote()->setIsMultiShipping(true);
            // by default, the shipping address may be the same as the billing address
            // in that case, they may fill everything out and then change the billing address and have it copy back to the
            // shipping address (resetting the rates)
            // so for now, need to not make it the same
            // also we want our addresses to save in address book by default
            $this->getQuote()->getShippingAddress()->setSameAsBilling(0)->setSaveInAddressBook(1);
        }

        if (isset($data['add_shipment']))
        {
            // just need to add a new empty address
            $newAddress = Mage::getModel('sales/quote_address');
            $newAddress->setSaveInAddressBook(1);
            $this->getQuote()->addShippingAddress($newAddress);
        }

        if (isset($data['shipping_method']))
        {
            // parent will cover setting the ship method property
            // we need to import the other fields.
            $address = $this->getShippingAddress();
            $address->setPreferredArrivalDate($data['preferred_arrival_date']);
            $address->setPlannedShipDate($data['planned_ship_date']);
            $address->setBaseShippingAmountAdjusted($data['base_shipping_amount_adjusted']);
        }

        // gift message and special instructions have to go on the address
        // not on the quote
        if (isset($data['gift_message']))
        {
            $this->_getApplicableAddress()->setGiftMessage($data['gift_message']);
        }
        if (isset($data['special_instructions']))
        {
            $this->_getApplicableAddress()->setSpecialInstructions($data['special_instructions']);
        }

        return $this;
    }

    /**
     * Overridden to handle cases where our quote is multishipping
     *
     * @return $this
     */
    public function resetShippingMethod()
    {
        foreach ($this->getQuote()->getAllShippingAddresses() as $shippingAddress)
        {
            $shippingAddress->setShippingMethod(false);
            $shippingAddress->removeAllShippingRates();
        }
        return $this;
    }

    /**
     * Overridden to either set all addresses to collect shipping rates
     * Or to only do a specific address as in the request
     *
     * @return $this
     */
    public function collectShippingRates()
    {
        if (!$this->getQuote()->getIsMultiShipping())
        {
            return parent::collectShippingRates();
        }

        if ($addressId = Mage::app()->getRequest()->getParam('edit_address_id'))
        {
            $this->getQuote()->getAddressById($addressId)->setCollectShippingRates(true);
        }
        else
        {
            foreach ($this->getQuote()->getAllShippingAddresses() as $shippingAddress)
            {
                $shippingAddress->setCollectShippingRates(true);
            }
        }
        $this->collectRates();
        return $this;
    }

    protected function _getApplicableAddress()
    {
        // virtual quotes will convert the billing address to an order
        if ($this->getQuote()->getIsVirtual())
        {
            return $this->getQuote()->getBillingAddress();
        }

        // but normal quotes convert shipping address to the order
        return $this->getQuote()->getShippingAddress();
    }


    /**
     * Initialize creation data from existing order
     *
     * @param Mage_Sales_Model_Order $order
     * @return unknown
     */
    public function initFromOrder(Mage_Sales_Model_Order $order)
    {



        if (!$order->getReordered()) {
            $this->getSession()->setOrderId($order->getId());
        } else {
            $this->getSession()->setReordered($order->getId());
        }



        /**
         * Check if we edit quest order
         */
        $this->getSession()->setCurrencyId($order->getOrderCurrencyCode());
        if ($order->getCustomerId()) {
            $this->getSession()->setCustomerId($order->getCustomerId());
        } else {
            $this->getSession()->setCustomerId(false);
        }

        $this->getSession()->setStoreId($order->getStoreId());

        /**
         * Initialize catalog rule data with new session values
         */
        $this->initRuleData();
        foreach ($order->getItemsCollection(
                     array_keys(Mage::getConfig()->getNode('adminhtml/sales/order/create/available_product_types')->asArray()),
                     true
                 ) as $orderItem) {



            /* @var $orderItem Mage_Sales_Model_Order_Item */
            if (!$orderItem->getParentItem()) {

                //MAC: I changed this so that we always copy all items even if they shipped.
                $qty = $orderItem->getQtyOrdered();



                $item = $this->initFromOrderItem($orderItem, $qty);
                if (is_string($item)) {
                    Mage::throwException($item);
                }

            }
        }

        $this->_initBillingAddressFromOrder($order);
        $this->_initShippingAddressFromOrder($order);

        if (!$this->getQuote()->isVirtual() && $this->getShippingAddress()->getSameAsBilling()) {
            $this->setShippingAsBilling(1);
        }

        $this->setShippingMethod($order->getShippingMethod());
        $this->getQuote()->getShippingAddress()->setShippingDescription($order->getShippingDescription());

        // goSolid - rather than set to use original data, set to use our
        // custom original order method, so that it takes care of things (if they select it)
        //$this->getQuote()->getPayment()->addData($order->getPayment()->getData());
        $this->getQuote()->getPayment()->setMethod(Mage::getModel('frans/payment_method_originalOrder')->getCode());

        $orderCouponCode = $order->getCouponCode();
        if ($orderCouponCode) {
            $this->getQuote()->setCouponCode($orderCouponCode);
        }

        if ($this->getQuote()->getCouponCode()) {
            $this->getQuote()->collectTotals();
        }

        Mage::helper('core')->copyFieldset(
            'sales_copy_order',
            'to_edit',
            $order,
            $this->getQuote()
        );

        Mage::dispatchEvent('sales_convert_order_to_quote', array(
            'order' => $order,
            'quote' => $this->getQuote()
        ));

        if (!$order->getCustomerId()) {
            $this->getQuote()->setCustomerIsGuest(true);
        }




        if ($this->getSession()->getUseOldShippingMethod(true)) {
            /*
             * if we are making reorder or editing old order
             * we need to show old shipping as preselected
             * so for this we need to collect shipping rates
             */
            $this->collectShippingRates();
        } else {
            /*
             * if we are creating new order then we don't need to collect
             * shipping rates before customer hit appropriate button
             */
            $this->collectRates();
        }


        // Make collect rates when user click "Get shipping methods and rates" in order creating
        // $this->getQuote()->getShippingAddress()->setCollectShippingRates(true);
        // $this->getQuote()->getShippingAddress()->collectShippingRates();



        $this->getQuote()->save();

        return $this;
    }

    //returns an edit order increment by appending a letter to the end of the increment id.
    private function _getEditIncrementId($oldOrderIncrementId)
    {
        $strippedIncrement = $oldOrderIncrementId;

        $letter = "A"; //this is the default

        //check to see if the old oreder ended with a letter.
        $lastCharacter = substr("$oldOrderIncrementId", -1);
        if(ctype_alpha($lastCharacter) == true)
        {
            $strippedIncrement = $final = substr($oldOrderIncrementId, 0, -1);
            //increase the letter count.
            $letter= ++$lastCharacter;
        }

        //var_dump($oldOrderIncrementId);

        $newIncrementId = $strippedIncrement . $letter;

        //var_dump($newIncrementId);die;

        return $newIncrementId;

    }

    /**
     * Overridden to allow us to handle multiple addresses in the case of multishipping
     *
     * @return Mage_Adminhtml_Model_Sales_Order_Create|void
     */
    public function _prepareCustomer()
    {
        parent::_prepareCustomer();

        // now do any multishipping logic
        if ($this->getQuote()->getIsMultiShipping())
        {
            // go through and save any additional addresses
            $customer = $this->getQuote()->getCustomer();

            $addressCount = $customer->getAddressesCollection()->count();

            // can only add an address if we have a customer ID
            if ($customer->getId())
            {
                // this logic pretty much just came from the _prepareCustomer in our base class
                // but is applied to the other shipping addresses
				// base functionality handles the first address
                $defaultShippingAddressId = $this->getQuote()->getShippingAddress()->getId();

                foreach ($this->getQuote()->getAllShippingAddresses() as $shippingAddress)
                {
                    // first one is default, skip it.
                    if ($shippingAddress->getId() == $defaultShippingAddressId)
                    {
                        Mage::log("Skipping initial shipping address ID $defaultShippingAddressId");
                        continue;
                    }

                    // see if we need to import it; if so, do it
                    if ($shippingAddress->getSaveInAddressBook() && !$shippingAddress->getCustomerAddressId())
                    {
                        $customerShippingAddress = $shippingAddress->exportCustomerAddress();
                        $customer->addAddress($customerShippingAddress);
                    }
                    else if ($shippingAddress->getSaveInAddressBook())
                    {
                        $customer->getAddressItemById($shippingAddress->getCustomerAddressId())
                                                    ->addData($shippingAddress->getData());
                    }
                }

                $addressCount = $customer->getAddressesCollection()->count();


            }
        }

        return $this;
    }

    public function createOrder()
    {
        $this->_prepareCustomer();
        $this->_validate();
        $quote = $this->getQuote();
        $this->_prepareQuoteItems();

        $service = Mage::getModel('sales/service_quote', $quote);
        if ($this->getSession()->getOrder()->getId()) {
            $oldOrder = $this->getSession()->getOrder();
            $originalId = $oldOrder->getOriginalIncrementId();
            if (!$originalId) {
                $originalId = $oldOrder->getIncrementId();
            }
            $orderData = array(
                'original_increment_id'     => $originalId,
                'relation_parent_id'        => $oldOrder->getId(),
                'relation_parent_real_id'   => $oldOrder->getIncrementId(),
                'edit_increment'            => $oldOrder->getEditIncrement()+1,
                'increment_id'              => $this->_getEditIncrementId($oldOrder->getIncrementId())
            );
            $quote->setReservedOrderId($orderData['increment_id']);
            $service->setOrderData($orderData);
        }

        $order = $service->submit();
        if ((!$quote->getCustomer()->getId() || !$quote->getCustomer()->isInStore($this->getSession()->getStore()))
            && !$quote->getCustomerIsGuest()
        ) {
            $quote->getCustomer()->setCreatedAt($order->getCreatedAt());
            $quote->getCustomer()
                ->save()
                ->sendNewAccountEmail('registered', '', $quote->getStoreId());;
        }
        if ($this->getSession()->getOrder()->getId()) {
            $oldOrder = $this->getSession()->getOrder();

            $this->getSession()->getOrder()->setRelationChildId($order->getId());
            $this->getSession()->getOrder()->setRelationChildRealId($order->getIncrementId());
            $this->getSession()->getOrder()->cancel()
                ->save();
            $order->save();
        }
        if ($this->getSendConfirmation()) {
            $order->sendNewOrderEmail();
        }

        Mage::dispatchEvent('checkout_submit_all_after', array('order' => $order, 'quote' => $quote));

        return $order;
    }


    public function getCustomerSavedQuotes($includeQuotesWithOrders = false)
    {
        if (!is_null($this->_savedQuotes)) {
            return $this->_savedQuotes;
        }

        if (!$this->getQuote()->getCustomerId())
        {
            return false;
        }

        $this->_savedQuotes = Mage::getModel('sales/quote')->getSavedQuotes($this->getQuote()->getCustomerId(), $includeQuotesWithOrders);

        return $this->_savedQuotes;
    }

    protected function _removeShippingAddress($addressId)
    {
        Mage::log("About to remove address with ID of $addressId");

        $this->getQuote()->removeAddress($addressId);
    }

    protected function _importShipmentData($shipmentData)
    {
        foreach ($shipmentData as $addressId => $addressData)
        {
            if (is_array($addressData)) {
                if (isset($addressData['save_method']))
                {
                    $this->_saveShipmentShippingMethod($addressId, $addressData);
                }
                else
                {
                    $this->_saveShipmentAddressData($addressId, $addressData);
                }

            }

        }
    }

    protected function _saveShipmentShippingMethod($addressId, $data)
    {
        $address = $this->getQuote()->getAddressById($addressId);

        $address->setShippingMethod($data['shipping_method'])
                ->setPreferredArrivalDate($data['preferred_arrival_date'])
                ->setPlannedShipDate($data['planned_ship_date'])
                ->setBaseShippingAmountAdjusted($data['base_shipping_amount_adjusted'])
                ->save();
    }

    protected function _saveShipmentAddressData($addressId, $addressData)
    {
        $addressData['save_in_address_book'] = isset($addressData['save_in_address_book'])
            && !empty($addressData['save_in_address_book']);

        $shippingAddress = ($addressId == 'new') ?
            Mage::getModel('sales/quote_address')
                ->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_SHIPPING)
            : $this->getQuote()->getAddressById($addressId);

        $this->_setQuoteAddress($shippingAddress, $addressData);
        $shippingAddress->implodeStreetAddress();

        // need to save the address and then collect rates.
        $shippingAddress->setQuote($this->getQuote())
            ->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_SHIPPING)
            ->setSameAsBilling(false)
            ->setCustomerAddressId($addressData['customer_address_id']);

        // now we need to add all items
        $items = $addressData['items'];

        if (is_array($items))
        {
            foreach ($items as $itemId => $itemData)
            {
                // only bother if they have a qty
                $qty = $itemData['qty'];
                $orderItem = $this->getQuote()->getItemById($itemId);

                if ($existingItem = $shippingAddress->getItemByQuoteItemId($orderItem->getId()))
                {
                    if ($qty > 0)
                    {
                        $existingItem->setQty($qty);
                    }
                    else
                    {
                        $shippingAddress->removeItem($existingItem->getId());
                    }
                }
                else if ($qty > 0)
                {
                    $shippingAddress->addItem($orderItem, $qty);
                }
            }
        }

        // have to collect totals to know subtotal, then rates to figure out shipping options
        $shippingAddress
            ->setGiftMessage($addressData['gift_message'])
            ->setSpecialInstructions($addressData['special_instructions'])
            ->setShippingMethod(false)
            ->removeAllShippingRates()
            ->collectTotals()
            ->setCollectShippingRates(1)
            ->collectShippingRates()
            ->save();

    }
}