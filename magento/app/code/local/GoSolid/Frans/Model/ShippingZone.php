<?php
class GoSolid_Frans_Model_ShippingZone extends Mage_Core_Model_Abstract
{
	const WILDCARD = '*';

    public function _construct()
    {
        parent::_construct();
        $this->_init('frans/shippingZone');
    }

    public function loadByPostcode($postcode)
    {
        Mage::log("Searching for zone by postcode $postcode");
        //we only use the first 5 digits for zip zones
        if(strpos($postcode,'-'))
        {
            $postcodePieces = str_split($postcode, strpos($postcode,'-'));
            $postcode = $postcodePieces[0];
        }

    	Mage::log("Searching for zone by postcode $postcode");
    	$collection = $this->getCollection();
    	$collection->setOrder('position', 'asc');
		//$collection->getSelect()->limit(1);

    	foreach ($collection as $zone)
    	{
    		$postcodeList = $zone->getPostcodeList();
    		// if this is a wildcard, or the code is present in the list, it's our match.
    		if ($postcodeList == self::WILDCARD || strpos($postcodeList, $postcode) !== FALSE)
    		{
    			Mage::log("Found matching zone of {$zone->getId()}");

    		}else
			{
				$collection->removeItemByKey($zone->getId());
			}
    	}

    	return $collection;
    }

    public function getShippingMethodTimes()
    {
    	if ($this->getId())
    	{
    		$timeCollection = Mage::getModel('frans/shippingZoneMethodTime')->getCollection();
    		$timeCollection->addFieldToFilter('zone_id', $this->getId());

    		return $timeCollection->load();
    	}

    	return false;
    }
}