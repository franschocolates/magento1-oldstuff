<?php
class GoSolid_Frans_Model_Article extends Mage_Core_Model_Abstract 
{
	//This is in case they ever decide to have differnt types of articles
	const ARTICLE_TYPE_NEWS = 0;
	
    public function _construct()
    {
        parent::_construct(); 
        $this->_init('frans/article');
    }
    
    public function getNewsArticles(){

    	$articles = $this->getCollection()
    		->addFieldToFilter('type_id', self::ARTICLE_TYPE_NEWS)
    		->addFieldToFilter('status', 1);
    	$articles->getSelect()->order('date DESC');
    	return $articles;
    }
    
    public function getLinkDomain(){
    	$url = strtolower($this->getUrl());
    	$host = parse_url($url, PHP_URL_HOST);
    	$domain = str_replace('www.', '', $host);
    	return $domain;
    }


	public function getArticleYears(){
		$yearSelect = array (new Zend_Db_Expr ('YEAR(`date`) AS `year`'));
		$articles = $this->getNewsArticles();
		$articles->getSelect()->columns($yearSelect)->group('year');

		foreach($articles as $article){
			$year = $article->getYear();
				$years[] = $year;
		}
		return $years;
	}

	public function getArticlesByYear($year){
		$articles = $this->getNewsArticles();
		$articles->getSelect()->where("YEAR(`date`) = {$year}");
		return $articles;
	}

}