<?php
class GoSolid_frans_Model_CustomerNotes extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct(); 
        $this->_init('frans/customerNotes');
    }
	
    public function loadByEmail($email)
    {
    	$collection = $this->getCollection()->addFieldToFilter('email', $email);
    	$notesItem = $collection->getFirstItem();
    	if ($notesItem != null)
    	{
    		return $notesItem;
    	}

    	return false;
    }
    
    public function getNotesFromCustomerOrEmail($customerId, $email)
    {
    	if ($customerId)
    	{
    		$notes = $this->loadByCustomer($customerId);
    		if ($notes && $notes->getId())
    		{
    			return $notes->getNotes();
    		}
    	}
    	
    	if ($email)
    	{
    		$notes = $this->loadByEmail($email);
    		if ($notes && $notes->getId())
    		{
    			return $notes->getNotes();
    		}
    	}
    	
    	return '';
    }
    
    public function loadByCustomer($customerId)
    {
    	$collection = $this->getCollection()->addFieldToFilter('customer_id', $customerId);
    	$notesItem = $collection->getFirstItem();
    	if ($notesItem != null)
    	{
    		return $notesItem;
    	}
    	
    	return false;
    }
    
    public function saveNotes($notes, $customerId, $orderId)
    {
    	// first, attempt to do it by the order
    	if ($orderId)
    	{
    		$order = Mage::getModel('sales/order')->load($orderId);
    		$notesRecord = null;
    		if ($customerId = $order->getCustomerId())
    		{
    			$notesRecord = $this->_getExistingOrNewForCustomer($customerId);
    		}

    		// no customer, will have to use email
    		if ($notesRecord == null)
    		{
    			$notesRecord = $this->_getExistingOrNewForEmail($order->getCustomerEmail());
    		}
    	}
    	else
    	{
    		# assume we use the customer ID
    		$notesRecord = $this->_getExistingOrNewForCustomer($customerId);
    	}
    	
		$notesRecord->setNotes($notes);
		$notesRecord->save();
    	    	
    }
    
    private function _getExistingOrNewForCustomer($customerId)
    {
		$notesModel = Mage::getModel('frans/customerNotes');
   		$notesRecord = $notesModel->loadByCustomer($customerId);
    		
    	// couldn't find one, create it.
    	if ($notesRecord && $notesRecord->getId())
    	{
    		return $notesRecord;
    	}

    	# need the customer email
   		$customer = Mage::getModel('customer/customer')->load($customerId);
   		$notesRecord = Mage::getModel('frans/customerNotes');
   		$notesRecord->setEmail($customer->getEmail());
   		$notesRecord->setCustomerId($customer->getId());
    	
    	return $notesRecord;
    }
    
    private function _getExistingOrNewForEmail($email)
    {
		$notesModel = Mage::getModel('frans/customerNotes');
   		$notesRecord = $notesModel->loadByEmail($email);
    		
    	// couldn't find one, create it.
    	if ($notesRecord && $notesRecord->getId())
    	{
    		return $notesRecord;
    	}

    	# need the customer email
   		$customer = Mage::getModel('customer/customer')->load($customerId);
   		$notesRecord = Mage::getModel('frans/customerNotes');
   		$notesRecord->setEmail($email);
    	
    	return $notesRecord;
    }
}