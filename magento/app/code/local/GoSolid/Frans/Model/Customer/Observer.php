<?php

class GoSolid_Frans_Model_Customer_Observer
{
	/**
	 * Fires before a customer address is saved. When it is a shipping address,
	 * 	will try and validate it with our address validator
	 * @param $observer 
	 */
	public function onCustomerAddressBeforeSave($observer)
	{
		Mage::log("In onCustomerAddressBeforeSave");
		/* var $address GoSolid_Frans_Model_Customer_Address */
		$address = $observer->getCustomerAddress();
		
		if ($address->getLocationHasChanged())
		{
			Mage::log("Frans_Customer_Observer :: Before save on address: {$address->getId()} :: Location has changed, updating address delivery type");

			/* var $validator GoSolid_Frans_Model_Shipping_AddressValidator */
			$validator = Mage::getModel('frans/shipping_addressValidator');
			
			try
			{
				// note that we can't necessarily filter by address type, due to the fact that
				// customer addresses dont' have a type; they can be default billing, or shipping
				// but since addresses can be both, they don't have a distinct type.
				$validator->storeAddressTypeAndProposedAddress($address);

			}
			catch (Exception $exception)
			{
				Mage::helper('frans')->logCriticalException($exception, "Customer_Observer");
				Mage::logException($exception);
			}
		}
		else
		{
			Mage::log("Frans_Customer_Observer :: Before save on address: {$address->getId()} :: No location change, nothing to do.");
		}
	}

    /**
     * this is our hacky way of preventing customer addresses from being deleted
     * in the default admin customer controller.
     * It's hard to rewrite that entire action, so we just prevent deletions
     * since we have an actionf or that now
     *
     * @param $event
     */
    public function onAdminCustomerPrepareSave($event)
    {
        /** @var GoSolid_Frans_Model_Customer_Customer $customer */
        if ($customer = $event->getCustomer())
        {
            // we can't delete addresses this way, prevent it
            foreach ($customer->getAddressesCollection() as $address)
            {
                if ($address->getId())
                {
                    $address->unsetData('_deleted');
                }
            }
        }
   }
}