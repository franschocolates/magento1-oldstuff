<?php 
class GoSolid_Frans_Model_Customer_Customer extends Mage_Customer_Model_Customer
{
	
	/**
     * Configuration pathes for email templates and identities
     */
    const XML_PATH_REGISTER_EMAIL_TEMPLATE = 'customer/create_account/email_template';
    const XML_PATH_REGISTER_EMAIL_IDENTITY = 'customer/create_account/email_identity';
    const XML_PATH_REMIND_EMAIL_TEMPLATE = 'customer/password/remind_email_template';
    const XML_PATH_FORGOT_EMAIL_TEMPLATE = 'customer/password/forgot_email_template';
    const XML_PATH_FORGOT_EMAIL_IDENTITY = 'customer/password/forgot_email_identity';
    
    const XML_PATH_BUSINESS_REGISTER_EMAIL_TEMPLATE = 'customer/create_account/business_email_template';

    const XML_PATH_DEFAULT_EMAIL_DOMAIN         = 'customer/create_account/email_domain';
    const XML_PATH_IS_CONFIRM                   = 'customer/create_account/confirm';
    const XML_PATH_CONFIRM_EMAIL_TEMPLATE       = 'customer/create_account/email_confirmation_template';
    const XML_PATH_CONFIRMED_EMAIL_TEMPLATE     = 'customer/create_account/email_confirmed_template';
    const XML_PATH_GENERATE_HUMAN_FRIENDLY_ID   = 'customer/create_account/generate_human_friendly_id';
	
	public function validate()
    {
    	$errors = array();
       
		//MAC: I removed the first name and last name must not be empty validation requirements.
    	
        if (!Zend_Validate::is($this->getEmail(), 'EmailAddress')) {
            $errors[] = Mage::helper('customer')->__('Invalid email address "%s".', $this->getEmail());
        }

        /* begun customer password validation. We depend on our override of backend/password, so that
         * we don't have the same regex/message hard-coded everywhere */
        $password = $this->getPassword();
        if (!$this->getId() && !Zend_Validate::is($password , 'NotEmpty')) {
            $errors[] = Mage::helper('customer')->__('The password cannot be empty.');
        }
        if (strlen($password) && !Zend_Validate::is($password, 'StringLength', array(8))) {
            $errors[] = Mage::helper('frans')->__(GoSolid_Frans_Model_Customer_Customer_Attribute_Backend_Password::ERROR_MESSAGE);
        }
        else if (strlen($password) && !Zend_Validate::is($password, 'Regex', array(GoSolid_Frans_Model_Customer_Customer_Attribute_Backend_Password::VALID_PASSWORD_REGEX))) {
        	$errors[] = Mage::helper('frans')->__(GoSolid_Frans_Model_Customer_Customer_Attribute_Backend_Password::ERROR_MESSAGE);
        }
        
        $confirmation = $this->getConfirmation();
        if ($password != $confirmation) {
            $errors[] = Mage::helper('customer')->__('Please make sure your passwords match.');
        }

        $entityType = Mage::getSingleton('eav/config')->getEntityType('customer');
        $attribute = Mage::getModel('customer/attribute')->loadByCode($entityType, 'dob');
        if ($attribute->getIsRequired() && '' == trim($this->getDob())) {
            $errors[] = Mage::helper('customer')->__('The Date of Birth is required.');
        }
        $attribute = Mage::getModel('customer/attribute')->loadByCode($entityType, 'taxvat');
        if ($attribute->getIsRequired() && '' == trim($this->getTaxvat())) {
            $errors[] = Mage::helper('customer')->__('The TAX/VAT number is required.');
        }
        $attribute = Mage::getModel('customer/attribute')->loadByCode($entityType, 'gender');
        if ($attribute->getIsRequired() && '' == trim($this->getGender())) {
            $errors[] = Mage::helper('customer')->__('Gender is required.');
        }

        if (empty($errors)) {
            return true;
        }
        return $errors;
        
        
    }
    
    public function sortAddressesByName($address1, $address2)
    {
    	$lastName1 = $address1->getLastname();
    	$lastName2 = $address2->getLastname();
    	$lastNameSort = strcasecmp($lastName1, $lastName2);
    	// if last names match, compare first names
    	if($lastNameSort == 0){
	    	$firstName1 = $address1->getFirstname();
	    	$firstName2 = $address2->getFirstname();
    		$firstNameSort = strcasecmp($firstName1, $firstName2);
    		return $firstNameSort;
    	} else {
    		return $lastNameSort;
    	}
    }
    
    public function sendNewAccountEmail($type = 'registered', $backUrl = '', $storeId = '0', $businessAccount = '')
    {
        $types = array(
            'registered'   => self::XML_PATH_REGISTER_EMAIL_TEMPLATE,  // welcome email, when confirmation is disabled
            'confirmed'    => self::XML_PATH_CONFIRMED_EMAIL_TEMPLATE, // welcome email, when confirmation is enabled
            'confirmation' => self::XML_PATH_CONFIRM_EMAIL_TEMPLATE,   // email with confirmation link
        	'business'	   => self::XML_PATH_BUSINESS_REGISTER_EMAIL_TEMPLATE
        );
        
        if ($businessAccount == 'on'){
        	$types['registered'] = $types['business'];
        }
        
        if (!isset($types[$type])) {
            Mage::throwException(Mage::helper('customer')->__('Wrong transactional account email type'));
        }

        if (!$storeId) {
            $storeId = $this->_getWebsiteStoreId($this->getSendemailStoreId());
        }

        $this->_sendEmailTemplate($types[$type], self::XML_PATH_REGISTER_EMAIL_IDENTITY,
            array(
                'customer' => $this,
                'back_url' => $backUrl,
                'contact_email'=> Mage::getStoreConfig('trans_email/ident_general/email'))
            , $storeId);

        return $this;
    }
    
	public function getAdditionalAddresses()
    {
        $addresses = array();
        $primaryIds = $this->getPrimaryAddressIds();
        foreach ($this->getAddressesCollection() as $address) {
            if (!in_array($address->getId(), $primaryIds)) {
                $addresses[] = $address;
            }
        }
        usort($addresses, array($this, "sortAddressesByName"));
        return $addresses;
    }

	public function sendPasswordResetConfirmationEmail()
	{
		$storeId = $this->getStoreId();
		if (!$storeId) {
			$storeId = $this->_getWebsiteStoreId();
		}

		$contactDetailsHtml = Mage::app()->getLayout()->createBlock('frans/page_contactdetails')->setTemplate('page/contact-details.phtml')->setUseEmailFormatting(true)->toHtml();

		$this->_sendEmailTemplate(self::XML_PATH_FORGOT_EMAIL_TEMPLATE, self::XML_PATH_FORGOT_EMAIL_IDENTITY,
			array(
				'customer' => $this,
				'contact_details' => $contactDetailsHtml
			), $storeId);

		return $this;
	}

	public function getShippingAddresses()
	{
		// get a collection of the customer's addresses, excluding the default billing address if there is one
		$defaultBillingAddress = $this->getPrimaryBillingAddress();

		$addresses = $this->getAddressCollection()
			->setCustomerFilter($this)
			->addAttributeToSelect('*');

		if($defaultBillingAddress){
			$addresses->addFieldToFilter('entity_id', array(
				'neq' => $defaultBillingAddress->getId()
			));
		}

		return $addresses;
	}


    public function save()
    {
        // Saving subscriptions
        $subscriptions = explode(",", (string)$this->getSubscriptions());

        $copy_origin = false;

        if(in_array('1', $subscriptions)) //email newsletter value == 1
        {
            if( ! $this->getIsSubscribed())
            {
                $this->setIsSubscribed(1);
                
                $copy_origin = true;
            }

            if($this->getSubscriptionOrigin())
            {
                $subscription_origin = $this->getSubscriptionOrigin();
            }
            else
            {
                if(Mage::app()->getStore()->isAdmin() && Mage::getDesign()->getArea() == 'adminhtml')
                {
                    $subscription_origin = 'Magento-admin';
                }
                else
                {
                    $subscription_origin = 'Magento';
                }
            }

            $this->setSubscriptionOrigin($subscription_origin);
        }
        else
        {
            $this->setIsSubscribed(0);
            $this->setSubscriptionOrigin(null);
        }

        parent::save();


        // Save newsletter origin in newsletter table
        if($copy_origin)
        {
            // Copy subscription origin from customer to newsletter
            $subscription = Mage::getModel('newsletter/subscriber')->load($this->getId(), 'customer_id');
            if($subscription->getId() != null)
            {
                $subscription->setSubscriptionOrigin($this->getSubscriptionOrigin());
                $subscription->save();
            }
        }


        return $this;
    }


    public function subscribeEmail($isSubscribed = true)
    {
        $subscriptions = $this->getSubscriptions();
        $subscriptions = explode(',', $subscriptions);

        if($isSubscribed)
        {
            if(array_search(1, $subscriptions) === false)
            {
                $subscriptions[] = 1;
            }
        }
        else
        {
            if(($key = array_search(1, $subscriptions)) !== false) {
                unset($subscriptions[$key]);
            }
        }

        $subscriptions = implode(',', $subscriptions);

        $this->setSubscriptions($subscriptions);
    }
}
