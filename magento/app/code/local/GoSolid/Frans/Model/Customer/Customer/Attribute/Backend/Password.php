<?php

class GoSolid_Frans_Model_Customer_Customer_Attribute_Backend_Password
		extends Mage_Customer_Model_Customer_Attribute_Backend_Password
{
	const VALID_PASSWORD_REGEX = '/[1-9\/\,\.<>$%^&*!@#~`*\(\)+-="\'_|]/';
	const ERROR_MESSAGE = 'Your password must be at least 8 characters and contain at least 1 number or symbol.';
	
    /**
     * Special processing before attribute save:
     * a) check some rules for password
     * b) transform temporary attribute 'password' into real attribute 'password_hash'
     * 
     * Adapted from base class to have it use new rules:
     * 	- must be at least 8 characters
     *  - must have at least one number or special character
     */
    public function beforeSave($object)
    {
        $password = trim($object->getPassword());
        $len = Mage::helper('core/string')->strlen($password);
        if ($len) {
        	// must be at least 8 characters
            // make sure it has at least one number or special character
        	if ($len < 8 || !preg_match(self::VALID_PASSWORD_REGEX, $password)) {
				Mage::throwException(Mage::helper('frans')->__(self::ERROR_MESSAGE));            	
        	}
            	
            $object->setPasswordHash($object->hashPassword($password));
        }
    }
		
}