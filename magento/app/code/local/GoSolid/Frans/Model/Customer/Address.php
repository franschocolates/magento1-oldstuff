<?php

/**
 * Customer address model override
 *
 * @method string getLocationHash()
 * @method GoSolid_Frans_Model_Customer_Address setLocationHash()
 */
class GoSolid_Frans_Model_Customer_Address extends Mage_Customer_Model_Address
{


    /**
     * Called before the address is saved. Updates the location hash & notes whether
     * it has changed
     * @see Mage_Customer_Model_Address_Abstract::_beforeSave()
     */
    protected function _beforeSave()
    {
        $newHash = Mage::helper('frans')->calculateLocationHash($this);
        $currentHash = $this->getLocationHash();

        if ($newHash != $currentHash)
        {
            $this->setLocationHasChanged(true);
            $this->setLocationHash($newHash);
        }

        // Save address to CatalogMailingList table
        if( $this->getIsDefaultBilling() == "1" ) {
            $this->setEntityType("customer_address");
            Mage::dispatchEvent('mailing_list_capture_billing_address', $this->getData());
        }

        return parent::_beforeSave();
    }

    /*protected function _afterSave(){
        Mage::dispatchEvent('mailing_list_capture_billing_address', $this->getCustomer()->getPrimaryAddresses());

        return parent::_afterSave();
    }*/


    public function validate()
    {
        $errors = array();
        $this->implodeStreetAddress();
        if (!Zend_Validate::is($this->getFirstname(), 'NotEmpty')) {
            $errors[] = Mage::helper('customer')->__('Please enter the first name.');
        }

        if (!Zend_Validate::is($this->getLastname(), 'NotEmpty')) {
            $errors[] = Mage::helper('customer')->__('Please enter the last name.');
        }

        if (!Zend_Validate::is($this->getStreet(1), 'NotEmpty')) {
            $errors[] = Mage::helper('customer')->__('Please enter the street.');
        }

        if (!Zend_Validate::is($this->getCity(), 'NotEmpty')) {
            $errors[] = Mage::helper('customer')->__('Please enter the city.');
        }

        $_havingOptionalZip = Mage::helper('directory')->getCountriesWithOptionalZip();
        if (!in_array($this->getCountryId(), $_havingOptionalZip)
            && !Zend_Validate::is($this->getPostcode(), 'NotEmpty')
        ) {
            $errors[] = Mage::helper('customer')->__('Please enter the zip/postal code.');
        }

        if (!Zend_Validate::is($this->getCountryId(), 'NotEmpty')) {
            $errors[] = Mage::helper('customer')->__('Please enter the country.');
        }

        if ($this->getCountryModel()->getRegionCollection()->getSize()
            && !Zend_Validate::is($this->getRegionId(), 'NotEmpty')
            && Mage::helper('directory')->isRegionRequired($this->getCountryId())
        ) {
            $errors[] = Mage::helper('customer')->__('Please enter the state/province.');
        }

        if (empty($errors) || $this->getShouldIgnoreValidation()) {
            return true;
        }
        return $errors;
    }


    /* reset all of the variables used to control the address validation screens */
    public function resetAddressValidation()
    {
        $this->setSuggestAddress(null); //reset suggestion.
        $this->setValidationAttemptCount(null);
        $this->setIsValid(null);
        $this->save();
    }

}