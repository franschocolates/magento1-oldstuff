<?php

class GoSolid_Frans_Model_Customer_Subscriptions extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{

	public function getAllOptions()
	{

	    $this->_options = array();

	    $subscriptions = $this->getSubscriptions('title'); // sort by title alphabetically (sorting currently not implemented)

	    foreach($subscriptions as $subscription)
	    {
		    $this->_options[] = array(
			    'label' => $subscription['title'],
			    'value' => $subscription['id']
		    );
	    }

	    return $this->_options;

	}


	public function toOptionArray()
	{
	    return $this->getAllOptions();
	}

	public function getSubscriptions($sortKeys = null)
	{
		$subscriptions = array(
			array(
				"id"            => 1,
				"title"         => Mage::helper('frans')->__("Newsletter"),
				"label"         => Mage::helper('frans')->__("Fran's News & Communications"),
				"group"         => 'email',
				"sort_order"    => 1
			),
			array(
				"id"            => 2,
				"title"         => Mage::helper('frans')->__("Print Catalogue"),
				"label"         => Mage::helper('frans')->__('Receive seasonal product catalogs'),
				"group"         => 'mail',
				"sort_order"    => 1
			)
		);

		// sorting disabled for now
		// $subscriptions = $this->sortSubscriptionsByKeys($subscriptions, $sortKeys);

		return $subscriptions;
	}

	/* TODO: This seemed like a nice idea but it's probably more trouble than it's worth to finish implementing
	public function sortSubscriptionsByKeys($subscriptions = array(), $sortKeys = false)
	{
		if($sortKeys)
		{
			if(is_string($sortKeys))
			{
				$sortKeys = array($sortKeys);
			}

			if(is_array($sortKeys))
			{
				foreach($sortKeys as $sortKey)
				{

				}
			}
		}
		return $subscriptions;
	} */

}
