<?php
class GoSolid_Frans_Model_PrintJobManager extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
    }

    //add a new print job to the print job queue.
    public function addPrintJob($printStationId, $printJobType, $document, $entityType, $entityId)
    {
        $printJob = Mage::getModel("frans/printJob");
        $printJob->setPrintStationId($printStationId);
        $printJob->setPrintJobType($printJobType);
        $printJob->setPrintStatus(GoSolid_Frans_Model_PrintJob::JOB_STATUS_QUEUED);
        $printJob->setDocument($document);

        $printJob->setEntityType($entityType);
        $printJob->setEntityId($entityId);

        $printJob->setCreatedAt(Mage::getSingleton('core/date')->gmtDate());
        $printJob->setUpdatedAt(Mage::getSingleton('core/date')->gmtDate());
        $printJob->save();

        return $printJob;

    }

    public function updatePrintJob($jobId, $status)
    {
        $printJob = Mage::getModel("frans/printJob")->load($jobId);


        if($printJob->getId() < 1)
        {
            throw new Exception("The print job with Id:$$jobId could not be found");
        }

        $printJob->setPrintStatus($status);
        $printJob->setUpdatedAt(Mage::getSingleton('core/date')->gmtDate());
        $printJob->save();


        return $printJob;
    }


}