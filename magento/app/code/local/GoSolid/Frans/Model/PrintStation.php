<?php
class GoSolid_Frans_Model_PrintStation extends Mage_Core_Model_Abstract 
{
    public function _construct()
    {
        parent::_construct(); 
        $this->_init('frans/printStation');
    }

    public function getActivePrintStations()
    {
        return $this->getCollection();
    }
	
}