<?php

/**
 * This is the central class for logging all activity history
 */
class GoSolid_Frans_Model_ActivityObserver extends Varien_Object
{
    /**
     * @param Varien_Event_Observer $observer
     */
    public function onOrderAfterPlace($observer)
    {
        /* @var Mage_Sales_Model_Order $order */
        $order = $observer->getOrder();

        Mage::helper('frans')->addActivity($order, 'sales/order', GoSolid_Frans_Model_Activity::TYPE_ORDER_CREATED);

    }

    /**
     * Logs cases where a multiship order was placed
     *
     * @param Varien_Event_Observer $observer
     */
    public function onCheckoutSubmitAllAfter($observer)
    {
        // we don't call order->place on multiship orders (and their children)
        // so this method is in place to take care of making sure we log the activity
        if ($orders = $observer->getOrders())
        {
            // assume we'd only get multiple orders for multiship
            if (count($orders) > 1)
            {
                foreach ($orders as $order)
                {
                    Mage::helper('frans')->addActivity($order, 'sales/order', GoSolid_Frans_Model_Activity::TYPE_ORDER_CREATED);
                }
            }
        }
    }

    public function onOrderItemSaveBefore($observer)
    {
        /* @var Mage_Sales_Model_Order_Item $orderItem */
        $orderItem = $observer->getItem();

        if ($orderItem->getId())
        {
            $description = "SKU: {$orderItem->getSku()}";
            Mage::helper('frans')->addChangeActivity($orderItem
                , GoSolid_Frans_Model_Activity::ENTITY_TYPE_ORDER
                , GoSolid_Frans_Model_Activity::TYPE_ORDER_ITEM_UPDATED
                , $orderItem->getOrder()->getId()
                , $description);
        }
    }

    public function onOrderAddressSaveBefore($observer)
    {
        /* @var Mage_Sales_Model_Order_Address $orderAddress */
        $orderAddress = $observer->getAddress();

        // only bother if it's an existing address
        if ($orderAddress->getId())
        {
            // build a custom description
            $description = "{$orderAddress->getAddressType()} address for {$orderAddress->getFirstname()} {$orderAddress->getLastname()}";
            Mage::helper('frans')->addChangeActivity(
                $orderAddress
                , GoSolid_Frans_Model_Activity::ENTITY_TYPE_ORDER
                , GoSolid_Frans_Model_Activity::TYPE_ORDER_ADDRESS_UPDATED
                , $orderAddress->getOrder()->getId()
                , $description);
        }
    }

    public function onOrderShipmentDeleteBefore($observer)
    {
        /* @var Mage_Sales_Model_Order_Address $orderAddress */
        $shipment = $observer->getShipment();



        $description = "Shipment #" . $shipment->getIncrementId() . " deleted.";



        Mage::helper('frans')->addActivity(
            $shipment
            , GoSolid_Frans_Model_Activity::ENTITY_TYPE_ORDER
            , GoSolid_Frans_Model_Activity::TYPE_SHIPMENT_DELETED
            , $shipment->getOrder()->getId()
            , $description);

        //var_dump($shipment->getId()); die;


    }

    public function onOrderSaveBefore($observer)
    {
        /* @var GoSolid_Frans_Model_Sales_Order $order */
        $order = $observer->getOrder();

        // only bother if the Order has an ID
        // and make sure we haven't indicated we want to suppress activity.
        if ($order->getId() && !$order->getSuppressActivity())
        {
            Mage::helper('frans')->addChangeActivity($order
                , GoSolid_Frans_Model_Activity::ENTITY_TYPE_ORDER
                , GoSolid_Frans_Model_Activity::TYPE_ORDER_UPDATED);
        }
    }

    public function onOrderChangePayment($observer)
    {
        /* @var GoSolid_Frans_Model_Sales_Order $order */
        $order = $observer->getOrder();
        $oldData = $this->_removePaymentData($observer->getOldPayment()->getData());
        $newData = $this->_removePaymentData($observer->getNewPayment()->getData());

        // changing payment means we always have an ID
        Mage::helper('frans')->addSpecificChangeActivity(
            GoSolid_Frans_Model_Activity::ENTITY_TYPE_ORDER
            , GoSolid_Frans_Model_Activity::TYPE_ORDER_PAYMENT_UPDATED
            , $order->getId()
            , $oldData
            , $newData
        );

    }

    /**
     * Handles any logic to run before a shipment is saved (no matter where it's created from
     *
     * @param $observer
     */
    public function onShipmentSaveBefore($observer)
    {
        /* @var Mage_Sales_Model_Order_Shipment $shipment */
        $shipment = $observer->getShipment();

        if (!$shipment->getId())
        {
            // no ID on shipment means its new, log it
            Mage::log("new shipment, logging activity");

            $oldData = array();
            $newData = $this->_removeShipmentData($shipment->getData());

            if ($tracks = $shipment->getAllTracks())
            {
                $newData['track_number'] = $tracks[0]->getTrackNumber();
            }

            // new shipment, log it
            $order = $shipment->getOrder();

            Mage::helper('frans')->addSpecificChangeActivity(
                GoSolid_Frans_Model_Activity::ENTITY_TYPE_ORDER
                , GoSolid_Frans_Model_Activity::TYPE_SHIPMENT_CREATED
                , $order->getId()
                , $oldData
                , $newData
            );
        }
        else
        {
            Mage::log("shipment has ID, doing nothing.");
        }
    }

    private function _removePaymentData($data)
    {
        $filterKeys = array('entity_id','store_id', 'method_instance', 'created_transaction', 'stripe_token');
        $filtered = $this->_removeDataByKey($data, $filterKeys);

        // also need to convert additional information
        // otherwise when it tries to diff arrays it complains about converting to a string.
        if (isset($filtered['additional_information']))
        {
            $additionalInfo = $filtered['additional_information'];

            if (is_array($additionalInfo) && count($additionalInfo) == 0)
            {
                unset($filtered['additional_information']);
            }
            else if (is_array($additionalInfo))
            {
                $filtered['additional_information'] = json_encode($additionalInfo);
            }
        }

        return $filtered;
    }

    private function _removeDataByKey($data, $filterKeys)
    {
        return array_diff_key( $data, array_flip($filterKeys));
    }

    private function _removeShipmentData($data)
    {
        // basically anything that isn't informative about the shipment
        $filterKeys = array('shipping_label', 'packages', 'store_id', 'cusotmer_id', 'global_currency_code'
            , 'order_currency_code', 'store_currency_code', 'base_currency_code', 'order_id'
        , 'store_to_base_rate', 'store_to_order_rate', 'base_to_global_rate', 'base_to_order_rate');
        return $this->_removeDataByKey($data, $filterKeys);
    }
}