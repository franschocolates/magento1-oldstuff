<?php
class GoSolid_Frans_Model_ShippingMethod extends Mage_Core_Model_Abstract 
{
    public function _construct()
    {
        parent::_construct(); 
        $this->_init('frans/shippingMethod');
    }

    public function loadByMethod($shippingMethod)
    {
        return $this->load($shippingMethod, 'shipping_method');
    }
	
}