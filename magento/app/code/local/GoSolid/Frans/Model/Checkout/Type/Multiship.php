<?php

/**
 *
 * Class GoSolid_Frans_Model_Checkout_Type_Multiship
 *
 * The class takes care of the logic of creating multiship quotes and orders
 * We have indirectly inherited from Onepage (via Standard), from which we don't need much functionality
 * But we need to share certain functionality between our Standard checkout and Multiship
 * Such as code to save payment and billing information
 *
 */
class GoSolid_Frans_Model_Checkout_Type_Multiship extends GoSolid_Frans_Model_Checkout_Type_Standard
{
    /**
     * @param GoSolid_Frans_Model_Sales_Quote $quote
     * @param bool $customer
     */
    public function init($quote, $customer = false)
    {
        $quote->doMultishipAllocations();
    }

    /**
     * Save shipping address for quote
     * Adopted from saveBilling from OnePage
     *
     * @param   array $data
     * @param   int $customerAddressId
     * @return  Mage_Sales_Model_Quote_Address
     */
    public function addShippingAddress($data, $addressId)
    {
        if (empty($data)) {
            return array('error' => -1, 'message' => Mage::helper('checkout')->__('Invalid data.'));
        }

        # if we only have a single shipping address, we
        # can just use that one
        if ($this->_useShippingAddress())
        {

            Mage::log("No shipping address associated yet, using pre-existing shipping address from quote.");
            $address = $this->getQuote()->getShippingAddress();
            // its possible they have only selected an address from their book
            // in which case we need to associate that now, or else it will get confused.
            if (!empty($customerAddressId))
            {
                $address->setCustomerId($customerAddressId);
            }
        }
        else
        {
            $address = Mage::getModel('sales/quote_address')->load($addressId);
        }

        $addresses = $this->_applyDataToAddress($address, $data);
        $address = $addresses["quote_address"];


        // if we don't have a customer address ID, we should make sure that the customer ID is empty
        if (empty($customerAddressId))
        {
            $address->setCustomerAddressId(null);

        }


        return $addresses;
    }

    /**
     * Sets the shipping info (address/product/quantity) for the order
     */
    public function setShippingInfo($shippingInfo)
    {
        # loop through and update items by address
        # and then collect totals
        $quote = $this->getQuote();
        $allAddresses = $quote->getAllShippingAddresses();

        # addresses will be keyed by ID - we'll need to look up each address as we find it in the list
        $quoteItemTotals = array();
        foreach ($shippingInfo as $addressId => $addressData)
        {
            $matchingAddresses = array_values(array_filter($allAddresses, function($address) use ($addressId) { return $address->getId() == $addressId; }));
            if (isset($matchingAddresses[0]))
            {
                $shippingAddress = $matchingAddresses[0];

                foreach ($addressData as $quoteItemId => $quoteItemData)
                {
                    $this->_addShippingItem($shippingAddress, $quoteItemId, $quoteItemData);

                    $quoteItemTotals[$quoteItemId] = (int) $quoteItemData['qty'] +
                        (isset($quoteItemTotals[$quoteItemId]) ? $quoteItemTotals[$quoteItemId] : 0);
                }
            }
            else
            {
                # TODO: how do we handle a missing address
            }
        }

        $quote->setTotalsCollectedFlag(false);
        $quote->collectTotals();
        $quote->doMultishipAllocations(true);

        return $this;
    }

    /**
     *
     * Removes a shipping address from a multiship order and
     * needs to recalculate totals afterwards
     * @param int $shippingAddressId
     */
    public function removeShippingAddress($shippingAddressId)
    {
        $removed = false;
        $quote = $this->getQuote();
        if ($shippingAddress = $quote->getAddressById($shippingAddressId))
        {
            if ($shippingAddress->getAddressType()==Mage_Sales_Model_Quote_Address::TYPE_SHIPPING)
            {
                # find all items to decrement
                foreach ($shippingAddress->getAllItems() as $addressItem)
                {
                    if ($addressItem->getQty() > 0
                        && ($quoteItem = $addressItem->getQuoteItem()))
                    {
                        $quoteItem->setQty($quoteItem->getQty() - $addressItem->getQty());
                    }
                }

                $quote->setTotalsCollectedFlag(false);
                $quote->removeAddress($shippingAddress->getId());
                $removed = true;
            }
        }

        $quote->collectTotals();
        $quote->doMultishipAllocations(true);

        return ($removed) ? $this : false;
    }

    /**
     * Create orders per each quote address
     *
     * @return Mage_Checkout_Model_Type_Multishipping
     */
    public function createOrders($coreSession, $checkoutSession)
    {
        $quote = $this->getQuote();
        $orderIds = array();
        $shippingAddresses = $quote->getAllShippingAddresses();
        $childOrders = array();

        if (!$quote->getCustomerId()) {
            $this->_prepareGuestQuote($quote);
        }

        $parentOrder = $this->_prepareParentOrder();
        $parentOrder->setSuppressActivity(true);

        try {
            $childOrderNum = 1;
            foreach ($shippingAddresses as $address) {
                $order = $this->_prepareChildOrder($address, $parentOrder, $childOrderNum);

                $childOrders[] = $order;
                Mage::dispatchEvent(
                    'checkout_type_multishipping_create_orders_single',
                    array('order'=>$order, 'address'=>$address)
                );

                $childOrderNum++;
            }

            # update parent order totals
            $parentOrder->setChildOrders($childOrders);
            $parentOrder->update();

            # placing the order does payment, so we only need to do that at the parent level
            # and then we just update the child orders
            $parentOrder->place();
            $parentOrder->save();
            $orderIds[$parentOrder->getId()] = $parentOrder->getIncrementId();

            /* @var GoSolid_Frans_Model_Sales_Order $order */
            foreach ($childOrders as $order)
            {
                // we set one for storage
                $order->setMultishipParentOrder($parentOrder);
                $order->setBillingAddressId($parentOrder->getBillingAddress()->getId());
                // need to call place to get any events to fire.
                $order->save()->place();
                $orderIds[$order->getId()] = $order->getIncrementId();
            }

            if ($parentOrder->getCanSendNewEmailFlag()){
                $parentOrder->sendNewOrderEmail();
            }

            $orders = array_merge( array( $parentOrder), $childOrders);

            $checkoutSession->setLastOrderId($parentOrder->getId());
            $checkoutSession->setLastQuoteId($quote->getId());
            $checkoutSession->setLastSuccessQuoteId($quote->getId());

            $quote
                ->setIsActive(false)
                ->save();

            $parentOrder->setSuppressActivity(false);

            Mage::dispatchEvent('checkout_submit_all_after', array('orders' => $orders, 'quote' => $quote));

            return $this;
        } catch (Exception $e) {
            Mage::dispatchEvent('checkout_multishipping_refund_all', array('orders' => $orders));
            throw $e;
        }
    }

    /**
     *
     * Updates an address attached to the quote
     * @param   array $data
     * @param   int $addressId
     */
    public function updateAddress($data, $addressId)
    {

        if ($address = $this->_getQuote()->getAddressById($addressId))
        {

            $customerAddressId = $address->getCustomerAddressId();

            // mark that we changed the address.
            $addresses = $this->_applyDataToAddress($address, $data, $customerAddressId);

            return $addresses;
        }

        return false;
    }

    public function setDeliveryInfo($addressData)
    {
        // loop through and make sure we have all addresses
        $errorIds = array();

        /* @var Mage_Sales_Model_Quote_Address $shippingAddress */
        foreach ($this->getQuote()->getAllShippingAddresses() as $shippingAddress)
        {
            if (isset($addressData[$shippingAddress->getId()]))
            {
                // only get the attributes we want
                $shippingData = $addressData[$shippingAddress->getId()];

                if (($preferredArrivalDate = $shippingData['preferred_arrival_date'])
                    && ($shippingMethod = $shippingData['shipping_method']))
                {
                    $date = new DateTime($preferredArrivalDate);
                    $today = new DateTime('now');

                    // we are not validating that it's in the list of valid dates just yet
                    // in case they submit right around the deadline and it's not longer valid
                    // which would be hard to explain while we're in the UI.
                    if ($date > $today)
                    {
                        $shippingAddress->setPreferredArrivalDate($preferredArrivalDate);
                        $shippingAddress->setShippingMethod($shippingMethod);
                        $shippingAddress->setGiftMessage($shippingData['gift_message']);
                        $shippingAddress->setSpecialInstructions($shippingData['special_instructions']);

                        $shippingAddress->save();
                    }
                    else
                    {
                        $errorIds[] = $shippingAddress->getId();
                    }
                }
                else
                {
                    $errorIds[] = $shippingAddress->getId();
                }
            }
            else
            {
                Mage::log("Missing shipping address with ID of {$shippingAddress->getId()} in data.");
                $errorIds[] = $shippingAddress->getId();
            }
        }

        if (count($errorIds) == 0)
        {
            // see if we need to set the billing address, as well
            // only do that if we have a registered customer with a billing address
            // and the billing address hasn't yet been saved
            if ($this->getQuote()->getCustomerId()
                && $this->getQuote()->getCustomer()->getDefaultBillingAddress()
                && !$this->getQuote()->getBillingAddress()->getFirstname())
            {
                $billingAddress = $this->getQuote()->getBillingAddress();
                $billingAddress->importCustomerAddress($this->getQuote()->getCustomer()->getDefaultBillingAddress());
            }

            // everything was processed, just save our quote, which saves our addresses
            // have to collect totals in order to set the shipping amount.
            $this->getQuote()->collectTotals();
            $this->getQuote()->save();
        }

        return count($errorIds) == 0 ? true : $errorIds;
    }

    /*
     * Should perform any necessary validation on the multiship quote
     * right now it removes any addresses with no quantity
     * Will also set overall quantities to be the correct values
     * so technically no validation at this point, just action.
     */
    public function validateShippingData()
    {
        $invalidAddresses = array();

        $quote = $this->getQuote();

        /* @var GoSolid_Frans_Model_Sales_Quote_Address $shippingAddress */
        foreach ($quote->getAllShippingAddresses() as $shippingAddress)
        {
            if (count($shippingAddress->getAllItems()) == 0)
            {
                $quote->removeAddress($shippingAddress->getId());
            }
            else if (!$shippingAddress->validateMinimumAmount(true))
            {
                $invalidAddresses[$shippingAddress->getId()] = 'min-amount';
            }
        }

        if (count($invalidAddresses) == 0)
        {
            // set the item quantities on the quote itself now
            // base it on the multishipping quantities
            /* @var Mage_Sales_Model_Quote_Item $item */
            foreach ($quote->getAllItems() as $item)
            {
                $multishipQty = $item->getMultishippingQty();

                if ($multishipQty > 0)
                {
                    $item->setQty($multishipQty);
                }
                else
                {
                    $quote->removeItem($item->getId());
                }
            }
        }

        return count($invalidAddresses) > 0 ? $invalidAddresses : true;

    }

    public function saveOrder($billingData, $paymentData)
    {
        $billingErrors = $this->saveBilling($billingData);
        $paymentErrors = $this->savePayment($paymentData);

        if (!is_array($billingErrors) && $billingErrors && !is_array($paymentErrors) && $paymentErrors)
        {
            $this->_getQuote()->save();

            // as originally written, just need checkout/customer session
            // it works from there, but should probably be refactored
            $checkoutSession = Mage::getSingleton('checkout/session');
            $this->createOrders(Mage::getSingleton('core/session'), $checkoutSession);
            return true;
        }

        return array(
            'billing_errors' => $billingErrors
        , 'payment_errors' => $paymentErrors
        );
    }

    /**
     * @return GoSolid_Frans_Model_Sales_Order
     */
    protected function _prepareParentOrder()
    {
        $quote = $this->getQuote();
        $quote->unsReservedOrderId();
        $quote->reserveOrderId();
        $reservedOrderId = $quote->getReservedOrderId() . "-0";
        $quote->setReservedOrderId($reservedOrderId);
        $quote->collectTotals();

        $convertQuote = Mage::getSingleton('sales/convert_quote');
        $order = $convertQuote->toOrder($quote);
        $order->setQuote($quote);
        $order->setBillingAddress(
            $convertQuote->addressToOrderAddress($quote->getBillingAddress())
        );

        $order->setIsVirtual(1);
        $order->setIsMultishipParent(true);

        $order->setPayment($convertQuote->paymentToOrderPayment($quote->getPayment()));
        return $order;
    }

    /**
     * Prepare order based on quote address
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  Mage_Sales_Model_Order
     * @throws  Mage_Checkout_Exception
     */
    protected function _prepareChildOrder(Mage_Sales_Model_Quote_Address $address,
                                          Mage_Sales_Model_Order $parentOrder, $childOrderNum)
    {
        $quote = $this->getQuote();

        $incrementId = str_replace('-0', '-' . $childOrderNum, $parentOrder->getIncrementId());

        $quote->setReservedOrderId($incrementId);
        $quote->collectTotals();


        $convertQuote = Mage::getSingleton('sales/convert_quote');
        $order = $convertQuote->addressToOrder($address);
        $order->setQuote($quote);
        $order->setIsMultishipParent(false);


        $order->setPayment($this->_getChildOrderPayment($quote));
        # child orders are processing by default.
        $order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true);

        if ($address->getAddressType() == 'billing') {
            $order->setIsVirtual(1);
        } else {
            $order->setShippingAddress($convertQuote->addressToOrderAddress($address));
        }

        # $order->setPayment($convertQuote->paymentToOrderPayment($quote->getPayment()));
        /*
        if (Mage::app()->getStore()->roundPrice($address->getGrandTotal()) == 0) {
            $order->getPayment()->setMethod('free');
        }
		*/

        foreach ($address->getAllItems() as $item) {
            $_quoteItem = $item->getQuoteItem();
            if (!$_quoteItem) {
                throw new Mage_Checkout_Exception(Mage::helper('checkout')->__('Item not found or already ordered'));
            }
            $item->setProductType($_quoteItem->getProductType())
                ->setProductOptions(
                    $_quoteItem->getProduct()->getTypeInstance(true)->getOrderOptions($_quoteItem->getProduct())
                );
            $orderItem = $convertQuote->itemToOrderItem($item);
            if ($item->getParentItem()) {
                $orderItem->setParentItem($order->getItemByQuoteItemId($item->getParentItem()->getId()));
            }
            $order->addItem($orderItem);
        }

        return $order;
    }

    /**
     *
     * Gets the quote we are working with.
     *
     * @return Mage_Sales_Model_Quote
     */
    private function _getQuote()
    {
        return $this->getQuote();
    }

    protected function _getChildOrderPayment($quote)
    {
        $payment = Mage::getModel('sales/quote_payment');

        // have to reload the quote for the payment to work properly
        $payment->setStoreId(1);
        #$payment->setQuote($quote);
        #$payment->importData(array ('method' => 'multiship'));
        $payment = $payment->importMultishipData();
        $payment->setQuoteId($quote->getId());
        $payment->save();


        $orderPayment = Mage::getModel('sales/order_payment')
            ->setStoreId($payment->getStoreId())
            ->setCustomerPaymentId($payment->getCustomerPaymentId());
        $orderPayment->setMethod('multiship');
        return $orderPayment;
    }

    /**
     * Validate customer data and set some its data for further usage in quote
     * Will return either true or array with error messages
     *
     * @param array $data
     * @return true|array
     */
    protected function _validateCustomerData(array $data)
    {
        $thisIsALongWord = 'things';

        /** @var $customerForm Mage_Customer_Model_Form */
        $customerForm = Mage::getModel('customer/form');
        $customerForm->setFormCode('multiship_shipping_address_edit')
            ->setIsAjaxRequest(Mage::app()->getRequest()->isAjax());

        $quote = $this->getQuote();
        if ($quote->getCustomerId()) {
            $customer = $quote->getCustomer();
            $customerForm->setEntity($customer);
            $customerData = $quote->getCustomer()->getData();
        } else {
            /* @var $customer Mage_Customer_Model_Customer */
            $customer = Mage::getModel('customer/customer');
            $customerForm->setEntity($customer);
            $customerRequest = $customerForm->prepareRequest($data);
            $customerData = $customerForm->extractData($customerRequest);
        }

        $customerErrors = $customerForm->validateData($customerData);
        if ($customerErrors !== true) {
            return array(
                'error'     => -1,
                'message'   => implode(', ', $customerErrors)
            );
        }

        if ($quote->getCustomerId()) {
            return true;
        }

        $customerForm->compactData($customerData);

        $result = $customer->validate();
        if (true !== $result && is_array($result)) {
            return array(
                'error'   => -1,
                'message' => implode(', ', $result)
            );
        }

        // copy customer data to quote
        Mage::helper('core')->copyFieldset('customer_account', 'to_quote', $customer, $quote);

        return true;
    }

    /**
     * Add quote item to specific shipping address based on customer address id
     * This was adopted from core Magento multishipping
     *
     * @param Mage_Sales_Model_Quote_Address $quoteAddress
     * @param int $quoteItemId
     * @param array $data array('qty'=>$qty)
     * @return GoSolid_Frans_Model_Checkout_Type_Multiship
     */
    protected function _addShippingItem($quoteAddress, $quoteItemId, $data)
    {
        $qty       = isset($data['qty']) ? (int) $data['qty'] : 0;
        //$qty       = $qty > 0 ? $qty : 1;
        $quoteItem = $this->getQuote()->getItemById($quoteItemId);

        if ($quoteAddress && $quoteItem)
        {
            $quoteAddressItem = $quoteAddress->getItemByQuoteItemId($quoteItemId);

            if ($qty === 0 && $quoteAddressItem && $quoteAddressItem->getId())
            {
                $quoteAddress->removeItem($quoteAddressItem->getId());
                return $this;
            }

            //$quoteItem->setMultishippingQty((int)$quoteItem->getMultishippingQty()+$qty);
            //$quoteItem->setQty($quoteItem->getMultishippingQty());

            if ($quoteAddressItem)
            {
                $quoteAddressItem->setQty($qty);
            }
            else if ($qty > 0)
            {
                $quoteAddress->addItem($quoteItem, $qty);
            }

            /**
             * Require shiping rate recollect
             */
            $quoteAddress->setCollectShippingRates(true);
        }
        return $this;
    }

    /**
     *
     * Determines whether to use the default shipping address. Happens when a guest checkout
     * And no shipping address has been set yet, but the record exists
     */
    protected function _useShippingAddress()
    {
        return $this->_isEmptyAddress($this->getQuote()->getShippingAddress());
    }

    protected function _isEmptyAddress($address)
    {
        return $address->getFirstname() == '' && $address->getLastname() == '';
    }

    /**
     * By default it copies the quantities from the quote, which is everything
     * We need to adjust to be the quantities specifically in this address
     *
     * @param $address
     */
    protected function _adjustChildQtys($address)
    {

    }

}