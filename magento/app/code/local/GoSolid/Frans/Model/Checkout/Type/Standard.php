<?php
/**
 * Our custom OnePage checkout for single shipments
 *
 * Could possibly override Onepage, but seemed cleaner to do it separately in case it doesn't work out
 */
class GoSolid_Frans_Model_Checkout_Type_Standard extends Mage_Checkout_Model_Type_Onepage
{
    /**
     * Initialize quote state to be valid for one page checkout
     *
     * @return Mage_Checkout_Model_Type_Onepage
     */
    public function initCheckout()
    {
        $checkout = $this->getCheckout();
        $customerSession = $this->getCustomerSession();
        if (is_array($checkout->getStepData())) {
            foreach ($checkout->getStepData() as $step=>$data) {
                // GS override to only allow shipping initially, there is no login/billing steps
                if ($step !== 'shipping') {
                    $checkout->setStepData($step, 'allow', false);
                }
            }
        }

        /**
         * Reset multishipping flag before any manipulations with quote address
         * addAddress method for quote object related on this flag
         */
        if ($this->getQuote()->getIsMultiShipping()) {
            $this->getQuote()->setIsMultiShipping(false);
            // remove any extraneous shipping addresses
            $shippingAddresses = $this->getQuote()->getAllShippingAddresses();
            $mainShipping = $this->getQuote()->getShippingAddress();

            if (count($shippingAddresses) > 1)
            {
                foreach ($shippingAddresses as $shippingAddress)
                {
                    if ($shippingAddress->getId() != $mainShipping->getId())
                    {
                        $this->getQuote()->removeAddress($shippingAddress->getId());
                    }
                }
            }
            $this->getQuote()->collectTotals()->save();
        }

        /*
        * want to load the correct customer information by assigning to address
        * instead of just loading from sales/quote_address
        */
        $customer = $customerSession->getCustomer();
        if ($customer) {
            $this->getQuote()->assignCustomer($customer);
        }
        return $this;
    }

    /**
     * Specify checkout method
     * Our default step is shipping
     *
     * @param   string $method
     * @return  array
     */
    public function saveCheckoutMethod($method)
    {
        if (empty($method)) {
            return array('error' => -1, 'message' => Mage::helper('checkout')->__('Invalid data.'));
        }

        $this->getQuote()->setCheckoutMethod($method)->save();
        // Our default step is shipping
        $this->getCheckout()->setStepData('shipping', 'allow', true);
        return array();
    }

    public function setDeliveryInfo($deliveryData)
    {
        $shippingAddress = $this->getQuote()->getShippingAddress();
        $valid = false;

        if (($preferredArrivalDate = $deliveryData['preferred_arrival_date'])
            && ($shippingMethod = $deliveryData['shipping_method']))
        {
            $date = new DateTime($preferredArrivalDate);
            $today = new DateTime('now');

            // we are not validating that it's in the list of valid dates just yet
            // in case they submit right around the deadline and it's not longer valid
            // which would be hard to explain while we're in the UI.
            if ($date > $today)
            {
                $shippingAddress->setPreferredArrivalDate($preferredArrivalDate);
                $shippingAddress->setShippingMethod($shippingMethod);
                $shippingAddress->setGiftMessage($deliveryData['gift_message']);
                $shippingAddress->setSpecialInstructions($deliveryData['special_instructions']);

                $shippingAddress->save();
                $valid = true;
            }
        }

        if ($valid)
        {
            // see if we need to set the billing address, as well
            // only do that if we have a registered customer with a billing address
            // and the billing address hasn't yet been saved
            if ($this->getQuote()->getCustomerId()
                && $this->getQuote()->getCustomer()->getDefaultBillingAddress()
                && !$this->getQuote()->getBillingAddress()->getFirstname())
            {
                $billingAddress = $this->getQuote()->getBillingAddress();
                $billingAddress->importCustomerAddress($this->getQuote()->getCustomer()->getDefaultBillingAddress());
            }

            // everything was processed, just save our quote, which saves our addresses
            // have to collect totals in order to set the shipping amount.
            $this->getQuote()->collectTotals();
            $this->getQuote()->save();
        }

        return $valid ? true : array('error' => 'Delivery data was incomplete');
    }

    public function saveOrder($billingData = array(), $paymentData = array())
    {
        $billingErrors = $this->saveBilling($billingData);
        $paymentErrors = $this->savePayment($paymentData);

        if (!is_array($billingErrors) && $billingErrors && !is_array($paymentErrors) && $paymentErrors)
        {
            $this->getQuote()->save();

            return parent::saveOrder();
        }

        return array(
            'billing_errors' => $billingErrors
        , 'payment_errors' => $paymentErrors
        );
    }

    public function saveBilling($data = array())
    {
        Mage::log("About to try and save this data: " . print_r($data, true));

        // just need to save the address
        $billingAddress = $this->getQuote()->getBillingAddress();

        // tell it we are doing billing, since that involves special logic.
        $result =  $this->_applyDataToAddress($billingAddress, $data, true, true);

        if (is_array($result) && isset($result['errors']))
        {
            return $result;
        }

        // handle the case that they may need to save the billing address but don't have one
        $quote = $this->getQuote();
        if ($quote->getCustomer()
            && isset($data['save_in_address_book'])
            && $data['save_in_address_book']
            && !$quote->getCustomer()->getDefaultBillingAddress())
        {
            Mage::log("They don't have a default billing but elected to save this one. Assigning to default billing.");
            $this->_saveDefaultBillingAddress($quote->getCustomer(), $billingAddress);
        }

        // handle the "save as default billing address" checkbox
        if ($quote->getCustomer()
            && isset($data['default_billing_address'])
            && $data['default_billing_address'])
        {
            $this->_saveDefaultBillingAddress($quote->getCustomer(), $billingAddress);
        }

        $quote->setCustomerEmail($data["email"]);
        $quote->save();

        return true;
    }

    /**
     * Overridden so that we don't just use customer address data when customer address ID specified.
     *
     * @param array $data
     * @param int $customerAddressId
     * @return Mage_Checkout_Model_Type_Onepage|Mage_Sales_Model_Quote_Address
     */
    public function saveShipping($data, $customerAddressId)
    {
        $shippingAddress = $this->getQuote()->getShippingAddress();
        $result = $this->_applyDataToAddress($shippingAddress, $data, $customerAddressId, false, false);
        $quoteAddress = $result["quote_address"];

        if (!is_array($quoteAddress) || !isset($quoteAddress['error']))
        {
            $quoteAddress->setCollectShippingRates(true);
            $this->getQuote()->collectTotals()->save();

        }
        return $result;
    }

    /**
     * Saves payment. Adopted from Onepage checkout savePayment
     *
     * @param   array $data
     * @return  array
     */
    public function savePayment($data)
    {
        if (empty($data)) {
            return array('error' => -1, 'message' => Mage::helper('frans')->__('Invalid data.'));
        }
        $quote = $this->getQuote();

        // first, see if we are using stripe; if so, then we need some extra logic
        // to populate records that stripe needs
        if ($data['method'] == 'stripe')
        {
            // we always set an existing stripe customer ID if we have it
            // set the stripe customer ID from the existing customer record
            if ($quote->getCustomer() && $quote->getCustomer()->getStripeCustomerId())
            {
                $data['stripe_customer_id'] = $quote->getCustomer()->getStripeCustomerId();

                // the other thing that matters is whether they checked create_stripe_customer (ie Save Card)
                // but that should be brought in automatically by the importProcess
            }

            // modify the inputs to take the proper stripe option
            if (isset($data['use_saved_card']) && $data['use_saved_card'] && $data['stripe_customer_id'])
            {
                // option 1 - use saved card - don't need a token, even if provided
                $data['stripe_token'] = '';
            }
            else
            {
                // option 2 & 3
                // in one, we want to save the new card
                // in one, we don't
                // the data should actually come through the right way.
                // in that either payment[create_stripe_customer] will be true or false
                // the token should stay as is
            }
        }

        if ($quote->isVirtual()) {
            $quote->getBillingAddress()->setPaymentMethod(isset($data['method']) ? $data['method'] : null);
        } else {
            $quote->getShippingAddress()->setPaymentMethod(isset($data['method']) ? $data['method'] : null);
        }

        // for now, shipping totals may be affected by payment method, don't recalculate shipping

        $payment = $quote->getPayment();
        $payment->importData($data);

        return true;
    }

    /**
     *
     * Saves an address based on form data. Can be a new or existing address
     * @param 	Mage_Sales_Model_Quote_Address	$address
     * @param   array $data
     * @param   int $customerAddressId
     * @return  Mage_Sales_Model_Quote_Address
     */




    /*
     * TODO: not sure if we need this intire function.
     */
    protected function _applyDataToAddress($address, $data, $isBilling = false, $saveToCustomer = true)
    {


        // well this is fun -
        // if we have a customer address ID, or the address already has one,
        // then that means first we update the customer address,
        // then we pull it over into our quote address.
        //




        if(empty($customerAddressId) && !$isBilling && Mage::getSingleton('customer/session')->isLoggedIn()) {

            $customerAddress = Mage::getModel('customer/address');
            $customerAddress->setData($data)
                ->setCustomerId($this->getQuote()->getCustomerId())
                ->setIsDefaultBilling('0')
                ->setIsDefaultShipping('0')
                ->setSaveInAddressBook('1');
            try {
                if($saveToCustomer == true)
                {
                    $customerAddress->save();
                    $customerAddressId = $customerAddress->getEntityId();
                }


            }
            catch (Exception $ex) {
                Mage::logException($ex->getMessage());
                return array('error' => 1,
                    'message' => Mage::helper('checkout')->__('Unable to save shipping address to address book.')
                );
            }
        } elseif(empty($customerAddressId) && !$isBilling && !Mage::getSingleton('customer/session')->isLoggedIn()){

            $customerAddress = Mage::getModel('customer/address');
            $customerAddress->setData($data);
        }


        // sometimes we don't want to update the customer address just yet
        // for instance, when billing.

        $updatingCustomerAddress = ($customerAddress && $customerAddress->getId());

        // if we are updating a billing address, then we may not want to save it
        // it depends on whether they elect to do save in address book
        if ($isBilling)
        {
            $updatingCustomerAddress = $updatingCustomerAddress
                && isset($data['save_in_address_book'])
                && $data['save_in_address_book'];
        }


        // we need to use the multiship form code
        // if we use customer_address_edit, it will wipe out the values that aren't included in our form
        $addressFormCode = ($isBilling) ? 'customer_address_edit' : 'multiship_shipping_address_edit';

        /* @var $addressForm Mage_Customer_Model_Form */
        $addressForm = Mage::getModel('customer/form');
        $addressForm->setFormCode($addressFormCode)
            ->setEntityType('customer_address')
            ->setIsAjaxRequest(Mage::app()->getRequest()->isAjax());

        $addressToSave = (!empty($customerAddressId)) ? Mage::getModel('customer/address')->load($customerAddressId) : $address;


        if(!$addressToSave){
            $addressToSave = $address;
        }

        $addressToSave = (!empty($customerAddressId)) ? Mage::getModel('customer/address')->load($customerAddressId) : $address;

        $addressForm->setEntity($addressToSave);

        // emulate request object
        $addressData    = $addressForm->extractData($addressForm->prepareRequest($data));
        $addressErrors  = $addressForm->validateData($addressData);
        if ($addressErrors !== true) {
            return array('error' => 1, 'message' => array_values($addressErrors));
        }

        $addressForm->compactData($addressData);

        //unset billing address attributes which were not shown in form
        foreach ($addressForm->getAttributes() as $attribute) {
            if (!isset($data[$attribute->getAttributeCode()])) {
                $addressToSave->setData($attribute->getAttributeCode(), NULL);
            }
        }

        // validate billing address
        if (($validateRes = $addressToSave->validate()) !== true) {
            return array('error' => 1, 'message' => $validateRes);
        }

        // hey, everything validated. we now need to save the customer address
        // if that's what we've been doing. otherwise,
        // we are done
        if ($updatingCustomerAddress)
        {

            // we aren't going to validate again, just import
            // since the customer address just validated.
            // pick your variable, this is also the addressToSave
            $addressToSave->save();

            $address->importCustomerAddress($addressToSave);
        }

        if (!$address->getAddressType())
        {
            $address->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_SHIPPING);
        }

        # we will assume that we aren't the same as billing
        $address->setSameAsBilling(false);

        // if it's a shipping address, we need to clear out any previously selected shipping data
        // because it may no longer be valid (ie, it was in the past, or they changed their location
        if ($address->getAddressType() == Mage_Sales_Model_Quote_Address::TYPE_SHIPPING)
        {
            $address->setPreferredArrivalDate(null);
            $address->setPlannedShipDate(null);
            $address->setShippingMethod(null);
        }

        $addresses = array("quote_address" => $address, "customer_address" => $customerAddress);

        return $addresses;

    }

    /**
     * Saves a default billing address into the address book,
     * overwriting any already present
     *
     * @param Mage_Customer_Model_Customer $customer
     * @param Mage_Sales_Model_Quote_Address $quoteAddress
     */
    protected function _saveDefaultBillingAddress($customer, $quoteAddress)
    {

        // first get the customer's existing default billing address, if there is one
        $customerDefaultBillingAddress = $customer->getDefaultBillingAddress();

        // get the billing address from the quote
        $quoteDefaultBillingAddress = $quoteAddress->exportCustomerAddress();

        if ($customerDefaultBillingAddress)
        {
            $customerDefaultBillingAddress->setUpdatedAt(Mage::helper('core')->formatDate(time()))
                ->setEntityTypeId($quoteDefaultBillingAddress->getEntityTypeId())
                ->setAttributeSetId($quoteDefaultBillingAddress->getAttributeSetId())
                ->setIncrementId($quoteDefaultBillingAddress->getIncrementId())
                ->setIsActive(1)
                ->setFirstname($quoteDefaultBillingAddress->getFirstname())
                ->setLastname($quoteDefaultBillingAddress->getLastname())
                ->setComapny($quoteDefaultBillingAddress->getCompany())
                ->setCity($quoteDefaultBillingAddress->getCity())
                ->setRegion($quoteDefaultBillingAddress->getRegion())
                ->setPostcode($quoteDefaultBillingAddress->getPostcode())
                ->setCountryId($quoteDefaultBillingAddress->getCountryId())
                ->setTelephone($quoteDefaultBillingAddress->getTelephone())
                ->setEmail($quoteDefaultBillingAddress->getEmail())
                ->setAddressDeliveryType($quoteDefaultBillingAddress->getAddressDeliveryType())
                ->setLocationHash($quoteDefaultBillingAddress->getLocationHash())
                ->setStreet($quoteDefaultBillingAddress->getStreet())
                ->setProposedAddress($quoteDefaultBillingAddress->getProposedAddress())
                ->setRegionId($quoteDefaultBillingAddress->getRegionId())
                ->save();
        }
        else
        {
            $quoteDefaultBillingAddress->setIsDefaultBilling(true);
            $customer->addAddress($quoteDefaultBillingAddress);
            $customer->save();
            $quoteAddress->setCustomerAddressId($quoteDefaultBillingAddress->getId());
        }

    }

}