<?php

class GoSolid_Frans_Model_Checkout_Observer
{
	/***
	 * Makes sure that once items are added to a multiship quote,
	 * The qtys are associated with an address.
     *
     * NOTE: This function is not currently called, as we don't want to associate items with an address
     * it is just disabled by removing the events node in Magento
     * If you re-enable the observer, then please remove this comment.
     * Leaving here for now in case we need it again; eventually, this should be removed.
	 */
	public function onProductAdd($observer)
	{
		$cartSession = Mage::getSingleton('checkout/cart');
		/* @var $quote Mage_Sales_Model_Quote */
		$quote = $cartSession->getQuote();
		
		if ($quote->getIsMultiShipping())
		{
			$product = $observer->getProduct();
			
			$this->_ensureExistsInAddress($quote, $product);
		}
		
		return $observer;
	}

	/**
	 * 
	 * Makes sure a product exists in at least one address if multiship
	 * @param Mage_Sales_Model_Quote $quote
	 * @param Mage_Catalog_Model_Product $product
	 */
	protected function _ensureExistsInAddress($quote, $product)
	{
		Mage::log("Ensuring that product with ID of {$product->getId()} exists in an address (and that qty matches), since we are in multiship.");
		# add it to the first address
		# if it's not associated
		
		$quoteItem = $quote->getItemByProduct($product);
		
		# make sure it exists in at least one address
		$totalQty = 0;
		/* @var $address Mage_Sales_Model_Quote_Address */
		foreach ($quote->getAllAddresses() as $address)
		{
			if ($addressItem = $address->getItemByQuoteItemId($quoteItem->getId()))
			{
				$totalQty += $addressItem->getQty();
			}
		}
		
		# see if there is a difference between the address total
		# and the total on the item
		# if so, then update/add the necessary qty to the first address
		$qtyDifference = $quoteItem->getQty() - $totalQty; 
		if ($qtyDifference > 0)
		{
			# add it to the first address
			/* @var $shippingAddress Mage_Sales_Model_Quote_Address */
			$shippingAddress = $quote->getShippingAddress();
			
			if ($shippingAddressItem = $shippingAddress->getItemByQuoteItemId($quoteItem->getId()))
			{
				Mage::log("New item already exists on first address, upping qty by $qtyDifference");
				$shippingAddressItem->setQty($shippingAddressItem->getQty() + $qtyDifference);
				# if we don't save, it doesn't get persisted
				$shippingAddressItem->save();
			}
			else
			{
				Mage::log("New item does not exist on first item, adding with qty of $qtyDifference");
				$shippingAddress->addItem($quoteItem, $qtyDifference);				
				$shippingAddress->save();
			}
			
			# we have to reload the quote. If we don't, the quote has an old copy
			# of the quoteItem, with no ID. As a result, totals collection fails because
			# it can't look up the item by ID.
			$reloadedQuote = Mage::getModel('sales/quote')->load($quote->getId());
			$reloadedQuote->collectTotals()
						  ->save();
		}
	}
	
}