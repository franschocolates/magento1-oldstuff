<?php

class GoSolid_Frans_Model_Activity extends Mage_Core_Model_Abstract
{
    const ENTITY_TYPE_ORDER = 'sales/order';

    const TYPE_ORDER_CREATED = 'order_created';
    const TYPE_ORDER_UPDATED = 'order_updated';
    const TYPE_ORDER_ITEM_UPDATED = 'order_item_updated';
    const TYPE_ORDER_ADDRESS_UPDATED = 'order_address_updated';
    const TYPE_ORDER_PAYMENT_UPDATED = 'order_payment_updated';
    const TYPE_SHIPMENT_DELETED = 'order_shipment_deleted';
    const TYPE_SHIPMENT_CREATED = 'order_shipment_created';

    const FIELD_DESCRIPTION = 'description';

    public function _construct()
    {
        parent::_construct();
        $this->_init('frans/activity');
    }


}