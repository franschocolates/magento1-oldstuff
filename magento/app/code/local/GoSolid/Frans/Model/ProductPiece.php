<?php

/**
 * Class GoSolid_Frans_Model_RetailStore
 *
 * @method bool getUseOnFrontend()
 * @method GoSolid_Frans_Model_RetailStore setUseOnFrontend(bool $val)
 *
 */
class GoSolid_Frans_Model_ProductPiece extends Mage_Core_Model_Abstract
{

	//TODO: Move this model into catalog/product

	const STATUS_DISABLED = 0;
	const STATUS_ENABLED = 1;

    public function _construct()
    {
        parent::_construct(); 
        $this->_init('frans/productPiece');
    }

    //returns the path relative to the media directory.
    public function getMediaPath()
    {
        return "product_pieces";
    }

	public function getPath(){
		return str_replace('\\', '/', $this->getPieceImage());
	}

    public function getCollection($status = null){

	    // filter to show only enabled pieces
    	$collection = parent::getCollection();

	    if($status){
		    $collection->addFieldToFilter('is_active', $status);
	    }
    	
    	return $collection;
    }

}