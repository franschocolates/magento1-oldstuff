<?php
class GoSolid_Frans_Model_ShippingBlackoutDate extends Mage_Core_Model_Abstract 
{
    public function _construct()
    {
        parent::_construct(); 
        $this->_init('frans/shippingBlackoutDate');
    }
	
    public function getFutureDates()
    {
    	// find any today or later, since today could also be blacked out
    	$collection = $this->getCollection()
    				->addFieldToFilter('blackout_date', 
    					array('gteq' => date("Y-m-d", Mage::getModel('core/date')->timestamp(time())))
    					)
    				->setOrder('blackout_date', 'ASC');
    	return $collection->load();
	}
}