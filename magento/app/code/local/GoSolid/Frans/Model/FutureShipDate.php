<?php
class GoSolid_Frans_Model_FutureShipDate extends Mage_Core_Model_Abstract 
{
	const AVAILABILITY_AVAILABLE = 0;
	const BLACKOUT_TYPE_RESIDENTIAL = 1;
	const BLACKOUT_TYPE_COMMERCIAL = 2;
	const BLACKOUT_TYPE_ALL = 3;

    const DATE_FORMAT = "Y-m-d";

    public function _construct()
    {
        parent::_construct(); 
        $this->_init('frans/futureShipDate');
    }
    
    /*
     * Looks up the message for a specific date
     */
    public function getMessage()
    {
    	if ($this->getMessageId())
    	{
    		return Mage::getModel('frans/futureShipMessage')->load($this->getMessageId())->getMessageText();
    	}
    	
    	return false;
    }

    public function isAvailable()
    {
        return $this->getAvailabilityType() == self::AVAILABILITY_AVAILABLE;
    }

    public function getFutureDates($address, $specificDate = false)
    {
        // we basically will want anything that's an available date
        // or a blackout date for this type of address
    	$availabilityTypes = array( self::AVAILABILITY_AVAILABLE, self::BLACKOUT_TYPE_ALL );
    	
    	if ($address->getIsResidential())
    	{
    		Mage::log("Address {$address->getId()} is residential, using residential blackouts.");
            $availabilityTypes[] = self::BLACKOUT_TYPE_RESIDENTIAL;
    	}
    	else
    	{
    		Mage::log("Address {$address->getId()} is not residential, using commercial blackouts.");
            $availabilityTypes[] = self::BLACKOUT_TYPE_COMMERCIAL;
    	}

        $collection = $this->getCollection();

        if ($specificDate)
        {
            $collection->addFieldToFilter('calendar_date', $specificDate);
        }
        else
        {
            $collection->addFieldToFilter('calendar_date',
                array('gteq' => date("Y-m-d", Mage::getModel('core/date')->timestamp(time())))
    					)
                ->addFieldToFilter('availability_type', array('in' => $availabilityTypes))
                ->setOrder('calendar_date', 'ASC');
        }

    	return $collection->load();
    }
    
    public function getAvailabilityTypeOptionArray()
    {
    	return array(
    		self::AVAILABILITY_AVAILABLE => 'Available'
    		, self::BLACKOUT_TYPE_RESIDENTIAL => 'Not Available - Residential'
    		, self::BLACKOUT_TYPE_COMMERCIAL => 'Not Available - Commercial'
    		, self::BLACKOUT_TYPE_ALL => 'Not Available'
    	);
    }
}