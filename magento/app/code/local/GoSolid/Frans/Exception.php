<?php
/**
 * Custom logic exception for having more detailed information
 *
 * @package    GoSolid Frans
 * @author      goSolid
 */
class GoSolid_Frans_Exception extends Exception
{
    protected $_code = null;

    public function __construct($message = null, $code = 0)
    {
        $this->_code = $code;
        parent::__construct($message, 0);
    }
}
