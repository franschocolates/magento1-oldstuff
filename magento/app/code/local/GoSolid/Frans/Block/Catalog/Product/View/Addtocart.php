<?php
/**
 * Created by goSolid.
 * Date: 8/10/15
 * Time: 3:30 PM
 */
class GoSolid_Frans_Block_Catalog_Product_View_Addtocart extends Mage_Catalog_Block_Product_View {

	protected function _prepareLayout()
	{
		return Mage_Core_Block_Template::_prepareLayout();
	}

	public function getJsonAvailabilityStatuses()
	{
		return Mage::helper('core')->jsonEncode($this->getAvailabilityStatuses());
	}

}