<?php
/**
 * Created by goSolid.
 * Date: 10/6/14
 * Time: 2:31 PM
 */ 
class GoSolid_Frans_Block_Catalog_Product_View_Attributes extends Mage_Catalog_Block_Product_View_Attributes {

	protected $_attributes;

	public function _beforeToHtml()
	{
		$this->_setAttributes();
	}

	public function getJsonAttributes()
	{
		return Mage::helper('core')->jsonEncode($this->_attributes);
	}

	public function getAttributes()
	{
		return $this->_attributes;
	}

	public function getSelectedProductAttributeHtml()
	{
		$selectedProductId = $this->getSelectedProductId();
		return $this->_attributes[$selectedProductId];
	}

	protected function _setAttributes()
	{
		$attributes = array();
		$productIds = $this->getProductIds();
		foreach($productIds as $productId){
			if($additional = $this->_getAdditionalData($productId)){
				$attributes[$productId] = '';
				foreach($additional as $data){
					$attributes[$productId] .= $this->_getExpanderHtml($data, $productId);
				}
			}
		}
		$this->_attributes = $attributes;
		return $this;
	}

	protected function _getExpanderHtml($data, $productId)
	{
		return $this->getChild('expander')->setProductId($productId)->setAttributeData($data)->toHtml();
	}

	// TODO: If the page is loading slowly, it's because we're loading full products in this function
	protected function _getAdditionalData($productId)
	{
		// get additional data for parent product (or current product if viewing simple product)
		$parentProduct = $this->getProduct();
		$parentProductData = $this->_prepareAdditionalData($parentProduct);

		// get additional data for child product if viewing configurable product
		$productData = array();
		if($parentProduct->getId() != $productId){
			// parent product and the product requested are not one and the same
			$product = Mage::getModel('catalog/product')->load($productId);
			$productData = $this->_prepareAdditionalData($product);
			$productData['description']['label'] = $parentProductData['description']['label'];
		}

		// remove attributes with null values from the child product array so they won't override parent data in array merge
		foreach($productData as $code => $attribute){
			if(empty($attribute['value'])){
				unset($productData[$code]);
			}
		}

		// merge the parent and child arrays, allowing child data to override parent data wherever it exists
		$data = array_merge($parentProductData, $productData);

		// if any attributes still have null values after the array, remove them so they won't show up on the PDP
		foreach($data as $code => $attribute){
			if(empty($attribute['value'])){
				unset($data[$code]);
			}
		}

		return $data;
	}

	/**
	 * $excludeAttr is optional array of attribute codes to
	 * exclude them from additional data array
	 */
	protected function _prepareAdditionalData($product, array $excludeAttr = array())
	{
		$data = array();
		$attributes = $product->getAttributes();
		foreach($attributes as $attribute) {
			if ($attribute->getIsVisibleOnFront() && !in_array($attribute->getAttributeCode(), $excludeAttr)) {
				$value = $attribute->getFrontend()->getValue($product);

				if ($product->hasData($attribute->getAttributeCode())) {
					if ($attribute->getFrontendInput() == 'price' && is_string($value)) {
						$value = Mage::app()->getStore()->convertPrice($value, true);
					}
				}

				// use a special label for the Description attribute
				if($attribute->getAttributeCode() == 'description'){
					$label = $this->__('About our %s', $product->getName());
				} else {
					$label = $attribute->getStoreLabel();
				}

				$data[$attribute->getAttributeCode()] = array(
					'label' => $label,
					'value' => $value,
					'code'  => $attribute->getAttributeCode()
				);
			}
		}
		return $data;
	}

}