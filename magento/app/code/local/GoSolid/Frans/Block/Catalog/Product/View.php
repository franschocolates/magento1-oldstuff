<?php
/**
 * Created by goSolid.
 * Date: 10/29/14
 * Time: 2:28 PM
 */ 
class GoSolid_Frans_Block_Catalog_Product_View extends Mage_Catalog_Block_Product_View {

	protected $_selectedProductId = null;
	protected $_productIds = array();
	protected $_attributes = array();
	protected $_productTypeConfigurableBlock;
	protected $_availabilityStatuses;

	const AVAILABILITY_STATUS_OUT_OF_STOCK  = 1;
	const AVAILABILITY_STATUS_CALL_TO_ORDER = 2;

	public function _prepareLayout()
	{
		parent::_prepareLayout();
		if($this->getProduct()->getTypeId() == Mage_Catalog_Model_Product_Type_Configurable::TYPE_CODE){
			$this->_setProductTypeConfigurableBlock();
			$this->_setAvailabilityStatuses();
			$this->_setProductIds();
			$this->_setSelectedProductId();
		} else {
			$this->_selectedProductId = $this->getProduct()->getId();
			$this->_productIds = array($this->_selectedProductId);
		}

		// if this is the only product in the category, remove that category breadcrumb from this view
		$category = $this->getProduct()->getCategory();
		if($category && $category->getProductCount() === 1)
		{
			$breadcrumbBlock = $this->getLayout()->getBlock('breadcrumbs');
			$breadcrumbBlock->removeCrumbByUrl($category->getUrl());
		}

	}

	public function getSelectedProductId()
	{
		return $this->_selectedProductId;
	}

	public function getProductIds()
	{
		return $this->_productIds;
	}

	public function getSelectedColorTileLabel()
	{
		$colorTileLabel = '';
		if(array_key_exists('color_tile', $this->_attributes))
		{
			foreach($this->_attributes['color_tile']['options'] as $option)
			{
				if($option['id'] == $this->_selectedProductId)
				{
					$colorTileLabel = $option['label'];
				}
			}
		}
		return $colorTileLabel;
	}

	public function getFormActionExtraParams()
	{
		$formActionExtraParams = array();
		$product = $this->getProduct();
		if($product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE){
			$formActionExtraParams['configurable_product_id'] = $product->getId();
		}
		return $formActionExtraParams;
	}

	public function getAvailabilityStatuses()
	{
		return $this->_availabilityStatuses;
	}

	protected function _setProductTypeConfigurableBlock()
	{
		$this->_productTypeConfigurableBlock = $this->getLayout()->createBlock('frans/catalog_product_view_type_configurable', 'product.type.configurable');
		$this->_productTypeConfigurableBlock->setTemplate('catalog/product/view/type/configurable.phtml');
		$this->setChild('product_type_data', $this->_productTypeConfigurableBlock);
	}

	protected function _setSelectedProductId()
	{
		$this->_selectedProductId = $this->_productTypeConfigurableBlock->getSelectedChildProductId();
	}

	protected function _setProductIds()
	{
		$this->_productIds = $this->_productTypeConfigurableBlock->getChildProductIds();
	}

	protected function _setAttributes()
	{
		$this->_attributes = $this->_productTypeConfigurableBlock->getAttributes();
	}

	protected function _setAvailabilityStatuses()
	{
		$this->_availabilityStatuses = $this->_productTypeConfigurableBlock->getAvailabilityStatuses();
	}

}