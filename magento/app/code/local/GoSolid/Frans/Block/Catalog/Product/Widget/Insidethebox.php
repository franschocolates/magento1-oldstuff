<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Product image attribute frontend
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class GoSolid_Frans_Block_Catalog_Product_Widget_Insidethebox extends Mage_Core_Block_Template
{

	public function getHtml($cols = 1){
		$html = '';
		// determine whether to show products (by sku) or pieces (by piece code)
		if($this->hasData('products')){
			$html .= $this->getProductHtml($cols);
		}elseif($this->hasData('pieces')){
			$html .= $this->getPieceHtml($cols);
		}
		return $html;
	}

	public function getPieceHtml($cols){
		$pieceString = $this->getData('pieces');
		// pieces are specified in a comma-separated list
		$pieces = explode(',', trim($pieceString));
		$html = '';
		$index = 1;
		foreach($pieces as $piece){

			// optional qty precedes piece code, separated by a pipe character
			$pieceInfo = explode('|', $piece);
			$pieceCode = end($pieceInfo);
			$model = Mage::getModel('frans/productPiece');
			$collection = $model->getCollection($model::STATUS_ENABLED);
			$collection->addFieldToFilter('piece_code', $pieceCode);

			// get the qty, default to 1 if none was specified
			if(count($pieceInfo) > 1){
				$qty = $pieceInfo[0];
			} else {
				$qty = 1;
			}

			if($collection->count() > 0)
			{

				if($index == 1){
					$html .= $this->getRowStartHtml();
				}

				$html .= $this->getColStartHtml($cols);

				$block = $this->getLayout()->createBlock('core/template');
				$block->setTemplate('catalog/product/widget/insidethebox/piece.phtml');
				$block->setPiece($collection->getFirstItem());
				$block->setQty($qty);
				$html .= $block->toHtml();

				$html .= $this->getColEndHtml();

				if($index == $cols){
					$html .= $this->getRowEndHtml();
					$index = 1;
				} else {
					$index++;
				}

			}

		}
		return $html;
	}

	public function getProductHtml($cols){
		$productString = $this->getData('products');
		// products are specified in a comma-separated list
		$products = explode(',', $productString);
		$html = '';
		$index = 1;
		foreach($products as $product){

			// optional qty precedes sku, separated by a pipe character
			$productInfo = explode('|', $product);
			$sku = end($productInfo);
			$collection = Mage::getModel('catalog/product')->getCollection();
			$collection->addFieldToFilter('sku', $sku)
				->addFieldToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);

			// get the qty, default to 1 if none was specified
			if(count($productInfo) > 1){
				$qty = $productInfo[0];
			} else {
				$qty = 1;
			}

			if($collection->count() > 0)
			{

				if($index == 1){
					$html .= $this->getRowStartHtml();
				}

				$html .= $this->getColStartHtml($cols);

				$block = $this->getLayout()->createBlock('core/template');
				$block->setTemplate('catalog/product/widget/insidethebox/product.phtml');
				$block->setProduct($collection->getFirstItem()->load());
				$block->setQty($qty);
				$html .= $block->toHtml();

				$html .= $this->getColEndHtml();

				if($index == $cols){
					$html .= $this->getRowEndHtml();
					$index = 1;
				} else {
					$index++;
				}

			}

		}
		return $html;
	}

	public function getColStartHtml($cols){
		switch($cols){
			case 1:
				$colWidth = 12;
				break;
			case 2:
				$colWidth = 6;
				break;
			case 3:
				$colWidth = 4;
				break;
			case 4:
				$colWidth = 3;
				break;
			default:
				$colWidth = 12;
				break;
		}
		return '<div class="inside-the-box-column col-sm-' . $colWidth . '">';
	}

	public function getColEndHtml(){
		return '</div>';
	}

	public function getRowStartHtml(){
		return '<div class="row">';
	}

	public function getRowEndHtml(){
		return '</div>';
	}

}
