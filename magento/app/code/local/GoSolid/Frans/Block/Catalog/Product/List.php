<?php
/**
 * Created by goSolid.
 * Date: 6/12/14
 * Time: 4:16 PM
 */ 
class GoSolid_Frans_Block_Catalog_Product_List extends Mage_Catalog_Block_Product_List {

	public function getIsGiftCard($product){
		$giftCardTypes = array('giftcard', 'virtualgiftcard');
		return in_array($product->getTypeId(), $giftCardTypes);
	}

    /**
     * Need use as _prepareLayout - but problem in declaring collection from
     * another block (was problem with search result)
     */
    protected function _beforeToHtml()
    {

	    //We only use toolbars for the search page
	    $isCatalogSearch = (Mage::app()->getRequest()->getModuleName() == 'catalogsearch' ? true : false);

	    if($isCatalogSearch)
	    {
		    $toolbar = $this->getToolbarBlock();

		    // called prepare sortable parameters
		    $collection = $this->_getProductCollection();

		    // use sortable parameters
		    if ($orders = $this->getAvailableOrders()) {
			    $toolbar->setAvailableOrders($orders);
		    }
		    if ($sort = $this->getSortBy()) {
			    $toolbar->setDefaultOrder($sort);
		    }
		    if ($dir = $this->getDefaultDirection()) {
			    $toolbar->setDefaultDirection($dir);
		    }
		    if ($modes = $this->getModes()) {
			    $toolbar->setModes($modes);
		    }

		    // set collection to toolbar and apply sort
		    $toolbar->setCollection($collection);

		    $this->setChild('toolbar', $toolbar);
	    }

        Mage::dispatchEvent('catalog_block_product_list_collection', array(
            'collection' => $this->_getProductCollection()
        ));

	    $this->_getProductCollection()->load();

	    // TODO: Remove this dump of all product names in collection
	    foreach($this->_getProductCollection() as $product){
		    Mage::log($product->getName(), Zend_Log::DEBUG, 'search.log');
	    }

        return parent::_beforeToHtml();
    }

	// return category-driven row HTML for use in any layout
	public function getRowHtml($rowTemplate, $products = array()){
		if($rowTemplate){
			$block = $this->getLayout()->createBlock('frans/catalog_product_tilerow');
			$block->setTemplate($rowTemplate);
			$block->setProducts($products);
			return $block->toHtml();
		} else {
			return "";
		}
	}

	// return static content-driven row HTML for use in any layout
	public function getStaticRowHtml($rowTemplate, $content = array())
	{
		if($rowTemplate){
			$block = $this->getLayout()->createBlock('frans/catalog_product_tilerow');
			$block->setTemplate($rowTemplate);
			$block->setContent($content);
			return $block->toHtml();
		} else {
			return "";
		}
	}

	// get an array of items from the loaded product collection, for easier subsets with array_slice()
	public function getProducts(){
		$collection = $this->getLoadedProductCollection();
		$products = array();
		foreach($collection as $product){
			$products[] = $product;
		}
		return $products;
	}

}