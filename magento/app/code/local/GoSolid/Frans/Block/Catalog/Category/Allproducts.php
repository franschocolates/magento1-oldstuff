<?php
/**
**/
class GoSolid_Frans_Block_Catalog_Category_Allproducts extends GoSolid_Frans_Block_Catalog_Category_List
{
	public function _construct() 
	{
		return parent::_construct();
	}
	
	/* Returns a collection of categories that are children of the specified category ID */
	public function getChildren($parentId){
		$children = Mage::getModel('catalog/category')->getCollection()
			->setStoreId(Mage::app()->getStore()->getStoreId())
			->addAttributeToFilter('is_active', array(
				'eq' => 1
			))
			->addAttributeToFilter('parent_id', array(
				'eq' => $parentId
			))
			->addAttributeToSelect(array(
				'name', 'url_path'
			))
			->addAttributeToSort('position', 'ASC');
		return $children;
	}

}