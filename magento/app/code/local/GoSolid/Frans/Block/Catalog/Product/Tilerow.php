<?php

class GoSolid_Frans_Block_Catalog_Product_Tilerow extends Mage_Core_Block_Template {

	// return category-driven tile HTML for use in any tile row
	public function getTileHtml($product, $tileTemplate = 'default'){
		$block = $this->getLayout()->createBlock('core/template');
		$block->setProduct($product);
		$block->setTemplate('catalog/product/list/layout/row/tile/' . $tileTemplate . '.phtml');
		return $block->toHtml();
	}

	// return static content-driven tile HTML for use in any tile row
	public function getStaticTileHtml($content, $tileTemplate = 'default')
	{
		$block = $this->getLayout()->createBlock('core/template');
		$block->setContent($content);
		$block->setTemplate('catalog/product/list/layout/row/tile/' . $tileTemplate . '.phtml');
		return $block->toHtml();
	}

}