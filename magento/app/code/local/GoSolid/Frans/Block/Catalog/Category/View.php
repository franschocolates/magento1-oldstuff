<?php
/**
 * Created by goSolid.
 * Date: 9/25/14
 * Time: 3:46 PM
 */ 
class GoSolid_Frans_Block_Catalog_Category_View extends Mage_Catalog_Block_Category_View {

	public function getProductListHtml($preContent = null, $doOpenContainer = true, $doCloseContainer = true)
	{
		$layoutId = $this->getCurrentCategory()->getProductGridLayout();
		// default to ID 1000 ("Three Up") if no ID is found
		if($layoutId == null){
			$layoutId = 1000;
		}
		$layouts = Mage::helper("frans/catalog_category")->getProductGridLayoutTemplates();
		$layout = $layouts[$layoutId];
		return $this->getChild('product_list')
			->setTemplate($layout)
			->toHtml();
	}

	public function getChildCategoryHtml($preContent = null, $doOpenContainer = true, $doCloseContainer = true)
	{
		$html = '';
		$layoutId = $this->getCurrentCategory()->getChildCategoryLayout();
		// default to ID 1 ("Alternating, Right First") if no ID is found
		if($layoutId == null){
			$layoutId = 1;
		}
		$layouts = Mage::helper("frans/catalog_category")->getChildCategoryLayoutTemplates();
		if(array_key_exists($layoutId, $layouts))
		{
			$layout = $layouts[$layoutId];
			$html = $this->getChild('category_list')
				->setTemplate($layout)
				->toHtml();
		}

		return $html;
	}

	public function getCrosssellHtml()
	{

        //load all child categories flagged with is_cross_sell = 1 and get the first one
        $categories = Mage::getModel('catalog/category')->getCrossellCategories($this)->load();
		$crossSellCategory = $categories->getFirstItem();

        if($crossSellCategory->getId() != null) {
            // get the product collection and label for the block
            $crossSellProducts = $crossSellCategory->getProductCollection();
            $crossSellLabel = $crossSellCategory->getName();

            // set the data to the block and get its HTML
            return $this->getChild('crosssell')->setProducts($crossSellProducts)->setLabel($crossSellLabel)->toHtml();
        }

        return null;
	}

	// if the CMS block HTML contains only one <p>...</p> tag, which surrounds the whole block, remove it
	public function getCmsBlockHtml()
	{
		$cmsBlockHtml = parent::getCmsBlockHtml();
		$firstTagOpenPosition = stripos($cmsBlockHtml, '<p>');
		$lastTagOpenPosition = strripos($cmsBlockHtml, '<p>');

		if(($firstTagOpenPosition == 0) && ($firstTagOpenPosition == $lastTagOpenPosition))
		{
			$cmsBlockHtmlLength = strlen($cmsBlockHtml);
			$firstTagClosePosition = stripos($cmsBlockHtml, '</p>');
			$lastTagClosePosition = strripos($cmsBlockHtml, '</p>');
			if(($firstTagClosePosition == $cmsBlockHtmlLength) && ($firstTagClosePosition == $lastTagClosePosition))
			{
				$cmsBlockHtml = substr($cmsBlockHtml, 3, ($cmsBlockHtmlLength - 7));
			}
		}

		return $cmsBlockHtml;
	}

}