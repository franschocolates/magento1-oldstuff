<?php

class GoSolid_Frans_Block_Catalog_Product_View_Type_Configurable extends Mage_Catalog_Block_Product_View_Type_Configurable
{

	protected $_attributes;
	protected $_selectedChildProductId;
	protected $_childProductIds;
	protected $_availabilityStatuses;

	public function __construct()
	{
		$this->_setAttributes();
		$this->_setChildProductIds();
		$this->_setSelectedChildProductId();
	}

	public function getAttributes()
	{
		return $this->_attributes;
	}

	public function getAvailabilityStatuses()
	{
		return $this->_availabilityStatuses;
	}

	public function getJsonAttributes()
	{
		return Mage::helper('core')->jsonEncode($this->_attributes);
	}

	public function getChildProductIds(){
		return $this->_childProductIds;
	}

	public function getSelectedChildProductId(){
		return $this->_selectedChildProductId;
	}

	public function getOptionPriceText($option)
	{
		$product = reset($option['products']);
		return $product['price'];
	}

	public function isProductIdInOption($option)
	{
		return array_key_exists($this->_selectedChildProductId, $option['products']);
	}

	public function getAllowProducts(){
		if (!$this->hasAllowProducts()) {
			$products = array();
			$skipSaleableCheck = Mage::helper('catalog/product')->getSkipSaleableCheck();
			$allProducts = $this->getProduct()->getTypeInstance(true)->getUsedProducts(null, $this->getProduct());

			foreach ($allProducts as $product) {
				if ($product->getStatus() == 1 && ($product->isSaleable() || $skipSaleableCheck)) {
					$products[] = $product;
				}
			}
			$this->setAllowProducts($products);
		}
		return $this->getData('allow_products');
	}

	protected function _setAttributes()
	{
		$attributes = array();
		$options    = array();
		$store      = $this->getCurrentStore();
		$helper  = Mage::helper('core');
		$currentProduct = $this->getProduct();
		$availabilityStatuses = array();

		$preconfiguredFlag = $currentProduct->hasPreconfiguredValues();
		if ($preconfiguredFlag) {
			$preconfiguredValues = $currentProduct->getPreconfiguredValues();
			$defaultValues       = array();
		}

		// allow out-of-stock items to appear in this collection
		$this->helper('catalog/product')->setSkipSaleableCheck(true);

		foreach ($this->getAllowProducts() as $product) {

			$productId  = $product->getId();

			foreach ($this->getAllowAttributes() as $attribute) {
				$productAttribute   = $attribute->getProductAttribute();
				$productAttributeId = $productAttribute->getId();
				$attributeValue     = $product->getData($productAttribute->getAttributeCode());
				if (!isset($options[$productAttributeId])) {
					$options[$productAttributeId] = array();
				}

				if (!isset($options[$productAttributeId][$attributeValue])) {
					$options[$productAttributeId][$attributeValue] = array();
				}
				$options[$productAttributeId][$attributeValue][] = $productId;
			}

			// while we're looping through products, check their availability and call to order status
			if($product->getCallToOrder())
			{
				$availabilityStatuses[$productId] = GoSolid_Frans_Block_Catalog_Product_View::AVAILABILITY_STATUS_CALL_TO_ORDER;
			}
			elseif(!$product->getIsSalable()) // call to order supersedes unavailability
			{
				$availabilityStatuses[$productId] = GoSolid_Frans_Block_Catalog_Product_View::AVAILABILITY_STATUS_OUT_OF_STOCK;
			}

		}

		$this->_resPrices = array(
			$this->_preparePrice($currentProduct->getFinalPrice())
		);

		foreach ($this->getAllowAttributes() as $attribute) {

			$productAttribute = $attribute->getProductAttribute();
			$attributeId = $productAttribute->getId();
			$info = array();

			$prices = $attribute->getPrices();
			if (is_array($prices)) {
				foreach ($prices as $value) {

					if(!$this->_validateAttributeValue($attributeId, $value, $options)) {
						continue;
					}
					$currentProduct->setConfigurablePrice(
						$this->_preparePrice($value['pricing_value'], $value['is_percent'])
					);
					$currentProduct->setParentId(true);
					Mage::dispatchEvent(
						'catalog_product_type_configurable_price',
						array('product' => $currentProduct)
					);

					if (isset($options[$attributeId][$value['value_index']])) {
						$productsIndex = $options[$attributeId][$value['value_index']];
						foreach($productsIndex as $key => $productId){
							$price = Mage::getResourceModel('catalog/product')->getAttributeRawValue($productId, 'price', $store->getId());
							$productsIndex[$productId] = array(
								'price' => str_replace('.00', '', $helper->currency($price, true, false)) // format prices and remove ".00"
							);
							unset($productsIndex[$key]);
						}
					} else {
						$productsIndex = array();
					}
					$optionDetails = array(
						'id'        => $value['value_index'],
						'label'     => $value['label'],
						'products'  => $productsIndex,
					);

					// we need some info about the color_tile attribute, so check if that's the one we have in this loop iteration
					if($productAttribute->getAttributeCode() == 'color_tile'){

						// load the specific color tile
						$colorTile = Mage::getModel('frans/colorTile')->load($value['value_index']);
						// if the color tile is disabled, skip to the next option without using this one
						if(!$colorTile->getStatus())
						{
							continue;
						}

						// add the image URL to the option details data, so we can display it on the frontend
						$imageUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . $colorTile->getImagePath();
						$optionDetails['image_url'] = $imageUrl;

						$position = $colorTile->getPosition();
						$optionDetails['position'] = $position;
					}


					$info['options'][] = $optionDetails;

				}
			}
			/**
			 * Prepare formatted values for options choose
			 */

			if($this->_validateAttributeInfo($info)) {
				$attributes[$productAttribute->getAttributeCode()] = $info;
			}

			// Add attribute default value (if set)
			if ($preconfiguredFlag) {
				$configValue = $preconfiguredValues->getData('super_attribute/' . $attributeId);
				if ($configValue) {
					$defaultValues[$attributeId] = $configValue;
				}
			}
		}

		$this->_attributes = $attributes;
		$this->_availabilityStatuses = $availabilityStatuses;

		return $this;
	}

	// set the IDs of all child products available
	protected function _setChildProductIds(){
		$attributes = $this->_attributes;
		$childProductIds = array();
		foreach($attributes as $attribute){
			$options = $attribute['options'];
			foreach($options as $option){
				$products = $option['products'];
				foreach($products as $productId => $product){
					if(!in_array($productId, $childProductIds)){
						$childProductIds[] = $productId;
					}
				}
			}
		}
		$this->_childProductIds = $childProductIds;
		return $this;
	}

	// set the ID of the child product to show; determine from URL or use the first available
	protected function _setSelectedChildProductId()
	{
		// declare array to hold the products for each option requested
		$optionProducts = array();

		// get the configurable product and see if it has a valid default child
		$product = $this->getProduct();
		$defaultChildProduct = $product->getDefaultChildProduct();

		// loop through all configurable attributes
		foreach($this->_attributes as $attributeCode => $attribute){

			// check if a URL param was provided, requesting an option ID to use for this attribute
			if($requestedOption = $this->getRequest()->getParam($attributeCode)){

				// loop through the attribute's options, looking for an ID that matches the one requested
				foreach($attribute['options'] as $option){
					if($requestedOption == $option['id']){
						// match found, remember the products in this option
						$optionProducts[$attributeCode] = $option['products'];
						break;
					}
				}

			}

			// check if any products have been found for this attribute code yet (only if default child is invalid)
			if(!$defaultChildProduct)
			{
				if(!array_key_exists($attributeCode, $optionProducts)){
					// if no products were found, grab the products from the first option available (fallback for invalid default)
					$optionProducts[$attributeCode] = $attribute['options'][0]['products'];
				}
			}

		}

		// look for the one product that intersects all of the option products
		$intersections = array();
		foreach($optionProducts as $products){
			if(empty($intersections)){
				$intersections = $products;
			} else {
				// TODO: Fix "array to string conversion" PHP notice cause by array_intersect_assoc() here
				// The code is producing correct results but should be updated for less ambiguity (and no notices in the log!)
				// For more information see https://bugs.php.net/bug.php?id=61118&edit=1
				$intersections = array_intersect_assoc($products, $intersections);
			}
		}

		// at this point, $intersections should contain only the single matching product; get its ID
		$this->_selectedChildProductId = key($intersections);

		// if we still haven't identified a child product, get the default from the parent
		if(!$this->_selectedChildProductId)
		{
			if($defaultChildProduct){
				$this->_selectedChildProductId = $defaultChildProduct->getId();
			}
		}

		return $this;
	}

	protected function _validateAttributeInfo(&$info)
	{
		if(count($info) > 0) {
			return true;
		}
		return false;
	}

	public function array_sort($array, $on, $order=SORT_ASC){

		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
					break;
				case SORT_DESC:
					arsort($sortable_array);
					break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}

}