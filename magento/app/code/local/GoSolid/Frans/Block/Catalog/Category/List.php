<?php
/**
 * Created by goSolid.
 * Date: 6/12/14
 * Time: 4:16 PM
 */ 
class GoSolid_Frans_Block_Catalog_Category_List extends Mage_Core_Block_Template {

	// return category-driven row HTML for use in any layout
	public function getRowHtml($rowTemplate, $categories = array())
	{
		if($rowTemplate){
			$block = $this->getLayout()->createBlock('frans/catalog_category_tilerow');
			$block->setTemplate($rowTemplate);
			$block->setCategories($categories);
			return $block->toHtml();
		} else {
			return "";
		}
	}

	// return static content-driven row HTML for use in any layout
	public function getStaticRowHtml($rowTemplate, $content = array())
	{
		if($rowTemplate){
			$block = $this->getLayout()->createBlock('frans/catalog_category_tilerow');
			$block->setTemplate($rowTemplate);
			$block->setContent($content);
			return $block->toHtml();
		} else {
			return "";
		}
	}

	// get an array of the current category's child categories, for easier subsets with array_slice()
	public function getCategories()
	{
		$_helper = Mage::helper('catalog/category');
		$parentCategory = Mage::registry('current_category');
		return $_helper->getChildCategories($parentCategory);
	}

}