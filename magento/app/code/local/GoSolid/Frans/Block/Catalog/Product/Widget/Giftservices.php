<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Product image attribute frontend
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class GoSolid_Frans_Block_Catalog_Product_Widget_Giftservices extends Mage_Core_Block_Template
{

	public function getHtml($cols){
		$blockString = $this->getData('blocks');
		// block IDs are specified in a comma-separated list
		$blocks = explode(',', $blockString);
		$html = '';
		$index = 1;
		foreach($blocks as $block){

			if($index == 1){
				$html .= $this->getRowStartHtml();
			}

			$html .= $this->getColStartHtml($cols);

			//$blockId = Mage::getModel('cms/block')->loadByAttribute('identifier', $block)->getId();
			$html .= $this->getLayout()->createBlock('cms/block')->setBlockId($block)->toHtml();

			$html .= $this->getColEndHtml();

			if($index == $cols){
				$html .= $this->getRowEndHtml();
				$index = 1;
			} else {
				$index++;
			}

		}
		return $html;
	}

	public function getColStartHtml($cols){
		switch($cols){
			case 1:
				$colWidth = 12;
				break;
			case 2:
				$colWidth = 6;
				break;
			case 3:
				$colWidth = 4;
				break;
			case 4:
				$colWidth = 3;
				break;
			default:
				$colWidth = 12;
				break;
		}
		return '<div class="gift-services-column col-sm-' . $colWidth . '">';
	}

	public function getColEndHtml(){
		return '</div>';
	}

	public function getRowStartHtml(){
		return '<div class="row">';
	}

	public function getRowEndHtml(){
		return '</div>';
	}

}
