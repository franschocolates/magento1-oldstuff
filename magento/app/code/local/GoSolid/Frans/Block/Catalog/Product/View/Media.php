<?php
/**
 * Created by goSolid.
 * Date: 10/30/14
 * Time: 1:35 PM
 */ 
class GoSolid_Frans_Block_Catalog_Product_View_Media extends Mage_Catalog_Block_Product_View_Media {

	protected $_galleries;

	public function _beforeToHtml()
	{
		$this->_setGalleries();
	}

	public function getJsonGalleries()
	{
		return Mage::helper('core')->jsonEncode($this->_galleries);
	}

	public function getGalleries()
	{
		return $this->_galleries;
	}

	public function getSelectedProductGalleryHtml()
	{
		$selectedProductId = $this->getSelectedProductId();
		return $this->_galleries[$selectedProductId];
	}

	public function getProduct($productId = null){

		$product = null;

		if($productId != null){
			$product = Mage::getModel('catalog/product');
			$product->setId($productId);
			$product->setStoreId(Mage::app()->getStore()->getId());
		} else {
			$product = parent::getProduct();
		}
		return $product;
	}

	// gets a product's gallery images without loading the full product
	public function getGalleryImagesByProductId($productId)
	{
		// TODO: Annotate this, there's a reason we're doing it this way
		$attribute = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', 'media_gallery');
		$galleryAttributeModel = Mage::getModel('catalog/product_attribute_backend_media');
		$product = Mage::getModel('catalog/product');
		$product->setId($productId);
		$product->setStoreId(Mage::app()->getStore()->getId());
		$galleryAttributeModel->setAttribute($attribute)->afterLoad($product);

		$imageGallery = $product->getMediaGallery();

		$images = new Varien_Data_Collection();
		foreach($imageGallery['images'] as $image )
		{
			if ($image['disabled']) {
				continue;
			}
			$image['url'] = $product->getMediaConfig()->getMediaUrl($image['file']);
			$image['id'] = isset($image['value_id']) ? $image['value_id'] : null;
			$image['path'] = $product->getMediaConfig()->getMediaPath($image['file']);
			$images->addItem(new Varien_Object($image));
		}
		return $images;
	}

	protected function _setGalleries()
	{
		$galleries = array();
		$productIds = $this->getProductIds();
		foreach($productIds as $productId){
			$galleryImages = $this->getGalleryImagesByProductId($productId);
			$galleries[$productId] = $this->_getGalleryHtml($galleryImages);
		}
		$this->_galleries = $galleries;
		return $this;
	}

	protected function _getGalleryHtml($galleryImages)
	{
		$product = $this->getProduct();
		return $this->getChild('gallery')->setProduct($product)->setGalleryImages($galleryImages)->toHtml();
	}

}