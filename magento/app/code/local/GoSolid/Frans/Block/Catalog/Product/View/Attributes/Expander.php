<?php
/**
 * Created by goSolid.
 * Date: 3/2/15
 * Time: 4:52 PM
 */

class GoSolid_Frans_Block_Catalog_Product_View_Attributes_Expander extends Mage_Core_Block_Template {

	protected $_attributeData;

	public function _beforeToHtml()
	{
		$this->_attributeData = $this->getAttributeData();
	}

	public function getAttributeLabel()
	{
		return $this->htmlEscape($this->__($this->_attributeData['label']));
	}

	public function getAttributeHtml()
	{
		$_product = Mage::getModel('catalog/product')->setId($this->getProductId());
		$html = $this->helper('catalog/output')->productAttribute($_product, $this->_attributeData['value'], $this->_attributeData['code']);
		if($this->_attributeData['code'] == 'nutrition_ingredients'){
			// If this is the Nutrition & Ingredients attribute, use custom display (the $html var contains the code to look up)
			$html = $this->getChild('nutrition')->setCode($html)->toHtml();
		}
		return trim($html);
	}

}