<?php
/**
 * Created by goSolid.
 * Date: 3/3/15
 * Time: 9:07 AM
 */

class GoSolid_Frans_Block_Catalog_Product_View_Attributes_Expander_Nutrition extends Mage_Core_Block_Template {

	protected $_nutritionInfo;

	public function _beforeToHtml()
	{
		$this->_nutritionInfo = $this->getNutritionInfo();
	}

	public function getNutritionInfo()
	{
		// retrieve a row from the nutrition table, based on a code which is set against this block by
		// GoSolid_Frans_Block_Catalog_Product_View_Attributes_Expander::getAttributeHtml()
		$code = $this->getCode();
		$nutritionInfo = Mage::getModel('frans/nutrition')
			->getCollection()
			->addFieldToFilter('code', array('eq' => $code))
			->getFirstItem();
		return $nutritionInfo;
	}

	public function getBackgroundUrl()
	{
		$url = null;
		$mediaPath = $this->_nutritionInfo->getBackgroundImage();
		if($mediaPath){
			$url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . $this->_nutritionInfo->getBackgroundImage();
		}
		return $url;
	}

}