<?php
/**
 * Created by goSolid.
 * Date: 11/4/14
 * Time: 10:46 AM
 */ 
class GoSolid_Frans_Block_Catalog_Product_View_Type_Simple extends Mage_Catalog_Block_Product_View_Type_Simple {

	public function getFormattedPrice(){
		$product = $this->getProduct();
		$helper  = Mage::helper('core');
		$price = $product->getPrice();
		return str_replace('.00', '', $helper->currency($price, true, false)); // format prices and remove ".00"
	}

}