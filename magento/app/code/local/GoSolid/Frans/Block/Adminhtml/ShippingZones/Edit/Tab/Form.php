<?php

class GoSolid_Frans_Block_Adminhtml_ShippingZones_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

	protected function _prepareForm()
	{

		$form = new Varien_Data_Form(); 
		$this->setForm($form);
		$fieldset = $form->addFieldset('frans_fs', array('legend'=>Mage::helper('frans')->__('Shipping Zone')));
	
		$fieldset->addField('zone_name', 'text', array(
	          'title'     => Mage::helper('frans')->__('Zone Name'),
			  'label'     => Mage::helper('frans')->__('Zone Name'),
	          'name'      => 'zone_name',
			  'class'     => 'required-entry', 
	          'required'  => true,
			  'note'	  => Mage::helper('frans')->__('Name of the zone'),
	      	));
	
		$fieldset->addField('postcode_list', 'textarea', array(
			'title'     => Mage::helper('frans')->__('Post Code List'),
			'label'     => Mage::helper('frans')->__('Post Code List'),
			'required'  => true,
	     	'name'      => 'postcode_list',
	    	'note'		=> Mage::helper('frans')->__('List of comma-separated postcodes (5 digit US only) to be included in Zone. Use "*" to include any postcodes that are not contained in other regions.'),
		));
	
		$fieldset->addField('position', 'text', array(
	          'title'     => Mage::helper('frans')->__('Position'),
			  'label'     => Mage::helper('frans')->__('Position'),
	          'name'      => 'position',
			  'class'     => 'required-entry validate-not-negative-number', 
	          'required'  => true,
			  'note'	  => Mage::helper('frans')->__('Position of this zone when evaluating rules'),
	      	));
	
		if ( Mage::getSingleton('adminhtml/session')->getShippingZonesData() )
		{
	    	$form->setValues(Mage::getSingleton('adminhtml/session')->getShippingZonesData());
	    	Mage::getSingleton('adminhtml/session')->getShippingZonesData(null);
		}
		elseif(Mage::registry('shippingZones_data'))
		{
	    	$form->setValues(Mage::registry('shippingZones_data')->getData());
		}
		
		return parent::_prepareForm();
	}
  
}