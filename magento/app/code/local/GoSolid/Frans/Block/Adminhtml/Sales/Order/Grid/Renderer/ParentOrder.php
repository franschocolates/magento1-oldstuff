<?php
/**
 *
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Grid_Renderer_ParentOrder extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        return $row->getIsMultishipParent() ? $this->__("Yes") : $this->__('No');
    }
}