<?php
/**
 * Created by goSolid.
 * Date: 8/4/14
 * Time: 4:59 PM
 */ 
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Create_Search extends Mage_Adminhtml_Block_Sales_Order_Create_Search {

	public function getHeaderText()
	{
		return Mage::helper('sales')->__('Add Items');
	}

}