<?php

class GoSolid_Frans_Block_Adminhtml_ShippingBlackoutDates_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

	protected function _prepareForm()
	{

		$form = new Varien_Data_Form(); 
		$this->setForm($form);
		$fieldset = $form->addFieldset('frans_fs', array('legend'=>Mage::helper('frans')->__('Shipping Blackout Date')));
	
		$fieldset->addField('blackout_date', 'date', array(
	          'title'     => Mage::helper('frans')->__('Date'),
			  'label'     => Mage::helper('frans')->__('Date'),
	          'name'      => 'blackout_date',
			  'class'     => 'required-entry date', 
	          'required'  => true,
			  'image'  => Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN).'/adminhtml/default/default/images/grid-cal.gif',
			  'format' => 'M/d/y',
		));
	
		if ( Mage::getSingleton('adminhtml/session')->getShippingBlackoutDateData() )
		{
	    	$form->setValues(Mage::getSingleton('adminhtml/session')->getShippingBlackoutDateData());
	    	Mage::getSingleton('adminhtml/session')->getShippingBlackoutDateData(null);
		}
		elseif(Mage::registry('shippingBlackoutDate_data'))
		{
	    	$form->setValues(Mage::registry('shippingBlackoutDate_data')->getData());
		}
		
		return parent::_prepareForm();
	}
  
}