<?php
class GoSolid_Frans_Block_Adminhtml_ShippingMethod_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{

		parent::__construct();
		$this->setId('fransGrid');
		
		//TODO: Set default sort
		//$this->setDefaultSort('id');
		//$this->setDefaultDir('ASC');
	}
 
	protected function _prepareCollection()
	{
		$collection = Mage::getModel('frans/shippingMethod')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}	
		
		
	protected function _prepareColumns()
  	{

  		$this->addColumn('id', array(
			  'header'    => Mage::helper('frans')->__('ID'),
			  'align'     =>'right',
			  'width'     => '50px',
			  'index'     => 'id',
		  ));

        $this->addColumn('shipping_method', array(
            'header'    => Mage::helper('frans')->__('Shipping Method'),
            'align'     =>'left',
            'index'     => 'shipping_method',
            'type'  => 'options',
            'options' => Mage::getModel('frans/shipping_carrier_frans')->allowedMethodsToGridOptionArray()
        ));


        $this->addColumn('label', array(
            'header'    => Mage::helper('frans')->__('Label'),
            'align'     =>'left',
            'index'     => 'label',
        ));

        $this->addColumn('color', array(
            'header'    => Mage::helper('frans')->__('Color'),
            'align'     =>'left',
            'index'     => 'color',
        ));
		  /*
		   * EXAMPLE CODE. CAN BE REMOVED.
		   *
		  

		  //date
		  $this->addColumn('mydate', array(
	            'header'    => Mage::helper('frans')->__('MyDate'),
	            'type'      => 'date',
	            'align'     => 'left',
	            'index'     => 'mydate', //change me
	            'gmtoffset' => false
	        ));
	        
	      //options
	      $options = Mage::getModel('frans/[model_lcase]')->getCollection()->setOrder("title", "asc")->toGridOptionArray("id", "title");
		  $this->addColumn('category_id', array(
			  'header'    => Mage::helper('frans')->__('Category'),
			  'align'     => 'left',
			  'index'     => 'category_id',
			  'type'  => 'options',
			  'options'	=> $options, //example of how to get options.
		  )); 
		  
		  //status
		  $this->addColumn('status', array(
			  'header'    => Mage::helper('frans')->__('Status'),
			  'align'     => 'left',
			  'width'     => '150px',
			  'index'     => 'status',
			  'type'	  => 'options',
			  'options'   => array(
				  'Enabled' => 'Enabled',
				  'Disabled' => 'Disabled',
			  ),
		  ));
		  
		  END EXAMPLE CODE
		  */ 
		  

		  return parent::_prepareColumns();
	}
	
	public function getRowUrl($row)
  	{
    	return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  	}

    protected function _getCarrierOptions()
    {
        $carriers = Mage::getSingleton('shipping/config')->getAllCarriers();
        $options = array();

        foreach ($carriers as $code => $label)
        {
            $options[] = array('value' => $code, 'label' => $label);
        }

        return $options;
    }
}