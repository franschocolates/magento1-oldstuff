<?php

class GoSolid_Frans_Block_Adminhtml_Pickupmanager_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'frans';
        $this->_controller = 'adminhtml_pickupmanager';
        
        // Optional
        //$this->_updateButton('save', 'label', Mage::helper('event')->__('Save'));
		$this->_removeButton('delete');
		$this->_removeButton('reset');
        $this->_removeButton('save');

		
        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
			
        ";
    }

	public function getHeaderText()
    {  
        if( Mage::registry('pickupmanager_data') && Mage::registry('pickupmanager_data')->getId() ) {
            return Mage::helper('frans')->__("Update Pickup Order");
		} else {
            return Mage::helper('frans')->__('Add');
        }
    }

}