<?php

/**
 * This class was adapted from the Engine Quickship form, for editing ice packs.
 * Changed to be more consistent in terms of naming
 *
 * Class GoSolid_Frans_Block_Adminhtml_Sales_Order_View_Shipping_IcePack_Form
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_View_Shipping_IcePack_Form
    extends Mage_Adminhtml_Block_Sales_Order_Abstract
{

    protected function _prepareLayout()
    {
        $this->setChild('save_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'   => Mage::helper('frans')->__('Save Ice &amp; Weight'),
                    'class'   => 'save'
                ))
        );

        return $this;
    }

    public function getSaveButtonHtml()
    {

        $this->getChild('save_button')->setOnclick(
            "icePackController.saveWeight('{$this->getHtmlId()}', '{$this->getSaveUrl()}')"
        );

        return $this->getChildHtml('save_button');
    }

    public function getFieldId($id)
    {
        return $this->getFieldIdPrefix() . $id;
    }

    public function getFieldIdPrefix()
    {
        return 'order_icepack_' . $this->getOrder()->getId() . '_';
    }

    public function getSaveUrl()
    {
        return $this->getUrl('*/*/saveWeight',
            array(
                '_current' => true
            )
        );
    }

    public function getHtmlId()
    {
        return substr($this->getFieldIdPrefix(), 0, -1);
    }


}