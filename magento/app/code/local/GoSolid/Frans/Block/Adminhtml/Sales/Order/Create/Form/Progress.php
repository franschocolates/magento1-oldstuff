<?php

class GoSolid_Frans_Block_Adminhtml_Sales_Order_Create_Form_Progress extends Mage_Adminhtml_Block_Widget_Form
{

	public function getSteps(){

		// use steps from session data if it exists, otherwise get the default set
		$session = Mage::getSingleton('adminhtml/session_quote');
		$quote = $session->getQuote();
		$dataKey = 'quote_progress_' . $quote->getId();

		$steps = $session->getData($dataKey);
		if(!$steps) {
			$steps = $this->_getDefaultSteps();
		}

		// if request contains step data, update the array with it
		$update = $this->getRequest()->getParam('steps');
		if($update){
			$steps = array_replace_recursive($steps, $update);
		}

		// replace all non-persistent step data with default values, only if not loading specific blocks via Ajax
		$handles = $this->getLayout()->getUpdate()->getHandles();
		if(!in_array('adminhtml_sales_order_create_load_block_progress', $handles)){
			foreach($steps as $id => $step){
				if($step['persists'] == 0){
					$steps[$id] = $this->_getDefaultSteps($id);
				}
			}
		}


		// set steps' hidden property based on quote conditions
		$isMultiShip = $quote->getIsMultiShipping();
		$isVirtual = $quote->isVirtual();
		$steps['order-shipping_address']['hidden']  = ($isMultiShip || $isVirtual) ? 1 : 0;
		$steps['order-shipping_method']['hidden']   = $isMultiShip ? 1 : 0;
		$steps['order-additional_fields']['hidden'] = $isMultiShip ? 1 : 0;
		$steps['order-shipments']['hidden']         = $isMultiShip ? 0 : 1;

		// make sure the steps are properly ordered
		$steps = $this->_sortSteps($steps);

		// save the current steps back to the session
		$session->setData($dataKey, $steps);
		return $steps;
	}

	public function getStepHtml(){
		$steps = $this->getSteps();
		$loopingInAdditionalSteps = false;
		$output = '';
		foreach($steps as $stepId => $step){
			if(!$step['required'] && !$loopingInAdditionalSteps){
				$output .= '<hr />';
				$loopingInAdditionalSteps = true;
			}
			$class='';
			if($step['required']) $class .= ' required';
			if($step['done']) $class .= ' done';
			if($step['hidden']) $class .= ' hidden';
			$output .= '<li class="' . $class . '" data-step-id="' . $stepId . '"><a href="#">' . $step['label'] . '</a></li>';
		}
		return $output;
	}

	// get default values for all steps, or an individual one by ID
	private function _getDefaultSteps($id = null){
		$steps = array(
			'order-form_account' => array(
				'position'  => 10,
				'label'     => $this->__('Account Basics'),
				'required'  => 1,
				'done'      => 0,
				'hidden'    => 0,
				'persists'  => 1
			),
			'order-form_accounting' => array(
				'position'  => 20,
				'label'     => $this->__('Accounting'),
				'required'  => 1,
				'done'      => 0,
				'hidden'    => 0,
				'persists'  => 0
			),
			'order-items-wrapper' => array(
				'position'  => 30,
				'label'     => $this->__('Add Items and Options'),
				'required'  => 1,
				'done'      => 0,
				'hidden'    => 0,
				'persists'  => 1
			),
			'order-additional_fields' => array(
				'position'  => 40,
				'label'     => $this->__('Messages'),
				'required'  => 1,
				'done'      => 0,
				'hidden'    => 0,
				'persists'  => 0
			),
			'order-shipments' => array(
				'position'  => 50,
				'label'     => $this->__('Shipments'),
				'required'  => 1,
				'done'      => 0,
				'hidden'    => 1,
				'persists'  => 1
			),
			'order-shipping_address' => array(
				'position'  => 50,
				'label'     => $this->__('Shipping Address'),
				'required'  => 1,
				'done'      => 0,
				'hidden'    => 0,
				'persists'  => 1
			),
			'order-billing_address' => array(
				'position'  => 60,
				'label'     => $this->__('Billing Address'),
				'required'  => 1,
				'done'      => 0,
				'hidden'    => 0,
				'persists'  => 1
			),
			'order-shipping_method' => array(
				'position'  => 70,
				'label'     => $this->__('Shipping Method'),
				'required'  => 1,
				'done'      => 0,
				'hidden'    => 0,
				'persists'  => 1
			),
			'order-gift_card' => array(
				'position'  => 80,
				'label'     => $this->__('Gift Card'),
				'required'  => 0,
				'done'      => 0,
				'hidden'    => 0,
				'persists'  => 1
			),
			'order-payment_method' => array(
				'position'  => 90,
				'label'     => $this->__('Payment Method'),
				'required'  => 1,
				'done'      => 0,
				'hidden'    => 0,
				'persists'  => 0
			),
			'order-comments' => array(
				'position'  => 100,
				'label'     => $this->__('Order Comments'),
				'required'  => 0,
				'done'      => 0,
				'hidden'    => 0,
				'persists'  => 1
			),
			'order-admin_notes' => array(
				'position'  => 110,
				'label'     => $this->__('Admin Notes'),
				'required'  => 0,
				'done'      => 0,
				'hidden'    => 0,
				'persists'  => 0
			),
            'order-customer_notes' => array(
                'position'  => 110,
                'label'     => $this->__('Customer Notes'),
                'required'  => 0,
                'done'      => 0,
                'hidden'    => 0,
                'persists'  => 0
            ),
			'order-save_quote' => array(
				'position'  => 120,
				'label'     => $this->__('Save Quote'),
				'required'  => 0,
				'done'      => 0,
				'hidden'    => 0,
				'persists'  => 1
			)
		);
		if(array_key_exists($id, $steps)){
			return $steps[$id];
		}
		return $steps;
	}

	private function _sortSteps($steps){
		foreach($steps as $key => $row){
			$position[$key] = $row['position'];
			$required[$key] = $row['required'];
		}
		array_multisort($required, SORT_DESC, $position, SORT_ASC, $steps);
		return $steps;
	}

}