<?php
 
class GoSolid_Frans_Block_Adminhtml_Article extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {

	$this->_controller = 'adminhtml_article';
	$this->_blockGroup = 'frans';
	$this->_headerText = Mage::helper('frans')->__("Manage Articles");
	$this->_addButtonLabel = Mage::helper('frans')->__("Add New Articles");
	
    parent::__construct();
    
  }
}