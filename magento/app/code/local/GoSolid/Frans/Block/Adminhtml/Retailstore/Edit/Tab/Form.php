<?php

class GoSolid_Frans_Block_Adminhtml_Retailstore_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

	protected function _prepareForm()
	{

		$form = new Varien_Data_Form(); 
		$this->setForm($form);
		$fieldset = $form->addFieldset('frans_fs', array('legend'=>Mage::helper('frans')->__('Retail Store')));
	
		//status
		$fieldset->addField('status', 'select',array(
			'label'     => Mage::helper('frans')->__('Status'),
			'name'      => 'status',
			'options'   => array(
					  		1 => 'Enabled',
					  		0 => 'Disabled'
				  )
		));
		
		//Use On Frontend
		$fieldset->addField('use_on_frontend', 'select',array(
			'label'     => Mage::helper('frans')->__('Use On Frontend'),
			'name'      => 'use_on_frontend',
            'note'      => 'Indicates whether to show this store in the frontend (e.g. on retail store and contact pages).',
			'options'   => array(
				1 => 'Enabled',
				0 => 'Disabled'
			)
		));

        $codeFieldAttributes = array(
            'title'     => Mage::helper('frans')->__('Store Code'),
            'label'     => Mage::helper('frans')->__('Store Code'),
            'name'      => 'store_code',
            'class'     => 'required-entry',
            'required'  => true,
        );

        // don't allow editing of code if we are editing an existing item
        //if ($model = Mage::registry('index_data'))
        //{
        //    if ($model->getId())
        //    {
        //        $codeFieldAttributes['disabled'] = 'disabled';
        //    }
        //}

		$fieldset->addField('store_code', 'text', $codeFieldAttributes);
		
		$fieldset->addField('position', 'text', array(
	          'title'     => Mage::helper('frans')->__('Position'),
			  'label'     => Mage::helper('frans')->__('Position'),
	          'name'      => 'position',
			  'class'     => 'required-entry', 
	          'required'  => true,
			  'note'	  => 'Determines sort order in shipping method list'
	      	));

		$fieldset->addField('url_key', 'text', array(
	          'title'     => Mage::helper('frans')->__('URL Key'),
			  'label'     => Mage::helper('frans')->__('URL Key'),
	          'name'      => 'url_key',
			  'class'     => 'required-entry',
	          'required'  => true,
			  'note'	  => 'Unique URL key to access this store details page, you will still need to manually create a corresponding URL rewrite',
	      	));

		$fieldset->addField('name', 'text', array(
			'title'     => Mage::helper('frans')->__('Name'),
			'label'     => Mage::helper('frans')->__('Name'),
			'name'      => 'name'
			//'class'     => 'required-entry',
			//'required'  => true,
			//'note'	  => 'My Note Here',
		));
	    $fieldset->addField('subtitle', 'text', array(
	          'title'     => Mage::helper('frans')->__('Subtitle'),
			  'label'     => Mage::helper('frans')->__('Subtitle'),
	          'name'      => 'subtitle'
			  //'class'     => 'required-entry', 
	          //'required'  => true,
			  //'note'	  => 'My Note Here',
	      	));

		//Store image large
		$fieldset->addField('store_image_large', 'image', array(
			'title'     => Mage::helper('frans')->__('Store Image (Large)'),
			'label'     => Mage::helper('frans')->__('Store Image (Large)'),
			'name'      => 'store_image_large',
			'note'      => '750x390 primary image on store details page'
		));

		//Store image banner
		$fieldset->addField('store_image_banner', 'image', array(
			'title'     => Mage::helper('frans')->__('Store Image (Banner)'),
			'label'     => Mage::helper('frans')->__('Store Image (Banner)'),
			'name'      => 'store_image_banner',
			'note'      => '1190x210 wide banner on store details page, row of 3 images: 1st is 440px wide, 2nd and 3rd are 375px wide'
		));

		//Store image tile
		$fieldset->addField('store_image_tile', 'image', array(
			'title'     => Mage::helper('frans')->__('Store Image (Tile)'),
			'label'     => Mage::helper('frans')->__('Store Image (Tile)'),
			'name'      => 'store_image_tile',
			'note'      => "290x50 tall tile image on Fran's Stores overview page"
		));

		//Store image thumbnail
		$fieldset->addField('store_image_thumbnail', 'image', array(
			'title'     => Mage::helper('frans')->__('Store Image (Thumbnail)'),
			'label'     => Mage::helper('frans')->__('Store Image (Thumbnail)'),
			'name'      => 'store_image_thumbnail',
			'note'      => '231x110 small image shown at bottom of contact page'
		));
		
		$fieldset->addField('address1', 'text', array(
	          'title'     => Mage::helper('frans')->__('Address Line 1'),
			  'label'     => Mage::helper('frans')->__('Address Line 1'),
	          'name'      => 'address1'
			  //'class'     => 'required-entry', 
	          //'required'  => true,
			  //'note'	  => 'My Note Here',
	      	));
		
		$fieldset->addField('address2', 'text', array(
	          'title'     => Mage::helper('frans')->__('Address Line 2'),
			  'label'     => Mage::helper('frans')->__('Address Line 2'),
	          'name'      => 'address2'
			  //'class'     => 'required-entry', 
	          //'required'  => true,
			  //'note'	  => 'My Note Here',
	      	));
		
		$fieldset->addField('city', 'text', array(
	          'title'     => Mage::helper('frans')->__('City'),
			  'label'     => Mage::helper('frans')->__('City'),
	          'name'      => 'city'
			  //'class'     => 'required-entry', 
	          //'required'  => true,
			  //'note'	  => 'My Note Here',
	      	));
		
		$fieldset->addField('region', 'text', array(
	          'title'     => Mage::helper('frans')->__('State'),
			  'label'     => Mage::helper('frans')->__('State'),
	          'name'      => 'region'
			  //'class'     => 'required-entry', 
	          //'required'  => true,
			  //'note'	  => 'My Note Here',
	      	));
		
		$fieldset->addField('postal_code', 'text', array(
	          'title'     => Mage::helper('frans')->__('ZIP'),
			  'label'     => Mage::helper('frans')->__('ZIP'),
	          'name'      => 'postal_code'
			  //'class'     => 'required-entry', 
	          //'required'  => true,
			  //'note'	  => 'My Note Here',
	      	));
		
		$fieldset->addField('country', 'text', array(
	          'title'     => Mage::helper('frans')->__('Country'),
			  'label'     => Mage::helper('frans')->__('Country'),
	          'name'      => 'country'
			  //'class'     => 'required-entry', 
	          //'required'  => true,
			  //'note'	  => 'My Note Here',
	      	));
	      	
	    $fieldset->addField('maps_url', 'text', array(
	          'title'     => Mage::helper('frans')->__('Map Location URL'),
			  'label'     => Mage::helper('frans')->__('Map Location URL'),
	          'name'      => 'maps_url',
	          'class'	  => 'validate-url',
	    	  'note'	  => 'Enter URL that starts with http://'
	      	));
		
		$fieldset->addField('phone', 'text', array(
	          'title'     => Mage::helper('frans')->__('Phone Number'),
			  'label'     => Mage::helper('frans')->__('Phone Number'),
	          'name'      => 'phone'
			  //'class'     => 'required-entry', 
	          //'required'  => true,
			  //'note'	  => 'My Note Here',
	      	));
		
		$fieldset->addField('store_hours', 'textarea', array(
	          'title'     => Mage::helper('frans')->__('Store Hours'),
			  'label'     => Mage::helper('frans')->__('Store Hours'),
	          'name'      => 'store_hours',
	          'style'    => 'height: 5em;',
			  //'class'     => 'required-entry', 
	          //'required'  => true,
			  'note'	  => 'Use | to separate days and time. Example: Mon-Sat | 9AM-5PM. New lines will add new rows to the time table.',
	      	));

		$fieldset->addField('map_image', 'image', array(
			'title'     => Mage::helper('frans')->__('Map Image'),
			'label'     => Mage::helper('frans')->__('Map Image'),
			'name'      => 'map_image',
			'note'      => 'Image needs to be exactly 300x200'
		));

		$fieldset->addField('cross_sell_label', 'text', array(
			'title'     => Mage::helper('frans')->__('Cross-Sell Label'),
			'label'     => Mage::helper('frans')->__('Cross-Sell Label'),
			'name'      => 'cross_sell_label',
			'note'      => 'Label above cross-sell section for this retail store.'
		));

		$fieldset->addField('cross_sell_skus', 'text', array(
			'title'     => Mage::helper('frans')->__('Cross-Sell Products'),
			'label'     => Mage::helper('frans')->__('Cross-Sell Products'),
			'name'      => 'cross_sell_skus',
			'note'      => 'Comma-separated list of product SKUs to be featured for this store. Leave empty to hide cross-sell section. Products in this list must be visible in catalog.'
		));
		
		/*
	    * EXAMPLE CODE. CAN BE REMOVED.
	    *
		
		//text
		$fieldset->addField('mytext', 'text', array(
	          'title'     => Mage::helper('frans')->__('MyText'),
			  'label'     => Mage::helper('frans')->__('MyText'),
	          'name'      => 'name',
			  //'class'     => 'required-entry', 
	          //'required'  => true,
			  //'note'	  => 'My Note Here',
	      	));
		
	    //yes no
	    $fieldset->addField('myyesno', 'select', array(
		          'title'    	 => Mage::helper('frans')->__('Add To Cart'),
				  'label'    	 => Mage::helper('frans')->__('Add To Cart'),
		          //'class'     => 'required-entry', 
				  //'required'  	=> true,
		          'name'     	 => 'myyesno',
		    	  'checked'=> false,
		    	  'name'  => 'myyesno',
		    	  'options'   => array(
									'1' => 'Yes',
									'0' => 'No',
						  ),
				  'note'	=> Mage::helper('frans')->__("My Note."),

      		));   	
	   
		//money
	   	$fieldset->addField('myprice', 'text', array(
          'title'     => Mage::helper('frans')->__('MyPrice'),
		  'label'     => Mage::helper('frans')->__('MyPrice'),
       	  'name'      => 'myprice',   
	   	  //'class'     => 'required-entry validate-zero-or-greater',
          //'required'  => true,
		  //'note' => 	 Mage::helper('frans')->__("My Note Here.")
      		));
	  
      	//options
      	$options = Mage::getSingleton('event/facility')->getCollection()->setOrder("name", "asc")->toOptionArray("id", "name", "Select");
      	$fieldset->addField('myoptions', 'select', array(
          'label'     => Mage::helper('frans')->__('MyOptions'),
		  'title'     => Mage::helper('frans')->__('MyOptions'),
          //'class'     => 'required-entry',
          //'required'  => true,
          'name'      => 'myoptions',
		  'values'   => $options
      	));
		
		//status
		$fieldset->addField('status', 'select',array(
			'label'     => Mage::helper('frans')->__('Status'),
			'required'  => true,
			'name'      => 'status',
			'options'   => array(
					  		'Enabled' => 'Enabled',
					  		'Disabled' => 'Disabled',
				  ),
		));
		
		// END EXAMPLE CODE
		*/
		
	
	
		if ( Mage::getSingleton('adminhtml/session')->getIndexData() )
		{
	    	$form->setValues(Mage::getSingleton('adminhtml/session')->getIndexData());
	    	Mage::getSingleton('adminhtml/session')->getIndexData(null);
		}
		elseif(Mage::registry('index_data'))
		{
	    	$form->setValues(Mage::registry('index_data')->getData());
		}
		
		return parent::_prepareForm();
	}
  
}