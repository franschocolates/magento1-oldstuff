<?php

/**
 * Order grid for capturing; contains a subset or orders based on those that are capturable
 *
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Capture_Grid extends Mage_Adminhtml_Block_Sales_Order_Grid
{
    /**
     * Massaction block name
     * Changed to allow us to have out auto disable type
     *
     * @var string
     */
    protected $_massactionBlockName = 'frans/adminhtml_widget_grid_massaction_autoDisable';

    public function __construct()
    {
        parent::__construct();
        $this->setId('sales_order_capture_grid');
        $this->setUseAjax(true);
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(false);
        // $this->setDefaultFilter(array(
        //             array('status' => 'shipped'),
        //             array('status' => 'picked_up')
        //         )
        //     );
    }

    protected function _prepareCollection()
    {
        // we have to use sales/order and not order_grid
        // because we want some columns that aren't on grid.
        /** @var GoSolid_Frans_Model_Resource_Sales_Order_Collection $collection */
        $collection = Mage::getModel('sales/order')->getCollection()
                        ->addReadyToCaptureFilter()
                        ->addPaymentMethodJoin();

        $filter = $this->getParam($this->getVarNameFilter(), null);

        $data = $this->helper('adminhtml')->prepareFilterString($filter);

        if (!is_string($filter) || (is_array($data) && !isset($data['status']))){
            $collection->addAttributeToSearchFilter('status', array('in' => array('shipped', 'picked_up', 'processing')));
        }
        
        $this->setCollection($collection);

        Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();

        return $this;
    }

	protected function _prepareColumns()
    {
        parent::_prepareColumns();
        
        # change the order ID to be wide enough for our increment ID
        $this->_columns['real_order_id']['width'] = '100px';

        // change away from "G.T. "
        $this->_columns['grand_total']['header'] = Mage::helper('frans')->__('Grand Total');

        // we need the following columns:
        $this->addColumnAfter('accounting_label', array(
            'header' => Mage::helper('sales')->__('Label'),
            'width'  => '80px',
            'type'   => 'options',
            'options' => Mage::getModel('frans/accounting_label')->getCollection()->toGridOptionArray('label_name', 'label_name'),
            'index'  => 'accounting_label',
        ), 'real_order_id');

        $this->addColumnAfter('ship_date', array(
            'header' => Mage::helper('sales')->__('Ship Date'),
            'index'  => 'ship_date',
            'type'   => 'date',
            'width'  => '100px',
            'format' => 'M/d/Y',  // this is Zend formatting, not PHP formatting
        ), 'accounting_label');

        Mage::log('gridding');

        $payment_method_keys = array_keys(Mage::getModel('payment/config')->getActiveMethods());
        $payment_method_paths = array_map(function($key){ return "payment/" . $key . "/title"; }, $payment_method_keys);
        $payment_method_titles = Mage::getModel('core/config_data')->getCollection()
                            ->addFieldToSelect('value', 'name')
                            ->addExpressionFieldToSelect('id', "REPLACE(REPLACE(path, 'payment/', ''), '/title', '')", [])
                            ->addFieldToFilter('path', array('in' => $payment_method_paths))
                            ->toOptionHash();

        $this->addColumnAfter('method', array(
            'header' => Mage::helper('frans')->__('Payment Method'),
            'index' => 'sfop.method',
            'type'   => 'options',
            'width'  => '80px',
            'filter_index' => 'sfop.method',
            'options' => $payment_method_titles
        ), 'ship_date');

        $this->addColumnAfter('giftcard_amount', array(
            'header' => Mage::helper('sales')->__('Gift Card Amount'),
            'index'  => 'giftcard_amount',
            'type'  => 'currency',
            'currency' => 'order_currency_code',
        ), 'payment_method');

        // remove a bunch of columns
        $this->removeColumn('shipping_name');
        $this->removeColumn('billing_name');
        $this->removeColumn('base_grand_total');
        $this->removeColumn('action');

        // removing a couple of redundant columns.
        // 'base' grand total is really only relevant with multiple currencies
        // and action is supplanted since you can click on any row

        return $this;
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('order_ids');
        $this->getMassactionBlock()->setUseSelectAll(true);

        // this probably needs to be updated
        $this->getMassactionBlock()->addItem('capture_order', array(
                'label' => Mage::helper('frans')->__('Capture'),
                'url'   => $this->getUrl('*/sales_order/massCapture', array('origin' => 'capture_grid'))
            ));

        return $this;
    }

    /**
     * Suppress RSS Lists
     */
    public function getRssLists()
    {
        return false;
    }

    public function getRowUrl($row)
    {
        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view')) {
            return $this->getUrl('adminhtml/sales_order/view', array('order_id' => $row->getId()));
        }
        return false;
    }

}
