<?php

class GoSolid_Frans_Block_Adminhtml_ProductPiece_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct()
    {
                 
        $this->_objectId = 'piece_id';
        $this->_blockGroup = 'frans';
        $this->_controller = 'adminhtml_productPiece';
		
		$this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('frans')->__('Save and Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);
		
        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
			
        ";

	    parent::__construct();
    }

	public function getHeaderText()
    {  
        if( Mage::registry('index_data') && Mage::registry('index_data')->getPieceId() ) {
            return Mage::helper('frans')->__("Edit");
		} else {
            return Mage::helper('frans')->__('Add');
        }
    }

}