<?php
class GoSolid_Frans_Block_Adminhtml_Colortile_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{

		parent::__construct();
		$this->setId('fransGrid');

		//TODO: Set default sort
		$this->setDefaultSort('position');
		$this->setDefaultDir('ASC');

		//TODO: Optional
		$this->setSaveParametersInSession(true);
		//$this->setDefaultFilter(array('status' => 'Enabled'));
	}
 
	protected function _prepareCollection()
	{
		$collection = Mage::getModel('frans/colorTile')->getCollection();

		$this->setCollection($collection);
		return parent::_prepareCollection();
	}	
		
		
	protected function _prepareColumns()
  	{

	    $this->addColumn('piece_image', array(
		    'header'    => Mage::helper('frans')->__('Image'),
		    'align'     => 'center',
		    'index'     => 'image',
		    'width'     => '45px',
		    'renderer'  => 'frans/adminhtml_colortile_grid_renderer_image',
		    'sort'      => false,
		    'filter'    => false
	    ));

	    $this->addColumn('title', array(
		    'header'    => Mage::helper('frans')->__('Title'),
		    'align'     =>'left',
		    'index'     => 'title',
	    ));

	    $this->addColumn('status', array(
		    'header'    => Mage::helper('frans')->__('Enabled'),
		    'align'     => 'left',
		    'width'     => '90px',
		    'index'     => 'status',
		    'type'	  => 'options',
		    'options'   => array(
			    1 => 'Enabled',
			    0 => 'Disabled'
		    )
	    ));

		$this->addColumn('position', array(
			'header'    => Mage::helper('frans')->__('Position'),
			'type'      => 'number',
			'align'     =>'left',
			'width'	 	=> 50,
			'index'     => 'position',
		));
		  
		  /*
		   * EXAMPLE CODE. CAN BE REMOVED.
		   *
		  
		  //text plain
		  $this->addColumn('myname', array(
			  'header'    => Mage::helper('frans')->__('MyName'),
			  'align'     =>'left',
			  'index'     => 'name',
		  ));
		  
		  //date
		  $this->addColumn('mydate', array(
	            'header'    => Mage::helper('frans')->__('MyDate'),
	            'type'      => 'date',
	            'align'     => 'left',
	            'index'     => 'mydate', //change me
	            'gmtoffset' => false
	        ));
	        
	      //options
	      $options = Mage::getModel('frans/[model_lcase]')->getCollection()->setOrder("title", "asc")->toGridOptionArray("id", "title");
		  $this->addColumn('category_id', array(
			  'header'    => Mage::helper('frans')->__('Category'),
			  'align'     => 'left',
			  'index'     => 'category_id',
			  'type'  => 'options',
			  'options'	=> $options, //example of how to get options.
		  )); 
		  
		  //status
		  $this->addColumn('status', array(
			  'header'    => Mage::helper('frans')->__('Status'),
			  'align'     => 'left',
			  'width'     => '150px',
			  'index'     => 'status',
			  'type'	  => 'options',
			  'options'   => array(
				  'Enabled' => 'Enabled',
				  'Disabled' => 'Disabled',
			  ),
		  ));
		  
		  END EXAMPLE CODE
		  */ 
		  

		  return parent::_prepareColumns();
	}
	
	public function getRowUrl($row)
  	{
    	return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  	}
}