<?php
/**
 * Encapsulates overall logic for managing shipments in a multiship quote
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Create_Shipments
    extends Mage_Adminhtml_Block_Sales_Order_Create_Abstract
{
    /**
     * Define block ID
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('sales_order_create_shipments');
    }

    /**
     * Accordion header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        return Mage::helper('frans')->__('Shipments');
    }

    /**
     * Render buttons and return HTML code
     *
     * @return string
     */
    public function getButtonsHtml()
    {
        return $this->getLayout()
            ->createBlock('adminhtml/widget_button')
            ->setData(array(
                'class'     => 'task'
                , 'onclick' => 'order.showImport()'
                , 'id'      => 'show_import_button'
                ,'label'    => Mage::helper('frans')->__('Upload Shipments File'),
            ))
            ->toHtml();
    }

    public function getValidateImportButtonHtml()
    {
        return $this->getLayout()
                    ->createBlock('adminhtml/widget_button')
                    ->setData(array(
                        'class'     => 'save'
                        , 'onclick' => "order.validateImport('{$this->getUrl('*/*/validateImport')}')"
                        , 'id'      => 'validate_import_button'
                        ,'label'    => Mage::helper('frans')->__('Validate File'),
                    ))
                    ->toHtml();
    }

    public function getIframeUrl()
    {
        return $this->getUrl('*/*/importQuote');
    }

}