<?php
class GoSolid_Frans_Block_Adminhtml_Retailstore_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{

		parent::__construct();
		$this->setId('fransGrid');
		
		//TODO: Set default sort
		//$this->setDefaultSort('id');
		//$this->setDefaultDir('ASC');
		
		//TODO: Optional
		//$this->setSaveParametersInSession(true);
		//$this->setDefaultFilter(array('status' => 'Enabled'));
	}
 
	protected function _prepareCollection()
	{
		$collection = Mage::getModel('frans/retailStore')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}	
		
		
	protected function _prepareColumns()
  	{

  		/*$this->addColumn('id', array(
			  'header'    => Mage::helper('frans')->__('ID'),
			  'align'     => 'right',
			  'width'     => '50px',
			  'index'     => 'id'
		  ));*/
		  
		  $this->addColumn('name', array(
			  'header'    => Mage::helper('frans')->__('Name'),
			  'align'     => 'left',
			  'index'     => 'name'
		  ));
		  
		  $this->addColumn('address1', array(
			  'header'    => Mage::helper('frans')->__('Address Line 1'),
			  'align'     => 'left',
			  'index'     => 'address1'
		  ));
		  
		  $this->addColumn('address2', array(
			  'header'    => Mage::helper('frans')->__('Address Line 2'),
			  'align'     => 'left',
			  'index'     => 'address2'
		  ));
		  
		  $this->addColumn('city', array(
			  'header'    => Mage::helper('frans')->__('City'),
			  'align'     => 'left',
			  'index'     => 'city'
		  ));
		  
		  $this->addColumn('region', array(
			  'header'    => Mage::helper('frans')->__('State'),
			  'align'     => 'left',
			  'width'     => '90px',
			  'index'     => 'region'
		  ));
		  
		  $this->addColumn('phone', array(
			  'header'    => Mage::helper('frans')->__('Phone Number'),
			  'align'     => 'left',
			  'width'     => '140px',
			  'index'     => 'phone'
		  ));
		  
		  $this->addColumn('status', array(
			  'header'    => Mage::helper('frans')->__('Status'),
			  'align'     => 'left',
			  'width'     => '90px',
			  'index'     => 'status',
			  'type'	  => 'options',
			  'options'   => array(
				  1 => 'Enabled',
				  0 => 'Disabled'
			  )
		  ));
		  
		  
		  /*
		   * EXAMPLE CODE. CAN BE REMOVED.
		   *
		  
		  //text plain
		  $this->addColumn('myname', array(
			  'header'    => Mage::helper('frans')->__('MyName'),
			  'align'     =>'left',
			  'index'     => 'name',
		  ));
		  
		  //date
		  $this->addColumn('mydate', array(
	            'header'    => Mage::helper('frans')->__('MyDate'),
	            'type'      => 'date',
	            'align'     => 'left',
	            'index'     => 'mydate', //change me
	            'gmtoffset' => false
	        ));
	        
	      //options
	      $options = Mage::getModel('frans/[model_lcase]')->getCollection()->setOrder("title", "asc")->toGridOptionArray("id", "title");
		  $this->addColumn('category_id', array(
			  'header'    => Mage::helper('frans')->__('Category'),
			  'align'     => 'left',
			  'index'     => 'category_id',
			  'type'  => 'options',
			  'options'	=> $options, //example of how to get options.
		  )); 
		  
		  //status
		  $this->addColumn('status', array(
			  'header'    => Mage::helper('frans')->__('Status'),
			  'align'     => 'left',
			  'width'     => '150px',
			  'index'     => 'status',
			  'type'	  => 'options',
			  'options'   => array(
				  'Enabled' => 'Enabled',
				  'Disabled' => 'Disabled',
			  ),
		  ));
		  
		  END EXAMPLE CODE
		  */ 
		  

		  return parent::_prepareColumns();
	}
	
	public function getRowUrl($row)
  	{
    	return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  	}
}