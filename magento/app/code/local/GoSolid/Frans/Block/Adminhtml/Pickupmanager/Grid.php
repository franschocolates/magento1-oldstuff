<?php
class GoSolid_Frans_Block_Adminhtml_Pickupmanager_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{

		parent::__construct();



		$this->setId('fransGrid');
		
		//TODO: Set default sort
		//$this->setDefaultSort('id');
		//$this->setDefaultDir('ASC');
		
		//TODO: Optional
		//$this->setSaveParametersInSession(true);
		//$this->setDefaultFilter(array('status' => 'Enabled'));


	}

    protected function _getCollectionClass()
    {
        return 'sales/order_grid_collection';
    }

	protected function _prepareCollection()
	{
        $collection = Mage::getResourceModel($this->_getCollectionClass());

        $pickupAtStoreCode = Mage::getModel("frans/shipping_carrier_pickupatstore")->getCarrierCode();
        $pickupAtStoreCode .= "_"; //because they have to be an underscore after the carrier code.

        $collection->addFieldToFilter("shipping_method", array("like" => $pickupAtStoreCode . "%"));
        $collection->addFieldToFilter("status", array("nin" => array("canceled","picked_up")));
        /*
        $resource = Mage::getSingleton('core/resource');
        $collection->join(
            array('billing' => $resource->getTableName('sales/order_address')),
            'order.billing_address_id = billing.entity_id',
            array('billing_country' => 'country_id')
        );

        $collection->joinLeft(
            array('shipping' => $resource->getTableName('sales/order_address')),
            'order.shipping_address_id = shipping.entity_id',
            array('shipping_country' => 'country_id')
        );
        */

        $collection->join(
            'sales/order_address',
            '`sales/order_address`.parent_id=`main_table`.entity_id',
            array('company')
        )->addFieldToFilter('sales/order_address.address_type',array('eq'=>'billing'));

        $collection->join(
            array('sfop'=>'sales/order_payment'),
            'sfop.parent_id = `main_table`.entity_id',
            array('sfop.method')
        );
        /*
        $collection->join(
            'sales/order_item',
            '`sales/order_item`.order_id=`main_table`.entity_id',
            array(
                'skus' => new Zend_Db_Expr('group_concat(`sales/order_item`.sku)'),
                'names' => new Zend_Db_Expr('group_concat(`sales/order_item`.name)')
            )
        );
        */
        //$pickupAtStore = Mage::getModel("frans/shipping_carrier_pickupatstore");
        //var_dump($pickupAtStore->getAllowedMethods()); die;

        $this->setCollection($collection);
		return parent::_prepareCollection();
	}	
		
		
	protected function _prepareColumns()
  	{

        $this->addColumn('shipping_method', array(
            'header' => Mage::helper('sales')->__('Store'),
            'type' => 'options',
            'width' => '150px',
            'options' => Mage::getModel("frans/retailStore")->getCollection()->toShipMethodGridOptionArray(),
            'index'  => 'shipping_method',
        ), 'shipping_name');


        $this->addColumn('real_order_id', array(
            'header'=> Mage::helper('sales')->__('Order #'),
            'width' => '90px',
            'type'  => 'text',
            'index' => 'increment_id',
        ));

        $this->addColumn('billing_name', array(
            'header' => Mage::helper('sales')->__('Bill to Name'),
            'index' => 'billing_name',
            'width' => '150px',
        ));

        $this->addColumn('billing_company', array(
            'header' => Mage::helper('sales')->__('Bill to Company'),
            'index' => 'company',
            'width' => '150px'
        ));

        $this->addColumn('shipping_name', array(
        'header' => Mage::helper('sales')->__('Ship to Name'),
        'index' => 'shipping_name',
        'width' => '150px',
    ));


        $this->addColumn('grand_total', array(
            'header' => Mage::helper('sales')->__('Grand Total'),
            'index' => 'grand_total',
            'width' => '150px',
            'type'  => 'currency',
            'currency' => 'base_currency_code',
        ));

        $this->addColumn('created_at', array(
            'header' => Mage::helper('sales')->__('Purchased On'),
            'index' => 'created_at',
            'type' => 'datetime',
            'width' => '100px',
        ));

        $this->addColumnAfter('planned_ship_date', array(
            'header' => Mage::helper('frans')->__('Planned Ship Date'),
            'index'  => 'planned_ship_date',
            'type'   => 'date',
            'renderer'  => 'GoSolid_Frans_Block_Adminhtml_Sales_Order_Grid_Renderer_ConditionalMultishipDate',
            'filter_condition_callback' => array($this, '_filterShipDate'),
        ), 'shipping_description');

        /*
        $this->addColumnAfter('ship_date', array(
            'header' => Mage::helper('frans')->__('Picked up On'),
            'index'  => 'ship_date',
            'type'   => 'date',
            'renderer'  => 'GoSolid_Frans_Block_Adminhtml_Sales_Order_Grid_Renderer_ConditionalMultishipDate',
            'filter_condition_callback' => array($this, '_filterShipDate'),
        ), 'planned_ship_date');
        */

        $statuses = Mage::getResourceModel('sales/order_status_collection')->addFieldToFilter('status',array("nin"=>array("canceled","picked_up")))->toOptionHash();

        $this->addColumnAfter('status', array(
            'header' => Mage::helper('sales')->__('Status'),
            'index' => 'status',
            'type'  => 'options',
            'width' => '70px',
            'options' => $statuses, # Mage::getSingleton('sales/order_config')->getStatuses(), // -- REMOVED to use filtered collection
        ), 'ship_date');

        $this->addColumn('paid', array(
            'header' => Mage::helper('sales')->__('Paid'),
            'index' => 'status',
            'type'  => 'options',
            'width' => '70px',
            'renderer'  => 'GoSolid_Frans_Block_Adminhtml_Sales_Order_Grid_Renderer_ConditionalPaid',
            'filter_condition_callback' => array($this, '_filterPaid'),
            'options' => array(1=>'Yes',2=>'No'),
        ));
		  /*
		   * EXAMPLE CODE. CAN BE REMOVED.
		   *
		  
		  //text plain
		  $this->addColumn('myname', array(
			  'header'    => Mage::helper('frans')->__('MyName'),
			  'align'     =>'left',
			  'index'     => 'name',
		  ));
		  
		  //date
		  $this->addColumn('mydate', array(
	            'header'    => Mage::helper('frans')->__('MyDate'),
	            'type'      => 'date',
	            'align'     => 'left',
	            'index'     => 'mydate', //change me
	            'gmtoffset' => false
	        ));
	        
	      //options
	      $options = Mage::getModel('frans/[model_lcase]')->getCollection()->setOrder("title", "asc")->toGridOptionArray("id", "title");
		  $this->addColumn('category_id', array(
			  'header'    => Mage::helper('frans')->__('Category'),
			  'align'     => 'left',
			  'index'     => 'category_id',
			  'type'  => 'options',
			  'options'	=> $options, //example of how to get options.
		  )); 
		  
		  //status
		  $this->addColumn('status', array(
			  'header'    => Mage::helper('frans')->__('Status'),
			  'align'     => 'left',
			  'width'     => '150px',
			  'index'     => 'status',
			  'type'	  => 'options',
			  'options'   => array(
				  'Enabled' => 'Enabled',
				  'Disabled' => 'Disabled',
			  ),
		  ));
		  
		  END EXAMPLE CODE
		  */ 
		  

		  return parent::_prepareColumns();
	}

    protected function _filterShipDate($collection, $column)
    {
        $filter = $column->getFilter();
        $value = $filter->getValue();

        // annoyingly, magento converts it to a datetime, rather than leaving as a date. As a result,
        // dates don't match when there is only one value
        // we just reset it back to the original
        $select = $collection->getSelect();

        if (isset($value['orig_from']))
        {
            $fromDate = date("Y-m-d", strtotime($value['orig_from']));
            $select->where("planned_ship_date >= '$fromDate'");
        }
        if (isset($value['orig_to']))
        {
            $toDate = date("Y-m-d", strtotime($value['orig_to']));
            $select->where("planned_ship_date <= '$toDate'");
        }

        return $this;
    }

    protected function _filterPaid($collection, $column){
        $filter = $column->getFilter();
        $value = $filter->getValue();

        $select = $collection->getSelect();
        $op = ($value == 2) ? "=" : "!=";
        $select->where('sfop.method ' . $op . ' "pay_at_pickup"');
        return $this;
    }

	public function getRowUrl($row)
  	{
    	return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  	}
}