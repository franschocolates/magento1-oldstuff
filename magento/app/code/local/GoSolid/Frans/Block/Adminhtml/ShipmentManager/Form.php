<?php

class GoSolid_Frans_Block_Adminhtml_ShipmentManager_Form extends Mage_Adminhtml_Block_Sales_Order_Abstract
{
	public function getSaveUrl()
	{
		return $this->getUrl('*/*/saveShipment', array('order_id' => $this->getOrder()->getId()));
	}

	public function getOrderViewUrl()
	{
		return $this->getUrl('adminhtml/sales_order/view', array('order_id' => $this->getOrder()->getId()));
	}

	public function getReprintUrl()
	{
		return $this->getUrl('*/*/reprint', array());
	}

	public function getShippingOptions()
	{
		# loop through all active carriers
		$shippingConfig = Mage::getSingleton('shipping/config');
		$shippingCarriers = $shippingConfig->getActiveCarriers();
		
		# use two different arrays so we can make current be first.
		$shippingOptions = array();
		$firstOption = array();
		
		$current = $this->getOrder()->getShippingMethod(false);
		
		foreach ($shippingCarriers as $code => $instance)
		{
			$carrierOptions = $instance->getAllowedMethods();
			
			foreach ($carrierOptions as $value => $label)
			{
				if ($instance->getIsAggregateCarrier())
				{
					$fullValue = $value;
					$display = $label;
				}
				else
				{
					$fullValue = $code . '_' . $value;
					$display = $instance->getConfigData('title') . ' - ' . $label;
				}
				if ($current == $fullValue)
				{
					$firstOption[$fullValue] = $display;
				}
				else
				{
					$shippingOptions[$fullValue] = $display;
				}
			}
		}
				
		return $firstOption + $shippingOptions;
	}
	
	public function getItemsHtml()
	{
		$itemsBlock = $this->getLayout()
						->createBlock('adminhtml/sales_order_view_items');
						
		$itemsBlock->setParentBlock($this)
				->setTemplate('shipmentManager/view/items.phtml');
		return $itemsBlock->toHtml();
	}
	
	public function getIcePackWeight()
	{
		return Mage::getModel('frans/icePack')->getWeight();
	}
	
	public function getInitialOrderWeight()
	{
		return $this->getOrder()->getWeightWithIcePacks();
	}

    public function showShipDateWarning()
    {
        return Mage::getModel('core/date')->date('Y-m-d') !=
                    $this->getOrder()->getPlannedShipDate();
    }

	/**
	 * 
	 * Gets an array with possible ship dates
	 * Used to display 
	 */
	public function getShipDateOptions()
	{
		$options = array();
		$maxDaysInFuture = 3;

        $date = Mage::getModel('core/date')->date();

		$date = new DateTime($date);
		$labelFormat = 'l M d, Y';
		$valueFormat = 'Y-m-d';
		$options['Today, ' . $date->format($labelFormat)] = $date->format($valueFormat);
		$dayInterval = DateInterval::createFromDateString('+1 day');
		for ($i = 0; $i < $maxDaysInFuture; $i++)
		{
			# PHP Date functions modify the date value,
			# so we only need to add one day
			$date->add($dayInterval);
			$prefix = ($i == 0) ? 'Tomorrow, ' : '';
			$options[$prefix . $date->format($labelFormat)] = $date->format($valueFormat);
		}
		
		return $options;
	}

	public function getIsWeekend()
	{
		// clever test from Stack Overflow
		return (Mage::getModel('core/date')->date('N') >= 6);
	}

	public function getNextWeekday()
	{
		return Mage::getModel('core/date')->date('Y-m-d', Mage::getModel('core/date')->gmtTimestamp('+ 1 weekdays'));
	}

	/**
	 * Display the available label printer drop down list and the Print Label button
	 */
	public function getPrintLabelBtn($type="button")
	{
		$printStations = Mage::getModel("frans/printStation")->getActivePrintStations();
		$defaultPrintStation = Mage::getModel('core/cookie')->get("default_print_station");

		$html = "<button type='" . $type . "' id='print_label' class='scalable btn-float-right'>Print Label</button>
				<select name='print_station' id='print_station' class='btn-float-right'>";

					foreach($printStations as $ps):
						$html .= "<option value='" . $ps->getId() . "'";
							if($defaultPrintStation == $ps->getId())
							{
								$html .= " selected";
							}
						$html .= " >" . $ps->getStationName() . "</option>";
					endforeach;

		$html .= "</select>";

		return $html;
	}
}