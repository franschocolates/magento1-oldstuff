<?php
/**
 * Adminhtml sales order create shipment address block
 * Differs from shipping, in that there are multiple shipments in a multiship order
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Create_Shipments_Grid_Address_Form
    extends Mage_Adminhtml_Block_Sales_Order_Create_Form_Address
{
    public function __construct()
    {
        $this->setTemplate('sales/order/create/shipments/grid/address.phtml');
        parent::__construct();
    }


    public function getAddressDisplayHtml()
    {
        // see config in frans\config.xml config/default/customer/address_templates
        return $this->getAddress()->format('admin_multiship_template');
    }

    /**
     * Prepare Form and add elements to form
     *
     * @return Mage_Adminhtml_Block_Sales_Order_Create_Shipping_Address
     */
    protected function _prepareForm()
    {
        $quoteAddressId = $this->getQuoteAddressId();
        $this->setJsVariablePrefix('shipmentAddress' . $quoteAddressId);
        parent::_prepareForm();

        $this->_form->addFieldNameSuffix(sprintf('shipment[%s]', $quoteAddressId));
        $this->_form->setHtmlNamePrefix(sprintf('shipment[%s]', $quoteAddressId));
        $this->_form->setHtmlIdPrefix(sprintf('order-shipment_address_%s_', $quoteAddressId));

        return $this;
    }

    /**
     * Return is shipping address flag
     *
     * @return boolean
     */
    public function getIsShipping()
    {
        return true;
    }

    /**
     * Same as billing address flag
     *
     * @return boolean
     */
    public function getIsAsBilling()
    {
        return false;
    }

    /**
     * Saving shipping address must be turned off, when it is the same as billing
     *
     * @return bool
     */
    public function getDontSaveInAddressBook()
    {
        return false;
    }

    /**
     * Return Form Elements values
     *
     * @return array
     */
    public function getFormValues()
    {
        return $this->getAddress()->getData();
    }

    /**
     * Return customer address id
     *
     * @return int|boolean
     */
    public function getAddressId()
    {
        return $this->getAddress()->getCustomerAddressId();
    }

    public function getQuoteAddressId()
    {
        return $this->getAddress()->getId();
    }

    /**
     * Return is address disabled flag
     * For now, never disabled (why have a disabled sihpment?)
     *
     * @return boolean
     */
    public function getIsDisabled()
    {
        return false;
    }
}
