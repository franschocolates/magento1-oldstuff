<?php
 
class GoSolid_Frans_Block_Adminhtml_XcartOrder extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {

	$this->_controller = 'adminhtml_xcartOrder';
	$this->_blockGroup = 'frans';
	$this->_headerText = Mage::helper('frans')->__("XCart Orders");

    parent::__construct();
    
  }

    protected function _beforeToHtml()
    {
        parent::_beforeToHtml();

        $this->_removeButton('add');

        return $this;
    }


}