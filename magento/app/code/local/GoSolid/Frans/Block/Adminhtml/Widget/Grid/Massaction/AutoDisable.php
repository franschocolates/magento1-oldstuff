<?php
/**
 * Created by goSolid to add JS to auto disable a mass action button when clicked
 */
class GoSolid_Frans_Block_Adminhtml_Widget_Grid_Massaction_AutoDisable
        extends Mage_Adminhtml_Block_Widget_Grid_Massaction_Abstract
{
    /**
     * Retrieve apply button html
     *
     * @return string
     */
    public function getApplyButtonHtml()
    {
        return $this->getButtonHtml($this->__('Submit'), "{$this->getJsObjectName()}.applyOnce(this)");
    }

}