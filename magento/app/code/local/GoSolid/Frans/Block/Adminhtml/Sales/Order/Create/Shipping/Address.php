<?php
/**
 * Created by goSolid.
 * Date: 8/8/14
 * Time: 8:52 AM
 */ 
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Create_Shipping_Address extends Mage_Adminhtml_Block_Sales_Order_Create_Shipping_Address {

	public function getRetailStores(){
		return Mage::getModel('frans/retailStore')->getActiveStores();
	}

}