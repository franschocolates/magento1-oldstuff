<?php
/*
 * Quick renderer to cover the situations where an order is a multiship parent, 
 * in which case certain fields aren't applicable. This renderer is specifically
 * for dates, so we need to inherit from the date renderer
 * 
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Grid_Renderer_ConditionalMultishipDate extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Date
{
	public function render(Varien_Object $row)
	{	
		if ($row->getIsMultishipParent())
		{
			return "--";
		}
		return parent::render($row);
	}
}
?>