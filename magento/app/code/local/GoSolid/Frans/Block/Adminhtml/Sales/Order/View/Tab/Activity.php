<?php

class GoSolid_Frans_Block_Adminhtml_Sales_Order_View_Tab_Activity
    extends Mage_Adminhtml_Block_Widget_Grid
{

    /**
     * Set grid params
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('order_activity');
        $this->setUseAjax(true);
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('DESC');
    }

    public function getOrder()
    {
        return Mage::registry('current_order');
    }

    /**
     * Prepare collection for grid
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('frans/activity')->getCollection();
        $collection->addOrderToFilter($this->getOrder());
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Add columns to grid
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('created_at', array(
            'header'    => Mage::helper('sales')->__('Created At'),
            'index'     => 'created_at',
            'width'     => 1,
            'type'      => 'datetime',
            'align'     => 'center',
            'default'   => $this->__('N/A'),
            'html_decorators' => array('nobr')
        ));

        if ($this->getOrder()->getIsMultishipParent())
        {
            $this->addColumn('increment_id', array(
                'header'    => Mage::helper('frans')->__('Order ID'),
                'index'     => 'increment_id',
                'type'      => 'text',
                'align'     => 'center',
            ));
        }

        $this->addColumn('activity_type', array(
            'header'    => Mage::helper('sales')->__('Activity Type'),
            'index'     => 'activity_type',
            'type'      => 'text'
        ));

        $this->addColumn('created_by', array(
            'header'    => Mage::helper('sales')->__('Change By'),
            'index'     => 'created_by',
            'type'      => 'text'
        ));

        $this->addColumn('details', array(
            'header'    => Mage::helper('sales')->__('Change Details'),
            'index'     => 'details',
            'type'      => 'text',
            'renderer'  => 'GoSolid_Frans_Block_Adminhtml_Sales_Order_View_Tab_Activity_Grid_Renderer_ChangeDetails'
        ));

        return parent::_prepareColumns();
    }

    /**
     * Retrieve grid url
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/activity', array('_current' => true));
    }

    public function getRowUrl($item)
    {
        return '';
    }

}
