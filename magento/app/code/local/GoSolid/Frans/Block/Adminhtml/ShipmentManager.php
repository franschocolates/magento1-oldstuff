<?php

class GoSolid_Frans_Block_Adminhtml_ShipmentManager extends Mage_Adminhtml_Block_Sales_Order_Abstract
{
	private $_shipment;
	
	public function getNewUrl()
	{
		return $this->getUrl('*/*/new');
	}
	
	public function getOrderViewUrl()
	{
		$order = $this->getOrder();
        return $this->getUrl('adminhtml/sales_order/view', array('order_id'=>$order->getId()));
	}
	
	/***
	 * 
	 * returns Mage_Sales_Model_Shipment|bool
	 */
	public function getShipment()
	{
		if ($this->_shipment == null)
		{
			$shipments = $this->getOrder()->getShipmentsCollection();
		
			$this->_shipment = ($shipments->count() > 0) ? $shipments->getLastitem() : false;
		}
		
		return $this->_shipment;
	}
	
	public function getShipmentViewUrl()
	{
		if ($shipment = $this->getShipment())
		{
			return $this->getUrl('adminhtml/sales_order_shipment/view', array('shipment_id' => $shipment->getId()));
		}
		
		return false;
	}

	public function getRePrintHtml()
	{
		if ($order = $this->getOrder())
		{
			$formBlock = $this->getLayout()
				->createBlock('frans/adminhtml_shipmentManager_form')
				->setTemplate('shipmentManager/reprint.phtml')
				->setOrder($order);

			return $formBlock->toHtml();
		}
	}
}