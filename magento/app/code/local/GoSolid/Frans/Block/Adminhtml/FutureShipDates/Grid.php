<?php
class GoSolid_Frans_Block_Adminhtml_FutureShipDates_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{

		parent::__construct();
		$this->setId('fransGrid');

		$this->setDefaultSort('calendar_date');
		$this->setDefaultDir('DESC');

		//TODO: Optional
		//$this->setSaveParametersInSession(true);


        //default to showing the dates in the future.
        $from = date("Y-m-d");
        $locale = Mage::app()->getLocale()->getLocaleCode();
        $this->setDefaultFilter( array(
                'calendar_date' => array(
                    'from'=> date("Y-m-d 00:00:00"),
                    //'locale' => $locale,
                    'orig_from' => date("m/d/Y 00:00:00"),
                    'datetime' => true
                )
            )

        );
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getModel('frans/futureShipDate')->getCollection();
		$this->setCollection($collection);

        parent::_prepareCollection();


		return this;
	}




	protected function _prepareColumns()
  	{

  		$this->addColumn('id', array(
			  'header'    => Mage::helper('frans')->__('ID'),
			  'align'     =>'right',
			  'width'     => '50px',
			  'index'     => 'id',
		  ));

		$this->addColumn('calendar_date', array(
            'header'    => Mage::helper('frans')->__('Date'),
            'type'      => 'date',
            'align'     => 'left',
            'index'     => 'calendar_date',
            'filter'    => 'frans/widget_grid_column_filter_date',
        ));

		$this->addColumn('message_id',
		    array(
		        'header'=> Mage::helper('catalog')->__('Message'),
		        'index' => 'message_id',
		        'type'  => 'options',
		        'options' => Mage::getModel('frans/futureShipMessage')->getCollection()->toGridOptionArray('id', 'message_title')
		));

		$this->addColumn('availability_type',
		    array(
		        'header'=> Mage::helper('catalog')->__('Availability'),
		        'index' => 'availability_type',
		        'type'  => 'options',
		        'options' => Mage::getModel('frans/futureShipDate')->getAvailabilityTypeOptionArray()
		));

		return parent::_prepareColumns();
	}

	public function getRowUrl($row)
  	{
    	return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  	}





}
