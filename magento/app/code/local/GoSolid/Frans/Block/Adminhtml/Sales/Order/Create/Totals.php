<?php
/**
 * Created by goSolid.
 *
 * Rewritten so we can put in our gift card totals
 */ 
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Create_Totals
    extends Mage_Adminhtml_Block_Sales_Order_Create_Totals
{
    const GIFTCARD_CODE = 'giftcard';

    public function getTotals()
    {


        /* reinit the quote + save the quote after collecting totals */
        $quote =  $this->getQuote();
        $newQuote = Mage::getModel("sales/quote")->setStore(Mage::getSingleton("core/store")->load(1))->load($quote->getEntityId());
        $newQuote->collectTotals()->save();
        $totals =  $newQuote->getTotals();


        if ($giftcardAmount = $this->getQuote()->getGiftcardAmount())
        {
            // put gift card second
            $giftcardTotals = array(
                'code' => self::GIFTCARD_CODE
                , 'title' => 'Gift Card'
                , 'value' => - $giftcardAmount
                , 'area' => ''
            );
            $totalsObject = Mage::getModel('sales/quote_address_total')
                        ->setData($giftcardTotals);

            array_splice($totals, 1, 0, array(self::GIFTCARD_CODE => $totalsObject));

        }
        return $totals;
    }

    protected function _getTotalRenderer($code)
    {
        if ($code != self::GIFTCARD_CODE)
        {
            return parent::_getTotalRenderer($code);
        }

        return $this->getLayout()
                    ->createBlock('adminhtml/sales_order_create_totals_default')
                    ->setTotals($this->getTotals());
    }

    public function renderTotals($area = null, $colspan = 1)
    {
        $html = '';
        foreach($this->getTotals() as $total) {
            if ($total->getArea() != $area && $area != -1) {
                continue;
            }
            $html .= $this->renderTotal($total, $area, $colspan);
        }
        return $html;
    }

}