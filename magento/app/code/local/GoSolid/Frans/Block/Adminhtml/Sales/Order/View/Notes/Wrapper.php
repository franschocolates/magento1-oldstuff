<?php

class GoSolid_Frans_Block_Adminhtml_Sales_Order_View_Notes_Wrapper extends Mage_Adminhtml_Block_Template
{

    public function initItem(){
        $item = $this->getParentBlock()->getItem();
        $this->setItem($item);
        return $item;
    }

    public function getNotesHtml(){
        $notesBlock = $this->getChild('item_notes');
        $notesBlock->setItem($this->getItem());
        $notesBlock->setUseForm(true);
        return $notesBlock->toHtml();
    }

    public function getVerb($options){
        return empty($options) ? 'Add' : 'Edit';
    }

    public function getOptions(){

        $item = $this->getItem();
        $result = array();
            
        $options = array('order_type' => array('default' => 'SALE',
                                            'label' => "Order Type"),
                        'category' => array('default' => 'Regular Sale',
                                            'label' => "Category"),
                        'notes' => array('default' => false,
                                            'label' => "Notes"));
        
        foreach($options as $k => $v):
            if($item->getData($k) && $item->getData($k) !== $v['default']):
                $result[$v['label']] = $item->getData($k);
            endif;
        endforeach;

        return $result;
    }

}