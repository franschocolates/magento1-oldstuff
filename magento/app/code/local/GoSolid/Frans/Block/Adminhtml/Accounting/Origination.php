<?php

class GoSolid_Frans_Block_Adminhtml_Accounting_Origination extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
	$this->_controller = 'adminhtml_accounting_origination';
	$this->_blockGroup = 'frans';
	$this->_headerText = Mage::helper('frans')->__("Originations");
	$this->_addButtonLabel = Mage::helper('frans')->__("Add Origination");

    parent::__construct();

  }
}