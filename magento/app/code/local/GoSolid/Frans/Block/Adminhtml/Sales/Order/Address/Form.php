<?php
/**
 * Rewritten to allow editing of Residential/Commercial status
 */ 
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Address_Form
    extends Mage_Adminhtml_Block_Sales_Order_Address_Form
{
    /**
     * Define form attributes (id, method, action)
     * Rewritten so we can add an address type to the field (residential, commercial, etc)
     *
     * @return Mage_Adminhtml_Block_Sales_Order_Address_Form
     */
    /* Perform a check before the form loads to see if validation is enabled
       or not. Otherwise, we return a singleton message letting the user know
       that validation will not be performed. */
    protected function _construct(){
        if(!Mage::getStoreConfig('carriers/fedex/validation', Mage::app()->getStore())){
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('frans')->__("Address validation is disabled."));
        }

        $this->setTemplate('sales/order/address/form.phtml');
        parent::_construct();

    }
    protected function _prepareForm()
    {
        parent::_prepareForm();
        // through inspecting code, we know that the main fieldset is called main
        // and we want our code to be in there for the field to line up with everything else.
        $fieldset = $this->_form->getElement('main');
        $deliveryTypes = array(
            GoSolid_Frans_Model_Sales_Quote_Address::UNKNOWN_ADDRESS_TYPE => $this->__('Unknown')
            , GoSolid_Frans_Model_Sales_Quote_Address::RESIDENTIAL_ADDRESS_TYPE => $this->__('Residential')
            , GoSolid_Frans_Model_Sales_Quote_Address::COMMERCIAL_ADDRESS_TYPE => $this->__('Commercial')
        );
        $fieldset->addField('address_delivery_type', 'select',
            array(
                'name'  => 'address_delivery_type',
                'label' => Mage::helper('frans')->__('Address Type'),
                'id'    => 'address_delivery_type',
                'class' => 'required-entry',
                'required' => true,
                // for some reason this doesn't come through in getFormValues, so specify it here
                'value' => $this->_getAddress()->getAddressDeliveryType(),
                'values' => $deliveryTypes
            )
        );
        return $this;
    }

}