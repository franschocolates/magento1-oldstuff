<?php

/**
 * Order grid for capturing; contains a subset or orders based on those that are capturable
 *
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Accounting_Grid
    extends Mage_Adminhtml_Block_Sales_Order_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('sales_order_accounting_grid');
        $this->setUseAjax(true);
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(false);
    }

    protected function _prepareCollection()
    {
        // we have to use sales/order and not order_grid
        // because we want some columns that aren't on grid.
        /** @var GoSolid_Frans_Model_Resource_Sales_Order_Collection $collection */
        $collection = Mage::getModel('sales/order')->getCollection()
            ->addPaymentMethodJoin();

        $this->setCollection($collection);

        return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        // don't call parent, it adds columns we don't want

        $this->addColumn('entity_id',
            array(
                'header' => Mage::helper('frans')->__('Order ID'),
                'index' => 'entity_id',
                'column_css_class'=>'no-display',//this sets a css class to the column row item
                'header_css_class'=>'no-display',//this sets a css class to the column header
            ));

        $this->addColumn('real_order_id', array(
            'header'=> Mage::helper('sales')->__('Order #'),
            'width' => '100px',
            'type'  => 'text',
            'index' => 'increment_id',
        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                'header'    => Mage::helper('sales')->__('Purchased From (Store)'),
                'index'     => 'store_id',
                'type'      => 'store',
                'store_view'=> true,
                'display_deleted' => true,
            ));
        }

        $this->addColumnAfter('accounting_label', array(
            'header' => Mage::helper('frans')->__('Label'),
            'width'  => '80px',
            'type'   => 'options',
            'options' => Mage::getModel('frans/accounting_label')->getCollection()->toGridOptionArray('label_name', 'label_name'),
            'index'  => 'accounting_label',
        ), 'real_order_id');

        // note that this isn't really the subtotal
        // because magento doesn't store it with the discount
        $this->addColumnAfter('base_subtotal', array(
            'header' => Mage::helper('sales')->__('Subtotal'),
            'type'  => 'currency',
            'currency' => 'base_currency_code',
            'renderer' => 'frans/adminhtml_sales_order_grid_renderer_subtotalWithDiscount'
        ), 'accounting_label');

        $this->addColumnAfter('base_discount_amount', array(
            'header' => Mage::helper('sales')->__('Discount'),
            'index' => 'base_discount_amount',
            'type'  => 'currency',
            'currency' => 'base_currency_code',
            'show_number_sign' => false
        ), 'base_subtotal');

        $this->addColumnAfter('base_tax_amount', array(
            'header' => Mage::helper('sales')->__('Tax'),
            'index' => 'base_tax_amount',
            'type'  => 'currency',
            'currency' => 'base_currency_code',
        ), 'base_discount_amount');

        $this->addColumnAfter('base_shipping_amount', array(
            'header' => Mage::helper('frans')->__('Shipping Amount'),
            'index'  => 'base_shipping_amount',
            'type'   => 'currency',
            'currency' => 'order_currency_code',
        ), 'base_tax_amount');

        $this->addColumn('base_grand_total', array(
            'header' => Mage::helper('sales')->__('Grand Total'),
            'index' => 'base_grand_total',
            'type'  => 'currency',
            'currency' => 'base_currency_code',
        ));

        $payment_method_keys = array_keys(Mage::getModel('payment/config')->getActiveMethods());
        $payment_method_paths = array_map(function($key){ return "payment/" . $key . "/title"; }, $payment_method_keys);
        $payment_method_titles = Mage::getModel('core/config_data')->getCollection()
                            ->addFieldToSelect('value', 'name')
                            ->addExpressionFieldToSelect('id', "REPLACE(REPLACE(path, 'payment/', ''), '/title', '')", [])
                            ->addFieldToFilter('path', array('in' => $payment_method_paths))
                            ->toOptionHash();

        $this->addColumn('method', array(
            'header' => Mage::helper('frans')->__('Payment Method'),
            'index' => 'sfop.method',
            'type'   => 'text',
            'renderer'   => 'frans/adminhtml_widget_grid_column_renderer_customerNumber',
            'width'  => '80px',
            'filter_index' => 'sfop.method',
            'options' => $payment_method_titles
        ));

        $yesNoOptions = array( 0 => 'No', 1 => 'Yes');

        $this->addColumnAfter('giftcard_amount', array(
            'header' => Mage::helper('frans')->__('Gift Card'),
            'index'  => 'giftcard_amount',
            'type'  => 'options',
            'renderer'   => 'frans/adminhtml_widget_grid_column_renderer_hasValue',
            'has_value_label' => 'Has Gift Card',
            'filter_condition_callback' => array($this, '_hasValueFilter'),
            'options' => $yesNoOptions,
        ), 'payment_method');


        $this->addColumn('status', array(
            'header' => Mage::helper('sales')->__('Status'),
            'index' => 'status',
            'type'  => 'options',
            'width' => '70px',
            'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
        ));

        $this->addColumn('created_at', array(
            'header' => Mage::helper('sales')->__('Purchased On'),
            'index' => 'created_at',
            'type' => 'datetime',
            'width' => '100px',
        ), 'status');

        $this->addColumnAfter('planned_ship_date', array(
            'header' => Mage::helper('frans')->__('Planned Ship Date'),
            'index'  => 'planned_ship_date',
            'type'   => 'date',
            'width'  => '100px',
            'format' => 'M/d/Y',  // this is Zend formatting, not PHP formatting
        ), 'created_at');

        $this->addColumnAfter('ship_date', array(
            'header' => Mage::helper('frans')->__('Ship Date'),
            'index'  => 'ship_date',
            'type'   => 'date',
            'width'  => '100px',
            'format' => 'M/d/Y',  // this is Zend formatting, not PHP formatting
        ), 'planned_ship_date');

        $this->addColumnAfter('has_shipped', array(
            'header' => Mage::helper('frans')->__('Shipped'),
            'index'  => 'ship_date',
            'type'  => 'options',
            'renderer'   => 'frans/adminhtml_widget_grid_column_renderer_hasValue',
            'options' => $yesNoOptions,
            'has_value_label' => 'Shipped',
            'filter_condition_callback' => array($this, '_hasValueFilter'),
        ), 'ship_date');

        $this->addColumn('captured_at', array(
            'header' => Mage::helper('frans')->__('Captured On'),
            'index' => 'captured_at',
            'type' => 'datetime',
            'width' => '100px',
        ));

        $this->addColumnAfter('is_captured', array(
            'header' => Mage::helper('frans')->__('Captured'),
            'index'  => 'captured_at',
            'type'  => 'options',
            'renderer'   => 'frans/adminhtml_widget_grid_column_renderer_hasValue',
            'options' => $yesNoOptions,
            'has_value_label' => 'Captured',
            'filter_condition_callback' => array($this, '_hasValueFilter'),
        ), 'captured_at');

        // had to copy in the below from the parent order
        // so that we still get our export functionality
        $this->addRssList('rss/order/new', Mage::helper('sales')->__('New Order RSS'));

        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel XML'));

        // skip parent, go to grandparent
        return Mage_Adminhtml_Block_Widget_Grid::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('order_ids');
        $this->getMassactionBlock()->setUseSelectAll(true);

        // // this probably needs to be updated
        // $this->getMassactionBlock()->addItem('capture_order', array(
        //     'label' => Mage::helper('frans')->__('Capture'),
        //     'url'   => $this->getUrl('*/sales_order/massCapture', array('origin' => 'capture_grid'))
        // ));

        $this->getMassactionBlock()->addItem('receipt_picktickets', array(
            'label' => Mage::helper('sales')->__('Print Picking Tickets'),
            'url' => $this->getUrl('*/sales_order/massPrintPickingTickets')
        ));

        $this->getMassactionBlock()->addItem('receipt_packingslips', array(
            'label' => Mage::helper('sales')->__('Print Packing Slips'),
            'url' => $this->getUrl('*/sales_order/massPrintPackingSlips')
        ));

        $this->getMassactionBlock()->addItem('receipt_giftmessages', array(
            'label' => Mage::helper('sales')->__('Print Gift Messages'),
            'url' => $this->getUrl('*/sales_order/massPrintGiftMessages')
        ));

        $this->getMassactionBlock()->addItem('receipt_all', array(
            'label' => Mage::helper('sales')->__('Print All Receipts'),
            'url' => $this->getUrl('*/sales_order/massPrintAllReceipts')
        ));

        $this->getMassactionBlock()->addItem('mark_ready_for_capture', array(
            'label' => Mage::helper('sales')->__('Mark as Ready for Capture'),
            'url' => $this->getUrl('*/sales_order/massMarkReadyForCapture')
        ));

        return $this;
    }

    /**
     * Suppress RSS Lists
     */
    public function getRssLists()
    {
        return false;
    }

    public function getRowUrl($row)
    {
        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view')) {
            return $this->getUrl('adminhtml/sales_order/view', array('order_id' => $row->getId()));
        }
        return false;
    }

    protected function _hasValueFilter($collection, $column)
    {
        $filterValue = $column->getFilter()->getValue();

        if($filterValue == 1)
        {
            // depends on which column we are handling
            // if ship date, just make sure not null
            if ($column->getIndex() == 'ship_date' || $column->getIndex() == 'captured_at')
            {
                $collection->addFieldToFilter($column->getIndex(),array('notnull' => true));
            }
            else
            {
                $collection->addFieldToFilter($column->getIndex(),array('gt' => 0));
            }
        }
        else if($filterValue == 0)
        {
            $collection->addFieldToFilter($column->getIndex(),array('null' => 'true' ));
        }

        return $this;
    }

}
