<?php
/*
 * This renderer was inherited from engine, but moved into GoSolid/Frans
 * in order to keep all Admin Grid stuff together.
 * 
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Grid_Renderer_GiftMessage extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	public function render(Varien_Object $row)
	{
		if ($row->getGiftMessageId() != 0 && $row->getGiftMessageId() != null)
		{
			return 'X';
		}
		return '';
	}
}
?>