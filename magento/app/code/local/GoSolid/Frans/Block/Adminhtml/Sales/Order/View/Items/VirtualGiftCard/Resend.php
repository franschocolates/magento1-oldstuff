<?php
/**
 * Block to possibly allow possible resending of virtual gift cards
 *
 * @method Mage_Sales_Model_Order_Item getItem()
 * @method GoSolid_Frans_Block_Adminhtml_Sales_Order_View_Items_VirtualGiftCard_Resend setItem(Mage_Sales_Model_Order_Item $value)
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_View_Items_VirtualGiftCard_Resend
        extends Mage_Adminhtml_Block_Widget_Form
{
    public function _beforeToHtml()
    {
        $this->initItem();

        return parent::_beforeToHtml();
    }

    public function initItem(){
        $item = $this->getParentBlock()->getItem();
        $this->setItem($item);
        return $item;
    }

    public function isVirtualGiftCard()
    {
        return $this->getItem()->getProductType() == 'virtualgiftcard';
    }

    public function getSendButtonHtml()
    {
        $itemId = $this->getItem()->getId();
        return $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setData(array(
                'label'   => Mage::helper('sales')->__('Send'),
                'class'   => 'save',
            'onclick'   => "virtualGiftCardResend.sendEmail($itemId);"
            ))
            ->toHtml();
    }

    public function getCancelButtonHtml()
    {
        $itemId = $this->getItem()->getId();
        return $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setData(array(
                'label'   => Mage::helper('sales')->__('Cancel'),
                'class'   => 'cancel',
                'onclick'   => "virtualGiftCardResend.toggleForm($itemId);"
            ))
            ->toHtml();
    }

    protected function _prepareForm(){

        $item = $this->getItem();
        $itemId = $item->getId();
        $form = new Varien_Data_Form(array(
            'id'        => 'vgc_resend_form_' . $itemId,
            'action'    => $this->getUrl('*/*/resendVirtualGiftCard'),
            'method'    => 'post',
        ));

        $form->setHtmlIdPrefix('vgc_resend_' . $itemId . '_');

        $form->addField('emails', 'textarea', array(
            'name'      => "vgc_resend[$itemId][emails]",
            'label'     => 'Email Addresses',
            'title'     => Mage::helper('frans')->__('Email Addresses'),
            'class'     => 'input-textarea resend-email',
            'after_element_html' => "<small class='resend-note'>{$this->__('Send to multiple by entering an address on each line (no limit).')}</small>"
        ));

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();

    }

}