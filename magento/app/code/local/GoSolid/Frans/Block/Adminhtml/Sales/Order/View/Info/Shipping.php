<?php
/**
 * Created by goSolid.
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_View_Info_Shipping
        extends Mage_Adminhtml_Block_Sales_Order_Abstract
{
    public function getAddressValidationFormHtml()
    {
        $shippingAddress = $this->getOrder()->getShippingAddress();
        return $this->getLayout()
            ->createBlock('frans/adminhtml_sales_order_address_validation_view')
            ->setTemplate('sales/order/address/validation/view.phtml')
            ->setAddress($shippingAddress)
            ->toHtml();
    }

    public function getUpdateAddressUrl()
    {
        return $this->getUrl('*/*/updateShippingAddress', array('order_id' => $this->getOrder()->getId(), 'address_id' => $this->getOrder()->getShippingAddress()->getId()));
    }

}