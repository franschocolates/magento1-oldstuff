<?php

class GoSolid_Frans_Block_Adminhtml_Productpiece_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

	protected function _prepareForm()
	{

		$form = new Varien_Data_Form(); 
		$this->setForm($form);
		$fieldset = $form->addFieldset('frans_fs', array('legend'=>Mage::helper('frans')->__('Product Piece')));

		// Piece Code
		$fieldset->addField('piece_code', 'text', array(
	          'title'     => Mage::helper('frans')->__('Piece Code'),
			  'label'     => Mage::helper('frans')->__('Piece Code'),
	          'name'      => 'piece_code',
			  'class'     => 'required-entry validate-code',
	          'required'  => true,
			  'note'	  => 'Unique code for identifying this piece'
	      	));

		// Piece label
		$fieldset->addField('piece_label', 'text', array(
	          'title'     => Mage::helper('frans')->__('Piece Label'),
			  'label'     => Mage::helper('frans')->__('Piece Label'),
	          'name'      => 'piece_label',
			  'class'     => 'required-entry',
	          'required'  => true,
			  'note'	  => 'Name of the piece displayed to customers'
	      	));

		// Piece label
		$fieldset->addField('piece_description', 'textarea', array(
			  'title'     => Mage::helper('frans')->__('Piece Description'),
			  'label'     => Mage::helper('frans')->__('Piece Description'),
			  'name'      => 'piece_description',
			  'style'  => 'height: 4.8em;',
			  'note'	  => 'Description of the piece displayed to customers'
	      	));
		
		// Piece image
		$fieldset->addField('piece_image', 'image', array(
			'title'     => Mage::helper('frans')->__('Piece Image'),
			'label'     => Mage::helper('frans')->__('Piece Image'),
			'name'      => 'piece_image',
			'note'      => 'Image dimensions should be 722x100'
	    ));

		// Status
		$fieldset->addField('is_active', 'select',array(
			'label'     => Mage::helper('frans')->__('Status'),
			'name'      => 'is_active',
			'options'   => array(
				1 => 'Enabled',
				0 => 'Disabled'
			)
		));
	
		if ( Mage::getSingleton('adminhtml/session')->getIndexData() )
		{
	    	$form->setValues(Mage::getSingleton('adminhtml/session')->getIndexData());
	    	Mage::getSingleton('adminhtml/session')->getIndexData(null);
		}
		elseif(Mage::registry('index_data'))
		{
	    	$form->setValues(Mage::registry('index_data')->getData());
		}
		return parent::_prepareForm();
	}
  
}