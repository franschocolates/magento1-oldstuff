<?php
/*
 * Quick renderer to cover the situations where an order is a multiship parent, 
 * in which case certain fields aren't applicable. This applies to multiple fields;
 * for now, we have the same display for all of them, eventually we could need to change
 * it based on the field
 * 
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Grid_Renderer_ConditionalMultishipText extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	public function render(Varien_Object $row)
	{	
		if ($row->getIsMultishipParent())
		{
			return "--";
		}
		return parent::render($row);
	}
}
?>