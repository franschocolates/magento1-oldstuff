<?php
class GoSolid_Frans_Block_Adminhtml_Quotesgrid_Renderer_CustomerName extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        return $this->_getValue($row);
    }
    protected function _getValue(Varien_Object $row)
    {
        $value = $row->getData( "customer_firstname" ) . ' ' . $row->getData( "customer_lastname" );

        return $value;
    }
}