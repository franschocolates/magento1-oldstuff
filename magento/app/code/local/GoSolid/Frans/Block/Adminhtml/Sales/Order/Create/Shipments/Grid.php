<?php
/**
 * Encapsulates logic for building the grid of shipments
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Create_Shipments_Grid
    extends Mage_Adminhtml_Block_Sales_Order_Create_Abstract
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('sales_order_create_shipments_grid');
    }

    public function getShipmentAddressHtml($address)
    {
        return $this->getLayout()
                ->createBlock('frans/adminhtml_sales_order_create_shipments_grid_address')
                ->setAddress($address)
                ->setEditMode($this->_addressIsBeingEdited($address))
                ->setCanEdit(!$this->_anyAddressBeingEdited()) // can only edit if nothing else being edited.
                ->toHtml();
    }

    public function canAddAddress()
    {
        $allAddresses = $this->getQuote()->getAllShippingAddresses();

        $noMethod = array_filter($allAddresses, function ($add) { return !$add->getShippingMethod(); });

        return count($noMethod) == 0;
    }

    public function getAddAddressButtonHtml()
    {
        if ($this->canAddAddress())
        {
            return $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'   => Mage::helper('frans')->__('Add Shipment'),
                    'class'   => 'add',
                    'onclick' => 'order.addShipment()',
                    'id'    => 'add_shipment_button'
                ))
                ->toHtml();
        }

        return '';
    }

    protected function _anyAddressBeingEdited()
    {
        $addressesWithNoNames = array_filter($this->getQuote()->getAllShippingAddresses(), function ($a) { return !$a->getFirstname();});
        return count($addressesWithNoNames) > 0 || Mage::app()->getRequest()->getParam('edit_address_id');
    }

    /**
     * @param GoSolid_Frans_Model_Sales_Quote_Address $address
     * @return bool
     */
    protected function _addressIsBeingEdited($address)
    {
        // kind of hacky to know it this way, but not sure of a better way to do it
        if ($address->getId())
        {
            // either we aren't filled out (for instance, no default address
            // or we are explicitly editing this address ID via a request params
            $editing = !$address->getFirstname() || count($address->getAllItems()) == 0;
            return $editing || ($address->getId() == Mage::app()->getRequest()->getParam('edit_address_id', -1));
        }

        // if no ID, then it's definitely being edited.
        return true;
    }
}