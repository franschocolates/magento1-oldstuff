<?php

class GoSolid_Frans_Block_Adminhtml_FutureShipMessages_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

	protected function _prepareForm()
	{

		$form = new Varien_Data_Form();
		$this->setForm($form);
		$fieldset = $form->addFieldset('frans_fs', array('legend'=>Mage::helper('frans')->__('Message Fields')));

		$fieldset->addField('message_title', 'text', array(
	          'title'     => Mage::helper('frans')->__('Message Title'),
			  'label'     => Mage::helper('frans')->__('Message Title'),
	          'name'      => 'message_title',
			  'class'     => 'required-entry',
	          'required'  => true,
			  'note'	  => 'Title that appears when date is selected',
	      	));
		
		$fieldset->addField('image', 'image', array(
	          'title'     => Mage::helper('frans')->__('Image'),
			  'label'     => Mage::helper('frans')->__('Image'),
	          'name'      => 'image',
			  'note'      => 'Image must be 134px width X 60px height'
	    ));  
	    
	    $fieldset->addField('message_text', 'text', array(
	          'title'     => Mage::helper('frans')->__('Message Text'),
			  'label'     => Mage::helper('frans')->__('Message Text'),
	          'name'      => 'message_text',
	          'required'  => false,
			  'note'	  => 'Extra message to display; leave blank to not display an extra message. Usually used on available dates.',
	    ));

	    if ( Mage::getSingleton('adminhtml/session')->getFutureShipMessagesData() )
		{
	    	$form->setValues(Mage::getSingleton('adminhtml/session')->getFutureShipMessagesData());
	    	Mage::getSingleton('adminhtml/session')->getFutureShipMessagesData(null);
		}
		elseif(Mage::registry('futureShipMessages_data'))
		{
	    	$form->setValues(Mage::registry('futureShipMessages_data')->getData());
		}

		return parent::_prepareForm();
	}

}