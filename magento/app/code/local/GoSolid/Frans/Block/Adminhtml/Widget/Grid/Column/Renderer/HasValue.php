<?php

class GoSolid_Frans_Block_Adminhtml_Widget_Grid_Column_Renderer_HasValue
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    protected function _getValue(Varien_Object $row)
    {
        $data = parent::_getValue($row);
        if (!is_null($data) && strlen($data) > 0)
        {
            return $this->getColumn()->getHasValueLabel();
        }

        return 'No';
    }
}
