<?php

class GoSolid_Frans_Block_Adminhtml_Reports_Accpac extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array('id' => 'edit_form',
                        'action' => Mage::getSingleton('core/url')->parseUrl($this->getUrl('*/*/generate'))->getPath(),
                        'method' => 'post'));

        $fieldset = $form->addFieldset('main', array());
        $fieldset->setLegend($this->__('AccPac Report'));

        $dateFormatIso = Mage::app()->getLocale()->getDateFormat(
            Mage_Core_Model_Locale::FORMAT_TYPE_SHORT
        );

        $fieldset->addField('date', 'date',
            array(
                'name'      => 'date',
                'label'     => Mage::helper('frans')->__('Report Date'),
                'id'        => 'date',
                'required'  => true,
                'value'     => time(),
                'format'    => $dateFormatIso,
                'image'     => $this->getSkinUrl('images/grid-cal.gif')
            )
        );

        $fieldset->addField('submit', 'submit',
            array(
                'name'      => 'submit',
                'id'        => 'submit',
                'value'     => $this->__('Submit')
            )
        );

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

}