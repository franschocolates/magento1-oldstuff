<?php
class GoSolid_Frans_Block_Adminhtml_FutureShipMessages_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{

		parent::__construct();
		$this->setId('fransGrid');

		$this->setDefaultSort('id');
		$this->setDefaultDir('DESC');

		//TODO: Optional
		//$this->setSaveParametersInSession(true);
		//$this->setDefaultFilter(array('status' => 'Enabled'));
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getModel('frans/futureShipMessage')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}


	protected function _prepareColumns()
  	{

  		$this->addColumn('id', array(
			  'header'    => Mage::helper('frans')->__('ID'),
			  'align'     =>'right',
			  'width'     => '50px',
			  'index'     => 'id',
		  ));

		  $this->addColumn('message_title', array(
			  'header'    => Mage::helper('frans')->__('Message Title'),
			  'align'     =>'left',
			  'index'     => 'message_title',
		  ));

		  $this->addColumn('message_text', array(
			  'header'    => Mage::helper('frans')->__('Message Text'),
			  'align'     =>'left',
			  'index'     => 'message_text',
		  ));

        $this->addColumn('message_image', array(
            'header'    => Mage::helper('frans')->__('Message Image'),
            'type'      => 'image',
            'align' => 'center',
            'index'     => 'image',
            'escape'    => true,
            'sortable'  => false,
            'filter'    => false,
            'renderer'  => 'frans/adminhtml_futureShipMessages_grid_renderer_image',
        ));


		  return parent::_prepareColumns();
	}

	public function getRowUrl($row)
  	{
    	return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  	}
}