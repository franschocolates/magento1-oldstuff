<?php

class GoSolid_Frans_Block_Adminhtml_Article_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

	protected function _prepareForm()
	{

		$form = new Varien_Data_Form(); 
		$this->setForm($form);
		$fieldset = $form->addFieldset('frans_fs', array('legend'=>Mage::helper('frans')->__('Article')));
	
	    $fieldset->addField('title', 'text', array(
			'title'     => Mage::helper('frans')->__('Title'),
			'label'     => Mage::helper('frans')->__('Title'),
			'class'     => 'required-entry',
			'required'  => true,
	     	'name'      => 'title'
		));
		
		$fieldset->addField('sub_title', 'text', array(
			'title'     => Mage::helper('frans')->__('Sub Title'),
			'label'     => Mage::helper('frans')->__('Sub Title'),
			'class'     => '',
			'required'  => false,
	     	'name'      => 'sub_title'
		));
		
		$fieldset->addField('date', 'date', array(
	          'title'     => Mage::helper('frans')->__('Date'),
			  'label'     => Mage::helper('frans')->__('Date'),
	          'name'      => 'date',
				'image'  => $this->getSkinUrl('images/grid-cal.gif'),
			  'class'     => 'required-entry date',
			  'format'	  => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT) ,
	          'required'  => true,
			  'note'	  => 'Only the month and year are used for display on the front end.',
	      	));
		
		$fieldset->addField('description', 'textarea', array(
			'title'     => Mage::helper('frans')->__('Description'),
			'label'     => Mage::helper('frans')->__('Description'),
			'class'     => '',
			'required'  => false,
	     	'name'      => 'description'
		));
		
		//This is just encase we ever add differnt types.
		$fieldset->addField('type_id', 'hidden', array(
			'title'     => Mage::helper('frans')->__('Description'),
			'label'     => Mage::helper('frans')->__('Description'),
	     	'name'      => 'type_id',
			'value'     => GoSolid_Frans_Model_Article::ARTICLE_TYPE_NEWS
		));
		
		$fieldset->addField('url', 'text', array(
			'title'     => Mage::helper('frans')->__('URL'),
			'label'     => Mage::helper('frans')->__('URL'),
			'class'     => 'validate-url',
			'required'  => false,
	     	'name'      => 'url',
		    'note'	    => 'This will only show if there is a URL entered and make sure start the url with "http:".',
		));
		
		$fieldset->addField('status', 'select',array(
			'label'     => Mage::helper('frans')->__('Status'),
			'required'  => true,
			'name'      => 'status',
			'options'   => array(
					  		'1' => 'Enabled',
					  		'0' => 'Disabled',
				  ),
		));
		
		
		if ( Mage::getSingleton('adminhtml/session')->getArticleData() )
		{
	    	$form->setValues(Mage::getSingleton('adminhtml/session')->getArticleData());
	    	Mage::getSingleton('adminhtml/session')->getArticleData(null);
		}
		elseif(Mage::registry('article_data'))
		{
	    	$form->setValues(Mage::registry('article_data')->getData());
		}
		
		return parent::_prepareForm();
	}
  
}