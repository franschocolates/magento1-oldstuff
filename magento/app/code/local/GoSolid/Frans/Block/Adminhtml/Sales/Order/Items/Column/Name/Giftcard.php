<?php
/**
 * Renderer to include the giftcard number when rendering giftcards
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Items_Column_Name_Giftcard
        extends Mage_Adminhtml_Block_Sales_Items_Column_Name
{
    public function getOrderOptions()
    {
        $result = parent::getOrderOptions();

        // append the giftcard number if thre is one
        if (is_array($result))
        {
            Mage::log("The options are: " . print_r($result, true));
            // try and get the order item
            if ($orderItem = $this->getItem())
            {
                // try and load by the item ID
                if ($giftcard = Mage::getModel('givex/giftcard')->loadByOrderItemId($orderItem->getId()))
                {
                    if ($giftcard->getId())
                    {
                        $cardNumber = $giftcard->getCardNumber();
                        // need to append something like this:
//                        [4] => Array
//                        (
//                            [label] => gc_sender_email
//                            [value] => barrett+sender@gosolid.net
//                            [print_value] => barrett+sender@gosolid.net
//                            [option_type] => field
//                            [option_value] => barrett+sender@gosolid.net
//                        )

                        $result[] = array(
                            'label' => $this->__('Gift card number')
                            , 'value' => $cardNumber
                            , 'print_value' => $cardNumber
                            , 'option_type' => 'field'
                            , 'option_value' => $cardNumber
                        );
                    }
                }
            }
        }

        return $result;
    }


}