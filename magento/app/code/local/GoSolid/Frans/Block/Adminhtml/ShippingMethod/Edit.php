<?php

class GoSolid_Frans_Block_Adminhtml_ShippingMethod_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'frans';
        $this->_controller = 'adminhtml_shippingMethod';
        
        // Optional
        //$this->_updateButton('save', 'label', Mage::helper('event')->__('Save'));
		//$this->_removeButton('delete');
		//$this->_removeButton('reset');
		
		$this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('frans')->__('Save and Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);
		
        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
			
        ";
    }

	public function getHeaderText()
    {  
        if( Mage::registry('shippingMethod_data') && Mage::registry('shippingMethod_data')->getId() ) {
            return Mage::helper('frans')->__("Edit Shipping Method");
		} else {
            return Mage::helper('frans')->__('Add Shipping Method');
        }
    }

}