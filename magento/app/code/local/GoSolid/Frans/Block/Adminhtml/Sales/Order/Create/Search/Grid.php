<?php
/**
 * Rewrite to allow use to customize order and auto-expansion
 */ 
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Create_Search_Grid
    extends Mage_Adminhtml_Block_Sales_Order_Create_Search_Grid
{
    // order ascending, since most popular products are early on
    protected $_defaultDir      = 'asc';


    /**
     * Prepare collection to be displayed in the grid
     *
     * @return Mage_Adminhtml_Block_Sales_Order_Create_Search_Grid
     */
    protected function _prepareCollection()
    {
        $adminOrderCreateProductTypes = Mage::getConfig()->getNode('adminhtml/sales/order/create/available_product_types')->asArray();

        if( isset($adminOrderCreateProductTypes['configurable']) == true )
        {
            unset($adminOrderCreateProductTypes['configurable']);
        }

        $attributes = Mage::getSingleton('catalog/config')->getProductAttributes();
        /* @var $collection Mage_Catalog_Model_Resource_Product_Collection */
        $collection = Mage::getModel('catalog/product')->getCollection();
        $collection
            ->setStore($this->getStore())
            ->addAttributeToSelect($attributes)
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('color_tile')
            ->addStoreFilter()
            ->addAttributeToFilter('type_id', array_keys($adminOrderCreateProductTypes))
            ->addFieldToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
            ->addAttributeToSelect('gift_message_available');

        Mage::getSingleton('catalog/product_status')->addSaleableFilterToCollection($collection);

        //Join the color tiles label the product collection
        $collection->getSelect()->join(array('at_color_tile' => 'catalog_product_entity_int'),
           'e.entity_id = at_color_tile.entity_id and at_color_tile.attribute_id = (SELECT attribute_id FROM `eav_attribute` WHERE attribute_code = "color_tile")',
            array('color_tile' => 'at_color_tile.value'))
            ->join(array('ct' => 'frans_color_tile'),
                '`at_color_tile`.`value` = ct.id',
                array('color_tile_title'=>'ct.title'));

        //echo $collection->getSelect();

        $this->setCollection($collection);
        return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
    }

    /**
     * Prepare columns
     *
     * @return Mage_Adminhtml_Block_Sales_Order_Create_Search_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('entity_id', array(
            'header'    => Mage::helper('sales')->__('ID'),
            'sortable'  => true,
            'width'     => '60',
            'index'     => 'entity_id'
        ));
        $this->addColumn('name', array(
            'header'    => Mage::helper('sales')->__('Product Name'),
            'renderer'  => 'adminhtml/sales_order_create_search_grid_renderer_product',
            'index'     => 'name'
        ));

        $this->addColumn('color_tile_title', array(
            'header'    => Mage::helper('sales')->__('Color tile'),
            'width'     => '165',
            'index'     => 'color_tile_title',
            'filter_condition_callback' => array($this, '_colortileFilter'),
        ));

        $this->addColumn('accpac_sku', array(
            'header'    => Mage::helper('sales')->__('SKU'),
            'width'     => '80',
            'index'     => 'accpac_sku'
        ));
        $this->addColumn('price', array(
            'header'    => Mage::helper('sales')->__('Price'),
            'column_css_class' => 'price',
            'align'     => 'center',
            'type'      => 'currency',
            'currency_code' => $this->getStore()->getCurrentCurrencyCode(),
            'rate'      => $this->getStore()->getBaseCurrency()->getRate($this->getStore()->getCurrentCurrencyCode()),
            'index'     => 'price',
            'renderer'  => 'adminhtml/sales_order_create_search_grid_renderer_price',
        ));

        $this->addColumn('in_products', array(
            'header'    => Mage::helper('sales')->__('Select'),
            'header_css_class' => 'a-center',
            'type'      => 'checkbox',
            'name'      => 'in_products',
            'values'    => $this->_getSelectedProducts(),
            'align'     => 'center',
            'index'     => 'entity_id',
            'sortable'  => false,
        ));

        $this->addColumn('qty', array(
            'filter'    => false,
            'sortable'  => false,
            'header'    => Mage::helper('sales')->__('Qty To Add'),
            'renderer'  => 'adminhtml/sales_order_create_search_grid_renderer_qty',
            'name'      => 'qty',
            'inline_css'=> 'qty',
            'align'     => 'center',
            'type'      => 'input',
            'validate_class' => 'validate-number',
            'index'     => 'qty',
            'width'     => '1',
        ));

        return Mage_Adminhtml_Block_Widget_Grid::_prepareColumns();
    }

    protected function _colortileFilter($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }

        $collection->getSelect()->where(
            "ct.title like ?"
            , "%$value%");

        return $this;
    }
}