<?php
/**
 * Allows us to use different totals when showing totals for pickup
 * So that retail store employees see how much needs to be collected in-store
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_PickUpTotals
    extends GoSolid_Frans_Block_Adminhtml_Sales_Order_Totals
{
    protected function _initTotals()
    {
        parent::_initTotals();

        // our payment method dictates what we do
        // unless our payment method is pickup at store, then
        // everything is "paid" since they don't need to collect payment at pickup
        /** @var GoSolid_Frans_Model_Sales_Order $order */
        $order = $this->getOrder();

        $payment = $this->getOrder()->getPayment();

        // only valid if not multiship child (where there are no totals)
        // and not yet captured
        if ($payment->getMethod() != GoSolid_Frans_Model_Payment_Method_PayAtPickup::CODE
            && !$order->getIsMultishipChildOrder()
            && !$order->hasCapturedAt()
        )
        {
            // adjust the totals so it looks like it is paid.
            $paidTotals = $this->_totals['paid'];
            $dueTotals = $this->_totals['due'];
            $paidTotals->setValue($dueTotals->getValue());
            $paidTotals->setBaseValue($dueTotals->getBaseValue());

            $dueTotals->setValue(0);
            $dueTotals->setBaseValue(0);
        }

        return $this;
    }

}