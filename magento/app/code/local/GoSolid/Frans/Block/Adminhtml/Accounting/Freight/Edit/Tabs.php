<?php

class GoSolid_Frans_Block_Adminhtml_Accounting_Freight_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{  
	public function __construct()
  	{
		$this->setId('frans_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('frans')->__('Accounting Freight'));
		parent::__construct();
  	}
	

	protected function _beforeToHtml()
  	{

	 $this->addTab('form_section', array(
          	'label'     => Mage::helper('frans')->__('General'),
         	'title'     => Mage::helper('frans')->__('General'),
          	'content'   =>  $this->getLayout()->createBlock('frans/adminhtml_accounting_freight_edit_tab_form')->toHtml(),
			'active'    => true
      ));


      return parent::_beforeToHtml();
  }
  
}