<?php
/*
 * Represents whether a payment has been captured
 * 
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Grid_Renderer_OnHold extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	public function render(Varien_Object $row)
	{

        if($row->getIsOnHold() == true)
        {
            return "Yes";
        }
		return '';
	}
}
?>