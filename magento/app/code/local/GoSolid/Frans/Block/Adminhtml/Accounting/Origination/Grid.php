<?php
class GoSolid_Frans_Block_Adminhtml_Accounting_Origination_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{

		parent::__construct();
		$this->setId('fransGrid');

		//TODO: Set default sort
		//$this->setDefaultSort('id');
		//$this->setDefaultDir('ASC');

		//TODO: Optional
		//$this->setSaveParametersInSession(true);
		//$this->setDefaultFilter(array('status' => 'Enabled'));
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getModel('frans/accounting_origination')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}


	protected function _prepareColumns()
  	{

  		$this->addColumn('id', array(
			  'header'    => Mage::helper('frans')->__('ID'),
			  'align'     =>'right',
			  'width'     => '50px',
			  'index'     => 'id',
		  ));


  		$this->addColumn('origination_name', array(
			  'header'    => Mage::helper('frans')->__('Origination Name'),
			  'align'     =>'right',
			  'width'     => '50px',
			  'index'     => 'origination_name',
		  ));
		  
		  return parent::_prepareColumns();
	}

	public function getRowUrl($row)
  	{
    	return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  	}
}