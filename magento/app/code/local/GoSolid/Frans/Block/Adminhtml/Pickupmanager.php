<?php
 
class GoSolid_Frans_Block_Adminhtml_Pickupmanager extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {

	$this->_controller = 'adminhtml_pickupmanager';
	$this->_blockGroup = 'frans';
	$this->_headerText = Mage::helper('frans')->__("Pickup Manager");
	$this->_addButtonLabel = Mage::helper('frans')->__("Add");



    parent::__construct();

    $this->_removeButton("add");
    
  }
}