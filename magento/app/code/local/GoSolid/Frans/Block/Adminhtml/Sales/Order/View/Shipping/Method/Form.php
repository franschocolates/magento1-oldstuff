<?php

/*
 * This block is adapted from the Mage_Adminhtml_Block_Sales_Order_Create_Shipping_Method_Form
 * block, which is used to set the shipping method before the order is placed, so it works on quotes.
 * Given that we have an already placed order, that won't work for us.
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_View_Shipping_Method_Form
	extends Mage_Adminhtml_Block_Sales_Order_Abstract
{
    protected $_rates;
	
    public function __construct()
    {
        parent::__construct();
        $this->setId('sales_order_view_shipping_method_form');
        $this->setTemplate('sales/order/view/shipping/method/form.phtml');
    }

    /**
     * Retrieve order shipping address model
     *
     * @return GoSolid_Frans_Model_Sales_Order_Address
     */
    public function getAddress()
    {
        return $this->getOrder()->getShippingAddress();
    }

    /**
     * Retrieve array of shipping rates groups
     *
     * @return array
     */
    public function getShippingRates()
    {
        if (empty($this->_rates)) {
            $groups = $this->getAddress()->getGroupedAllShippingRates();
            
            return $this->_rates = $groups;
        }
        return $this->_rates;
    }

    public function getShippingDescription()
    {
    	return $this->getOrder()->getShippingDescription();
    }

    public function getPreferredArrivalDate()
    {
        return $this->_formatDate($this->getOrder()->getPreferredArrivalDate());
    }

    public function getPlannedShipDate()
    {
        return $this->_formatDate($this->getOrder()->getPlannedShipDate());
    }

    public function getShipDate()
    {
        if ($shipDate = $this->getOrder()->getShipDate())
        {
            return $this->_formatDate($shipDate);
        }

        return '';
    }


    public function showShippingDescription()
    {
    	if ($this->getIsMethodLocked())
    	{
    		return true;
    		
    	}
    	
    	$rates = $this->getShippingRates();
    	return count($rates) == 0;
    }
    
    /**
     * Check whether the current shipping method is locked
     * Would be locked if everything already shipped.
     */
    public function getIsMethodLocked()
    {
    	$orderState = $this->getOrder()->getState(); 
    	return $orderState == Mage_Sales_Model_Order::STATE_CANCELED ||
    			$orderState == Mage_Sales_Model_Order::STATE_CLOSED ||
    			$orderState == Mage_Sales_Model_Order::STATE_COMPLETE;
    }
    
    public function getIsEditMode()
    {
    	if (isset($this->_data['is_edit_mode']))
    	{
    		return $this->getData('is_edit_mode');
    	}
    	
    	return false;
    }

    public function getCurrentShippingAmount()
    {
    	return Mage::getModel('directory/currency')->format(
    		$this->getOrder()->getBaseShippingAmount(), 
    		array('display'=>Zend_Currency::NO_SYMBOL), 
    		false
			);
    }
    
    public function getEditUrl()
    {
		return $this->getUrl('adminhtml/sales_order/editShippingForm', array ( 'order_id' => $this->getOrder()->getId()));
    }
    
    public function getSaveUrl()
    {
    	return $this->getUrl("adminhtml/sales_order/saveShipMethod", array( 'order_id' => $this->getOrder()->getId()));
    }
    
    /**
     * Check activity of method by code
     *
     * @param   string $code
     * @return  bool
     */
    public function isMethodActive($code)
    {
        return $code===$this->getShippingMethod();
    }

    public function getShippingMethod()
    {
    	return $this->getOrder()->getShippingMethod();
    }
    
    /**
     * Rertrieve carrier name from store configuration
     *
     * @param   string $carrierCode
     * @return  string
     */
    public function getCarrierName($carrierCode)
    {
        if ($name = Mage::getStoreConfig('carriers/'.$carrierCode.'/title', $this->getOrder()->getStore()->getId())) {
            return $name;
        }
        return $carrierCode;
    }

    public function getShippingPrice($price, $flag)
    {
        return $this->getOrder()->getStore()->convertPrice(
            Mage::helper('tax')->getShippingPrice(
                $price,
                $flag,
                $this->getAddress(),
                null,
                //We should send exact quote store to prevent fetching default config for admin store.
                $this->getOrder()->getStore()
            ),
            true
        );
    }

    public function getDateFieldHtml($fieldName, $value)
    {
        return $this->_getDateField($fieldName, $value);
    }

    protected function _formatDate($date)
    {
        return Mage::helper('frans')->calendarDisplayDate($date);
    }

    protected function _getDateField($fieldName, $value = '')
    {
        if ($value)
        {
            // we need to do this to prevent it from switching month & day
            $value = new Zend_Date($value, GoSolid_Frans_Helper_Data::CALENDAR_ZEND_DATE_FORMAT, Mage::app()->getLocale()->getLocale());
        }

        $dateField = new Varien_Data_Form_Element_Date(array(
            'format'   => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
            'image'    => $this->getSkinUrl('images/grid-cal.gif'),
            'name'     => $fieldName,
            'value'     => $value
        ));

        $dateField->setForm(new Varien_Data_Form()); // just need it so it doesn't crash.
        $dateField->setId(trim(str_replace(array('[', ']'), '_', $fieldName), '_')); // can't have [ ] in IDs
        return $dateField->toHtml();
    }

    protected function _getIcePackHtml()
    {
        return $this->getLayout()
                    ->createBlock('frans/adminhtml_sales_order_view_shipping_icePack_form')
                    ->setTemplate('sales/order/view/shipping/icepack/form.phtml')
                    ->toHtml();
    }

}