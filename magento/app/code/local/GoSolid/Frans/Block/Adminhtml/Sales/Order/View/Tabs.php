<?

class GoSolid_Frans_Block_Adminhtml_Sales_Order_View_Tabs extends Mage_Adminhtml_Block_Sales_Order_View_Tabs
{
	protected function _beforeToHtml()
	{
		$order = $this->getOrder();
		
		if ($order->getIsMultishipChildOrder())
		{
			$this->removeTab('order_creditmemos');
			$this->removeTab('order_invoices');
			$this->removeTab('order_transactions');
		}
		elseif ($order->getIsMultishipParent())
		{
			$this->removeTab('order_shipments');
		}

        $this->addTab('activity', array(
            'label'     => Mage::helper('frans')->__('Activity'),
            'class'     => 'ajax',
            'url'       => $this->getUrl('*/*/activity', array('_current' => true)),
            'after'     => 'transactions'
        ));

		return parent::_beforeToHtml();
	}

}
