<?php
 
class GoSolid_Frans_Block_Adminhtml_Accounting_Freight extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {

	$this->_controller = 'adminhtml_accounting_freight';
	$this->_blockGroup = 'frans';
	$this->_headerText = Mage::helper('frans')->__("Accounting Freight");
	$this->_addButtonLabel = Mage::helper('frans')->__("Add Freight Code");
	
    parent::__construct();
    
  }
}