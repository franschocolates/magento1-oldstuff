<?php

class GoSolid_Frans_Block_Adminhtml_Accounting_Origination_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

	protected function _prepareForm()
	{

		$form = new Varien_Data_Form();
		$this->setForm($form);
		$fieldset = $form->addFieldset('frans_fs', array('legend'=>Mage::helper('frans')->__('Origination')));

		$fieldset->addField('origination_name', 'text', array(
	          'title'     => Mage::helper('frans')->__('Origination Name'),
			  'label'     => Mage::helper('frans')->__('Origination Name'),
	          'name'      => 'origination_name',
			  'class'     => 'required-entry',
	          'required'  => true,
			  'note'	  => 'Name of accounting origination',
	      	));
				
		
		if ( Mage::getSingleton('adminhtml/session')->getOriginationData() )
		{
	    	$form->setValues(Mage::getSingleton('adminhtml/session')->getOriginationData());
	    	Mage::getSingleton('adminhtml/session')->getOriginationData(null);
		}
		elseif(Mage::registry('origination_data'))
		{
	    	$form->setValues(Mage::registry('origination_data')->getData());
		}

		return parent::_prepareForm();
	}

}