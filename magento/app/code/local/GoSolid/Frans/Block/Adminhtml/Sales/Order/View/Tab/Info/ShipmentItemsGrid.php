<?php
/**
 * Child/Shipment grid
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_View_Tab_Info_ShipmentItemsGrid extends Mage_Adminhtml_Block_Widget_Grid
{
	private $_parentOrderId;
	
    public function __construct()
    {
        parent::__construct();
        $this->setId('sales_order_view_info_shipmentItemsGrid');
        $this->setDefaultSort('product_id');
        $this->setDefaultDir('ASC');
    }

    public function setParentOrderId($parentOrderId)
    {
    	$this->_parentOrderId = $parentOrderId;
    }
    
    public function getRowUrl($item)
    {
    	return false;
    }
    
    protected function _getCollectionClass()
    {
        return 'sales/order_item_collection';
    }
    
    protected function _prepareCollection()
    {
    	if (isset($this->_parentOrderId))
    	{
        	$collection = Mage::getResourceModel($this->_getCollectionClass());
        	$orderTable = Mage::getSingleton('core/resource')->getTableName('sales/order');
        	$collection->getSelect()
        		->joinInner(
					array( 'sfo' => $orderTable),
					 'sfo.entity_id = main_table.order_id')
                ->joinInner(
                    array('cpei' => 'catalog_product_entity_int'),
                        'main_table.product_id = cpei.entity_id AND cpei.attribute_id = (SELECT attribute_id FROM eav_attribute WHERE attribute_code = \'color_tile\')')
                ->joinInner (
                        array('fct' => 'frans_color_tile'),
                            'fct.id = cpei.value')
        		->where('sfo.multiship_parent_id = ?', $this->_parentOrderId)
                ->group(array('main_table.product_id', 'main_table.sku', 'main_table.name'))
        		# remove canceled
        		->having('SUM(qty_ordered - qty_canceled) > 0')
        		->columns("SUM(qty_ordered - qty_canceled) AS total_qty, CASE WHEN COUNT(notes) > 0 THEN 'Yes' ELSE 'No' END AS has_notes, fct.title AS color_tile");
        	$this->setCollection($collection);
		}

       	return $this;
    }

    
	protected function _prepareColumns()
    {
        $this->addColumn('name', array(
            'header'	=> Mage::helper('sales')->__('Product Name'),
            'type'  	=> 'text',
            'index' 	=> 'name',
        	'sortable'	=> false,
        	'filter' 	=> false,
        	'width' 	=> '40%' # otherwise it gets too wide
        ));

        $this->addColumn('accpac_sku', array(
            'header'=> Mage::helper('sales')->__('SKU'),
            'type'  => 'text',
            'index' => 'accpac_sku',
        	'sortable'	=> false,
        	'filter' => false,
        ));

        $this->addColumn('color_tile', array(
            'header'=> Mage::helper('sales')->__('Color Title'),
            'type'  => 'text',
            'index' => 'color_tile',
            'sortable'	=> false,
            'filter' => false,
        ));

        $this->addColumn('total_qty', array(
            'header' => Mage::helper('sales')->__('Quantity'),
            'index' => 'total_qty',
            'width' => '70px',
        	'sortable'	=> false,
        	'filter' => false,
        'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
        ));

        # remove back button
        
        # center this
        $this->addColumn('has_notes', array(
            'header' => Mage::helper('sales')->__('Has Product Notes'),
        	'width' => '70px',
            'index'  => 'has_notes',
        	'sortable'	=> false,
        	'align' 	=> 	'center',
        	'filter' => false,
        ));

        return $this;
    }

    /* 
     * We override this to prevent showing extra buttons
     */
    protected function _prepareLayout()
    {
    	$this->setFilterVisibility(false);
    	$this->setPagerVisibility(false);
        return Mage_Adminhtml_Block_Widget::_prepareLayout();
    }
}
