<?php
class GoSolid_Frans_Block_Adminhtml_ShippingZoneMethodTimes_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{

		parent::__construct();
		$this->setId('fransGrid');
	}
 
	protected function _prepareCollection()
	{
		$collection = Mage::getModel('frans/shippingZoneMethodTime')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}	
		
		
	protected function _prepareColumns()
  	{

  		$this->addColumn('id', array(
			  'header'    => Mage::helper('frans')->__('ID'),
			  'align'     =>'right',
			  'width'     => '50px',
			  'index'     => 'id',
		  ));
		  
		$this->addColumn('zone_id',
		    array(
		        'header'=> Mage::helper('frans')->__('Zone'),
		        'index' => 'zone_id',
		        'type'  => 'options',
		        'options' => Mage::getModel('frans/shippingZone')->getCollection()->toGridOptionArray('id', 'zone_name')
		));	        

		$this->addColumn('shipping_method',
		    array(
		        'header'=> Mage::helper('frans')->__('Shipping Method'),
		        'index' => 'shipping_method',
		        'type'  => 'options',
		        'options' => Mage::getModel('frans/shipping_carrier_frans')->allowedMethodsToGridOptionArray()
		));	        

		$this->addColumn('time_in_days',
		    array(
		        'header'=> Mage::helper('frans')->__('Time (in days)'),
		        'index' => 'time_in_days',
		        'type'  => 'text',
		));

        $this->addColumn('delivery_days',
            array(
                'header'=> Mage::helper('frans')->__('Delivery days'),
                'index' => 'delivery_days',
                'type'  => 'text',
                'renderer' => 'GoSolid_Frans_Block_Adminhtml_ShippingZoneMethodTimes_Grid_Renderer_DayOfWeek'
            ));

        $deliveryTypeOptions = Mage::getModel('frans/shippingZoneMethodTime')->deliveryTypesToGridOptionArray();
        $this->addColumn('delivery_type',
            array(
                'header'=> Mage::helper('frans')->__('Delivery Type'),
                'index' => 'delivery_type',
                'type'  => 'options',
                'options' => $deliveryTypeOptions
            ));

        /*
         * EXAMPLE CODE. CAN BE REMOVED.
         *

        //text plain
        $this->addColumn('myname', array(
            'header'    => Mage::helper('frans')->__('MyName'),
            'align'     =>'left',
            'index'     => 'name',
        ));

        //date
        $this->addColumn('mydate', array(
              'header'    => Mage::helper('frans')->__('MyDate'),
              'type'      => 'date',
              'align'     => 'left',
              'index'     => 'mydate', //change me
              'gmtoffset' => false
          ));

        //options
        $options = Mage::getModel('frans/[model_lcase]')->getCollection()->setOrder("title", "asc")->toGridOptionArray("id", "title");
        $this->addColumn('category_id', array(
            'header'    => Mage::helper('frans')->__('Category'),
            'align'     => 'left',
            'index'     => 'category_id',
            'type'  => 'options',
            'options'	=> $options, //example of how to get options.
        ));

        //status
        $this->addColumn('status', array(
            'header'    => Mage::helper('frans')->__('Status'),
            'align'     => 'left',
            'width'     => '150px',
            'index'     => 'status',
            'type'	  => 'options',
            'options'   => array(
                'Enabled' => 'Enabled',
                'Disabled' => 'Disabled',
            ),
        ));

        END EXAMPLE CODE
        */
		  

		  return parent::_prepareColumns();
	}
	
	public function getRowUrl($row)
  	{
    	return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  	}
}