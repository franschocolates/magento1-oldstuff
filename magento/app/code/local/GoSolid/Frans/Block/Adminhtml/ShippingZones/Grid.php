<?php
class GoSolid_Frans_Block_Adminhtml_ShippingZones_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{

		parent::__construct();
		$this->setId('fransGrid');
		
	}
 
	protected function _prepareCollection()
	{
		$collection = Mage::getModel('frans/shippingZone')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}	
		
		
	protected function _prepareColumns()
  	{

  		$this->addColumn('id', array(
			  'header'    => Mage::helper('frans')->__('ID'),
			  'align'     =>'right',
			  'width'     => '50px',
			  'index'     => 'id',
		  ));
		  
		$this->addColumn('zone_name', array(
            'header'    => Mage::helper('frans')->__('Zone Name'),
            'type'      => 'text',
            'align'     => 'left',
            'index'     => 'zone_name', 
        ));
		
		$this->addColumn('postcode_list',
		    array(
		        'header'=> Mage::helper('catalog')->__('Postcode List'),
		        'index' => 'postcode_list',
            	'type'      => 'text',
		));	        
		  		  
		$this->addColumn('position', array(
            'header'    => Mage::helper('frans')->__('Position'),
            'type'      => 'text',
            'align'     => 'left',
            'index'     => 'position', 
        ));
		
		/*
		   * EXAMPLE CODE. CAN BE REMOVED.
		   *
		  
		  //text plain
		  $this->addColumn('myname', array(
			  'header'    => Mage::helper('frans')->__('MyName'),
			  'align'     =>'left',
			  'index'     => 'name',
		  ));
		  
		  //date
		  $this->addColumn('mydate', array(
	            'header'    => Mage::helper('frans')->__('MyDate'),
	            'type'      => 'date',
	            'align'     => 'left',
	            'index'     => 'mydate', //change me
	            'gmtoffset' => false
	        ));
	        
	      //options
	      $options = Mage::getModel('frans/[model_lcase]')->getCollection()->setOrder("title", "asc")->toGridOptionArray("id", "title");
		  $this->addColumn('category_id', array(
			  'header'    => Mage::helper('frans')->__('Category'),
			  'align'     => 'left',
			  'index'     => 'category_id',
			  'type'  => 'options',
			  'options'	=> $options, //example of how to get options.
		  )); 
		  
		  //status
		  $this->addColumn('status', array(
			  'header'    => Mage::helper('frans')->__('Status'),
			  'align'     => 'left',
			  'width'     => '150px',
			  'index'     => 'status',
			  'type'	  => 'options',
			  'options'   => array(
				  'Enabled' => 'Enabled',
				  'Disabled' => 'Disabled',
			  ),
		  ));
		  
		  END EXAMPLE CODE
		  */ 
		  

		  return parent::_prepareColumns();
	}
	
	public function getRowUrl($row)
  	{
    	return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  	}
}