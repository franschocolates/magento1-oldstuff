<?php

class GoSolid_Frans_Block_Adminhtml_FutureShipDates_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

	protected function _prepareForm()
	{

		$form = new Varien_Data_Form();
		$this->setForm($form);
		$fieldset = $form->addFieldset('frans_fs', array('legend'=>Mage::helper('frans')->__('Future Ship Date')));

		$fieldset->addField('calendar_date', 'date', array(
	          'title'     => Mage::helper('frans')->__('Date'),
			  'label'     => Mage::helper('frans')->__('Date'),
	          'name'      => 'calendar_date',
			  'class'     => 'required-entry date',
	          'required'  => true,
			  'image'  => Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN).'/adminhtml/default/default/images/grid-cal.gif',
			  'format' => 'M/d/y',
		));

      	$options = Mage::getSingleton('frans/futureShipMessage')->getCollection()->toOptionArray("id", 'message_title', "Select");
      	$fieldset->addField('message_id', 'select', array(
          'label'     => Mage::helper('frans')->__('Message'),
		  'title'     => Mage::helper('frans')->__('Message'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'message_id',
		  'values'   => $options,
      	  'note'	  => 'Please select the message to display'
      	));
      	
      	$availabilityTypeOptions = Mage::getSingleton('frans/futureShipDate')->getAvailabilityTypeOptionArray();
      	$fieldset->addField('availability_type', 'select', array(
          'label'     => Mage::helper('frans')->__('Availability Type'),
		  'title'     => Mage::helper('frans')->__('Availability Type'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'availability_type',
		  'values'   => $availabilityTypeOptions,
      	  'note'	  => 'Availability on the date'
      	));

      	      	/*
	    * EXAMPLE CODE. CAN BE REMOVED.
	    *

		//text

	    //yes no
	    $fieldset->addField('myyesno', 'select', array(
		          'title'    	 => Mage::helper('frans')->__('Add To Cart'),
				  'label'    	 => Mage::helper('frans')->__('Add To Cart'),
		          //'class'     => 'required-entry',
				  //'required'  	=> true,
		          'name'     	 => 'myyesno',
		    	  'checked'=> false,
		    	  'name'  => 'myyesno',
		    	  'options'   => array(
									'1' => 'Yes',
									'0' => 'No',
						  ),
				  'note'	=> Mage::helper('frans')->__("My Note."),

      		));

		//money
	   	$fieldset->addField('myprice', 'text', array(
          'title'     => Mage::helper('frans')->__('MyPrice'),
		  'label'     => Mage::helper('frans')->__('MyPrice'),
       	  'name'      => 'myprice',
	   	  //'class'     => 'required-entry validate-zero-or-greater',
          //'required'  => true,
		  //'note' => 	 Mage::helper('frans')->__("My Note Here.")
      		));

      	//options
      	$options = Mage::getSingleton('event/facility')->getCollection()->setOrder("name", "asc")->toOptionArray("id", "name", "Select");
      	$fieldset->addField('myoptions', 'select', array(
          'label'     => Mage::helper('frans')->__('MyOptions'),
		  'title'     => Mage::helper('frans')->__('MyOptions'),
          //'class'     => 'required-entry',
          //'required'  => true,
          'name'      => 'myoptions',
		  'values'   => $options
      	));

		//status
		$fieldset->addField('status', 'select',array(
			'label'     => Mage::helper('frans')->__('Status'),
			'required'  => true,
			'name'      => 'status',
			'options'   => array(
					  		'Enabled' => 'Enabled',
					  		'Disabled' => 'Disabled',
				  ),
		));

		// END EXAMPLE CODE
		*/



		if ( Mage::getSingleton('adminhtml/session')->getFutureShipDatesData() )
		{
	    	$form->setValues(Mage::getSingleton('adminhtml/session')->getFutureShipDatesData());
	    	Mage::getSingleton('adminhtml/session')->getFutureShipDatesData(null);
		}
		elseif(Mage::registry('futureShipDates_data'))
		{
	    	$form->setValues(Mage::registry('futureShipDates_data')->getData());
		}

		return parent::_prepareForm();
	}

}