<?php
/**
 * Form for changing billing method after an order has been created.
 *
 * Based on a combination of Mage_Payment_Block_Form_Container
 *  and Mage_Adminhtml_Block_Sales_Order_Create_Billing_Method_Form
 *
 * @see Mage_Payment_Block_Form_Container
 * @see Mage_Adminhtml_Block_Sales_Order_Create_Billing_Method_Form
 */

class GoSolid_Frans_Block_Adminhtml_Sales_Order_View_Billing_Method_Form
        extends Mage_Adminhtml_Block_Template
{
    /**
     * @return GoSolid_Frans_Model_Sales_Order
     */
    public function getOrder()
    {
        return Mage::registry('current_order');
    }

    public function canEditPayment()
    {
        return $this->getOrder()->canEditPayment()
                && $this->hasMethods();
    }

    public function getEditUrl()
    {


        return $this->getUrl('*/sales_order/changePayment', array('order_id' => $this->getOrder()->getId(), "is_retail_payment" => $this->getIsRetailPayment() ));
    }

    public function getAddressFormHtml()
    {
        $form = new Varien_Data_Form();
        $addressFields = $form->addFieldset('billing_address_fields', array());
        $billingAddress = $this->getOrder()->getBillingAddress();

        // field mappings
        // key is property on address to use to get value
        // value is field name in address form
        // we use a capital because we are going to prefix with "get"
        // so that we can use built in things like Street1, which
        // wouldn't exist in getData
        $fieldMapping = array(
            'Firstname' => 'order-billing_address_firstname'
            , 'Lastname' => 'order-billing_address_lastname'
            , 'Street1' => 'order-billing_address_street0'
            , 'Street2' => 'order-billing_address_street1'
            , 'Region' => 'order-billing_address_region'
            , 'Postcode' => 'order-billing_address_postcode'
            , 'CountryId' => 'order-billing_address_country_id'
        );

        foreach ($fieldMapping as $sourceField => $formFieldName)
        {
            $getter = 'get' . $sourceField;
            $addressFields->addField(
                $formFieldName, 'text', array( 'value' => $billingAddress->$getter())
            );
        }

        return $addressFields->toHtml();
    }

    public function getGiftcardNumber()
    {
        return $this->getOrder()->getGiftcardNumber();
    }

    /**
     * Prepare children blocks
     */
    protected function _beforeToHtml()
    {
        /**
         * Create child blocks for payment methods forms
         */
        foreach ($this->getMethods() as $method) {
            $this->setChild(
                'payment.method.'.$method->getCode(),
                $this->helper('payment')->getMethodFormBlock($method)
            );
        }

        return parent::_beforeToHtml();
    }

    /**
     * Check and prepare payment method model
     *
     * Redeclare this method in child classes for declaring method info instance
     *
     * @return bool
     */
    protected function _assignMethod($method)
    {
        $method->setInfoInstance($this->getOrder()->getPayment()->clearForEdit());
        return $this;
    }

    /**
     * Declare template for payment method form block
     *
     * @param   string $method
     * @param   string $template
     * @return  Mage_Payment_Block_Form_Container
     */
    public function setMethodFormTemplate($method='', $template='')
    {
        if (!empty($method) && !empty($template)) {
            if ($block = $this->getChild('payment.method.'.$method)) {
                $block->setTemplate($template);
            }
        }
        return $this;
    }

    /**
     * Rewritten to use order properties instead of quote
     *
     * @param $method
     * @return bool
     */
    protected function _canUseMethod($method)
    {
        if (!$method->canUseInternal()) {
            return false;
        }
        if (!$method->canUseForCountry($this->getOrder()->getBillingAddress()->getCountry())) {
            return false;
        }

        if (!$method->canUseForCurrency($this->getOrder()->getStore()->getBaseCurrencyCode())) {
            return false;
        }

        /**
         * Checking for min/max order total for assigned payment method
         */
        $total = $this->getOrder()->getBaseGrandTotal();
        $minTotal = $method->getConfigData('min_order_total');
        $maxTotal = $method->getConfigData('max_order_total');

        if((!empty($minTotal) && ($total < $minTotal)) || (!empty($maxTotal) && ($total > $maxTotal))) {
            return false;
        }
        return true;
    }

    /**
     * Retrieve availale payment methods
     * Changed from base to use order properties (instead of quote)
     *
     * @return array
     */
    public function getMethods()
    {
        $methods = $this->getData('methods');
        if (is_null($methods))
        {
            /* @var GoSolid_Frans_Model_Sales_Order $order */
            $order = $this->getOrder();
            $store = $order ? $order->getStoreId() : null;
            $methods = $this->helper('payment')->getStoreMethods($store, null);
            foreach ($methods as $key => $method) {
                if ($this->_canUseMethod($method))
                {
                    $this->_assignMethod($method);
                } else {
                    unset($methods[$key]);
                }

                //handle hiding the pay at pickup from the stores.
                if($this->getIsRetailPayment() == true && $method->getCode() == "pay_at_pickup")
                {
                    unset($methods[$key]);
                }

            }
            $this->setData('methods', $methods);
        }
        return $methods;
    }

    public function getSelectedMethodCode()
    {
        return $this->getOrder()->getPayment()->getMethod();
    }

}