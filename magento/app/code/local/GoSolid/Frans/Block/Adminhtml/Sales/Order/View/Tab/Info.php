<?php
class GoSolid_Frans_Block_Adminhtml_Sales_Order_View_Tab_Info 
		extends Mage_Adminhtml_Block_Sales_Order_View_Tab_Info
{
	public function getChildOrdersHtml()
	{
		if ($this->getOrder()->getIsMultishipParent())
		{
			$shipmentGridBlock = $this->getLayout()
										->createBlock('frans/adminhtml_sales_order_view_tab_info_shipmentGrid');
			$shipmentGridBlock->setParentOrderId($this->getOrder()->getId());
			
			return $shipmentGridBlock->toHtml();
		}
		
		return '';
	}
	
	public function getAllItemsHtml()
	{
		if ($this->getOrder()->getIsMultishipParent())
		{
			$shipmentGridBlock = $this->getLayout()
										->createBlock('frans/adminhtml_sales_order_view_tab_info_shipmentItemsGrid');
			$shipmentGridBlock->setParentOrderId($this->getOrder()->getId());
			
			return $shipmentGridBlock->toHtml();
		}
		
		return '';
	}
	
	public function getShippingMethodHtml()
	{
		$block = $this->getLayout()->createBlock('frans/adminhtml_sales_order_view_shipping_method_form');
		return $block->toHtml();
	}

    public function getPaymentHtml()
    {

        return $this->getChild('order_payment_form')
                    ->setOrder($this->getOrder())
                    ->toHtml();
    }

}
?>