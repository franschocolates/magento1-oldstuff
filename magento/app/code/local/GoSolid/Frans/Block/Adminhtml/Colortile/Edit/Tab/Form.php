<?php

class GoSolid_Frans_Block_Adminhtml_Colortile_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

	protected function _prepareForm()
	{

		$form = new Varien_Data_Form(); 
		$this->setForm($form);
		$fieldset = $form->addFieldset('frans_fs', array('legend'=>Mage::helper('frans')->__('Color Tiles')));

		$fieldset->addField('title', 'text', array(
			'title'     => Mage::helper('frans')->__('Title'),
			'label'     => Mage::helper('frans')->__('Title'),
			'name'      => 'title',
			'class'     => 'required-entry',
			'required'  => true
		));

		$fieldset->addField('image', 'image', array(
			'title'     => Mage::helper('frans')->__('Tile Image'),
			'label'     => Mage::helper('frans')->__('Tile Image'),
			'name'      => 'image',
			'note'      => 'Image dimensions must be 35x35 pixels'
		));

		$fieldset->addField('status', 'select', array(
			'title'    	 => Mage::helper('frans')->__('Enabled'),
			'label'    	 => Mage::helper('frans')->__('Enabled'),
			'name'     	 => 'status',
			'checked'=> true,
			'options'   => array(
				'1' => Mage::helper('frans')->__('Yes'),
				'0' => Mage::helper('frans')->__('No')
			)
		));

		$fieldset->addField('position', 'text', array(
			'title'     => Mage::helper('frans')->__('Position'),
			'label'     => Mage::helper('frans')->__('Position'),
			'name'      => 'position',
			'class'     => 'required-entry validate-not-negative-number',
			'required'  => true,
			'note'	  => Mage::helper('frans')->__('Position '),
		));
		
		
		/*
	    * EXAMPLE CODE. CAN BE REMOVED.
	    *
		
		//text
		$fieldset->addField('mytext', 'text', array(
	          'title'     => Mage::helper('frans')->__('MyText'),
			  'label'     => Mage::helper('frans')->__('MyText'),
	          'name'      => 'name',
			  //'class'     => 'required-entry', 
	          //'required'  => true,
			  //'note'	  => 'My Note Here',
	      	));
		
	    //yes no
	    $fieldset->addField('myyesno', 'select', array(
		          'title'    	 => Mage::helper('frans')->__('Add To Cart'),
				  'label'    	 => Mage::helper('frans')->__('Add To Cart'),
		          //'class'     => 'required-entry', 
				  //'required'  	=> true,
		          'name'     	 => 'myyesno',
		    	  'checked'=> false,
		    	  'name'  => 'myyesno',
		    	  'options'   => array(
									'1' => 'Yes',
									'0' => 'No',
						  ),
				  'note'	=> Mage::helper('frans')->__("My Note."),

      		));   	
	   
		//money
	   	$fieldset->addField('myprice', 'text', array(
          'title'     => Mage::helper('frans')->__('MyPrice'),
		  'label'     => Mage::helper('frans')->__('MyPrice'),
       	  'name'      => 'myprice',   
	   	  //'class'     => 'required-entry validate-zero-or-greater',
          //'required'  => true,
		  //'note' => 	 Mage::helper('frans')->__("My Note Here.")
      		));
	  
      	//options
      	$options = Mage::getSingleton('event/facility')->getCollection()->setOrder("name", "asc")->toOptionArray("id", "name", "Select");
      	$fieldset->addField('myoptions', 'select', array(
          'label'     => Mage::helper('frans')->__('MyOptions'),
		  'title'     => Mage::helper('frans')->__('MyOptions'),
          //'class'     => 'required-entry',
          //'required'  => true,
          'name'      => 'myoptions',
		  'values'   => $options
      	));
		
		//status
		$fieldset->addField('status', 'select',array(
			'label'     => Mage::helper('frans')->__('Status'),
			'required'  => true,
			'name'      => 'status',
			'options'   => array(
					  		'Enabled' => 'Enabled',
					  		'Disabled' => 'Disabled',
				  ),
		));
		
		// END EXAMPLE CODE
		*/
		
	
	
		if ( Mage::getSingleton('adminhtml/session')->getColorTileData() )
		{
	    	$form->setValues(Mage::getSingleton('adminhtml/session')->getColorTileData());
	    	Mage::getSingleton('adminhtml/session')->getColorTileData(null);
		}
		elseif(Mage::registry('colorTile_data'))
		{
	    	$form->setValues(Mage::registry('colorTile_data')->getData());
		}
		
		return parent::_prepareForm();
	}
  
}