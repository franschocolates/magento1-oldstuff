<?php

class GoSolid_Frans_Block_Adminhtml_Sales_Order_Create_Form_Notes extends Mage_Adminhtml_Block_Widget_Form
{

    public function _getCategoryOptions()
    {
        return Mage::getModel('frans/orderItemNoteCategory')
                        ->getCollection()
                        ->toOptionArray('category_label', 'category_label', 'None');
/*
        return array(0 =>
                    array('label' => 'Sale',
                            'value' => array(
                                        0 => array('label' => 'Regular Sale',
                                                    'value' => 'Regular Sale'),
                                        1 => array('label' => 'A Gift For Friends or Family',
                                                    'value' => 'A Gift For Friends or Family'),
                                        2 => array('label' => 'A Gift For Myself',
                                                    'value' => 'A Gift For Myself'),
                                        3 => array('label' => 'Business',
                                                    'value' => 'Business'),
                                        4 => array('label' => 'Event Customer',
                                                    'value' => 'Event Customer'),
                                        5 => array('label' => 'Wedding customer',
                                                    'value' => 'Wedding customer'),
                                    )
                        ),
                    1 => array('label' => 'No Charge',
                            'value' => array(
                                        0 => array('label' => 'Business Gifting',
                                                'value' => 'Business Gifting'),
                                        1 => array('label' => 'I\'m Sorry Gift',
                                                'value' => 'I\'m Sorry Gift'),
                                        2 => array('label' => 'Item Not Received - Exact Replacement',
                                                'value' => 'Item Not Received - Exact Replacement'),
                                        3 => array('label' => 'Item Not Received - I\'m Sorry Gift',
                                                'value' => 'Item Not Received - I\'m Sorry Gift'),
                                        4 => array('label' => 'Product Replacement - Damaged',
                                                'value' => 'Product Replacement - Damaged'),
                                        5 => array('label' => 'Product Replacement - Exact',
                                                'value' => 'Product Replacement - Exact'),
                                        6 => array('label' => 'Product Replacement - Melted',
                                                'value' => 'Product Replacement - Melted'),
                                        7 => array('label' => 'Product Replacement - New/Additional Item',
                                                'value' => 'Product Replacement - New/Additional Item'),
                                        8 => array('label' => 'Wedding Samples',
                                                'value' => 'Wedding Samples'),
                                        9 => array('label' => 'Wrong Item Received - Entry Error',
                                                'value' => 'Wrong Item Received - Entry Error'),
                                        10 => array('label' => 'Wrong Item Received - Picking Error',
                                                'value' => 'Wrong Item Received - Picking Error')
                                            )
                        )
                );*/
    }



    protected function _prepareForm(){

        $item = $this->getItem();
        $item_id = $item->getId();
        $form = new Varien_Data_Form(array(
                            'id'        => 'item_notes_' . $item_id . '_form',
                            'action'    => $this->getUrl('*/sales_order_productNotes/save', array('id' => $item_id)),
                            'method'    => 'post'
                            ));

        $form->setHtmlIdPrefix('item_' . $item_id . '_');

        // order type and category are only available if editable
        // otherwise it is order view and doesn't make sense to
        // allow them to edit, since they already placed the order
        if ($this->getIsEditable())
        {
            $form->addField('order_type', 'select', array(
                'name'      => "item[$item_id][order_type]",
                'label'     => Mage::helper('frans')->__('Order Type'),
                'id'        => 'order_type',
                'title'     => Mage::helper('frans')->__('Order Type'),
                'class'     => 'input-select',
                'values'    => array(0 => array('label' => 'Sale', 'value' => 'SALE'),
                    1 => array('label' => 'No Charge', 'value' => 'NO CHARGE')),
                'onchange'  => "order.setCustomPrice(this, '$item_id');"

            ));

            $form->addField('category', 'select', array(
                'name'      => "item[$item_id][category]",
                'label'     => Mage::helper('frans')->__('Category'),
                'id'        => 'category',
                'title'     => Mage::helper('frans')->__('Category'),
                'class'     => 'input-select',
                'values'    => $this->_getCategoryOptions(),
            ));
        }
        else
        {
            $form->addField('order_type', 'hidden', array(
                'name' =>  "item[$item_id][order_type]",
                'id'        => 'order_type',
            ));

            $form->addField('category', 'hidden', array(
                'name' =>  "item[$item_id][category]",
                'id'        => 'category',
            ));
        }


        $form->addField('notes', 'textarea', array(
                'name'      => "item[$item_id][notes]",
                'label'     => 'Notes',
                'id'        => 'notes',
                'title'     => Mage::helper('frans')->__('Notes'),
                'class'     => 'input-textarea',
                'after_element_html'    => (Mage::getSingleton('adminhtml/session_quote')->getQuote()->getIsMultiShipping()) ? '<button type="button" data-ta-id="item_' . $item_id . '_notes" onclick="order.updateItemNotes(' . $item_id . ')" class="disabled">Save Notes</button>' : '',
                ));

        $data = $item->getData();
        
        if($this->getUseForm()):
            $form->addField('cancel', 'button', array(
                'value'     => 'Cancel',
                'no_span'   => true,
                'class'     => 'item-notes-cancel',
                'onclick'   => "productNotes.hideForm('$item_id');"
                ));
            $form->addField('save', 'button', array(
                'value'     => 'Save',
                'no_span'   => true,
                'class'     => 'item-notes-save',
                'onclick'   => "productNotes.saveNotes('item_notes_$item_id');"
                ));
            $data['cancel'] = 'Cancel';
            $data['save'] = 'Save';
        endif;

        $form->setUseContainer($this->getUseForm());
        $form->setValues($data);
        $this->setForm($form);

        return parent::_prepareForm();

    }

}