<?php
/**
 * Created by goSolid.
 * Added to allow special address tab
 * As well as to add the new xcart orders tab.
 */ 
class GoSolid_Frans_Block_Adminhtml_Customer_Edit_Tabs
    extends Mage_Adminhtml_Block_Customer_Edit_Tabs
{
    /**
     * Overrides to replace the addresses tab if the customer exists.
     * Necessary because we can't change it after the _beforeHtml in the parent is called
     * and parent is the one that adds the address tab.
     *
     * @param string $tabId
     * @param array|Varien_Object $tab
     * @return Mage_Adminhtml_Block_Widget_Tabs
     */
    public function addTab($tabId, $tab)
    {
        $customerId = Mage::registry('current_customer')->getId();
        if ($customerId && $tabId == 'addresses')
        {
            $tab = array(
                'label'     => Mage::helper('customer')->__('Addresses'),
                'class'     => 'ajax',
                'url'       => $this->getUrl('*/*/addresses', array('_current' => true)),
            );
        }
        return parent::addTab($tabId, $tab);
    }

    protected function _beforeToHtml()
    {
        if (Mage::registry('current_customer')->getId())
        {
            // use same permissions as normal for viewing orders
            if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view'))
            {
                $this->addTab('xcartOrders', array(
                    'label'     => Mage::helper('frans')->__('XCart Orders'),
                    'class'     => 'ajax',
                    'url'       => $this->getUrl('*/*/xcartOrders', array('_current' => true)),
                    'after'     => 'tags'
                ));
            }
        }

        return parent::_beforeToHtml();

    }
}