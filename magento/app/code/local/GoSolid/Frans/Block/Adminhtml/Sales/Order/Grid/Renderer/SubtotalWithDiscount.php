<?php
/**
 * Handles rendering a subtotal with a discount
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Grid_Renderer_SubtotalWithDiscount
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Currency
{
    public function render(Varien_Object $row)
    {
        $data = floatval($row->getBaseSubtotal())
                    + floatval($row->getBaseDiscountAmount());
        $sign = (bool)(int)$this->getColumn()->getShowNumberSign() && ($data > 0) ? '+' : '';
        $data = sprintf("%f", $data);
        $data = Mage::app()->getLocale()->currency($this->getColumn()->getCurrencyCode())->toCurrency($data);
        return $sign . $data;
    }
}