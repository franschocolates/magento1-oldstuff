<?php

class GoSolid_Frans_Block_Adminhtml_ShippingZoneMethodTimes_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

	protected function _prepareForm()
	{

		$form = new Varien_Data_Form(); 
		$this->setForm($form);
		$fieldset = $form->addFieldset('frans_fs', array('legend'=>Mage::helper('frans')->__('Shipping Zone Method/Time')));
	
      	$zoneOptions = Mage::getModel('frans/shippingZone')->getCollection()->toOptionArray('id', 'zone_name');
      	$fieldset->addField('zone_id', 'select', array(
          'label'     => Mage::helper('frans')->__('Shipping Zone'),
		  'title'     => Mage::helper('frans')->__('Shipping Zone'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'zone_id',
		  'values'   => $zoneOptions,
      	  'note'	  => 'Shipping Zone the time is being designated for'
      	));
				
      	$shippingOptions = Mage::getSingleton('frans/shipping_carrier_frans')->allowedMethodsToGridOptionArray();
      	$fieldset->addField('shipping_method', 'select', array(
          'label'     => Mage::helper('frans')->__('Shipping Method'),
		  'title'     => Mage::helper('frans')->__('Shipping Method'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'shipping_method',
		  'values'   => $shippingOptions,
      	  'note'	  => 'Shipping Method the time is being designated for'
      	));
	    
		$fieldset->addField('time_in_days', 'text', array(
	          'title'     => Mage::helper('frans')->__('Time in Days'),
			  'label'     => Mage::helper('frans')->__('Time in Days'),
	          'name'      => 'time_in_days',
              'class'     => 'validate-not-negative-number',
		      'required'  => true,
      	  	  'note'	  => 'Time in days it will take for something to arrive after being shipped'
		));

        $dayOptions = array();

        $startDate = new DateTime('Sunday next week');

        for ($i = 0; $i < 7; $i++)
        {
            Mage::log("$i");
            if ($i > 0)
            {
                $startDate->modify("+1 day");
            }

            $dayOptions[] = array('value' => $i, 'label' => $startDate->format('l'));
        }


        $fieldset->addField('delivery_days', 'multiselect', array(
            'title'     => Mage::helper('frans')->__('Delivery Days'),
            'label'     => Mage::helper('frans')->__('Delivery Days'),
            'name'      => 'delivery_days',
            'required'  => true,
            'note'	  => 'Days this zone can be delivered to for this method',
            'values'   => $dayOptions,
        ));

        $fieldset->addField('delivery_type', 'select', array(
            'title'     => Mage::helper('frans')->__('Delivery Type'),
            'label'     => Mage::helper('frans')->__('Delivery Type'),
            'name'      => 'delivery_type',
            'required'  => true,
            'note'	  => 'Type of Delivery available (Any, Residential, Commercial)',
            'values'   => Mage::getModel('frans/shippingZoneMethodTime')->deliveryTypesToOptionArray(),
        ));


        /*
        * EXAMPLE CODE. CAN BE REMOVED.
        *

        //text
        $fieldset->addField('mytext', 'text', array(
              'title'     => Mage::helper('frans')->__('MyText'),
              'label'     => Mage::helper('frans')->__('MyText'),
              'name'      => 'name',
              //'class'     => 'required-entry',
              //'required'  => true,
              //'note'	  => 'My Note Here',
              ));

        //yes no
        $fieldset->addField('myyesno', 'select', array(
                  'title'    	 => Mage::helper('frans')->__('Add To Cart'),
                  'label'    	 => Mage::helper('frans')->__('Add To Cart'),
                  //'class'     => 'required-entry',
                  //'required'  	=> true,
                  'name'     	 => 'myyesno',
                  'checked'=> false,
                  'name'  => 'myyesno',
                  'options'   => array(
                                    '1' => 'Yes',
                                    '0' => 'No',
                          ),
                  'note'	=> Mage::helper('frans')->__("My Note."),

              ));

        //money
           $fieldset->addField('myprice', 'text', array(
          'title'     => Mage::helper('frans')->__('MyPrice'),
          'label'     => Mage::helper('frans')->__('MyPrice'),
             'name'      => 'myprice',
             //'class'     => 'required-entry validate-zero-or-greater',
          //'required'  => true,
          //'note' => 	 Mage::helper('frans')->__("My Note Here.")
              ));

          //options
          $options = Mage::getSingleton('event/facility')->getCollection()->setOrder("name", "asc")->toOptionArray("id", "name", "Select");
          $fieldset->addField('myoptions', 'select', array(
          'label'     => Mage::helper('frans')->__('MyOptions'),
          'title'     => Mage::helper('frans')->__('MyOptions'),
          //'class'     => 'required-entry',
          //'required'  => true,
          'name'      => 'myoptions',
          'values'   => $options
          ));

        //status
        $fieldset->addField('status', 'select',array(
            'label'     => Mage::helper('frans')->__('Status'),
            'required'  => true,
            'name'      => 'status',
            'options'   => array(
                              'Enabled' => 'Enabled',
                              'Disabled' => 'Disabled',
                  ),
        ));

        // END EXAMPLE CODE
        */
		
	
	
		if ( Mage::getSingleton('adminhtml/session')->getShippingZoneMethodTimesData() )
		{
	    	$form->setValues(Mage::getSingleton('adminhtml/session')->getShippingZoneMethodTimesData());
	    	Mage::getSingleton('adminhtml/session')->getShippingZoneMethodTimesData(null);
		}
		elseif(Mage::registry('shippingZoneMethodTimes_data'))
		{
	    	$form->setValues(Mage::registry('shippingZoneMethodTimes_data')->getData());
		}
		
		return parent::_prepareForm();
	}
  
}