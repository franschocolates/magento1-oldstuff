<?php

/*
 * This class inherited from Engine. Moved into GoSolid/Frans to add the removal of the Back Button.
 * 
 * 
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_View extends Mage_Adminhtml_Block_Sales_Order_View{

	public function __construct(){

        /** @var GoSolid_Frans_Model_Sales_Order $order */
        $order = $this->getOrder();

        $customer = Mage::getModel('customer/customer')->load($order->getCustomerId());
        $stripeCustomer = Mage::getModel('stripe/payment')->getStripeCustomer($customer->getStripeCustomerId());
        if(is_array($stripeCustomer)){
            Mage::getSingleton('core/session')->addError('Stripe error: '.$stripeCustomer['error']);
        }
		parent::__construct();


        //these are a list of allowed actions, defined in the table sales_order_status.
        //we make a button for each of these actions and add it to the order view.

        $statusActions = $order->getAllowedStatusActionOptions();
        //get the orders allowed actions.. and add those buttons.

        $pos = 200;

        foreach($statusActions as $action)
        {
            $this->_addButton('statusaction_' . $action["status"], array(
                'label'     => Mage::helper('sales')->__($action["label"]),
                'onclick'   => "setLocation('" . $this->getUrl('*/sales_order/changestatus', array("order_id" => $order->getId(), "status" => $action["status"] ) ) . "');"
            ), 0, $pos);

            $pos++;
        }

        if($order->getIsMultishipChildOrder()){
            $this->_removeButton('send_notification');
        }

        //unship option
        if($order->getShipDate() != null )
        {
            $unshipWarning = $this->__('Are you sure? Unshipping will delete the previous shipment and move the order back to processing.');
            $this->_addButton("unship", array(
                'label'     => Mage::helper('sales')->__("UnShip"),
                'title'       => "Resets the ship date and removes the shipment. Can not be undone.",
                'onclick'   => "deleteConfirm('$unshipWarning','" . $this->getUrl('*/sales_order/unship') . "');"
            ));
        }


        //resend message

        if($order->getIsMultishipParent() == false)
        {
            if($order->getShipDate() != null )
            {
                $disabled = false;
                $resendMsg = $this->__("Click to resend the shipment notification email.");
            }
            else
            {
                $disabled = true;
                $resendMsg = $this->__("Order has not yet been shipped.");
            }

            if($order->getShipmentEmailSentAt() != null )
            {
                $resendMsg = $this->__("Email last sent on %s.", Mage::helper('core')->formatDate($order->getShipmentEmailSentAt(), 'medium', false)   ) ;
            }

            $this->_addButton("resend", array(
                'label'     => Mage::helper('sales')->__("Resend Shipment Email"),
                'title'       => $resendMsg,
                'onclick'   => "setLocation('" . $this->getUrl('*/sales_order/resendShipmentNotification') . "');",
                'disabled' => $disabled
            ));

            // change the message on order edit if present
            // and editing won't actually do anything
            if ($order->getShipDate() != null)
            {
                $onclickJs = 'deleteConfirm(\''
                    . Mage::helper('frans')->__('This order has already shipped. Editing it will create a new order, but will not cancel the existing order.')
                    . '\', \'' . $this->getEditUrl() . '\');';

                $this->_updateButton('order_edit', 'onclick', $onclickJs);
            }

            if($order->getBaseGiftcardAmount() != null) {
                // can't edit orders where gift card was used
                $this->_updateButton('order_edit',
                    'onclick', "alert('This order was purchased using a gift card and cannot be edited.');");
                $this->_updateButton('order_edit', 'class', 'disabled');
            }
        }
        else
        {
            // can't edit multiship parent orders
            // update button works even if the edit button isn't present.
            $this->_updateButton('order_edit',
                    'onclick', "alert('Sorry, you may not edit parent orders.');");
            $this->_updateButton('order_edit', 'class', 'disabled');
        }

		$this->_addButton('receipt', array(
				'label' 	=> Mage::helper('receipt')->__('Print Receipts'),
				'onclick'	=> 'setLocation(\'' . $this->getUrl('*/sales_order/receipt') . '\');',
				'class'		=> 'go'
				), 0, 100, 'header', 'header');
				
		$this->_removeButton('back');

	}

}