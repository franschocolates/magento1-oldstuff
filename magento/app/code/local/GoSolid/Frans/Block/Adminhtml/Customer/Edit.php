<?php
/**
 * Top level block for editing customers
 * Rewritten to allow custom logic for buttons & interacting with orders...
 */ 
class GoSolid_Frans_Block_Adminhtml_Customer_Edit
    extends Mage_Adminhtml_Block_Customer_Edit
{
    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        // handle the case where we might need to return to an order
        $fromOrder = $this->getRequest()->getParam('from_order', false);
        if ($fromOrder)
        {
            // we need an extra param so we know we came from an order
            // also add a button so they can create and go back to creating an order
            $onclick = "editForm.submit('{$this->getUrl('*/' . $this->_controller . '/saveAndCreateOrder', array('from_order' => true))}');";
            $this->_addButton('save_and_create_order', array(
                'label'     => $this->__('Save and Create Order'),
                'onclick'   => $onclick,
                'class'     => 'save'
            ), 15);

        }

        return $this;
    }

}