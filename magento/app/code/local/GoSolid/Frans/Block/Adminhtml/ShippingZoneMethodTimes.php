<?php
 
class GoSolid_Frans_Block_Adminhtml_ShippingZoneMethodTimes extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {

	$this->_controller = 'adminhtml_shippingZoneMethodTimes';
	$this->_blockGroup = 'frans';
	$this->_headerText = Mage::helper('frans')->__("Shipping Zone Method/Times");
	$this->_addButtonLabel = Mage::helper('frans')->__("Add Method/Time Entry");
	
    parent::__construct();
    
  }

}