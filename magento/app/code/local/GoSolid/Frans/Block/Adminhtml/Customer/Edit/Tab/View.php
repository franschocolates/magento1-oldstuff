<?php
class GoSolid_Frans_Block_Adminhtml_Customer_Edit_Tab_View extends Mage_Adminhtml_Block_Customer_Edit_Tab_View
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getCustomerNotes()
    {
        $notesValue = Mage::getModel('frans/customerNotes')
            ->getNotesFromCustomerOrEmail($this->_customer->getId(), false);
        return $notesValue;
    }
}

