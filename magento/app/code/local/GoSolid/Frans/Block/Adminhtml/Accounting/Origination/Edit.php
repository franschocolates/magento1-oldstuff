<?php

class GoSolid_Frans_Block_Adminhtml_Accounting_Origination_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct()
    {
        parent::__construct();
        $this->_removeButton('delete');

        $this->_objectId = 'id';
        $this->_blockGroup = 'frans';
        $this->_controller = 'adminhtml_accounting_origination';

        // Optional
        //$this->_updateButton('save', 'label', Mage::helper('event')->__('Save'));
		//$this->_removeButton('delete');
		//$this->_removeButton('reset');

		$this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('frans')->__('Save and Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }

        ";
    }

	public function getHeaderText()
    {
        if( Mage::registry('origination_data') && Mage::registry('origination_data')->getId() ) {
            return Mage::helper('frans')->__("Edit Origination");
		} else {
            return Mage::helper('frans')->__('Add Origination');
        }
    }

}