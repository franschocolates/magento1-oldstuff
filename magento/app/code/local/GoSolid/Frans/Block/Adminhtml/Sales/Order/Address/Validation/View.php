<?php
/**
 * Encapsulates display of validation results for Addresses
 *
 *
 *
 *
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Address_Validation_View
        extends Mage_Adminhtml_Block_Abstract
{
    var $_proposedAddress = false;

    var $_addressValidator = null;
    var $_changedFields = null;

    protected function _beforeToHtml()
    {
        $this->_addressValidator = Mage::getModel('frans/shipping_addressValidator');
        $this->_changedFields = $this->_addressValidator->getChangedAddressFields($this->getAddress());

        return parent::_beforeToHtml();
    }

    public function getEnteredAddressHtml()
    {
        return $this->_getAddressHtml($this->getAddress());
    }

    public function isFieldChanged($fieldName)
    {
        return in_array($fieldName, $this->_changedFields);
    }

    public function getDisplayMessages()
    {
        $filteredMessages = array();
        $messages = $this->_addressValidator->getChangeMessages($this->getAddress());

        // so if we only have modified, we don't want to show it - there are no messages to warn them about
        // but if we have modified and something, we want to show the message
        $hasModified = false;

        foreach ($messages as $message)
        {
            if ($message != GoSolid_Frans_Model_Shipping_AddressValidator::MODIFIED)
            {
                $filteredMessages[] = $message;
            }
            else
            {
                $hasModified = true;
            }
        }

        // if we have modified but we also have filtered messages, put modified back in at the end
        if (count($filteredMessages) > 0 && $hasModified)
        {
            $filteredMessages[] =  GoSolid_Frans_Model_Shipping_AddressValidator::MODIFIED;
        }

        return $filteredMessages;
    }

    public function getProposedAddress()
    {
        return $this->_addressValidator->convertChangesToCustomerAddress($this->getAddress());
    }

    public function getCountrySelectHtml($countryId)
    {
        $changed = $this->isFieldChanged('country_id');

        $fakeForm = new Varien_Data_Form();
        $fakeForm->setFieldContainerIdPrefix('');

        $options = Mage::getSingleton('eav/config')
                        ->getAttribute('customer_address', 'country_id')
                        ->getSource()
                        ->getAllOptions();
        $formAttributes = array(
            'name'  => 'updated_address[country_id]'
            , 'value' => $countryId
            , 'values' => $options
        );

        if ($changed)
        {
            $formAttributes['class'] = 'changed-field';
        }

        $select = new Varien_Data_Form_Element_Select($formAttributes);
        $select->setId('updated_address_country_id');
        $select->setForm($fakeForm);

        $fakeForm->setValues(array('country_id' => $countryId));
        return $select->getElementHtml();
    }

    // move to a template
    protected function _getNoChangesHtml()
    {
        // TODO move this into a different block
        return "<div>No validation data available.</div>";
    }

}