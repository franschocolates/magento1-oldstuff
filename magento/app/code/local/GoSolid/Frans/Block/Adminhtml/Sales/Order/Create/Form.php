<?php
/**
 * Rewritten to allow us to switch up some AdminOrder JS initialization
 */ 
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Create_Form
    extends Mage_Adminhtml_Block_Sales_Order_Create_Form
{
    /**
     * Rewritten to add in indicating if we are multiship
     *
     * @return string
     */
    public function getOrderDataJson()
    {
        $data = array();

	    /* @var $addressForm Mage_Customer_Model_Form */
	    $addressForm = Mage::getModel('customer/form')
		    ->setFormCode('adminhtml_customer_address')
		    ->setStore($this->getStore());

	    // get retail store data for shipping address drop-down
	    $retailStores = Mage::getModel('frans/retailStore')->getActiveFrontendStores();
	    foreach($retailStores as $retailStore){
		    $storeAsAddress = $retailStore->getAsCustomerAddress();
		    $data['stores'][$retailStore->getId()] = $addressForm->setEntity($storeAsAddress)
			    ->outputData(Mage_Customer_Model_Attribute_Data::OUTPUT_FORMAT_JSON);
	    }
        if (!is_null($this->getCustomerId())) {
            $data['customer_id'] = $this->getCustomerId();
            $data['addresses'] = array();
            foreach ($this->getCustomer()->getAddresses() as $address) {
                $data['addresses'][$address->getId()] = $addressForm->setEntity($address)
                    ->outputData(Mage_Customer_Model_Attribute_Data::OUTPUT_FORMAT_JSON);
            }
        }
        if (!is_null($this->getStoreId())) {
            $data['store_id'] = $this->getStoreId();
            $currency = Mage::app()->getLocale()->currency($this->getStore()->getCurrentCurrencyCode());
            $symbol = $currency->getSymbol() ? $currency->getSymbol() : $currency->getShortName();
            $data['currency_symbol'] = $symbol;
            $data['shipping_method_reseted'] = !(bool)$this->getQuote()->getShippingAddress()->getShippingMethod();
            $data['payment_method'] = $this->getQuote()->getPayment()->getMethod();
            // goSolid - added the following:
            $data['is_multiship'] = $this->getQuote()->getIsMultiShipping();
        }
        return Mage::helper('core')->jsonEncode($data);
    }

}