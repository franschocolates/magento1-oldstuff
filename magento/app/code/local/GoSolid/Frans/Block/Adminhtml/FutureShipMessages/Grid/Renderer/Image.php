<?php
/**
 * Created by PhpStorm.
 * User: Mac
 * Date: 3/31/14
 * Time: 12:57 PM
 */


class GoSolid_Frans_Block_Adminhtml_FutureShipMessages_Grid_Renderer_Image extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
{
    if($row->getData($this->getColumn()->getIndex())==""){
        return "";
    }
    else{
        $html = '<img ';
        $html .= 'id="' . $this->getColumn()->getId() . '" ';
        $html .= 'style="margin-top: 10px;" ';
        $html .= 'src="' . Mage::getBaseUrl("media") . $row->getData($this->getColumn()->getIndex()) . '"';
        $html .= 'class="grid-image ' . $this->getColumn()->getInlineCss() . '"/>';

        return $html;
    }
}
}