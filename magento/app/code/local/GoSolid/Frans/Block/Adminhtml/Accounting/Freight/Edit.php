<?php

class GoSolid_Frans_Block_Adminhtml_Accounting_Freight_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct()
    {
        parent::__construct();
        $this->_removeButton('delete');

        $this->_objectId = 'id';
        $this->_blockGroup = 'frans';
        $this->_controller = 'adminhtml_accounting_freight';
        
        // Optional
        //$this->_updateButton('save', 'label', Mage::helper('event')->__('Save'));
		//$this->_removeButton('delete');
		//$this->_removeButton('reset');
		
		$this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('frans')->__('Save and Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);
		
        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
			
        ";
    }

	public function getHeaderText()
    {  
        if( Mage::registry('accountingFreight_data') && Mage::registry('accountingFreight_data')->getId() ) {
            return Mage::helper('frans')->__("Edit Freight Code");
		} else {
            return Mage::helper('frans')->__('Add Freight Code');
        }
    }

}