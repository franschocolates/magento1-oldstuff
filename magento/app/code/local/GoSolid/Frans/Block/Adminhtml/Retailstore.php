<?php
 
class GoSolid_Frans_Block_Adminhtml_Retailstore extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {

	$this->_controller = 'adminhtml_retailstore';
	$this->_blockGroup = 'frans';
	$this->_headerText = Mage::helper('frans')->__("Manage Retail Stores");
	$this->_addButtonLabel = Mage::helper('frans')->__("Add New Retail Store");
	
    parent::__construct();
    
  }
}