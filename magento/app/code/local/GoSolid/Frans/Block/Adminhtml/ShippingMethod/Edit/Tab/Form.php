<?php

class GoSolid_Frans_Block_Adminhtml_ShippingMethod_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

	protected function _prepareForm()
	{

		$form = new Varien_Data_Form(); 
		$this->setForm($form);
		$fieldset = $form->addFieldset('frans_fs', array('legend'=>Mage::helper('frans')->__('ShippingMethod')));

        $shippingOptions = Mage::getSingleton('frans/shipping_carrier_frans')->allowedMethodsToGridOptionArray();
        $fieldset->addField('shipping_method', 'select', array(
            'label'     => Mage::helper('frans')->__('Shipping Method'),
            'title'     => Mage::helper('frans')->__('Shipping Method'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'shipping_method',
            'values'   => $shippingOptions,
            'note'	  => 'Shipping Method that is being configured'
        ));

		//text
		$fieldset->addField('label', 'text', array(
	          'title'     => Mage::helper('frans')->__('Label'),
			  'label'     => Mage::helper('frans')->__('Label'),
	          'name'      => 'label',
			  'class'     => 'required-entry',
	          'required'  => true,
			  'note'	  => 'Text to represent the shipping method when viewing during checkout',
	      	));
		
        $fieldset->addField('color', 'text', array(
            'title'     => Mage::helper('frans')->__('Color'),
            'label'     => Mage::helper('frans')->__('Color'),
            'name'      => 'color',
            'class'     => 'required-entry',
            'required'  => true,
            'note'	  => 'Color the ship method should use in the calendar',
        ));
	  
		if ( Mage::getSingleton('adminhtml/session')->getShippingMethodData() )
		{
	    	$form->setValues(Mage::getSingleton('adminhtml/session')->getShippingMethodData());
	    	Mage::getSingleton('adminhtml/session')->getShippingMethodData(null);
		}
		elseif(Mage::registry('shippingMethod_data'))
		{
	    	$form->setValues(Mage::registry('shippingMethod_data')->getData());
		}
		
		return parent::_prepareForm();
	}
  
}