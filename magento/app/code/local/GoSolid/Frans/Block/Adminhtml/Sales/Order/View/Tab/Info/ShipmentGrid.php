<?php
/**
 * Child/Shipment grid
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_View_Tab_Info_ShipmentGrid extends Mage_Adminhtml_Block_Widget_Grid
{
	private $_parentOrderId;
	
    public function __construct()
    {
        parent::__construct();
        $this->setId('sales_order_view_tab_info_shipmentGrid');
        $this->setUseAjax(true);
        $this->setDefaultSort('increment_id');
        $this->setDefaultDir('ASC');
    }

    public function setParentOrderId($parentOrderId)
    {
    	$this->_parentOrderId = $parentOrderId;
    }
    
    public function getGridUrl()
    {
    	return $this->getUrl('adminhtml/sales_order/childGrid/parent_id/' . $this->_parentOrderId);
    }
    
    protected function _getCollectionClass()
    {
        return 'sales/order_grid_collection';
    }
    
    protected function _prepareCollection()
    {
    	if (isset($this->_parentOrderId))
    	{
        	$collection = Mage::getResourceModel($this->_getCollectionClass());
        	$collection->addFieldToFilter('multiship_parent_id', $this->_parentOrderId);
        	$this->setCollection($collection);
        	parent::_prepareCollection();
		}
		
       	return $this;
    }

    
	protected function _prepareColumns()
    {
        $this->addColumn('real_order_id', array(
            'header'=> Mage::helper('sales')->__('Order #'),
            'width' => '100px',
            'type'  => 'text',
            'index' => 'increment_id',
        ));

        $this->addColumn('status', array(
            'header' => Mage::helper('sales')->__('Status'),
            'index' => 'status',
            'type'  => 'options',
            'width' => '70px',
            'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
        ));

        $this->addColumn('shipping_city_state', array(
            'header' => Mage::helper('sales')->__('Destination'),
            'index'  => 'shipping_city_state',
        ));

        $this->addColumnAfter('shipping_description', array(
            'header' => Mage::helper('sales')->__('Shipping'),
            'index'  => 'shipping_description',
        ), 'shipping_city_state');

        $this->addColumnAfter('planned_ship_date', array(
            'header' => Mage::helper('sales')->__('Planned Ship Date'),
            'index'  => 'planned_ship_date',
            'type'   => 'date',
            ), 'shipping_description');

        $this->addColumnAfter('gift_message', array(
            'index'  => 'gift_message',
            'header' => Mage::helper('frans')->__('Gift Message'),
            'width'  => '65px',
            'type'   => 'options',
            'renderer'   => 'frans/adminhtml_widget_grid_column_renderer_hasValue',
            'has_value_label' => 'Has Gift Msg',
            'filter_condition_callback' => array($this, '_hasContentFilter'),
            'align'  => 'center',
            'options' => array( 0 => 'No', 1 => 'Yes'),
        ), 'planned_ship_date');

        return $this;
    }

    public function getRowUrl($row)
    {
        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view')) {
            return $this->getUrl('adminhtml/sales_order/view', array('order_id' => $row->getId()));
        }
        return false;
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('order_ids');
        $this->getMassactionBlock()->setUseSelectAll(false);

		$returnParams = array('return_order_id' => $this->_parentOrderId);
        $this->getMassactionBlock()->addItem('receipt_picktickets', array(
                'label' => Mage::helper('sales')->__('Print Picking Tickets'),
                'url' => $this->getUrl('*/sales_order/massPrintPickingTickets', $returnParams)
            ));

        $this->getMassactionBlock()->addItem('receipt_packingslips', array(
                'label' => Mage::helper('sales')->__('Print Packing Slips'),
                'url' => $this->getUrl('*/sales_order/massPrintPackingSlips', $returnParams)
            ));

        $this->getMassactionBlock()->addItem('receipt_giftmessages', array(
                'label' => Mage::helper('sales')->__('Print Gift Messages'),
                'url' => $this->getUrl('*/sales_order/massPrintGiftMessages', $returnParams)
            ));

        $this->getMassactionBlock()->addItem('receipt_all', array(
                'label' => Mage::helper('sales')->__('Print All Receipts'),
                'url' => $this->getUrl('*/sales_order/massPrintAllReceipts', $returnParams)
            ));

        return $this;
    }

    /**
     * Helper method to filter values. Unfortunately copied from our normal Sales Order Grid.
     *
     * @param $collection
     * @param $column
     * @return $this
     */
    protected function _hasContentFilter($collection, $column)
    {
        $filterValue = $column->getFilter()->getValue();

        if($filterValue == 1)
        {
            $this->getCollection()
                ->addFieldToFilter($column->getIndex(),array('neq' => ''));
        }
        else if($filterValue == 0)
        {
            $this->getCollection()
                ->addFieldToFilter($column->getIndex(),array('eq' => '' ));
        }

        return $this;
    }

}
