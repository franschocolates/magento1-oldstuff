<?php

class GoSolid_Frans_Block_Adminhtml_Pickupmanager_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

	protected function _prepareForm()
	{

		$form = new Varien_Data_Form(); 
		$this->setForm($form);
		$fieldset = $form->addFieldset('frans_fs', array('class'=>'wide','legend'=>Mage::helper('frans')->__('Order Information')));



        $fieldset->addType('itemgrid','GoSolid_Frans_Model_Pickupmanager_Itemgrid');
        $fieldset->addType('orderlink','GoSolid_Frans_Model_Pickupmanager_Orderlink');
        $fieldset->addType('orderinfo','GoSolid_Frans_Model_Pickupmanager_Orderinfo');


        $fieldset->addField('order_link', 'orderlink', array(
            'label'     => Mage::helper('frans')->__('Order #'),
            'required'  => false,
            'name'      => 'order_link',
            'value'     => Mage::registry('pickupmanager_data')->getData()

        ));


        $fieldset->addField('billing_name','orderinfo',array(
            'label'     => Mage::helper('frans')->__('Billing Name'),
            'required'  => false,
            'value'     => Mage::registry('pickupmanager_data')->getData()
        ));

        $fieldset->addField('billing_company','orderinfo',array(
            'label'     => Mage::helper('frans')->__('Billing Company'),
            'required'  => false,
            'value'     => Mage::registry('pickupmanager_data')->getData()
        ));

        $fieldset->addField('shipping_name','orderinfo',array(
            'label'     => Mage::helper('frans')->__('Shipping Name'),
            'required'  => false,
            'value'     => Mage::registry('pickupmanager_data')->getData()
        ));

        $fieldset->addField('accounting_label','orderinfo',array(
            'label'     => Mage::helper('frans')->__('Account Label'),
            'required'  => false,
            'value'     => Mage::registry('pickupmanager_data')->getData()
        ));

        $fieldset->addField('origination','orderinfo',array(
            'label'     => Mage::helper('frans')->__('Origination'),
            'required'  => false,
            'value'     => Mage::registry('pickupmanager_data')->getData()
        ));

        $fieldset->addField('salesperson','orderinfo',array(
            'label'     => Mage::helper('frans')->__('Order Taken By'),
            'required'  => false,
            'value'     => 'salesperson'
        ));

        $fieldset->addField('admin_notes','orderinfo',array(
            'label'     => Mage::helper('frans')->__('Admin Notes'),
            'required'  => false,
            'index'     => 'admin_notes'
        ));


        $fieldset->addField('item_info', 'itemgrid', array(
            'label'     => Mage::helper('frans')->__('Order Info'),
            'required'  => false,
            'name'      => 'item_info',
            'value'     => Mage::registry('pickupmanager_data')->getData()

        ));



        /*
        $fieldset->addField('status', 'label',array(
            'label'     => Mage::helper('frans')->__('Current Status'),
            'required'  => false,
            'name'      => 'status'


        ));


        //set the Picked Up Label
        $pickedUpLabel = "Picked Up";
        $fieldset->addField('status_input', 'select',array(
            'label'     => Mage::helper('frans')->__('Update Status To'),
            'required'  => true,
            'name'      => 'status_label',
            'options'   => array(
                '' => '',
                'ready_for_pickup' => 'Ready for Pickup',
                'picked_up' => $pickedUpLabel,
                'processing' => 'Processing',
            ),
        ));
        */


		/*
	    * EXAMPLE CODE. CAN BE REMOVED.
	    *

		//text
		$fieldset->addField('mytext', 'text', array(
	          'title'     => Mage::helper('frans')->__('MyText'),
			  'label'     => Mage::helper('frans')->__('MyText'),
	          'name'      => 'name',
			  //'class'     => 'required-entry',
	          //'required'  => true,
			  //'note'	  => 'My Note Here',
	      	));

	    //yes no
	    $fieldset->addField('myyesno', 'select', array(
		          'title'    	 => Mage::helper('frans')->__('Add To Cart'),
				  'label'    	 => Mage::helper('frans')->__('Add To Cart'),
		          //'class'     => 'required-entry',
				  //'required'  	=> true,
		          'name'     	 => 'myyesno',
		    	  'checked'=> false,
		    	  'name'  => 'myyesno',
		    	  'options'   => array(
									'1' => 'Yes',
									'0' => 'No',
						  ),
				  'note'	=> Mage::helper('frans')->__("My Note."),

      		));

		//money
	   	$fieldset->addField('myprice', 'text', array(
          'title'     => Mage::helper('frans')->__('MyPrice'),
		  'label'     => Mage::helper('frans')->__('MyPrice'),
       	  'name'      => 'myprice',
	   	  //'class'     => 'required-entry validate-zero-or-greater',
          //'required'  => true,
		  //'note' => 	 Mage::helper('frans')->__("My Note Here.")
      		));

      	//options
      	$options = Mage::getSingleton('event/facility')->getCollection()->setOrder("name", "asc")->toOptionArray("id", "name", "Select");
      	$fieldset->addField('myoptions', 'select', array(
          'label'     => Mage::helper('frans')->__('MyOptions'),
		  'title'     => Mage::helper('frans')->__('MyOptions'),
          //'class'     => 'required-entry',
          //'required'  => true,
          'name'      => 'myoptions',
		  'values'   => $options
      	));

		//status
		$fieldset->addField('status', 'select',array(
			'label'     => Mage::helper('frans')->__('Status'),
			'required'  => true,
			'name'      => 'status',
			'options'   => array(
					  		'Enabled' => 'Enabled',
					  		'Disabled' => 'Disabled',
				  ),
		));

		// END EXAMPLE CODE
		*/
		
	
	
		if ( Mage::getSingleton('adminhtml/session')->getPickupmanagerData() )
		{
	    	$form->setValues(Mage::getSingleton('adminhtml/session')->getPickupmanagerData());
	    	Mage::getSingleton('adminhtml/session')->getPickupmanagerData(null);
		}
		elseif(Mage::registry('pickupmanager_data'))
		{
	    	$form->setValues(Mage::registry('pickupmanager_data')->getData());
		}
		
		return parent::_prepareForm();
	}
  
}