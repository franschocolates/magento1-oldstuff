<?php
/**
 * Used to capture additional fields, which currently is Gift Message and Special Instructions
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Create_Form_Additional
    extends Mage_Adminhtml_Block_Sales_Order_Create_Form_Abstract
{
    public function hasOrder()
    {
        return $this->getOrder() && $this->getOrder()->getId();
    }

    public function canSave()
    {
        return $this->hasOrder();
    }

    /**
     * Indicates whether to show the form. We show it in all cases except a multiship parent.
     *
     * @return bool
     */
    public function getShowForm()
    {
        if ($order = $this->getOrder())
        {
            return !$order->getIsMultishipParent();
        }

        return true;
    }

    /***
     * When working in the context of creating, wont' have an order.
     *
     * @return GoSolid_Frans_Model_Sales_Order
     */
    public function getOrder()
    {
        if (Mage::registry('current_order'))
        {
            return Mage::registry('current_order');
        }

        return false;
    }

    public function getSaveUrl()
    {
        return $this->getUrl("adminhtml/sales_order/saveAdditional", array( '_current' => true));
    }

    public function getHeaderCssClass()
    {
        return 'head-account';
    }

    public function getHeaderText()
    {
        return Mage::helper('frans')->__('Messages');
    }

    public function getSaveButtonHtml()
    {
        $onclick = "submitAndReloadArea($('order_additional_fields').parentNode, '".$this->getSaveUrl()."')";
        return $this
            ->getLayout()
            ->createBlock('adminhtml/widget_button')
            ->setData(array(
                'label'   => Mage::helper('frans')->__('Save'),
                'class'   => 'save',
                'onclick' => $onclick
            ))
            ->toHtml();
    }

    /**
     * Prepare Form and add elements to form
     *
     * @return Mage_Adminhtml_Block_Sales_Order_Create_Form_Abstract
     */
    protected function _prepareForm()
    {

        $fieldset = $this->_form->addFieldset('main', array());

        $giftMessageValue = '';
        $specialInstructionsValue = '';

        if ($this->hasOrder())
        {
            $giftMessageValue = $this->getQuote()->getShippingAddress()->getGiftMessage();
            $specialInstructionsValue = $this->getQuote()->getShippingAddress()->getSpecialInstructions();
        }

        $fieldset->addField('gift_message', 'textarea',
            array(
            'name'  => 'gift_message',
            'label' => Mage::helper('frans')->__('Gift Message'),
            'id'    => 'gift_message',
            'class' => 'gift_message_entry',
            'value' => $giftMessageValue,
            'required' => false,
            )
        );

        $fieldset->addField('special_instructions', 'textarea',
            array(
                'name'  => 'special_instructions',
                'label' => Mage::helper('frans')->__('Special Instructions'),
                'id'    => 'special_instructions',
                'class' => 'special_instructions_entry',
                'value' => $specialInstructionsValue,
                'required' => false,
            )
        );

        $this->_form->addFieldNameSuffix('order');

        return $this;
    }

}