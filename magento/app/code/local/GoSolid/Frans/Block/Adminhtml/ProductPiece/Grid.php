<?php
class GoSolid_Frans_Block_Adminhtml_ProductPiece_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{

		parent::__construct();
		$this->setId('fransGrid');
		
		//TODO: Set default sort
		//$this->setDefaultSort('id');
		//$this->setDefaultDir('ASC');
		
		//TODO: Optional
		//$this->setSaveParametersInSession(true);
		//$this->setDefaultFilter(array('status' => 'Enabled'));
	}
 
	protected function _prepareCollection()
	{
		$collection = Mage::getModel('frans/productPiece')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}	

	protected function _prepareColumns()
  	{

		$this->addColumn('piece_code', array(
		  'header'    => Mage::helper('frans')->__('Code'),
		  'align'     => 'left',
		  'index'     => 'piece_code'
		));

		$this->addColumn('piece_label', array(
		  'header'    => Mage::helper('frans')->__('Label'),
		  'align'     => 'left',
		  'index'     => 'piece_label'
		));

		$this->addColumn('piece_image', array(
		  'header'    => Mage::helper('frans')->__('Image'),
		  'align'     => 'center',
		  'index'     => 'piece_image',
		  'renderer'  => 'frans/adminhtml_productPiece_grid_renderer_image'
		));

		$this->addColumn('is_active', array(
		  'header'    => Mage::helper('frans')->__('Status'),
		  'align'     => 'left',
		  'width'     => '90px',
		  'index'     => 'is_active',
		  'type'	  => 'options',
		  'options'   => array(
			  1 => 'Enabled',
			  0 => 'Disabled'
		  )
		));

		return parent::_prepareColumns();
	}
	
	public function getRowUrl($row)
  	{
    	return $this->getUrl('*/*/edit', array('piece_id' => $row->getPieceId()));
  	}
}