<?php
/*
 * Represents whether a payment has been captured
 * 
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Grid_Renderer_PaymentCaptured extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	public function render(Varien_Object $row)
	{
		if ($totalPaid = $row->getTotalPaid())
		{
			$grandTotal = $row->getGrandTotal();
			# Should we do an epsilon here? perhaps
			if ($totalPaid == $grandTotal)
			{
				return 'Yes';
			}
			else if ($totalPaid > 0 && $totalPaid <= $grandTotal)
			{
				return 'Partial'; // partial
			}
		}  	
		
		return 'No';
	}
}
?>