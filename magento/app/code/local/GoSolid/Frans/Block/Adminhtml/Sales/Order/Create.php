<?php
/**
 * Overridden to allow us to add the multiship button
 */ 
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Create
    extends Mage_Adminhtml_Block_Sales_Order_Create
{
    public function __construct()
    {
        parent::__construct();

        $quote = $this->_getSession()->getQuote();

        // only available if we aren't already multishipping
        // and if there is no order ID in the session (meaning that we aren't editing an order
        // we can't switch to multiship when editing a previous order.
        if (!$quote->getIsMultiShipping() && !$this->_getSession()->getOrderId())
        {
            $this->_addButton('multiship', array(
                'label'     => Mage::helper('frans')->__('Switch to Multiship'),
                'onclick'   => "order.setMultiship();",
                'id'        => 'switch_multiship_button'
            ));

            $customerId = $this->_getSession()->getCustomerId();
            $storeId    = $this->_getSession()->getStoreId();

            if (is_null($customerId) || !$storeId) {
                $this->_updateButton('multiship', 'style', 'display:none');
            }

        }
    }

}