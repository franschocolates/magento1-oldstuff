<?php
 
class GoSolid_Frans_Block_Adminhtml_ShippingZones extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {

	$this->_controller = 'adminhtml_shippingZones';
	$this->_blockGroup = 'frans';
	$this->_headerText = Mage::helper('frans')->__("Shipping Zones");
	$this->_addButtonLabel = Mage::helper('frans')->__("Add Zone");
	
    parent::__construct();
    
  }
}