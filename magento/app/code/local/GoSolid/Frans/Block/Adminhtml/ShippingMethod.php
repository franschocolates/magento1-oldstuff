<?php
 
class GoSolid_Frans_Block_Adminhtml_ShippingMethod extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {

	$this->_controller = 'adminhtml_shippingMethod';
	$this->_blockGroup = 'frans';
	$this->_headerText = Mage::helper('frans')->__("Shipping Methods");
	$this->_addButtonLabel = Mage::helper('frans')->__("Add Shipping Method");
	
    parent::__construct();
    
  }
}