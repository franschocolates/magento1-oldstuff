<?php
/**
 * Created to allow us to specify a different button action
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Create_Form_Import_Frame_Result
        extends Mage_ImportExport_Block_Adminhtml_Import_Frame_Result
{
    /**
     * Import button HTML for append to message.
     *
     * @return string
     */
    public function getImportButtonHtml()
    {
        return '&nbsp;&nbsp;<button onclick="order.startImport(\'' . $this->getImportStartUrl()
        . '\', \'' . Mage_ImportExport_Model_Import::FIELD_NAME_SOURCE_FILE . '\');" class="scalable save"'
        . ' type="button"><span><span><span>' . $this->__('Import') . '</span></span></span></button>';
    }

    /**
     * Import start action URL.
     *
     * @return string
     */
    public function getImportStartUrl()
    {
        return $this->getUrl('*/*/startImport');
    }

}

