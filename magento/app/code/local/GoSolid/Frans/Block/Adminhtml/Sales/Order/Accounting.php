<?php

/**
 * Adminhtml sales orders block for accounting
 *
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Accounting extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    // if this isn't present, it tries to create it in "admin", which clearly isn't our block.
    protected $_blockGroup = 'frans';

    public function __construct()
    {
        $this->_controller = 'adminhtml_sales_order_accounting';
        $this->_headerText = Mage::helper('frans')->__('Orders [Accounting]');
        parent::__construct();

        $this->removeButton('add');
    }

    protected function _prepareLayout()
    {
        // parent creates child grid, but sets it to save stuff in session
        // we prefer not to do that
        parent::_prepareLayout();
        $this->getChild('grid')
            ->setSaveParametersInSession(false);

        return $this;
    }
}
