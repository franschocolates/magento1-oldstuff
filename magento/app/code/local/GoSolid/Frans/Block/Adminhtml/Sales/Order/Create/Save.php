<?php
/**
 * Block to handle saving of quotes
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Create_Save
        extends Mage_Adminhtml_Block_Sales_Order_Create_Abstract
{
    public function getPrintUrl()
    {
        return $this->getUrl('*/*/printView');
    }

    public function getDownloadUrl()
    {
        return $this->getUrl('*/*/downloadQuote');
    }
}