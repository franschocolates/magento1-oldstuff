<?php
/**
 * Handles displaying saved quotes in sidebar. Adapted from the Sidebar blocks, but changed to use quotes.
 *
 * @see Mage_Adminhtml_Block_Sales_Order_Create_Sidebar_Abstract
 *
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Create_Sidebar_Quote
        extends Mage_Adminhtml_Block_Sales_Order_Create_Abstract
{

    public function getHeaderText()
    {
        return Mage::helper('frans')->__('Saved Quotes');
    }

    /**
     * Retrieve quote count
     *
     * @return int
     */
    public function getQuoteCount()
    {
        $count = $this->getData('quote_count');
        if (is_null($count)) {
            $count = count($this->getQuoteCollection());
            $this->setData('quote_count', $count);
        }
        return $count;
    }

    public function getQuoteCollection()
    {
        $collection = $this->getData('quote_collection');
        if (is_null($collection))
        {
            $collection = $this->getCreateOrderModel()->getCustomerSavedQuotes();
            $this->setData('quote_collection', $collection);
        }
        return $collection;
    }

    public function getLoadQuoteUrl($quote)
    {
        return $this->getUrl('*/*/loadQuote', array('quote_id' => $quote->getId()));
    }
}