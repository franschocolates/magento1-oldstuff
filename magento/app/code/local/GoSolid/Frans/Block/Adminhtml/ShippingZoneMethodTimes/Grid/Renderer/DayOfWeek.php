<?php
/*
 * Quick renderer to cover the situations where an order is a multiship parent, 
 * in which case certain fields aren't applicable. This renderer is specifically
 * for dates, so we need to inherit from the date renderer
 * 
 */
class GoSolid_Frans_Block_Adminhtml_ShippingZoneMethodTimes_Grid_Renderer_DayOfWeek
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Text
{
    // shockingly, there is no easy way in PHP to get this
    // so just hardcode for now
    static $daysOfWeek = array(
        0 => 'Sunday',
        1 => 'Monday',
        2 => 'Tuesday',
        3 => 'Wednesday',
        4 => 'Thursday',
        5 => 'Friday',
        6 => 'Saturday',
    );

	public function render(Varien_Object $row)
	{
        $deliveryDays = $row->getDeliveryDays();
        $selectedDays = explode(',', $deliveryDays);

        $translatedDays = array();

        foreach ($selectedDays as $dayInt)
        {
            $translatedDays[] = GoSolid_Frans_Block_Adminhtml_ShippingZoneMethodTimes_Grid_Renderer_DayOfWeek::$daysOfWeek[$dayInt];
        }

        return implode(', ', $translatedDays);
	}
}
?>