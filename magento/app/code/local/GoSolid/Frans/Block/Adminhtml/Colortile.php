<?php
 
class GoSolid_Frans_Block_Adminhtml_Colortile extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {

	$this->_controller = 'adminhtml_colortile';
	$this->_blockGroup = 'frans';
	$this->_headerText = Mage::helper('frans')->__("Manage Color Tiles");
	$this->_addButtonLabel = Mage::helper('frans')->__("Add Tile");

    parent::__construct();
    
  }
}