<?php

class GoSolid_Frans_Block_Adminhtml_Accounting_Label extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {

	$this->_controller = 'adminhtml_accounting_label';
	$this->_blockGroup = 'frans';
	$this->_headerText = Mage::helper('frans')->__("Accounting Labels");
	$this->_addButtonLabel = Mage::helper('frans')->__("Add Label");
	
    parent::__construct();
    
  }
}