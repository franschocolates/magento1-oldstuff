<?php
/**
 * Adminhtml sales order create shipment address block
 * Differs from shipping, in that there are multiple shipments in a multiship order
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Create_Shipments_Grid_Address
    extends Mage_Adminhtml_Block_Sales_Order_Create_Form_Address
{
    protected $_htmlNamePrefix;
    protected $_htmlIdPrefix;

    public function __construct()
    {
        $this->setTemplate('sales/order/create/shipments/grid/address.phtml');
        parent::__construct();
    }


    public function getAddressDisplayHtml()
    {
        // see config in frans\config.xml config/default/customer/address_templates
        return $this->getAddress()->format('admin_multiship_template');
    }

    protected function _beforeToHtml()
    {
        parent::_beforeToHtml();

        $quoteAddressId = $this->getQuoteAddressId();
        $this->_htmlNamePrefix = sprintf('order[shipments][%s]', $quoteAddressId);
        $this->_htmlIdPrefix = sprintf('order-shipments_address_%s_', $quoteAddressId);

        return $this;
    }

    /**
     * Prepare Form and add elements to form
     *
     * @return Mage_Adminhtml_Block_Sales_Order_Create_Shipping_Address
     */
    protected function _prepareForm()
    {
        $quoteAddressId = $this->getQuoteAddressId();
        $this->setJsVariablePrefix('shipmentAddress' . $quoteAddressId);
        parent::_prepareForm();

        $this->_form->addFieldNameSuffix(sprintf('order[shipments][%s]', $quoteAddressId));
        $this->_form->setHtmlNamePrefix(sprintf('order[shipments][%s]', $quoteAddressId));
        $this->_form->setHtmlIdPrefix(sprintf('order-shipments_address_%s_', $quoteAddressId));

        return $this;
    }

    public function getFieldName($name)
    {
        return $this->_htmlNamePrefix . $name;
    }

    public function getTextFieldHtml($attributeName, $placeholderText, $class = 'full', $index = false)
    {
        // because we are doing form field in a particular order/layout
        // we can't call getFormHtml()
        // instead, we have this method to reduce the drudgery
        $existingValue = $this->getAddress()->getData($attributeName);

        if ($index !== false)
        {
            $values = is_array($existingValue) ? $existingValue : explode("\n", $existingValue);
            $existingValue = $values[$index];
        }
        return sprintf("<input id='%s_%s' name='%s[%s]' class='input-text %s' type='text' placeholder='%s' value='%s' />",
            $this->_htmlIdPrefix, $attributeName, $this->_htmlNamePrefix, $attributeName
            , $class
            , $this->__($placeholderText)
            , $existingValue);
    }

    public function getShippingMethodHtml()
    {
        return $this->getLayout()
                    ->createBlock('frans/adminhtml_sales_order_create_shipping_method_form')
                    ->setShipmentAddress($this->getAddress())
                    ->setTemplate('sales/order/create/shipping/method/form.phtml')
                    ->setShowEdit($this->getEditMode()) // suppress shipping if we aren't editing.
                    ->toHtml();
    }

    public function canEdit()
    {
        return $this->getAddress()->getFirstname()
                && $this->getCanEdit(); // from parent block
    }

    public function canRemove()
    {
        // to be able to remove, we must have more than one
        // and either all addresses have to have a method,
        // or this particular method can not have a method (ie, they started adding/editing it
        // but haven't yet saved.
        return count($this->getQuote()->getAllShippingAddresses()) > 1
                &&
                (!$this->getAddress()->getShippingMethod() || $this->allAddressesHaveMethod());
    }

    public function allAddressesHaveMethod()
    {
        $allAddresses = $this->getQuote()->getAllShippingAddresses();

        $noMethod = array_filter($allAddresses, function ($add) { return !$add->getShippingMethod(); });

        return count($noMethod) == 0;
    }

    /**
     * Return is shipping address flag
     *
     * @return boolean
     */
    public function getIsShipping()
    {
        return true;
    }

    /**
     * Same as billing address flag
     *
     * @return boolean
     */
    public function getIsAsBilling()
    {
        return false;
    }

    /**
     * Saving shipping address must be turned off, when it is the same as billing
     *
     * @return bool
     */
    public function getDontSaveInAddressBook()
    {
        return false;
    }

    /**
     * Return Form Elements values
     *
     * @return array
     */
    public function getFormValues()
    {
        return $this->getAddress()->getData();
    }

    /**
     * Return customer address id
     *
     * @return int|boolean
     */
    public function getAddressId()
    {
        return $this->getAddress()->getCustomerAddressId();
    }

    public function getQuoteAddressId()
    {
        return $this->getAddress()->getId();
    }

    /**
     * Return is address disabled flag
     * For now, never disabled (why have a disabled sihpment?)
     *
     * @return boolean
     */
    public function getIsDisabled()
    {
        return false;
    }
}
