<?php

class GoSolid_Frans_Block_Adminhtml_FutureShipDates extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {

	$this->_controller = 'adminhtml_futureShipDates';
	$this->_blockGroup = 'frans';
	$this->_headerText = Mage::helper('frans')->__("Future Ship Dates");
	$this->_addButtonLabel = Mage::helper('frans')->__("Add Date");

    parent::__construct();

  }
}