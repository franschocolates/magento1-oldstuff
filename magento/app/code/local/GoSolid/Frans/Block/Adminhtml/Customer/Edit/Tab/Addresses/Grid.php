<?php
/**
 * Created by goSolid.
 */

class GoSolid_Frans_Block_Adminhtml_Customer_Edit_Tab_Addresses_Grid
    extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('customer_addresses_grid');
        $this->setUseAjax(true);
        $this->setDefaultSort('lastname');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(false);
        $this->setRowClickCallback($this->getJsObjectName() . '.doEditAddress');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('customer/address_collection')
            ->setCustomerFilter(Mage::registry('current_customer'))
            ->addAttributeToSelect('firstname')
            ->addAttributeToSelect('lastname')
            ->addAttributeToSelect('*');

        // join to try and find out if they are default billing
        $this->_addDefaultAddressFieldsToCollection($collection);

        $this->setCollection($collection);
        parent::_prepareCollection();

        return $this;
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/addressEdit', array('_current' => true, 'address_id' => $row->getId()));
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/addresses', array('_current' => true));
    }

    public function getMainButtonsHtml()
    {
        $html = '';

        $html .= $this->getChildHtml('add_address_button');
        $html.= $this->getResetFilterButtonHtml();
        $html.= $this->getSearchButtonHtml();

        return $html;
    }

    protected function _prepareColumns()
    {
        $this->addColumn('firstname', array(
            'header'    => Mage::helper('frans')->__('First Name'),
            'type'      => 'text',
            'align'     => 'left',
            'index'     => 'firstname',
        ));

        $this->addColumn('lastname', array(
            'header'    => Mage::helper('frans')->__('Last Name'),
            'type'      => 'text',
            'align'     => 'left',
            'index'     => 'lastname',
        ));

        $this->addColumn('company', array(
            'header'    => Mage::helper('frans')->__('Company'),
            'type'      => 'text',
            'align'     => 'left',
            'index'     => 'company',
        ));

        $this->addColumn('street', array(
            'header'    => Mage::helper('frans')->__('Street'),
            'type'      => 'text',
            'align'     => 'left',
            'index'     => 'street',
        ));

        $this->addColumn('city', array(
            'header'    => Mage::helper('frans')->__('City'),
            'type'      => 'text',
            'align'     => 'left',
            'index'     => 'city',
        ));

        $this->addColumn('region', array(
            'header'    => Mage::helper('frans')->__('State'),
            'type'      => 'text',
            'align'     => 'left',
            'index'     => 'region',
        ));

        $this->addColumn('postcode', array(
            'header'    => Mage::helper('frans')->__('Zip'),
            'type'      => 'text',
            'align'     => 'left',
            'index'     => 'postcode',
        ));

        $this->addColumn('country_id', array(
            'header'    => Mage::helper('frans')->__('Country'),
            'type'      => 'text',
            'align'     => 'left',
            'index'     => 'country_id',
        ));

        // really should be a better way, but oh well.
        $yesNoOptions = array( 0 => 'No', 1 => 'Yes');
        $this->addColumn('is_default_billing', array(
            'header'    => Mage::helper('frans')->__('Default Billing'),
            'align'     => 'left',
            'index'     => 'is_default_billing',
            'filter' => false, // right now we can't filter because it isn't actually a column
            'type'   => 'options',
            'sortable'   => false,
            'options' => $yesNoOptions
        ));


//         Changing Default Shipping to Default Mailing 8/26/16
        $this->addColumn('is_default_shipping', array(
            'header'    => Mage::helper('frans')->__('Default Mailing'),
            'type'      => 'text',
            'align'     => 'left',
            'index'     => 'is_default_shipping',
            'filter' => false,
            'type'   => 'options',
            'sortable'   => false,
            'options' => $yesNoOptions
        ));

        return parent::_prepareColumns();
    }

    // if we enable massaction, we need to do extra grid stuff
    // because the massaction grid stuff overwrites our custom grid stuff.
//    protected function _prepareMassaction()
//    {
//        $this->setMassactionIdField('entity_id');
//        $this->getMassactionBlock()->setFormFieldName('customer_address');
//
//        $this->getMassactionBlock()->addItem('delete', array(
//            'label'    => Mage::helper('customer')->__('Delete'),
//            'url'      => $this->getUrl('*/*/addressMassDelete'),
//            'confirm'  => Mage::helper('customer')->__('Are you sure?')
//        ));
//
//        return $this;
//    }

    protected function _prepareLayout()
    {
        $addUrl = $this->getUrl('*/*/addressAdd', array('_current' => true));

        $this->setChild('add_address_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('adminhtml')->__('Add New Address'),
                    'onclick'   => $this->getJsObjectName(). ".doAddAddress('$addUrl', $({$this->getId()}).parentElement.id)",
                    'class'   => 'task'
                ))
        );
        return parent::_prepareLayout();
    }

    /**
     * @param Mage_Customer_Model_Resource_Address_Collection $collection
     */
    protected function _addDefaultAddressFieldsToCollection($collection)
    {
        $defaultBillingAttribute = Mage::getModel('eav/entity_attribute')->loadByCode('customer', 'default_billing');
        $defaultShippingAttribute = Mage::getModel('eav/entity_attribute')->loadByCode('customer', 'default_shipping');

        $collection->getSelect()
            ->joinLeft(
                array('cust_def_billing' => Mage::getModel('core/resource')->getTableName('customer_entity_int'))
                , 'cust_def_billing.entity_id = e.parent_id AND cust_def_billing.`value` = e.entity_id AND cust_def_billing.attribute_id = ' . $defaultBillingAttribute->getId()
                , array('default_billing_id' => 'value')
            )
            ->columns(
                array('is_default_billing' => 'IF(cust_def_billing.value = e.entity_id, 1, 0)')
            )
            ->joinLeft(
                array('cust_def_shipping' => Mage::getModel('core/resource')->getTableName('customer_entity_int'))
                , 'cust_def_shipping.entity_id = e.parent_id AND cust_def_shipping.`value` = e.entity_id AND cust_def_shipping.attribute_id = ' . $defaultShippingAttribute->getId()
                , array('default_shipping_id' => 'value')
            )
            ->columns(
                array('is_default_shipping' => 'IF(cust_def_shipping.value = e.entity_id, 1, 0)')
            );

    }
}
