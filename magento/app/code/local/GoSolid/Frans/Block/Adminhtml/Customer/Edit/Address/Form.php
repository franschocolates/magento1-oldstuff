<?php
/**
 * Actual form for address editing
 */

class GoSolid_Frans_Block_Adminhtml_Customer_Edit_Address_Form
    extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * @return Mage_Customer_Model_Customer
     */
    public function getCustomer()
    {
        return Mage::registry('current_customer');
    }

    public function getCustomerId()
    {
        return $this->getCustomer()->getId();
    }

    public function getAddress()
    {
        return Mage::registry('current_customer_address');
    }

    public function getLegend()
    {
        $operation = ($this->getAddress()->getId()) ? 'Edit' : 'Add';

        return "$operation Address";
    }

    protected function _beforeToHtml()
    {
        parent::_beforeToHtml();
        return $this->initForm();
    }

    protected function _afterToHtml($html)
    {
        // note that address_fieldset isn't a form, but the
        // varien_form stuff still works.
        // the default billing/shipping is necessary because stupid Varien Checkboxes don't work
        // to set the value, so we do it after it loads. lame.
 $formJs = <<<EOHTML
<script type='text/javascript'>
customerAddressForm = new varienForm('address_fieldset');
 $('default_billing').value = 1;
 $('default_shipping').value = 1;
</script>
EOHTML;

        return parent::_afterToHtml($html) . $formJs;
    }

    /**
     * Adapted from Core
     *
     * @see Mage_Adminhtml_Block_Customer_Edit_Tab_Addresses
     * @return mixed
     */
    public function isReadonly()
    {
        return $this->getCustomer()->isReadonly();
    }

    /**
     * Initialize form object
     * Adapted from original addresses tab
     *
     * @see Mage_Adminhtml_Block_Customer_Edit_Tab_Addresses
     * @return GoSolid_Frans_Block_Adminhtml_Customer_Edit_Address
     */
    public function initForm()
    {
        /* @var $customer Mage_Customer_Model_Customer */
        $customer = $this->getCustomer();

        $form = new Varien_Data_Form();
        $fieldset = $form->addFieldset('address_fieldset', array(
                'legend'    => $this->getLegend())
        );

        $defaultBillingAddress = $customer->getDefaultBillingAddress();
        $defaultShippingAddress = $customer->getDefaultShippingAddress();

        $fieldset->addField('default_billing', 'checkbox', array(
            'label'     => 'Default Billing?',
            'name'      => 'default_billing',
            'value'     => 1,
            'checked'   => ($defaultBillingAddress && $defaultBillingAddress->getId() == $this->getAddress()->getId())
        ));
//         Changing Default Shipping to Default Mailing 8/26/16
        $fieldset->addField('default_shipping', 'checkbox', array(
            'label'     => 'Default Mailing?',
            'name'      => 'default_shipping',
            'value'     => 1,
            'checked'   => ($defaultShippingAddress && $defaultShippingAddress->getId() == $this->getAddress()->getId())
        ));

        $addressModel = $this->getAddress();
        if (!$addressModel->getCountryId())
        {
            $addressModel->setCountryId(Mage::helper('core')->getDefaultCountry($customer->getStore()));
        }
        /** @var $addressForm Mage_Customer_Model_Form */
        $addressForm = Mage::getModel('customer/form');
        $addressForm->setFormCode('adminhtml_customer_address')
            ->setEntity($addressModel)
            ->initDefaultValues();

        $attributes = $addressForm->getAttributes();
        if(isset($attributes['street'])) {
            Mage::helper('adminhtml/addresses')
                ->processStreetAttribute($attributes['street']);
        }
        foreach ($attributes as $attribute) {
            /* @var $attribute Mage_Eav_Model_Entity_Attribute */
            $attribute->setFrontendLabel(Mage::helper('customer')->__($attribute->getFrontend()->getLabel()));
            $attribute->unsIsVisible();
        }
        //$attributes = $this->_getCustomerDefaultAddressAttributes() + $attributes;
        //Mage::log("Attributes: " . print_r($attributes, true));
        $this->_setFieldset($attributes, $fieldset);

        $regionElement = $form->getElement('region');
        $regionElement->setRequired(true);
        if ($regionElement) {
            $regionElement->setRenderer($this->getLayout()->createBlock('adminhtml/customer_edit_renderer_region'));
        }

        $regionElement = $form->getElement('region_id');
        if ($regionElement) {
            $regionElement->setNoDisplay(true);
        }

        $country = $form->getElement('country_id');
        if ($country) {
            $country->addClass('countries');
        }

        if ($this->isReadonly()) {
            foreach ($addressModel->getAttributes() as $attribute) {
                $element = $form->getElement($attribute->getAttributeCode());
                if ($element) {
                    $element->setReadonly(true, true);
                }
            }
        }

        $customerStoreId = null;
        if ($customer->getId()) {
            $customerStoreId = Mage::app()->getWebsite($customer->getWebsiteId())->getDefaultStore()->getId();
        }

        $prefixElement = $form->getElement('prefix');
        if ($prefixElement) {
            $prefixOptions = $this->helper('customer')->getNamePrefixOptions($customerStoreId);
            if (!empty($prefixOptions)) {
                $fieldset->removeField($prefixElement->getId());
                $prefixField = $fieldset->addField($prefixElement->getId(),
                    'select',
                    $prefixElement->getData(),
                    '^'
                );
                $prefixField->setValues($prefixOptions);
            }
        }

        $suffixElement = $form->getElement('suffix');
        if ($suffixElement) {
            $suffixOptions = $this->helper('customer')->getNameSuffixOptions($customerStoreId);
            if (!empty($suffixOptions)) {
                $fieldset->removeField($suffixElement->getId());
                $suffixField = $fieldset->addField($suffixElement->getId(),
                    'select',
                    $suffixElement->getData(),
                    $form->getElement('lastname')->getId()
                );
                $suffixField->setValues($suffixOptions);
            }
        }

        $this->assign('customer', $customer);
        $form->setValues($addressModel->getData());
        $this->setForm($form);

        return $this;
    }


    /**
     * Return Form object
     *
     * @return Varien_Data_Form
     */
    /*    public function getForm()
        {
            if (is_null($this->_form)) {
                $this->_form = new Varien_Data_Form();
                $this->_prepareForm();
            }

            return $this->_form;
        }*/

    /**
     * Return customer address form instance
     * (originally from Core Edit Order address
     *
     * @see Mage_Adminhtml_Block_Sales_Order_Create_Form_Address
     * @return Mage_Customer_Model_Form
     */
    protected function _getAddressForm()
    {
        if (is_null($this->_addressForm)) {
            $this->_addressForm = Mage::getModel('customer/form')
                ->setFormCode('adminhtml_customer_address')
                ->setStore($this->getStore());
        }
        return $this->_addressForm;
    }

    /**
     * Prepare Form and add elements to form
     *
     * @return GoSolid_Frans_Block_Adminhtml_Customer_Edit_Address
     */
//    protected function _prepareForm()
//    {
//        $fieldset = $this->_form->addFieldset('main', array(
//            'no_container' => true
//            , 'legend'      => $this->getLegend()
//        ));
//
//        /* @var $addressModel Mage_Customer_Model_Address */
//        $addressModel = Mage::getModel('customer/address');
//
//        $addressForm = $this->_getAddressForm()
//            ->setEntity($addressModel);
//
//        $attributes = $addressForm->getAttributes();
//        if(isset($attributes['street'])) {
//            Mage::helper('adminhtml/addresses')
//                ->processStreetAttribute($attributes['street']);
//        }
//        $this->_addAttributesToForm($attributes, $fieldset);
//
//        $prefixElement = $this->_form->getElement('prefix');
//        if ($prefixElement) {
//            $prefixOptions = $this->helper('customer')->getNamePrefixOptions($this->getStore());
//            if (!empty($prefixOptions)) {
//                $fieldset->removeField($prefixElement->getId());
//                $prefixField = $fieldset->addField($prefixElement->getId(),
//                    'select',
//                    $prefixElement->getData(),
//                    '^'
//                );
//                $prefixField->setValues($prefixOptions);
//                if ($this->getAddressId()) {
//                    $prefixField->addElementValues($this->getAddress()->getPrefix());
//                }
//            }
//        }
//
//        $suffixElement = $this->_form->getElement('suffix');
//        if ($suffixElement) {
//            $suffixOptions = $this->helper('customer')->getNameSuffixOptions($this->getStore());
//            if (!empty($suffixOptions)) {
//                $fieldset->removeField($suffixElement->getId());
//                $suffixField = $fieldset->addField($suffixElement->getId(),
//                    'select',
//                    $suffixElement->getData(),
//                    $this->_form->getElement('lastname')->getId()
//                );
//                $suffixField->setValues($suffixOptions);
//                if ($this->getAddressId()) {
//                    $suffixField->addElementValues($this->getAddress()->getSuffix());
//                }
//            }
//        }
//
//
//        $regionElement = $this->_form->getElement('region_id');
//        if ($regionElement) {
//            $regionElement->setNoDisplay(true);
//        }
//
//        $this->_form->setValues($this->getFormValues());
//
//        if ($this->_form->getElement('country_id')->getValue()) {
//            $countryId = $this->_form->getElement('country_id')->getValue();
//            $this->_form->getElement('country_id')->setValue(null);
//            foreach ($this->_form->getElement('country_id')->getValues() as $country) {
//                if ($country['value'] == $countryId) {
//                    $this->_form->getElement('country_id')->setValue($countryId);
//                }
//            }
//        }
//        if (is_null($this->_form->getElement('country_id')->getValue())) {
//            $this->_form->getElement('country_id')->setValue(
//                Mage::helper('core')->getDefaultCountry($this->getStore())
//            );
//        }
//
//        // Set custom renderer for VAT field if needed
//        // removed this, we don't need VAT for now
//        // and it referenced order stuff that we don't want.
//
//        return $this;
//    }

    /**
     * Add additional data to form element
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @return GoSolid_Frans_Block_Adminhtml_Customer_Edit_Address
     */
//    protected function _addAdditionalFormElementData(Varien_Data_Form_Element_Abstract $element)
//    {
//        if ($element->getId() == 'region_id') {
//            $element->setNoDisplay(true);
//        }
//        return $this;
//    }

    /**
     * Add rendering EAV attributes to Form element
     * Adapted from Core
     *
     * @see Mage_Adminhtml_Block_Sales_Order_Create_Form_Abstract
     * @param array|Varien_Data_Collection $attributes
     * @param Varien_Data_Form_Abstract $form
     * @return GoSolid_Frans_Block_Adminhtml_Customer_Edit_Address
     */
//    protected function _addAttributesToForm($attributes, Varien_Data_Form_Abstract $form)
//    {
//        // add additional form types
//        $types = $this->_getAdditionalFormElementTypes();
//        foreach ($types as $type => $className) {
//            $form->addType($type, $className);
//        }
//        $renderers = $this->_getAdditionalFormElementRenderers();
//
//        foreach ($attributes as $attribute) {
//            /** @var $attribute Mage_Customer_Model_Attribute */
//            $attribute->setStoreId(Mage::getSingleton('adminhtml/session_quote')->getStoreId());
//            $inputType = $attribute->getFrontend()->getInputType();
//
//            if ($inputType) {
//                $element = $form->addField($attribute->getAttributeCode(), $inputType, array(
//                    'name'      => $attribute->getAttributeCode(),
//                    'label'     => $this->__($attribute->getStoreLabel()),
//                    'class'     => $attribute->getFrontend()->getClass(),
//                    'required'  => $attribute->getIsRequired(),
//                ));
//                if ($inputType == 'multiline') {
//                    $element->setLineCount($attribute->getMultilineCount());
//                }
//                $element->setEntityAttribute($attribute);
//                $this->_addAdditionalFormElementData($element);
//
//                if (!empty($renderers[$attribute->getAttributeCode()])) {
//                    $element->setRenderer($renderers[$attribute->getAttributeCode()]);
//                }
//
//                if ($inputType == 'select' || $inputType == 'multiselect') {
//                    $element->setValues($attribute->getFrontend()->getSelectOptions());
//                } else if ($inputType == 'date') {
//                    $format = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
//                    $element->setImage($this->getSkinUrl('images/grid-cal.gif'));
//                    $element->setFormat($format);
//                }
//            }
//        }
//
//        return $this;
//    }

    /**
     * Return array of additional form element types by type
     *
     * @see Mage_Adminhtml_Block_Sales_Order_Create_Form_Abstract
     * @return array
     */
    protected function _getAdditionalFormElementTypes()
    {
        return array(
            'file'      => Mage::getConfig()->getBlockClassName('adminhtml/customer_form_element_file'),
            'image'     => Mage::getConfig()->getBlockClassName('adminhtml/customer_form_element_image'),
            'boolean'   => Mage::getConfig()->getBlockClassName('adminhtml/customer_form_element_boolean'),
        );
    }

    protected function _getCustomerDefaultAddressAttributes()
    {
        $defaultAddressAttributes = array();

        $defaultAddressAttributes[] =
            Mage::getModel('eav/entity_attribute')->loadByCode('customer', 'default_billing')
                ->setIsVisible(true);
                //->setFrontendInput()
        $defaultAddressAttributes[] =
            Mage::getModel('eav/entity_attribute')->loadByCode('customer', 'default_shipping')
                ->setIsVisible(true);

        return $defaultAddressAttributes;
    }

}