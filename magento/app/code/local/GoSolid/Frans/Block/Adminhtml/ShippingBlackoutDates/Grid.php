<?php
class GoSolid_Frans_Block_Adminhtml_ShippingBlackoutDates_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{

		parent::__construct();
		$this->setId('fransGrid');
		
		$this->setDefaultSort('blackout_date');
		$this->setDefaultDir('DESC');

        // only show from today onwards
        $this->setDefaultFilter( array(
                'blackout_date' => array(
                    'from'=> Mage::getModel('core/date')->date('Y-m-d'),
                    'orig_from' => Mage::getModel('core/date')->date('m/d/Y'),
                    'date' => true
                )
            )

        );

	}
 
	protected function _prepareCollection()
	{
		$collection = Mage::getModel('frans/shippingBlackoutDate')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}	
		
		
	protected function _prepareColumns()
  	{

  		$this->addColumn('id', array(
			  'header'    => Mage::helper('frans')->__('ID'),
			  'align'     =>'right',
			  'width'     => '50px',
			  'index'     => 'id',
		  ));
		  
		$this->addColumn('blackout_date', array(
            'header'    => Mage::helper('frans')->__('Date'),
            'type'      => 'date',
            'align'     => 'left',
            'index'     => 'blackout_date',
            'filter'    => 'frans/widget_grid_column_filter_date',
        ));
		
		  return parent::_prepareColumns();
	}
	
	public function getRowUrl($row)
  	{
    	return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  	}
}