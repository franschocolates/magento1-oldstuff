<?php
 
class GoSolid_Frans_Block_Adminhtml_ProductPiece extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {

	$this->_controller = 'adminhtml_productPiece';
	$this->_blockGroup = 'frans';
	$this->_headerText = Mage::helper('frans')->__("Manage Product Pieces");
	$this->_addButtonLabel = Mage::helper('frans')->__("Add New Product Piece");
	
    parent::__construct();
    
  }
}