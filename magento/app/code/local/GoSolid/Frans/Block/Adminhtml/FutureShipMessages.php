<?php

class GoSolid_Frans_Block_Adminhtml_FutureShipMessages extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {

	$this->_controller = 'adminhtml_futureShipMessages';
	$this->_blockGroup = 'frans';
	$this->_headerText = Mage::helper('frans')->__("Future Ship Messages");
	$this->_addButtonLabel = Mage::helper('frans')->__("Add Message");

    parent::__construct();

  }
}