<?php
 
class GoSolid_Frans_Block_Adminhtml_Quotesgrid extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {

        $this->_controller = 'adminhtml_quotesgrid';
        $this->_blockGroup = 'frans';
        $this->_headerText = Mage::helper('frans')->__("Manage Quotes");
        $this->_addButtonLabel = Mage::helper('frans')->__("Create Quote");
	
    parent::__construct();

      $this->_addButton('add', array(
          'label'     => $this->getAddButtonLabel(),
          'onclick'   => 'setLocation(\'' . $this->getUrl('adminhtml/sales_order_create') .'\')',
          'class'     => 'add',
      ));
  }
}