<?php

/**
 * 
 * Handles accounting fields when creating new orders
 * Adopted/Migrated from Engine Accounting module
 * @author Barrett
 *
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Create_Form_Accounting 
		extends Mage_Adminhtml_Block_Sales_Order_Create_Form_Abstract{

    public function getHeaderCssClass()
    {
        return 'head-account';
    }

    public function getHeaderText()
    {
        return Mage::helper('sales')->__('Accounting');
    }

    protected function _prepareForm()
    {
        $fieldset = $this->_form->addFieldset('main', array());
        

        $originations = Mage::getModel('frans/accounting_origination')->getCollection()->toOptionArray('origination_name', 'origination_name');
        $fieldset->addField('origination', 'select',
            array(
                'name'  => 'origination',
                'label' => Mage::helper('accounting')->__('Origination'),
                'id'    => 'origination',
                'class' => 'required-entry',
                'required' => true,
                'values' => $originations
            )
        );

        $labels = Mage::getModel('frans/accounting_label')->getCollection()->toOptionArray('label_name', 'label_name');
        $fieldset->addField('accounting_label', 'select',
            array(
                'name'  => 'accounting_label',
                'label' => Mage::helper('accounting')->__('Accounting Label'),
                'id'    => 'accounting-label',
                'class' => 'required-entry',
                'required' => true,
                'values' => $labels
            )
        );

        $this->_form->addFieldNameSuffix('order');
        $this->_form->setValues($this->getFormValues());

        return $this;
    }

    protected function _addAdditionalFormElementData(Varien_Data_Form_Element_Abstract $element)
    {
        // switch ($element->getId()) {
        //     case 'email':
        //         $element->setRequired(0);
        //         $element->setClass('validate-email');
        //         break;
        // }
        return $this;
    }

    public function getFormValues()
    {
        $quote = $this->getQuote();
        $data = $quote->getData();

        $adminUser = Mage::getSingleton('admin/session')->getUser();
        
        if(!isset($data['origination'])):
            if($adminUser->getDefaultOrigination())
                $data['origination'] = $adminUser->getDefaultOrigination();
        endif;

        if(!isset($data['accounting_label'])):
            // a customer group > 1 would imply a business customer
            if(isset($data['customer_group_id']) && $data['customer_group_id'] > 1):
                    $accounting_label = $adminUser->getDefaultOrigination();

            // nab the default from the admin user.
            else:
                    $accounting_label = $adminUser->getDefaultOrigination();
            endif;

            if(isset($accounting_label))
                $data['accounting_label'] = $accounting_label;

        endif;

        return $data;
    }
    
}