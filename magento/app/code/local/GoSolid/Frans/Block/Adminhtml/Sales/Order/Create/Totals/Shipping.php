<?php
/**
 * Rewritten to allow us to pull the total shipping in the case of multishipping
 */ 
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Create_Totals_Shipping
    extends Mage_Adminhtml_Block_Sales_Order_Create_Totals_Shipping
{
    /**
     * Get shipping amount exclude tax
     * Overridden to use the actual calculated total
     * NOTE: this is only excluding tax; if we start doing tax, need to override that, too
     *
     *
     * @return float
     */
    public function getShippingExcludeTax()
    {
        if ($this->getTotal()->getAddress()->getQuote()->getIsMultiShipping())
        {
            // total should be populated on our total object
            return $this->getTotal()->getValue(); //->getQuote()->getShippingAmount();
        }

        return parent::getShippingExcludeTax();
    }

}