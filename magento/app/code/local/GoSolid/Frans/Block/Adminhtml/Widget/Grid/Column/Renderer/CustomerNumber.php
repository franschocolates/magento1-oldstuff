<?php

class GoSolid_Frans_Block_Adminhtml_Widget_Grid_Column_Renderer_CustomerNumber
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Options
{

    public function render(Varien_Object $row)
    {
        $data = parent::render($row);
        $customer_id = $row->getCustomerId();
        Mage::log($customer_id);
        Mage::log($data);
        if($data == 'Pay By Invoice' && $customer_id){
            $customer_number = Mage::getModel('customer/customer')->load($customer_id)->getCustomerNumber();
            $data .= ' (' . $customer_number . ')';
        }

        return $data;
    }
}
