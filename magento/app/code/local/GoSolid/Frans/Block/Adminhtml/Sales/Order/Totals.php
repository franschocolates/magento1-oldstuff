<?php

class GoSolid_Frans_Block_Adminhtml_Sales_Order_Totals  extends Mage_Adminhtml_Block_Sales_Order_Totals
{
    protected function _initTotals()
    {
        parent::_initTotals();
        
        # if we're a child order, we don't want to show the paid/refunded/due
       	if ($this->getOrder()->getIsMultishipChildOrder())
       	{
       		$this->setShowMultishipMessage(true);
       		unset($this->_totals['paid']);
       		unset($this->_totals['refunded']);
			unset($this->_totals['due']);
       	}
        else if ($this->getOrder()->getGiftcardAmount() > 0)
        {
            // without the shipping totals, the giftcard totals don't make much sense.
            $this->_totals['shipping'] = new Varien_Object(array(
                'code'      => 'shipping',
                'strong'    => false,
                'value'     => $this->getSource()->getShippingAmount(),
                'base_value'=> $this->getSource()->getBaseShippingAmount(),
                'label'     => $this->helper('frans')->__('Shipping'),
            ));

            // note that we use a negative to indicate the giftcard was a deduction
            $this->_totals['giftcard'] = new Varien_Object(array(
                'code'      => 'giftcard',
                'strong'    => false,
                'value'     => - $this->getSource()->getGiftcardAmount(),
                'base_value'=> - $this->getSource()->getBaseGiftcardAmount(),
                'label'     => $this->helper('frans')->__('Gift Card') . " (#{$this->getOrder()->getGiftcardNumber()})",
            ));
        }

        return $this;
    }
	
}