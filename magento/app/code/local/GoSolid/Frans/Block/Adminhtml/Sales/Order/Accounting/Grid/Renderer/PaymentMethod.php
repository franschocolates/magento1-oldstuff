<?php
/**
 * Handles rendering the payment method
 * Special logic needs customer number for invoice orders
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Accounting_Grid_Renderer_PaymentMethod
        extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Text
{
    public function render(Varien_Object $row)
    {
        if ($row->getMethod() == 'paybyinvoice')
        {
            // find customer number for this customer
            $methodDisplay = $row->getData('payment_method_title');


            if ($customerId = Mage::getModel('sales/order')->load($row->getId())->getCustomerId())
            {
                $customer = Mage::getModel('customer/customer')
                            ->load($customerId);

                if ($customer->getId() && ($customerNumber = $customer->getCustomerNumber()))
                {
                    return "$methodDisplay [#{$customerNumber}]";
                }
            }

        }

        return parent::render($row);
    }
}