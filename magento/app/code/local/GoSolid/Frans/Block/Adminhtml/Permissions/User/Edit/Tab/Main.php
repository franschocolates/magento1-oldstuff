<?php

class GoSolid_Frans_Block_Adminhtml_Permissions_User_Edit_Tab_Main extends Mage_Adminhtml_Block_Permissions_User_Edit_Tab_Main
{

    protected function _prepareForm()
    {
        $parent = parent::_prepareForm();
        $form = $parent->getForm();
        $fieldset = $form->getElement('base_fieldset');

        $userModel = Mage::registry('permissions_user');
        
        $origination = ($userModel->hasData('default_origination')) ?
        					$userModel->getDefaultOrigination()
        					: Mage::getModel('frans/accounting_origination')->getDefaultAdminOrigination();
        
		// system was built to use origination as a string throughout
		// so we just use the name for both id/value
        $originations = Mage::getModel('frans/accounting_origination')
        						->getCollection()
        						->toOptionArray('origination_name', 'origination_name');

        $fieldset->addField('default_origination', 'select', array(
                'name'  => 'default_origination',
                'label' => Mage::helper('adminhtml')->__('Default Origination'),
                'id'    => 'default_origination',
                'title' => Mage::helper('adminhtml')->__('Default Origination'),
                'class' => 'input-select',
                'required' => true,
                'values'    => $originations,
        		'value'		=> $origination
            ));

        return $parent;
    }
}
