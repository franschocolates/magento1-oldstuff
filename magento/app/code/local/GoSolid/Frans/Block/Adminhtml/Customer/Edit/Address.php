<?php
/**
 * Created to support editing of addresses
 */
class GoSolid_Frans_Block_Adminhtml_Customer_Edit_Address
    //extends Mage_Adminhtml_Block_Widget_Form
    extends Mage_Adminhtml_Block_Widget_Form_Container
{
    protected $_objectId = 'address_id';
    protected $_controller = 'adminhtml_customer';
    protected $_mode = 'edit_address';
    protected $_blockGroup = 'frans';

    public function __construct()
    {
        parent::_construct();

        $this->_removeButton('back')
             ->_removeButton('reset')
             ->_removeButton('save');

        $objId = $this->getRequest()->getParam($this->_objectId);

        if (! empty($objId)) {
            $this->_addButton('delete', array(
                'label'     => Mage::helper('frans')->__('Delete'),
                'class'     => 'delete',
                'onclick'   => "customer_info_tabsJsTabs.confirmDeleteAddress('{$this->getDeleteUrl()}')"
            ), 3);
        }

        $this->_addButton('save', array(
            'label'     => Mage::helper('frans')->__('Save Address'),
            // urgh, some hardcoding here of IDs
            'onclick'   => "customer_info_tabsJsTabs.saveCustomerAddress('{$this->getFormActionUrl()}', $('address_fieldset'))",
            'class'     => 'save',
        ), 1);

        $cancelParams = array(
            'label'     => Mage::helper('frans')->__('Cancel'),
            'onclick'   => $this->getAddressGridJsObject() . '.cancelAddressEdit()',
            'class'     => 'cancel',
        );

        $this->_addButton('cancel', $cancelParams, 2);

    }

    public function getAddressGridJsObject()
    {
        // should really not hardcode this...
        return 'customer_addresses_gridJsObject';
    }

    /**
     * @return Mage_Customer_Model_Customer
     */
    public function getCustomer()
    {
        return Mage::registry('current_customer');
    }

    public function getCustomerId()
    {
        return $this->getCustomer()->getId();
    }

    public function getAddress()
    {
        return Mage::registry('current_customer_address');
    }

    public function getLegend()
    {
        $operation = ($this->getAddress()->getId()) ? 'Edit' : 'Add';

        return "$operation Address";
    }

    public function getFormActionUrl()
    {
        $params = array('id' => $this->getCustomerId());

        if ($addressId = $this->getAddress()->getId())
        {
            $params['address_id'] = $addressId;
        }
        return $this->getUrl('*/*/addressSave', $params);
    }

    public function getDeleteUrl()
    {
        return $this->getUrl('*/*/addressDelete', array('_current' => true));
    }

    /**
     * Adapted from Core
     *
     * @see Mage_Adminhtml_Block_Customer_Edit_Tab_Addresses
     * @return mixed
     */
    public function isReadonly()
    {
        return $this->getCustomer()->isReadonly();
    }

    /**
     * Return customer address form instance
     * (originally from Core Edit Order address
     *
     * @see Mage_Adminhtml_Block_Sales_Order_Create_Form_Address
     * @return Mage_Customer_Model_Form
     */
    protected function _getAddressForm()
    {
        if (is_null($this->_addressForm)) {
            $this->_addressForm = Mage::getModel('customer/form')
                ->setFormCode('adminhtml_customer_address')
                ->setStore($this->getStore());
        }
        return $this->_addressForm;
    }

}