<?php

class GoSolid_Frans_Block_Adminhtml_Accounting_Label_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

	protected function _prepareForm()
	{

		$form = new Varien_Data_Form(); 
		$this->setForm($form);
		$fieldset = $form->addFieldset('frans_fs', array('legend'=>Mage::helper('frans')->__('Accounting Label')));
	
		$fieldset->addField('label_name', 'text', array(
	          'title'     => Mage::helper('frans')->__('Label Name'),
			  'label'     => Mage::helper('frans')->__('Label Name'),
	          'name'      => 'label_name',
			  'class'     => 'required-entry',
	          'required'  => true,
			  'note'	  => 'Name of accounting label',
	      	));
				
		$fieldset->addField('accpac_customer_id', 'text', array(
	          'title'     => Mage::helper('frans')->__('AccPac Customer Id'),
			  'label'     => Mage::helper('frans')->__('AccPacc Customer Id'),
	          'name'      => 'accpac_customer_id',
			  'class'     => 'required-entry',
	          'required'  => true,
			  'note'	  => 'Customer ID',
	      	));

		$originationCode = Mage::getModel('frans/accounting_origination')->getCollection()->toGridOptionArray('origination_name', 'origination_name');

		$fieldset->addField('accpac_location_code', 'select', array(
			'title'     => Mage::helper('frans')->__('AccPac Origination Code'),
			'label'     => Mage::helper('frans')->__('AccPacc Origination Code'),
			'name'      => 'accpac_location_code',
			'class'     => 'required-entry',
			'required'  => true,
			'options'	=> $originationCode,
			'note'	  => 'Origination (Location) Code',
		));

		$freightCode = Mage::getModel('frans/accounting_freight')->getCollection()->toGridOptionArray('freight_code', 'freight_code');

		$fieldset->addField('accpac_freight_code', 'select', array(
			'title'     => Mage::helper('frans')->__('AccPac Freight Code'),
			'label'     => Mage::helper('frans')->__('AccPacc Freight Code'),
			'name'      => 'accpac_freight_code',
			'class'     => 'required-entry',
			'options'	=> $freightCode,
			'required'  => true,
			'note'	  => 'Freight Code',
		));
	
		if ( Mage::getSingleton('adminhtml/session')->getAccountingLabelData() )
		{
	    	$form->setValues(Mage::getSingleton('adminhtml/session')->getAccountingLabelData());
	    	Mage::getSingleton('adminhtml/session')->getAccountingLabelData(null);
		}
		elseif(Mage::registry('accountingLabel_data'))
		{
	    	$form->setValues(Mage::registry('accountingLabel_data')->getData());
		}
		
		return parent::_prepareForm();
	}
  
}