<?php
/**
 * Created by goSolid.
 * Date: 8/4/14
 * Time: 4:33 PM
 */ 
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Create_Form_Account extends Mage_Adminhtml_Block_Sales_Order_Create_Form_Account {

	public function getHeaderText()
	{
		return Mage::helper('sales')->__('Account Basics');
	}

    /**
     * Prepare Form and add elements to form
     *
     * @return Mage_Adminhtml_Block_Sales_Order_Create_Form_Account
     */
    protected function _prepareForm()
    {

        /* @var $customerModel Mage_Customer_Model_Customer */
        $customerModel = Mage::getModel('customer/customer');

        /* @var $customerForm Mage_Customer_Model_Form */
        $customerForm   = Mage::getModel('customer/form');
        $customerForm->setFormCode('adminhtml_checkout')
            ->setStore($this->getStore())
            ->setEntity($customerModel);

        // prepare customer attributes to show
        $attributes     = array();

        // add system required attributes
        foreach ($customerForm->getSystemAttributes() as $attribute) {
            /* @var $attribute Mage_Customer_Model_Attribute */
            if ($attribute->getIsRequired()) {
                $attributes[$attribute->getAttributeCode()] = $attribute;
            }
        }
        
        // add user defined attributes
        foreach ($customerForm->getUserAttributes() as $attribute) {
            /* @var $attribute Mage_Customer_Model_Attribute */
            $attributes[$attribute->getAttributeCode()] = $attribute;
        }

        $fieldset = $this->_form->addFieldset('main', array());

        $this->_addAttributesToForm($attributes, $fieldset);

        $formValues = $this->getFormValues();

        if(isset($formValues['telephone']) && array_key_exists('telephone',$formValues)) {
            $fieldset->addField('telephone', 'text', array(
                'name' => 'telephone',
                'label' => 'Phone',
                'class' => 'input-text',
                'readonly' => 'readonly',
            ));
        }

        $this->_form->addFieldNameSuffix('order[account]');
        $this->_form->setValues($formValues);

        return $this;
    }
}