<?php

class GoSolid_Frans_Block_Adminhtml_Sales_Order_View_Info extends Mage_Adminhtml_Block_Sales_Order_View_Info{

    public function getCustomerAccountData(){
        $accountData = parent::getCustomerAccountData();
        // these keys pertain to sort order. 300+ puts it at the end.
        $accountData[300] = array('label' => Mage::helper('frans')->__('Accounting Label'),
                                    'value' => $this->getOrder()->getAccountingLabel());
        $accountData[301] = array('label' => Mage::helper('frans')->__('Origination'),
                                    'value' => $this->getOrder()->getOrigination());

        // TODO: this is largely for debugging purposes.
        // Ship Date instead needs to live in an updateable field.
        $accountData[303] = array('label' => Mage::helper('frans')->__('Bill Date'),
                                    'value' => $this->getOrder()->getBillDate());

        return $accountData;
    }

    public function getSaveAdminNotesButtonHtml()
    {
        $onclick = "submitAreaInBackground('admin_notes_fields', '{$this->getSaveNotesUrl()}')";

        return $this->getLayout()
            ->createBlock('adminhtml/widget_button')
            ->setData(array(
                'label'   => Mage::helper('sales')->__('Save Notes'),
                'onclick' => $onclick,
                'name'  => 'notes_button',
                'id'    => 'notes_button',
                'class' => 'form-button scalable save',
                'value' => 'Save'
            ))
            ->toHtml();

    }

    public function getSaveNotesUrl()
    {
        return $this->getUrl("adminhtml/sales_order/saveAdminNotes", array( '_current' => true));
    }

    public function getSaveQueueButtonHtml()
    {
        $onclick = "submitAreaInBackground('order_queue', '{$this->getSaveQueueUrl()}')";

        return $this->getLayout()
            ->createBlock('adminhtml/widget_button')
            ->setData(array(
                'label'   => Mage::helper('sales')->__('Save Queue'),
                'onclick' => $onclick,
                'name'  => 'queue_button',
                'id'    => 'queue_button',
                'class' => 'form-button scalable save',
                'value' => 'Save'
            ))
            ->toHtml();

    }

    public function getSaveQueueUrl()
    {
        return $this->getUrl("adminhtml/orderqueues/saveQueue", array( '_current' => true));
    }


}