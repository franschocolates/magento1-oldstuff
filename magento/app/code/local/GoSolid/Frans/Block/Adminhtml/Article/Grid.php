<?php
class GoSolid_Frans_Block_Adminhtml_Article_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{

		parent::__construct();
		$this->setId('fransGrid');
		
		//TODO: Set default sort
		//$this->setDefaultSort('id');
		//$this->setDefaultDir('ASC');
		
		//TODO: Optional
		//$this->setSaveParametersInSession(true);
		//$this->setDefaultFilter(array('status' => 'Enabled'));
	}
 
	protected function _prepareCollection()
	{
		$collection = Mage::getModel('frans/article')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}	
		
		
	protected function _prepareColumns()
  	{

  		$this->addColumn('title', array(
			  'header'    => Mage::helper('frans')->__('Title'),
			  'align'     =>'left',
			  'index'     => 'title',
		  ));
		  
		  $this->addColumn('sub_title', array(
			  'header'    => Mage::helper('frans')->__('Sub Title'),
			  'align'     =>'left',
			  'index'     => 'sub_title',
		  ));
		  
		  $this->addColumn('date', array(
			  'header'    => Mage::helper('frans')->__('Date'),
			  'align'     =>'left',
			  'width'     => '50px',
			  'index'     => 'date',
		  	  'type'      => 'date',
		  ));
		  
		  $this->addColumn('status', array(
			  'header'    => Mage::helper('frans')->__('Status'),
			  'align'     => 'left',
			  'width'     => '150px',
			  'index'     => 'status',
			  'type'	  => 'options',
			  'options'   => array(
				  '1' => 'Enabled',
				  '0' => 'Disabled',
			  ),
		  ));
		  
		  return parent::_prepareColumns();
	}
	
	public function getRowUrl($row)
  	{
    	return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  	}
}