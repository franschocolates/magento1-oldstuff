<?php

class GoSolid_Frans_Block_Adminhtml_Sales_Order_Creditmemo_Create_Items
		extends Mage_Adminhtml_Block_Sales_Order_Creditmemo_Create_Items
{
    public function __construct()
    {
        parent::__construct();
    }

    public function setIsMultishipView($value)
    {
    	if ($value === true)
    	{
    		# need to change which template we show
        	$this->setTemplate('sales/order/creditmemo/create/multishipitems.phtml');
    	}
    	$this->setData('is_multiship_view', $value);
    }
    
    public function getOrdersAndItems()
    {
    	$allInvoiceItems = $this->getCreditmemo()->getAllItems();
    	
    	# go through and divide them up based on which order they are in.
    	$ordersAndItems = array();
    	
    	foreach($allInvoiceItems as $invoiceItem)
    	{
    		# we build an array with the order ID as the key and an
    		# array as the value. the array has a key of 'order' and 'items'
    		# we do this because we need to pass both the order and the items
    		# and we can't use objects as keys
    		$orderItem = $invoiceItem->getOrderItem();
    		$childOrderId = $orderItem->getOrderId();
    		
    		if (isset($ordersAndItems[$childOrderId]))
    		{
    			$invoiceItems = $ordersAndItems[$childOrderId]['items'];
    		}
    		else
    		{
    			$childOrder = $this->_getChildOrder($childOrderId);
    			$invoiceItems = array();
    			$ordersAndItems[$childOrderId] = array(
    				'order' => $childOrder,
    				'items' => $invoiceItems
    			);
    		}
    		
    		$invoiceItems[] = $invoiceItem;
    		
    		$ordersAndItems[$childOrderId]['items'] = $invoiceItems;
    	}
    	
    	return $ordersAndItems;
    }
    
    public function getChildOrderUrl($childOrderId)
    {
    	return $this->getUrl('*/sales_order/view', array('order_id'=>$childOrderId));
    }
    
    protected function _beforeToHtml()
    {
    	if ($this->getOrder()->getIsMultishipParent())
    	{
    		$this->setIsMultishipView(true);
    	}
    }
    
    private function _getChildOrder($orderId)
    {
    	$childOrders = $this->getOrder()->getChildOrders();
    	
    	foreach ($childOrders as $childOrder)
    	{
    		if ($childOrder->getId() == $orderId)
    		{
    			return $childOrder;
    		}
    	}
    }
}