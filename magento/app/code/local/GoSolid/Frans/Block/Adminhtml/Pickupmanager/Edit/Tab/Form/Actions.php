<?php
/**
 * Handles logic for determining actions
 */
class GoSolid_Frans_Block_Adminhtml_Pickupmanager_Edit_Tab_Form_Actions
        extends Mage_Adminhtml_Block_Template
{
    public function canSetReadyForPickup()
    {
        // check both order state and order status, also confirm it's not in a status of ready for pickup or picked up
        return ($this->getOrder()->getState() == GoSolid_Frans_Model_Sales_Order::STATE_PROCESSING
            || $this->getOrder()->getStatus() == GoSolid_Frans_Model_Sales_Order::STATE_PROCESSING)
	        && $this->getOrder()->getStatus() != GoSolid_Frans_Model_Sales_Order::STATE_READY_FOR_PICKUP
	        && $this->getOrder()->getStatus() != GoSolid_Frans_Model_Sales_Order::STATE_PICKED_UP;
    }
}