<?php
/**
 * Block for showing xcart orders for a specific customer
 *
 */
class GoSolid_Frans_Block_Adminhtml_Customer_Edit_Tab_XcartOrders
    extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('customer_xcart_orders_grid');
        $this->setDefaultSort('created_at', 'desc');
        $this->setUseAjax(true);
        $this->setRowClickCallback('openExternalGridRow'); // need this to open in a tab
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('frans/xcartOrder')
            ->getCollection()
            ->addFieldToFilter('customer_email', Mage::registry('current_customer')->getEmail());

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('orderid', array(
            'header'    => Mage::helper('frans')->__('ID'),
            'align'     =>'right',
            'width'     => '50px',
            'index'     => 'orderid',
        ));

        $this->addColumn('order_number', array(
            'header'    => Mage::helper('frans')->__('Order #'),
            'align'     =>'right',
            'width'     => '50px',
            'index'     => 'order_number',
        ));

        $this->addColumn('created_at', array(
            'header'    => Mage::helper('frans')->__('Purchase On'),
            'index'     => 'created_at',
            'type'      => 'datetime',
        ));

        $this->addColumn('billing_name', array(
            'header'    => Mage::helper('frans')->__('Bill to Name'),
            'align'     =>'left',
            'index'     => 'billing_name',
        ));

        $this->addColumn('total', array(
            'header' => Mage::helper('sales')->__('Total'),
            'index' => 'total',
            'type'  => 'currency',
            'currency' => 'USD',
        ));

        $this->addColumn('email', array(
            'header'    => Mage::helper('frans')->__('Order Email'),
            'align'     =>'left',
            'index'     => 'email',
            'type'      => 'text'
        ));

        $this->addColumn('order_status', array(
            'header'    => Mage::helper('frans')->__('Status'),
            'align'     =>'left',
            'index'     => 'order_status',
            'type'      => 'text'
        ));

        $this->addColumn('shipping_name', array(
            'header'    => Mage::helper('frans')->__('Ship to Name'),
            'align'     =>'left',
            'index'     => 'shipping_name',
            'type'      => 'text'
        ));

        $this->addColumn('sku_list', array(
            'header' => Mage::helper('frans')->__('SKUs'),
            'width'  => '80px',
            'type'   => 'text',
            'index'  => 'sku_list',
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/xcartOrders', array('_current' => true));
    }

    public function getRowUrl($item)
    {
        return Mage::helper('frans/xcart')->getOrderSearchUrl($item->getOrderNumber());
    }

    public function getRowClass($item)
    {
        return 'external';
    }
}
