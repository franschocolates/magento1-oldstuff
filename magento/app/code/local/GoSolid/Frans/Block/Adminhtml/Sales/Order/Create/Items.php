<?php

class GoSolid_Frans_Block_Adminhtml_Sales_Order_Create_Items extends Mage_Adminhtml_Block_Sales_Order_Create_Items {

	public function getHeaderText()
	{
        /* Frans wants the block title to be Item Summary on PDF print views */
        if (strpos(Mage::helper('core/url')->getCurrentUrl(),'printView')){
            return Mage::helper('sales')->__('Item Summary');
        } else {
            return Mage::helper('sales')->__('Item Options');
        }
	}

}