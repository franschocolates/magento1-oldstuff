<?php
 
class GoSolid_Frans_Block_Adminhtml_ShippingBlackoutDates extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {

	$this->_controller = 'adminhtml_shippingBlackoutDates';
	$this->_blockGroup = 'frans';
	$this->_headerText = Mage::helper('frans')->__("Shipping Blackout Dates");
	$this->_addButtonLabel = Mage::helper('frans')->__("Add Blackout Date");
	
    parent::__construct();
  }
}