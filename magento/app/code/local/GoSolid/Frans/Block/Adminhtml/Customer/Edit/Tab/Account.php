<?php
/**
 * Rewritten to handle special logic when creating new accounts
 */ 
class GoSolid_Frans_Block_Adminhtml_Customer_Edit_Tab_Account
    extends Mage_Adminhtml_Block_Customer_Edit_Tab_Account
{
    /**
     * Initialize form
     *  Overridden
     *
     * @return Mage_Adminhtml_Block_Customer_Edit_Tab_Account
     */
    public function initForm()
    {
        $customer = Mage::registry('current_customer');

        // if no customer yet, set to default website (not admin)
        if (!$customer->getId())
        {
            $websites = Mage::app()->getWebsites();
            // website id is the key.
            $customer->setWebsiteId(key($websites));
        }

        return parent::initForm();
    }

	public function _toHtml()
	{
		// remove prefix and suffix fields from form
		foreach($this->getForm()->getElements() as $fieldset)
		{
			$fieldset->removeField('prefix');
            $fieldset->removeField('middlename');
			$fieldset->removeField('suffix');
		}
		return parent::_toHtml();
	}
}