<?php

/**
 * Adminhtml sales orders grid
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Grid extends Mage_Adminhtml_Block_Sales_Order_Grid
{
    const PARENT_ORDER_YES = 1;
    const PARENT_ORDER_NO = 2; // can't use 0, it is falsey (like the no filter option)

	protected function _prepareColumns()
    {
        parent::_prepareColumns();
        
        # change the order ID to be wide enough for our increment ID
        $this->_columns['real_order_id']['width'] = '100px';

        $parentOrderOptions = array(
            '' => $this->__('Any')
            , self::PARENT_ORDER_YES => $this->__('Yes')
            , self::PARENT_ORDER_NO => $this->__('No' ));

        $queueOptions = Mage::getSingleton('orderQueues/orderQueue')->__getOrderGridOptionArray();
        asort($queueOptions);
        $temp = array('na' => $queueOptions['na']);
        unset($queueOptions['na']);
        $queueOptions = $temp + $queueOptions;


        $this->addColumnAfter('is_parent_order', array(
            'header' => $this->__('Parent Order'),
            'type'  => 'options',
            'index' => 'increment_id',
            'options' => $parentOrderOptions,
            'renderer' => 'GoSolid_Frans_Block_Adminhtml_Sales_Order_Grid_Renderer_ParentOrder',
            'filter_condition_callback' => array($this, '_filterParentOrder'),
        ), 'real_order_id');

        // fix the ordering issues
        $this->addColumnsOrder('created_at', 'is_parent_order');

        $this->addColumnAfter('is_on_hold', array(
            'header' => Mage::helper('frans')->__('On Hold'),
            'type'   => 'options',
            'index'  => 'is_on_hold',
            'align' => 'center',
            'renderer' => 'GoSolid_Frans_Block_Adminhtml_Sales_Order_Grid_Renderer_OnHold',
            'options' => array( 0 => 'No', 1 => 'Yes'),
        ), 'status');

        $this->addColumnAfter('shipping_city_state', array(
            'header' => Mage::helper('frans')->__('Destination'),
            'index'  => 'shipping_city_state',
        	'renderer'  => 'GoSolid_Frans_Block_Adminhtml_Sales_Order_Grid_Renderer_ConditionalMultishipText',
        ), 'shipping_name');

        $this->addColumnAfter('shipping_score', array(
            'header' => Mage::helper('frans')->__('Score'),
            'index'  => 'shipping_score',
            'type' => 'number',
            'width' => '60px'
        ), 'shipping_city_state');

        $this->addColumnAfter('shipping_description', array(
            'header' => Mage::helper('frans')->__('Shipping'),
            'index'  => 'shipping_description',
        	'renderer'  => 'GoSolid_Frans_Block_Adminhtml_Sales_Order_Grid_Renderer_ConditionalMultishipText',
        ), 'shipping_score');

        $this->addColumnAfter('planned_ship_date', array(
            'header' => Mage::helper('frans')->__('Planned Ship Date'),
            'index'  => 'planned_ship_date',
            'type'   => 'date',
        	'renderer'  => 'GoSolid_Frans_Block_Adminhtml_Sales_Order_Grid_Renderer_ConditionalMultishipDate',
        	'filter_condition_callback' => array($this, '_filterShipDate'),
            ), 'shipping_description');

        /** Temporarily disabling for ticket Ticket #1032: Admin Sales Order is Slow
        $this->addColumnAfter('sku_list', array(
            'header' => Mage::helper('frans')->__('SKUs'),
            'width'  => '80px',
            'type'   => 'text',
            'index'  => 'sku_list',
            'renderer'  => 'GoSolid_Frans_Block_Adminhtml_Sales_Order_Grid_Renderer_ConditionalMultishipText',
        ), 'ship_date');
         **/

        $this->addColumnAfter('accounting_label', array(
            'header' => Mage::helper('frans')->__('Label'),
            'width'  => '80px',
            'type'   => 'options',
            'options' => Mage::getModel('frans/accounting_label')->getCollection()->toGridOptionArray('label_name', 'label_name'),
            'index'  => 'accounting_label',
        ), 'status');

        $this->addColumnAfter('gift_message', array(
            'index'  => 'gift_message',
            'header' => Mage::helper('frans')->__('Gift Message'),
            'width'  => '65px',
            'type'   => 'options',
            'renderer'   => 'frans/adminhtml_widget_grid_column_renderer_hasValue',
            'has_value_label' => 'Has Gift Msg',
            'filter_condition_callback' => array($this, '_hasContentFilter'),
            'align'  => 'center',
            'options' => array( 0 => 'No', 1 => 'Yes'),
            ), 'origination');

        $this->addColumnAfter('special_instructions', array(
            'index'   => 'special_instructions',
            'header'  => Mage::helper('frans')->__('Special Instructions'),
            'width'   => '70px',
            'type'    => 'options',
            'align'   => 'center',
            'options' => array( 0 => 'No', 1 => 'Yes'),
            'renderer'   => 'frans/adminhtml_widget_grid_column_renderer_hasValue',
            'has_value_label' => 'Has Special Inst',
            'filter_condition_callback' => array($this, '_hasContentFilter'),
            ), 'gift_message');

        $this->addColumnAfter('admin_notes', array(
            'index'   => 'admin_notes',
            'header'  => Mage::helper('frans')->__('Admin Notes'),
            'width'   => '50px',
            'type'    => 'options',
            'align'   => 'center',
            'options' => array( 0 => 'No', 1 => 'Yes'),
            'renderer'   => 'frans/adminhtml_widget_grid_column_renderer_hasValue',
            'has_value_label' => 'Has Admin Notes',
            'filter_condition_callback' => array($this, '_hasContentFilter'),
        ), 'special_instructions');

        $this->addColumnAfter('processing_queue', array(
            'index'   => 'processing_queue',
            'header'  => Mage::helper('orderQueues')->__('Queue'),
            'width'   => '200px',
            'type'    => 'options',
            'align'   => 'center',
            'filter_condition_callback' => array($this, '_filterQueueNotAssigned'),
            'options' => $queueOptions,
            ),'admin_notes');

        # the Payment Captured is derived from whether the Total Paid is present
        # and then whether it matches the Grand Total (full payment)
        # if they don't match (but it's present), it's partial
        $this->addColumn('total_paid', array(
            'index'   => 'total_paid',
            'header'  => Mage::helper('sales')->__('Payment Captured?'),
            'width'   => '40px',
            'align'   => 'center',
        	'sortable' 	=> false,
        	'type' => 'options',
        	'renderer' => 'GoSolid_Frans_Block_Adminhtml_Sales_Order_Grid_Renderer_PaymentCaptured',
        	'filter_condition_callback' => array($this, '_filterPaymentCaptured'),
        	'options' => array( 'Y' => 'Yes', 'P' => 'Partial', 'N' => 'No'),
            ));

        // removing a couple of redundant columns.
        // 'base' grand total is really only relevant with multiple currencies
        // and action is supplanted since you can click on any row
        $this->removeColumn('shipping_name');
        $this->removeColumn('base_grand_total');
        $this->removeColumn('action');

        // we have to re-call sort or else our addColumnAfter has no effect
        // as the base grid sorts the columns in its prepareColumns
        $this->sortColumnsByOrder();

        return $this;
    }



    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('order_ids');
        $this->getMassactionBlock()->setUseSelectAll(false);

        $queueOptions = Mage::getSingleton('orderQueues/orderQueue')->__getOrderGridOptionArray();
        asort($queueOptions);
        $temp = array('na' => $queueOptions['na']);
        unset($queueOptions['na']);
        $queueOptions = $temp + $queueOptions;

        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/hold')) {
            $this->getMassactionBlock()->addItem('hold_order', array(
                 'label'=> Mage::helper('sales')->__('Hold'),
                 'url'  => $this->getUrl('*/sales_order/massHold'),
            ));
        }

        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/unhold')) {
            $this->getMassactionBlock()->addItem('unhold_order', array(
                 'label'=> Mage::helper('sales')->__('Unhold'),
                 'url'  => $this->getUrl('*/sales_order/massUnhold'),
            ));
        }

        // $this->getMassactionBlock()->addItem('capture_order', array(
        //         'label' => Mage::helper('accounting')->__('Capture'),
        //         'url'   => $this->getUrl('*/sales_order/massCapture')
        //     ));

        $this->getMassactionBlock()->addItem('receipt_picktickets', array(
                'label' => Mage::helper('sales')->__('Print Picking Tickets'),
                'url' => $this->getUrl('*/sales_order/massPrintPickingTickets')
            ));

        $this->getMassactionBlock()->addItem('receipt_packingslips', array(
                'label' => Mage::helper('sales')->__('Print Packing Slips'),
                'url' => $this->getUrl('*/sales_order/massPrintPackingSlips')
            ));

        $this->getMassactionBlock()->addItem('receipt_giftmessages', array(
                'label' => Mage::helper('sales')->__('Print Gift Messages'),
                'url' => $this->getUrl('*/sales_order/massPrintGiftMessages')
            ));

        $this->getMassactionBlock()->addItem('receipt_all', array(
                'label' => Mage::helper('sales')->__('Print All Receipts'),
                'url' => $this->getUrl('*/sales_order/massPrintAllReceipts')
            ));

        $this->getMassactionBlock()->addItem('change_ship_date', array(
            'label'=> Mage::helper('frans')->__('Change ship date'),
            'url'  => $this->getUrl('adminhtml/sales_order/massChangeShipDate'),
            'additional' => array(
                'ship_date' => array(
                    'name' => 'ship_date',
                    'type' => 'date',
                    'class' => 'required-entry',
                    'label' => Mage::helper('frans')->__('New Ship Date'),
                    'format' => Mage::app()->getLocale() ->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
                    'image' => $this->getSkinUrl('images/grid-cal.gif'),
                )
            )
        ));


        $this->getMassactionBlock()->addItem('change_queues', array(
            'label'=> Mage::helper('frans')->__('Update Queues'),
            'url'  => $this->getUrl('adminhtml/orderqueues/massChangeQueue'),
            'additional' => array(
                'processing_queue' => array(
                    'name' => 'queue',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('frans')->__('Queue'),
                    'options' => $queueOptions,
                )
            )
        ));

        //add labels
        $statuses = Mage::getModel("sales/order_status")->getCollection()->toOptionArray();
        foreach($statuses AS $key => $value)
        {
            $actions[] = array("status" => $value["value"], "label" => $value["label"]);
        }

        //get the orders allowed actions.. and add those buttons.
        $options = array();
        foreach($actions as $action)
        {
           $options[ $action["status"]] =  $action["label"];
        }

        if(count($options) > 0)
        {
            //add additional actions..
            $this->getMassactionBlock()->addItem('status_update', array(
                'label' => Mage::helper('sales')->__("Change Status"),
                'url' => $this->getUrl('*/sales_order/changestatusmass', array("status" => $action["status"] ) ),
                'additional' => array(
                    'status_value' => array(
                        'name' => 'status_value',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => Mage::helper('frans')->__('Status'),
                        'options' => $options
                    )
                )
            ));
        }

        $this->getMassactionBlock()->addItem('send_shipment_emails', array(
                 'label'=> Mage::helper('sales')->__('Send Shipment Emails'),
                 'url'  => $this->getUrl('*/sales_order/massSendShipmentEmails'),
            ));

        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/cancel')) {
            $this->getMassactionBlock()->addItem('cancel_order', array(
                 'label'=> Mage::helper('sales')->__('Cancel'),
                 'url'  => $this->getUrl('*/sales_order/massCancel'),
            ));
        }

        return $this;
    }

    protected function _onHoldRenderer(Varien_Object $row)
    {
        return "test";

    }

    protected function _filterQueueNotAssigned($collection, $column){
        if (!$value = $column->getFilter()->getValue())
        {
            return $this;
        }
        $select = $collection->getSelect();

        if($value == "na"){
            $select->where("processing_queue IS NULL OR processing_queue=0");
        }
        else{
            $select->where("processing_queue = " . $value);
        }

        return $this;
    }

	protected function _filterPaymentCaptured($collection, $column)
	{
		if (!$value = $column->getFilter()->getValue()) 
		{
        	return $this;
    	}
    	
    	$select = $collection->getSelect();

    	if ($value == 'P')
    	{
    		$select->where('(total_paid IS NOT NULL AND total_paid < grand_total)');
    	}
    	else if ($value == 'Y')
    	{
     		$select->where('(total_paid = grand_total)');
    	}
		else
		{
			$select->where('total_paid IS NULL');
		}

    	return $this;
	}
	
	protected function _filterShipDate($collection, $column)
	{
		$filter = $column->getFilter();
		$value = $filter->getValue();
		
		// annoyingly, magento converts it to a datetime, rather than leaving as a date. As a result, 
		// dates don't match when there is only one value
		// we just reset it back to the original
    	$select = $collection->getSelect();
		
		if (isset($value['orig_from']))
		{
			$fromDate = date("Y-m-d", strtotime($value['orig_from']));
			$select->where("planned_ship_date >= '$fromDate'");
		}
		if (isset($value['orig_to']))
		{
			$toDate = date("Y-m-d", strtotime($value['orig_to']));
			$select->where("planned_ship_date <= '$toDate'");
		}
		
		return $this;
	}

    /**
     * @param GoSolid_Frans_Model_Resource_Sales_Order_Grid_Collection $collection
     * @param $column
     */
    protected function _filterParentOrder($collection, $column)
    {
        $filter = $column->getFilter();
        $value = $filter->getValue();

        // our filter values are not falsey, so both true
        if ($value)
        {
            $operation = ($value == self::PARENT_ORDER_YES) ? 'like' : 'nlike';
            // will either end up with LIKE %-0 or NOT LIKE %-0
            $collection->addFieldToFilter('increment_id', array( $operation => '%-0'));
        }

        return $this;
    }

    protected function _hasContentFilter($collection, $column)
    {


        $filterValue = $column->getFilter()->getValue();

        if($filterValue == 1)
        {

            $this->getCollection()
                ->addFieldToFilter($column->getIndex(),array('neq' => ''));


        }elseif($column->getIndex() == 'admin_notes' && $filterValue == 0)
        {
            $this->getCollection()
                ->addFieldToFilter($column->getIndex(),array('null' => true ));
        }
        elseif($filterValue == 0)
        {

            $this->getCollection()
                ->addFieldToFilter($column->getIndex(),array('eq' => '' ));

        }

        return $this;
    }

	
}
