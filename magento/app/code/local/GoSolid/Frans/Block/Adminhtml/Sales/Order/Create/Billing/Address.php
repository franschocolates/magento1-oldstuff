<?php
/**
 * Created by goSolid.
 * Date: 8/20/14
 * Time: 11:07 AM
 */ 
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Create_Billing_Address extends Mage_Adminhtml_Block_Sales_Order_Create_Billing_Address {

	/**
	 * Return is shipping address flag
	 *
	 * @return boolean
	 */
	public function getIsShipping()
	{
		return false;
	}

	/**
	 * Determine if it's a single shipment option
	 *
	 * @return GoSolid_Frans_Model_Sales_Quote_Address
	 */
	public function getIsMultiShipment()
	{
		return $this->getQuote()->getIsMultiShipping();
	}

}