<?php
/**
 * Renderer to put our JSON details in a nice readable format
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_View_Tab_Activity_Grid_Renderer_ChangeDetails
        extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    const FIELD_CHANGE_TEMPLATE = <<<EOHTML
<tr>
    <td colspan='3'><strong>%s</strong></td>
</tr>
<tr>
    <td></td><td>From:</td><td>%s</td>
</tr>
<tr>
    <td></td><td>To:</td><td>%s</td>
</tr>
EOHTML;


    /**
     * @param GoSolid_Frans_Model_Activity $row
     * @return string|void
     */
    public function render(Varien_Object $row)
    {

        $html = '';

        // get our JSON details and then turn it into a nice HTML table.
        $detailsJson = $row->getDetails();

        $detailsArray = json_decode($detailsJson, true);

        if (count($detailsArray) > 0)
        {
            if (isset($detailsArray[GoSolid_Frans_Model_Activity::FIELD_DESCRIPTION]))
            {
                $html .= $detailsArray[GoSolid_Frans_Model_Activity::FIELD_DESCRIPTION];
                unset($detailsArray[GoSolid_Frans_Model_Activity::FIELD_DESCRIPTION]);
            }

            $html .= '<table cellspacing="0">';

            foreach ($detailsArray as $field => $values)

            {
                $oldValue = $values['old'];
                $newValue = $values['new'];
                if($field == 'processing_queue'){
                    $adminOptions = Mage::getSingleton('orderQueues/orderQueue')->__getOrderGridOptionArray();
                    $oldValue = $adminOptions[$oldValue];
                    $newValue = $adminOptions[$newValue];
                }
                $html .= sprintf(self::FIELD_CHANGE_TEMPLATE, $field, $oldValue, $newValue);
            }

            $html .= '</table>';

        }

        return $html;
    }

}