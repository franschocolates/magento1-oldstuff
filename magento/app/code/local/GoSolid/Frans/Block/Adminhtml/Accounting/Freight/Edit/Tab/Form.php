<?php

class GoSolid_Frans_Block_Adminhtml_Accounting_Freight_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

	protected function _prepareForm()
	{

		$form = new Varien_Data_Form(); 
		$this->setForm($form);
		$fieldset = $form->addFieldset('frans_fs', array('legend'=>Mage::helper('frans')->__('Accounting Freight')));
	
	    $fieldset->addField('freight_code', 'text', array(
			'title'     => Mage::helper('frans')->__('Freight Code'),
			'label'     => Mage::helper('frans')->__('Freight Code'),
			'name'      => 'freight_code',
			'class'     => 'required-entry',
			'required'  => true,
			'note'		=> 'Name of Accounting Freight Code'
		));


		
		
		/*
	    * EXAMPLE CODE. CAN BE REMOVED.
	    *
		
		//text
		$fieldset->addField('mytext', 'text', array(
	          'title'     => Mage::helper('frans')->__('MyText'),
			  'label'     => Mage::helper('frans')->__('MyText'),
	          'name'      => 'name',
			  //'class'     => 'required-entry', 
	          //'required'  => true,
			  //'note'	  => 'My Note Here',
	      	));
		
	    //yes no
	    $fieldset->addField('myyesno', 'select', array(
		          'title'    	 => Mage::helper('frans')->__('Add To Cart'),
				  'label'    	 => Mage::helper('frans')->__('Add To Cart'),
		          //'class'     => 'required-entry', 
				  //'required'  	=> true,
		          'name'     	 => 'myyesno',
		    	  'checked'=> false,
		    	  'name'  => 'myyesno',
		    	  'options'   => array(
									'1' => 'Yes',
									'0' => 'No',
						  ),
				  'note'	=> Mage::helper('frans')->__("My Note."),

      		));   	
	   
		//money
	   	$fieldset->addField('myprice', 'text', array(
          'title'     => Mage::helper('frans')->__('MyPrice'),
		  'label'     => Mage::helper('frans')->__('MyPrice'),
       	  'name'      => 'myprice',   
	   	  //'class'     => 'required-entry validate-zero-or-greater',
          //'required'  => true,
		  //'note' => 	 Mage::helper('frans')->__("My Note Here.")
      		));
	  
      	//options
      	$options = Mage::getSingleton('event/facility')->getCollection()->setOrder("name", "asc")->toOptionArray("id", "name", "Select");
      	$fieldset->addField('myoptions', 'select', array(
          'label'     => Mage::helper('frans')->__('MyOptions'),
		  'title'     => Mage::helper('frans')->__('MyOptions'),
          //'class'     => 'required-entry',
          //'required'  => true,
          'name'      => 'myoptions',
		  'values'   => $options
      	));
		
		//status
		$fieldset->addField('status', 'select',array(
			'label'     => Mage::helper('frans')->__('Status'),
			'required'  => true,
			'name'      => 'status',
			'options'   => array(
					  		'Enabled' => 'Enabled',
					  		'Disabled' => 'Disabled',
				  ),
		));
		
		// END EXAMPLE CODE
		*/
		
	
	
		if ( Mage::getSingleton('adminhtml/session')->getAccountingFreightData() )
		{
	    	$form->setValues(Mage::getSingleton('adminhtml/session')->getAccountingFreightData());
	    	Mage::getSingleton('adminhtml/session')->getAccountingFreightData(null);
		}
		elseif(Mage::registry('accountingFreight_data'))
		{
	    	$form->setValues(Mage::registry('accountingFreight_data')->getData());
		}

		return parent::_prepareForm();
	}
  
}