<?php

class GoSolid_Frans_Block_Adminhtml_Sales_Order_Create_Form_AdminNotes
	extends Mage_Adminhtml_Block_Sales_Order_Create_Form_Abstract
{
	public function hasOrder()
	{
		return $this->getOrder() !== false;
	}
	
	public function canSave()
	{
		return $this->hasOrder();
	}
	
	/***
	 * When working in the context of creating, wont' have an order.
	 */
	public function getOrder()
	{
		if (Mage::registry('current_order')) {
            return Mage::registry('current_order');
        }

        return false;
	}
	
    public function getSaveUrl()
    {
    	return $this->getUrl("adminhtml/sales_order/saveAdminNotes", array( 'order_id' => $this->getOrder()->getId()));
    }
	
    public function getHeaderCssClass()
    {
        return 'head-account';
    }

    public function getHeaderText()
    {
        return Mage::helper('sales')->__('Admin Notes');
    }

    protected function _prepareLayout()
    {
    	if ($this->canSave())
    	{
	        $onclick = "submitAndReloadArea($('admin_notes_fields').parentNode, '".$this->getSaveUrl()."')";
	        $button = $this->getLayout()->createBlock('adminhtml/widget_button')
	            ->setData(array(
	                'label'   => Mage::helper('sales')->__('Save Notes'),
	                'class'   => 'save',
	                'onclick' => $onclick
	            ));
	        $this->setChild('save_button', $button);
		}
        return parent::_prepareLayout();
    }
    
    protected function _prepareForm()
    {
        $fieldset = $this->_form->addFieldset('main', array());

        $notesValue = '';
        if ($this->hasOrder())
        {
        	$notesValue = $this->getOrder()->getAdminNotes();
        }
        elseif($this->getQuote())
        {
            $notesValue = $this->getQuote()->getAdminNotes();
        }


        
        $fieldset->addField('admin_notes', 'textarea',
            array(
                'name'  => 'admin_notes',
                'label' => Mage::helper('frans')->__('Admin Notes'),
                'id'    => 'admin_notes',
                'class' => 'admin_notes_entry',
            	'value' => $notesValue,
                'required' => false,
            )
        );
        
        if ($this->hasOrder())
        {
	        $fieldset->addField('notes_button', 'button',
	            array(
	                'name'  => 'notes_button',
	                'label' => '',
	                'id'    => 'notes_button',
	                'class' => 'form-button scalable',
	            	'value' => 'Save'
	            )
	        );
        }
        
        $this->_form->addFieldNameSuffix('order');
        
        return $this;
    }

}