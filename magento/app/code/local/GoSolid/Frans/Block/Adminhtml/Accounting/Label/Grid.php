<?php
class GoSolid_Frans_Block_Adminhtml_Accounting_Label_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{

		parent::__construct();
		$this->setId('fransGrid');
		
		//TODO: Set default sort
		//$this->setDefaultSort('id');
		//$this->setDefaultDir('ASC');
		
		//TODO: Optional
		//$this->setSaveParametersInSession(true);
		//$this->setDefaultFilter(array('status' => 'Enabled'));
	}
 
	protected function _prepareCollection()
	{
		$collection = Mage::getModel('frans/accounting_label')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}	
		
		
	protected function _prepareColumns()
  	{

  		$this->addColumn('id', array(
			  'header'    => Mage::helper('frans')->__('ID'),
			  'align'     =>'right',
			  'width'     => '50px',
			  'index'     => 'id',
		  ));
		  
  		$this->addColumn('label_name', array(
			  'header'    => Mage::helper('frans')->__('Label'),
			  'align'     =>'right',
			  'width'     => '50px',
			  'index'     => 'label_name',
		  ));
		  
  		$this->addColumn('accpac_customer_id', array(
			  'header'    => Mage::helper('frans')->__('AccPac Customer ID'),
			  'align'     =>'right',
			  'width'     => '100px',
			  'index'     => 'accpac_customer_id',
		  ));
		  
  		$this->addColumn('accpac_location_code', array(
			  'header'    => Mage::helper('frans')->__('AccPac Origination Code'),
			  'align'     =>'right',
			  'width'     => '100px',
			  'index'     => 'accpac_location_code',
		  ));
		  
		$this->addColumn('accpac_freight_code', array(
			  'header'    => Mage::helper('frans')->__('AccPac Freight Code'),
			  'align'     =>'right',
			  'width'     => '100px',
			  'index'     => 'accpac_freight_code',
		  ));
		  
		  return parent::_prepareColumns();
	}
	
	public function getRowUrl($row)
  	{
    	return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  	}
}