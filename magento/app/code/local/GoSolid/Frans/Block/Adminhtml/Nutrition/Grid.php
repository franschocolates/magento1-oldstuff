<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Shipping carrier table rate grid block
 * WARNING: This grid used for export table rates
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class GoSolid_Frans_Block_Adminhtml_Nutrition_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Website filter
     *
     * @var int
     */
    protected $_websiteId;

    /**
     * Condition filter
     *
     * @var string
     */
    protected $_conditionName;

    /**
     * Define grid properties
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('nutritionInfoGrid');
        $this->_exportPageSize = 10000;
    }

    /**
     * Set current website
     *
     * @param int $websiteId
     * @return Mage_Adminhtml_Block_Shipping_Carrier_Tablerate_Grid
     */
    public function setWebsiteId($websiteId)
    {
        $this->_websiteId = Mage::app()->getWebsite($websiteId)->getId();
        return $this;
    }

    /**
     * Retrieve current website id
     *
     * @return int
     */
    public function getWebsiteId()
    {
        if (is_null($this->_websiteId)) {
            $this->_websiteId = Mage::app()->getWebsite()->getId();
        }
        return $this->_websiteId;
    }

    /**
     * Set current website
     *
     * @param int $websiteId
     * @return Mage_Adminhtml_Block_Shipping_Carrier_Tablerate_Grid
     */
    public function setConditionName($name)
    {
        $this->_conditionName = $name;
        return $this;
    }

    /**
     * Retrieve current website id
     *
     * @return int
     */
    public function getConditionName()
    {
        return $this->_conditionName;
    }

    /**
     * Prepare shipping table rate collection
     *
     * @return Mage_Adminhtml_Block_Shipping_Carrier_Tablerate_Grid
     */
    protected function _prepareCollection()
    {

        $collection = Mage::getModel('frans/nutrition')->getCollection();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * Prepare table columns
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header'    => Mage::helper('adminhtml')->__('ID'),
            'index'     => 'id',
            'default'   => '*',
        ));

        $this->addColumn('code', array(
            'header'    => Mage::helper('adminhtml')->__('Code'),
            'index'     => 'code',
            'default'   => '*',
        ));

        $this->addColumn('accpac_sku', array(
            'header'    => Mage::helper('adminhtml')->__('AccPac Sku'),
            'index'     => 'accpac_sku',
            'default'   => '*',
        ));

        $this->addColumn('name', array(
            'header'    => Mage::helper('adminhtml')->__('Name'),
            'index'     => 'name',
        ));

        $this->addColumn('ingredients', array(
            'header'    => Mage::helper('adminhtml')->__('Ingredients'),
            'index'     => 'ingredients',
        ));

        $this->addColumn('serving_size', array(
            'header'    => Mage::helper('adminhtml')->__('Serving Size'),
            'index'     => 'serving_size',
        ));

        $this->addColumn('weight', array(
            'header'    => Mage::helper('adminhtml')->__('Weight'),
            'index'     => 'weight',
        ));

        $this->addColumn('net_weight', array(
            'header'    => Mage::helper('adminhtml')->__('Net Weight'),
            'index'     => 'net_weight',
        ));

        $this->addColumn('calories', array(
            'header'    => Mage::helper('adminhtml')->__('Calories'),
            'index'     => 'calories',
        ));

        $this->addColumn('calories_from_fat', array(
            'header'    => Mage::helper('adminhtml')->__('Calories from Fat'),
            'index'     => 'calories_from_fat',
        ));

        $this->addColumn('total_fat', array(
            'header'    => Mage::helper('adminhtml')->__('Total Fat'),
            'index'     => 'total_fat',
        ));

        $this->addColumn('total_fat_dv', array(
            'header'    => Mage::helper('adminhtml')->__('Total Fat DV'),
            'index'     => 'total_fat_dv',
        ));

        $this->addColumn('saturated_fat', array(
            'header'    => Mage::helper('adminhtml')->__('Saturated Fat'),
            'index'     => 'saturated_fat',
        ));

        $this->addColumn('saturated_fat_dv', array(
            'header'    => Mage::helper('adminhtml')->__('Saturated Fat DV'),
            'index'     => 'saturated_fat_dv',
        ));

        $this->addColumn('trans_fat', array(
            'header'    => Mage::helper('adminhtml')->__('Trans Fat'),
            'index'     => 'trans_fat',
        ));

        $this->addColumn('trans_fat_dv', array(
            'header'    => Mage::helper('adminhtml')->__('Trans Fat DV'),
            'index'     => 'trans_fat_dv',
        ));

        $this->addColumn('cholesterol', array(
            'header'    => Mage::helper('adminhtml')->__('Cholesterol'),
            'index'     => 'cholesterol',
        ));

        $this->addColumn('cholesterol_dv', array(
            'header'    => Mage::helper('adminhtml')->__('Cholesterol DV'),
            'index'     => 'cholesterol_dv',
        ));

        $this->addColumn('sodium', array(
            'header'    => Mage::helper('adminhtml')->__('Sodium'),
            'index'     => 'sodium',
        ));

        $this->addColumn('sodium_dv', array(
            'header'    => Mage::helper('adminhtml')->__('Sodium DV'),
            'index'     => 'sodium_dv',
        ));

        $this->addColumn('total_carbohydrate', array(
            'header'    => Mage::helper('adminhtml')->__('Total Carbohydrate'),
            'index'     => 'total_carbohydrate',
        ));

        $this->addColumn('total_carbohydrate_dv', array(
            'header'    => Mage::helper('adminhtml')->__('Total Carbohydrate DV'),
            'index'     => 'total_carbohydrate_dv',
        ));

        $this->addColumn('dietary_fiber', array(
            'header'    => Mage::helper('adminhtml')->__('Dietary Fiber'),
            'index'     => 'dietary_fiber',
        ));

        $this->addColumn('dietary_fiber_dv', array(
            'header'    => Mage::helper('adminhtml')->__('Dietary Fiber DV'),
            'index'     => 'dietary_fiber_dv',
        ));

        $this->addColumn('sugars', array(
            'header'    => Mage::helper('adminhtml')->__('Sugars'),
            'index'     => 'sugars',
        ));

        $this->addColumn('protein', array(
            'header'    => Mage::helper('adminhtml')->__('Protein'),
            'index'     => 'protein',
        ));

        $this->addColumn('vitamin_a', array(
            'header'    => Mage::helper('adminhtml')->__('Vitamin A'),
            'index'     => 'vitamin_a',
        ));

        $this->addColumn('vitamin_c', array(
            'header'    => Mage::helper('adminhtml')->__('Vitamin C'),
            'index'     => 'vitamin_c',
        ));

        $this->addColumn('calcium', array(
            'header'    => Mage::helper('adminhtml')->__('Calcium'),
            'index'     => 'calcium',
        ));

        $this->addColumn('iron', array(
            'header'    => Mage::helper('adminhtml')->__('Iron'),
            'index'     => 'iron',
        ));

        $this->addColumn('is_gluten_free', array(
            'header'    => Mage::helper('adminhtml')->__('Is Gluten Free'),
            'index'     => 'is_gluten_free',
        ));

        $this->addColumn('is_non_gmo', array(
            'header'    => Mage::helper('adminhtml')->__('Is Non GMO'),
            'index'     => 'is_non_gmo',
        ));

        $this->addColumn('is_vegan', array(
            'header'    => Mage::helper('adminhtml')->__('Is Vegan'),
            'index'     => 'is_vegan',
        ));

        $this->addColumn('contains_alcohol', array(
            'header'    => Mage::helper('adminhtml')->__('Contains Alcohol'),
            'index'     => 'contains_alcohol',
        ));

        $this->addColumn('contains_nuts', array(
            'header'    => Mage::helper('adminhtml')->__('Contains Nuts'),
            'index'     => 'contains_nuts',
        ));

        $this->addColumn('contains_milk_eggs', array(
            'header'    => Mage::helper('adminhtml')->__('Contains Milk/Eggs'),
            'index'     => 'contains_milk_eggs',
        ));

        $this->addColumn('is_organic', array(
            'header'    => Mage::helper('adminhtml')->__('Is Organic'),
            'index'     => 'is_organic',
        ));

        $this->addColumn('contains_soy', array(
            'header'    => Mage::helper('adminhtml')->__('Contains Soy'),
            'index'     => 'contains_soy',
        ));

        $this->addColumn('is_fair_trade', array(
            'header'    => Mage::helper('adminhtml')->__('Is Fair Trade'),
            'index'     => 'is_fair_trade',
        ));

        $this->addColumn('background_image', array(
            'header'    => Mage::helper('adminhtml')->__('Background Image'),
            'index'     => 'background_image',
        ));

        return parent::_prepareColumns();
    }
}
