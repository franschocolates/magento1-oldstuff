<?php
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Grid_Renderer_ConditionalPaid extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {
    public function render(Varien_Object $row)
    {
        $paid = Mage::helper('frans')->__('Yes');
        if($row->getMethod() == "pay_at_pickup"){
            $paid = Mage::helper('frans')->__('No');
        }
        return $paid;
    }
}