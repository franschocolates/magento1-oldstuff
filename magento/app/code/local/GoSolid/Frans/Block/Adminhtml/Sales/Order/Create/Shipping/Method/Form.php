<?php
/**
 * Allows updating of preferred arrival date and ship date.
 */ 
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Create_Shipping_Method_Form
    extends Mage_Adminhtml_Block_Sales_Order_Create_Shipping_Method_Form
{
    private $_addressDatesAndMethods = false;
    private $_rateOptions = false;

    /**
     * Retrieve quote shipping address model
     *
     * @return GoSolid_Frans_Model_Sales_Quote_Address
     */
    public function getAddress()
    {
        if ($this->hasShipmentAddress())
        {
            return $this->getShipmentAddress();
        }
        elseif ($shipmentAddress = Mage::registry('current_shipment_address'))
        {
            return $shipmentAddress;
        }

        return $this->getQuote()->getShippingAddress();
    }

    public function getShowAddressValidation()
    {
        // this is ugly - we should really only validation if we just saved a shipping address
        // which is indicated by the request....
        $order = $this->getRequest()->getParam('order', array());
        $address = $this->getAddress();

        // the address was updated if:
        //      - we are in multiship and the shipment was updated
        //      - we are not in multiship and the shipping_address was present
        $addressWasUpdated =
            ($this->getQuote()->getIsMultiShipping() && isset($order['shipments'][$address->getId()]))
            ||
            (!$this->getQuote()->getIsMultiShipping() && isset($order['shipping_address']));

        if ($addressWasUpdated)
        {
            return $address->hasSuggestedAddressChanges();
        }

        return false;
    }

    public function getAddressValidationFormHtml()
    {
        return $this->getLayout()
                    ->createBlock('frans/adminhtml_sales_order_address_validation_view')
                    ->setTemplate('sales/order/address/validation/view.phtml')
                    ->setAddress($this->getAddress())
                    ->toHtml();
    }

    /**
     * Determine if it's a single shipment option
     *
     * @return GoSolid_Frans_Model_Sales_Quote_Address
     */
    public function getIsMultiShipment()
    {
        return $this->getQuote()->getIsMultiShipping();
    }

    public function getHtmlIdPrefix()
    {
        if ($this->getIsMultiShipment())
        {
            return 'shipment_' . $this->getAddress()->getId() . '_';
        }

        return '';
    }

    public function getHtmlNamePrefix()
    {
        $midFix = $this->getIsMultiShipment() ? ("[shipments][{$this->getAddress()->getId()}]") : '';

        return 'order' . $midFix;
    }

    public function getHtmlFieldName($fieldName)
    {
        return $this->getHtmlNamePrefix() . "[$fieldName]";
    }

    public function getHtmlFieldId($idSuffix)
    {
        return $this->getHtmlIdPrefix() . $idSuffix;
    }

    /**
     * Overridden so we can do different logic for multiship
     *
     * @return bool
     */
    public function getIsRateRequest()
    {
        if (!$this->getQuote()->getIsMultiShipping())
        {
            return parent::getIsRateRequest();
        }

        if ($this->getRequest()->getParam('collect_shipping_rates'))
        {
            // see if it's a specific one
            if ($addressId = $this->getRequest()->getParam('edit_address_id'))
            {
                return $this->getAddress()->getId() == $addressId;
            }

            // no specific address, assume it's a rate request
            return true;
        }

        return false;

    }

    /**
     * Indicates whether to show the shipping options
     *
     * @return int
     */
    public function showShippingOptions()
    {
        // make sure we have a zipcode (quick check)
        // and have shipping rates (not as quick)
        return strlen($this->getAddress()->getPostcode()) &&
                $this->getShippingRateOptions();
    }

    public function getShippingRateOptions()
    {
        if (!$this->_rateOptions)
        {
            // occasionally we may get a duplicate due to the same shipping method being
            // listed for multiple days; to get around this, we make sure we don't have it already
            $processedRates = array();
            $rateOptions = array();

            $ratesCollection = $this->getAddress()
                ->getGroupedAllShippingRates();

            // grouped all rates takes care of the fact that they might be removed
            // it also groups them by carrier, which may or may not be good.
            foreach ($ratesCollection as $carrierRates)
            {
                foreach ($carrierRates as $rate)
                {
                    if (!in_array($rate->getCode(),$processedRates))
                    {
                        $rateOptions[] = $rate;
                        $processedRates[] = $rate->getCode();
                    }
                }
            }

            $this->_rateOptions = (count($rateOptions) > 0) ? $rateOptions : false;
        }

        return $this->_rateOptions;
    }

    public function getBestValueOption()
    {

        return array_filter($this->_getDatesAndMethods(), function ($e) { return isset($e['cheapest']) && $e['cheapest']; });
    }

    public function getArrivalDateSearchField()
    {
        return $this->_getDateField('s_search_arrival_date');
    }

    public function getArrivalDateField()
    {
        return $this->_getDateField('order[preferred_arrival_date]');
    }

    public function getPlannedShipDateField()
    {
        return $this->_getDateField('order[planned_ship_date]');
    }

    public function getDateFieldHtml($fieldName, $value)
    {
        return $this->_getDateField($fieldName, $value);
    }

    public function calculateArrivalDate($shippingMethod)
    {
        if ($datesAndMethod = $this->_getDatesAndMethods())
        {
            $firstForMethod = array_filter($datesAndMethod, function ($d) use($shippingMethod) { return isset($d['shipping_method']) && $d['shipping_method'] == $shippingMethod; });

            if (is_array($firstForMethod) && count($firstForMethod))
            {
                $key =  $firstForMethod;
                $displayDate = Mage::helper('frans')->calendarDisplayDate( key($firstForMethod));
                return " [$displayDate]";
            }
        }

        return '';
    }

    public function getPreferredArrivalDate($formatted = false)
    {
        $preferredArrivalDate = $this->getAddress()->getPreferredArrivalDate();
        if (!$preferredArrivalDate && ($bestValueOption = $this->getBestValueOption()))
        {
            if (is_array($bestValueOption) && count($bestValueOption) > 0)
            {
                $preferredArrivalDate = key($bestValueOption);
            }
        }

        if ($preferredArrivalDate && $formatted)
        {
            return Mage::helper('frans')->calendarDisplayDate($preferredArrivalDate);
        }
        else if ($preferredArrivalDate)
        {
            return $preferredArrivalDate;
        }

        return '';
    }

    public function getPlannedShipDate()
    {
        if ($selectedMethod = $this->getSelectedMethodCode())
        {
            // see if we have one already; if so, use that
            // otherwise, calculate from preferred arrival date if we can.
            if (!($plannedShipDate = $this->getAddress()->getPlannedShipDate()))
            {
                $preferredArrivalDate = $this->getPreferredArrivalDate(false);

                $plannedShipDate = Mage::helper('frans')->getShipDateForMethodAndPostcode($preferredArrivalDate, $selectedMethod, $this->getAddress()->getPostcode());
            }

            if ($plannedShipDate)
            {
                return Mage::helper('frans')->calendarDisplayDate($plannedShipDate);
            }
        }

        return '';
    }

    public function getUserSelectedMethod()
    {
        return $this->getAddress()->getShippingMethod();
    }

    public function getSelectedMethodCode()
    {
        if ($selectedCode = $this->getUserSelectedMethod())
        {
            return $selectedCode;
        }
        return $this->_getBestValueField('shipping_method');
    }

    public function getSelectedPrice()
    {
        // find out if we have any selection; if so, use base shipping amount
        // which might be 0, so we can't use in an IF statement
        if ($selectedCode = $this->getUserSelectedMethod())
        {
            return $this->getAddress()->getBaseShippingAmount();
        }
        return $this->_getBestValueField('price');
    }

    public function formatPrice($price)
    {
        return Mage::getModel('directory/currency')
            ->format($price, array('display'=>Zend_Currency::NO_SYMBOL),false);
    }

    public function getSaveButtonHtml()
    {
        $onclick = ($this->getIsMultiShipment()) ?
                        "order.saveShipmentShippingMethod({$this->getAddress()->getId()})"
                        : "order.saveShippingData()";
        $button = $this->getLayout()->createBlock('adminhtml/widget_button');
	    if($this->getIsMultiShipment()){
            $button->setData(array(
                'label'   => Mage::helper('sales')->__('Update Totals'),
                'class'   => 'save',
                'onclick' => $onclick
            ));
	    } else {
		    $button->setData(array(
			    'label'   => Mage::helper('sales')->__('Done'),
			    'class'   => 'save progress-done',
			    'onclick' => $onclick
		    ));
	    }
        return $button->toHtml();
    }

    protected function _getBestValueField($fieldName)
    {
        if ($bestValueOption = $this->getBestValueOption())
        {
            if (is_array($bestValueOption) && count($bestValueOption) > 0)
            {
                $key = key($bestValueOption);
                return $bestValueOption[$key][$fieldName];
            }
        }

        return '';
    }

    protected function _getDateField($fieldName, $value = '')
    {
        if ($value)
        {
            // we need to do this to prevent it from switching month & day
            $value = new Zend_Date($value, GoSolid_Frans_Helper_Data::CALENDAR_ZEND_DATE_FORMAT, Mage::app()->getLocale()->getLocale());
        }

        $dateField = new Varien_Data_Form_Element_Date(array(
            'format'   => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
            'image'    => $this->getSkinUrl('images/grid-cal.gif'),
            'name'     => $this->getHtmlFieldName($fieldName),
            'value'     => $value,
            'class'     => 'required-entry validate-date-format' . (($fieldName == 'planned_ship_date') ? ' validate-shipment-dates' : '')
        ));

        $dateField->setForm(new Varien_Data_Form()); // just need it so it doesn't crash.
        //$dateField->setId(trim(str_replace(array('[', ']'), '_', $fieldName), '_')); // can't have [ ] in IDs
        $dateField->setId($this->getHtmlFieldId($fieldName));
        return $dateField->toHtml();
    }

    protected function _getDatesAndMethods()
    {
        if (!$this->_addressDatesAndMethods)
        {
            $this->_addressDatesAndMethods = $this->getAddress()->getAvailableArrivalDatesAndMethods();
            Mage::log('Dates and methods: ' . print_r($this->_addressDatesAndMethods, true));
        }

        return $this->_addressDatesAndMethods;
    }
}