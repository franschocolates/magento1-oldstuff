<?php

class GoSolid_Frans_Block_Adminhtml_Sales_Order_Create_Form_CustomerNotes
    extends Mage_Adminhtml_Block_Template
{
    public function getSession() {
        return Mage::getSingleton('adminhtml/session_quote');
    }
    public function getCustomerNotes()
    {
        $notesValue='';
        if($this->getSession()){
            $notesValue = Mage::getModel('frans/customerNotes')->getNotesFromCustomerOrEmail($this->getSession()->getCustomer()->getId(), false);
        }
        return $notesValue;
    }

}
