<?php
/**
 * Handles any print specific logic
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Create_Print
        extends Mage_Adminhtml_Block_Sales_Order_Create_Abstract
{
    public function getShipments()
    {
        return $this->getQuote()->getAllShippingAddresses();
    }

    public function getActiveMethodRate($address)
    {
        $rates = $address->getGroupedAllShippingRates();
        if (is_array($rates)) {
            foreach ($rates as $group) {
                foreach ($group as $code => $rate) {
                    if ($rate->getCode() == $address->getShippingMethod()) {
                        return $rate;
                    }
                }
            }
        }
        return false;
    }

    public function getShippingMethodDisplay($address)
    {
        return $address->getShippingMethod();
    }

    /**
     * Rertrieve carrier name from store configuration
     *
     * @param   string $carrierCode
     * @return  string
     */
    public function getCarrierName($carrierCode)
    {
        if ($name = Mage::getStoreConfig('carriers/'.$carrierCode.'/title', $this->getStore()->getId())) {
            return $name;
        }
        return $carrierCode;
    }

    public function getPreferredArrivalDate($address, $formatted = false)
    {
        $preferredArrivalDate = $address->getPreferredArrivalDate();
        if (!$preferredArrivalDate && ($bestValueOption = $this->getBestValueOption()))
        {
            if (is_array($bestValueOption) && count($bestValueOption) > 0)
            {
                $preferredArrivalDate = key($bestValueOption);
            }
        }

        if ($preferredArrivalDate && $formatted)
        {
            return Mage::helper('frans')->calendarDisplayDate($preferredArrivalDate);
        }
        else if ($preferredArrivalDate)
        {
            return $preferredArrivalDate;
        }

        return '';
    }

    public function getShippingPrice($address, $price, $flag)
    {
        return $this->getQuote()->getStore()->convertPrice(
            Mage::helper('tax')->getShippingPrice(
                $price,
                $flag,
                $address,
                null,
                //We should send exact quote store to prevent fetching default config for admin store.
                $this->getQuote()->getStore()
            ),
            true
        );
    }

    function cleanBaseUrl($url) {
        $disallowed = array('http://', 'https://');
        foreach($disallowed as $d) {
            if(strpos($url, $d) === 0) {
                return str_replace($d, '', trim($url, '/'));
            }
        }
        return $url;
    }
}