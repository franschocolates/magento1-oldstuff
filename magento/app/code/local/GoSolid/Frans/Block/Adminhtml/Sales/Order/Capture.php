<?php

/**
 * Adminhtml sales orders block for capture
 *
 */
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Capture extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    // if this isn't present, it tries to create it in "admin", which clearly isn't our block.
    protected $_blockGroup = 'frans';

    public function __construct()
    {
        $this->_controller = 'adminhtml_sales_order_capture';
        $this->_headerText = Mage::helper('sales')->__('Orders Ready to Capture');
        parent::__construct();

        $this->removeButton('add');
    }

    protected function _prepareLayout()
    {
        // parent creates child grid, but sets it to save stuff in session
        // we not likey.
        parent::_prepareLayout();
        $this->getChild('grid')
                ->setSaveParametersInSession(false);

        $this->_addPaymentMethodNotice();

        return $this;
    }

    protected function _addPaymentMethodNotice()
    {
        $paymentAlias = 'sfop';
        // also make sure we don't have any canceled, since that's what shows in the grid
        $orderCollection = Mage::getModel('sales/order')->getCollection()
                            ->addReadyToCaptureFilter()
                            ->addFieldToFilter('state', array('neq' => Mage_Sales_Model_Order::STATE_CANCELED))
                            ->addPaymentMethodJoin($paymentAlias);

        $orderCollection->addFilter("$paymentAlias.method", GoSolid_Frans_Model_Payment_Method_PaidAtRegister::METHOD_CODE);

        if ($orderCollection->count() > 0)
        {
            // create a new notice
            // rather than add to the session, we add directly to the messages block
            // because it has already cached the messages to display.
            $notice = Mage::getSingleton('core/message')
                        ->notice($this->__('Note that one or more orders that are ready to capture were Paid at Register'));

            $this->getLayout()->getMessagesBlock()->addMessage($notice);
        }

        return $this;
    }
}
