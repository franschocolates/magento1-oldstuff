<?php
/**
 * Data block for creating orders
 * Overridden in order to add multiship functionality
 */ 
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Create_Data
    extends Mage_Adminhtml_Block_Sales_Order_Create_Data
{
    public function showShipments()
    {
        return $this->getQuote()->getIsMultiShipping();
    }

    public function showAdditionalFields()
    {
        return !$this->getQuote()->getIsMultiShipping();
    }
}