<?php
/**
 * Rewritten to allow us to go to the customer create action, rather than to continue with no customer ID
 */ 
class GoSolid_Frans_Block_Adminhtml_Sales_Order_Create_Customer
    extends Mage_Adminhtml_Block_Sales_Order_Create_Customer
{

    /**
     * Overridden to specify a different onclick action
     *
     * @return mixed
     */
    public function getButtonsHtml()
    {
        $onclick = "window.location = '{$this->getUrl('*/customer/new', array('from_order' => true))}';";

        $addButtonData = array(
            'label'     => Mage::helper('sales')->__('Create New Customer'),
            'onclick'   => $onclick,
            'class'     => 'add',
        );
        return $this->getLayout()->createBlock('adminhtml/widget_button')->setData($addButtonData)->toHtml();
    }
}