<?php
class GoSolid_Frans_Block_Adminhtml_Quotesgrid_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{

		parent::__construct();
		$this->setId('fransGrid');
		
		//TODO: Set default sort
		//$this->setDefaultSort('id');
		//$this->setDefaultDir('ASC');
		
		//TODO: Optional
		//$this->setSaveParametersInSession(true);
		//$this->setDefaultFilter(array('status' => 'Enabled'));
	}
 
	protected function _prepareCollection()
	{
		$collection = Mage::getModel('sales/quote')->getSavedQuotes();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}	
		
		
	protected function _prepareColumns()
  	{

  		$this->addColumn('entity_id', array(
			'header'    => Mage::helper('frans')->__('Quote #'),
			'align'     =>'right',
			'width'     => '50px',
			'index'     => 'entity_id',
            'sortable'  => false,
            'filter'    => false
		  ));

        $this->addColumn('created_at', array(
            'header'    => Mage::helper('frans')->__('Created on'),
            'type'      => 'datetime',
            'align'     => 'left',
            'width'     => '90px',
            'index'     => 'created_at',
		  	'sortable'  => false,
            'filter'    => false
        ));

        $this->addColumn('customer_name', array(
            'header'    => Mage::helper('frans')->__('Customer Name'),
            'align'     =>'left',
            'sortable'  => false,
            'filter'    => false,
            'renderer' => 'GoSolid_Frans_Block_Adminhtml_Quotesgrid_Renderer_CustomerName',
        ));

        $this->addColumn('billto_name', array(
            'header'    => Mage::helper('frans')->__('Bill to Name'),
            'align'     =>'left',
            'index'     => 'billto_name',
            'sortable'  => false,
            'filter'    => false
        ));

        $this->addColumn('billto_company', array(
            'header'    => Mage::helper('frans')->__('Billing Business'),
            'align'     =>'left',
            'index'     => 'billto_company',
            'sortable'  => false,
            'filter'    => false
        ));

        $this->addColumn('customer_email', array(
            'header'    => Mage::helper('frans')->__('Email'),
            'align'     =>'left',
            'index'     => 'customer_email',
            'sortable'  => false,
            'filter'    => false
        ));

        $this->addColumn('customer_group_code', array(
            'header'    => Mage::helper('frans')->__('Group'),
            'align'     =>'left',
            'width'     => '120px',
            'index'     => 'customer_group_code',
            'sortable'  => false,
            'filter'    => false
        ));

        $this->addColumn('base_grand_total', array(
            'header' => Mage::helper('sales')->__('Grand Total'),
            'index' => 'base_grand_total',
            'type'  => 'currency',
            'currency' => 'global_currency_code',
            'sortable'  => false,
            'filter'    => false
        ));

        $this->addColumn('planned_ship_date', array(
            'header'    => Mage::helper('frans')->__('Planned ship date'),
            'type'      => 'date',
            'align'     => 'left',
            'index'     => 'planned_ship_date_shipping',
		  	'sortable'  => false,
            'filter'    => false
        ));
/*
        $this->addColumn('myname', array(
            'header'    => Mage::helper('frans')->__('Product Name and QTY SKU'),
            'align'     =>'left',
            'index'     => 'name',
        ));


        $this->addColumn('shipping_city', array(
            'header'    => Mage::helper('frans')->__('Shipping City'),
            'type'      => 'text',
            'align'     => 'left',
            'index'     => 'shipping_city'
        ));

        $this->addColumn('shipping_state', array(
            'header'    => Mage::helper('frans')->__('Shipping State'),
            'type'      => 'text',
            'align'     => 'left',
            'index'     => 'shipping_state'
        ));
*/
        $this->addColumn('admin_notes', array(
            'header'    => Mage::helper('frans')->__('Admin notes'),
            'type'      => 'text',
            'align'     => 'left',
            'index'     => 'admin_notes',
            'sortable'  => false,
            'filter'    => false
        ));

        $this->addColumn('customer_notes', array(
            'header'    => Mage::helper('frans')->__('Customer notes'),
            'type'      => 'text',
            'align'     => 'left',
            'index'     => 'customer_notes',
            'sortable'  => false,
            'filter'    => false
        ));

        //TODO: add tracking to the quote so we know who made it.
//        $this->addColumn('created_by', array(
//            'header'    => Mage::helper('frans')->__('Created By'),
//            'align'     =>'left',
//            'index'     => 'created_by',
//        ));

		  /*
		   * EXAMPLE CODE. CAN BE REMOVED.
		   *

		  //text plain
		  $this->addColumn('myname', array(
			  'header'    => Mage::helper('frans')->__('MyName'),
			  'align'     =>'left',
			  'index'     => 'name',
		  ));

		  //date
		  $this->addColumn('mydate', array(
	            'header'    => Mage::helper('frans')->__('MyDate'),
	            'type'      => 'date',
	            'align'     => 'left',
	            'index'     => 'mydate', //change me
	            'gmtoffset' => false
	        ));

	      //options
	      $options = Mage::getModel('frans/[model_lcase]')->getCollection()->setOrder("title", "asc")->toGridOptionArray("id", "title");
		  $this->addColumn('category_id', array(
			  'header'    => Mage::helper('frans')->__('Category'),
			  'align'     => 'left',
			  'index'     => 'category_id',
			  'type'  => 'options',
			  'options'	=> $options, //example of how to get options.
		  ));

		  //status
		  $this->addColumn('status', array(
			  'header'    => Mage::helper('frans')->__('Status'),
			  'align'     => 'left',
			  'width'     => '150px',
			  'index'     => 'status',
			  'type'	  => 'options',
			  'options'   => array(
				  'Enabled' => 'Enabled',
				  'Disabled' => 'Disabled',
			  ),
		  ));

		  END EXAMPLE CODE
		  */
		  

		  return parent::_prepareColumns();
	}

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('quote');

        $this->getMassactionBlock()->addItem('delete', array(
            'label'    => Mage::helper('frans')->__('Delete'),
            'url'      => $this->getUrl('*/*/massDelete'),
            'confirm'  => Mage::helper('frans')->__('Are you sure?')
        ));

        return $this;
    }
	public function getRowUrl($row)
  	{
    	return $this->getUrl('*/sales_order_create/loadQuote', array('quote_id' => $row->getEntityId()));
  	}
}