<?php

class GoSolid_Frans_Block_Payment_Form_Invoice extends Mage_Payment_Block_Form
{

    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('paybyinvoice/form.phtml');
    }

    public function getPoNumber()
    {
    	return $this->getInfoData('po_number');
    }
    
}