<?php
/**
 * Created by goSolid.
 * Date: 9/15/14
 * Time: 10:25 AM
 */ 
class GoSolid_Frans_Block_Page_Html_Topmenu extends Mage_Page_Block_Html_Topmenu {

	/**
	 * Get top menu tree object (not just an HTML string)
	 *
	 * @param string $outermostClass
	 * @param string $childrenWrapClass
	 * @return string
	 */
	public function getMenuTree($outermostClass = '', $childrenWrapClass = '')
	{

		// this function is called instead of getHtml() and returns no HTML, but does fire the normal events
		Mage::dispatchEvent('page_block_html_topmenu_gethtml_before', array(
			'menu' => $this->_menu
		));

		$this->_menu->setOutermostClass($outermostClass);
		$this->_menu->setChildrenWrapClass($childrenWrapClass);

		$menuTree = $this->_getMenuTree($this->_menu, $childrenWrapClass);

		return $menuTree;
	}

	/**
	 * Prepares menu tree object and returns it
	 *
	 * @param Varien_Data_Tree_Node $menuTree
	 * @param string $childrenWrapClass
	 * @return string
	 */
	protected function _getMenuTree(Varien_Data_Tree_Node $menuTree, $childrenWrapClass)
	{

		$children = $menuTree->getChildren();
		$parentLevel = $menuTree->getLevel();
		$childLevel = is_null($parentLevel) ? 0 : $parentLevel + 1;

		$counter = 1;
		$childrenCount = $children->count();

		$parentPositionClass = $menuTree->getPositionClass();
		$itemPositionClassPrefix = $parentPositionClass ? $parentPositionClass . '-' : 'nav-';

		foreach ($children as $child) {

			$child->setLevel($childLevel);
			$child->setIsFirst($counter == 1);
			$child->setIsLast($counter == $childrenCount);
			$child->setPositionClass($itemPositionClassPrefix . $counter);

			$outermostClass = $menuTree->getOutermostClass();

			if ($childLevel == 0 && $outermostClass) {
				$child->setClass($outermostClass);
			}

			$counter++;
		}

		return $menuTree;
	}

	protected function _getDisplaySectionNodes(Varien_Data_Tree_Node_Collection $nodeCollection, $displaySection)
	{
		$myCollection = clone $nodeCollection;
		foreach($myCollection as $node)
		{
			if($node->getDisplaySection() != $displaySection)
			{
				$myCollection->delete($node);
			}
		}
		return $myCollection;
	}

	// Split a data tree node collection into sections according to the Display Section data set for each node. Display Section is
	// a custom category attribute we're adding to data tree nodes in GoSolid_Frans_Model_Catalog_Observer::_addCategoriesToMenu().
	// This function splits a collection into multiple cloned collections, each containing nodes associated with one display section.
	// Returns an array where each key corresponds to a Display Section attribute option value.
	public function getDisplaySections(Varien_Data_Tree_Node_Collection $nodeCollection)
	{
		$displaySections = array();
		$allDisplaySectionOptions = Mage::getModel('frans/attribute_source_displaysection')->getAllOptions();
		foreach($allDisplaySectionOptions as $displaySectionOption)
		{
			$displaySections[$displaySectionOption['value']] = $this->_getDisplaySectionNodes($nodeCollection, $displaySectionOption['value']);
		}
		return $displaySections;
	}

}