<?php
/**
 * Created by goSolid.
 * Date: 8/21/15
 * Time: 3:09 PM
 */ 
class GoSolid_Frans_Block_Page_Html_Breadcrumbs extends Mage_Page_Block_Html_Breadcrumbs {

	public function getCrumbs()
	{
		return $this->_crumbs;
	}

	public function removeCrumbByUrl($url)
	{
		foreach($this->_crumbs as $key => $crumb)
		{
			if($crumb['link'] == $url)
			{
				unset($this->_crumbs[$key]);
			}
		}
		return $this;
	}

}