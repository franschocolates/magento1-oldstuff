<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Page
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class GoSolid_Frans_Block_Page_Franshomepage extends Mage_Core_Block_Template {
	
    protected function _construct()
    {
		return parent::_construct();
    }

	public function getBanners(){
		return Mage::getModel('banner/banner')->getBanners('homepage_tile');
	}

	public function getRetailBanner(){
		return Mage::getModel('banner/banner')->getBanners('homepage_retail')->getFirstItem();
	}

	public function getBannerHtml($banner, $columnCount, $bannerIndex){
		$helper = Mage::helper('frans');
		$tileNumber = ($bannerIndex % $columnCount);
		if($tileNumber == 0){
			$tileNumber = $columnCount;
		}
		$tileClass = $helper->getNumberText($tileNumber);
		$bannerBlock = $this->getLayout()->createBlock('banner/banner')
			->setTemplate('banner/tile.phtml')
			->setBanner($banner)
			->setTileClass($tileClass);
		return $bannerBlock->toHtml();
	}

	public function getNewsletterTileHtml(){
		$bannerBlock = $this->getLayout()->createBlock('banner/newslettertile')
			->setTemplate('banner/newsletter-tile.phtml');
		return $bannerBlock->toHtml();
	}

}