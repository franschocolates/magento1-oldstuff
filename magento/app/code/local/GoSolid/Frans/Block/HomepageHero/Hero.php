<?php

class GoSolid_Frans_Block_HomepageHero_Hero extends Mage_Core_Block_Template {

	public function _prepareLayout()
	{
		parent::_prepareLayout();
		$template = Mage::getStoreConfig('frans/homepage_hero/hero_template');
		if(!empty($template))
		{
			$templateDirectory = GoSolid_Frans_Model_HomepageHero::TEMPLATE_DIRECTORY;
			$this->setTemplate($templateDirectory . '/' . $template);
		}
	}

}