<?php
/**
**/
class GoSolid_Frans_Block_Fransemail_Order extends Mage_Core_Block_Template
{
	public function _construct() 
	{
		return parent::_construct();
	}
	
	public function getSummaryTotalLabel()
	{
		$storeId = Mage::app()->getStore()->getId();		
		switch($this->getParentTemplate()){
			case Mage::getStoreConfig(Mage_Sales_Model_Order_Creditmemo::XML_PATH_EMAIL_TEMPLATE, $storeId):
			case Mage::getStoreConfig(Mage_Sales_Model_Order_Creditmemo::XML_PATH_EMAIL_GUEST_TEMPLATE, $storeId):
			case Mage::getStoreConfig(Mage_Sales_Model_Order_Creditmemo::XML_PATH_UPDATE_EMAIL_TEMPLATE, $storeId):
			case Mage::getStoreConfig(Mage_Sales_Model_Order_Creditmemo::XML_PATH_UPDATE_EMAIL_GUEST_TEMPLATE, $storeId):
				$result = $this->__("REFUND TOTAL");
				break;
			default:
				$result = $this->__("ORDER TOTAL");
				break;
		}
		return $result;
	}

	public function getSummaryTotalAmount($order)
	{
		$storeId = Mage::app()->getStore()->getId();
		switch($this->getParentTemplate()){
			case Mage::getStoreConfig(Mage_Sales_Model_Order_Creditmemo::XML_PATH_EMAIL_TEMPLATE, $storeId):
			case Mage::getStoreConfig(Mage_Sales_Model_Order_Creditmemo::XML_PATH_EMAIL_GUEST_TEMPLATE, $storeId):
			case Mage::getStoreConfig(Mage_Sales_Model_Order_Creditmemo::XML_PATH_UPDATE_EMAIL_TEMPLATE, $storeId):
			case Mage::getStoreConfig(Mage_Sales_Model_Order_Creditmemo::XML_PATH_UPDATE_EMAIL_GUEST_TEMPLATE, $storeId):
				$result = $order->getTotalRefunded();
				break;
			default:
				$result = $order->getGrandTotal();
				break;
		}
		return $result;
	}

    public function hasShipment($order)
    {
        $shipment = $order->getShipmentsCollection()->getFirstItem();
        return ($shipment->getId() > 0);
    }

	public function getColorTileLabel($item)
	{
		$storeId = Mage::app()->getStore()->getId();
		$product = $item->getProduct();
		$colorTileValue = Mage::getResourceModel('catalog/product')->getAttributeRawValue($product->getId(), 'color_tile', $storeId);
		$colorTileLabel = null;
		$parentId = $item->getConfigurableProductId();
		if($parentId && !empty($colorTileValue)){
			$colorTileLabel = $product->setColorTile($colorTileValue)->getAttributeText('color_tile');
		}
		return $colorTileLabel;
	}

	public function getProductName($item)
	{
		$productName = $item->getName();
		if($configurableProductId = $item->getConfigurableProductId()){
			$storeId = Mage::app()->getStore()->getId();
			$productName = Mage::getResourceModel('catalog/product')->getAttributeRawValue($configurableProductId, 'name', $storeId);
		}
		return $productName;
	}
}