<?php

class GoSolid_Frans_Block_Eventfavors_Eventfavors extends Mage_Core_Block_Template
{

	// get the first banner for the event favors page
	public function getBanner()
	{
		return Mage::getModel('banner/banner')->getBanners('eventfavors_backdrop')->getFirstItem();
	}

	// get all banners for the event favors page tiles, as an array
	public function getTileBanners()
	{
		$tileBanners = Mage::getModel('banner/banner')->getBanners('eventfavors_tile');
		$array = array();
		foreach($tileBanners as $tileBanner)
		{
			$array[] = $tileBanner;
		}
		return $array;
	}

	public function getBannerHtml($banner, $columnCount, $bannerIndex){
		$helper = Mage::helper('frans');
		$tileNumber = ($bannerIndex % $columnCount);
		if($tileNumber == 0){
			$tileNumber = $columnCount;
		}
		$tileClass = $helper->getNumberText($tileNumber);
		$bannerBlock = $this->getLayout()->createBlock('banner/banner')
			->setTemplate('banner/tile.phtml')
			->setBanner($banner)
			->setTileClass($tileClass);
		return $bannerBlock->toHtml();
	}

}