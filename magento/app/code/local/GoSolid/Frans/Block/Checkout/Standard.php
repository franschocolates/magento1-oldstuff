<?php
/**
 * Created by goSolid.
 */
class GoSolid_Frans_Block_Checkout_Standard extends Mage_Checkout_Block_Onepage_Abstract
{

    public function getMultishipUrl()
    {
        return $this->getUrl('*/checkout_multiship');
    }

    public function getIsVirtual()
    {
        return $this->getQuote()->getIsVirtual();
    }

    public function getShippingHtml($invalidFields = array())
    {
	    // $validationInfo should be an array of changed/invalid field names (empty if no validation issues)
        return $this->getLayout()
            ->createBlock('frans/checkout_standard_shipping')
            ->setInvalidFields($invalidFields)
            ->setTemplate('checkout/standard/shipping.phtml')
            ->setQuote($this->getQuote())
	        ->setIsFormSubmitted($this->getIsFormSubmitted())
            ->toHtml();
    }

	public function getSummaryHtml($showShipping = false, $showDelivery = false)
	{
		return $this->getLayout()
			->createBlock('frans/checkout_summary')
			->setTemplate('checkout/summary.phtml')
			->setQuote($this->getQuote())
            ->setShowShipping($showShipping)
            ->setShowDelivery($showDelivery)
			->setIsMultiship(false)
			->toHtml();
	}

    public function getDeliveryHtml()
    {
        return $this->getLayout()
            ->createBlock('frans/checkout_standard_delivery')
            ->setTemplate('checkout/standard/delivery.phtml')
            ->setQuote($this->getQuote())
            ->toHtml();
    }

    public function getPaymentStepHtml()
    {
        return $this->getLayout()
            ->createBlock('frans/checkout_standard_payment')
            ->setTemplate('checkout/standard/payment.phtml')
            ->setQuote($this->getQuote())
            ->toHtml();
    }

    public function showLoginSection()
    {
        if ($this->isCustomerLoggedIn())
        {
            return false;
        }

        // we have a magical getter/setter on the session indicating whether they want guest checkout
        return !$this->getCheckout()->getUseGuestCheckout();
    }

    public function preventMultiship()
    {
        return $this->getQuote()->hasVirtualItems();
    }

    /**
     * Get checkout steps codes
     * Overrides abstract, since we have completely different steps
     *
     * @return array
     */
    protected function _getStepCodes()
    {
        return array('shipping', 'delivery', 'billing_payment');
    }

}

?>
