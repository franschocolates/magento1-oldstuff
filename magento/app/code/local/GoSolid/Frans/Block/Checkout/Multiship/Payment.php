<?php

class GoSolid_Frans_Block_Checkout_Multiship_Payment extends Mage_Core_Block_Template
{
	public function __construct()
    {
        $this->setTemplate('checkout/multiship/payment.phtml');
    }


	public function getSummaryHtml()
	{
		return $this->getLayout()
			->createBlock('frans/checkout_summary')
			->setTemplate('checkout/summary.phtml')
			->setQuote($this->getQuote())
			->setShowShipping(true)
			->setIsMultiship(true)
			->toHtml();
	}

    public function getBillingHtml()
    {
        return $this->getLayout()
            ->createBlock('frans/checkout_billing')
            ->setTemplate('checkout/billing.phtml')
	        ->setShipmentType('multi')
            ->setQuote($this->getQuote())
            ->toHtml();
    }

    public function getInnerPaymentBlock()
    {
        return $this->getLayout()
            ->createBlock('frans/checkout_payment')
            ->setTemplate('checkout/payment.phtml')
            ->setQuote($this->getQuote());
    }

    public function getPaymentHtml()
    {
        return $this->getInnerPaymentBlock()
            ->toHtml();
    }
}