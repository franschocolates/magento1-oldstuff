<?php
/**
 * Created by goSolid.
 */
class GoSolid_Frans_Block_Checkout_Standard_Summary extends Mage_Core_Block_Template
{
    private $_arrivalDate;
    private $_arrivalDateImageUrl;
    private $_shippingDescription;

    protected function _beforeToHtml()
    {
        $shippingAddress = $this->getQuote()->getShippingAddress();

        /* @var GoSolid_Frans_Model_Sales_Quote_Address $shippingAddress */
        $preferredArrivalDate = $shippingAddress->getPreferredArrivalDate();
        $this->_arrivalDate = new DateTime($preferredArrivalDate);

        $defaultMessage = Mage::getModel('frans/shipping_carrier_frans')->getDefaultMessage();
        $this->_arrivalDateImageUrl = $defaultMessage->getImageUrl();

        $this->_determineShippingMethod($shippingAddress);
    }

    public function getImageUrl()
    {
        return $this->_arrivalDateImageUrl;
    }

    public function getArrivalDate()
    {
        return Mage::helper('frans')->prettyDate($this->_arrivalDate);
    }

    public function getShippingDescription()
    {
        return $this->_shippingDescription;
    }

    protected function _determineShippingMethod($shippingAddress)
    {
        $friendlyShipMethod = Mage::getModel('frans/shippingMethod')->loadByMethod($shippingAddress->getShippingMethod());

        $this->_shippingDescription = ($friendlyShipMethod->getId()) ? $friendlyShipMethod->getLabel() :
                                                                    $shippingAddress->getShippingDescription();
    }
}