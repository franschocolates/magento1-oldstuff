<?php

class GoSolid_Frans_Block_Checkout_Multiship_Shipmentsummary extends Mage_Core_Block_Template
{
	/* Generate the HTML for displaying truncated messages */
	public function getTruncatedMessageHtml($text, $label)
	{
		$cutoff = 125; // truncate the text after this many characters
		$arr = array(
			'show' => substr($text, 0, $cutoff),
			'hide' => substr($text, $cutoff)
		);
		$output = '<span class="truncated-msg">' . $arr['show'] . '</span>';

		// only add the control element and additional span if there's more text to show
		if($arr['hide'] != ''){
			$output .= '<span class="truncated-msg hide">' . $arr['hide'] . '</span><span class="truncated-msg-control">... <a href="javascript:void(0);">' . $label . '</a></span>';
		}

		return $output;
	}
}