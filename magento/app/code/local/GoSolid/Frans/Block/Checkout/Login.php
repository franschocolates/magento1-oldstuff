<?php
/**
 * Customer register form block for checkout
 * Overridden in order to suppress the title change
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class GoSolid_Frans_Block_Checkout_Login extends Mage_Customer_Block_Form_Register
{
    protected function _prepareLayout()
    {
        // we can't call parent::_prepareLayout, since that sets the title
        // so skip to grandparent
        return Mage_Directory_Block_Data::_prepareLayout();
    }

    /**
     * Retrieve form posting url
     *
     * @return string
     */
    public function getPostActionUrl()
    {
        return $this->helper('customer')->getLoginPostUrl();
    }

    public function getForgotPasswordUrl()
    {
        return $this->helper('customer')->getForgotPasswordUrl();
    }

}
