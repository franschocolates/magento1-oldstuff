<?php

class GoSolid_Frans_Block_Checkout_Billing extends Mage_Directory_Block_Data
{
    public function getRegionId()
    {
        return $this->getBillingAddress()->getRegionId();
    }

    public function getRegionHtmlSelect()
    {
        $select = $this->getLayout()->createBlock('core/html_select')
            ->setName('billing[region]')
            ->setId('billing:region')
            ->setTitle(Mage::helper('frans')->__('State/Province'))
            ->setClass('required-entry validate-state region')
            ->setValue($this->getBillingAddress()->getRegionId())
            ->setOptions($this->getRegionCollection()->toOptionArray());

        return $select->getHtml();
    }

    // assumes a quote has been set
    public function getBillingAddress()
    {
        if ($quote = $this->getQuote())
        {
            /* @var Mage_Sales_Model_Quote $quote */
            // we either use the billing address if it somehow already has information ie, they entered something and came back?
            // or we use the customer address
            if ($quote->getBillingAddress()->getFirstname())
            {
                return $quote->getBillingAddress();
            }
            else if ($quote->getCustomerId() && $quote->getCustomer()->getDefaultBillingAddress())
            {
                return $quote->getCustomer()->getDefaultBillingAddress();
            }
        }

        return Mage::getModel('customer/address')
            ->setCountryId($this->getCountryId());
    }

    public function getRegionCollection()
    {
        if (!$this->_regionCollection) {
            $this->_regionCollection = Mage::getModel('directory/region')->getResourceCollection()
                ->addCountryFilter($this->getBillingAddress()->getCountryId())
                ->load();
        }
        return $this->_regionCollection;
    }

    public function allowSaveAddress()
    {
        return $this->getQuote()->getCustomerId() > 0;
    }

    public function saveAddressChecked()
    {
        return $this->getQuote()->getCustomer()->getDefaultBilling();
    }

	public function isSingleShipment(){
		if($this->getShipmentType() == 'single'){
			return true;
		}
		return false;
	}

	public function getNameBlockHtml($flag = '')
	{
		$nameBlock = $this->getLayout()
			->createBlock('customer/widget_name')
			->setObject($this->getBillingAddress())
            ->setData("field_name_format", "billing[%s]" )
			->setFlag($flag);

		return $nameBlock->toHtml();
	}


    public function getEmailAddress()
    {
        //if the customer is loggeed in, use customer email.
        $email = "";

        if($this->getQuote()->getCustomer() != null)
        {
            $email = $this->getQuote()->getCustomer()->getEmail();
        }

        return $email;

    }

	// returns the quote's shipping address field values in JSON format, for single shipment only
	public function getShippingAddressData(){
		if($this->isSingleShipment()){
			/* @var Mage_Customer_Model_Address $shippingAddress */
			$shippingAddress = $this->getQuote()->getShippingAddress();

			// get the shipping address values to autofill the billing address form when the checkbox is activated
			$autofill = array();
			$fields = array('firstname', 'lastname', 'company', 'country_id', 'city', 'region_id', 'region', 'postcode', 'telephone');
			foreach ($fields as $field)
			{
				$autofill[sprintf('billing[%s]', $field)] = $shippingAddress->getData($field);
			}
			// add street address fields to array
			for($i = 1; $i <= $this->helper('customer/address')->getStreetLines(); $i++){
				$autofill['street' . $i] = $shippingAddress->getStreet($i);
			}
			return json_encode($autofill, JSON_HEX_APOS | JSON_HEX_QUOT); // proper escaping of single/double quotes
		} else {
			return '{}';
		}
	}

	public function getIsLoggedIn(){
		return Mage::getSingleton('customer/session')->isLoggedIn();
	}

}