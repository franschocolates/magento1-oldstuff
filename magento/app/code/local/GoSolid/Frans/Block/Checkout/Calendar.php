<?php
/**
 * 
 * Calendar for allowing users to select their date
 * 
 * A quick note on the esoteric date formats used in this code:
 * 		'F' => long name of month (e.g. November)
 * 		'l' => long name of day of week (e.g. Saturday)
 * 		't' => last day of the month
 * 		'j' => day of month with no leading zero
 * 
 * @author goSolid
 *
 */
class GoSolid_Frans_Block_Checkout_Calendar extends Mage_Core_Block_Template
{
    const DEFAULT_COLOR = "#cccccc";
    const DEFAULT_LABEL = "Shipping";

	private $_months;
	private $_messages = array();
	
    private $_shippingMethods;

	public function _beforeToHtml()
	{
		parent::_beforeToHtml();
		
		// always have the default message
		$this->_messages['default'] = $this->_getMessageFields(
			Mage::getModel('frans/shipping_carrier_frans')->getDefaultMessage());
		
			
		$allMessages = Mage::getModel("frans/FutureShipMessage")->getCollection();
		foreach($allMessages as $message)
		{
			$this->_messages[$message->getId()] =  $this->_getMessageFields($message);
		}
		
        $this->_setShippingMethods();
			
		$this->_setDisplayDates();
		
		return $this;
	}
	
	public function getMonths()
	{
		return $this->_months;
	}
	
	public function getMessages()
	{
		return $this->_messages;
	}

    protected function _setShippingMethods()
    {
        $this->_shippingMethods = array();

        foreach(Mage::getModel('frans/shippingMethod')->getCollection() as $shippingMethod)
        {
            $this->_shippingMethods[$shippingMethod->getShippingMethod()] = $shippingMethod;
        }
    }

	protected function _setDisplayDates()
	{
		// we get today's date in the store date/time
		// rather than UTC
		$today = new DateTime(Mage::getModel('core/date')->date('Y-m-d'));

		// build an array in format
		// [entry]
		// 		'month' => array ('name' => 'Jan', 'year' => 2014)
		//		'days' => array with lots of day info.
		$datesWithMethods = $this->getArrivalDatesAndMethods();
		
		$lastDisplayDate = new DateTime(key(array_slice($datesWithMethods, -1, 1, TRUE)));
		
		// always assume we do this month
		$months = array();

		$startDate = clone $today;
		$months[] = $this->_buildMonthEntry($today, $startDate, $datesWithMethods, $lastDisplayDate);

        // if we don't make the date the first, it gets weird if we are on the 31st
        $startDate->modify('first day of this month');

		while ($startDate->format('m') != $lastDisplayDate->format('m'))
		{
			$startDate->modify("+1 month");
			$months[] = $this->_buildMonthEntry($today, $startDate, $datesWithMethods, $lastDisplayDate);
		}
		
		$this->_months = $months;
	}
	
	/**
	 * 
	 * Builds the entry for a month. Should be in format
	 * 	array(
	 * 		'month' => array('name' => 'January', 'year' => 2014)
	 * @param $dateInMonth DateTime	- does not have to be the first/last date, just any date
	 * @param $blackoutDates	array
	 */
	protected function _buildMonthEntry($today, $dateInMonth, $datesWithMethods, $finalDate)
	{
		$todayAsTime = strtotime($today->format('Y-m-d'));
		$finalDayAsTime = strtotime($finalDate->format('Y-m-d'));
		$monthEntry = array(
						'month' => array('name' => $dateInMonth->format('F'),
										 'year' => $dateInMonth->format('Y'))
		);
		
		$monthStartDate = $this->_getMonthStartDate($dateInMonth);
		$monthEndDate = $this->_getMonthEndDate($dateInMonth);
		
		// just loop and add until these match
		$displayDate = clone $monthStartDate;

		$days = array();
        $availableMethods = array();
		
		//get the days for the calendar month that is to show.
		$diff = $monthEndDate->getTimestamp() - $monthStartDate->getTimestamp();
		$dayCount = floor($diff/3600/24);
		
		// we use less than or equal, since we want the dates on both ends.
		for ($i = 0; $i <= $dayCount; $i++)
		{
			$compareFormat = $displayDate->format('Y-m-d');
			$compareTime = strtotime($compareFormat);
			$messageId = 0;
			
			// figure out the class name
			$class = array();
			
			//today
			if ($compareTime == $todayAsTime)
			{
				$class[] = "today";
			}
			
			// see if in the past, in which case it's unavailable
			if ($compareTime <= $todayAsTime || $compareTime > $finalDayAsTime)
			{
				$class[] = "unavailable";
			}
			
			//get more info about this date
			$info = array();
            $method = '';
            $style = '';
			if(array_key_exists($compareFormat, $datesWithMethods))
			{
				$info = $datesWithMethods[$compareFormat];

                if (isset($info['shipping_method']))
                {
                    $method = $info['shipping_method'];
                    $class[] = 'available';
                    $style = $this->_getDayStyle($method);

                    if (!isset($availableMethods[$method]))
                    {
                        $availableMethods[$method] = $this->_getLegendEntry($method, $info['price']);
                    }

                    if ($info['fastest'])
                    {
                        $class[] = 'fastest';
                    }

                    if ($info['cheapest'])
                    {
                        $class[] = 'cheapest';
                    }
                }
                else
                {
                    $class[] = 'unavailable';
                }
			}

			$daysEntry = array(
				'numeric' => $displayDate->format('m/d/Y'),
				'text' => Mage::helper('frans')->prettyDate($displayDate),
				'number' => $displayDate->format('j'),
				'class' => implode(" ", $class),
                'method' => $method,
			);
			
			if (isset($info['message_id']))
            {
                $daysEntry['message_id'] = $info['message_id'];
            }

			if ($style)
            {
                $daysEntry['style'] = $style;
            }

			$days[] = $daysEntry;
			
			$displayDate->modify('+1 day');
		}

		$monthEntry['days'] = $days;
        $monthEntry['legend'] = $availableMethods;
		
		return $monthEntry;
	}
	
	protected function _getMessageFields($message)
	{
		return array(
			'image_url' => $message->getImageUrl(),
			'text' => $message->getMessageText(),
			'title' => $message->getMessageTitle()
		);		
	}
	
	protected function _getMonthStartDate($startDate)
	{
		$firstOfMonth = new DateTime($startDate->format('Y-m-1'));

		$firstSunday = clone $firstOfMonth;
		
		while ($firstSunday->format('w') != '0')
		{
			$firstSunday->modify('-1 day');
		}
		
		return $firstSunday;
	}
	
	protected function _getMonthEndDate($startDate)
	{
		$lastOfMonth = new DateTime($startDate->format('Y-m-t'));
		
		$lastSaturday = clone $lastOfMonth;
		while ($lastSaturday->format('w') != '6')
		{
			$lastSaturday->modify('+1 day');
		}
		
		return $lastSaturday;
	}

    protected function _getDayStyle($shippingMethod)
    {
        $color = isset($this->_shippingMethods[$shippingMethod]) ?
            $this->_shippingMethods[$shippingMethod]->getColor() : self::DEFAULT_COLOR;

        return "background-color: $color";
    }

    protected function _getLegendEntry($shippingMethod, $price)
    {
        if (isset($this->_shippingMethods[$shippingMethod]))
        {
            $methodEntry = $this->_shippingMethods[$shippingMethod];
        }
        else
        {
            $methodEntry = Mage::getModel('frans/shippingMethod')
                            ->setColor(self::DEFAULT_COLOR)
                            ->setLabel($this->_getDefaultLabel($shippingMethod));
        }

	    $rawPrice = $price;
	    $price = Mage::helper('core')->formatPrice($price, false);

	    if(substr($price, -3) == '.00'){
		    $price = substr($price, 0, -3);
	    }

        return array(
            'label' => $methodEntry->getLabel()
            , 'style' => $this->_getDayStyle($shippingMethod)
            , 'method' => $shippingMethod
	        , 'rawPrice' => $rawPrice
            , 'price' => $price
        );
    }

    protected function _getDefaultLabel($shippingMethod)
    {
        list($carrier, $methodName) = explode('_', $shippingMethod, 2);

        $carrier = Mage::getSingleton('shipping/config')->getCarrierInstance($carrier);

        $methods = $carrier->getAllowedMethods();

        if (isset($methods[$methodName]))
        {
            return $methods[$methodName];
        }

        return self::DEFAULT_LABEL;
    }
}