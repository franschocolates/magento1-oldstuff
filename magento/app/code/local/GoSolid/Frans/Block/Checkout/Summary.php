<?php

class GoSolid_Frans_Block_Checkout_Summary extends Mage_Core_Block_Template
{
    /**
     * Builds the total discount across addresses. We have to build it ourselves since the
     * discount amount only exists on the address object, not the quote object.
     *
     * @return float
     */
    public function getDiscountAmount()
    {
        $discountAmount = 0;

        foreach ($this->getQuote()->getAllShippingAddresses() as $shippingAddress)
        {
            $discountAmount += $shippingAddress->getDiscountAmount();
        }

        Mage::log("Returning discount amount of $discountAmount");
        return $discountAmount;
    }

    /**
     * Builds the description of the discount based on the applied Rule IDs
     * We don't just pull from the DiscountDescription field because that might have something like GiftCard in it
     * at least with our current implementation. So this only checks for salesrules
     * @return string
     */
    public function getDiscountDescription()
    {
        // have to do it by salesrule IDs
        $allRuleIds = array();

        /* @var Mage_Sales_Model_Quote_Item $_quoteItem */
        foreach ($this->getQuote()->getAllItems() as $_quoteItem)
        {
            if ($_quoteItem->getAppliedRuleIds())
            {
                $ruleIds = explode(',', $_quoteItem->getAppliedRuleIds());
                // for some reason array_merge doesn't work right (it duplicates)
                // so doing it the lame way.
                foreach ($ruleIds as $ruleId)
                {
                    if (!in_array($ruleId, $allRuleIds))
                    {
                        $allRuleIds[] = $ruleId;
                    }
                }
            }
        }

        $discountNames = array();

        foreach ($allRuleIds as $ruleId)
        {
            $discountNames[] = Mage::getModel('salesrule/rule')->load($ruleId)->getStoreLabel();
        }

        $discountDescription = implode(',', $discountNames);

        if (!$discountDescription)
        {
            // fallback in case for some reason there is a discount with no salesrule
            $discountDescription = $this->__('Discount');
        }

        return $discountDescription;
    }

	public function getShipmentListHtml()
	{
		if($this->getIsMultiship()){
			return $this->getLayout()
				->createBlock('frans/checkout_multiship_shipmentsummary')
				->setTemplate('checkout/multiship/shipment-summary.phtml')
				->setQuote($this->getQuote())
				->toHtml();
		} else {
			return $this->getLayout()
				->createBlock('core/template')
				->setTemplate('checkout/standard/shipment-summary.phtml')
				->setQuote($this->getQuote())
				->toHtml();
		}
	}

	public function getDeliverySummaryHtml()
    {
        // n/a for multiship
        if($this->getIsMultiship())
        {
            return "";
        }

		return $this->getLayout()
			->createBlock('frans/checkout_standard_summary')
			->setTemplate('checkout/standard/delivery-summary.phtml')
			->setQuote($this->getQuote())
			->toHtml();
	}

	/* Generate the HTML for displaying truncated messages */
	public function getTruncatedMessageHtml($text, $label)
	{
		$cutoff = 125; // truncate the text after this many characters
		$arr = array(
			'show' => substr($text, 0, $cutoff),
			'hide' => substr($text, $cutoff)
		);
		$output = '<span class="truncated-msg">' . $arr['show'] . '</span>';

		// only add the control element and additional span if there's more text to show
		if($arr['hide'] != ''){
			$output .= '<span class="truncated-msg hide">' . $arr['hide'] . '</span><span class="truncated-msg-control">... <a href="javascript:void(0);">' . $label . '</a></span>';
		}

		return $output;
	}

    public function getShippingTotal()
    {
        $total = '--';
        $quote = $this->getQuote();

        if($quote->getShippingAddress()->getShippingMethod() != null)
        {
            $total = $this->helper('core')->formatPrice($quote->getShippingAmount(), false);
        }

        return $total;
    }
}