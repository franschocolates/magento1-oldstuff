<?php

class GoSolid_Frans_Block_Checkout_Multiship_Delivery extends Mage_Core_Block_Template
{

    public function getCheckout()
    {
        if (empty($this->_checkout)) {
            $this->_checkout = Mage::getSingleton('checkout/session');
        }
        return $this->_checkout;
    }

    /**
     * Retrieve sales quote model
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        if (empty($this->_quote)) {
            $this->_quote = $this->getCheckout()->getQuote();
        }
        return $this->_quote;
    }


    public function getBestArrivalDatesAndMethods()
    {
        $range = $this->getQuote()->getBestArrivalDatesAndMethodsAsRange();

        //echo "<pre>";
        //var_dump($range);

        $fastestDate = $this->prettyDate($range["fastest"]["date"]["from"], $range["fastest"]["date"]["to"]);
        $cheapestDate = $this->prettyDate($range["cheapest"]["date"]["from"], $range["cheapest"]["date"]["to"]);

        $fastestPrice = $this->prettyPrice($range["fastest"]["price"]["from"], $range["fastest"]["price"]["to"]);
        $cheapestPrice = $this->prettyPrice($range["cheapest"]["price"]["from"], $range["cheapest"]["price"]["to"]);

        return array(
            "fastest" => array("date" => $fastestDate, "price" => $fastestPrice),
            "cheapest" => array("date" => $cheapestDate, "price" => $cheapestPrice),
        );

    }

    public function showShipmentsContracted()
    {
        $addressesWithDates = array_filter(
            $this->getQuote()->getAllShippingAddresses()
            , function($a) { return $a->hasPreferredArrivalDate(); }
        );

        return count($addressesWithDates) == 0;
    }

    /**
     * @param Mage_Sales_Model_Quote_Address $address
     */
	public function getAddressHtml($address)
	{
		return $this->getLayout()
			->createBlock('core/template')
			->setTemplate('checkout/multiship/delivery/address.phtml')
			->setAddress($address)
			->toHtml();
	}

	public function getShippingPopupHtml()
	{
		return $this->getLayout()->createBlock('cms/block')->setBlockId('shipping-information-popup')->toHtml();
	}

    public function getAddressEditUrl($address)
    {
        return $this->getUrl('*/*/editShippingAddress', array( 'id' => $address->getId(), 'delivery' => 1));
    }

    function prettyDate( $start, $end = NULL)
    {
        if($start == $end)
        {
            $startDate = new DateTime();
            $startDate->setTimestamp($start);
            $complete_date = Mage::helper('frans')->prettyDate($startDate);
        }
        else
        {
            $startDate = new DateTime();
            $startDate->setTimestamp($start);
            $endDate = new DateTime();
            $endDate->setTimestamp($end);
            $complete_date = Mage::helper('frans')->prettyDate($startDate) . " -<br />" . Mage::helper('frans')->prettyDate($endDate);
        }

        return $complete_date;
    }

    function prettyPrice( $start, $end = NULL ) {

        if($start == $end)
        {
            $complete_price = Mage::helper('core')->formatPrice($start, false);
        }
        else
        {
            $complete_price = Mage::helper('core')->formatPrice($start, false) . ' - ' . Mage::helper('core')->formatPrice($end, false);
        }

        return $complete_price;
    }


}