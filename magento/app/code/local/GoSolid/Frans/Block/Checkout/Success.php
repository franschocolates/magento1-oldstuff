<?php

class GoSolid_Frans_Block_Checkout_Success extends Mage_Core_Block_Template
{

    public function showCreateAccount()
    {
        return !$this->getOrder()->getCustomerId();
    }

    public function getOrderEmailAddress()
    {
        return $this->getOrder()->getCustomerEmail();
    }

    public function getFormKey()
    {
        return Mage::getSingleton('core/session')->getFormKey();
    }

    public function getOrderId()
    {
        return $this->getOrder()->getId();
    }

    /**
     * Initialize data and prepare it for output
     */
    protected function _beforeToHtml()
    {
        $this->_prepareLastOrder();
        return parent::_beforeToHtml();
    }

    /**
     * Get last order ID from session, fetch it and check whether it can be viewed, printed etc
     */
    protected function _prepareLastOrder()
    {
        $orderId = Mage::getSingleton('checkout/session')->getLastOrderId();

        if ($orderId) {
            $order = Mage::getModel('sales/order')->load($orderId);
            if ($order->getId()) {
                $this->setOrder($order);
            }
        }
    }

	public function getReceiptDetailsHtml()
	{
		return $this->getLayout()
			->createBlock('core/template')
			->setTemplate('checkout/receipt.phtml')
			->setOrder($this->getOrder())
			->toHtml();
	}

}