<?php

/**
 * Class GoSolid_Frans_Block_Checkout_Payment
 *
 * @
 */
class GoSolid_Frans_Block_Checkout_Payment extends Mage_Core_Block_Template
{
    // we store this so we don't call to Stripe more than once.
    protected $_stripeCustomer;

    /**
     *
     */
    public function isSavedCardOptionAvailable()
    {
        return $this->getQuote()->getCustomer() && $this->getQuote()->getCustomer()->getStripeCustomerId();
    }

    public function getGiftcardHtml()
    {
        return $this->getLayout()
                ->createBlock('core/template')
                ->setTemplate('checkout/payment/giftcard.phtml')
                ->setQuote($this->getQuote())
                ->toHtml();
    }

    public function getExistingCardMasked()
    {
	    $formattedCc = '';
        if ($stripeCustomer = $this->_getStripeCustomer()){
            if ($primaryCardInfo = $stripeCustomer->getPrimaryCardInfo()){
	            $ccType = $primaryCardInfo['type'];
	            $ccLast4 = $primaryCardInfo['last4'];
	            switch($ccType):
		            case "AE":
			            $formattedCc = 'XXXX-XXXXXX-X'.$ccLast4;
			            break;
		            case "DC":
			            $formattedCc = 'XXXX-XXXXXX-'.$ccLast4;
			            break;
		            default:
			            $formattedCc = 'XXXX-XXXX-XXXX-'.$ccLast4;
			            break;
	            endswitch;
            }
        }
	    return $formattedCc;
    }

    public function getExistingCardExpDate()
    {
        if ($stripeCustomer = $this->_getStripeCustomer())
        {
            if ($primaryCardInfo = $stripeCustomer->getPrimaryCardInfo())
            {
                return $primaryCardInfo['exp_month'] . '/' . $primaryCardInfo['exp_year'];
            }
        }
    }

    protected function _getStripeCustomer()
    {
        if ($this->_stripeCustomer != null)
        {
            return $this->_stripeCustomer;
        }

        if ($this->getQuote()->getCustomer() && $this->getQuote()->getCustomer()->getStripeCustomerId())
        {
            $this->_stripeCustomer = Mage::getModel("stripe/stripeCustomer")->init($this->getQuote()->getCustomer());
            return $this->_stripeCustomer;
        }

        return false;
    }

    public function getShowInvoice (){
        return Mage::getModel('frans/payment_method_invoice')->isAvailable($this->getQuote());
    }
}