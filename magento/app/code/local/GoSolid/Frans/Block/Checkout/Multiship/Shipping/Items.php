<?php

class GoSolid_Frans_Block_Checkout_Multiship_Shipping_Items extends Mage_Core_Block_Template
{
	public function getAddItemUrl()
	{
		# return Mage::getStoreConfig('frans/checkout_options/add_item_url');
		return $this->getUrl('*/*/addItem');
	}
	
	public function getRemoveItemUrl($item)
	{
		$params = array ( 'id' => $item->getId() );
		
		if ($this->isAjaxRemove())
		{
			$params['ajax'] = 1;
		}
		return $this->getUrl('*/*/removeItem', $params);
	}

	public function isAjaxRemove() 
	{
		return true;
	}

    /**
     * Returns a string indicating how many they have used
     *
     * @param Mage_Sales_Model_Quote_Item $quoteItem
     */
    public function getAllocatedDisplay($quoteItem)
    {
        $allocatedQty = ($quoteItem->getMultishippingQty()) ? $quoteItem->getMultishippingQty() : 0;
        $totalQty = max($allocatedQty, $quoteItem->getQty());

        return $allocatedQty . " / " . $totalQty;
    }

}