<?php

class GoSolid_Frans_Block_Checkout_Multiship_Shipping_Addresses extends Mage_Core_Block_Template
{
	public function getEditUrl($quoteAddress)
	{
		return $this->getUrl('*/*/editShippingAddress', array("id" => $quoteAddress->getId()));
	}
	
	public function getAddUrl()
	{
		return $this->getUrl('*/*/addShippingAddress');
	}
	
	/**
	 * 
	 * Text to put on the Add Recipient button. Depends on whether they are a guest or not
	 */
	public function getAddButtonText()
	{
		return $this->getCustomerIsGuest() ? 
			'Add Recipient' : 'Select or Add Recipient';
	}
}