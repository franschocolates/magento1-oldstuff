<?php

class GoSolid_Frans_Block_Checkout_Multiship extends Mage_Core_Block_Template
{
	private $_checkout;
	private $_quote;
	
    /**
     * Retrieve checkout session model
     *
     * @return Mage_Checkout_Model_Session
     */
    public function getCheckout()
    {
        if (empty($this->_checkout)) {
            $this->_checkout = Mage::getSingleton('checkout/session');
        }
        return $this->_checkout;
    }

    /**
     * Retrieve sales quote model
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        if (empty($this->_quote)) {
            $this->_quote = $this->getCheckout()->getQuote();
        }
        return $this->_quote;
    }
    
	public function getShippingHtml()
	{
		return $this->getLayout()
			->createBlock('frans/checkout_multiship_shipping')
			->setTemplate('checkout/multiship/shipping.phtml')
			->setQuote($this->getQuote())
			->toHtml();
	}


	public function getShippingModalHtml($invalidFields = array(), $customerAddressId)
	{
		$addressId = $this->getAddressId();
		$customerAddId = $customerAddressId;



		// $validationInfo should be an array of changed/invalid field names (empty if no validation issues)
		return $this->getLayout()
			->createBlock('frans/checkout_multiship_shipping_modal')
			->setNewCustomerAddressId($customerAddId)
			->setInvalidFields($invalidFields)
			->setTitle('Add New Address')
			->setTemplate('checkout/multiship/shipping/modal_form.phtml')
			->setQuote($this->getQuote())
			->setIsFormSubmitted($this->getIsFormSubmitted())
			->setAddressId($addressId)
			->toHtml();
	}

	public function getEditAddressHtml($addressId = false, $delivery = false, $invalidFields = array(), $customerAddressId)
	{
		$address = Mage::getModel('sales/quote_address')->load($addressId);
		$customerAddId = $customerAddressId;

		return $this->getLayout()
			->createBlock('frans/sales_quote_address_edit', '', array($address))
			->setInvalidFields($invalidFields)
			->setNewCustomerAddressId($customerAddId ? $customerAddId : '')
			->setTitle('Edit Address')
			->setTemplate('customer/address/edit.phtml')
			->setDelivery($delivery)
			->setIsTemplateAddress(false)
			->setIsFormSubmitted($this->getIsFormSubmitted())
			->setAddressId($addressId)
			->toHtml();
	}

	public function getDeliveryHtml()
	{
		return $this->getLayout()
			->createBlock('frans/checkout_multiship_delivery')
			->setTemplate('checkout/multiship/delivery.phtml')
			->setQuote($this->getQuote())
			->toHtml();
	}
	
	public function getPaymentHtml()
	{
		return $this->getLayout()
			->createBlock('frans/checkout_multiship_payment')
			->setTemplate('checkout/multiship/payment.phtml')
			->setQuote($this->getQuote())
			->toHtml();
	}

	public function getStandardUrl()
	{
		return $this->getUrl('*/checkout');
	}
	
}