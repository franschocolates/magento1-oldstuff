<?php
/**
 * Created by goSolid.
 */
class GoSolid_Frans_Block_Checkout_Standard_Payment extends Mage_Checkout_Block_Onepage_Abstract
{
    public function getBillingHtml()
    {
        return $this->getLayout()
            ->createBlock('frans/checkout_billing')
            ->setTemplate('checkout/billing.phtml')
            ->setShipmentType('single')
            ->setQuote($this->getQuote())
            ->toHtml();
    }

    public function getInnerPaymentBlock()
    {
        return $this->getLayout()
            ->createBlock('frans/checkout_payment')
            ->setTemplate('checkout/payment.phtml')
            ->setQuote($this->getQuote());
    }

    public function getPaymentHtml()
    {
        return $this->getInnerPaymentBlock()
            ->toHtml();
    }


}