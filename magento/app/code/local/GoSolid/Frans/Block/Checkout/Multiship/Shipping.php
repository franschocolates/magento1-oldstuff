<?php

class GoSolid_Frans_Block_Checkout_Multiship_Shipping extends Mage_Core_Block_Template
{
	const PAGE_SIZE = 5;
	
	private $_columnCount;
	private $_pageCount;
	
	private $_items;
	private $_addressItemsQtys;
	private $_addresses;
	
	private $_pagerHtml = '';

    private $_allocatedSubtotal = 0;

	/**
	 * 
	 * @param $quote	Mage_Sales_Model_Quote
	 */
	public function setQuote($quote)
	{
		parent::setQuote($quote);
		
		$addressItemQtys = array();
		
		# need to build an array of address/item/qtys
		$this->_addresses = $quote->getAllShippingAddresses();
		$this->_items = $quote->getAllItems();
        $this->_doAllocationForItems();
		
		# we may not want to show addresses
		# this happens if there is a single empty address
		if ($this->_showAddresses())
		{
			foreach ($this->_addresses as $address)
			{
				$addressItems = array();
				$addressQuoteItems = $address->getAllItems();
				
				foreach ($this->_items as $quoteItem)
				{
					# find if we have this in our quote
					if ($addressQuoteItem = $address->getItemByQuoteItemId($quoteItem->getId()))
					{
						$qty = $addressQuoteItem->getQty();
					}
					else 
					{
						$qty = 0;
					}
					
					$addressItems[$quoteItem->getId()] = $qty;
				}
				
				$addressItemQtys[$address->getId()] = $addressItems;
			}
		}
		else
		{
			$this->_addresses = array();
		}
		
		$this->_addressItemsQtys = $addressItemQtys;
		
		$this->_setPageParams();
		
		return $this;
	}
	
	public function getItems() {
        return $this->_items;
	}
	
	public function getAddresses() {
        return $this->_addresses;
	}

	public function getAddressItemQtys() {
		return $this->_addressItemsQtys;
	}
	
	public function getColumnCount() {
		return $this->_columnCount;
	}
	
	public function getPageCount() {
		return $this->_pageCount;
	}

	public function getItemsHtml()
	{
		return $this->getLayout()
					->createBlock('frans/checkout_multiship_shipping_items')
					->setTemplate('checkout/multiship/shipping/items.phtml')
					->setItems($this->getItems())
					->setColumnCount($this->getColumnCount())
					->toHtml();
	}
	
	public function getAddressItemsHtml()
	{
		return $this->getLayout()
					->createBlock('core/template')
					->setTemplate('checkout/multiship/shipping/address_items.phtml')
					->setAddressItemQtys($this->getAddressItemQtys())
					->setColumnCount($this->getColumnCount())
					->toHtml();
	}
	
	public function getAddressesHtml()
	{
		$session = Mage::getSingleton('customer/session');
		return $this->getLayout()
					->createBlock('frans/checkout_multiship_shipping_addresses')
					->setTemplate('checkout/multiship/shipping/addresses.phtml')
					->setAddresses($this->getAddresses())
					->setCustomerIsGuest(!$session->isLoggedIn())
					->toHtml();
	}
	
	public function getTotalsHtml()
	{
		return $this->getLayout()
					->createBlock('core/template')
					->setTemplate('checkout/multiship/shipping/totals.phtml')
					->setItems($this->getItems())
                    ->setAllocatedSubtotal($this->_allocatedSubtotal)
					->toHtml();
	}
	
	public function getAddressTemplateHtml()
	{
		# need an empty address model to bind to
		return $this->_getAddressHtml(false);
	}
	
	public function getSaveInfoUrl() 
	{
		return $this->getUrl('*/*/saveShippingInfo');
	}
	
	public function getPagerHtml()
	{
		if ($this->_pagerHtml == '')
		{
			$this->_pagerHtml = $this->getLayout()
									->createBlock('core/template')
									->setTemplate('checkout/multiship/shipping/pager.phtml')
									->setPageCount($this->getPageCount())
									->toHtml();
		}
		
		return $this->_pagerHtml;
	}

	private function _getAddressHtml($addressId = false)
	{
		$address = Mage::getModel('sales/quote_address');
		
		if ($addressId)
		{
			$address->load($addressId);
		}
		
		# we set the address via a constructor param
		# because otherwise it tries to do a bunch of stuff in _prepareLayout
		# which we can't really inject to
		return $this->getLayout()
					->createBlock('frans/sales_quote_address_edit', '', $address)
					->setTemplate('customer/address/edit.phtml')
					->setIsTemplateAddress(true)
					->toHtml();
	}
	
	private function _setPageParams()
	{
		$itemCount = count($this->getItems());
		$remainder = ($itemCount > self::PAGE_SIZE) ?
						(self::PAGE_SIZE - ($itemCount % self::PAGE_SIZE)) : (self::PAGE_SIZE - $itemCount);

		# if we have a remainder, we just need to round up to the next closest
		# if we don't, then we add a full page of items
		$colCount = ($remainder > 0) ? ($itemCount + $remainder) : (self::PAGE_SIZE + $itemCount);
		$this->_columnCount = $colCount;
		$this->_pageCount = ($colCount / self::PAGE_SIZE);
	}
	
	private function _showAddresses()
	{
		return (count($this->_addresses) > 1) || !$this->_addressIsEmpty($this->_addresses[0]);
	}
	
	private function _addressIsEmpty($address)
	{
        if (isset($address) && $address instanceof Mage_Sales_Model_Quote_Address)
        {
            return $address->getFirstname() == '' && $address->getLastname() == '';
        }

        return true;
	}

    /**
     *
     * Goes through and figures out the allocated amount for each item and sets it
     * Also calculates our allocated subtotal while it's at it.
     */
    private function _doAllocationForItems()
    {
        $allocatedSubtotal = 0;

        foreach ($this->_items as $_quoteItem)
        {
            if ($_quoteItem->getMultishippingQty() > 0)
            {
                $_itemAllocatedTotal = $_quoteItem->getMultishippingQty() * $_quoteItem->getPrice();
                $_quoteItem->setAllocatedRowTotal($_itemAllocatedTotal);
                Mage::log("Incrementing the total.");
                $this->_allocatedSubtotal += $_itemAllocatedTotal;
            }
            else
            {
                $_quoteItem->setAllocatedRowTotal(0);
            }
        }
   }
}