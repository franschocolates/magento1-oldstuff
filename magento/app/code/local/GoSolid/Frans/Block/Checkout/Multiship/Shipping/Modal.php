<?php
/**
 * Block for handling shipping for standard/single shipments
 * Should really be empty apart from calling to the address edit, with a few parameters to make it work inline.
 */

class GoSolid_Frans_Block_Checkout_Multiship_Shipping_Modal extends Mage_Checkout_Block_Onepage_Abstract
{
    public function getAddressFormHtml()
    {
        return $this->_getAddressHtml();
    }

    private function _getAddressHtml()
    {

        $addressId = $this->getAddressId();
        $newCustomerAddressId = $this->getNewCustomerAddressId();



        $address = Mage::getModel('sales/quote_address')->load($addressId);

        # we set the address via a constructor param
        # because otherwise it tries to do a bunch of stuff in _prepareLayout
        # which we can't really inject to



        return $this->getLayout()
            ->createBlock('frans/sales_quote_address_edit', '', array($address))
            ->setTemplate('customer/address/edit.phtml')
            ->setNewCustomerAddressId($newCustomerAddressId ? $newCustomerAddressId : '')
            ->setAddressId($addressId)
            ->setIsTemplateAddress(false)
            ->setSuppressTitle(false)
            ->setTitle('Add New Address')
            ->setAllowSelect(true)
            ->setShowRemoveButton(false)
            ->setSubmitButtonLabel('Continue')
            ->setIsModal(true)
            ->setCustomFormAction('addShippingAddressPost')
            ->setCustomFormId('quote-address-form')
            ->setFieldNameFormat('%s')
            ->setIsTelephoneRequired(true)
            ->setInvalidFields($this->getInvalidFields())
            ->setIsFormSubmitted($this->getIsFormSubmitted())
            ->toHtml();
    }

}