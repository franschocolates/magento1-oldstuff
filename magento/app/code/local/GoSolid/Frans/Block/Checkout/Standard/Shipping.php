<?php
/**
 * Block for handling shipping for standard/single shipments
 * Should really be empty apart from calling to the address edit, with a few parameters to make it work inline.
 */

class GoSolid_Frans_Block_Checkout_Standard_Shipping extends Mage_Checkout_Block_Onepage_Abstract
{
    public function getAddressFormHtml()
    {
        return $this->_getAddressHtml($this->getQuote()->getShippingAddress());
    }

    private function _getAddressHtml($address)
    {
        $customerAddressId = $address->getCustomerAddressId();

        # we set the address via a constructor param
        # because otherwise it tries to do a bunch of stuff in _prepareLayout
        # which we can't really inject to
        return $this->getLayout()
            ->createBlock('frans/sales_quote_address_edit', '', array($address))
            ->setTemplate('customer/address/edit.phtml')
            ->setIsTemplateAddress(false)
            ->setSuppressTitle(true)
            ->setAllowSelect(true)
            ->setShowRemoveButton(false)
            ->setSubmitButtonLabel('Continue')
            ->setIsModal(false)
            ->setCustomFormAction('saveShipping')
            ->setCustomFormId('shipping_form')
            ->setFieldNameFormat('shipping[%s]')
            ->setIsTelephoneRequired(true)
            ->setInvalidFields($this->getInvalidFields())
            ->setIsFormSubmitted($this->getIsFormSubmitted())
            ->setOriginalCustomerAddressId($customerAddressId)
            ->toHtml();
    }

}