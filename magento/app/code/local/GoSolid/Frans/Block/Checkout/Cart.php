<?php
/**
 * Created by goSolid.
 */ 
class GoSolid_Frans_Block_Checkout_Cart extends Mage_Checkout_Block_Cart
{
    public function getCheckoutUrl()
    {
        return Mage::helper('checkout/url')->getCheckoutUrl();
    }

    public function isCheckoutDisabled()
    {
        return !Mage::getSingleton('checkout/session')->getQuote()->validateMinimumAmount();
    }

	public function getFormattedSubtotal(){
		$subtotal = Mage::helper('checkout/cart')->getCart()->getQuote()->getBaseSubtotal();
		return Mage::helper('core')->currency($subtotal, true, false);
	}

    public function getFormKey()
    {
        return Mage::getSingleton('core/session')->getFormKey();
    }
}