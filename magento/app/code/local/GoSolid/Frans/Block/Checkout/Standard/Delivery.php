<?php
/**
 * Created by goSolid.
 */

class GoSolid_Frans_Block_Checkout_Standard_Delivery extends Mage_Checkout_Block_Onepage_Abstract
{
	public function getShippingPopupHtml()
	{
		return $this->getLayout()->createBlock('cms/block')->setBlockId('shipping-information-popup')->toHtml();
	}

    public function getPreferredArrivalDate()
    {
        /** @var GoSolid_Frans_Model_Sales_Quote_Address $shippingAddress */
        $shippingAddress = $this->getQuote()->getShippingAddress();

        $selectedDate = $shippingAddress->getPreferredArrivalDate();

        if (!$selectedDate)
        {
            // get the default best value
            $arrivalDates = $shippingAddress->getAvailableArrivalDatesAndMethods(true);

            $bestValue = array_filter($arrivalDates, function($v) { return isset($v['cheapest']) && $v['cheapest']; });

            if (is_array($bestValue))
            {
                $selectedDate = key($bestValue);
            }
        }

        return Mage::helper('frans')->calendarDisplayDate($selectedDate);
    }
}