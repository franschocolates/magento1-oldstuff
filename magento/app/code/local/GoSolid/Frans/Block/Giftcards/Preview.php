<?php
/**
 * Created by goSolid.
 * Date: 4/21/15
 * Time: 2:02 PM
 */

class GoSolid_Frans_Block_Giftcards_Preview extends Mage_Core_Block_Template
{

	protected $_message;
	protected $_recipientName;
	protected $_senderName;

	public function _construct()
	{
		//Get the posted data
		$params = $this->getRequest()->getParams();
		$sku = $params["sku"];
		$options = $params['option'];

		//lets load the view block to recode the options to the OG values
		//TODO:maybe rethink the location of these functions as they kinda belong to the giftcard module
		$giftcardVirtualOptionsBlock = Mage::app()->getLayout()->createBlock('frans/giftcards_options');

		//Check for the min required data
		if($sku)
		{

			//Now lets set the product we posted.
			$giftcardVirtualOptionsBlock->setProduct(Mage::getModel('catalog/product')->loadByAttribute('sku',$sku));

			//We should have the correct options to labels now
			$giftcardVirtualOptions = $giftcardVirtualOptionsBlock->getOptions();

			//$giftcardVirtualOptionValues = $giftcardVirtualOptionsBlock->getOptionValues();

			//Set the data against the block so the template can use it
			foreach($giftcardVirtualOptions as $key => $giftcardVirtualOption)
			{
				if($key == 'gc_recipient_name') {
					$this->_recipientName = $options[$giftcardVirtualOption];
				}
				if($key == 'gc_sender_name') {
					$this->_senderName = $options[$giftcardVirtualOption];
				}
				if($key == 'gc_message') {
					$this->_message = $options[$giftcardVirtualOption];
				}
			}
		}
		return parent::_construct();
	}

	public function getMessage()
	{
		return $this->_message;
	}

	public function getRecipientName()
	{
		return $this->_recipientName;
	}

	public function getSenderName()
	{
		return $this->_senderName;
	}

}