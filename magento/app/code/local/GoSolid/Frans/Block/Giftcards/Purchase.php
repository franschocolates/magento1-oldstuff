<?php

class GoSolid_Frans_Block_Giftcards_Purchase extends Mage_Core_Block_Template
{

	const CLASSIC_GIFT_CARD_SKU = '988700';
	const VIRTUAL_GIFT_CARD_SKU = '988710';

	protected $_category;
	protected $_classicProduct;
	protected $_virtualProduct;

	protected $_termsTitle;
	protected $_termsContent;

	public function __construct()
	{
		$this->_category = Mage::registry('current_category');
		$this->_classicProduct = Mage::getModel('catalog/product')->loadByAttribute('sku', self::CLASSIC_GIFT_CARD_SKU);
		$this->_virtualProduct = Mage::getModel('catalog/product')->loadByAttribute('sku', self::VIRTUAL_GIFT_CARD_SKU);
		$this->_termsTitle = Mage::getModel('cms/block')->load('gift-cards-terms-and-conditions')->getTitle();
		$this->_termsContent = Mage::app()->getLayout()->createBlock('cms/block')->setBlockId('gift-cards-terms-and-conditions')->toHtml();
		parent::__construct();
	}

	public function getCategoryDescription()
	{
		$categoryDescription = $this->_category->getDescription();
		return $categoryDescription;
	}

	public function getCategoryName()
	{
		$categoryName = $this->_category->getName();
		return $categoryName;
	}

	public function getProductImage($product)
	{
		// images should be scaled to 350px wide
		$width = 350;
		return Mage::helper('frans/image')->getImageResizedByProduct($product, 'image', $width);
	}

	public function getProductName($product)
	{
		return $product->getName();
	}

	public function getProductDescription($product)
	{
		return $product->getDescription();
	}

	public function preventVirtual()
	{
		return Mage::getSingleton('checkout/session')->getQuote()->getIsMultiShipping();
	}

	// get the first banner for the gift cards page
	public function getBanner()
	{
		return Mage::getModel('banner/banner')->getBanners('giftcards_backdrop')->getFirstItem();
	}

}