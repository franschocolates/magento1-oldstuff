<?php

class GoSolid_Frans_Block_Giftcards_CheckBalance extends Mage_Core_Block_Template
{

	protected $_termsTitle;
	protected $_termsContent;

	public function __construct()
	{
		$this->_termsTitle = Mage::getModel('cms/block')->load('gift-cards-terms-and-conditions')->getTitle();
		$this->_termsContent = Mage::app()->getLayout()->createBlock('cms/block')->setBlockId('gift-cards-terms-and-conditions')->toHtml();
		parent::__construct();
	}

	public function getCardBalance()
	{

		$giftCardNumber = $this->getRequest()->getParam('card_number');

		if($giftCardNumber != null){
			// remove non-numeric characters from user input
			$giftCardNumber = preg_replace("/[^0-9]/", "", $giftCardNumber);
		}

		$result = array('status' => 'none'); // prepare default empty response array

		$balance = Mage::getModel('givex/giftcard')->getBalance($giftCardNumber);

		// if we have no balance, just show 0.
		if ($balance == GoSolid_Givex_Model_Giftcard::RETURN_NO_BALANCE)
		{
			$balance = 0;
		}

		# if we have a balance of 0 or above, it retrieved it, show it
		# otherwise it means the gift card doesn't exist, or there was an error connecting
		if ($balance >= 0)
		{
			$message = Mage::helper('core')->currency($balance, true, false);
			if(substr($message, -3) === '.00'){
				// chop off decimal for even dollar amounts
				$result = substr($message, 0, -3);
			}
			$result = array(
				'status'    => 'success',
				'message'   => $message
			);
		}
		else if (($balance == GoSolid_Givex_Model_Giftcard::RETURN_INVALID_CARD) && ($giftCardNumber !== null))
		{
			$message = $this->__('The gift card number you entered was not found. Please try to enter your card number again or call us at  %s for assistance.', Mage::getStoreConfig('general/store_information/phone'));
			$result = array(
				'status'    => 'error',
				'message'   => $message
			);
		}
		else if ($balance == GoSolid_Givex_Model_Giftcard::RETURN_UNKNOWN_ERROR)
		{
			$message = $this->__('Our gift card processor is unavailable at this time. Please try again later, or call us at %s.', Mage::getStoreConfig('general/store_information/phone'));
			$result = array(
				'status'    => 'error',
				'message'   => $message
			);
		}

		return $result;

	}

	// get the first banner for the gift card check balance page
	public function getBanner()
	{
		return Mage::getModel('banner/banner')->getBanners('giftcardbalance_backdrop')->getFirstItem();
	}

}