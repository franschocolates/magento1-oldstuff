<?php
/**
 * Created by goSolid.
 * Date: 6/13/14
 * Time: 11:31 AM
 */

class GoSolid_Frans_Block_Giftcards_Options extends Mage_Core_Block_Template
{

	private $_product = null;

	public function setProduct($product){
		$this->_product = $product;
		$this->_product->load();
		return $this;
	}

	// returns all product option names mapped to option_ids
	public function getOptions(){
		$options = array();

		foreach($this->_product->getOptions() as $option){
			$options[$option->getTitle()] = $option->getOptionId();
		}
		return $options;
	}

	// returns all product option values mapped to option value titles
	public function getOptionValues(){
		$options = array();

		foreach($this->_product->getOptions() as $option){
			$values = array();
			foreach($option->getValues() as $value){
				$values[$value->getOptionTypeId()] = $value->getTitle();
			}
			$options[$option->getOptionId()] = $values;
		}
		return $options;
	}

}