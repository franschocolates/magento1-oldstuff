<?php

class GoSolid_Frans_Block_Contacts_Contacts extends Mage_Core_Block_Template
{

	// get the first banner for the contact us page
	public function getBanner(){
		return Mage::getModel('banner/banner')->getBanners('contact_backdrop')->getFirstItem();
	}

}