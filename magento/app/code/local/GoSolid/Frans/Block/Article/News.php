<?php
class GoSolid_Frans_Block_Article_News extends Mage_Core_Block_Template
{
	public function _construct() 
	{
		return parent::_construct();
	}

	public function getArticlesByYear($year){
		$articles = Mage::getModel('frans/article')->getArticlesByYear($year);
		return $articles; // collection sorted news articles
	}

	public function getArticleYears(){
		$years = Mage::getModel('frans/article')->getArticleYears();
		return $years; // array of years
	}

	public function buildArticleLink($text, $article){
		$html = '<a href="';
		$html .= $article->getUrl();
		$html .= '" target="_blank" title="';
		$html .= $this->__('Read more at %s', $article->getLinkDomain());
		$html .= '">';
		$html .= $text;
		$html .= '</a>';
		return $html;
	}
}