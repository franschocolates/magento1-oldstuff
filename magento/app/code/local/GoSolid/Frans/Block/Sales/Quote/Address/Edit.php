<?php

/**
 *
 * Block for allowing editing of quote addresses
 * @author goSolid
 *
 */
class GoSolid_Frans_Block_Sales_Quote_Address_Edit extends GoSolid_Frans_Block_Customer_Address_Edit
{

	public function __construct($params)
	{
		$this->_address = $params[0];

		parent::__construct();
	}

	protected function _prepareLayout($initAddress = true)
	{
		parent::_prepareLayout(false);
	}

    public function getSaveUrl()
    {
    	$params = array('_secure'=>true);
    	if ($this->hasCustomFormAction())
        {
            $action = $this->getCustomFormAction();
        }
        else if ($addressId = $this->getAddress()->getId())
    	{
    		$params['id'] = $addressId;
    		$action = 'saveAddressPost';
            $params['delivery'] = $this->getDelivery();
    	}
    	else
    	{
    		$action = 'addShippingAddressPost';
    	}

        return Mage::getUrl('*/*/' . $action, $params);
    }

//    public function addAddressChoiceUrl()
//    {
//
//        $params = array('_secure'=>true);
//        $action = 'addChosenAddress';
//
//        return Mage::getUrl('*/*/' . $action, $params);
//    }



    public function getCountryId()
    {
        if ($countryId = $this->getAddress()->getCountryId()) {
            return $countryId;
        }
        return parent::getCountryId();
    }

    public function getRegionId()
    {
        return $this->getAddress()->getRegionId();
    }

    public function canSetAsDefaultBilling()
    {
        return false;
    }

    public function canSetAsDefaultShipping()
    {
     	return false;
    }

    public function isDefaultBilling()
    {
        return false;
    }

    public function isDefaultShipping()
    {
        return false;
    }

    public function showDeleteButton()
    {
    	return false;
    }

    public function showRemoveButton()
    {
    	return ($this->getAddress()->getId() > 0)
                && (!$this->hasAllowSelect() || !$this->getAllowSelect())
                && !$this->getDelivery();
    }
    
    public function getDeleteUrl()
    {
    	return '';
    }

    public function getRemoveUrl()
    {
    	return $this->getUrl('*/*/removeShippingAddress', array('id' => $this->getAddress()->getId()));
    }
    
    public function showBackButton()
    {
    	return false;
    }

    public function getCustomer()
    {
        return Mage::getSingleton('customer/session')->getCustomer();
    }

    public function getFormId()
    {
        if ($this->hasCustomFormId())
        {
            return $this->getCustomFormId();
        }
    	return "quote-address-form";
    }
    
    public function showCustomerAddresses()
    {
    	// if editing, don't want to show addresses
    	if ($this->getAddress()->getId() && (!$this->hasAllowSelect() || !$this->getAllowSelect()))
    	{
    		return false;
    	}

    	// we only want to show if we have a customer, and they have addresses
    	if (($customer = $this->getCustomer()) && $customer->getId())
    	{
    		return $customer->getAddressesCollection()->count() > 0;
    	}

    	return false;
    }
    
    public function getCustomerAddresses()
    {
    	/* var Mage_Customer_Model_Customer $customer */
    	if (($customer = $this->getCustomer()) && $customer->getId())
    	{
            $shippingAddresses = $customer->getShippingAddresses()
                ->addAttributeToSort('firstname','ASC')
                ->addAttributeToSort('lastname','ASC');
    		return $shippingAddresses;
    	}
    	
    	return false;
    }
    
    public function showEmail()
    {
    	return false;
    }
    
    public function showDob()
    {
    	return false;
    }
    
    /**
     * 
     * Builds the json to store for an address
     * @param Mage_Customer_Model_Address_Abstract $address
     */
    public function getAddressAttributesJson($address)
    {
    	// only include fields we want to display
    	return $this->escapeHtml($address->toJson(
    		array('entity_id', 'firstname', 'lastname', 'city', 'country_id', 'region_id', 'region',
    		'postcode', 'telephone', 'street', 'fax', 'email', 'dob', 'company')
	    ));
    }

	public function showTitle()
	{
		if ($this->hasSuppressTitle())
		{
			return !$this->getSuppressTitle();
		}
		return true;
	}

	public function isModal()
	{
		if($this->hasIsModal()){
			return $this->getIsModal();
		}
		return true;
	}

    public function getFieldNameFormat()
    {
        if (!$this->hasData('field_name_format')) {
            $this->setData('field_name_format', '%s');
        }
        return $this->getData('field_name_format');
    }

    public function getFieldName($field)
    {
        return sprintf($this->getFieldNameFormat(), $field);
    }

}