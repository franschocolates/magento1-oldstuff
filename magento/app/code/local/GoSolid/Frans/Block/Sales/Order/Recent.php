<?php
/**
 * Created by goSolid.
 * Date: 1/27/15
 * Time: 8:59 AM
 */ 
class GoSolid_Frans_Block_Sales_Order_Recent extends Mage_Sales_Block_Order_Recent {

	public function __construct()
	{
		parent::__construct();

		$orders = Mage::getResourceModel('sales/order_collection')
			->addAttributeToSelect('*')
			->joinAttribute('shipping_firstname', 'order_address/firstname', 'shipping_address_id', null, 'left')
			->joinAttribute('shipping_lastname', 'order_address/lastname', 'shipping_address_id', null, 'left')
			->addAttributeToFilter('customer_id', Mage::getSingleton('customer/session')->getCustomer()->getId())
			->addAttributeToFilter('state', array('in' => Mage::getSingleton('sales/order_config')->getVisibleOnFrontStates()))
			->addAttributeToSort('created_at', 'desc')
			->addFieldToFilter('multiship_parent_id',array('null' => true))
			->setPageSize('4') // this is the only Fran's customization, changing the number of orders shown from 5 to 4
			->load()
		;

		$this->setOrders($orders);
	}

}