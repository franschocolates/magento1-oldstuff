<?php
/**
 * Created by goSolid.
 * Date: 2/3/15
 * Time: 10:02 AM
 */ 
class GoSolid_Frans_Block_Sales_Order_History extends Mage_Sales_Block_Order_History {

	const MAX_AGE_PARAM = 'a';

	public function __construct()
	{
		parent::__construct();
		$orders = Mage::getResourceModel('sales/order_collection')
			->addFieldToSelect('*')
			->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getCustomer()->getId())
			->addFieldToFilter('state', array('in' => Mage::getSingleton('sales/order_config')->getVisibleOnFrontStates()))
			->addFieldToFilter('multiship_parent_id',array('null' => true));

		if($maxAge = $this->getMaxAge()){
			$orders->addFieldToFilter('created_at', array('from' => Mage::getModel('core/date')->date('Y-m-d', "-$maxAge day")));
		}
        $orders->addAttributeToSort('created_at', 'desc');
		$this->setOrders($orders);
		return $this;
	}

	// get the current requested maximum age (in days) for orders to display, if the URL contains a valid option
	public function getMaxAge()
	{
		if ($maxAge = (int) $this->getRequest()->getParam(self::MAX_AGE_PARAM))
		{
			switch($maxAge):
				case 30:
					$maxAge = 30;
					break;
				case 90:
					$maxAge = 90;
					break;
				case 180:
					$maxAge = 180;
					break;
				default:
					$maxAge = false;
					break;
			endswitch;
		}
		else
		{
			$maxAge = false;
		}
		return $maxAge;
	}

	// generate a URL to the order history page with a maximum age filter applied (or not)
	public function getFilterUrl($maxAge = false)
	{
		$url = $this->getUrl('sales/order/history');
		if($maxAge)
		{
			$url .= '?' . self::MAX_AGE_PARAM . '=' . $maxAge;
		}
		return $url;
	}

	// get either a "selected" attribute or empty string for an <option> tag for a certain maximum age filter on the current page
	public function getSelectedFlag($maxAge = false)
	{
		$flag = '';
		$currentMaxAge = $this->getMaxAge();
		if($maxAge == $currentMaxAge)
		{
			$flag = 'selected';
		}
		return $flag;
	}

}