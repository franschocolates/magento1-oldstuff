<?php
/**
 * Created by goSolid.
 * Date: 4/22/15
 * Time: 2:44 PM
 */ 
class GoSolid_Frans_Block_CatalogSearch_Result extends Mage_CatalogSearch_Block_Result {

	public function getProductListHtml($preContent = null, $doOpenContainer = true, $doCloseContainer = true)
	{
		// determine which layout to use from system config
		$layoutId = Mage::getStoreConfig('frans/search_results_page/product_grid_layout');
		// default to ID 1000 ("Three Up") if no ID is found
		if($layoutId == null){
			$layoutId = 1000;
		}
		$layouts = Mage::helper("frans/catalog_category")->getProductGridLayoutTemplates();
		$layout = $layouts[$layoutId];
		return $this->getChild('search_result_list')
			->setTemplate($layout)
			->toHtml();
	}

	// in the event that there are no search results, get the banner image to use as page backdrop
	public function getBanner(){

		$banners = Mage::getModel('banner/banner')->getBanners('no-route');

		if($banners->count() > 0)
		{
			return $banners->getFirstItem();
		}

		// if there aren't any banners for no-route, use the CMS default instead (hopefully it exists)
		return Mage::getModel('banner/banner')->getBanners('cms_backdrop_default')->getFirstItem();

	}

}