<?php
/**
 * Created by goSolid.
 * Date: 9/25/14
 * Time: 3:46 PM
 */ 
class GoSolid_Frans_Block_Retailstore_Stores_View extends Mage_Core_Block_Template {

	public function getStore(){
		$storeId = $this->getRequest()->getParam('id');
		$store = Mage::getModel('frans/retailStore')->load($storeId, 'id');
		return $store;
	}

	public function getCrosssellHtml()
	{
		$store = $this->getStore();

		// get the comma-delimited list of cross-sell SKUs from the retail store, as an array
		$crossSellSkus = explode(',', $store->getCrossSellSkus());

		// clean up the array in case there was whitespace before/after the commas
		foreach($crossSellSkus as $key => $sku)
		{
			$crossSellSkus[$key] = trim($sku);
		}

		// get the product collection and label for the block
		$crossSellLabel = $store->getCrossSellLabel();
		$crossSellProducts = Mage::getResourceModel('catalog/product_collection')
			->addAttributeToFilter('sku', array('in' => $crossSellSkus));

		// preserve order of products so we can control them with category positions
		// this has to be so complicated because we're getting a separate collection from the original one, due to configurable product logic above
		$orderString = array('CASE e.sku');
		foreach($crossSellSkus as $key => $sku)
		{
			$orderString[] = "WHEN '" . $sku . "' THEN " . $key;
		}
		$orderString[] = 'END';
		$orderString = implode(' ', $orderString);
		$crossSellProducts->getSelect()->order(new Zend_Db_Expr($orderString));

		// set the data to the block and get its HTML
		return $this->getChild('crosssell')->setProducts($crossSellProducts)->setLabel($crossSellLabel)->toHtml();
	}

}