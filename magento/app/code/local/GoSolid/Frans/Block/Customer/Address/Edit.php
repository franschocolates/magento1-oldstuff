<?php
/**
 * Customer address edit block
 * Similar to Magento core, but new functionality to not force it to be a customer address
 *
 * @author      goSolid
 */
class GoSolid_Frans_Block_Customer_Address_Edit extends Mage_Directory_Block_Data
{
    protected $_address;
    protected $_countryCollection;
    protected $_regionCollection;

    protected function _prepareLayout($initAddress = true)
    {
        parent::_prepareLayout();

        if ($initAddress)
        {

	        $this->_address = Mage::getModel('customer/address');

	        // Init address object
	        if ($id = $this->getRequest()->getParam('id')) {
	            $this->_address->load($id);
	            if ($this->_address->getCustomerId() != Mage::getSingleton('customer/session')->getCustomerId()) {
	                $this->_address->setData(array());
	            }
	        }

	
	        if ($headBlock = $this->getLayout()->getBlock('head')) {
	            $headBlock->setTitle($this->getTitle());
	        }

	        if ($postedData = Mage::getSingleton('customer/session')->getAddressFormData(true)) {
	            $this->_address->addData($postedData);
	        }
		}
    }

    /**
     * Generate name block html
     *
     * @return string
     */

    public function getCountryHtmlSelect($defValue=null, $name='country_id', $id='country', $title='Country')
    {
        Varien_Profiler::start('TEST: '.__METHOD__);
        if (is_null($defValue)) {
            $defValue = $this->getCountryId();
        }
        $cacheKey = 'DIRECTORY_COUNTRY_SELECT_STORE_'.Mage::app()->getStore()->getCode();
        if (Mage::app()->useCache('config') && $cache = Mage::app()->loadCache($cacheKey)) {
            $options = unserialize($cache);
        } else {
            $options = $this->getCountryCollection()->toOptionArray();
            if (Mage::app()->useCache('config')) {
                Mage::app()->saveCache(serialize($options), $cacheKey, array('config'));
            }
        }

        // for now we don't preselect the first option for country
        // if we don't need this option
        // we can probably ditch this method entirely, but we already took the time to override
        // and we may need to re-enable the lack of preselection in the future.
        $firstOptionParams = array('disabled' => 'disabled');
        $options[0]['value'] = "";
        $options[0]['label'] = "Country";
        $options[0]['params'] = $firstOptionParams;

        $html = $this->getLayout()->createBlock('core/html_select')
            ->setName($name)
            ->setId($id)
            ->setTitle(Mage::helper('directory')->__($title))
            ->setClass('validate-select')
            ->setValue($defValue)
            ->setOptions($options)
            ->getHtml();

        Varien_Profiler::stop('TEST: '.__METHOD__);
        return $html;
    }

    public function getNameBlockHtml($flag = '')
    {
        $nameBlock = $this->getLayout()
            ->createBlock('customer/widget_name')
            ->setObject($this->getAddress())
            ->setFieldNameFormat($this->getFieldNameFormat())
            ->setFlag($flag);

        return $nameBlock->toHtml();
    }

    public function addAddressMultishipChoiceUrl()
    {

        $params = array('_secure'=>true);
        $action = 'addChosenMultishipAddress';

        return Mage::getUrl('*/*/' . $action, $params);
    }

    public function addAddressCustomerChoiceUrl()
    {

        $params = array('_secure'=>true);
        $action = 'addChosenCustomerAddress';

        return Mage::getUrl('*/*/' . $action, $params);
    }

    public function getTitle()
    {
        if ($title = $this->getData('title')) {
            return $title;
        }
        if ($this->getAddress()->getId()) {
            $title = Mage::helper('customer')->__('Edit Address');
        }
        else {
	        if($this->getAddressType() == Mage_Customer_Model_Address_Abstract::TYPE_BILLING){
		        $title = Mage::helper('customer')->__('Add Billing Address');
	        } else {
                $title = Mage::helper('customer')->__('Add New Address');
	        }
        }
        return $title;
    }

    public function getBackUrl()
    {
        if ($this->getData('back_url')) {
            return $this->getData('back_url');
        }

        if ($this->getCustomerAddressCount()) {
            return $this->getUrl('customer/address');
        } else {
            return $this->getUrl('customer/account/');
        }
    }

    public function showBackButton() 
    {
    	return true;
    }

    public function showDeleteButton()
    {
    	return $this->getAddress()->getId() > 0;
    }
    
    public function showRemoveButton()
    {
    	return false;
    }
    
    public function getSaveUrl()
    {
    	return $this->getUrl('customer/address/save', array('id'=> $this->getAddress()->getId(),'ajax' => true, 'type' => $this->getAddressType()));
    }

    public function getSaveDobUrl()
    {
        return $this->getUrl('customer/address/saveDob');
    }

    public function getAddress()
    {
        return $this->_address;
    }

    public function setAddress($address)
    {
        $this->_address = $address;
        return $this->_address;
    }

    public function getCustomerAddressId()
    {
        if ($this->getRequest()->getPost('customer_address_id', false))
        {
            return $this->getRequest()->getPost('customer_address_id', false);
        } else {
            return '';
        }
    }

    public function getOriginalCustomerAddressId()
    {
        if ($this->getRequest()->getPost('original_customer_address_id', false))
        {
            return $this->getRequest()->getPost('original_customer_address_id', false);
        } else {
            return '';
        }
    }

    public function getCountryId()
    {
        if ($countryId = $this->getAddress()->getCountryId()) {
            return $countryId;
        }
        return parent::getCountryId();
    }

    public function getRegionId()
    {
        return $this->getAddress()->getRegionId();
    }

    public function getCustomerAddressCount()
    {
        return count(Mage::getSingleton('customer/session')->getCustomer()->getAddresses());
    }

    public function canSetAsDefaultBilling()
    {
        if (!$this->getAddress()->getId()) {
            return $this->getCustomerAddressCount();
        }
        return !$this->isDefaultBilling();
    }

    public function canSetAsDefaultShipping()
    {
        if (!$this->getAddress()->getId()) {
            return $this->getCustomerAddressCount();
        }
        return !$this->isDefaultShipping();;
    }

    public function isDefaultBilling()
    {
        $defaultBilling = Mage::getSingleton('customer/session')->getCustomer()->getDefaultBilling();
        return $this->getAddress()->getId() && $this->getAddress()->getId() == $defaultBilling;
    }

    public function isDefaultShipping()
    {
        $defaultShipping = Mage::getSingleton('customer/session')->getCustomer()->getDefaultShipping();
        return $this->getAddress()->getId() && $this->getAddress()->getId() == $defaultShipping;
    }

    public function getCustomer()
    {
        return Mage::getSingleton('customer/session')->getCustomer();
    }

    public function getBackButtonUrl()
    {
        if ($this->getCustomerAddressCount()) {
            return $this->getUrl('customer/address');
        } else {
            return $this->getUrl('customer/account/');
        }
    }
    
    public function getAddressType(){

    	$type = $this->getRequest()->getParam('type');
    	return $type;
    }
    
    public function showCustomerAddresses()
    {
    	// never want to show the list of addresses for this form.
    	return false;
    }
    
    public function getFormId()
    {
    	return "my-account-address";
    }
    
    public function showEmail()
    {
    	if($this->getAddressType() == "billing")
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }
    
    public function showDob()
    {
    	if($this->getAddressType() == "billing" || $this->getAddressType() == "default_mailing")
    	{
    		return false;
    	}
    	else
    	{
    		return true;
    	}
    }

	public function showTitle()
	{
		return true;
	}

	public function getSubmitButtonLabel(){
		$label = 'Save';
		if($this->hasSubmitButtonLabel()){
			$label = $this->getData('submit_button_label');
		}
		return $this->__($label);
	}

	public function isModal()
	{
		if($this->hasIsModal()){
			return $this->getIsModal();
		}
		return true;
	}

    public function getFieldNameFormat()
    {
        if (!$this->hasData('field_name_format')) {
            $this->setData('field_name_format', '%s');
        }
        return $this->getData('field_name_format');
    }



    public function getFieldName($field)
    {
        return sprintf($this->getFieldNameFormat(), $field);
    }

    public function getEditUrl($addressId)
    {
        return $this->getUrl('*/*/editShippingAddress', array("id" => $addressId));
    }

}
