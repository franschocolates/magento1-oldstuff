<?php
/**
 * Created by goSolid.
 * Date: 2/11/15
 * Time: 1:59 PM
 */ 
class GoSolid_Frans_Block_Customer_Account_Dashboard_Address extends Mage_Customer_Block_Account_Dashboard_Address {

	public function getHeaderText()
	{
		$headerText = $this->__('Default Billing Address');
		if($this->getCol() == 'shipping')
		{
			$headerText = $this->__('Default Shipping Address');
		}
		if($this->hasHeaderText())
		{
			$headerText = $this->getData('header_text');
		}
		return $headerText;
	}

}