<?php
/**
 * Created by goSolid.
 * Date: 1/30/15
 * Time: 10:52 AM
 */

class GoSolid_Frans_Block_Customer_Account_Creditcard extends Mage_Core_Block_Template
{

	protected function _prepareLayout()
	{
		if(!$this->hasCardInfo()){
			// get the stripe information for this customer
			$customer = Mage::getSingleton('customer/session')->getCustomer();
			$cardInfo = null;
			try
			{
				$stripeCustomer = Mage::getModel("stripe/stripeCustomer")->init($customer);
				$this->setCardInfo($stripeCustomer->getPrimaryCardInfo());
			}
			catch(Exception $ex)
			{
				// var_dump($ex->getMessage());
			}
		}
		return parent::_prepareLayout();
	}

	public function getFormattedLast4()
	{
		$formattedCc = '';
		if($this->hasCardInfo())
		{
			$cardInfo = $this->getCardInfo();
			$ccLast4 = $cardInfo["last4"];
			$ccType = $cardInfo["type"];
			// use &#8226; (HTML entity for a bullet character) as placeholder, and a space as separator
			$formattedCc = Mage::helper('frans/creditCard')->formatLast4ByCardType($ccLast4, $ccType, '&#8226;', ' ');
		}
		return $formattedCc;
	}

	public function getCustomerDiscountLabel()
	{
		// by default there is no label
		$discountLabel = false;

		// get the list of customer group IDs to show a label for, from config, as an array
		$visibleGroupIds = explode(',', Mage::app()->getStore()->getConfig('frans/customer_discounts/visible_discount_customer_group_ids'));

		// get the customer's group ID and see if it matches anything in the array
		$groupId = Mage::getSingleton('customer/session')->getCustomer()->getGroupId();
		if(in_array($groupId, $visibleGroupIds))
		{
			$group = Mage::getModel('customer/group')->load($groupId);
			$groupCode = strtolower($group->getCode()); // remove capitalization from group name
			$discountLabel = $this->__('You receive a %s.', $groupCode);
		}

		return $discountLabel;
	}

}
