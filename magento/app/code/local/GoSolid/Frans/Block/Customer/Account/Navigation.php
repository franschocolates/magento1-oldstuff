<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Customer
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Customer account navigation sidebar
 *
 * @category   Mage
 * @package    Mage_Customer
 * @author      Magento Core Team <core@magentocommerce.com>
 */

class GoSolid_Frans_Block_Customer_Account_Navigation extends Mage_Customer_Block_Account_Navigation
{

	
	/* Avalible Customer links and there sections are defeined below */
	
	public function getLinks()
	{
		$myAcountAreaLinks = array(
				'overview' 			=> 	array('customer/account', 'customer/account/index'),
				'personal_profile'	=>	array('customer/address','customer/account/edit','customer/address/index','customer/address/billing','customer/account/creditcard'),
				'order_history'		=>	array('sales/order/history'),
				'communication'		=>	array('newsletter/manage', 'newsletter/manage/index')
			);
			
		return $myAcountAreaLinks;
	}
	
    public function getActiveSection($section)
    {
       $myAcountAreaLinks = $this->getLinks();
       $activeLink = trim($this->getRequest()->getPathInfo(), "/");

       foreach ($myAcountAreaLinks as $key => $value)
       {
       		if(in_array($activeLink, $value) && $section == $key)
       	 	{
       	 		return "active";
       	 	}
       }
       
    
    	
        return false;
    }


}
