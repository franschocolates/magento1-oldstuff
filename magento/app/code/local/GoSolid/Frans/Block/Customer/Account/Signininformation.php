<?php
/**
 * Created by goSolid.
 * Date: 1/30/15
 * Time: 10:52 AM
 */

class GoSolid_Frans_Block_Customer_Account_Signininformation extends Mage_Core_Block_Template
{

	public function getCustomer()
	{
		return Mage::getSingleton('customer/session')->getCustomer();
	}

	public function getHeaderText()
	{
		$headerText = $this->__('Sign In Information');
		if($this->hasHeaderText())
		{
			$headerText = $this->getData('header_text');
		}
		return $headerText;
	}

}
