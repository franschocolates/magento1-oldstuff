<?php
/**
 * Created by goSolid.
 * Date: 12/11/14
 * Time: 10:22 AM
 */ 
class GoSolid_Frans_Block_Customer_Widget_Name extends Mage_Customer_Block_Widget_Name {

	protected $_baseColClass;
	protected $_colSpan;
	protected $_colSpanOverrides;

	public function _construct()
	{
		parent::_construct();

		$this->_baseColClass = 'col-xs-';
		$columnsNeeded = 2; // first and last names are always required

		if($this->showPrefix()){
			$columnsNeeded++;
		}
		if($this->showMiddlename()){
			$columnsNeeded++;
		}
		if($this->showSuffix()){
			$columnsNeeded++;
		}

		$this->_colSpan = (12 / $columnsNeeded);
		$this->_classOverrides = array();

		if($this->_colSpan == 2.4){
			// if all 5 columns are used, we can't use a decimal colspan so implement a workaround
			$this->_colSpan = 3;
			$this->_colSpanOverrides['prefix'] = 2;
			$this->_colSpanOverrides['middlename'] = 2;
			$this->_colSpanOverrides['suffix'] = 2;
		}
	}

	public function getColClass($fieldName)
	{
		$colSpan = $this->_colSpan;
		if(isset($this->_colSpanOverrides[$fieldName])){
			$colSpan = $this->_colSpanOverrides[$fieldName];
		}
		return $this->_baseColClass . $colSpan;
	}

}