<?php 
	class GoSolid_Frans_Block_Customer_Widget_Dob extends Mage_Customer_Block_Widget_Dob{
		
		public function _construct() 
		{
			return parent::_construct();
			$this->setTemplate('customer/widget/dob-modal.phtml');
		}
		
		public function setTemplate($name){
			return parent::setTemplate('customer/widget/dob-modal.phtml');
		}
		
	}
?>
