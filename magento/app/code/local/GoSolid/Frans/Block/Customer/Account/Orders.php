<?php
/**
 * Created by goSolid.
 * Date: 1/30/15
 * Time: 10:52 AM
 */

class GoSolid_Frans_Block_Customer_Account_Orders extends Mage_Core_Block_Template
{

	public function getOrderHistoryHtml()
	{
		$orderHistory = $this->getChild('sales.order.history');
		$orderHistory->setShowTotals(true)
			->setShowAgeFilter(true);
		return $orderHistory->toHtml();
	}

}
