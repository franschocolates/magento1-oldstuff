<?php
/**
 * Created by goSolid.
 * Date: 3/13/15
 * Time: 10:19 AM
 */ 
class GoSolid_Frans_Block_Customer_Address_Book extends Mage_Customer_Block_Address_Book {

	protected function _prepareLayout()
	{
		// this is here to skip creation of a head block done in the parent class
		return Mage_Core_Block_Template::_prepareLayout();
	}

}