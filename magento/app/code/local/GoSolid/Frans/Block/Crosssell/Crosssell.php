<?php
/**
 * Created by goSolid.
 * Date: 4/28/15
 * Time: 11:26 AM
 */

class GoSolid_Frans_Block_Crosssell_Crosssell extends Mage_Core_Block_Template {

	protected $_configurableParents;

	public function _construct()
	{
		$this->_configurableParents = array();
	}

	public function getProducts()
	{
		$products = $this->getData('products');
		$productIds = array();

		// filter the collection to make sure we have visible products only
		$products->addAttributeToFilter('visibility', array(
			'neq' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE
		));

		// we're not going to use this collection, first we have to check for configurable products
		foreach($products as $product)
		{
			$productId = $product->getId();

			// if it's a configurable product, look up its default child
			if($product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE)
			{
				$childProductId = Mage::getResourceModel('catalog/product')->getAttributeRawValue($productId, 'default_child', Mage::app()->getStore()->getId());
				if($childProductId)
				{
					$productIds[] = $childProductId;
					// remember the parent/child relationship for later, so we can get the right URL
					$this->_configurableParents[$childProductId] = $productId;
				}
			} else {
				$productIds[] = $productId;
			}
		}

		$products = Mage::getResourceModel('catalog/product_collection')
			->addAttributeToFilter('entity_id', array(
				'in' => $productIds
			));

		// preserve order of products so we can control them with category positions
		// this has to be so complicated because we're getting a separate collection from the original one, due to configurable product logic above
        if($products->count() > 0) {
            $orderString = array('CASE e.entity_id');
            foreach($productIds as $key => $productId)
            {
                $orderString[] = 'WHEN ' . $productId . ' THEN ' . $key;
            }
            $orderString[] = 'END';
            $orderString = implode(' ', $orderString);
            $products->getSelect()->order(new Zend_Db_Expr($orderString));
        }
		return $products;
	}

	public function getProductUrl($product)
	{
		$productId = $product->getId();
		$url = '';

		// OPTION #1: see if this simple product is the child of a configurable product, get the Fran's configurable URL rewrite
		if(array_key_exists($productId, $this->_configurableParents))
		{
			$path = Mage::getModel('frans/configurableProductUrls')->getCollection()
				->addFieldToFilter('configurable_id', array('eq' => $this->_configurableParents[$productId]))
				->addFieldToFilter('simple_id', array('eq' => $productId))
				->getFirstItem()
				->getPath();
			$url = $this->getUrl($path);
		}

		// OPTION #2: try to get a rewrite from the core_url_rewrite table
		if(empty($url))
		{
			$path = Mage::getModel('core/url_rewrite')->loadByIdPath('product/' . $product->getId())->getRequestPath();
			$url = $this->getUrl($path);
		}

		// OPTION #3: get the URL from the core Magento product model (last resort)
		if(empty($url))
		{
			$url = $product->getProductUrl();
		}

		return $url;
	}

} 