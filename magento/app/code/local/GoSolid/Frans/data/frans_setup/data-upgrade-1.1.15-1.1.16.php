<?php

/* create reports */
$categoryTitle = 'Pending Quote';
$quoteCategory = Mage::getModel('sqlReports/sqlReportCategory');

$quoteCategory->load($categoryTitle, 'title');

if (!$quoteCategory->getId())
{
    Mage::log("Creating SqlReports category $categoryTitle");
    // doesn't exist, create.
    $quoteCategory->setTitle($categoryTitle);
    $quoteCategory->setPosition(1);

    $quoteCategory->save();
}


// now do the actual reports
$reportsForCategoryAndTitle = Mage::getModel("sqlReports/sqlReport")
    ->getCollection()
    ->addFieldToFilter("category_id", $quoteCategory->getId());

$reportByItemTitle = $categoryTitle . ' - Items';
$reportsForCategoryAndTitle->addFieldToFilter('title', $reportByItemTitle)->load();

if ($reportsForCategoryAndTitle->count() == 0)
{
    // need to create it
    $sql = <<<ENDOFSQL
SELECT
	sfq.entity_id AS "Quote #"
	, IF(STRCMP(IFNULL(sfq.customer_firstname,"NULL"),"NULL"),CONCAT(sfq.customer_firstname,' ',sfq.customer_lastname),CONCAT(cevFname.value,' ',cevLname.value)) AS "Customer Name"
	, IF(STRCMP(IFNULL(sfqas.company,"NULL"),"NULL"),sfqas.company, cevBname.value) AS "Customer Company"
	, CONCAT(sfqab.firstname, ' ', sfqab.lastname) AS "Billing Name"
	, DATE(CONVERT_TZ(sfq.created_at, '+0:00', '-07:00')) AS "Created On"
	, DATE(IFNULL(sfq.ship_date,IFNULL(sfqaShipping.planned_ship_date,sfqas.planned_ship_date))) AS "Expected Ship Date"
	, CONCAT(FORMAT(sfqi.qty,0), 'x ',sfqi.name, ' (',sfqi.sku,')') AS "Product Name and Quantity SKU"
	, sfqi.notes AS "Product Notes"
	, sfqi.base_row_total_incl_tax AS "Grand Total"
	, IFNULL(sfqaShipping.city,sfqas.city) AS "Shipping City"
	, IFNULL(sfqaShipping.region,sfqas.region) AS "Shipping State"
	, sfq.admin_notes AS "Admin Notes"
FROM sales_flat_quote_item  AS sfqi
INNER JOIN sales_flat_quote AS sfq
	ON sfq.entity_id = sfqi.quote_id

LEFT JOIN sales_flat_quote_address AS sfqas
	ON sfqas.quote_id = sfqi.quote_id AND sfqas.address_type = "shipping"

LEFT JOIN sales_flat_quote_address AS sfqab
	ON sfqab.quote_id = sfqi.quote_id AND sfqab.address_type = "billing"

LEFT JOIN sales_flat_quote_address_item AS sfqai
	ON sfqai.quote_item_id = sfqi.item_id

LEFT JOIN sales_flat_quote_address AS sfqaShipping
	ON sfqaShipping.address_id = sfqai.quote_address_id

LEFT JOIN customer_entity AS ce
	ON ce.entity_id = sfq.customer_id
LEFT JOIN customer_entity_varchar AS cevFname
	ON cevFname.entity_id = sfq.customer_id AND cevFname.attribute_id = 5
LEFT JOIN customer_entity_varchar AS cevLname
	ON cevLname.entity_id = sfq.customer_id AND cevLname.attribute_id = 7
LEFT JOIN customer_entity_varchar AS cevBname
	ON cevBname.entity_id = sfq.customer_id AND cevBname.attribute_id = 138
WHERE  saved_quote = 1
GROUP BY sfqi.quote_id, sfqi.product_id
ORDER BY sfq.created_at DESC;
ENDOFSQL;
    $reportByItem = Mage::getModel('sqlReports/sqlReport')
        ->setSql($sql)
        ->setCategoryId($quoteCategory->getId())
        ->setTitle($reportByItemTitle);
    $reportByItem->save();

    Mage::log("Created report '$reportByItemTitle', has ID of {$reportByItem->getId()}");
}
else
{
    Mage::log("Report '$reportByItemTitle' already exists, not creating.");
}