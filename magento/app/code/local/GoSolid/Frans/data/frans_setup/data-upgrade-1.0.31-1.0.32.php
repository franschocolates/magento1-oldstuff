<?php

/* create categories */
$categoryTitle = 'Commerce Reports';
$category = Mage::getModel('sqlReports/sqlReportCategory');

$category->load($categoryTitle, 'title');

if (!$category->getId())
{
	Mage::log("Creating SqlReports category $categoryTitle");
	// doesn't exist, create.
	$category->setTitle($categoryTitle);
	$category->setPosition(7);
	
	$category->save();
}


// now do the actual reports
$reportsForCategoryAndTitle = Mage::getModel("sqlReports/sqlReport")
						->getCollection()
  						->addFieldToFilter("category_id", $category->getId());
  						
$reportTitle = 'Commerce Reports - Gift Cards - Redeemed';
$reportsForCategoryAndTitle->addFieldToFilter('title', $reportTitle)
					->load();

if ($reportsForCategoryAndTitle->count() == 0)
{
	// need to create it
	$sql = <<<ENDOFSQL
SELECT
	sfo.increment_id AS "Order Number"
	, sfo.entity_id AS order_view
	, CONVERT_TZ(sfo.created_at, '+0:00', '-07:00') AS "Order Date"
	, sfo.status
	, DATE(CONVERT_TZ(sfo.captured_at, '+0:00', '-07:00')) AS "Billed Date"
	, CONVERT_TZ(sfo.created_at, '+0:00', '-07:00') AS "Redeemed Date"
	, giftcard_amount AS "Amount Redeemed"
	, giftcard_number AS "Gift Card Number"
	, ggo.notes AS "Comments"
	, fa.created_by AS "User Name"
FROM
	sales_flat_order AS sfo
LEFT OUTER JOIN
	gosolid_givex_giftcard AS ggo
ON
	ggo.card_number = sfo.giftcard_number
LEFT OUTER JOIN
	frans_activity AS fa
ON
	fa.entity_id = sfo.entity_id
AND
	fa.activity_type = 'order_created'
AND
	fa.entity_type = 'sales/order'
WHERE
	LENGTH(sfo.giftcard_number) > 0
AND
	DATE(CONVERT_TZ(sfo.created_at, '+0:00', '-07:00')) BETWEEN '{Start date:date}' AND '{End date:date}'
ENDOFSQL;
	$report = Mage::getModel('sqlReports/sqlReport')
							->setSql($sql)
							->setCategoryId($category->getId())
                            ->setPosition(2)
							->setTitle($reportTitle);
	$report->save();
	
	Mage::log("Created report '$reportTitle', has ID of {$report->getId()}");
}
else
{
	Mage::log("Report '$reportTitle' already exists, not creating.");
}

