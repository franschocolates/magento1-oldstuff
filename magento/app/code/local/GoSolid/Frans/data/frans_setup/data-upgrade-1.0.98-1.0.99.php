<?php

$installer = $this;
$installer->startSetup();

// change the content on the home page (note that CSS now hides the text but we keep it in the markup for SEO/accessibility)
$content = <<<EOT
<h2>Elegant favors your guests will remember.</h2>
<h3><a href="{{config path="web/unsecure/base_url"}}wedding-event-favors/wedding-favors">Shop our favors</a></h3>
EOT;

if ($cmsPage = Mage::getModel('cms/page')->load('home', 'identifier')){
	$cmsPage->setContent($content);
	$cmsPage->save();
}

$installer->endSetup();