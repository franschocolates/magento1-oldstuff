<?php


    /** @var $categories Mage_Catalog_Model_Catalog */
    $categories = Mage::getModel('catalog/category')->getCollection();

    foreach($categories as $category)
    {
        $category->setIncludeInSubmenu(1);

        if($category->getParentId() == 32)
        {
            $category->setData('include_in_menu',0);
        }
        $category->save();
    }
