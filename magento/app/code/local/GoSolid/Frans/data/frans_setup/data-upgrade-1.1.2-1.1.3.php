<?php

// Gift Card Balance text is no longer controlled via CMS
$cmsBlock = Mage::getModel('cms/block')->load('gc-check-balance', 'identifier');
$cmsBlock->delete();
$cmsBlock->save();