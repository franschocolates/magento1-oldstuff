<?php

/* Create initial ship method descriptions / colors */
Mage::getModel('frans/shippingMethod')
    ->setShippingMethod('fedex_FEDEX_2_DAY')
    ->setLabel('FedEx Priority')
    ->setColor('#7e7e7e')
    ->save();

Mage::getModel('frans/shippingMethod')
    ->setShippingMethod('fedex_FEDEX_GROUND')
    ->setLabel('FedEx Ground')
    ->setColor('#b9b9b9')
    ->save();

Mage::getModel('frans/shippingMethod')
    ->setShippingMethod('fedex_STANDARD_OVERNIGHT')
    ->setLabel('FedEx Overnight')
    ->setColor('#505050')
    ->save();

Mage::getModel('frans/shippingMethod')
    ->setShippingMethod('fedex_GROUND_HOME_DELIVERY')
    ->setLabel('FedEx Ground')
    ->setColor('#b9b9b9')
    ->save();
