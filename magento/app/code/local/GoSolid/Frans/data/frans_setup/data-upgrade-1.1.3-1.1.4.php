<?php
// the only carriers we currently need to suppress from calling are fedex & usps
Mage::getModel('core/config')->saveConfig('shipping/rules/fixed_rate_carriers', 'fedex,usps');