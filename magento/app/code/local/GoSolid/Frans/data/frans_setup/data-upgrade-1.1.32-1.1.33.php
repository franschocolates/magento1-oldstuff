<?php

$installer = $this;
$installer->startSetup();

// create new New Website CMS page
$content = <<<EOF
<h1 class="page-title">Our Commitment</h1>
<p>
Nothing is more important to us than your complete satisfaction. We stand behind everything that we make, our confections are hand-crafted and carefully hand-packed to ensure your order arrives at the peak of freshness. If your order does not meet your expectations, please call us at {{config path="general/store_information/tollfree_number"}}, and we will do whatever it takes to make it right for you.
</p>
EOF;


Mage::getModel('cms/page')
	->setTitle('Our Guarantee')
	->setIdentifier('guarantee')
	->setContent($content)
	->setisActive(1)
	->setRootTemplate('one_column')
	->setStoreId(array(1))
	->save();

$installer->endSetup();