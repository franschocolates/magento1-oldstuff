<?php

$installer = $this;
$installer->startSetup();

// Configuration for newly installed module CueBlocks_SitemapEnhancedPlus

$configModel = new Mage_Core_Model_Config();

$configModel ->saveConfig('sitemap_enhanced_plus/general/product_link_type', "category");
$configModel ->saveConfig('sitemap_enhanced_plus/cron/enabled', "1");
$configModel ->saveConfig('sitemap_enhanced_plus/cron/frequency', "D");
$configModel ->saveConfig('crontab/jobs/sitemapEnhancedPlus_generate/schedule/cron_expr', "0 3 * * *");
$configModel ->saveConfig('crontab/jobs/sitemapEnhancedPlus_generate/run/model', "sitemapEnhancedPlus/observer::cronGenerateSitemap");
$configModel ->saveConfig('sitemap_enhanced_plus/cron/time', "03,00,00");
$configModel ->saveConfig('sitemap_enhanced_plus/cron/report_enabled', "0");
$configModel ->saveConfig('sitemap_enhanced_plus/ping/enabled', "0");

$installer->endSetup();