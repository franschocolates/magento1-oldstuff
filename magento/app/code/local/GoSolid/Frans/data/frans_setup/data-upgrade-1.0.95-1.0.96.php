<?php
/**
 * Created by goSolid.
 * Date: 7/21/14
 * Time: 1:51 PM
 */

$installer = $this;
$installer->startSetup();

$content = <<<EOT
<h2>Shipping Information</h2>
<p>We are committed to delivering your chocolates fresh and in the best possible condition. Due to the perishable nature of our confections we ship exclusively via one or two day shipping methods.</p>
<p>We carefully handcraft and hand-package all of our chocolates to maintain quality. If forecasted temperatures are warm, your order will be shipped with ice packs and placed in insulated packaging at no additional charge.</p>
<p>If temperatures are above 78 degrees at the recipient's destination, we recommend choosing overnight shipping. Please also alert recipients that a sweet gift is on its way so the package is not left outside in warm weather for an extended period of time.</p>
EOT;

$cmsBlock = Mage::getModel('cms/block')->load('shipping-information-popup', 'identifier');
if($cmsBlock->isObjectNew()) {
	$cmsBlock->setIdentifier('shipping-information-popup')
		->setStores(array(0))
		->setIsActive(1)
		->setTitle('Shipping Information Popup');
}
$cmsBlock->setContent($content)->save();

$installer->endSetup();
