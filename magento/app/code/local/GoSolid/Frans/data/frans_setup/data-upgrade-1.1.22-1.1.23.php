<?php

$installer = $this;
$installer->startSetup();

// New content for home page (use the Banner module now)
$content = <<<EOHTML
{{block type="banner/banner" banner_type="home_hero_banner" template="banner/home-hero-banner.phtml"}}
EOHTML;

// Update page title and save the new content
$cmsPage = Mage::getModel('cms/page')->load('home');

$cmsPage->setContent($content)->save();

$installer->endSetup();