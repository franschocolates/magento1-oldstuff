<?php

/* update the payment methods that use pending to use processing */
Mage::getModel('core/config')->saveConfig('payment/checkmo/order_status', 'processing' );
Mage::getModel('core/config')->saveConfig('payment/pay_at_pickup/order_status', 'processing' );
Mage::getModel('core/config')->saveConfig('payment/paybyinvoice/order_status', 'processing' );