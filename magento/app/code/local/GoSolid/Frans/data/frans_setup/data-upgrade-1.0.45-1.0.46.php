<?php

/* Make sure we use the right block for boutique CMS page */
if ($boutiquePage = Mage::getModel('cms/page')->load('locations', 'identifier'))
{
    if ($boutiquePage->getId())
    {
        // make sure it uses the correct block
        $badBlockName = 'frans/adminhtml_retailstore';
        if (stristr($boutiquePage->getContent(), $badBlockName) !== false)
        {
            Mage::log("Updating retail store page to use correct block");
            $boutiquePage->setContent(str_replace($badBlockName, 'core/template', $boutiquePage->getContent()));
            $boutiquePage->save();
        }
    }
}


