<?php

/* create reports */
$categoryTitle = 'Items on Order';
$salesCategory = Mage::getModel('sqlReports/sqlReportCategory');

$salesCategory->load($categoryTitle, 'title');

if (!$salesCategory->getId())
{
	Mage::log("Creating SqlReports category $categoryTitle");
	// doesn't exist, create.
    $salesCategory->setTitle($categoryTitle);
    $salesCategory->setPosition(1);

    $salesCategory->save();
}


// now do the actual reports
$reportsForCategoryAndTitle = Mage::getModel("sqlReports/sqlReport")
                                            ->getCollection()
                                            ->addFieldToFilter("category_id", $salesCategory->getId());
  						
$reportByItemTitle = 'Items on Order - By Item (Not Printed)';
$reportsForCategoryAndTitle->addFieldToFilter('title', $reportByItemTitle)->load();

if ($reportsForCategoryAndTitle->count() == 0)
{
	// need to create it
	$sql = <<<ENDOFSQL
SELECT
	`sku`, `name` AS 'Product Name', SUM(sfoi.qty_ordered - (sfoi.qty_shipped + sfoi.qty_canceled)) AS 'Quantity'
FROM
	sales_flat_order_item AS sfoi
INNER JOIN
	sales_flat_order AS sfo
ON
	sfo.entity_id = sfoi.order_id
WHERE
	(sfoi.qty_ordered - (sfoi.qty_shipped + sfoi.qty_canceled)) > 0
AND
	sfo.`status` IN ('processing', 'ready_for_review', 'ready_to_print', 'holded')
AND
	DATE(sfo.planned_ship_date) BETWEEN '{Start Date:date}' AND '{End Date:date}'
GROUP BY
	sfoi.`sku`, sfoi.`name`
ORDER BY
	SUM(sfoi.qty_ordered - (sfoi.qty_shipped + sfoi.qty_canceled)) DESC
ENDOFSQL;
	$reportByItem = Mage::getModel('sqlReports/sqlReport')
							->setSql($sql)
							->setCategoryId($salesCategory->getId())
							->setTitle($reportByItemTitle);
	$reportByItem->save();
	
	Mage::log("Created report '$reportByItemTitle', has ID of {$reportByItem->getId()}");
}
else
{
	Mage::log("Report '$reportByItemTitle' already exists, not creating.");
}