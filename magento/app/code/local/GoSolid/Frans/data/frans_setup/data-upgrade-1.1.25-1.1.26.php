<?php

$installer = $this;
$installer->startSetup();

// create new New Website CMS page
$content = <<<EOT
<h1 class="page-title">Take a peek and see what's new</h1>
<p>Our new website is brimming with simply brilliant ways to make online shopping with us even more delicious. Each new feature brings you more of the handcrafted, family-owned experience we love sharing with you. Nothing makes us happier than your delight, so please feel free to <a href="{{config path="web/unsecure/base_url"}}contact">contact us</a> if we can help you in any way possible as you explore and enjoy what&rsquo;s new.</p>
<ul>
<li>
<div class="image">
<div class="vertical-align"><img src="{{media url="wysiwyg/img-assorted-ribbon.png"}}" alt="Assorted Ribbon &amp; Box Colors" /></div>
</div>
<div class="content">
<div class="vertical-align">
<h2>Assorted Ribbon &amp; Box Colors</h2>
<p>You can now select seasonal ribbon colors, as well as our signature ribbons, together with their corresponding boxes.</p>
</div>
</div>
</li>
<li>
<div class="image">
<div class="vertical-align"><img src="{{media url="wysiwyg/img-arrival-date.png"}}" alt="Choose Your Preferred Arrival Date" /></div>
</div>
<div class="content">
<div class="vertical-align">
<h2>Choose Your Preferred Arrival Date</h2>
<p>Our online calendar invites you to specify your preferred arrival date for special occasions. We&rsquo;ll coordinate with you to arrange shipment as close as possible to that date so your gift arrives when you want it to.</p>
</div>
</div>
</li>
<li>
<div class="image">
<div class="vertical-align"><img src="{{media url="wysiwyg/img-multi-ship.png"}}" alt="Multiple Shipments Are a Breeze" /></div>
</div>
<div class="content">
<div class="vertical-align">
<h2>Multiple Shipments Are a Breeze</h2>
<p>Our interactive tool makes it easy to ship to as many people as you wish.</p>
</div>
</div>
</li>
<li>
<div class="image">
<div class="vertical-align"><img src="{{media url="wysiwyg/img-seasonal.png"}}" alt="Seasonal &amp; Holiday Gift Collections" /></div>
</div>
<div class="content">
<div class="vertical-align">
<h2>Seasonal &amp; Holiday Gift Collections</h2>
<p>Taking a cue from the vibrant colors of the season, our hand-tied satin bows and boxes are coordinated to celebrate the times. Check back often to see our holiday specialties and our spring, summer, winter and fall products as well.</p>
</div>
</div>
</li>
<li>
<div class="image">
<div class="vertical-align"><img src="{{media url="wysiwyg/img-communication.png"}}" alt="Customize Our Communications With You" /></div>
</div>
<div class="content">
<div class="vertical-align">
<h2>Customize Our Communications With You</h2>
<p>We love being in touch with you and want to make sure that the frequency is to your liking. Feel free to share your <a href="{{config path="web/secure/base_url"}}newsletter/manage/">communication preferences</a> with us. Choose whether or not you would like to receive a catalog, keep birthday dates on file, learn about our new product arrivals and exclusive store events, and stay informed of other surprises along the way!</p>
</div>
</div>
</li>
<li>
<div class="image">
<div class="vertical-align"><img src="{{media url="wysiwyg/img-mobile.png"}}" alt="Shop Even Faster on Mobile" /></div>
</div>
<div class="content">
<div class="vertical-align">
<h2>Shop Even Faster on Mobile</h2>
<p>Our new website works beautifully on smart phones and tablets, so you can be in touch with Fran&rsquo;s whenever and wherever you like.</p>
</div>
</div>
</li>
</ul>
<div class="more-to-come"><img src="{{media url="wysiwyg/img-more-to-come.png"}}" alt="More to Come" />
<h2>More to Come</h2>
<p>Gift subscriptions, advanced search capabilities, back-by-request product notifications, pick-up in stores, birthday reminders and much more is coming. We look forward to delighting you every time you visit us online.</p>
</div>
EOT;

$newWebsitePage = Mage::getModel('cms/page')->load('new-website', 'identifier');
$newWebsitePage->setContent($content)->save();

$installer->endSetup();