<?php

$noticeContent = '<p>Please use your email address to access your existing account. If you need further assistance, contact us at {{config path="general/store_information/tollfree_number"}}.</p>';

$getLoginNotice = Mage::getModel('cms/block')
    ->load('login-notice', 'identifier');

//Set the new content to Login Notice
if ($getLoginNotice->getBlockId())
{
    $getLoginNotice->setContent($noticeContent)->save();
}
