<?php

/* add the necessary data for the order item note categories */
$noteCategories = array(
        'Business Gifting',
        'I\'m Sorry Gift',
        'Item Not Received - Exact Replacement',
        'Item Not Received - I\'m Sorry Gift',
        'Product Replacement - Damaged',
        'Product Replacement - Exact',
        'Product Replacement - Melted',
        'Product Replacement - New/Additional Item',
        'Wedding Samples',
        'Wrong Item Received - Entry Error',
        'Wrong Item Received - Picking Error'
);

foreach ($noteCategories as $category)
{
    $model = Mage::getModel('frans/orderItemNoteCategory')
                ->setCategoryLabel($category)
                ->save();
}


