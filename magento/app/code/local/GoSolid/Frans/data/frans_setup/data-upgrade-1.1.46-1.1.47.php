<?php
$installer  =  $this;
$installer->startSetup();

$attr = Mage::getResourceModel('catalog/eav_attribute')->loadByCode('catalog_category','tile_label');
if (!$attr->getId()) {
    $this->addAttribute('catalog_category', 'tile_label', array(
        'type'         => 'text',
        'input'        => 'text',
        'visible'      => true,
        'required'     => false,
        'user_defined' => false,
        'default'      => null,
        'group'        => 'General Information',
        'label'        => 'Tile Label',
        'position'     => '110',
        'sort_order'   => 110,
        'note'         => 'Title displayed on color tiles. If blank, the category\'s name will be used.',
        'global'       => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
    ));
}

$installer->endSetup();