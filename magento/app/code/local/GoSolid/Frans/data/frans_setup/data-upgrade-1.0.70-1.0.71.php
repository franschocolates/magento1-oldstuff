<?php
$installer = $this;
$installer->startSetup();

// address needs to be required

$installer->updateAttribute(
    'customer_address',
    'telephone',
    'is_required',
    1
);

$installer->endSetup();