<?php
// insert any data inserts/changes here
// as an example, CMS blocks to create/update

$corpLinks = <<<EOT
<ul>
<li><a class="corplink-story" href="{{store direct_url="story"}}">Fran's Story</a></li>
<li><a class="corplink-boutiques" href="{{store direct_url="locations"}}">Boutiques</a></li>
<li><a class="corplink-gifting" href="{{store direct_url="about/business-gifting"}}">Business Gifting</a></li>
<li><a class="corplink-news" href="{{store direct_url="articles"}}">News</a></li>
<li><a class="corplink-custcare" href="{{store direct_url="service"}}">Customer Care</a></li>
<li><a class="corplink-contact" href="{{store direct_url="contact"}}">Contact Us</a></li>
</ul>
EOT;

//Update corporate links
$getCorpLinks = Mage::getModel('cms/block')
    ->load('corporate_links', 'identifier');

if ($getCorpLinks->getBlockId())
{
    $getCorpLinks->setContent($corpLinks)->save();;
}

//Create redirect for about/business-gifting -> frans/businessgifting
$getRewriteModel1 = Mage::getModel('core/url_rewrite')->load('about/business-gifting', 'request_path');
$getRewriteModel1
    ->setIdPath('about-business-gifting-page')
    ->setRequestPath('about/business-gifting')
    ->setStoreId('1')
    ->setTargetPath('frans/businessgifting')
    ->setIsSystem('0')
    ->setOptions('')
    ->save();

//Create redirect for business-gifting -> gifts/business-gifting
$getRewriteModel2 = Mage::getModel('core/url_rewrite')->load('business-gifting', 'request_path');
$getRewriteModel2
    ->setIdPath('gifts-business-gifting-category')
    ->setRequestPath('business-gifting')
    ->setStoreId('1')
    ->setTargetPath('catalog/category/view/id/36')
    ->setIsSystem('0')
    ->setOptions('')
    ->save();