<?php

/* create reports */
$categoryTitle = 'Sales';
$salesCategory = Mage::getModel('sqlReports/sqlReportCategory');

$salesCategory->load($categoryTitle, 'title');

if (!$salesCategory->getId())
{
	Mage::log("Creating SqlReports category $categoryTitle");
	// doesn't exist, create.
    $salesCategory->setTitle($categoryTitle);
    $salesCategory->setPosition(1);

    $salesCategory->save();
}


// now do the actual reports
$reportsForCategoryAndTitle = Mage::getModel("sqlReports/sqlReport")
                                            ->getCollection()
                                            ->addFieldToFilter("category_id", $salesCategory->getId());
  						
$reportByItemTitle = 'Sales by Captured Date';
$reportsForCategoryAndTitle->addFieldToFilter('title', $reportByItemTitle)->load();

if ($reportsForCategoryAndTitle->count() == 0)
{
	// need to create it
	$sql = <<<ENDOFSQL
SELECT
	DATE(CONVERT_TZ(sfo.captured_at, '+0:00', '-07:00')) AS "Captured Date"
	, COUNT(*) AS "Total Orders"
	, (SELECT COUNT(*)
	   FROM sales_flat_shipment
	   WHERE ship_date=DATE(CONVERT_TZ(sfo.captured_at, '+0:00', '-07:00'))) AS "Total Shipments"

	, CONCAT('$', FORMAT(SUM(sfo.base_subtotal + IFNULL(sfo.base_discount_amount,0)),2)) AS "Subtotal"
	, CONCAT('$', FORMAT(SUM(sfo.base_shipping_amount),2)) AS "Shipping"
	, CONCAT('$', FORMAT(SUM(sfo.base_grand_total + IFNULL(sfo.base_giftcard_amount,0)),2)) AS "Grand Total"
FROM
	sales_flat_order AS sfo
WHERE
	sfo.accounting_label IN ({Accounting Label:multiselect[SELECT label_name AS `label`, label_name AS `value` FROM `frans_accounting_label`]})
	AND sfo.origination IN ({Origination:multiselect[SELECT origination AS `label`, origination AS `value` FROM `sales_flat_order` GROUP BY origination]})
	AND DATE(CONVERT_TZ(sfo.captured_at, '+0:00', '-07:00')) BETWEEN '{Start Date:date}' AND '{End Date:date}'
	AND sfo.status IN ({ORDER STATUS:multiselect[SELECT UPPER(REPLACE(`status`,'_',' ')) AS `label`, `status` AS `value` FROM sales_flat_order GROUP BY `status`]})
GROUP BY
	DATE(CONVERT_TZ(sfo.captured_at, '+0:00', '-07:00'));
ENDOFSQL;
	$reportByItem = Mage::getModel('sqlReports/sqlReport')
							->setSql($sql)
							->setCategoryId($salesCategory->getId())
							->setTitle($reportByItemTitle);
	$reportByItem->save();
	
	Mage::log("Created report '$reportByItemTitle', has ID of {$reportByItem->getId()}");
}
else
{
	Mage::log("Report '$reportByItemTitle' already exists, not creating.");
}