<?php
// set a label for any salesrules that are missing it
// should just match the discount
$salesrules = Mage::getModel('salesrule/rule')->getCollection();

foreach ($salesrules as $salesRule)
{
    if (false === $salesRule->getStoreLabel())
    {
        // doesn't have a default, add one
        $salesRule->setStoreLabels(
            array(0 => $salesRule->getName())
            )->save();
    }
}