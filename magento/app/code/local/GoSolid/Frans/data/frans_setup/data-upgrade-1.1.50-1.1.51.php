<?php

$installer = $this;
$installer->startSetup();

$reportTitle = 'Orders Shipped Not Captured';

$sql = <<<ENDOFSQL
# Child Orders Shipped NotCaptured, is 4 unioned queries
# Single Shipment Orders = shipped but not captured
SELECT
	entity_id AS ID
	, increment_id AS 'Order Number'
	, entity_id AS order_view
	, `status` AS 'Status'
	, planned_ship_date AS 'Planned Ship Date'
	, shipping_description AS 'Shipping Description'
	, IF(captured_at IS NOT NULL, 'Yes', 'No') AS 'Captured'
	, sales_flat_order.admin_notes AS 'Admin Notes'
FROM
	sales_flat_order
WHERE
	shipping_method NOT LIKE 'pickupatstore_%'
AND
	ship_date IS NOT NULL
AND
	captured_at IS NULL
AND
	is_multiship_parent IS NULL
AND
	multiship_parent_id IS NULL
UNION

# Multi Shipment Orders = Any child shipped and Parent not captured
SELECT
	sfoChild.entity_id AS ID
	, sfoChild.increment_id AS 'Order Number'
	, sfoChild.entity_id AS order_view
	, sfoChild.status AS 'Status'
	, sfoChild.planned_ship_date AS 'Planned Ship Date'
	, sfoChild.shipping_description AS 'Shipping Description'
	, IF(sfoChild.captured_at IS NOT NULL, 'Yes', 'No') AS 'Captured'
	, sfoParent.admin_notes AS 'Admin Notes'
FROM
	sales_flat_order AS sfoParent
LEFT JOIN
	sales_flat_order AS sfoChild ON sfoParent.entity_id = sfoChild.multiship_parent_id AND sfoChild.ship_date IS NOT NULL
WHERE
	sfoParent.is_multiship_parent IS NOT NULL
AND
	sfoChild.multiship_parent_id IS NOT NULL
AND
	sfoParent.captured_at IS NULL
UNION

# Pick Up Orders = Ready to be picked up and not captured
SELECT
	entity_id AS ID
	, increment_id AS 'Order Number'
	, entity_id AS order_view
	, `status` AS 'Status'
	, planned_ship_date AS 'Planned Ship Date'
	, shipping_description AS 'Shipping Description'
	, IF(captured_at IS NOT NULL, 'Yes', 'No') AS 'Captured'
	, sales_flat_order.admin_notes AS 'Admin Notes'
FROM
	sales_flat_order
WHERE
	`status` = 'ready_for_pickup'
AND
	shipping_method LIKE 'pickupatstore_%'
AND
	captured_at IS NULL
AND
	is_multiship_parent IS NULL
AND
	multiship_parent_id IS NULL
UNION

# Child Pick Up Orders = Ready to be picked up and not captured
SELECT
	sfoChild.entity_id AS ID
	, sfoChild.increment_id AS 'Order Number'
	, sfoChild.entity_id AS order_view
	, sfoChild.status  AS 'Status'
	, sfoChild.planned_ship_date AS 'Planned Ship Date'
	, sfoChild.shipping_description AS 'Shipping Description'
	, IF(sfoParent.captured_at IS NOT NULL, 'Yes', 'No') AS 'Captured'
	, sfoParent.admin_notes AS 'Admin Notes'
FROM
	sales_flat_order AS sfoParent
LEFT JOIN
	sales_flat_order AS sfoChild ON sfoParent.entity_id = sfoChild.multiship_parent_id AND sfoChild.ship_date IS NOT NULL
WHERE
	sfoParent.is_multiship_parent IS NOT NULL
AND
	sfoChild.multiship_parent_id IS NOT NULL
AND
	sfoChild.shipping_method LIKE 'pickupatstore_%'
AND
	sfoParent.captured_at IS NULL
AND
	sfoChild.multiship_parent_id IS NOT NULL
ENDOFSQL;

if (Mage::getModel("sqlReports/sqlReport")->load($reportTitle ,'title')->getId()) {
    $report = Mage::getModel("sqlReports/sqlReport")->load($reportTitle, 'title')
        ->setSql($sql);
    $report->save();
}
$installer->endSetup();












