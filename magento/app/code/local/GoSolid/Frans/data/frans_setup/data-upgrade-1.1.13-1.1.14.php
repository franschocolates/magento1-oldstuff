<?php

/* create reports */
$categoryTitle = 'Shipments';
$salesCategory = Mage::getModel('sqlReports/sqlReportCategory');

$salesCategory->load($categoryTitle, 'title');

if (!$salesCategory->getId())
{
	Mage::log("Creating SqlReports category $categoryTitle");
	// doesn't exist, create.
    $salesCategory->setTitle($categoryTitle);
    $salesCategory->setPosition(1);

    $salesCategory->save();
}


// now do the actual reports
$reportsForCategoryAndTitle = Mage::getModel("sqlReports/sqlReport")
                                            ->getCollection()
                                            ->addFieldToFilter("category_id", $salesCategory->getId());
  						
$reportByItemTitle = 'Shipments - Shipped Dates';
$reportsForCategoryAndTitle->addFieldToFilter('title', $reportByItemTitle)->load();

if ($reportsForCategoryAndTitle->count() == 0)
{
	// need to create it
	$sql = <<<ENDOFSQL
SELECT
	DATE(CONVERT_TZ(sfs.created_at, '+0:00', '-07:00')) AS "Ship Date"
	, COUNT(*) AS "Total Shipments"
	, CONCAT('$', FORMAT(SUM(sfo.base_shipping_amount),2)) AS "Shipping Total"
	, CONCAT('$', FORMAT(SUM(sfo.base_subtotal + IFNULL(sfo.base_discount_amount,0)),2)) AS "Grand Total (No Shipping)"
FROM
	sales_flat_shipment AS sfs
INNER JOIN sales_flat_order AS sfo
    ON sfo.entity_id = sfs.order_id
WHERE sfo.accounting_label IN ({Accounting Label:multiselect[SELECT label_name AS `label`, label_name AS `value` FROM `frans_accounting_label`]})
    AND sfo.origination IN ({Origination:multiselect[SELECT origination AS `label`, origination AS `value` FROM `sales_flat_order` GROUP BY origination]})
	AND DATE(CONVERT_TZ(sfs.created_at, '+0:00', '-07:00')) BETWEEN '{Start Date:date}' AND '{End Date:date}'
GROUP BY
    DATE(CONVERT_TZ(sfs.created_at, '+0:00', '-07:00'))
ENDOFSQL;
	$reportByItem = Mage::getModel('sqlReports/sqlReport')
							->setSql($sql)
							->setCategoryId($salesCategory->getId())
							->setTitle($reportByItemTitle);
	$reportByItem->save();
	
	Mage::log("Created report '$reportByItemTitle', has ID of {$reportByItem->getId()}");
}
else
{
	Mage::log("Report '$reportByItemTitle' already exists, not creating.");
}