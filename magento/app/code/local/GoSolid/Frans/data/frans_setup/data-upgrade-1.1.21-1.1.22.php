<?php
$installer = $this;
$installer->startSetup();

$printedStatus = Mage::getModel('sales/order_status')
    ->load('printed');
$printedValues = "ready_for_review,ready_to_print,processing";


$printedStatus->setAllowedActions($printedValues)
    ->save();

$installer->endSetup();