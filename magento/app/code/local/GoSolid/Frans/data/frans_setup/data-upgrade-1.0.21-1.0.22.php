<?php

/* create reports */
$categoryTitle = 'Shipments';
$shipmentsCategory = Mage::getModel('sqlReports/sqlReportCategory');

$shipmentsCategory->load($categoryTitle, 'title');

if (!$shipmentsCategory->getId())
{
	Mage::log("Creating SqlReports category $categoryTitle");
	// doesn't exist, create.
	$shipmentsCategory->setTitle($categoryTitle);
	$shipmentsCategory->setPosition(4);
	
	$shipmentsCategory->save();
}


// now do the actual reports
$reportsForCategoryAndTitle = Mage::getModel("sqlReports/sqlReport")
						->getCollection()
  						->addFieldToFilter("category_id", $shipmentsCategory->getId());
  						
$billedShipmentsTitle = 'Shipments - Billed';
$reportsForCategoryAndTitle->addFieldToFilter('title', $billedShipmentsTitle)
					->load();

if ($reportsForCategoryAndTitle->count() == 0)
{
	// need to create it
$preSql = <<<ENDOFPRESQL
DROP TEMPORARY TABLE IF EXISTS tmp_BilledOrders;
DROP TEMPORARY TABLE IF EXISTS tmp_TrackingNumbers;
DROP TEMPORARY TABLE IF EXISTS tmp_Products;


CREATE TEMPORARY TABLE tmp_BilledOrders
(
	order_id INT NOT NULL PRIMARY KEY,
	parent_id INT NULL,
	invoice_created_date DATETIME NOT NULL
);

CREATE TEMPORARY TABLE tmp_TrackingNumbers
(
	order_id INT NOT NULL PRIMARY KEY
	, tracking_numbers TEXT
);

CREATE TEMPORARY TABLE tmp_Products
(
	order_id INT NOT NULL PRIMARY KEY
	, products TEXT
);

/* identify all orders that are involved, based on billed date */
INSERT INTO tmp_BilledOrders
(
	order_id
	, parent_id
	, invoice_created_date
)
SELECT
	sfo.entity_id
	, sfp.entity_id
	, sfi.created_at
FROM
	sales_flat_order AS sfo
LEFT OUTER JOIN 
	sales_flat_order AS sfp
ON
	sfp.entity_id = sfo.multiship_parent_id
INNER JOIN
	sales_flat_invoice AS sfi
ON
	sfi.order_id = IFNULL(sfp.entity_id, sfo.entity_id)
WHERE
	(sfo.is_multiship_parent IS NULL OR sfo.is_multiship_parent = 0)
AND 
	DATE(sfi.created_at) BETWEEN '{Start date:date}' AND '{End date:date}';
	
/* determine tracking numbers for all of our orders */
INSERT INTO tmp_TrackingNumbers
(
	order_id, tracking_numbers
)
SELECT sfs.order_id, GROUP_CONCAT(track_number SEPARATOR ',') AS tracking_numbers
FROM sales_flat_shipment_track AS sfst
INNER JOIN sales_flat_shipment AS sfs
ON sfs.entity_id = sfst.parent_id
INNER JOIN 
	tmp_BilledOrders AS tbo
ON	
	tbo.order_id = sfs.order_id
GROUP BY sfs.order_id;

INSERT INTO tmp_Products
(
	order_id, products
)
SELECT
	sfoi.order_id
	, GROUP_CONCAT(CONCAT(FORMAT(sfoi.qty_invoiced, 0), ' ', sfoi.name) SEPARATOR ' | ')
FROM
	sales_flat_order_item AS sfoi
INNER JOIN
	tmp_BilledOrders tbo
ON
	tbo.order_id = sfoi.order_id
GROUP BY 
	sfoi.order_id;
ENDOFPRESQL;
	
	$sql = <<<ENDOFSQL
SELECT 
	COALESCE(sfp.increment_id, sfo.increment_id) AS shipmentgroupid
	, IF(INSTR(sfo.increment_id, '-') > 0, SUBSTRING(sfo.increment_id FROM 11), 1) AS shipmentnr
	, sfo.increment_id AS ship_id
	, sfo.ice_packs AS icepack_count
	, sfo.entity_id AS orderid
	, sfo.subtotal AS subtotal
	, sfo.discount_amount AS discount
	, sfo.tax_amount AS tax
	, sfo.shipping_amount AS shipping_cost
	, tn.tracking_numbers AS tracking
	, sfo.grand_total
	, COALESCE(sfp.grand_total, sfo.grand_total) AS order_grand_total
	, sfo.ship_date
	, DATE(sfo.created_at) AS order_date
	, DATE(tbo.invoice_created_date) AS order_status_date
	, 'billed' AS order_status
	, sfoa.prefix AS s_title
	, sfoa.firstname AS s_firstname
	, sfoa.lastname AS s_lastname
	, sfoa.street AS s_address
	, sfoa.city AS s_city
	, sfoa.region AS s_state
	, sfoa.postcode AS s_zipcode
	, sfoa.country_id AS s_country
	, sfob.prefix AS b_title
	, sfob.firstname AS b_firstname
	, sfob.lastname AS b_lastname
	, sfob.street AS b_address
	, sfob.city AS b_city
	, sfob.region AS b_state
	, sfob.postcode AS b_zipcode
	, sfob.country_id AS b_country
	, sfob.telephone AS phone
	, COALESCE(sfp.customer_email, sfo.customer_email) AS email
	, sn.note AS cutomer_notes
	, gm.message AS gift_message
	, sfo.admin_notes AS admi_note
	, sfo.shipping_description AS shipping_description
	, sfo.origination AS origination_level
	, sfo.accounting_label AS accounting_label
	, sfop.cc_type AS creditcard_type
	, sfo.weight AS total_weight
	, tp.products AS 'Products'
FROM
	sales_flat_order AS sfo
INNER JOIN
	tmp_BilledOrders AS tbo
ON
	tbo.order_id = sfo.entity_id
INNER JOIN
	sales_flat_order_address AS sfoa
ON
	sfoa.entity_id = sfo.shipping_address_id
LEFT OUTER JOIN
	sales_flat_order AS sfp
ON
	sfp.entity_id = tbo.parent_id
INNER JOIN
	sales_flat_order_address AS sfob
ON
	sfob.entity_id = COALESCE(sfp.billing_address_id, sfo.billing_address_id)
LEFT OUTER JOIN
	tmp_TrackingNumbers AS tn
ON
	tn.order_id = sfo.entity_id
LEFT OUTER JOIN
	tmp_Products AS tp
ON
	tp.order_id = sfo.entity_id
LEFT OUTER JOIN
	shipnote_note AS sn
ON
	sn.note_Id = sfo.ship_note_id
LEFT OUTER JOIN
	gift_message AS gm
ON
	gm.gift_message_id = sfo.gift_message_id
LEFT OUTER JOIN
	sales_flat_order_payment AS sfop
ON
	sfop.parent_id = IFNULL(sfp.entity_id, sfo.entity_id);
ENDOFSQL;
	$shipmentsBilledReport = Mage::getModel('sqlReports/sqlReport')
							->setPreSql($preSql)
							->setSql($sql)
							->setCategoryId($shipmentsCategory->getId())
							->setTitle($billedShipmentsTitle);
	$shipmentsBilledReport->save();
	
	Mage::log("Created report '$billedShipmentsTitle', has ID of {$shipmentsBilledReport->getId()}");
}
else
{
	Mage::log("Report '$billedShipmentsTitle' already exists, not creating.");
}

