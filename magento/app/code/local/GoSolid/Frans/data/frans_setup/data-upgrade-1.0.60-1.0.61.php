<?php
/**
 * Make quotes expire after 6 months or so
 */
Mage::getModel('core/config')->saveConfig('checkout/cart/delete_quote_after', 183);