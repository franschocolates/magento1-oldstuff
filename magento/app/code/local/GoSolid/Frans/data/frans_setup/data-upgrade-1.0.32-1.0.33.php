<?php

/* create categories */
$categoryTitle = 'Commerce Reports';
$category = Mage::getModel('sqlReports/sqlReportCategory');

$category->load($categoryTitle, 'title');

// now do the actual reports
$reportsForCategoryAndTitle = Mage::getModel("sqlReports/sqlReport")
						->getCollection()
  						->addFieldToFilter("category_id", $category->getId());
  						
$reportTitle = 'Commerce Reports - Gift Cards - Activated';
$reportsForCategoryAndTitle->addFieldToFilter('title', $reportTitle)
					->load();

if ($reportsForCategoryAndTitle->count() == 0)
{
	// need to create it
	$sql = <<<ENDOFSQL
SELECT
	sfo.increment_id AS "Order Number"
	, sfo.entity_id AS order_view
	, CONVERT_TZ(sfo.created_at, '+0:00', '-07:00') AS "Order Date"
	, sfo.status
	, CONVERT_TZ(sfo.captured_at, '+0:00', '-07:00') AS "Billed Date"
	, CONVERT_TZ(ggg.activated_at, '+0:00', '-07:00') AS "Activation Date"
	, ggg.amount
	, ggg.card_number
	, ggg.notes AS "Comments"
	, ggg.activated_by AS "User Name"
FROM
	sales_flat_order AS sfo
INNER JOIN
	gosolid_givex_giftcard AS ggg
ON
	ggg.order_id = sfo.entity_id
WHERE
	DATE(CONVERT_TZ(ggg.activated_at, '+0:00', '-07:00')) BETWEEN '{Start date:date}' AND '{End date:date}' /* non named timezone */
ENDOFSQL;
	$report = Mage::getModel('sqlReports/sqlReport')
							->setSql($sql)
							->setCategoryId($category->getId())
                            ->setPosition(3)
							->setTitle($reportTitle);
	$report->save();
	
	Mage::log("Created report '$reportTitle', has ID of {$report->getId()}");
}
else
{
	Mage::log("Report '$reportTitle' already exists, not creating.");
}

