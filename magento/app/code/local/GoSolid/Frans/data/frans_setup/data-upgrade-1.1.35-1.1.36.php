<?php
$installer = $this;
$installer->startSetup();

// Change sitemap generation settings for CueBlocks_SitemapEnhancedPlus

$configModel = new Mage_Core_Model_Config();

$configModel ->saveConfig('sitemap_enhanced_plus/category/changefreq', "weekly");
$configModel ->saveConfig('sitemap_enhanced_plus/product/changefreq', "weekly");
$configModel ->saveConfig('sitemap_enhanced_plus/prod_out/changefreq', "weekly");
$configModel ->saveConfig('sitemap_enhanced_plus/prod_tag/changefreq', "weekly");
$configModel ->saveConfig('sitemap_enhanced_plus/prod_review/changefreq', "weekly");
$configModel ->saveConfig('sitemap_enhanced_plus/page/changefreq', "weekly");

$installer->endSetup();