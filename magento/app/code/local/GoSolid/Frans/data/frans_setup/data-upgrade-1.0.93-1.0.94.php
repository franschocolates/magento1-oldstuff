<?php
// insert any data inserts/changes here
// as an example, CMS blocks to create/update

$cmsTerms = <<<EOT
<p>Last Updated: October 2007</p>
<p>ACCEPTANCE OF TERMS<br />This site is provided to you by Fran's Chocolates, Ltd. and its affiliates (collectively, "FCL") and is subject to the following Terms of Use ("TOU"). FCL reserves the right to update the TOU at any time without notice to you. The most current version of the TOU can be reviewed by clicking on the "Terms of Use" hypertext link located at the bottom of our web pages.</p>
<p>DESCRIPTION OF SERVICES.<br />Through its web sites, FCL may provide you with access to a variety of resources, including but not limited to information on, and the ability to purchase, various products or services (collectively "Services"). The Services, including any updates, enhancements, new features, and/or the addition of any new web pages, are subject to the TOU.</p>
<p>USE LIMITATION.<br />No one may modify, copy, distribute, transmit, display, perform, reproduce, publish, license, create derivative works from, transfer, or sell any information, photos, data, products or services obtained from the Services, without the prior written consent of FCL in each instance.</p>
<p>PRIVACY AND PROTECTION OF PERSONAL INFORMATION.<br />See the Privacy Statement disclosures relating to the collection and use of your information.</p>
<p>DISCLAIMERS.<br />FCL AND/OR ITS AGENTS MAKE NO REPRESENTATIONS ABOUT THE SUITABILITY OF THE INFORMATION CONTAINED IN THIS SITE FOR ANY PURPOSE. THIS SITE IS PROVIDED BY FCL ON AN "AS IS" BASIS AND "WITH ALL FAULTS". FCL MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY KIND, EXPRESS OR IMPLIED, AS TO THE OPERATION OF THE SITE OR THE INFORMATION, CONTENT, MATERIALS, OR SERVICES INCLUDED ON THIS SITE. TO THE FULLEST EXTENT PERMISSIBLE BY APPLICABLE LAW, FCL DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NONINFRINGEMENT. IN NO EVENT SHALL FCL AND/OR ITS AGENTS BE LIABLE FOR ANY DAMAGES OF ANY KIND ARISING FROM THE USE OF THIS SITE OR THE INFORMATION CONTAINED THEREIN, INCLUDING, BUT NOT LIMITED TO INDIRECT, SPECIAL, INCIDENTAL, PUNITIVE, OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION BASED ON CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION.</p>
<p>THE INFORMATION PUBLISHED ON THIS SITE COULD INCLUDE TECHNICAL INACCURACIES OR TYPOGRAPHICAL ERRORS. CHANGES ARE PERIODICALLY ADDED TO THE INFORMATION IN THIS SITE. FCL AND/OR ITS AGENTS MAY MAKE CHANGES TO THE INFORMATION CONTAINED IN THIS SITE AT ANY TIME.</p>
<p>USER ACCOUNT, PASSWORD, AND SECURITY.<br />If any of the Services requires you to open an account, you must complete the registration process by providing us with current, complete and accurate information as prompted by the applicable registration form. You also may be asked to choose a password and a user name. You are entirely responsible for maintaining the confidentiality of your password and account. Furthermore, you are entirely responsible for any and all activities that occur under your account. You agree to notify FCL immediately of any unauthorized use of your account or any other breach of security. FCL will not be liable for any loss that you may incur as a result of someone else using your password or account, either with or without your knowledge. However, you could be held liable for losses incurred by FCL or another party due to someone else using your account or password. You may not use anyone else's account at any time, without the permission of the account holder.</p>
<p>NO UNLAWFUL OR PROHIBITED USE.<br />As a condition of your use of the Services, you will not use the Services for any purpose that violates any applicable laws or regulations, or that is prohibited by the TOU. You may not use the Services in any manner that could damage, disable, overburden, or impair any server that hosts the FCL web sites, or the network(s) connected to any such server, or interfere with any other party's use and enjoyment of any Services. You may not attempt to gain unauthorized access to other accounts or to any of the Services, through hacking, password mining or any other means. You may not obtain or attempt to obtain any materials or information through any means not intentionally made available through the Services.</p>
<p>FCL reserves the right at all times to disclose any information as FCL deems necessary to satisfy any applicable law, regulation, legal process or governmental request, or to edit, refuse to post or to remove any information or materials, in whole or in part, in FCL's sole discretion.</p>
<p>MATERIALS PROVIDED TO FCL.<br />FCL does not claim ownership of the information you provide to FCL (including feedback and suggestions) or that you input or submit to any Services (collectively "Submissions"). However, by providing Submissions you are granting FCL a royalty-free, sublicensable right to use your Submissions as FCL sees fit in connection with the operation of FCL's business and the modification or development of products or services.</p>
<p>NOTICES AND PROCEDURE FOR MAKING CLAIMS OF COPYRIGHT OR OTHER INTELLECTUAL PROPERTY INFRINGEMENT.<br />FCL's agent for notice of claims of copyright or other intellectual property infringement can be reached as follows</p>
<p>CEO<br />Fran's Chocolates, Ltd.<br />{{config path="shipping/origin/street_line1"}}<br />{{config path="shipping/origin/city"}}, WA {{config path="shipping/origin/postcode"}}<br />Ph. {{config path="general/store_information/phone"}}<br />E-mail: {{config path="trans_email/ident_sales/email"}}</p>
<p>LINKS TO THIRD PARTY SITES.<br />FCL MAY PROVIDE LINKS THAT WILL LET YOU LEAVE FCL'S SITE. THE LINKED SITES ARE NOT UNDER THE CONTROL OF FCL AND FCL IS NOT RESPONSIBLE FOR THE CONTENTS OF ANY LINKED SITE OR ANY LINK CONTAINED IN A LINKED SITE, OR ANY CHANGES OR UPDATES TO SUCH SITES. FCL IS NOT RESPONSIBLE FOR WEBCASTING OR ANY OTHER FORM OF TRANSMISSION RECEIVED FROM ANY LINKED SITE. FCL IS PROVIDING THESE LINKS TO YOU ONLY AS A CONVENIENCE, AND THE INCLUSION OF ANY LINK DOES NOT IMPLY ENDORSEMENT BY FCL OF THE SITE.</p>
<p>COPYRIGHT NOTICE.<br />Copyright &copy; 2014 Fran's Chocolates. All rights reserved.</p>
<p>Any rights not expressly granted herein are reserved.</p>
EOT;

// Update the Terms CMS page
if ($getCMSTerms = Mage::getModel('cms/page')->load('terms', 'identifier')){
    $getCMSTerms->setContent($cmsTerms);
    $getCMSTerms->save();
}

$cmsPrivacy = <<<EOT
<p>Last Updated: October 2007</p>
<p>Privacy is important to Fran's Chocolates, Ltd. and its affiliates (collectively, "FCL"). This privacy statement explains data collection and use practices of the <a href="{{config path="web/unsecure/base_url"}}">www.frans.com</a> site (the "Site"); it does not apply to other online or offline FCL sites, products or services. By accessing the Site, you are consenting to the information collection and use practices described in this privacy statement.</p>
<p class="legal-header">COLLECTION OF YOUR PERSONAL INFORMATION</p>
<p>We will ask you when we need information that personally identifies you ("personal information") or allows us to contact you to provide a product or service or carry out a transaction that you have requested. The personal information we collect may include, without limitation, your name, e-mail address, credit card information, address, and phone number.</p>
<p>The Site may collect certain information about your visit, such as the name of the Internet service provider and the Internet Protocol (IP) address through which you access the Internet; the date and time you access the Site; the pages that you access while at the Site and the Internet address of the Web site from which you linked directly to the Site. This information is used to help improve the Site, analyze trends, and administer the Site.</p>
<p class="legal-header">USE OF YOUR PERSONAL INFORMATION</p>
<p>The personal information collected on this Site will be used to operate the Site and to provide the products or services or carry out the transactions you have requested or authorized.</p>
<p>In support of these uses, FCL may use personal information to provide you with more effective customer service, to improve the Site and any related FCL products or services, and to make the Site easier to use by eliminating the need for you to repeatedly enter the same information. In order to offer you a more consistent experience in your interactions with FCL, information collected by the Site may be combined with information collected by FCL from other sources.</p>
<p>We may use your personal information to provide you with important information about the product or service that you have purchased or inquired about. Additionally, we may send you information about other FCL products and services, and/or share information with FCL partners so they may send you information about their products and services.</p>
<p>We may merge Site-visitation data with demographic information, and we may use this information to provide more relevant content. We may combine Site-visitation data with your personal information in order to provide you with personalized content.</p>
<p>FCL may hire other companies to provide products or services on our behalf, and we may provide such companies with the personal information they need to perform their duties.</p>
<p>FCL may disclose personal information if required to do so by law or in the good faith belief that such action is necessary to (a) conform to the edicts of the law or comply with legal process served on FCL; (b) protect and defend the rights or property of FCL; or (c) act in urgent circumstances to protect the personal safety of FCL employees or agents, users of FCL products or services, or members of the public.</p>
<p>Personal information collected on the Site may be stored and processed in the State of Washington or any other state or country in which FCL or its agents maintain facilities, and by using the Site you consent to any such transfer of information to another state or country.</p>
<p class="legal-header">CONTROL OF YOUR PERSONAL INFORMATION</p>
<p>Please be aware that this privacy statement and any choices you make on the Site will not necessarily apply to personal information you may have provided to FCL in the context of other FCL products or services. If you give us your e-mail address, FCL may send out e-mails to you with information on our products or services.</p>
<p class="legal-header">SECURITY OF YOUR PERSONAL INFORMATION</p>
<p>FCL will use commercially reasonable efforts to protect your personal information from unauthorized access, use, or disclosure. However, due to computer hackers, electronic malfunctions, and other events, FCL cannot guaranty that such safeguards will always protect such information.</p>
<p class="legal-header">PROTECTION OF CHILDREN'S PERSONAL INFORMATION</p>
<p>The Site is a general audience web site and does not knowingly collect any personal information from children.</p>
<p class="legal-header">USE OF COOKIES</p>
<p>We may use cookies on this Site to enhance your user experience. A cookie is a small text file that is placed on your computer's hard disk by a Web page server. One of the primary purposes of cookies is to provide a convenience feature to save you time. For example, if you navigate within a site, a cookie helps the site to recall your specific information on subsequent visits. This simplifies the process of delivering relevant content, eases site navigation, and so on.</p>
<p>You have the ability to accept or decline cookies. Most Web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. If you choose to decline cookies, you may not be able to fully experience the features of this or other Web sites you visit.</p>
<p>Web beacons, also known as clear gif technology, or action tags, may be used to assist in delivering the cookie on the Site. This technology is a tool we may use to compile aggregated statistics about the Site usage, such as how many visitors clicked on key elements (such as links or graphics) on a web page. We may share such Site statistics with partner companies.</p>
<p>FCL reserves the right at all times to disclose any information as FCL deems necessary to satisfy any applicable law, regulation, legal process or governmental request, or to edit, refuse to post or to remove any information or materials, in whole or in part, in FCL's sole discretion.</p>
<p class="legal-header">ENFORCEMENT OF THIS PRIVACY STATEMENT</p>
<p>If you have questions regarding this statement, you should contact FCL.</p>
<p class="legal-header">CHANGES TO THIS STATEMENT</p>
<p>We may update this privacy statement from time to time. When we do, we will also revise the "last updated" date at the top of the privacy statement. We encourage you to periodically review this privacy statement to stay informed about our practices. Your continued use of the Site constitutes your agreement to this privacy statement and any updates.</p>
<p class="legal-header">CONTACT INFORMATION</p>
<p>FCL welcomes your comments regarding this privacy statement. If you believe that FCL has not adhered to this privacy statement, please contact us electronically or via postal mail at the address provided below, and we will use commercially reasonable efforts to promptly determine and remedy the problem.</p>
<p>Attn: CEO<br />Fran's Chocolates, Ltd.<br />{{config path="shipping/origin/street_line1"}}<br />{{config path="shipping/origin/city"}}, WA {{config path="shipping/origin/postcode"}}<br />Ph. {{config path="general/store_information/phone"}}<br />E-mail: <a href="mailto:{{config path="trans_email/ident_sales/email"}}">{{config path="trans_email/ident_sales/email"}}</a></p>
EOT;

// Update the Privacy CMS page
if ($getCMSPrivacy = Mage::getModel('cms/page')->load('privacy', 'identifier')){
    $getCMSPrivacy->setContent($cmsPrivacy);
    $getCMSPrivacy->save();
}

//Create a redirect: about/terms -> terms
$getRewriteModel = Mage::getModel('core/url_rewrite')->load('about-terms', 'id_path');
    $getRewriteModel
        ->setIdPath('about-terms')
        ->setRequestPath('about/terms')
        ->setStoreId('1')
        ->setTargetPath('terms')
        ->setIsSystem('0')
        ->setOptions('RP')
        ->save();