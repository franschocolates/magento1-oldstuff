<?php
/**
 * Sets a label so that things show up in the capture grid with the payment method
 */
Mage::getModel('core/config')->saveConfig('payment/paid_at_register/title', 'Paid at Register' );
