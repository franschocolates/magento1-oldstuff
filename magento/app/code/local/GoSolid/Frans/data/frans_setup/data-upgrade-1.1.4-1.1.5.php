<?php

$installer = $this;
$installer->startSetup();

// Disassociate the Prefix, Suffix, VAT ID, and Email fields with Adminhtml Customer Address forms
$sql = <<<EOSQL
	DELETE FROM `customer_form_attribute`
	WHERE `form_code` = 'adminhtml_customer_address'
	AND `attribute_id` IN (
		SELECT `attribute_id`
		FROM `eav_attribute`
		WHERE `attribute_code` IN ('prefix', 'suffix', 'vat_id', 'email')
	);
EOSQL;

$installer->run($sql);
$installer->endSetup();