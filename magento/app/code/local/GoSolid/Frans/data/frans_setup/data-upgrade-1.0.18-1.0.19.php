<?php

/* create reports */
$categoryTitle = 'Items on Order';
$itemsOnOrderCategory = Mage::getModel('sqlReports/sqlReportCategory');

$itemsOnOrderCategory->load($categoryTitle, 'title');

if (!$itemsOnOrderCategory->getId())
{
	Mage::log("Creating SqlReports category $categoryTitle");
	// doesn't exist, create.
	$itemsOnOrderCategory->setTitle($categoryTitle);
	$itemsOnOrderCategory->setPosition(2);
	
	$itemsOnOrderCategory->save();
}


// now do the actual reports
$reportsForCategoryAndTitle = Mage::getModel("sqlReports/sqlReport")
						->getCollection()
  						->addFieldToFilter("category_id", $itemsOnOrderCategory->getId());
  						
$reportByItemTitle = 'Items on Order - By Item';
$reportsForCategoryAndTitle->addFieldToFilter('title', $reportByItemTitle)
					->load();

if ($reportsForCategoryAndTitle->count() == 0)
{
	// need to create it
	$sql = <<<ENDOFSQL
SELECT
	`sku`, `name` AS 'Product Name', SUM(sfoi.qty_ordered - (sfoi.qty_shipped + sfoi.qty_canceled)) AS 'Quantity'
FROM
	sales_flat_order_item AS sfoi
INNER JOIN
	sales_flat_order AS sfo
ON
	sfo.entity_id = sfoi.order_id
WHERE
	(sfoi.qty_ordered - (sfoi.qty_shipped + sfoi.qty_canceled)) > 0
AND
	DATE(sfo.ship_date) BETWEEN '{Start Date:date}' AND '{End Date:date}'
GROUP BY
	sfoi.`sku`, sfoi.`name`
ORDER BY
	SUM(sfoi.qty_ordered - (sfoi.qty_shipped + sfoi.qty_canceled)) DESC	
ENDOFSQL;
	$reportByItem = Mage::getModel('sqlReports/sqlReport')
							->setSql($sql)
							->setCategoryId($itemsOnOrderCategory->getId())
							->setTitle($reportByItemTitle);
	$reportByItem->save();
	
	Mage::log("Created report '$reportByItemTitle', has ID of {$reportByItem->getId()}");
}
else
{
	Mage::log("Report '$reportByItemTitle' already exists, not creating.");
}

$reportsForCategoryAndTitle = Mage::getModel("sqlReports/sqlReport")
						->getCollection()
  						->addFieldToFilter("category_id", $itemsOnOrderCategory->getId());
  						
$reportByShipDateTitle = 'Items on Order - By Ship Date';

$reportsForCategoryAndTitle->addFieldToFilter('title', $reportByShipDateTitle)
					->load();

if ($reportsForCategoryAndTitle->count() == 0)
{
	// need to create it
	$sql = <<<ENDOFSQL
SELECT
	`sku`
	, `name` AS 'Product Name'
	, SUM(sfoi.qty_ordered - (sfoi.qty_shipped + sfoi.qty_canceled)) AS 'Quantity'
	, DATE_FORMAT(sfo.ship_date, '%m/%d/%Y') AS 'Ship Date'
	, IF (COUNT(DISTINCT sfo.increment_id) > 1, CONCAT(COUNT(DISTINCT sfo.increment_id), ' shipments'), GROUP_CONCAT(DISTINCT sfo.increment_id SEPARATOR '')) AS 'Order #'
	, sfoi.notes AS 'Product Notes'
FROM
	sales_flat_order_item AS sfoi
INNER JOIN
	sales_flat_order AS sfo
ON
	sfo.entity_id = sfoi.order_id
WHERE
	(sfoi.qty_ordered - (sfoi.qty_shipped + sfoi.qty_canceled)) > 0
AND
	DATE(sfo.ship_date) BETWEEN '{Start Date:date}' AND '{End Date:date}'
GROUP BY
	sfoi.`sku`, sfoi.`name`, sfo.ship_date, sfoi.notes
ORDER BY
	ship_date ASC, sku ASC;
ENDOFSQL;
	$reportByShipDate = Mage::getModel('sqlReports/sqlReport')
							->setSql($sql)
							->setCategoryId($itemsOnOrderCategory->getId())
							->setTitle($reportByShipDateTitle);
	$reportByShipDate->save();
	
	Mage::log("Created report '$reportByShipDate', has ID of {$reportByShipDate->getId()}");
}
else
{
	Mage::log("Report '$reportByShipDateTitle' already exists, not creating.");
}
