<?php

$config = new Mage_Core_Model_Config();

//Get currnt gua code
$gauCode = Mage::app()->getStore()->getConfig('google/analytics/account');

//set module params
$config->saveConfig('aromicon_gua/general/account_id', $gauCode, 'default', 0);
$config->saveConfig('aromicon_gua/general/enable', "1", 'default', 0);
$config->saveConfig('aromicon_gua/general/add_to', "before_body_end", 'default', 0);
$config->saveConfig('aromicon_gua/general/anonymize_ip', "0", 'default', 0);
$config->saveConfig('aromicon_gua/general/force_ssl', "1", 'default', 0);
$config->saveConfig('aromicon_gua/ecommerce/enable', "1", 'default', 0);
$config->saveConfig('aromicon_gua/ecommerce/transaction_id', "increment_id", 'default', 0);
$config->saveConfig('aromicon_gua/ecommerce/checkout_url', "/frans/checkout", 'default', 0);
$config->saveConfig('aromicon_gua/ecommerce/success_url', "/frans/checkout/success", 'default', 0);
$config->saveConfig('aromicon_gua/ecommerce/funnel_enable', "1", 'default', 0);


//disable old way of doing analytics...
$config->saveConfig('google/analytics/active', 0, 'default', 0);