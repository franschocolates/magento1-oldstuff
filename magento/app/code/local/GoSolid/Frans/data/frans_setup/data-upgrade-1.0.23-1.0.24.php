<?php

/* create reports */
$categoryTitle = 'Safe Crossings';
$category = Mage::getModel('sqlReports/sqlReportCategory');

$category->load($categoryTitle, 'title');

if (!$category->getId())
{
	Mage::log("Creating SqlReports category $categoryTitle");
	// doesn't exist, create.
	$category->setTitle($categoryTitle);
	$category->setPosition(5);
	
	$category->save();
}


// now do the actual reports
$reportsForCategoryAndTitle = Mage::getModel("sqlReports/sqlReport")
						->getCollection()
  						->addFieldToFilter("category_id", $category->getId());
  						
$reportTitle = 'Safe Crossings - Orders by Date';
$reportsForCategoryAndTitle->addFieldToFilter('title', $reportTitle)
					->load();

if ($reportsForCategoryAndTitle->count() == 0)
{
	// need to create it
$preSql = <<<ENDOFPRESQL
DROP TEMPORARY TABLE IF EXISTS tmp_OrderItems;
DROP TEMPORARY TABLE IF EXISTS tmp_Orders;

CREATE TEMPORARY TABLE tmp_OrderItems
(
	order_id INT NOT NULL,
	increment_id VARCHAR(50) NOT NULL,
	sku VARCHAR(64) NOT NULL,
	`name` VARCHAR(255) NOT NULL,
	qty INT NOT NULL,
	price DECIMAL(12,4) NOT NULL,
	donation DECIMAL(12,4) NOT NULL,
	PRIMARY KEY (order_id, sku)
);

CREATE TEMPORARY TABLE tmp_Orders
(
	order_id INT NOT NULL,
	increment_id VARCHAR(50) NOT NULL,
	order_date DATE NOT NULL,
	ship_date DATE NOT NULL,
	b_name VARCHAR(100) NOT NULL,
	b_street VARCHAR(255) NOT NULL,
	b_city VARCHAR(50) NOT NULL,
	b_state VARCHAR(2) NOT NULL,
	b_postcode VARCHAR(10) NOT NULL,
	b_telephone VARCHAR(50) NULL,
	customer_email VARCHAR(255) NOT NULL,
	s_name VARCHAR(100) NOT NULL,
	s_state VARCHAR(10) NOT NULL,
	
	PRIMARY KEY (order_id)
);

/* find any shipped items that are safe crossings and have an invoice */
INSERT INTO tmp_OrderItems
(
	order_id, increment_id, sku, `name`, qty, price, donation
)
SELECT
	sfoi.order_id
	, sfo.increment_id
	, sfoi.sku
	, sfoi.name
	, sfoi.qty_shipped
	, sfoi.price
	, cped_don.`value`
FROM
	sales_flat_order_item AS sfoi
INNER JOIN
	catalog_product_entity_int AS cped_sc
ON
	cped_sc.entity_id = sfoi.product_id
AND
	cped_sc.attribute_id = (
		SELECT attribute_id 
		FROM eav_attribute ea 
		INNER JOIN eav_entity_type AS eet 
		ON eet.entity_type_id = ea.entity_type_id 
		WHERE ea.attribute_code = 'is_safe_crossings_product' 
		AND eet.entity_type_code='catalog_product'
	)
LEFT OUTER JOIN
	catalog_product_entity_decimal AS cped_don
ON
	cped_don.entity_id = sfoi.product_id
AND
	cped_don.attribute_id = (
		SELECT attribute_id 
		FROM eav_attribute ea 
		INNER JOIN eav_entity_type AS eet 
		ON eet.entity_type_id = ea.entity_type_id 
		WHERE ea.attribute_code = 'safe_crossings_donation' 
		AND eet.entity_type_code='catalog_product'
	)
INNER JOIN
	sales_flat_order AS sfo
ON
	sfo.entity_id = sfoi.order_id
LEFT OUTER JOIN
	sales_flat_order AS sfp
ON
	sfp.entity_id = sfo.multiship_parent_id
INNER JOIN 
	sales_flat_invoice AS sfi
ON
	sfi.order_id = COALESCE(sfp.entity_id, sfo.entity_id)
WHERE
	DATE(sfi.created_at) BETWEEN '{Start date:date}' AND '{End date:date}'
AND
	(sfo.is_multiship_parent = 0 OR sfo.is_multiship_parent IS NULL);

/* make a table with the main order information */
INSERT INTO tmp_Orders
SELECT sfo.entity_id
	, sfo.increment_id
	, DATE(sfo.created_at)
	, sfo.ship_date
	, CONCAT(sfoa_bill.firstname, ' ', sfoa_bill.lastname)
	, sfoa_bill.street
	, sfoa_bill.city
	, sfoa_bill_region.code
	, sfoa_bill.postcode
	, sfoa_bill.telephone
	, sfo.customer_email
	, CONCAT(sfoa_ship.firstname, ' ', sfoa_ship.lastname)
	, sfoa_ship_region.code
	
FROM
	sales_flat_order AS sfo
LEFT OUTER JOIN
	sales_flat_order AS sfp
ON
	sfp.entity_id = sfo.multiship_parent_id
INNER JOIN
	sales_flat_order_address AS sfoa_bill
ON
	sfoa_bill.entity_id = COALESCE(sfp.billing_address_id, sfo.billing_address_id)
INNER JOIN
	directory_country_region AS sfoa_bill_region
ON
	sfoa_bill_region.region_id = sfoa_bill.region_id
INNER JOIN
	sales_flat_order_address AS sfoa_ship
ON
	sfoa_ship.entity_id = sfo.shipping_address_id
INNER JOIN
	directory_country_region AS sfoa_ship_region
ON
	sfoa_ship_region.region_id = sfoa_ship.region_id

WHERE
	sfo.entity_id IN
	( SELECT DISTINCT order_id FROM tmp_OrderItems);
ENDOFPRESQL;
	
	$sql = <<<ENDOFSQL
/* finally, return just joins the two temp tables with some grouping */
SELECT 
	increment_id AS 'Invoice #'
	, order_date AS 'Order Date'
	, ship_date AS 'Ship Date'
	, item_totals.skus AS 'Item #'
	, item_totals.product_names AS 'Item Description'
	, item_totals.qtys AS 'Quantity'
	, item_totals.prices AS 'Item(s) Price'
	, item_totals.donation_amount
	, b_name AS 'Sold To Name'
	, b_street AS 'Address'
	, b_city AS 'City'
	, b_state AS 'State'
	, b_postcode AS 'Zip Code'
	, b_telephone AS 'Phone Number'
	, customer_email
	, s_name
	, s_state
FROM
	tmp_Orders AS tmpo
INNER JOIN
(
	SELECT order_id
		, GROUP_CONCAT(tmpoi.sku SEPARATOR '\n') AS skus
		, GROUP_CONCAT(tmpoi.name SEPARATOR '\n') as product_names
		, GROUP_CONCAT(tmpoi.qty SEPARATOR '\n') AS qtys
		, GROUP_CONCAT((tmpoi.price * tmpoi.qty) SEPARATOR '\n') AS prices
		, SUM(tmpoi.donation * tmpoi.qty) AS donation_amount
	FROM 
		tmp_OrderItems AS tmpoi
	GROUP BY tmpoi.order_id
) AS item_totals
on
	item_totals.order_id = tmpo.order_id;
ENDOFSQL;
	$report = Mage::getModel('sqlReports/sqlReport')
							->setPreSql($preSql)
							->setSql($sql)
							->setCategoryId($category->getId())
							->setTitle($reportTitle);
	$report->save();
	
	Mage::log("Created report '$reportTitle', has ID of {$report->getId()}");
}
else
{
	Mage::log("Report '$reportTitle' already exists, not creating.");
}

