<?php
// insert any data inserts/changes here
$installer = $this;

// this will update any giftcards to have an activated at of the order date
// we would like to use the order item created at, but for some reason that is getting updated
// so the order date should be accurate enough
$sql = <<<EOSQL
UPDATE
gosolid_givex_giftcard AS ggg
INNER JOIN sales_flat_order_item AS sfoi
ON sfoi.item_id = ggg.order_item_id
AND sfoi.order_id = ggg.order_id
INNER JOIN sales_flat_order AS sfo
ON sfo.entity_id = sfoi.order_id
SET activated_at = sfo.created_at
	, activated_by = 'System'
WHERE
	ggg.card_type='V'
AND
	ggg.activated_at IS NULL
AND
	ggg.activated_by IS NULL;
EOSQL;

$installer->run($sql);