<?php
/*
this code will create all the dependencies for the REST users
*/

// first, create a REST Print Client role
$restPrintRole = Mage::getModel('admin/role')
    ->setRoleName('REST Print Client')
    ->setRoleType('G') // group
    ->setTreeLevel(1)
     ->save(); // first level

$restAdminUser = Mage::getModel('admin/user')
    ->setData(array(
        'username'  => 'REST Print Client Admin User',
        'firstname' => 'REST',
        'lastname'    => 'Print Client',
        'email'     => 'frans_rest_print_client@gosolid.net',
        'password'  => 'gosolidrests1',
        'is_active' => 1
    ))->save();

$restAdminUser->setRoleIds(array($restPrintRole->getId()))
                ->setRoleUserId($restAdminUser->getUserId())
                ->saveRelations();

// now that we have an admin user,
// try creating our necessary API Role/user/token
$restApiRole = Mage::getModel('api2/acl_global_role')
                ->setRoleName('REST Print Client')
                ->save();

foreach (array('retrieve', 'update') as $operation)
{
    Mage::getModel('api2/acl_global_rule')
        ->setRoleId($restApiRole->getId())
        ->setResourceId('print_job')
        ->setPrivilege($operation)
        ->save();
}

// add our admin user from above to this role
$resourceModel = Mage::getResourceModel('api2/acl_global_role');
$resourceModel->saveAdminToRoleRelation($restAdminUser->getId(), $restApiRole->getId());

// now also setup the attributes that are allowed for the print client REST API for admin
$allowedAttributes = 'entity_id,entity_type,id,print_station_id,print_status';

foreach (array('read', 'write') as $operationType)
{
    Mage::getModel('api2/acl_filter_attribute')
        ->setUserType('admin') // should probably be a constant
        ->setResourceId('print_job')
        ->setOperation($operationType)
        ->setAllowedAttributes($allowedAttributes)
        ->save();
}

// finally, configure our token and oauth stuff.
$helper = Mage::helper('oauth');
$consumer = Mage::getModel('oauth/consumer')
            ->setKey($helper->generateConsumerKey())
            ->setSecret($helper->generateConsumerSecret())
            ->setName('REST Print Client')
            ->save();

// create and authorize the token.
$oauthToken = Mage::getModel('oauth/token')
                ->createRequestToken($consumer->getId(), Mage_Oauth_Model_Server::CALLBACK_ESTABLISHED) // fails if URL doesn't match
                ->setType(Mage_Oauth_Model_Token::TYPE_ACCESS) // needs to be set from request to access or else it fails
                ->authorize($restAdminUser->getId(), Mage_Oauth_Model_Token::USER_TYPE_ADMIN);


