<?php
// add the picked up status; will upsert it to what it should be.
$pickedUpStatus = Mage::getModel('sales/order_status')
    ->load('picked_up');

$existed = strlen($pickedUpStatus->getStatus()) > 0;

$pickedUpStatus->setStatus('picked_up')
    ->setLabel('Picked Up')
    ->save();

// doesn't exist, also need to assign to a state
if (!$existed)
{
    $pickedUpStatus->assignState('picked_up', false);
}