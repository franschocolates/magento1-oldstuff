<?php

/* create reports */
$categoryTitle = 'Sales';
$salesCategory = Mage::getModel('sqlReports/sqlReportCategory');

$salesCategory->load($categoryTitle, 'title');

if (!$salesCategory->getId())
{
	Mage::log("Creating SqlReports category $categoryTitle");
	// doesn't exist, create.
    $salesCategory->setTitle($categoryTitle);
    $salesCategory->setPosition(1);

    $salesCategory->save();
}


// now do the actual reports
$reportsForCategoryAndTitle = Mage::getModel("sqlReports/sqlReport")
                                            ->getCollection()
                                            ->addFieldToFilter("category_id", $salesCategory->getId());
  						
$reportByItemTitle = 'Sales by Order Date';
$reportsForCategoryAndTitle->addFieldToFilter('title', $reportByItemTitle)->load();

if ($reportsForCategoryAndTitle->count() == 0)
{
	// need to create it
	$sql = <<<ENDOFSQL
SELECT DATE(CONVERT_TZ(sfo.created_at, '+0:00', '-07:00')) AS "Create Date" ,
	COUNT(sfo.entity_id) AS "Total Orders",
	IFNULL(tsfo.shipments,0) AS "Total Shipments" ,
	CONCAT('$', FORMAT(SUM(sfo.base_subtotal + IFNULL(sfo.base_discount_amount,0)),2)) AS "Sales",
	CONCAT('$', FORMAT(SUM(sfo.base_shipping_amount),2)) AS "Shipping" ,
	CONCAT('$', FORMAT(SUM(sfo.base_grand_total),2)) AS "Grand Total"
FROM sales_flat_order AS sfo
LEFT JOIN (
	SELECT
		COUNT(entity_id) AS shipments,
		accounting_label,
		planned_ship_date,
		DATE(CONVERT_TZ(created_at, '+0:00', '-07:00')) AS created_at
	FROM sales_flat_order
	WHERE (is_multiship_parent = 0 OR is_multiship_parent IS NULL)
		AND STATUS IN  ({Order Status:multiselect[SELECT UPPER(REPLACE(`status`,'_',' ')) AS `label`, `status` AS `value` FROM sales_flat_order GROUP BY `status`]})
                AND accounting_label IN ({Accounting Label:multiselect[SELECT label_name AS `label`, label_name AS `value` FROM `frans_accounting_label`]})
	GROUP BY
		DATE(CONVERT_TZ(created_at, '+0:00', '-07:00'))
) AS tsfo
ON tsfo.created_at= DATE(CONVERT_TZ(sfo.created_at, '+0:00', '-07:00'))
WHERE
	DATE(CONVERT_TZ(sfo.created_at, '+0:00', '-07:00')) BETWEEN  '{Start Date:date}' AND '{End Date:date}'
	AND sfo.status IN ({Order Status:multiselect[SELECT UPPER(REPLACE(`status`,'_',' ')) AS `label`, `status` AS `value` FROM sales_flat_order GROUP BY `status`]})
        AND sfo.accounting_label IN ({Accounting Label:multiselect[SELECT label_name AS `label`, label_name AS `value` FROM `frans_accounting_label`]})
	AND (sfo.is_multiship_parent = 1 OR sfo.is_multiship_parent IS NULL)
GROUP BY
	DATE(CONVERT_TZ(sfo.created_at, '+0:00', '-07:00'))
ENDOFSQL;
	$reportByItem = Mage::getModel('sqlReports/sqlReport')
							->setSql($sql)
							->setCategoryId($salesCategory->getId())
							->setTitle($reportByItemTitle);
	$reportByItem->save();
	
	Mage::log("Created report '$reportByItemTitle', has ID of {$reportByItem->getId()}");
}
else
{
	Mage::log("Report '$reportByItemTitle' already exists, not creating.");
}