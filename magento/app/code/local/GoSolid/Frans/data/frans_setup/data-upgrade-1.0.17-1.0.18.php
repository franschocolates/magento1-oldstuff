<?php

/* create all necessary accounting originations and labels (with accpac customer ID) */
$originations = array('BELL', 'DT', 'MFG', 'MO', 'UV', 'WEB');

foreach ($originations as $orig)
{
	Mage::getModel('frans/accounting_origination')->setOriginationName($orig)->save();
}

$labels = array(
			'BELL' => '00',
			'DT' => '0',
			'MFG' => '239143',
			'MO' => '00003',
			'UV' => '000',
			'WEB' => '00003',
			'BUS' => '00003'
);


foreach ($labels as $labelName => $accPacCustomerId)
{
	Mage::getModel('frans/accounting_label')
			->setLabelName($labelName)
			->setAccpacCustomerId($accPacCustomerId)
			->save();
}

/* remove the accounting ID EAV if it exists */
$attributeCode = 'accounting_id';
$eavConfig = Mage::getSingleton('eav/config');

$attribute = $eavConfig->getAttribute('customer', $attributeCode);

if ($attribute->getId())
{
	$setup = Mage::getModel('customer/entity_setup', 'core_setup');
	// Remove the old attribute
	$setup->removeAttribute('customer' , $attributeCode);
}
