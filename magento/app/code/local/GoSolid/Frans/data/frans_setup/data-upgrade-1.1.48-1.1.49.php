<?php
$installer = $this;
$installer->startSetup();

$reportTitle1 = 'Items on Order - By Item';
$reportTitle2 = 'Items on Order - By Ship Date';

$preSql1 = <<<ENDOFSQL
DROP TEMPORARY TABLE IF EXISTS tmp_ItemsByOrderByItem;
CREATE TEMPORARY TABLE tmp_ItemsByOrderByItem
SELECT
	 sfoi.accpac_sku
	, sfoi.name
	, (SELECT title
		FROM eav_attribute ea
		INNER JOIN `catalog_product_entity_int` ei ON ei.attribute_id = ea.attribute_id
		INNER JOIN `frans_color_tile` ct ON ct.id = ei.value
		WHERE ea.attribute_code = 'color_tile'
			AND ei.entity_id = sfoi.product_id
		) AS 'color_tile'
	, sfoi.qty_ordered - (sfoi.qty_shipped + sfoi.qty_canceled) AS 'qty'
FROM
	sales_flat_order_item AS sfoi
INNER JOIN sales_flat_order AS sfo ON sfo.entity_id = sfoi.order_id
WHERE sfo.status NOT IN (
		'ready_for_pickup'
		,'picked_up'
		,'checked'
		)
	AND DATE (sfo.planned_ship_date) BETWEEN '{Start Planned Ship Date:date}' AND '{End Planned Ship Date:date}';
ENDOFSQL;

$sql1 = <<<ENDOFSQL
SELECT
	ibi.accpac_sku AS 'ACCPAC SKU',
	ibi.name AS 'Product Name',
	ibi.color_tile AS 'Color Tile',
	SUM(ibi.qty) AS 'Quantity'
FROM tmp_ItemsByOrderByItem AS ibi
WHERE ibi.qty > 0
GROUP BY accpac_sku, name, color_tile
ORDER BY ibi.qty DESC;
ENDOFSQL;

$preSql2 = <<<ENDOFSQL
DROP TEMPORARY TABLE IF EXISTS tmp_ItemsByOrderByShipDate;
CREATE TEMPORARY TABLE tmp_ItemsByOrderByShipDate
SELECT
	 sfoi.accpac_sku
	, sfoi.name
	, (SELECT title
		FROM eav_attribute ea
		INNER JOIN `catalog_product_entity_int` ei ON ei.attribute_id = ea.attribute_id
		INNER JOIN `frans_color_tile` ct ON ct.id = ei.value
		WHERE ea.attribute_code = 'color_tile'
			AND ei.entity_id = sfoi.product_id
		) AS 'color_tile'
	, sfoi.qty_ordered - (sfoi.qty_shipped + sfoi.qty_canceled) AS 'qty'
	, sfo.planned_ship_date AS 'ship_date'
	, sfoi.notes AS 'product_notes'
	, IF (COUNT(DISTINCT sfo.increment_id) > 1, CONCAT(COUNT(DISTINCT sfo.increment_id), ' shipments'), GROUP_CONCAT(DISTINCT sfo.increment_id SEPARATOR '')) AS 'order_number'
FROM
	sales_flat_order_item AS sfoi
INNER JOIN sales_flat_order AS sfo ON sfo.entity_id = sfoi.order_id
WHERE sfo.status NOT IN (
		'ready_for_pickup'
		,'picked_up'
		,'checked'
		)
	AND DATE (sfo.planned_ship_date) BETWEEN '{Start Planned Ship Date:date}' AND '{End Planned Ship Date:date}'
	GROUP BY sfoi.accpac_sku;
ENDOFSQL;

$sql2 = <<<ENDOFSQL
SELECT
	ibsd.accpac_sku AS 'ACCPAC SKU',
	ibsd.name AS 'Product Name',
	ibsd.color_tile AS 'Color Tile',
	SUM(ibsd.qty) AS 'Quantity',
	ibsd.ship_date AS 'Ship Date',
	ibsd.order_number AS 'Order #',
	ibsd.product_notes AS 'Product Notes'
FROM tmp_ItemsByOrderByShipDate AS ibsd
WHERE ibsd.qty > 0
GROUP BY name, color_tile, ship_date, product_notes, accpac_sku
ORDER BY ibsd.ship_date DESC;
ENDOFSQL;

if (Mage::getModel("sqlReports/sqlReport")->load($reportTitle1 ,'title')->getId()) {
    $report = Mage::getModel("sqlReports/sqlReport")->load($reportTitle1, 'title')
        ->setPreSql($preSql1)
        ->setSql($sql1);
    $report->save();
}
if (Mage::getModel("sqlReports/sqlReport")->load($reportTitle2 ,'title')->getId()) {
    $report = Mage::getModel("sqlReports/sqlReport")->load($reportTitle2, 'title')
        ->setPreSql($preSql2)
        ->setSql($sql2);
    $report->save();
}
$installer->endSetup();












