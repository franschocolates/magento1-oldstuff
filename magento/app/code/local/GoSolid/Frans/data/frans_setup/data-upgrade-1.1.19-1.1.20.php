<?php
$installer = $this;
$installer->startSetup();

Mage::getModel('core/config')->saveConfig('frans/checkout_options/large_order_amount', '600.00');
$message = 'To produce the freshest chocolates possible, we hand make & hand pack chocolates in small batches daily. Please note large orders and custom orders may have a longer lead time. A representative will contact you by the next business day with an updated ship date if necessary. Please continue to place you order so we can begin preparing your chocolates.';
Mage::getModel('core/config')->saveConfig('frans/checkout_options/large_order_message', $message);

$installer->endSetup();