<?php

$preferredArrival = Mage::getModel('frans/futureShipMessage')
    ->load('Preferred Arrival', 'message_title');

if ($preferredArrival->getId())
{
    $preferredArrival->setMessageText('')
        ->save();
}