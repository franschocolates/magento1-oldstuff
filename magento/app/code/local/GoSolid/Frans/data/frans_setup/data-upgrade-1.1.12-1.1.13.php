<?php

/* create reports */
$categoryTitle = 'Items on Order';
$salesCategory = Mage::getModel('sqlReports/sqlReportCategory');

$salesCategory->load($categoryTitle, 'title');

if (!$salesCategory->getId())
{
	Mage::log("Creating SqlReports category $categoryTitle");
	// doesn't exist, create.
    $salesCategory->setTitle($categoryTitle);
    $salesCategory->setPosition(1);

    $salesCategory->save();
}


// now do the actual reports
$reportsForCategoryAndTitle = Mage::getModel("sqlReports/sqlReport")
                                            ->getCollection()
                                            ->addFieldToFilter("category_id", $salesCategory->getId());
  						
$reportByItemTitle = 'Items on Order - By Ship Date (Not Printed)';
$reportsForCategoryAndTitle->addFieldToFilter('title', $reportByItemTitle)->load();

if ($reportsForCategoryAndTitle->count() == 0)
{
	// need to create it
	$sql = <<<ENDOFSQL
SELECT
		sfoi.sku AS "SKU",
	CONCAT("<a href='../../../../../admin/catalog_product/edit/id/",sfoi.product_id,"' target='_blank'>",sfoi.name,"</a>") AS "Product Name",
	sfoi.qty_ordered AS "Quantity",
	DATE(CONVERT_TZ(sfo.planned_ship_date, '+0:00', '-07:00')) AS "Ship Date",
	CONCAT("<a href='../../../../../admin/sales_order/view/order_id/",sfoi.order_id,"' target='_blank'>#",sfo.increment_id,"</a>") AS "Order #",
	sfoi.notes AS "Product Notes"
FROM sales_flat_order_item AS sfoi
INNER JOIN sales_flat_order AS sfo
ON sfoi.order_id = sfo.entity_id
WHERE sfo.is_printed = 0
AND DATE(CONVERT_TZ(sfo.planned_ship_date, '+0:00', '-07:00')) BETWEEN '{Start Date:date}' AND '{End Date:date}'
ORDER BY sfo.planned_ship_date DESC, sfo.entity_id DESC'
ENDOFSQL;
	$reportByItem = Mage::getModel('sqlReports/sqlReport')
							->setSql($sql)
							->setCategoryId($salesCategory->getId())
							->setTitle($reportByItemTitle);
	$reportByItem->save();
	
	Mage::log("Created report '$reportByItemTitle', has ID of {$reportByItem->getId()}");
}
else
{
	Mage::log("Report '$reportByItemTitle' already exists, not creating.");
}