<?php
$installer = $this;
$installer->startSetup();

$copyright = "&copy; 2015 Fran's Chocolates. All Rights Reserved.";
Mage::getModel('core/config')->saveConfig('design/footer/copyright', $copyright);

$installer->endSetup();