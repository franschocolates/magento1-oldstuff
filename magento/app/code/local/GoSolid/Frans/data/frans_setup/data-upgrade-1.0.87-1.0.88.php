<?php
// insert any data inserts/changes here
// as an example, CMS blocks to create/update

$storeStreet = "5900 Airport Way South";
$storeCity = "Seattle";
$storePostcode = "98108";
$storeRegionId = "62"; // WA
$storeCountryId = "US"; // WA

$completeAddress = "Fran's Chocolates Office" . "\n" . $storeStreet . "\n" . $storeCity . ", WA " . $storePostcode;

// Save shipping/origin
Mage::getModel('core/config')->saveConfig('shipping/origin/country_id', $storeCountryId);
Mage::getModel('core/config')->saveConfig('shipping/origin/region_id', $storeRegionId);
Mage::getModel('core/config')->saveConfig('shipping/origin/postcode', $storePostcode);
Mage::getModel('core/config')->saveConfig('shipping/origin/city', $storeCity);
Mage::getModel('core/config')->saveConfig('shipping/origin/street_line1', $storeStreet);

// Save general/store_information
Mage::getModel('core/config')->saveConfig('general/store_information/address', $completeAddress);
