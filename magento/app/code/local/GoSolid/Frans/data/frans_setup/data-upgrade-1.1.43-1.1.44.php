<?php
$installer = $this;
$installer->startSetup();

// Change Weekend Hours to include html that obscures a section when it is printed in the Quote PDF

$configModel = new Mage_Core_Model_Config();
$content = <<<EOT
Saturday:  10AM - 6PM<div class="pac-time"></br></br><em>hours are in Pacific Time</em></br></br></div>
EOT;

$configModel->saveConfig('general/store_information/weekend_hours', $content);


$installer->endSetup();