<?php

/* create categories */
$categoryTitle = 'Pickup';
$category = Mage::getModel('sqlReports/sqlReportCategory');

$category->load($categoryTitle, 'title');

if (!$category->getId())
{
	Mage::log("Creating SqlReports category $categoryTitle");
	// doesn't exist, create.
	$category->setTitle($categoryTitle);
	$category->setPosition(6);
	
	$category->save();
}


// now do the actual reports
$reportsForCategoryAndTitle = Mage::getModel("sqlReports/sqlReport")
						->getCollection()
  						->addFieldToFilter("category_id", $category->getId());
  						
$reportTitle = 'Pickup - Pickup Report';
$reportsForCategoryAndTitle->addFieldToFilter('title', $reportTitle)
					->load();

if ($reportsForCategoryAndTitle->count() == 0)
{
	// need to create it
	$sql = <<<ENDOFSQL
SELECT o.increment_id AS order_number
	, COALESCE(sosl.label, o.status) AS `status`
	, IF(sfop.method='pay_at_pickup', 'no', 'paid') AS 'is paid'
	, frs.name AS 'pick up at'
	, CONCAT(o.customer_firstname, ' ', o.customer_lastname) AS 'customer'
	, DATE(CONVERT_TZ(o.ship_date, 'UTC', '-08:00')) AS 'ship date'
	, o.grand_total AS 'total'
FROM
	sales_flat_order AS o
INNER JOIN
	frans_retail_store AS frs
ON
	frs.store_code = RIGHT(o.shipping_method, LENGTH(frs.store_code))
LEFT OUTER JOIN
	sales_flat_order AS sfp /* parent */
ON
	sfp.entity_id = o.multiship_parent_id
INNER JOIN /* make sure there is a payment - might be on parent */
	sales_flat_order_payment AS sfop
ON
	sfop.parent_id = COALESCE(sfp.entity_id, o.entity_id)
LEFT OUTER JOIN
	sales_order_status_label AS sosl
ON
	sosl.status = o.`status`
WHERE
	o.state NOT IN ('canceled', 'closed', 'complete')
AND
	o.captured_at IS NULL /* not captured */
AND
	sfop.captured_at IS NULL /* parent not captured */
AND
	o.shipping_method LIKE 'pickupatstore%'
ENDOFSQL;
	$report = Mage::getModel('sqlReports/sqlReport')
							->setSql($sql)
							->setCategoryId($category->getId())
                            ->setPosition(1)
							->setTitle($reportTitle);
	$report->save();
	
	Mage::log("Created report '$reportTitle', has ID of {$report->getId()}");
}
else
{
	Mage::log("Report '$reportTitle' already exists, not creating.");
}

