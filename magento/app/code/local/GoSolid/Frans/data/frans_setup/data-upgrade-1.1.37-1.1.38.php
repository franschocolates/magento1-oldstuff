<?php
$installer = $this;
$installer->startSetup();

// switch the homepage hero for Holiday 2015
Mage::getModel('core/config')->saveConfig('frans/homepage_hero/hero_template', "2015-holiday.phtml");

$installer->endSetup();