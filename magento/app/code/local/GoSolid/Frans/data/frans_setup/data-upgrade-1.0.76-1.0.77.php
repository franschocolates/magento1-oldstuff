<?php

$uspsShippingMethod = Mage::getModel('frans/shippingMethod')
    ->load('usps_Priority Mail', 'shipping_method')
    ->setShippingMethod('usps_Priority Mail')
    ->setLabel('USPS Priority')
    ->setColor('blue')
    ->save();

// create shipping rules to handle USPS
$uspsShippingRule = Mage::getModel('shippingRules/rules')
    ->load('USPS Non-Zero Weight', 'title')
    ->setTitle('USPS Non-Zero Weight')
    ->setCarrier('frans,usps')
    ->setCarrierShipCode(GoSolid_ShippingRules_Model_Rules::VALUE_ANY)
    ->setAllowedCountries('US')
    ->setAllowedRegions(GoSolid_ShippingRules_Model_Rules::VALUE_ANY)
    ->setAllowedPostalCodes(GoSolid_ShippingRules_Model_Rules::VALUE_ANY)
    ->setCusomerGroups(GoSolid_ShippingRules_Model_Rules::VALUE_ANY)
    ->setQty('>=1')
    ->setWeight('>0') // remove anything greater than 0
    ->setPosition(1110)
    ->setRemoveRate(GoSolid_ShippingRules_Model_Rules::REMOVE_RATE_YES) // remove it
    ->save();

$uspsShippingRule = Mage::getModel('shippingRules/rules')
    ->load('USPS Zero Weight', 'title')
    ->setTitle('USPS Zero Weight')
    ->setCarrier('frans,usps')
    ->setCarrierShipCode(GoSolid_ShippingRules_Model_Rules::VALUE_ANY)
    ->setAllowedCountries('US')
    ->setAllowedRegions(GoSolid_ShippingRules_Model_Rules::VALUE_ANY)
    ->setAllowedPostalCodes(GoSolid_ShippingRules_Model_Rules::VALUE_ANY)
    ->setCusomerGroups(GoSolid_ShippingRules_Model_Rules::VALUE_ANY)
    ->setQty('>=1')
    ->setWeight('==0') // only equal to 0
    ->setPosition(1120)
    ->setRemoveRate(GoSolid_ShippingRules_Model_Rules::REMOME_RATE_NO)
    ->save();

// create a shipping zone method time for USPS for each shipping zone
// should be 2 days across the board
foreach (Mage::getModel('frans/shippingZone')->getCollection() as $shippingZone)
{
    Mage::log("Creating a USPS shipping zone method time for shipping zone with name of {$shippingZone->getZoneName()}");
    Mage::getModel('frans/shippingZoneMethodTime')
        ->setZoneId($shippingZone->getId())
        ->setShippingMethod('usps_Priority Mail')
        ->setTimeInDays(3)
        ->setDeliveryDays('1,2,3,4,5,6')
        ->setDeliveryType(0)
        ->save();
}