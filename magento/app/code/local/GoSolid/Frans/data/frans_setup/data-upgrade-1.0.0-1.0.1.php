<?php

# This script adds the custom "ready_for_payment" status to orders

$ready_for_payment_code = 'ready_for_payment';


$status = Mage::getModel('sales/order_status')
                    ->load($ready_for_payment_code);
                    
if ($status->getStatus())
{
	// do nothing, already present
}
else
{
	$status->setStatus($ready_for_payment_code);
	$status->setLabel('Ready for Payment');
	
	$status->save();
	
	# now make sure it is assigned to the state
	$status->assignState('processing', false);
}
