<?php
/**
 * Suppres gift messages for the whole order
 */
Mage::getModel('core/config')->saveConfig('sales/gift_options/allow_order', 0);