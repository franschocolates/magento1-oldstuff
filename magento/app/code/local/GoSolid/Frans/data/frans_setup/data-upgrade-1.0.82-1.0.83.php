<?php

$description = <<<EOL
Always the perfect gift for any occasion and guaranteed to bring a smile. Use online or in our retails stores and the possibilities for delight are endless. Complimentary shipping sweetens our Classic Gift Cards even further. Already have a Fran's Gift Card? <a href="/frans/giftcards">Check Card Balance &raquo;</a>
EOL;

$layout = <<<EOL
<reference name="content">
	<block type="frans/giftcards_purchase" name="purchase" template="giftcards/purchase.phtml" />
</reference>
EOL;


Mage::getModel('catalog/category')
	->load(34)
	->setDescription($description)
	->setCustomLayoutUpdate($layout)
	->setDisplayMode('PAGE')
	->save();