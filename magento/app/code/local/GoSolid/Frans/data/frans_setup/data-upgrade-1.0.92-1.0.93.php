<?php
Mage::getModel('core/config')->saveConfig('sales/minimum_order/multi_address', 1);
Mage::getModel('core/config')->saveConfig('sales/minimum_order/multi_address_description', 'Sorry we have a $15 minimum per recipient. Please add items or remove these recipients.');
