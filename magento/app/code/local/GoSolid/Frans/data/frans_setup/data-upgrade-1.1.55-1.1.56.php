<?php
$installer = $this;
$installer->startSetup();

$tileName = 'Homepage Tile -- Email Sign Up';
$homepageTiles = Mage::getModel('banner/banner')->getBanners('homepage_tile');
$homepageTiles->addFieldToFilter("name", $tileName);
$collectionCount = count($homepageTiles->getitems());

if($collectionCount >= 1){
   $oldSignUpTile =  $homepageTiles->getLastitem();
    $oldSignUpTile->setStatus('disabled');
    $oldSignUpTile->save();
}

$installer->endSetup();











