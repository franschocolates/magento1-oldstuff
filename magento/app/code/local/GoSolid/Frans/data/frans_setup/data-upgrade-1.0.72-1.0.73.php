<?php
// add the ready for pickup status; will upsert it to what it should be.
$readyForPickupStatus = Mage::getModel('sales/order_status')
    ->load('ready_for_pickup');

$existed = strlen($readyForPickupStatus->getStatus()) > 0;

$readyForPickupStatus->setStatus('ready_for_pickup')
    ->setLabel('Ready for Pickup')
    ->save();

// doesn't exist, also need to assign to a state
if (!$existed)
{
    $readyForPickupStatus->assignState('ready_for_pickup', false);
}
