<?php
// insert any data inserts/changes here

// add the ready to print status; will upsert it to what it should be.
$readyToPrintStatus = Mage::getModel('sales/order_status')
                        ->load('ready_to_print');

// see if it already exists
$existed = strlen($readyToPrintStatus->getStatus()) > 0;

$readyToPrintStatus->setStatus('ready_to_print')
    ->setStatusFrontend('processing')
    ->setLabel('Ready to Print')
    ->setAllowedActions('processing,ready_for_review')
    ->save();

    // doesn't exist, also need to assign to a state
if (!$existed)
{
    $readyToPrintStatus->assignState('processing', false);
}


Mage::getModel('sales/order_status')
        ->load('processing')
        ->setAllowedActions('ready_for_review,ready_to_print')
        ->save();

Mage::getModel('sales/order_status')
    ->load('ready_for_review')
    ->setAllowedActions('processing,ready_to_print')
    ->save();


