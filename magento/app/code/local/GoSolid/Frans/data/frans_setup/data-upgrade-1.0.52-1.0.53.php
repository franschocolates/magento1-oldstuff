<?php
/* make sure that print_job_type is in allowed attributes */
$printJobFilters = Mage::getModel('api2/acl_filter_attribute')
    ->getCollection()
    ->addFieldToFilter('user_type', 'admin')
    ->addFieldToFilter('resource_id', 'print_job');

foreach ($printJobFilters as $filter)
{
    $allowedAttributes = $filter->getAllowedAttributes();

    if (stristr($allowedAttributes, 'print_job_type') === false)
    {
        $allowedAttributes .= ',print_job_type';
        $filter->setAllowedAttributes($allowedAttributes);
        $filter->save();
    }
}
