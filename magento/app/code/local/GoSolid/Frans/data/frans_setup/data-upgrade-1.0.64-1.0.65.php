<?php
/**
 * Set admin session lifetime to be 8 hours
 */
// lifetime is in seconds so need to go hours->seconds
$lifetime = 8 * 60 * 60;
Mage::getModel('core/config')->saveConfig('admin/security/session_cookie_lifetime', $lifetime);