<?php
    $coreUrlRewrite = Mage::getModel('core/url_rewrite');

    $dataArray = array(
        'store_id' => 1,
        'id_path' =>   'category/34',
        'request_path' => 'giftcards',
        'target_path' => 'other/gift-cards',
        'is_system' => 0,
        'options' => 'RP',
        'category_id' => 34
    );


    $coreUrlRewrite->setData($dataArray);
    $coreUrlRewrite->save();