<?php

$categoryTitle = 'Marketing';
$marketingCategory = Mage::getModel('sqlReports/sqlReportCategory')->load($categoryTitle, 'title');

if (!$marketingCategory->getId())
{
	Mage::log("Creating SqlReports category $categoryTitle");
	// doesn't exist, create.
	$marketingCategory->setTitle($categoryTitle);
	$marketingCategory->setPosition(300);

	$marketingCategory->save();
}

// create or update subscriptions report
$subscriptionReportTitle = 'Subscriptions';

$sql = <<<ENDOFSQL
SELECT
 IFNULL(namefirst.value, "") AS 'Billing First Name'
,IFNULL(namelast.value, "") AS 'Billing Last Name'
,IFNULL(company.value, "") AS 'Company Name'
,IFNULL(CONCAT(caet_st.value, ", ",caev_city.value ," ", caev_state.value, ", ", caev_zip.value, ", ", caev_co.value ), "") AS "Billing Address"
,IFNULL(caev_ph.value, "") AS 'Billing Phone'
,c.email AS "Email"
,IFNULL(SUM(sfo.base_subtotal), 0) AS "Subtotal"
,IF(cev_sub.value LIKE '%2%', 'Yes', 'No') AS "Print Catalog"
,IF(cev_sub.value LIKE '%1%', 'Yes', 'No') AS "Newsletter"
FROM customer_entity AS c
LEFT JOIN customer_entity_varchar namefirst ON c.entity_id = namefirst.entity_id AND namefirst.attribute_id = 5
LEFT JOIN customer_entity_varchar namelast ON c.entity_id = namelast.entity_id AND namelast.attribute_id = 7
LEFT JOIN customer_entity_varchar company ON c.entity_id = company.entity_id AND company.attribute_id = 138
LEFT JOIN customer_entity_int  default_billing ON
        (`default_billing`.`entity_id` = `c`.`entity_id`) AND
        (`default_billing`.`attribute_id` = '13')
LEFT JOIN customer_address_entity_text caet_st ON caet_st.entity_id = default_billing.value AND caet_st.attribute_id = 25
LEFT JOIN customer_address_entity_varchar caev_city ON caev_city.entity_id = default_billing.value AND caev_city.attribute_id = 26
LEFT JOIN customer_address_entity_varchar caev_state ON caev_state.entity_id = default_billing.value AND caev_state.attribute_id = 28
LEFT JOIN customer_address_entity_varchar caev_zip ON caev_zip.entity_id = default_billing.value AND caev_zip.attribute_id = 30
LEFT JOIN customer_address_entity_varchar caev_co ON caev_co.entity_id = default_billing.value AND caev_co.attribute_id = 27
LEFT JOIN customer_address_entity_varchar caev_ph ON caev_ph.entity_id = default_billing.value AND caev_ph.attribute_id = 31
LEFT JOIN sales_flat_order sfo ON c.entity_id = sfo.customer_id
LEFT JOIN customer_entity_varchar cev_sub ON c.entity_id = cev_sub.entity_id AND cev_sub.attribute_id = 147
WHERE
	CONVERT_TZ(c.created_at, 'GMT', 'America/Los_Angeles') BETWEEN '{Customer Create Start Date:date} 00:00:00' AND '{Customer Create End Date:date} 23:59:59'
GROUP BY c.entity_id
ORDER BY c.entity_id DESC
ENDOFSQL;

if (!Mage::getModel("sqlReports/sqlReport")->load($subscriptionReportTitle ,'title')->getId())
{
	// need to create it
	$subscriptionReport = Mage::getModel('sqlReports/sqlReport')
							->setSql($sql)
							->setCategoryId($marketingCategory->getId())
							->setTitle($subscriptionReportTitle);
	$subscriptionReport->save();
	
	Mage::log("Created report '$subscriptionReportTitle', has ID of {$subscriptionReport->getId()}");
}
else
{

	//need to update
	$subscriptionReport = Mage::getModel("sqlReports/sqlReport")->load($subscriptionReportTitle ,'title')
		->setSql($sql);
	$subscriptionReport->save();
}

