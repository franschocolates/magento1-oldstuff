<?php

$defaultContent = 'Your request will be forwarded to the appropriate person and you will receive a response by the next business day.<p style = "margin-bottom: 0; margin-top: 9px;">Thank you!</p>';
$donationsContent = "Thank you for contacting Fran&rsquo;s Chocolates with a donation request. Fran&rsquo;s is committed to supporting events and fundraisers which benefit our local community. Due to the high volume of donation requests we receive, please allow 4 weeks for a response. We look forward to learning more about your work!<p style = 'margin-bottom: 0; margin-top: 9px;'>Sincerely, The Team at Fran's Chocolates</p>";

$defaultTitle = 'WE HAVE RECEIVED YOUR INQUIRY.';


// ORDER ASSISTANCE CMS BLOCK
$blockIdentifier = 'email_autoreply-order_assistance';
$getCMSBlock = Mage::getModel('cms/block')
    ->load($blockIdentifier, 'identifier');
$getCMSBlock->setContent($defaultContent);
$getCMSBlock->setTitle($defaultTitle);
$getCMSBlock->setStores(array(1));
$getCMSBlock->setIdentifier($blockIdentifier);
$getCMSBlock->save();


// REQUESTING A CATALOG CMS BLOCK
$blockIdentifier = 'email_autoreply-catalog_request';
$getCMSBlock = Mage::getModel('cms/block')
    ->load($blockIdentifier, 'identifier');
$getCMSBlock->setContent($defaultContent);
$getCMSBlock->setTitle($defaultTitle);
$getCMSBlock->setStores(array(1));
$getCMSBlock->setIdentifier($blockIdentifier);
$getCMSBlock->save();


// INTERNATIONAL CMS BLOCK
$blockIdentifier = 'email_autoreply-international_orders';
$getCMSBlock = Mage::getModel('cms/block')
    ->load($blockIdentifier, 'identifier');
$getCMSBlock->setContent($defaultContent);
$getCMSBlock->setTitle($defaultTitle);
$getCMSBlock->setStores(array(1));
$getCMSBlock->setIdentifier($blockIdentifier);
$getCMSBlock->save();

// PRESS INQUIRIES CMS BLOCK
$blockIdentifier = 'email_autoreply-press_inquiries';
$getCMSBlock = Mage::getModel('cms/block')
    ->load($blockIdentifier, 'identifier');
$getCMSBlock->setContent($defaultContent);
$getCMSBlock->setTitle($defaultTitle);
$getCMSBlock->setStores(array(1));
$getCMSBlock->setIdentifier($blockIdentifier);
$getCMSBlock->save();


// DONATIONS CMS BLOCK
$blockIdentifier = 'email_autoreply-donations';
$getCMSBlock = Mage::getModel('cms/block')
    ->load($blockIdentifier, 'identifier');
$getCMSBlock->setContent($donationsContent);
$getCMSBlock->setTitle($defaultTitle);
$getCMSBlock->setStores(array(1));
$getCMSBlock->setIdentifier($blockIdentifier);
$getCMSBlock->save();


// JOBS CMS BLOCK
$blockIdentifier = 'email_autoreply-jobs';
$getCMSBlock = Mage::getModel('cms/block')
    ->load($blockIdentifier, 'identifier');
$getCMSBlock->setContent($defaultContent);
$getCMSBlock->setTitle($defaultTitle);
$getCMSBlock->setStores(array(1));
$getCMSBlock->setIdentifier($blockIdentifier);
$getCMSBlock->save();


// WHOLESALE CMS BLOCK
$blockIdentifier = 'email_autoreply-wholesale';
$getCMSBlock = Mage::getModel('cms/block')
    ->load($blockIdentifier, 'identifier');
$getCMSBlock->setContent($defaultContent);
$getCMSBlock->setTitle($defaultTitle);
$getCMSBlock->setStores(array(1));
$getCMSBlock->setIdentifier($blockIdentifier);
$getCMSBlock->save();


// OTHER CMS BLOCK
$blockIdentifier = 'email_autoreply-other';
$getCMSBlock = Mage::getModel('cms/block')
    ->load($blockIdentifier, 'identifier');
$getCMSBlock->setContent($defaultContent);
$getCMSBlock->setTitle($defaultTitle);
$getCMSBlock->setStores(array(1));
$getCMSBlock->setIdentifier($blockIdentifier);
$getCMSBlock->save();