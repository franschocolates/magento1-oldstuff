<?php

$homeAdOneContent = '<p><a href="seasonal"><img src="{{media url="wysiwyg/20pc_frans_assortment_green_with_yellow_tile.png"}}" alt="" /></a></p>
<h2><a href="seasonal">fran\'s assortment</a></h2>
<p><em>Enjoy an assortment of our handmade truffles and award-winning salt caramels.</em></p>
<p><a href="seasonal">shop now &raquo;</a></p>';

$homeAdTwoContent = '<p><a href="caramels/salted-caramels"><img src="{{media url="wysiwyg/20pc_gray_and_smoked_tile.png"}}" alt="" /></a></p>
<h2><a href="caramels/salted-caramels">salted caramels</a></h2>
<p><em>Featured on the Cooking Channel\'s Unique Sweets – our signature, classic salted caramels.</em></p>
<p><a href="caramels/salted-caramels">shop now &raquo;</a></p>';

// Set the content to Home Ad Top
$getHomeAdOne = Mage::getModel('cms/block')
    ->load('home_ad_one', 'identifier');

if ($getHomeAdOne->getBlockId())
{
    $getHomeAdOne->setContent($homeAdOneContent)->save();;
}

// Set the content to Home Ad Bottom
$getHomeAdTwo = Mage::getModel('cms/block')
    ->load('home_ad_two', 'identifier');

if ($getHomeAdTwo->getBlockId())
{
    $getHomeAdTwo->setContent($homeAdTwoContent)->save();;
}