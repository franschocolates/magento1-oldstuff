<?php

/* create reports */
$categoryTitle = 'AccPac';
$accPacCategory = Mage::getModel('sqlReports/sqlReportCategory');

$accPacCategory->load($categoryTitle, 'title');

if (!$accPacCategory->getId())
{
	Mage::log("Creating SqlReports category $categoryTitle");
	// doesn't exist, create.
	$accPacCategory->setTitle($categoryTitle);
	$accPacCategory->setPosition(3);
	
	$accPacCategory->save();
}


// now do the actual reports
$reportsForCategoryAndTitle = Mage::getModel("sqlReports/sqlReport")
						->getCollection()
  						->addFieldToFilter("category_id", $accPacCategory->getId());
  						
$accPacReconciliationReportTitle = 'AccPac - Reconciliation Summary Report';
$reportsForCategoryAndTitle->addFieldToFilter('title', $accPacReconciliationReportTitle)
					->load();

if ($reportsForCategoryAndTitle->count() == 0)
{
	// need to create it
	$sql = <<<ENDOFSQL
SELECT sfo.increment_id AS orderid
	, COALESCE(sfo.accounting_label, sfp.accounting_label) AS order_type
	, (sfo.subtotal + sfo.discount_amount) AS subtotal /* must apply discount to subtotal, it's adding a negative */
	, 0 AS discount
	, sfo.tax_amount AS tax
	, sfo.shipping_amount AS shipping_cost
	, sfo.grand_total
	/* only use customer's ID when they paid by invoice */
	, IF (sfop.method='paybyinvoice', COALESCE(cev.`value`, fal.accpac_customer_id), fal.accpac_customer_id) AS CustomerID /* use customer ID on customer if present */
FROM
	sales_flat_order AS sfo
/* try and join to parent if we have that field */
LEFT OUTER JOIN 
	sales_flat_order AS sfp
ON
	sfp.entity_id = sfo.multiship_parent_id
INNER JOIN
	sales_flat_invoice AS sfi
ON
	sfi.order_id = COALESCE(sfo.entity_id, sfp.entity_id)
/* try and find out if invoice to determine account # */
INNER JOIN
	sales_flat_order_payment AS sfop
ON
	sfop.parent_id = sfi.order_id
/* doing outer to highlight if something is missing a label */
LEFT OUTER JOIN
	frans_accounting_label AS fal
ON
	fal.label_name = COALESCE(sfo.accounting_label, sfp.accounting_label)
/* try and get customer specific ID */
LEFT OUTER JOIN
	customer_entity_varchar AS cev
ON
	cev.entity_id = COALESCE(sfo.customer_id, sfp.customer_id)
AND
	cev.attribute_id = (SELECT attribute_id FROM eav_attribute WHERE attribute_code = 'customer_number')
WHERE
	(sfo.is_multiship_parent IS NULL OR sfo.is_multiship_parent = 0) /* only get normal and child, not parent */
AND
	DATE(sfi.created_at) BETWEEN '{Start Date:date}' AND '{End Date:date}'	
ENDOFSQL;
	$accPacReconciliationReport = Mage::getModel('sqlReports/sqlReport')
							->setSql($sql)
							->setCategoryId($accPacCategory->getId())
							->setTitle($accPacReconciliationReportTitle);
	$accPacReconciliationReport->save();
	
	Mage::log("Created report '$accPacReconciliationReportTitle', has ID of {$accPacReconciliationReport->getId()}");
}
else
{
	Mage::log("Report '$accPacReconciliationReportTitle' already exists, not creating.");
}

