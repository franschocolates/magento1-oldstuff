<?php
// setup the extended shipping zone method times for 2 day and overnight
// basically, if they go over a weekend, they can take (normal amount) + 2 days (for sat/sun)
// so 2 day can be 4 days
// and overnight can be 3 days
$unitedStatesZone = Mage::getModel('frans/shippingZone')->load('United States', 'zone_name');

$methodTime2DayExtended = Mage::getModel('frans/shippingZoneMethodTime')
                            ->getCollection()
                            ->addFieldToFilter('zone_id', $unitedStatesZone->getId())
                            ->addFieldToFilter('shipping_method', 'fedex_FEDEX_2_DAY')
                            ->addFieldToFilter('time_in_days', 3)
                            ->getFirstItem();

$methodTime2DayExtended
    ->setZoneId($unitedStatesZone->getId())
    ->setShippingMethod('fedex_FEDEX_2_DAY')
    ->setDeliveryDays('1,2,3,4,5')
    ->setDeliveryType(0)
    ->setTimeInDays(4)
    ->save();

// overnight extended shouldn't exist anywhere
$methodTimeOvernightExtended = Mage::getModel('frans/shippingZoneMethodTime')
    ->getCollection()
    ->addFieldToFilter('zone_id', $unitedStatesZone->getId())
    ->addFieldToFilter('shipping_method', 'fedex_STANDARD_OVERNIGHT')
    ->addFieldToFilter('time_in_days', 2)
    ->getFirstItem();
$methodTimeOvernightExtended
    ->setZoneId($unitedStatesZone->getId())
    ->setShippingMethod('fedex_STANDARD_OVERNIGHT')
    ->setDeliveryDays('1,2,3,4,5')
    ->setDeliveryType(0)
    ->setTimeInDays(3)
    ->save();
