<?php
// insert any data inserts/changes here
$currentAdminOnly = Mage::getStoreConfig('shipping/rules/adminOnlyCarriers');

if (!$currentAdminOnly || stristr($currentAdminOnly, 'freeshipping') === false)
{
    $adminMethods = ($currentAdminOnly) ? explode(',', $currentAdminOnly) : array();

    $adminMethods[] = 'freeshipping';

    Mage::getModel('core/config')->saveConfig('shipping/rules/adminOnlyCarriers', implode(',', $adminMethods));
}

// make sure free shipping is enabled
Mage::getModel('core/config')->saveConfig('carriers/freeshipping/active', true);
// give free shipping a name for virtual gift cards
Mage::getModel('core/config')->saveConfig('carriers/freeshipping/title', 'Virtual Shipping');