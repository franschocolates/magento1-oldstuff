<?php

Mage::log("Adding the accpac location and freight codes");

$labels = array(
			'BV' => array(
					'accpac_location_code' => 'BELL'
					, 'accpac_freight_code' => 'BELFRT'
				)
			, 'DT' => array(
					'accpac_location_code' => 'FSEA'
					, 'accpac_freight_code' => 'FSFRT'
				)
			, 'MFG' => array(
					'accpac_location_code' => 'MFG'
					, 'accpac_freight_code' => 'FRT'
				)
			, 'MO' => array(
					'accpac_location_code' => '03MAIL'
					, 'accpac_freight_code' => 'MOFRT'
				)
			, 'UV' => array(
					'accpac_location_code' => 'UVILL'
					, 'accpac_freight_code' => 'UVLFRT'
				)
			, 'WEB' => array(
					'accpac_location_code' => '03MAIL'
					, 'accpac_freight_code' => 'MOFRT'
				)
			, 'BUS' => array(
					'accpac_location_code' => '03MAIL'
					, 'accpac_freight_code' => 'MOFRT'
				)
);

foreach ($labels as $label => $data)
{
	$label = Mage::getModel('frans/accounting_label')
				->load($label, 'label_name');
	if ($label->getId())
	{
		$label->addData($data);
		$label->save();
	}
	else
	{
		Mage::throwException("Could not find matching accounting label for $label");
	}
}

