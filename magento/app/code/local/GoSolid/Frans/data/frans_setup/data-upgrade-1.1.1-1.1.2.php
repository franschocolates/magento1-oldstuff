<?php

$newWeatherAlertMessage = <<<EOL
<p>If temperatures exceed 75&deg; F, we will pack your chocolates with ice and mylar to prevent them from melting. In addition, to reduce the time in transit orders received Thursday and Friday will be held over the weekend to ship on Monday.</p>
EOL;

$cmsBlock = Mage::getModel('cms/block')->load('warm-weather-alert', 'identifier');
$cmsBlock->setContent($newWeatherAlertMessage);
$cmsBlock->setTitle('Warm Weather Alert');
$cmsBlock->setStores(array(1));
$cmsBlock->setIdentifier('warm-weather-alert');
$cmsBlock->save();