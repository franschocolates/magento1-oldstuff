<?php
/**
 * Created by GoSolid.
 * Date: 9/17/15
 * Time: 4:52 PM
 */
require_once 'Mage/Cms/controllers/IndexController.php';

class GoSolid_Frans_Cms_IndexController extends Mage_Cms_IndexController {


	public function indexAction($coreRoute = null)
	{
	    $pageId = Mage::getStoreConfig(Mage_Cms_Helper_Page::XML_PATH_HOME_PAGE);
	    if ($pageId == -1 || !Mage::helper('cms/page')->renderPage($this, $pageId)) {
	        $this->_forward('defaultIndex');
	    }
	}

	/**
	 * Default index action, but don't show 404 headers
	 * Used because default page no longer exists as CMS page
	 *
	 */
	public function defaultIndexAction()
	{
		$this->loadLayout();
		$this->renderLayout();
	}

}