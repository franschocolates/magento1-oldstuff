<?php

require_once 'Mage/Cms/controllers/IndexController.php';

class GoSolid_Frans_Cms_NewsletterController extends Mage_Cms_IndexController {

	protected function _initAction() {
		$this->loadLayout();
		return $this;
	}

	public function indexAction() {

		$this->_initAction();
		$this->renderLayout();
	}

	protected function _getNewsletterModalBlock()
	{
		return $this->getLayout()
			->createBlock('banner/newslettertile')
			->setTemplate('banner/newsletter-modal.phtml');
	}

	public function formAction()
	{

		$ajaxRequest = $this->getRequest()->getParam('ajax');
		//check if this is a ajax request
		if ($ajaxRequest != 1) {
			return $this->_redirect('*/*/');
		}

		$responseAjax = new Varien_Object();

		$responseAjax->setHtml(
			$this->_getNewsletterModalBlock(false)->toHtml()
		);

		$responseAjax->setStatus('success');

		$this->getResponse()->setBody($responseAjax->toJson());

	}

	public function tileSubscribeNewsletterAction(){
		$response = array();

		$data = $this->getRequest()->getPost();
		$email = $data['email'];
		$step = $data['step'];
		$origin = $data['origin'];

		//check if email is valid
		if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
			//if valid email, continue
			$customer = Mage::getModel('customer/customer');
			$websiteId = Mage::app()->getWebsite()->getId();

			if ($websiteId) {
				$customer->setWebsiteId($websiteId);
			}

			$customer->loadByEmail($email);

			$api = Mage::getSingleton('monkey/api');
			$defaultList = Mage::getStoreConfig(Ebizmarts_MageMonkey_Model_Config::GENERAL_LIST);
			$mailChimpEmailInfo = $api->listMemberInfo($defaultList, $email);

			$isEmailNotInMC = ($mailChimpEmailInfo['data'][0]['error'] === 'The email address passed does not exist on this list') || $mailChimpEmailInfo['data'][0]['status'] === 'unsubscribed';

			if($customer->getId()){
				//if customer exists, update customer. The observer will save the data to MC when the customer is saved
				$subscriptions = explode(",", $customer->getSubscriptions());
				if(!in_array('1',$subscriptions)){

					$customer->setIsSubscribed(1);
					$customer->subscribeEmail(); //this sets the custom subscriptions table.
					$customer->save();

					$response['status']	= 'success';
					$response['email'] = $email;
					$response['lastStep'] = $step;
					$response['msg'][]	= Mage::helper('customer')->__('Customer already existed. Successfully subscribed.');

				} elseif($isEmailNotInMC) {
					//if email is NOT in MC OR if the email is currently unsubscribed, subscribe them

					$merge_vars = array('ORIGIN' => "Magento"); //hardcoded magento origin.

					$api->listSubscribe($defaultList, $email, $merge_vars, $email_type = 'html', $double_optin = false, $update_existing = false, $replace_interests = true, $send_welcome = false);

					$response['status']	= 'success';
					$response['email'] = $email;
					$response['lastStep'] = $step;
					$response['msg'][]	= Mage::helper('customer')->__('Email subscribed to MC');
				}
				else
				 {
					$response['status']	= 'success';
					$response['email'] = $email;
					$response['lastStep'] = $step;
					$response['msg'][]	= Mage::helper('customer')->__('Customer already existed and was already subscribed.');
				}
			} else {
				//if customer does NOT exist, check if email is in Mail Chimp

				if($isEmailNotInMC){

					$merge_vars = array('ORIGIN' => "Magento"); //hardcoded magento origin.

					//if email is NOT in MC OR if the email is currently unsubscribed, subscribe them
					$api->listSubscribe($defaultList, $email, $merge_vars, $email_type = 'html', $double_optin = false, $update_existing = false, $replace_interests = true, $send_welcome = false);

					$response['status']	= 'success';
					$response['email'] = $email;
					$response['lastStep'] = $step;
					$response['msg'][]	= Mage::helper('customer')->__('Email subscribed to MC');
				} else {
					//if email is in MC AND customer is currently not unsubscribed, do nothing
					$response['status']	= 'success';
					$response['email'] = $email;
					$response['lastStep'] = $step;
					$response['msg'][]	= Mage::helper('customer')->__('Email already in MC');

				}

			}

		} else{
			//if NOT valid email, return error
			$response['status']	= 'error';
			$response['msg'][]	= Mage::helper('customer')->__('please enter a valid email address.');
		}

		echo json_encode($response);
	}

	public function updateDobAction(){

		$response = array();

		$data = $this->getRequest()->getPost();


		$email = $data['email'];
		$step = $data['step'];

		if($data['month'] != null && $data['day'] != null){
			//if DOB found, continue
			$customer = Mage::getModel('customer/customer');
			$websiteId = Mage::app()->getWebsite()->getId();
			$dob = $data['month'] .'/'. $data['day'];

			if ($websiteId) {
				$customer->setWebsiteId($websiteId);
			}

			$customer->loadByEmail($email);


			if($customer->getId()){
				//if customer exists, update customer. The observer will save the data to MC when the customer is saved


				$customer->setDob($dob);
				$customer->save();

				$response['status']	= 'success';
				$response['lastStep'] = $step;
				$response['msg'][]	= Mage::helper('customer')->__('Customer DOB updated');

			} else {
				//if customer does NOT exist, update the record directly in MC

				$api = Mage::getSingleton('monkey/api');
				$defaultList = Mage::getStoreConfig(Ebizmarts_MageMonkey_Model_Config::GENERAL_LIST);

				$merge_vars = array('BDAY' => $dob);

				$api->listUpdateMember($defaultList, $email, $merge_vars, $email_type = '', $replace_interests = true);

				$response['status']	= 'success';
				$response['lastStep'] = $step;
				$response['msg'][]	= Mage::helper('customer')->__('Email DOB updated in MC');


			}

		} else {
			//if no DOB provided, report error
			$response['status']	= 'error';
			$response['msg'][]	= Mage::helper('customer')->__('please enter your birthday.');
		}

		echo json_encode($response);
	}

}