<?php
class GoSolid_Frans_StoresController extends Mage_Core_Controller_Front_Action
{
    
	protected function _initAction() {
		$this->loadLayout();
		$this->_initLayoutMessages('customer/session');
		return $this;
	}

	public function indexAction() {
		$this->_initAction();

		// add breadcrumbs
		$breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');
		if ($breadcrumbs) {
			$breadcrumbs->addCrumb('home', array(
				'label' => $this->__('Home'),
				'title' => $this->__('Go to Home Page'),
				'link'  => Mage::getBaseUrl()
			));
			$breadcrumbs->addCrumb('stores', array(
				'label' => $this->__("Fran's Stores")
			));
		}

		$this->renderLayout();
	}

	public function viewAction() {
		$this->_initAction();

		// look up store requested in URL
		$storeId = $this->getRequest()->getParam('id');
		$store = Mage::getModel('frans/retailStore')->load($storeId, 'id');

		// if store ID is invalid, redirect to main stores page
		if(!$store->getId()){
			$this->getResponse()->setRedirect(Mage::getUrl('*/*'));
		}

		// add breadcrumbs
		$breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');
		if ($breadcrumbs) {
			$breadcrumbs->addCrumb('home', array(
				'label' => $this->__('Home'),
				'title' => $this->__('Go to Home Page'),
				'link'  => Mage::getBaseUrl()
			));

			$breadcrumbs->addCrumb('stores', array(
				'label' => $this->__('Fran\'s Stores'),
				'title' => $this->__('Go to Fran\'s Stores'),
				'link'  => Mage::getBaseUrl() . "stores/"
			));

			$breadcrumbs->addCrumb('store', array(
				'label' => $store->getName()
			));
		}

		$this->renderLayout();
	}
	
}