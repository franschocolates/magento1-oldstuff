<?php
class GoSolid_Frans_GiftCardsController extends Mage_Core_Controller_Front_Action
{
	protected function _initAction() {
		$this->loadLayout();

		// add breadcrumbs
		$breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');
		if ($breadcrumbs) {

			$breadcrumbs->addCrumb('home', array(
				'label' => $this->__('Home'),
				'title' => $this->__('Go to Home Page'),
				'link'  => Mage::getBaseUrl()
			));
			$breadcrumbs->addCrumb('gift-cards', array(
				'label' => $this->__('Gift Cards'),
				'title' => $this->__('Gift Cards'),
				'link'  => Mage::getUrl('gift-cards')
			));
			$breadcrumbs->addCrumb('check-balance', array(
				'label' => $this->__('Check Balance')
			));

		}
		return $this;
	}
 
	public function indexAction() 
	{
		$this->_initAction();
		$this->renderLayout();
	}

	/**
	 * Get checkout session model instance
	 *
	 * @return Mage_Checkout_Model_Session
	 */
	protected function _getSession()
	{
		return Mage::getSingleton('checkout/session');
	}

	public function addtocartAction()
	{
		// DH add custom givex log
		Mage::Helper('givex')->logError("Starting addtocartAction function.", Zend_Log::INFO);

		$params = $this->getRequest()->getParams();
		//var_dump($params);

		$cart = Mage::getSingleton('checkout/cart');
		$cart->init();

		// load the product by sku, using fix for "The stock item for Product is not valid" exception found here:
		// http://inchoo.net/ecommerce/magento/programatically-add-bundle-product-to-cart-n-magento/comment-page-1/#comment-8926
		$_product = Mage::getModel('catalog/product');
		$_product->load($_product->getIdBySku($params['sku']));

		// validate product type
		if($_product->getTypeId() != 'giftcard' && $_product->getTypeId() != 'virtualgiftcard')
		{
			// product does not appear to be a gift card at all
			$err = $this->__('This is not a known gift card type.');
			Mage::getSingleton('catalog/session')->addError($err);

			// DH add custom givex log
			Mage::Helper('givex')->logError($err, Zend_Log::ERR, true);

			$this->_redirectReferer();
			return;
		}

		$qty = $params['quantity'];
		$requestInfo = array(
			'product'			=> $_product->getId(),
			'qty'				=> $qty,
			'options'           => $params['option']
		);

		// add to cart
		try
		{
			$cart->addProduct($_product, $requestInfo);
			$cart->save();
		}
		catch(Mage_Core_Exception $e)
		{
			if ($this->_getSession()->getUseNotice(true)) {
				$this->_getSession()->addNotice($e->getMessage());
			} else {
				$messages = array_unique(explode("\n", $e->getMessage()));
				foreach ($messages as $message) {
					// DH add custom givex log
					Mage::Helper('givex')->logError($message, Zend_Log::ERR, true);
					$this->_getSession()->addError($message);
				}
			}
			$url = $this->_getSession()->getRedirectUrl(true);
			$this->getResponse()->setRedirect($url);
			return;
		}
/*
		// get the last item added to the cart
		$items = $cart->getQuote()->getAllVisibleItems();
		$lastItem = end($items);

		// add options for the item
		$lastItem->addOption(
			array(
				'product_id' => $_product->getId(),
				'code' => 'gc_amount',
				'value' => $params['gc_amount']
			)
		);
		$lastItem->save();

		$cart->save();
*/
		// redirect to the cart
		$this->getResponse()->setRedirect(Mage::getUrl('checkout/cart'));

		// DH add custom givex log
		Mage::Helper('givex')->logError("Successfully added to cart.", Zend_Log::INFO);

		return $this;
	}

	public function getCardPreviewAction()
	{
		$this->_initAction();
		$this->renderLayout();
	}

}