<?php
class GoSolid_Frans_AjaxController extends Mage_Core_Controller_Front_Action
{
	protected function _initAction() {
		$this->loadLayout();
		return $this;
	}

	public function toplinksAction()
	{
		$this->_initAction();
		$this->renderLayout();
	}

    public function virtualgiftcardsAction()
    {

        $virtualGiftCard = Mage::getModel('catalog/product')
            ->loadByAttribute('sku', GoSolid_Frans_Block_Giftcards_Purchase::VIRTUAL_GIFT_CARD_SKU);

        $this->_initAction();

        $this->getLayout()->getBlock('content')->getChild('virtual.giftcard')->setProduct($virtualGiftCard);

        $this->renderLayout();
    }

}