<?php
class GoSolid_Frans_IndexController extends Mage_Core_Controller_Front_Action
{
	protected function _initAction() {
		$this->loadLayout();
		return $this;
	}   
 
	public function indexAction() {
		
		$this->_initAction();
		$this->renderLayout();
	}
	//TODO: Remove this test function, logCleanTestAction
	public function logCleanTestAction(){
		Mage::getModel('log/cron')->logClean();
	}
}