<?php
class GoSolid_Frans_CustomordersController extends Mage_Core_Controller_Front_Action
{
    
	protected function _initAction() {
		$this->loadLayout();

		// add breadcrumbs
		$breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');
		if ($breadcrumbs) {

			$breadcrumbs->addCrumb('home', array(
				'label' => $this->__('Home'),
				'title' => $this->__('Go to Home Page'),
				'link'  => Mage::getBaseUrl()
			));
			$breadcrumbs->addCrumb('custom-orders', array(
				'label' => $this->__('large & custom orders')
			));

		}

		$this->_initLayoutMessages('customer/session');
		return $this;
	}
	
	public function indexAction() {
		$this->_initAction();
		$this->renderLayout();
	}
	
}