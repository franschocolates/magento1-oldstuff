<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Customer
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Customer account controller
 *
 * @category   Mage
 * @package    Mage_Customer
 * @author      Magento Core Team <core@magentocommerce.com>
 */


require_once 'Mage/Customer/controllers/AccountController.php';

class GoSolid_Frans_Customer_AccountController extends Mage_Customer_AccountController
{

	public function indexAction()
	{

		$this->loadLayout();
		$this->_initLayoutMessages('customer/session');
		$this->_initLayoutMessages('catalog/session');

		$this->getLayout()->getBlock('content')->append(
			$this->getLayout()->createBlock('customer/account_dashboard')
		);
		$this->getLayout()->getBlock('head')->setTitle($this->__('Your Account'));

		// add breadcrumbs
		$breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');
		if ($breadcrumbs) {
			$breadcrumbs->addCrumb('home', array(
				'label' => $this->__('Home'),
				'title' => $this->__('Go to Home Page'),
				'link'  => Mage::getBaseUrl()
			));
			$breadcrumbs->addCrumb('customer', array(
				'label' => $this->__("Your Account")
			));
		}

		$this->renderLayout();
	}
	
	/**
     * Create customer account action
     */
    public function createPostAction()
    {
    	
        $session = $this->_getSession();
        if ($session->isLoggedIn()) {
            $this->_redirect('*/*/');
            return;
        }
        $session->setEscapeMessages(true); // prevent XSS injection in user input
        if ($this->getRequest()->isPost()) {
            $errors = array();

            if (!$customer = Mage::registry('current_customer')) {
                $customer = Mage::getModel('customer/customer')->setId(null);
            }

            /* @var $customerForm Mage_Customer_Model_Form */
            $customerForm = Mage::getModel('customer/form');
            $customerForm->setFormCode('customer_account_create')
                ->setEntity($customer);

            $customerData = $customerForm->extractData($this->getRequest());


            if ($this->getRequest()->getParam('is_subscribed', false)) {
                $customer->setIsSubscribed(1);
                // this is deprecated, we should be using $customer->subscribeEmail()
                $customer->setSubscriptions(1); //this sets the custom subscriptions table.
            }

            /**
             * Initialize customer group id
             */
            $customer->getGroupId();

            if ($this->getRequest()->getPost('create_address')) {
                /* @var $address Mage_Customer_Model_Address */
                $address = Mage::getModel('customer/address');
                /* @var $addressForm Mage_Customer_Model_Form */
                $addressForm = Mage::getModel('customer/form');
                $addressForm->setFormCode('customer_register_address')
                    ->setEntity($address);

                $addressData    = $addressForm->extractData($this->getRequest(), 'address', false);
                $addressErrors  = $addressForm->validateData($addressData);
                if ($addressErrors === true) {
                    $address->setId(null)
                        ->setIsDefaultBilling($this->getRequest()->getParam('default_billing', false))
                        ->setIsDefaultShipping($this->getRequest()->getParam('default_shipping', false));
                    $addressForm->compactData($addressData);
                    $customer->addAddress($address);
                    $addressErrors = $address->validate();
                    if (is_array($addressErrors)) {
                        $errors = array_merge($errors, $addressErrors);
                    }
                } else {
                    $errors = array_merge($errors, $addressErrors);
                }
            }

            try {
                $customerErrors = $customerForm->validateData($customerData);
                if ($customerErrors !== true) {
                    $errors = array_merge($customerErrors, $errors);
                } else {
                    $customerForm->compactData($customerData);
                    if($this->getRequest()->getPost('business-account') == 'on'){
                    	$customer->setCompanyName($this->getRequest()->getPost('company_name'));
                        $defaultBusinessGroupId = Mage::getStoreConfig('frans/business_customers/default_customer_group_id');
                        if ($defaultBusinessGroupId)
                        {
                            $customer->setGroupId($defaultBusinessGroupId);
                        }
                    }
                    $customer->setPassword($this->getRequest()->getPost('password'));
                    $customer->setConfirmation($this->getRequest()->getPost('confirmation'));
                    $customerErrors = $customer->validate();
                    if (is_array($customerErrors)) {
                        $errors = array_merge($customerErrors, $errors);
                    }
                }

                $validationResult = count($errors) == 0;

                if (true === $validationResult) {
                    $customer->save();


                    Mage::dispatchEvent('customer_register_success',
                        array('account_controller' => $this, 'customer' => $customer)
                    );

                    if ($customer->isConfirmationRequired()) {

                        $customer->sendNewAccountEmail(
                            'confirmation',
                            $session->getBeforeAuthUrl(),
                            Mage::app()->getStore()->getId()
                        );
                        $session->addSuccess($this->__('Account confirmation is required. Please, check your email for the confirmation link. To resend the confirmation email please <a href="%s">click here</a>.', Mage::helper('customer')->getEmailConfirmationUrl($customer->getEmail())));
                        $this->_redirectSuccess(Mage::getUrl('*/*/edit', array('_secure'=>true)));
                        return;
                    } else {
                        $session->setCustomerAsLoggedIn($customer);
                        $url = $this->_welcomeCustomer($customer);
                        $this->_redirectSuccess( Mage::getUrl('*/*/edit') ); //MAC: I updated the url to go from the config to the edit screen.
                        return;
                    }
                } else {
                    $session->setCustomerFormData($this->getRequest()->getPost());
                    if (is_array($errors)) {
                        foreach ($errors as $errorMessage) {
                            $session->addError($errorMessage);
                        }
                    } else {
                        $session->addError($this->__('Invalid customer data'));
                    }
                }
            } catch (Mage_Core_Exception $e) {
                $session->setCustomerFormData($this->getRequest()->getPost());
                if ($e->getCode() === Mage_Customer_Model_Customer::EXCEPTION_EMAIL_EXISTS) {
                    $url = Mage::getUrl('customer/account/forgotpassword');
                    $message = $this->__('There is already an account with this email address. If you are sure that it is your email address, <a href="%s">click here</a> to get your password and access your account.', $url);
                    $session->setEscapeMessages(false);
                } else {
                    $message = $e->getMessage();
                }
                $session->addError($message);
            } catch (Exception $e) {
                $session->setCustomerFormData($this->getRequest()->getPost())
                    ->addException($e, $this->__('Cannot save the customer.'));
            }
        }
        $source = $this->getRequest()->getParam('businessgifting', false);
        if( filter_var($source, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) ) {
        	$this->_redirectError(Mage::getUrl('frans/businessgifting/index', array('_secure' => true, 'tab' => 'signup')));
        } else {
        	$this->_redirectError(Mage::getUrl('*/*/login', array('_secure' => true)));
        }
    }

	/**
	 * Forgot customer account information page
	 */
	public function editAction()
	{
		$this->loadLayout();
		$this->_initLayoutMessages('customer/session');
		$this->_initLayoutMessages('catalog/session');

		$block = $this->getLayout()->getBlock('customer_edit');
		if ($block) {
			$block->setRefererUrl($this->_getRefererUrl());
		}
		$data = $this->_getSession()->getCustomerFormData(true);
		$customer = $this->_getSession()->getCustomer();
		if (!empty($data)) {
			$customer->addData($data);
		}
		if ($this->getRequest()->getParam('changepass') == 1){
			$customer->setChangePassword(1);
		}

		// add breadcrumbs
		$breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');
		if ($breadcrumbs) {
			$breadcrumbs->addCrumb('home', array(
				'label' => $this->__('Home'),
				'title' => $this->__('Go to Home Page'),
				'link'  => Mage::getBaseUrl()
			));
			$breadcrumbs->addCrumb('customer', array(
				'label' => $this->__("Your Account")
			));
		}

		$this->getLayout()->getBlock('head')->setTitle($this->__('Your Account'));
		$this->getLayout()->getBlock('messages')->setEscapeMessageFlag(true);
		$this->renderLayout();
	}

	protected function _welcomeCustomer(Mage_Customer_Model_Customer $customer, $isJustConfirmed = false)
    {

        if($customer->getGroupId() === '10'){
            $this->_getSession()->addSuccess(
            // don't change message string below, it's referred to in customer/account/navigation.phtml template
            // changing it via locale translation is fine
                $this->__('Welcome, your business account has been created.', Mage::app()->getStore()->getFrontendName())
            );
        } else {
            $this->_getSession()->addSuccess(
            // don't change message string below, it's referred to in customer/account/navigation.phtml template
            // changing it via locale translation is fine
                $this->__('Welcome, your account has been created.', Mage::app()->getStore()->getFrontendName())
            );
        }
        if ($this->_isVatValidationEnabled()) {
            // Show corresponding VAT message to customer
            $configAddressType = Mage::helper('customer/address')->getTaxCalculationAddressType();
            $userPrompt = '';
            switch ($configAddressType) {
                case Mage_Customer_Model_Address_Abstract::TYPE_SHIPPING:
                    $userPrompt = $this->__('If you are a registered VAT customer, please click <a href="%s">here</a> to enter you shipping address for proper VAT calculation', Mage::getUrl('customer/address/edit'));
                    break;
                default:
                    $userPrompt = $this->__('If you are a registered VAT customer, please click <a href="%s">here</a> to enter you billing address for proper VAT calculation', Mage::getUrl('customer/address/edit'));
            }
            $this->_getSession()->addSuccess($userPrompt);
        }

        $customer->sendNewAccountEmail(
            $isJustConfirmed ? 'confirmed' : 'registered',
            '',
            Mage::app()->getStore()->getId(),
            $this->getRequest()->getPost('business-account')
        );

        $successUrl = Mage::getUrl('*/*/index', array('_secure'=>true));
        if ($this->_getSession()->getBeforeAuthUrl()) {
            $successUrl = $this->_getSession()->getBeforeAuthUrl(true);
        }
        return $successUrl;
    }
	
	public function creditcardAction()
	{
		$this->getResponse()->setRedirect(Mage::getUrl('*/*/edit'));
	}

	public function savesubscriptionsAction()
	{
		//get the params
		$data = $this->getRequest()->getParams();
		
		$subscriptions = $data["subscriptions"];
		
		$session = $this->_getSession();
		$customer = $session->getCustomer();
		
        // The switch origin is hardcoded to Magento for the customer
		$customer->setSubscriptions(implode(",",$subscriptions));

		$customer->save();
		
		// return a success message
		$successMessage = array(
			'success' => true
		);
		echo json_encode($successMessage);
	}
	
	public function removecardAction()
	{

		$session = $this->_getSession();
		$customer = $session->getCustomer();
		$stripeCustomerId = $customer->getStripeCustomerId();
		
		try {
			$customer->setStripeCustomerId(null);
			$customer->save();
			//add the message.
			Mage::getSingleton('core/session')->addSuccess("Your credit card has been removed.");
		}
		catch(Exception $ex)
		{		
			Mage::getSingleton('core/session')->addSuccess("There was a problem removing your card. Please contact customer service.");
		}
		
		//redirect
		$this->getResponse()->setRedirect(Mage::getUrl('*/*/edit'));
		
	}
	
	public function savecardAction()
	{
		
		//todo: check for token
		
		//init stripe
		$payment = Mage::getModel("stripe/payment");
		
		//get the params
		$data = $this->getRequest()->getParams();
		
		$session = $this->_getSession();
		$customer = $session->getCustomer();
		$stripeCustomerId = $customer->getStripeCustomerId();
		$token = $data["stripeToken"];
		
		if($stripeCustomerId == null)
		{
			//try to create a new stripe number and card.
			
			//create the customer
			try {
				$payment->createStripeCustomer($token, $customer);
			}
			catch(Exception $ex)
			{
				Mage::getSingleton('core/session')->addError($ex->getMessage());
				//redirect
				$this->getResponse()->setRedirect(Mage::getUrl('*/*/creditcard'));
				return;
			}
		}
		else
		{
			//update the customers card.	
			try {
				$payment->updateStripeCustomer($token, $customer);
			}
			catch(Exception $ex)
			{
				Mage::getSingleton('core/session')->addError($ex->getMessage());
				//redirect
				$this->getResponse()->setRedirect(Mage::getUrl('*/*/creditcard'));
				return;
			}

		}

		//add the message.
		Mage::getSingleton('core/session')->addSuccess("Your credit card has been updated.");
		
		//redirect
		$this->getResponse()->setRedirect(Mage::getUrl('*/*/edit'));
	}
    protected function _loginPostRedirect()
    {
        $session = $this->_getSession();
        $referer = $this->getRequest()->getParam(Mage_Customer_Helper_Data::REFERER_QUERY_PARAM_NAME);
        $refer_url = Mage::helper('core')->urlDecode($referer);

        if (!$session->getBeforeAuthUrl() || $session->getBeforeAuthUrl() == Mage::getBaseUrl()) {
            // Set default URL to redirect customer to
            $session->setBeforeAuthUrl(Mage::helper('customer')->getAccountUrl());
            // Redirect customer to the last page visited after logging in
            if ($session->isLoggedIn()) {
                if (!Mage::getStoreConfigFlag(
                    Mage_Customer_Helper_Data::XML_PATH_CUSTOMER_STARTUP_REDIRECT_TO_DASHBOARD
                )) {
                    if ($referer) {
                        // Rebuild referer URL to handle the case when SID was changed
                        $referer = Mage::getModel('core/url')
                            ->getRebuiltUrl(Mage::helper('core')->urlDecode($referer));
                        if ($this->_isUrlInternal($referer)) {
                            $session->setBeforeAuthUrl($referer);
                        }
                    }
                } else if ($session->getAfterAuthUrl()) {
                    $session->setBeforeAuthUrl($session->getAfterAuthUrl(true));
                }
            } else {
                $session->setBeforeAuthUrl(Mage::helper('customer')->getLoginUrl());
            }
        } else if ($session->getBeforeAuthUrl() == Mage::helper('customer')->getLogoutUrl()) {
            $session->setBeforeAuthUrl(Mage::helper('customer')->getDashboardUrl());
        } else {
            if (!$session->getAfterAuthUrl()) {
                $session->setAfterAuthUrl($session->getBeforeAuthUrl());
            }
            if ($session->isLoggedIn()) {
                $session->setBeforeAuthUrl($session->getAfterAuthUrl(true));
            }
        }
        if(strpos($refer_url, 'checkout')){
            $session->setBeforeAuthUrl(Mage::getUrl('frans/checkout'));
        }
        $this->_redirectUrl($session->getBeforeAuthUrl(true));
    }

}
