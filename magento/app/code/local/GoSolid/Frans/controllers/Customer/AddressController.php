<?php

require_once 'Mage/Customer/controllers/AddressController.php';

class GoSolid_Frans_Customer_AddressController extends Mage_Customer_AddressController
{

	public function indexAction()
	{
		$this->loadLayout();
		$this->_initLayoutMessages('customer/session');
		$this->_initLayoutMessages('catalog/session');

		$block = $this->getLayout()->getBlock('address_book');
		if ($block) {
			$block->setRefererUrl($this->_getRefererUrl());
		}
		$this->renderLayout();

	}

	protected function _getCustomerAddressBlock($addressId = false)
	{
		$address = Mage::getModel('customer/address');

		return $this->getLayout()
			->createBlock('frans/customer_address_edit', '', array($address))
			->setTemplate('customer/address/edit.phtml')
			->setIsTemplateAddress(false);

	}

	protected function _getCustomerMailingAddressBlock($addressId = false)
	{
		$address = Mage::getModel('customer/address');

		return $this->getLayout()
			->createBlock('frans/customer_address_edit', '', array($address))
			->setTemplate('customer/address/edit.phtml')
			->setIsDefaultMailing(1)
			->setTitle('Add Default Mailing Address')
			->setIsTemplateAddress(false);
	}

	protected function getCustomerMailingAddressBlock($addressId = false)
	{
		$address = Mage::getModel('customer/address')->load($addressId);

		$this->_getSession()->setAddressFormData($address->getData());

		return $this->getLayout()
			->createBlock('frans/customer_address_edit', '', array($address))
			->setTemplate('customer/address/edit.phtml')
			->setTitle('Edit Address')
			->setIsDefaultMailing(1)
			->setShowDeleteButton(true)
			->setIsTemplateAddress(false);
	}

	public function getEditCustomerAddressHtml($addressId = false, $invalidFields = array())
	{
		$address = Mage::getModel('customer/address')->load($addressId);
		return $this->getLayout()
			->createBlock('frans/customer_address_edit', '', array($address))
			->setInvalidFields($invalidFields)
			->setTitle('Edit Address')
			->setSubmitButtonLabel('Continue')
			->setTemplate('customer/address/edit.phtml')
			->setCustomFormAction('save')
			->setIsTemplateAddress(false)
			->setFieldNameFormat('%s')
			->setIsFormSubmitted(true)
			->setIsModal(true)
			->setAddressId($address->getEntityId())
			->toHtml();
	}

	protected function getCustomerAddressBlock($addressId = false)
	{
		$address = Mage::getModel('customer/address')->load($addressId);

		$this->_getSession()->setAddressFormData($address->getData());

		return $this->getLayout()
			->createBlock('frans/customer_address_edit', '', array($address))
			->setTemplate('customer/address/edit.phtml')
			->setTitle('Edit Address')
			->setIsTemplateAddress(false);
	}

	protected function getCustomerDobBlock()
	{

		return $this->getLayout()
			->createBlock('frans/customer_address_edit')
			->setTemplate('customer/address/editDob.phtml')
			->setTitle('Edit Email Preferences')
			->setSubmitButtonLabel('Update')
			->setIsTemplateAddress(false);

	}
	/**
	 * Address book form
	 */


	/**
	 * Address book form
	 */



	public function formAction()
	{
		$ajaxRequest = $this->getRequest()->getParam('ajax');
		$addressType = $this->getRequest()->getParam('type');
		//check if this is a ajax request
		if ($ajaxRequest != 1) {
			return $this->_redirect('*/*/');
		}

		$this->loadLayout();
		$this->_initLayoutMessages('customer/session');
		$navigationBlock = $this->getLayout()->getBlock('customer_account_navigation');

		if ($navigationBlock) {
			$navigationBlock->setActive('customer/address');
		}

		$responseAjax = new Varien_Object();

		if($addressType == 'default_mailing'){
			$responseAjax->setHtml(
				$this->_getCustomerMailingAddressBlock(false)->toHtml()
			);
		} else {
			$responseAjax->setHtml(
				$this->_getCustomerAddressBlock(false)->toHtml()
			);
		}

		$responseAjax->setStatus('success');

		$this->getResponse()->setBody($responseAjax->toJson());

	}

	public function editAction()
	{
		$responseAjax = new Varien_Object();

		$this->loadLayout();
		$this->_initLayoutMessages('customer/session');
		$navigationBlock = $this->getLayout()->getBlock('customer_account_navigation');
		$addressType = $this->getRequest()->getParam('type');
		$addressBlock = null;
		if ($navigationBlock) {
			$navigationBlock->setActive('customer/address');
		}

		if($addressId = $this->getRequest()->getParam('id')){

			if($addressType == 'default_mailing'){
				$addressBlock = $this->getCustomerMailingAddressBlock($addressId);
			} else {
				$addressBlock = $this->getCustomerAddressBlock($addressId);
			}

			$responseAjax->setHtml(
				$addressBlock->toHtml()
			);
			$responseAjax->setStatus('success');
		}

		else {
			$responseAjax->setStatus('error');
			$responseAjax->setMsg(array('No address was provided.'));
		}
		$this->getResponse()->setBody($responseAjax->toJson());
	}

	public function editDobAction()
	{
		$responseAjax = new Varien_Object();
		$this->loadLayout();
		$this->_initLayoutMessages('customer/session');

		$addressBlock = $this->getCustomerDobBlock();

		$responseAjax->setHtml(
			$addressBlock->toHtml()
		);
		$responseAjax->setStatus('success');

		$this->getResponse()->setBody($responseAjax->toJson());
	}

	public function editCustomerAddressAction()
	{

		$responseAjax = new Varien_Object();

		if ($id = $this->getRequest()->getParam('id'))
		{
			$addressBlock = $this->_getCustomerAddressBlock($id);

			$responseAjax->setHtml(
				$addressBlock->toHtml()
			);
			$responseAjax->setStatus('success');

		}
		else
		{
			$responseAjax->setStatus('error');
			$responseAjax->setMsg(array('No address was provided.'));
		}

		$this->getResponse()->setBody($responseAjax->toJson());
	}

	public function saveAction()
	{
		$helper = Mage::helper('addressValidation');
		$ajaxRequest = $this->getRequest()->getParam('ajax');
		$response = array();

		if ($ajaxRequest != 1) {
			return $this->_redirect('*/*/');
		}

		$customer = $this->_getSession()->getCustomer();
		/* @var $address Mage_Customer_Model_Address */
		$address  = Mage::getModel('customer/address');

		$addressId = $this->getRequest()->getParam('id');

		if ($addressId) {
			$existsAddress = $customer->getAddressById($addressId);
			if ($existsAddress->getId() && $existsAddress->getCustomerId() == $customer->getId()) {
				// in the original incarnation, Magento just sets the ID
				// however, this causes any non-visible fields to be lost (e.g. the hash)
				// so now we are just using the existing record, when it's there.
				$address = $existsAddress;
			}
		}
		$errors = array();

		/* @var $addressForm Mage_Customer_Model_Form */
		$addressForm = Mage::getModel('customer/form');

		$addressForm->setFormCode('customer_address_edit')
			->setEntity($address);

		$addressData = $addressForm->extractData($this->getRequest());

		if($this->getRequest()->getParam("has-dob") != "1")
		{
			$addressData["dob"] = "";
		}

		$addressErrors  = $addressForm->validateData($addressData);
		if ($addressErrors !== true) {
			$errors = $addressErrors;
		}

		try {
			$addressForm->compactData($addressData);
			$address->setCustomerId($customer->getId())
				->setIsDefaultBilling($this->getRequest()->getParam('default_billing', false));

			$addressErrors = $address->validate();
			if ($addressErrors !== true) {
				$errors = array_merge($errors, $addressErrors);
			}

			$address->save();
			$this->getRequest()->setParam('id', $address->getEntityId());

			//We have to set the new session customer because it was loaded before the address data was changed.
			$this->_getSession()->setCustomer( Mage::getModel('customer/customer')->load($customer->getId() ) );
			$this->_getSession()->setAddressFormData($this->getRequest()->getPost());

		} catch (Mage_Core_Exception $e) {
			$this->_getSession()->setAddressFormData($this->getRequest()->getPost())
				->addException($e, $e->getMessage());
		} catch (Exception $e) {
			$this->_getSession()->setAddressFormData($this->getRequest()->getPost())
				->addException($e, $this->__('Cannot save address.'));
		}


		$isDefaultMailing = $this->getRequest()->getPost('default_mailing');
		$isBilling = $this->getRequest()->getPost('default_billing');

		if($isBilling == false && $isDefaultMailing == false)
		{
			//address is validated and validation info is saved back to the address.
			$validationResult = $helper->validateAddress($address);
			//save the results back to the address for the next pass
			$address->setIsValid($validationResult["is_valid"]);
			//set a var to show proposed. Used by view.
			$address->setSuggestAddress($validationResult["suggest_address"]);

			$address->save();


			if($address->getIsValid() != true)
			{
				//invalid

				$response['msg'][] = $this->__('The address appears to have errors.');
				$response['status'] = 'success';
				$response['invalidFields'] = 'Yes';
				$response['score'] = $validationResult["score"];
				$response['default_billing_modal'] = $this->getEditCustomerAddressHtml($address->getEntityId(), $validationResult["invalid_fields"]);

				$response['blocks'] = array(
					"billing_default" => $this->getLayout()->createBlock('customer/address_book')->setTemplate('customer/address/book/billing-default.phtml')->toHtml(),
					"shipping_address_grid" => $this->getLayout()->createBlock('customer/address_book')->setTemplate('customer/address/book/addresses_grid.phtml')->toHtml());

			}
			else
			{
				//it is valid

				$address->save();

				$response['msg'][] = $this->__('The address has been saved.');
				$response['status'] = 'success';
				$response['score'] = $validationResult["score"];

				$response['blocks'] = array(
					"billing_default" => $this->getLayout()->createBlock('customer/address_book')->setTemplate('customer/address/book/billing-default.phtml')->toHtml(),
					"shipping_address_grid" => $this->getLayout()->createBlock('customer/address_book')->setTemplate('customer/address/book/addresses_grid.phtml')->toHtml());
			}

		}
		else
		{
			$defaultShippingId = null;

			if($isDefaultMailing){
				$address->setIsDefaultShipping(true);
				$defaultShippingId = $address->getEntityId();
			}

			$address->save();

			$mailingAddressBlock = $this->getLayout()->createBlock('customer/address_book')->setTemplate('customer/address/book/catalog-subscribe-mailing-default.phtml')->toHtml();

			if($defaultShippingId != null){
				$mailingAddressBlock = $this->getLayout()->createBlock('customer/address_book')
					->setTemplate('customer/address/book/catalog-subscribe-mailing-default.phtml')
					->setDefaultShippingId($defaultShippingId)
					->toHtml();
			}


			$response['msg'][] = $this->__('The address has been saved.');
			$response['status'] = 'success';


			$response['blocks'] = array(
				"catalog_subscribe_mailing_default" => $mailingAddressBlock,
				"billing_default" => $this->getLayout()->createBlock('customer/address_book')->setTemplate('customer/address/book/billing-default.phtml')->toHtml(),
				"shipping_address_grid" => $this->getLayout()->createBlock('customer/address_book')->setTemplate('customer/address/book/addresses_grid.phtml')->toHtml()
			);
		}

		echo json_encode($response);
	}

	public function saveDobAction() {

		$response = array();

		$customer = $this->_getSession()->getCustomer();

		$isSubscriptionDobUpdate = $this->getRequest()->getPost('subscription_dob');

		if($isSubscriptionDobUpdate !== null) {
			$customer->setDob($this->getRequest()->getPost('dob'));
			$customer->save();
			$response['msg'][] = $this->__('The customer Dob has been saved.');
			$response['status'] = 'success';

			$subscriptions = Mage::getModel('frans/customer_subscriptions')->getSubscriptions();
			$mySubscriptions = explode(",", $customer->getSubscriptions());

			$response['blocks'] = array(
				"subscriptions" => $this->getLayout()->createBlock('customer/newsletter')
					->setTemplate('customer/form/newsletter-email.phtml')
					->setCustomer($customer)
					->setSubscriptions($subscriptions)
					->setCustomerSubscriptions($mySubscriptions)
					->toHtml());
		}
		echo json_encode($response);

	}

	public function addChosenCustomerAddressAction(){

		$responseAjax = new Varien_Object();
		$addressData = $this->getRequest()->getPost();
		$addressId = $addressData['address_id'];
		$addressChoice = $addressData['address_choice'];
		$address = Mage::getModel('customer/address')->load($addressId);



		if($addressChoice === 'suggested') {
			$address->setStreet($addressData['address']['suggested']['street1'] . PHP_EOL . $addressData['address']['suggested']['street2']);
			$address->setPostcode($addressData['address']['suggested']['postcode']);
			$address->setCity($addressData['address']['suggested']['city']);
			$address->setRegionId($addressData['address']['suggested']['region_id']);
			$address->setCountyId($addressData['address']['suggested']['country_id']);
		} else {
			$address->setStreet($addressData['address']['original']['street']);
			$address->setPostcode($addressData['address']['original']['postcode']);
			$address->setCity($addressData['address']['original']['city']);
			$address->setRegionId($addressData['address']['original']['region_id']);
			$address->setCountyId($addressData['address']['original']['country_id']);
		}

		$address->resetAddressValidation();

		$address->save();

		//setting response info, edit URL, and HTML to be reloaded into the page

		if(Mage::getSingleton('customer/session')->getCustomer()->getDefaultBillingAddress()){
			$customerDefaultBilling = Mage::getSingleton('customer/session')->getCustomer()->getDefaultBillingAddress()->getData()['entity_id'];
			if($customerDefaultBilling == $addressId){
				$addressType = 'billing';
			} else {
				$addressType = 'shipping';
			}
		} else {
			$addressType = 'shipping';
		}



		//We have to set the new session customer because it was loaded before the address data was changed.
		$customer = $this->_getSession()->getCustomer();
		$this->_getSession()->setCustomer( Mage::getModel('customer/customer')->load($customer->getId() ) );

		$responseAjax->setData('editUrl', Mage::getUrl('*/*/editCustomerAddress',
			array(
				"id" => $address->getId(),
				"ajax" => 1,
				"type" => $addressType
			)
		));

		$responseAjax->setData('billingDefault', $this->getLayout()->createBlock('customer/address_book')->setTemplate('customer/address/book/billing-default.phtml')->toHtml());
		$responseAjax->setData('shippingAddressGrid', $this->getLayout()->createBlock('customer/address_book')->setTemplate('customer/address/book/addresses_grid.phtml')->toHtml());

		$this->getResponse()->setBody($responseAjax->toJson());

	}

	public function deleteAction()
	{
		$ajaxRequest = $this->getRequest()->getParam('ajax');
		$addressId = $this->getRequest()->getParam('id', false);
		$type =  $this->getRequest()->getParam('type');
		$response = array();

		if ($ajaxRequest != 1 || trim($addressId) == "") {
			return $this->_redirect('*/*/');
		}

		if ($addressId) {
			$address = Mage::getModel('customer/address')->load($addressId);

			// Validate address_id <=> customer_id
			if ($address->getCustomerId() != $this->_getSession()->getCustomerId())
			{
				$response['error'] = $this->__('The address does not belong to this customer.');
			}
			else
			{
				try {
					$address->delete();
					$response['msg'] = $this->__('The address has been deleted.');
					$response['status'] = 'success';

					if ($type == "shipping"){
						$response['blocks'] = array(
							"shipping_default" => $this->getLayout()->createBlock('customer/address_book')->setTemplate('customer/address/book/shipping-default.phtml')->toHtml(),
							"shipping_address_grid" => $this->getLayout()->createBlock('customer/address_book')->setTemplate('customer/address/book/addresses_grid.phtml')->toHtml(),
						);
					}
					else{
						$response['blocks'] = array(
							"catalog_subscribe_mailing_default" => $this->getLayout()->createBlock('customer/address_book')->setTemplate('customer/address/book/catalog-subscribe-mailing-default.phtml')->toHtml(),
							"billing_default" => $this->getLayout()->createBlock('customer/address_book')->setTemplate('customer/address/book/billing-default.phtml')->toHtml()
						);
					}

				} catch (Exception $e){
					$response['error'] = $this->__('An error occurred while deleting the address.');
				}
			}
		}

		echo json_encode($response);
	}

	public function billingAction(){
		$this->loadLayout();
		$this->_initLayoutMessages('customer/session');
		$this->_initLayoutMessages('catalog/session');

		$this->renderLayout();
	}
	public function creditcardAction(){
		$this->loadLayout();
		$this->_initLayoutMessages('customer/session');
		$this->_initLayoutMessages('catalog/session');

		$this->renderLayout();
	}

}