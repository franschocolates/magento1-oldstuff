<?php
class GoSolid_Frans_CheckoutController extends Mage_Core_Controller_Front_Action
{
    private $_checkoutBlock = null;

    protected function _initAction() {
        // include system messages
        $this
            ->loadLayout()
            ->_initLayoutMessages('checkout/session')
            ->_initLayoutMessages('catalog/session');
        return $this;
    }

    protected function _ajaxRedirectResponse()
    {
        $this->getResponse()
            ->setHeader('HTTP/1.1', '403 Session Expired')
            ->setHeader('Login-Required', 'true')
            ->sendResponse();
        return $this;
    }

    /**
     * Validate ajax request and redirect on failure
     *
     * @return bool
     */
    protected function _expireCheckout($ajax = true)
    {
        if (!$this->getStandard()->getQuote()->hasItems()
            || $this->getStandard()->getQuote()->getHasError()
            || $this->getStandard()->getQuote()->getIsMultiShipping()) {

            // if no items, or has errors, or is  multiship..
            // revert back to default single order state
            $this->getStandard()->getQuote()->setIsMultiShipping(false);

            // save the quote
            $this->getStandard()->getQuote()->save();

            // if this request is not ajax...
            if (!$ajax){

                // redirect back to the cart
                $this->_redirect('checkout/cart');
            } else {

                // session expired redirect
                $this->_ajaxRedirectResponse();
            }
            return true;
        }

        return false;
    }

    public function indexAction()
    {
        $quote = $this->getStandard()->getQuote();
        if (!$quote->hasItems() || $quote->getHasError()) {
            $this->_redirect('checkout/cart');
            return;
        }
        if (!$quote->validateMinimumAmount()) {
            $error = Mage::getStoreConfig('sales/minimum_order/error_message') ?
                Mage::getStoreConfig('sales/minimum_order/error_message') :
                Mage::helper('checkout')->__('Subtotal must exceed minimum order amount');

            Mage::getSingleton('checkout/session')->addError($error);
            $this->_redirect('checkout/cart');
            return;
        }
        Mage::getSingleton('checkout/session')->setCartWasUpdated(false);
        // a little bit of trickery to have it redirect back properly
        // if we use before/After AuthUrl it ends up persisting in a way that means we
        // don't get the results we'd like
        // if we plug this URL in as the referer, the login URL will contain it
        // and redirect back correctly
        $this->getRequest()->setParam(Mage_Customer_Helper_Data::REFERER_QUERY_PARAM_NAME,
            Mage::helper('core')->urlEncode(Mage::getUrl('*/*', array('_secure'=>true))));
        $this->getStandard()->initCheckout();
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->renderLayout();
    }

    /**
     * Shipping address save action
     */
    public function saveShippingAction()
    {

        if ($this->_expireCheckout()) {
            return;
        }

        $helper = Mage::helper('addressValidation');
        $responseAjax = new Varien_Object();
        if ($this->getRequest()->isPost())
        {
            $data = $this->getRequest()->getPost('shipping', array());
            $customerAddressId = $this->getRequest()->getPost('customer_address_id', false);
            $result = $this->getStandard()->saveShipping($data, $customerAddressId);
            $address = $result["quote_address"];

            //address is validated and validation info is saved back to the address.
            $validationResult = $helper->validateAddress($address);
            //save the results back to the address for the next pass
            $address->setIsValid($validationResult["is_valid"]);
            //set a var to show proposed. Used by view.
            $address->setSuggestAddress($validationResult["suggest_address"]);

            //if we chose to not use the suggested address, we should cary on.
            if($this->getRequest()->getPost("use_suggested") == 'false')
            {
                $address->setIsValid(true);
            }


            //set the customer address id. This will carry over existing addresses
            if(strlen($customerAddressId) == 0)
            {
                $address->setCustomerAddressId(null);
            }
            else
            {
                $address->setCustomerAddressId($customerAddressId);
            }

            $address->save();


            if($address->getIsValid() != true)
            {

                //setting response info and HTML to be reloaded into the page
                $checkoutBlock = $this->_getCheckoutBlock()->setIsFormSubmitted(true);
                $checkoutBlock->setAddressId($address->getId());


                $responseAjax->setStatus('success');
                $responseAjax->setData('invalidFields', "Yes");
                $responseAjax->setData('score', $validationResult["score"]);
                //$responseAjax->setData('shippingHtml', $checkoutBlock->getEditAddressHtml($address->getId(), $this->getRequest()->getParam('delivery', false), $validationResult["invalid_fields"]));

                $responseAjax->setData('shippingHtml', $checkoutBlock->getShippingHtml($validationResult["invalid_fields"]));



            }
            else {
                // it is valid move on.
                $customerAddress = $address->convertToCustomerAddress();


                $address->resetAddressValidation();
                $address->setIsValid(true);
                $address->save();


                //set the response.
                $checkoutBlock = $this->_getCheckoutBlock()->setIsFormSubmitted(true);
                $responseAjax->setStatus('success');
                $responseAjax->setData('score', $validationResult["score"]);

                //we want to update the side bar

                $responseAjax->setData('editUrl', Mage::getUrl('*/*/editShippingAddress', array("id" => $address->getId())));
                $responseAjax->setData('deliveryHtml', $checkoutBlock->getDeliveryHtml());
                $responseAjax->setData('summaryHtml', $checkoutBlock->getSummaryHtml(true));

            }

        }

        else
        {
            $responseAjax->setStatus('failure');
            $responseAjax->setMessage('No shipping data was provided.');
        }

        $this->getResponse()->setBody($responseAjax->toJson());

    }

    public function multishipAction() {

        $this->_initAction();
        $this->renderLayout();
    }

    public function addMultishipAddressAction()
    {
        $quote = $this->_getQuote();
        $multishipCheckout = $this->_getMultishipCheckout($quote);

        $addressId = $this->getRequest()->getParam('customer_address_id');
        $shippingData = $this->getRequest()->getParam('shipping_address');

        $address = $multishipCheckout->addShippingAddress($shippingData, $addressId);

        if ($address instanceof Mage_Sales_Model_Quote_Address)
        {
            $quote->addAddress($address);
            $address->save();
            $quote->save();

            return $this->_buildReturnJson($quote);
        }
        else
        {
            # TODO: return error
            return $address;
        }
    }

    public function removeMultishipAddressAction()
    {
        $quote = $this->_getQuote();
        $multishipCheckout = $this->_getMultishipCheckout($quote);

        $addressId = $this->getRequest()->getParam('address_id');

        if ($multishipCheckout->removeShippingAddress($addressId))
        {
            $quote->save();
        }

        return $this->_buildReturnJson($quote);
    }

    public function saveShippingInfoAction()
    {
        $quote = $this->_getQuote();
        $multishipCheckout = $this->_getMultishipCheckout($quote);

        $shippingInfo = $this->getRequest()->getParam('shipping_info');

        $multishipCheckout->setShippingInfo($shippingInfo);
        $quote->save();

        return $this->_buildReturnJson($quote);
    }

    /**
     *
     * Validates the delivery info and proceeds to next step if valid
     */
    public function saveDeliveryAction()
    {
        if ($this->_expireCheckout()) {
            return;
        }
        $responseAjax = new Varien_Object();

        // do validation logic
        $deliveryData = $this->getRequest()->getParam('delivery');
        Mage::log("Delivery data: " . print_r($deliveryData, true));

        $result = $this->getStandard()->setDeliveryInfo($deliveryData);
        if (!is_array($result))
        {
            // build the response delivery output
            $checkoutBlock = $this->_getCheckoutBlock();
            $responseAjax->setData('paymentHtml', $checkoutBlock->getPaymentStepHtml());
            $responseAjax->setData('summaryHtml', $checkoutBlock->getSummaryHtml(true, true));
            $responseAjax->setStatus('success');
        }
        else
        {
            $responseAjax->setStatus('failure');
            $responseAjax->setMessage('Delivery info was not complete or was invalid.');
        }

        $this->getResponse()->setBody($responseAjax->toJson());
    }

    public function applyGiftcardAction()
    {
        // DH add custom givex log
        Mage::Helper('givex')->logError("Starting applyGiftcardAction function.", Zend_Log::INFO);

        if ($this->_expireCheckout()) {
            // DH add custom givex log
            Mage::Helper('givex')->logError("Returning gift card has expired.", Zend_Log::INFO);

            return;
        }

        $responseAjax = new Varien_Object();
        $cardNumber = $this->getRequest()->getParam('card_number');

        if ($cardNumber)
        {
            // DH switch from Mage log to custom givex log
            Mage::Helper('givex')->logError("About to try applying '$cardNumber'.", Zend_Log::INFO);

            try
            {
                $quote = $this->_getQuote();
                $givex = Mage::getModel('givex/giftcard');

                $result = $givex->applyToQuote($cardNumber, $quote);

                // DH switch from Mage log to custom givex log
                Mage::Helper('givex')->logError("Result was $result.", Zend_Log::INFO);

                if ($result > 0)
                {
                    // need to output new summary HTML and also new giftcard html
                    $paymentBlock = $this->_getPaymentStepBlock();
                    $responseAjax->setStatus('success');
                    $responseAjax->setData('summaryHtml', $this->_getSummaryHtml(true, true));
                    $responseAjax->setData('giftcardHtml', $paymentBlock->getInnerPaymentBlock()->getGiftcardHtml());
                    $coversBalance = !($quote->getGrandTotal() > 0);
                    $responseAjax->setData('coversBalance', $coversBalance);

                    // DH add custom givex log
                    Mage::Helper('givex')->logError("Gift card '$cardNumber' successfully applied to quote.", Zend_Log::INFO);
                }
                else
                {
                    // TODO: implement better error handling
                    // DH add custom givex log
                    Mage::Helper('givex')->logError("Couldn't apply gift card number '$cardNumber' to quote.", Zend_Log::ERR, true);

                    $responseAjax->setStatus('failure');
                    $responseAjax->setMessage($this->_getErrorMessageForGiftcardError($result, $cardNumber));
                }
            }
            catch (Exception $e)
            {
                // DH add custom givex log
                Mage::Helper('givex')->logError("An error occurred saving the gift card $cardNumber data to quote. Sorry.", Zend_Log::ERR, true);

                Mage::logException($e);

                $responseAjax->setStatus('failure');
                $responseAjax->setMessage('An error occurred saving the gift card data. Sorry.');
            }

        }
        else
        {
            // DH add custom givex log
            Mage::Helper('givex')->logError("Failed in getting a gift card number due to a billing and payment info issue OR an incorrect giftcard number was removed from the field.", Zend_Log::ERR, true);

            $responseAjax->setStatus('success');
//            $responseAjax->setMessage('Please specify billing and payment info.');

        }

        $this->getResponse()->setBody($responseAjax->toJson());
    }

    public function removeGiftcardAction()
    {
        // DH add custom givex log
        Mage::Helper('givex')->logError("Starting removeGiftcardAction function.", Zend_Log::INFO);

        if ($this->_expireCheckout()) {
            // DH add custom givex log
            Mage::Helper('givex')->logError("Ajax expired in removeGiftcardAction function.", Zend_Log::INFO);

            return;
        }

        $responseAjax = new Varien_Object();
        $quote = $this->_getQuote();

        if ($cardNumber = $quote->getGiftcardNumber())
        {
            try
            {
                // DH add custom givex log
                Mage::Helper('givex')->logError("Starting to remove gift card number '$cardNumber' from quote.", Zend_Log::INFO);

                $givex = Mage::getModel('givex/giftcard');
                $givex->removeFromQuote($quote);
                $quote->setGiftcardRemoved(true);
                $quote->setGiftcardRemovedNumber($cardNumber);

                // need to output new summary HTML and also new giftcard html
                $paymentBlock = $this->_getPaymentStepBlock();
                $responseAjax->setStatus('success');
                $responseAjax->setData('summaryHtml', $this->_getSummaryHtml(true, true));
                $responseAjax->setData('giftcardHtml', $paymentBlock->getInnerPaymentBlock()->getGiftcardHtml());
            }
            catch (Exception $e)
            {
                // DH add custom givex log
                Mage::Helper('givex')->logError("Failed in removing gift card number '$cardNumber' from quote.", Zend_Log::ERR, true);

                Mage::logException($e);

                $responseAjax->setStatus('failure');
                $responseAjax->setMessage('An error occurred saving the gift card data. Sorry.');
            }

            // DH add custom givex log
            Mage::Helper('givex')->logError("Gift card number $cardNumber removed from quote.", Zend_Log::INFO);
        }

        $this->getResponse()->setBody($responseAjax->toJson());
    }

    public function saveOrderAction()
    {
        if ($this->_expireCheckout()) {
            return;
        }

        $responseAjax = new Varien_Object();

        $billingData = $this->getRequest()->getParam('billing');
        $paymentData = $this->getRequest()->getParam('payment');

        if ($billingData && $paymentData)
        {
            try
            {
                $checkout = $this->getStandard();

                $result = $checkout->saveOrder($billingData, $paymentData);
                if ($result && !is_array($result))
                {
                    // if we don't save the quote, it doesn't become inactive
                    $this->getStandard()->getQuote()->save();

                    // succeeded, pretend we worked
                    $responseAjax->setStatus('success');
                }

            }
            catch (Exception $e)
            {
                Mage::logException($e);

                $responseAjax->setStatus('failure');
                $responseAjax->setMessage($e->getMessage());
            }
        }
        else
        {
            $responseAjax->setStatus('failure');
            $responseAjax->setMessage('Please specify billing and payment info.');
        }

        $this->getResponse()->setBody($responseAjax->toJson());
    }

    protected function _getSummaryHtml($showShipping = false, $showDelivery = false)
    {
        return $this->_getCheckoutBlock()->getSummaryHtml($showShipping, $showDelivery);
    }

    /*
     * @return Mage_Core_Block_Template
     */
    protected function _getPaymentStepBlock()
    {
        // DH add custom givex log
        Mage::Helper('givex')->logError("Starting _getPaymentStepBlock function.", Zend_Log::INFO);

        $quote = $this->_getQuote();
        // also get the giftcard balance if the quote has a giftcard number
        if ($cardNumber = $quote->getGiftcardNumber())
        {
            // DH add custom givex log
            Mage::Helper('givex')->logError("Getting balance for gift card number '$cardNumber' and setting it to quote.", Zend_Log::INFO);

            $balance = Mage::getModel('givex/giftcard')->getBalance($cardNumber);
            $quote->setGiftcardBalance($balance);
        }

        return $this->getLayout()
            ->createBlock($this->_getPaymentStepBlockType())
            ->setQuote($quote);
    }

    protected function _buildReturnJson($quote)
    {
        $jsonData = Mage::helper('core')->jsonEncode($this->_buildReturnArray($quote));

        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody($jsonData);

        return;
    }

    /**
     * Order success action
     */
    public function successAction()
    {
        $session = Mage::getSingleton('checkout/session');
        if (!$session->getLastSuccessQuoteId()) {
            $this->_redirect('checkout/cart');
            return;
        }

        $lastQuoteId = $session->getLastQuoteId();
        $lastOrderId = $session->getLastOrderId();
        if (!$lastQuoteId || !$lastOrderId) {
            $this->_redirect('checkout/cart');
            return;
        }

        Mage::helper('addressValidation')->resetErrorCount();

        $session->unsetData('use_guest_checkout');
        $session->clear();

        // need to keep the last order ID in case they want to view the receipt
        Mage::getSingleton('customer/session')->setLastOrderId($lastOrderId);
        $this->loadLayout();
        $this->_initLayoutMessages('checkout/session');
        $this->renderLayout();
    }

    /**
     * Creates an account for the specified order (if not already created)
     * Note that some logic is similar to some stuff in Customer/AccountController
     * But since this is all ajaxy and we create a bunch of addresses and infer a bunch of other info
     * it's not really combinable with that logic.
     *
     * @return Mage_Core_Controller_Varien_Action
     */
    public function createAccountPostAction()
    {
        $responseAjax = new Varien_Object();

        if ($this->_validateFormKey() && $this->getRequest()->isPost())
        {
            $params = $this->getRequest()->getParams();
            if ($orderId = $params['order_id'])
            {
                $order = Mage::getModel('sales/order')->load($orderId);
                if ($order->getId() && !$order->getCustomerId())
                {
                    // go ahead and convert
                    $result = $this->_convertOrderToCustomer($order);
                }
                else
                {
                    // this could mean they somehow re-requested the order
                    // or some other shenanigans
                    // we'll assume they already requested, in which case it already happened
                    // so we can just say success again
                }

                // it'll be a bool true if it worked
                // and an array of errors otherwise.
                if ($result && !is_array($result))
                {
                    $successBlock = $this->getLayout()
                        ->createBlock('core/template')
                        ->setTemplate('checkout/account_success.phtml');

                    $responseAjax->setHtml($successBlock->toHtml());
                    $responseAjax->setStatus('success');
                }
                else
                {
                    $responseAjax->setStatus('fail');

                    if (is_array($result))
                    {
                        $responseAjax->setMessage($result[0]);
                    }
                    else
                    {
                        $responseAjax->setMessage($this->__('An unknown error occurred creating your account.'));
                    }
                }
            }
            else
            {
                $responseAjax->setStatus('fail');
                $responseAjax->setMessage($this->__('No order specified.'));
            }
        }
        else
        {
            $responseAjax->setStatus('fail');
            $responseAjax->setMessage($this->__('Invalid request'));
        }

        $this->getResponse()->setBody($responseAjax->toJson());
    }


    public function optInEmailNewsletterAction()
    {
        $responseAjax = new Varien_Object();
        //get the params
        $data = $this->getRequest()->getParams();
        $email = $data["email"];

        $customer = Mage::getSingleton('customer/session')->getCustomer();

        if($email){
            $customer->setIsSubscribed(1);
            $customer->setSubscriptions(1); //this sets the custom subscriptions table.
        }

        $successBlock = $this->getLayout()
            ->createBlock('core/template')
            ->setTemplate('checkout/opt_in_success.phtml');

        $responseAjax->setHtml($successBlock->toHtml());
        $responseAjax->setStatus('success');

        $customer->save();

        $this->getResponse()->setBody($responseAjax->toJson());

    }

    /**
     * Action to store they want to use guest checkout. No response necessary
     */
    public function useGuestCheckoutAction()
    {
        Mage::getSingleton('checkout/session')->setUseGuestCheckout(true);

        // no, seriously, no response, just die, that will give a 200.
        die;
    }

    public function expiredAction()
    {
        $this
            ->_initAction()
            ->renderLayout();
    }

    protected function _buildReturnArray($quote)
    {
        return array(

            'data' => array(
                'items' => $this->_buildItemData($quote),
                'addresses' => $this->_buildAddressData($quote),
            )
        );
    }

    protected function _buildItemData($quote)
    {
        $itemData = array();

        foreach ($quote->getAllItems() as $quoteItem)
        {
            $itemImages = $quoteItem->getProduct()->getMediaGalleryImages();
            $itemData[$quoteItem->getId()] = array(
                'product_id' => $quoteItem->getProductId(),
                'productName' => $quoteItem->getName(),
                'sku' => $quoteItem->getSku(),
                'productImage' => $itemImages[0]['url'],
                'price' => $quoteItem->getPrice(),
                'qty' => $quoteItem->getQty(),
                'row_total' => $quoteItem->getRowTotal(),
            );
        }

        return $itemData;
    }

    protected function _buildAddressData($quote)
    {
        $addressData = array();

        foreach ($quote->getAllShippingAddresses() as $shippingAddress)
        {
            $streetLines = $shippingAddress->getStreetFull();
            $addressData[$shippingAddress->getId()] = array(
                'customer_address_id' => $shippingAddress->getCustomerAddressId(),
                'firstname' => $shippingAddress->getFirstname(),
                'lastname' => $shippingAddress->getLastname(),
                'middlename' => $shippingAddress->getMiddlename(),
                'prefix' => $shippingAddress->getPrefix(),
                'suffix' => $shippingAddress->getSuffix(),
                'company' => $shippingAddress->getCompany(),
                'street1' => $streetLines[0],
                'street2' => isset($streetLines[1]) ? $streetLines[1] : '',
                'city' => $shippingAddress->getCity(),
                'postcode' => $shippingAddress->getPostcode(),
                'region_id' => $shippingAddress->getRegionId(),
                'country_id' => $shippingAddress->getCountryId(),
                'telephone' => $shippingAddress->getTelephone(),
                'fax' => $shippingAddress->getFax()
            );
        }

        return $addressData;
    }

    protected function _getPaymentStepBlockType()
    {
        return 'frans/checkout_standard_payment';
    }

    /**
     *
     * Gets the checkout type for multiship
     * @param Mage_Sales_Model_Quote $quote
     * @return GoSolid_Frans_Model_Checkout_Type_Multiship
     */
    protected function _getMultishipCheckout($quote)
    {
        $multishipCheckout = Mage::getModel('frans/checkout_type_multiship');
        $multishipCheckout->init($quote);

        return $multishipCheckout;
    }

    /**
     *
     *
     * @return Mage_Sales_Model_Quote
     */
    protected function _getQuote()
    {
        return Mage::getModel('checkout/cart')->getQuote();
    }

    /**
     * Retrieve shopping cart model object
     *
     * @return Mage_Checkout_Model_Cart
     */
    protected function _getCart()
    {
        return Mage::getSingleton('checkout/cart');
    }

    /**
     * @param array $params
     * @param GoSolid_Frans_Model_Sales_Order $order
     */
    protected function _convertOrderToCustomer($order)
    {
        $session = Mage::getSingleton('customer/session');

        $errors = array();

        if (!$customer = Mage::registry('current_customer')) {
            $customer = Mage::getModel('customer/customer')->setId(null);
        }

        /* @var $customerForm Mage_Customer_Model_Form */
        $customerForm = Mage::getModel('customer/form');
        $customerForm->setFormCode('customer_account_create')
            ->setEntity($customer);

        $customerData = $customerForm->extractData($this->getRequest());

        if ($this->getRequest()->getParam('is_subscribed', false)) {
            $customer->setIsSubscribed(1);
            // this is deprecated, we should be using $customer->subscribeEmail()
            $customer->setSubscriptions(1); //this sets the custom subscriptions table.
        }

        // you'll see a lot of the same code here as you'd see in customer/account
        // we've just adjusted the address stuff.
        /**
         * Initialize customer group id
         */
        $customer->getGroupId();

        try {
            $customerErrors = $customerForm->validateData($customerData);
            if ($customerErrors !== true) {
                $errors = array_merge($customerErrors, $errors);
            } else {
                $customerForm->compactData($customerData);
                $businessAccount = '';
                if($this->getRequest()->getPost('business-account') == 'on')
                {
                    $businessAccount = $this->getRequest()->getPost('business-account');
                    $customer->setCompanyName($this->getRequest()->getPost('company_name'));
                    $defaultBusinessGroupId = Mage::getStoreConfig('frans/business_customers/default_customer_group_id');
                    if ($defaultBusinessGroupId)
                    {
                        $customer->setGroupId($defaultBusinessGroupId);
                    }
                }
                $customer->setPassword($this->getRequest()->getPost('password'));
                $customer->setConfirmation($this->getRequest()->getPost('confirmation'));
                $customerErrors = $customer->validate();
                if (is_array($customerErrors)) {
                    $errors = array_merge($customerErrors, $errors);
                }
            }

            $validationResult = count($errors) == 0;

            if (true === $validationResult) {
                // clear out the confirmation field so that we don't store PW
                // and it doesn't think we need to confirm.
                $customer->setConfirmation(null);

                // go through and add the customer to everything
                // and convert all of our addresses
                $order->setCustomer($customer);
                $order->setCustomerIsGuest(false);
                $order->setCustomerGroupId($customer->getGroupId());

                // only need customer addresses in event that saving customer doesn't cascade
                $customerAddresses = array();
                $childOrders = array();
                $orderAddresses = array();

                if ($order->getIsMultishipParent())
                {
                    $childOrders = $order->getChildOrders();
                    foreach ($childOrders as $childOrder)
                    {
                        $childOrder->setCustomer($customer);
                        $childOrder->setCustomerIsGuest(false);
                        $childOrder->setCustomerGroupId($customer->getGroupId());
                    }
                }

                /* @var GoSolid_Frans_Model_Sales_Order_Address $orderBillingAddress */
                $orderBillingAddress = $order->getBillingAddress();

                // need to add the name fields to the customer
                foreach (array('firstname', 'lastname', 'middlename', 'prefix', 'suffix') as $attributeName)
                {
                    $customer->setData($attributeName, $orderBillingAddress->getData($attributeName));
                }

                if ($customerAddress = $orderBillingAddress->convertToCustomerAddress($customer, true))
                {
                    $customerAddresses[] = $customerAddress;
                    $orderAddresses[] = $orderBillingAddress;
                }

                /* @var GoSolid_Frans_Model_Sales_Order_Address $shippingAddress */
                foreach ($order->getAllShippingAddresses() as $shippingAddress)
                {
                    if ($customerAddress = $shippingAddress->convertToCustomerAddress($customer, false))
                    {
                        $customerAddresses[] = $customerAddress;
                        $orderAddresses[] = $shippingAddress;
                    }
                }

                // we save everything together so that we don't have a weird state where the order is partially converted
                $transaction = Mage::getModel('core/resource_transaction');

                $transaction->addObject($customer);
                $transaction->addObject($order);

                // this could just be empty, which is fine.
                foreach ($childOrders as $childOrder)
                {
                    $transaction->addObject($childOrder);
                }

                // add all order & quote addresses to the transaction
                // to set their relationship to the new customer & addresses
                foreach ($orderAddresses as $address)
                {
                    $transaction->addObject($address);
                }

                // now save the transaction to start everything off
                $transaction->save();

                // can't raise an event for registration (as original code does)
                // due to the fact that we don't have an account controller
//                Mage::dispatchEvent('customer_register_success',
//                    array('account_controller' => $this, 'customer' => $customer)
//                );

                // also need to send registered email.
                $customer->sendNewAccountEmail('registered', '',Mage::app()->getStore()->getId(), $businessAccount);
            } else {
                if (!is_array($errors)) {
                    $errors = array( $this->__('Invalid customer data'));
                }
            }
        } catch (Mage_Core_Exception $e) {
            if ($e->getCode() === Mage_Customer_Model_Customer::EXCEPTION_EMAIL_EXISTS) {
                $message = $this->__('There is already an account with this email address.');
            } else {
                $message = $e->getMessage();
            }
            $errors[] = $message;
        } catch (Exception $e) {
            $errors[] = $this->__('Cannot save the customer.');
        }

        return (count($errors) == 0) ? true : $errors;
    }

    /**
     * Get standard checkout model
     *
     * @return GoSolid_Frans_Model_Checkout_Type_Standard
     */
    public function getStandard()
    {
        return Mage::getSingleton('frans/checkout_type_standard');
    }

    /**
     *
     * Builds a checkout block from which we can get other blocks
     * @return GoSolid_Frans_Block_Checkout_Standard
     */
    protected function _getCheckoutBlock()
    {
        if ($this->_checkoutBlock != null)
        {
            return $this->_checkoutBlock;
        }

        return ($this->_checkoutBlock = $this->getLayout()
            ->createBlock('frans/checkout_standard')
            ->setQuote($this->_getQuote()));
    }

    protected function _getErrorMessageForGiftcardError($errorCode, $cardNumber)
    {
        switch ($errorCode)
        {
            case GoSolid_Givex_Model_Giftcard::RETURN_INVALID_CARD:
                return $this->__('Gift card %s is invalid. Please call %s for assistance.',
                    substr($cardNumber, -4), Mage::getStoreConfig('general/store_information/tollfree_number')
                );
                break;

            case 0: // zero balance may just be 0
            case GoSolid_Givex_Model_Giftcard::RETURN_NO_BALANCE:
                return $this->__('Gift card %s has a zero balance and cannot be used.',
                    substr($cardNumber, -4));
                break;

            case GoSolid_Givex_Model_Giftcard::RETURN_UNKNOWN_ERROR:
            default:
                return $this->__('We are unable to process your gift card at this time. Please call %s for further assistance.',
                    Mage::getStoreConfig('general/store_information/tollfree_number') );

        }
    }
}