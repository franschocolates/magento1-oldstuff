<?php

/**
 * Configuration controller

 */
class GoSolid_Frans_Adminhtml_System_ConfigController extends Mage_Adminhtml_System_ConfigController
{
    /**
     * Export nutrition info in csv format
     *
     */
    public function exportNutritionAction()
    {
        $fileName   = 'nutrition.csv';
        /** @var $gridBlock GoSolid_Frans_Block_Adminhtml_Nutrition_Grid */

        $gridBlock  = $this->getLayout()->createBlock('frans/adminhtml_nutrition_grid');

        $content    = $gridBlock->getCsvFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

}
