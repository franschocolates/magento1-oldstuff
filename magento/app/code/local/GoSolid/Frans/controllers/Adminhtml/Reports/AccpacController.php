<?php

class GoSolid_Frans_Adminhtml_Reports_AccpacController extends Mage_Adminhtml_Controller_Action{


	public function indexAction(){
        $this->loadLayout();
        $this->renderLayout();
	}

    public function generateAction()
    {
        if ($reportDate = $this->getRequest()->getParam('date'))
		{
			try
			{
		    	$model = Mage::getModel('frans/reports_accPac_reconciliationManager');
		    	
		    	$model->generateReport($reportDate);

                $formattedDate = date('m/d/Y', strtotime($reportDate));
				Mage::getSingleton('core/session')->addSuccess(
                    "The report for $formattedDate was successfully generated and is available on S3.");
			}
			catch (Exception $exc)
			{
				Mage::logException($exc);
				
				Mage::getSingleton('core/session')->addError("An error occurred generating the report: {$exc->getMessage()}");
			}
	    }
	    
	    $this->_redirect('*/*/index');
    }
}