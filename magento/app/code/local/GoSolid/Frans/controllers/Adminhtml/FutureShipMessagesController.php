<?php

class GoSolid_Frans_Adminhtml_FutureShipMessagesController extends Mage_Adminhtml_Controller_action
{
	const IMAGE_FOLDER = 'fs_images';
	
	protected function _initAction() {
        $this->loadLayout()
            ->_setActiveMenu('system');
        return $this;
	}   
 
	public function indexAction() {

		$this->_initAction()
			->renderLayout();

	}

	public function editAction() {

		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('frans/futureShipMessage')->load($id);
		
		Mage::register('futureShipMessages_data', $model);
		
		if ($model->getId() || $id == 0) 
		{
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

			$this->loadLayout();
			
			//Optional for WYSIWYG must change loading blocks to XML
			//$this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
			//$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
			
			$this->_addContent($this->getLayout()->createBlock('frans/adminhtml_futureShipMessages_edit'))
				->_addLeft($this->getLayout()->createBlock('frans/adminhtml_futureShipMessages_edit_tabs'));
				
			$this->renderLayout();

		}
		else
		{
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('frans')->__('Record does not exist'));
			$this->_redirect('*/*/');
		}

	}
	
	
	public function newAction() {
		$this->_forward('edit');

	}
	
	public function saveAction() {
		$session = Mage::getSingleton('adminhtml/session');
		if ($data = $this->getRequest()->getPost()) 
		{
			$folder = self::IMAGE_FOLDER;
			$filename = "image";
			if(isset($_FILES[$filename]['name']) and (file_exists($_FILES[$filename]['tmp_name']))) 
			{
				//save the message images
				$path = Mage::getBaseDir('media') . DS . $folder . DS ;
				$uploader = new Varien_File_Uploader($filename);
			    $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png')); 
			    $uploader->setAllowRenameFiles(false);
			    $uploader->setFilesDispersion(false);   
			    
			    $saveArr = $uploader->save($path, $_FILES[$filename]['name']);			   
			    $data[$filename] = $folder . DS .  $saveArr['file'];
				$data[$filename] = str_replace('\\', '/', $data[$filename]);
			}
			elseif (isset($data[$filename]["delete"]) & $data[$filename]["delete"] == "1")
			{
				$path = Mage::getBaseDir('media') . DS ;
				$path = $path . $data[$filename]["value"] ; 
				unlink($path);  
				$data[$filename] = null;
			}
			else
			{
				unset($data[$filename]);
			}
			
			$model = Mage::getModel('frans/futureShipMessage');
			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));

			try {


				$model->save();

				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('frans')->__('Record was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
				
			}
			catch(Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
			}
			
		}
		
		//on save and edit
		 if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('page_id' => $model->getId(), '_current'=>true));
                    return;
                }
		
		
		//no agency to save.
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('frans')->__('Record not found.'));
        $this->_redirect('*/*/');
		
	}

	
}