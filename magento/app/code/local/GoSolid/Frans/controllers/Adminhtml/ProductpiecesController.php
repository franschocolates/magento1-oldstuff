<?php

class GoSolid_Frans_Adminhtml_ProductpiecesController extends Mage_Adminhtml_Controller_action
{
	protected function _initAction() {
		$this->loadLayout();
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction()
			->renderLayout();
	}

	public function editAction() {

		$id     = $this->getRequest()->getParam('piece_id');
		$model  = Mage::getModel('frans/productPiece')->load($id);
		
		Mage::register('index_data', $model);
		
		if ($model->getPieceId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

			$this->loadLayout();

			$this->_addContent($this->getLayout()->createBlock('frans/adminhtml_productPiece_edit'))
				->_addLeft($this->getLayout()->createBlock('frans/adminhtml_productPiece_edit_tabs'));

			$this->renderLayout();

		}
		else
		{
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('frans')->__('Record does not exist'));
			$this->_redirect('*/*/');
		}

	}
	
	
	public function newAction() {
		$this->_forward('edit');

	}
	
	public function saveAction() {
		$folder = Mage::getModel("frans/productPiece")->getMediaPath();

        $session = Mage::getSingleton('adminhtml/session');
		if ($data = $this->getRequest()->getPost()) {
			
			foreach ($_FILES as $key => $value)
			{
				if(isset($_FILES[$key]['name']) and (file_exists($_FILES[$key]['tmp_name']))) {
					//save the product piece images
					$path = Mage::getBaseDir('media') . DS . $folder . DS ;
					$uploader = new Varien_File_Uploader($key);
				    $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png')); // or pdf or anything
				    $uploader->setAllowRenameFiles(true);
				    $uploader->setFilesDispersion(false);
				    $saveArr = $uploader->save($path, $_FILES[$key]['name']);			   
				    $data[$key] = $folder . DS .  $saveArr['file'];
				}
				elseif (isset($data[$key]["delete"])  & $data[$key]["delete"] == "1" )
				{
					$path = Mage::getBaseDir('media') . DS . $folder ;
					$path = $path . $data[$key]["value"] ; 
					unlink($path);  
					$data[$key] = null;
				}
				else
				{
					unset($data[$key]);
				}
			}
			
			$model = Mage::getModel('frans/productPiece');
			$model->setData($data)
				->setPieceId($this->getRequest()->getParam('piece_id'));

			try {


				$model->save();

				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('frans')->__('Record was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('piece_id' => $model->getPieceId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
				
			}
			catch(Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('piece_id' => $this->getRequest()->getParam('piece_id')));
                return;
			}
			
		}
		
		//on save and edit
		 if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('page_id' => $model->getPieceId(), '_current'=>true));
                    return;
                }
		
		
		//no agency to save.
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('frans')->__('Record not found.'));
        $this->_redirect('*/*/');
		
	}
	
	public function deleteAction()
	{
		$id     = $this->getRequest()->getParam('piece_id');
		$model  = Mage::getModel('frans/productPiece')->load($id);
		
		$model->delete();
		$model->save();
		
		Mage::getSingleton('adminhtml/session')->addSuccess('Record was successfully deleted');
		Mage::getSingleton('adminhtml/session')->setFormData(false);

	
		$this->_redirect('*/*/index/');
		return;
	}

	
}