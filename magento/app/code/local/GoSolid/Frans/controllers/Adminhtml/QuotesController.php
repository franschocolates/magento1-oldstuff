<?php

class GoSolid_Frans_Adminhtml_QuotesController extends Mage_Adminhtml_Controller_action
{
	protected function _initAction() {
		$this->loadLayout();
		
		//Optional
		//$this->_setActiveMenu('system/pstadmin');
		//$this->_title($this->__('frans'))->_title($this->__('PST Admin'))->_title($this->__('Event Type Management'));
		
		return $this;
	}   
 
	public function indexAction() {

		$this->_initAction();
        $this->renderLayout();
	}

	public function editAction() {

		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('frans/sales_quote_item')->load($id);
		
		Mage::register('quotes_data', $model);
		
		if ($model->getId() || $id == 0) {
				$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
				if (!empty($data)) {
					$model->setData($data);
				}

				$this->loadLayout();
				
				//Optional for WYSIWYG must change loading blocks to XML
				//$this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
				//$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
				
				$this->_addContent($this->getLayout()->createBlock('frans/adminhtml_quotesgrid_edit'))
					->_addLeft($this->getLayout()->createBlock('frans/adminhtml_quotesgrid_edit_tabs'));
					
				$this->renderLayout();

			}
			else
			{
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('frans')->__('Record does not exist'));
				$this->_redirect('*/*/');
			}

	}
	
	
	public function newAction() {
		$this->_forward('edit');

	}

    public function massDeleteAction(){
        $quoteIds = $this->getRequest()->getParam('quote');
        if(!is_array($quoteIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select quote(s).'));
        } else {
            try {
                $savedQuotes = Mage::getModel('sales/quote')->getCollection()
                    ->addFieldToFilter('entity_id', array('in' => $quoteIds));
                foreach ($savedQuotes as $savedQuote) {
                        $savedQuote->setSavedQuote(0)->save();
                }

                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__('Total of %d record(s) were deleted.', count($quoteIds))
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }

        $this->_redirect('*/*/index');

    }

	public function saveAction() {
		$session = Mage::getSingleton('adminhtml/session');
        $model = Mage::getModel('frans/sales_quote_item');
		if ($data = $this->getRequest()->getPost()) {
			

			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));

			try {


				$model->save();

				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('frans')->__('Record was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
				
			}
			catch(Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
			}
			
		}
		
		//on save and edit
		 if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('page_id' => $model->getId(), '_current'=>true));
                    return;
                }
		
		
		//no agency to save.
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('frans')->__('Record not found.'));
        $this->_redirect('*/*/');
		
	}

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('sales/quotes');
    }
}