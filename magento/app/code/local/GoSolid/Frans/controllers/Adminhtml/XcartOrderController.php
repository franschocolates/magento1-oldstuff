<?php

class GoSolid_Frans_Adminhtml_XcartOrderController extends Mage_Adminhtml_Controller_action
{
	protected function _initAction() {
		$this->loadLayout();
		
		//Optional
		$this->_setActiveMenu('sales');

		return $this;
	}   
 
	public function indexAction() {

		$this->_initAction()
			->renderLayout();

	}

}