<?php

class GoSolid_Frans_Adminhtml_Sales_Order_ProductNotesController extends Mage_Adminhtml_Controller_Action{

    public function saveAction(){
        
        $item_id = $this->getRequest()->getParam('id');
        $data = $this->getRequest()->getParam('item');
        $item = Mage::getModel('sales/order_item')->load($item_id);
        
        $item->setOrderType($data[$item_id]['order_type']);
        $item->setNotes($data[$item_id]['notes']);
        $item->setCategory($data[$item_id]['category']);
        $item->save();

    }

    //If some one can edit an order they should be able to update the notes.
    //TODO: tie into the save action to notify the user if they don't have permissions...
    protected function _isAllowed(){
        return Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/edit');
    }

}