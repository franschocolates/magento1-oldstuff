<?php

class GoSolid_Frans_Adminhtml_Sales_Order_CaptureController extends Mage_Adminhtml_Controller_Action
{
	protected function _initAction() {
        $this->loadLayout()
            ->_setActiveMenu('accounting/ordersforcapture')
            ->_addBreadcrumb($this->__('Accounting'), $this->__('Accounting'));
        return $this;
	}

    /**
     * Order grid
     */
    public function gridAction()
    {
        $this->loadLayout(false);
        $this->renderLayout();
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    /**
     * Export order grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName   = 'orders_capture.csv';
        $grid       = $this->getLayout()->createBlock('frans/adminhtml_sales_order_capture_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     *  Export order grid to Excel XML format
     */
    public function exportExcelAction()
    {
        $fileName   = 'orders_capture.xml';
        $grid       = $this->getLayout()->createBlock('frans/adminhtml_sales_order_capture_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }


}