<?php

class GoSolid_Frans_Adminhtml_Sales_OrderController extends Mage_Adminhtml_Controller_Action
{
    protected function _initOrder()
    {
        $id = $this->getRequest()->getParam('order_id');
        $order = Mage::getModel('sales/order')->load($id);

        if (!$order->getId()) {
            $this->_getSession()->addError($this->__('This order no longer exists.'));
            $this->_redirect('*/*/');
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
            return false;
        }
        Mage::register('sales_order', $order);
        Mage::register('current_order', $order);
        return $order;
    }

	protected function _initAction() {
		$this->loadLayout();
		return $this;
	}

	public function childGridAction() {
		$parent_id = $this->getRequest()->getParam('parent_id');
		$filter = $this->getRequest()->getParam('filter');
		
		if ($parent_id > 0)
		{
			$block = $this->getLayout()->createBlock('frans/adminhtml_sales_order_view_tab_info_shipmentGrid');
			$block->setParentOrderId($parent_id);
        	$this->getResponse()->setBody(
            	$block->toHtml()
        	);
		}
	}

    public function unshipAction()
    {
        $id = $this->getRequest()->getParam('order_id');
        /** @var GoSolid_Frans_Model_Sales_Order $order */
        $order = Mage::getModel('sales/order')->load($id);

        $transaction = Mage::getSingleton('core/resource')->getConnection('core_write');

        try
        {
            $transaction->beginTransaction();

            //remove shipments.
            $shipments = $order->getShipmentsCollection();
            foreach($shipments as $shipment)
            {
                $shipment->delete();

            }

            //remove items
            $items = $order->getAllItems();
            foreach($items as $item)
            {
                $item->setQtyShipped(0);
                $item->save();

            }

            $order->setShipDate(null);
            $order->setIsReadyForCapture(false);
            $order->setShipmentEmailSentAt(null);
            $order->setStatus(Mage_Sales_Model_Order::STATE_PROCESSING);
            $order->save();


            $transaction->commit();

            $this->_getSession()->addSuccess("The order was unshipped and set back to processing status." );

        }
        catch (Exception $ex)
        {
            $transaction->rollback();
            $this->_getSession()->addError("The order could not be unshipped. " . $ex->getMessage() );
        }

        $this->_redirect('*/sales_order/view', array('order_id' => $order->getId()));


    }

    public function changestatusAction()
    {
        $orderId = $this->getRequest()->getParam("order_id");
        $newStatus = $this->getRequest()->getParam("status");

        $order = Mage::getModel('sales/order')->load($orderId);



        $order->setStatus($newStatus);
        $order->save();



        $this->_redirect('*/sales_order/view', array('order_id' => $order->getId()));
    }

    public function changestatusmassAction()
    {
        $orderIds = $this->getRequest()->getParam('order_ids');
        $status = $this->getRequest()->getParam('status_value');

        $orders = Mage::getModel('sales/order')
                    ->getCollection()
                    ->addFieldToFilter('entity_id', array ('in' => $orderIds));

        $successCount = 0;
        $parentOrdersCount = 0;

        foreach ($orders as $order)
        {
            if($order->getIsMultishipParent() == true)
            {
                $parentOrdersCount++;

            }
            else // not a parent order, so we can change it.
            {
                $successCount++;
                $order->setStatus($status);
                $order->save();
            }
        }


        if($successCount > 0)
        {
            $this->_getSession()->addSuccess($successCount . " " . "order(s) updated successfully");
        }
        if($parentOrdersCount > 0)
        {
            $this->_getSession()->addError($parentOrdersCount . " " . "order(s) could not be updated with the new status because they are not shipment orders.");
        }


        $this->_redirect('adminhtml/sales_order/index');

    }

    public function changePaymentAction()
    {
        /* @var GoSolid_Frans_Model_Sales_Order $order */
        if ($order = $this->_initOrder())
        {

            $paymentData = $this->getRequest()->getParam('payment');

            if ($order->getId() && $order->canEditPayment() && $paymentData)
            {
                try
                {
                    $order->changePayment($paymentData)
                        ->save();


                    //return a special case when it is paid in the store.
                    if($this->getRequest()->getParam("is_retail_payment") == true)
                    {
                        //update the status to picked up.
                        $order->setStatus("picked_up");
                        $today = Mage::getModel('core/date')->date("Y-m-d");
                        $order->setShipDate($today);

                        if (!$order->getIsMultishipChildOrder())
                        {
                            $order->setIsReadyForCapture(true);
                        }
                        $order->save();

                        // let them know it was picked up.
                        $order->sendShipmentEmail(true, $comment = "", $batch = false );

                        //show the actions in retail form.
                        // spoofing the entire order info section
                        // so that totals get updated
                        $orderData = array( 'value' => array('entity_id' => $order->getId() ));
                        $itemsFormModel = Mage::getModel('frans/pickupmanager_itemgrid', $orderData);
                        echo $itemsFormModel->getElementHtml();
                        return;
                    }






                    $this->loadLayout();
                    $this->renderLayout();
                }
                catch (Exception $exception)
                {
                    Mage::logException($exception);
                    throw $exception;
                }
            }
            else
            {
                Mage::throwException('This order is not eligible for editing payment');
            }
        }
    }

	public function saveShipMethodAction() 
	{
		$orderId = $this->getRequest()->getParam("order_id");
        /* @var GoSolid_Frans_Model_Sales_Order $order */
		$order = Mage::getModel('sales/order')->load($orderId);
		
		$shippingMethod = $this->getRequest()->getParam("shipping_method");
		$shippingAmount = $this->getRequest()->getParam('adjusted_rate');
        $preferredArrivalDate = $this->getRequest()->getParam('preferred_arrival_date');
        $plannedShipDate = $this->getRequest()->getParam('planned_ship_date');

		$order->updateShipMethod($shippingMethod, $shippingAmount, $preferredArrivalDate, $plannedShipDate);

		# we don't do anything other than set the order, 
		# since we don't want to load the rates.
		Mage::register('current_order', $order);
		$shippingBlock = $this->getLayout()->createBlock('frans/adminhtml_sales_order_view_shipping_method_form');
		$shippingBlock->setIsEditMode(false);
		
		$totalsBlock = $this->getLayout()->createBlock('adminhtml/sales_order_totals');
		$totalsBlock->setTemplate('sales/order/totals.phtml');

		$responseData = array (
			'success' => true,
			'shippingContent' => $shippingBlock->toHtml(),
			'totalsContent' => $totalsBlock->toHtml()
		);
		
		$jsonData = Mage::helper('core')->jsonEncode($responseData);
        	
		$this->getResponse()->setHeader('Content-type', 'application/json');		

		$this->getResponse()->setBody($jsonData);
	}
	
	public function editShippingFormAction() {
		$orderId = $this->getRequest()->getParam("order_id");
		$order = Mage::getModel('sales/order')->load($orderId);
		
		# need the order address in order to collect the rates
        /* @var GoSolid_Frans_Model_Sales_Order_Address $shippingAddress */
		$shippingAddress = $order->getShippingAddress();
		
		$success = $shippingAddress->requestShippingRates();
		
		Mage::register('current_order', $order);
		$block = $this->getLayout()->createBlock('frans/adminhtml_sales_order_view_shipping_method_form');
		$block->setIsEditMode(true);
		
		$this->getResponse()->setBody(
            	$block->toHtml()
        	);
	}

    public function resendShipmentNotificationAction()
    {

        $orderId = $this->getRequest()->getParam("order_id");
        $order = Mage::getModel("sales/order")->load($orderId);

        $result = $order->sendShipmentEmail(true, ''); //send the email with no message.



        if($result == true)
        {
            $this->_getSession()->addSuccess($this->__('Shipment notification email sent.'));
        }
        else
        {
            $this->_getSession()->addError($this->__('Unable to send shipment notification email.'));
        }

        $this->_redirect('adminhtml/sales_order/view', array("order_id" => $orderId ));

    }


	public function saveAdminNotesAction() 
	{
        return $this->_updateOrderField(array('admin_notes'), true);
	}

    public function saveAdditionalAction()
    {
        return $this->_updateOrderField(array('gift_message', 'special_instructions'));
    }

    public function saveWeightAction()
    {
        return $this->_updateOrderField(array('ice_packs'));
    }

    public function activityAction()
    {
        $this->_initOrder();
        $this->loadLayout();
        $this->renderLayout();
    }

    public function updateShippingAddressAction()
    {
        if ($order = $this->_initOrder()) {
            try
            {
                $response = false;
                $shippingAddressId = $this->getRequest()->getParam('address_id');
                $shippingAddress = $order->getAddressById($shippingAddressId);
                $addressData = $this->getRequest()->getParam('updated_address');

                Mage::helper('frans')->applyChangesFromAddressValidation($shippingAddress, $addressData);

                $shippingAddress->save();

                // to render it as html in case we aren't doing json.
                $this->loadLayout('empty')
                    ->renderLayout();
            }
            catch (Mage_Core_Exception $e) {
                $response = array(
                    'error'     => true,
                    'message'   => $e->getMessage(),
                );
            }
            if (is_array($response)) {
                $response = Mage::helper('core')->jsonEncode($response);
                $this->getResponse()->setBody($response);
            }
        }
    }

    /**
     * @param array $fieldNames
     */
    protected function _updateOrderField($fieldNames, $asJson = false)
    {
        if ($order = $this->_initOrder()) {
            try {
                $response = false;
                $request = $this->getRequest()->getParams();

                foreach ($fieldNames as $fieldName)
                {
                    $order->setData($fieldName, $request['order'][$fieldName]);
                }
                $order->save();

                if ($asJson)
                {
                    $response = array('success' => true);
                }
                else
                {
                    // to render it as html in case we aren't doing json.
                    $this->loadLayout('empty')
                        ->renderLayout();
                }
            }
            catch (Mage_Core_Exception $e) {
                $response = array(
                    'error'     => true,
                    'message'   => $e->getMessage(),
                );
            }
            catch (Exception $e) {
                $response = array(
                    'error'     => true,
                    'message'   => $this->__("Cannot update $fieldName.")
                );
            }
            if (is_array($response)) {
                $response = Mage::helper('core')->jsonEncode($response);
                $this->getResponse()->setBody($response);
            }
        }
    }

    public function massChangeShipDateAction()
    {
        $orderIds = $this->getRequest()->getParam('order_ids');
        $shipDate = $this->getRequest()->getParam('ship_date');

        if (!is_array($orderIds)) {
            $this->_getSession()->addError($this->__('Please select order(s).'));
        }
        else if (!$shipDate)
        {
            $this->_getSession()->addError($this->__('Please specify a ship date.'));
        }
        else {
            $parsedDate = new DateTime($shipDate);
            $today = new DateTime('today');
            if ($parsedDate >= $today)
            {
                $dateToSave = $parsedDate->format('Y-m-d');
                Mage::log("Updating ship date to $dateToSave for these orders: " . implode(',', $orderIds));
                if (!empty($orderIds)) {
                    try
                    {
                        $updated = 0;
                        $skipped = 0;
                        $orders = Mage::getModel('sales/order')
                            ->getCollection()
                            ->addFieldToFilter('entity_id', array ('in' => $orderIds));

                        foreach ($orders as $order)
                        {
                            // only change the ship date if we have an order
                            // and it isn't captured
                            // and it is not a parent order
                            if ($order->getId() && !$order->getCapturedAt() && !$order->getIsMultishipParent())
                            {
                                $order->setPlannedShipDate($dateToSave);
                                $order->save();
                                $updated++;
                            }
                            else
                            {
                                $skipped++;
                            }
                        }
                        $message = $this->__('Changed total of %d order(s) to have ship date of %s', $updated, $shipDate);

                        if ($skipped > 0)
                        {
                            $message .= $this->__(' (%d order(s) skipped)', $skipped);
                        }

                        $this->_getSession()->addSuccess($message);
                    } catch (Exception $e) {
                        $this->_getSession()->addError($e->getMessage());
                    }
                }
            }
            else
            {
                $this->_getSession()->addError($this->__('Please specify a ship date of today or in the future.'));
            }

        }
        $this->_redirect('adminhtml/sales_order/index');
    }

    public function massCaptureAction()
    {
        $this->_initAction();

        $orderIds = $this->getRequest()->getPost('order_ids', array());

        $errorOrderIds = array();
        $successOrderIds = array();
        foreach($orderIds as $orderId)
        {
            $order = Mage::getModel('sales/order')->load($orderId);

            if(!$order->canInvoice())
            {
                $errorOrderIds[] = $order->getIncrementId();
                continue;
            }

            try
            {
                $invoice = Mage::getModel('sales/service_order', $order)
                                ->prepareInvoice() // use default of everything
                                ->setRequestedCaptureCase(true) // need to capture
                                ->register();

                // save via a transaction
                Mage::getModel('core/resource_transaction')
                    ->addObject($invoice)
                    ->addObject($invoice->getOrder())
                    ->save();

                $successOrderIds[] = $order->getIncrementId();

                // this catch block is a bit generic. May need to drill down to more specific cases.
            }
            catch (Mage_Core_Exception $e)
            {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError($this->__('Failed to capture payment for order #%s', $order->getIncrementId()));
                Mage::logException($e);
            }
        }

        if(!empty($errorOrderIds)):
            Mage::getSingleton('core/session')->addError($this->__('Could not captured orders: #%s.', implode(', #', $errorOrderIds)));
        endif;

        if(!empty($successOrderIds)):
            Mage::getSingleton('core/session')->addSuccess($this->__('Successfully captured orders: #%s.', implode(', #', $successOrderIds)));
        endif;

        if ($this->getRequest()->getParam('origin') == 'capture_grid')
        {
            $this->_redirect('*/sales_order_capture/index');
        }
        else
        {
            $this->_redirect('*/*/index');
        }
    }

    /**
     * Marks a set of orders as being printed.
     */
    public function massMarkAsPrintedAction(){

        $ordersSaved = false;
        $errorCount = 0;
        $successCount = 0;
        if($orderId = $this->getRequest()->getParam('order_id', false))
        {
            $order = Mage::getModel('sales/order')->load($orderId);
            if ($order->getId())
            {
                if($order->getStatus() != 'canceled')
                {
                    $order->setPrinted()->save();
                    $successCount++;
                }
                else
                {
                    $errorCount++;
                };
            }
        }
        elseif ( $orderIds = $this->getRequest()->getParam('order_ids', false))
        {
            $orderIdsSplit = explode(',', $orderIds);
            foreach($orderIdsSplit as $id)
            {

                $order = Mage::getModel('sales/order')->load($id);
                if ($order->getId())
                {
                    if($order->getStatus() != 'canceled')
                    {
                        $order->setPrinted()->save();
                        $successCount++;
                    }
                    else
                    {
                        $errorCount++;
                    };
                }
            }
        }
        if($successCount > 0)
        {
            Mage::getSingleton('core/session')->addSuccess($successCount.' orders were successfully marked as printed.');
        }
        if($errorCount > 0){
            Mage::getSingleton('core/session')->addError($this->__('Could not update orders: '.$errorCount.' orders flagged as cancelled.'));
        }
        $this->_redirect('*/*/index');
    }

    public function resendVirtualGiftCardAction()
    {
        $orderItemId = $this->getRequest()->getParam('order_item_id', false);
        $emails = $this->getRequest()->getParam('emails', array());

        if ($orderItemId && $emails)
        {
            // check to make sure we have that order item, and it's a virtual gift card
            $virtualGiftCard = Mage::getModel('givex/giftcard')->loadByOrderItemId($orderItemId);

            if ($virtualGiftCard->getId() && $virtualGiftCard->getIsVirtual())
            {
                // can resend, do it
                Mage::log("Will resend email for order item $orderItemId to emails " . implode(',', $emails));
                Mage::helper('frans/giftCard')->sendVirtualGiftCardEmail($virtualGiftCard, $emails);
            }

        }
    }
    protected function _isAllowed(){
        return Mage::getSingleton('admin/session')->isAllowed('sales/order');
    }


    public function massSendShipmentEmailsAction()
    {
        $child_order_ids = $this->getRequest()->getPost('order_ids', array());

        // now, we need to find parent orders based on these child order ids
        $parent_order_ids = Mage::getModel('sales/order')->getCollection()
                                ->addAttributeToSelect('entity_id')
                                ->addAttributeToSelect('multiship_parent_id')
                                ->addAttributeToFilter('multiship_parent_id', array('notnull' => true))
                                ->addAttributeToFilter('entity_id', array('in' => $child_order_ids))
                                ->getColumnValues('multiship_parent_id');

        $parent_orders = Mage::getModel('sales/order')->getCollection()
                                ->addAttributeToFilter('entity_id', array('in' => $parent_order_ids))
                                ->load();

        $success_count = 0;

        foreach($parent_orders->getItems() as $order):
            $order->sendShipmentEmail(true, '', true);
            $order->addStatusHistoryComment("Shipment confirmation has been sent.", false);
            $order->save();
            $success_count++;
        endforeach;

        Mage::getSingleton('core/session')->addSuccess('Successfully sent shipment emails for ' . $success_count . ' orders.');

        $this->_redirect('*/*/index');

    }

}