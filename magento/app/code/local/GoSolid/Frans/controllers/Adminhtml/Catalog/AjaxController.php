<?php

class GoSolid_Frans_Adminhtml_Catalog_AjaxController extends Mage_Adminhtml_Controller_Action
{
	protected function _initAction() {
		$this->loadLayout();
		return $this;
	}

	public function indexAction() {

		$this->_initAction();
		$this->renderLayout();
	}

	public function getProductGridLayoutsAction() {
		$layouts = Mage::helper("frans/catalog_category")->getProductGridLayoutIcons();
		echo json_encode($layouts);
	}

	public function getChildCategoryLayoutsAction() {
		$layouts = Mage::helper("frans/catalog_category")->getChildCategoryLayoutIcons();
		echo json_encode($layouts);
	}

}