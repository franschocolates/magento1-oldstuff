<?php

class GoSolid_Frans_Adminhtml_ShipmentManagerController extends Mage_Adminhtml_Controller_Action
{
	const ERROR_INVALID_CARRIER = -1;
	const ERROR_CARRIER_NO_LABELS = -2;
	
	protected function _initAction() 
	{
		$this->loadLayout();
		return $this;
	}   
 
	protected function _initOrder($loadByIncrement)
	{
		$orderId = $this->getRequest()->getParam('order_id');

		# make sure exists and can ship
		if ($orderId)
		{
			# we can load by either increment or by actual ID
			$order = ($loadByIncrement) ? 
						Mage::getModel('sales/order')->loadByIncrementId(trim($orderId))
						: Mage::getModel('sales/order')->load($orderId);  
			
			if ($order && $order->getId())
			{
				return $order;
			}
		}
		
		return false;
	}
	
	public function indexAction() 
	{
		$this->_initAction()
			->renderLayout();
	}
	
	public function newAction() 
	{

		$responseAjax = new Varien_Object();
		# make sure exists and can ship
		$params = $this->getRequest()->getParams();
        /** @var GoSolid_Frans_Model_Sales_Order $order */

		if ($order = $this->_initOrder(true))
		{
			if ($order->getIsMultishipParent())
			{
				$formBlock = $this->getLayout()
								->createBlock('frans/adminhtml_shipmentManager')
								->setTemplate('shipmentManager/not_shippable.phtml')
								->setOrder($order);
			}
            else if ($order->getIsVirtual())
            {
                $formBlock = $this->getLayout()
                    ->createBlock('frans/adminhtml_shipmentManager')
                    ->setTemplate('shipmentManager/virtual_order.phtml')
                    ->setOrder($order);
            }
            else if ($order->getIsOnHold())
            {
                $formBlock = $this->getLayout()
                    ->createBlock('frans/adminhtml_shipmentManager')
                    ->setTemplate('shipmentManager/on_hold.phtml')
                    ->setOrder($order);
            }
            else if ($order->getState() == Mage_Sales_Model_Order::STATE_CANCELED)
            {
                $formBlock = $this->getLayout()
                    ->createBlock('frans/adminhtml_shipmentManager')
                    ->setTemplate('shipmentManager/canceled.phtml')
                    ->setOrder($order);
            }
			else
			{
				# figure out if anything shipped before or if forced
				$forceShipment = $this->_forceShipment();
				$shipmentCount = $order->getShipmentsCollection()->count(); 
				# if no shipments, can ship, or forced, show the form
				if ($shipmentCount == 0 || $forceShipment)
				{
					$formBlock = $this->getLayout()
									->createBlock('frans/adminhtml_shipmentManager_form')
									->setTemplate('shipmentManager/form.phtml')
									->setOrder($order);
				}
				elseif ($order->canShip() && $shipmentCount > 0)
				{
					$formBlock = $this->getLayout()
									->createBlock('frans/adminhtml_shipmentManager')
									->setTemplate('shipmentManager/partially_shipped.phtml')
									->setOrder($order);
				}
				else
				{
					# what about cancelled orders?
					$formBlock = $this->getLayout()
									->createBlock('frans/adminhtml_shipmentManager')
									->setTemplate('shipmentManager/shipped.phtml')
									->setOrder($order);
				}
				
			}

			$responseAjax->setHtml($formBlock->toHtml());
			$responseAjax->setSuccess(true);
		} 
		else
		{
			$orderId = trim($this->getRequest()->getParam('order_id'));
			# TODO: does not exist, give message
			$formBlock = $this->getLayout()
							->createBlock('core/template')
							->setTemplate('shipmentManager/not_found.phtml')
							->setOrderId($orderId);
			$responseAjax->setHtml($formBlock->toHtml());
			$responseAjax->setSuccess(true);
		}

		$this->getResponse()->setBody($responseAjax->toJson());
	}
	
	public function saveShipmentAction()
	{
		$responseAjax = new Varien_Object();
		
		if ($order = $this->_initOrder(false))
		{
			try
			{
				$shipment = $this->_initShipment($order);
				$params = $this->getRequest()->getParams();



				
				# registering the shipment updates the order so that it
				# knows about changed quantities
            	$shipment->register();

            	# figure out if it's manual (no label needed)
            	# or if we are printing (label needed)
            	$printLabel = isset($params['create_shipping_label']) && $params['create_shipping_label'];
            	
            	$success = true;
            	if ($printLabel)
            	{
            		Mage::log('Creating a label for new shipment.');
            		// will return an int code if errored.
            		if (
            			($trackingNumber = $this->_createShippingLabel($shipment)) &&
            			!is_int($trackingNumber))
            		{
            			Mage::log("Successfully created tracking number $trackingNumber");

                        //print the label.
                        $printStationId = $params["print_station"];
                        $document = $shipment->getShippingLabel();
                        $entityType = "sales/order";
                        $entityId = $shipment->getOrderId();
                        $printJob = Mage::getModel("frans/printJobManager")->addPrintJob($printStationId, GoSolid_Frans_Model_PrintJob::JOB_TYPE_ZPLII, $document, $entityType, $entityId);

                        //set the cookie for the default print station.
                        Mage::getModel('core/cookie')->set("default_print_station", $printStationId);

                    }
            		else
            		{
						$success = false;
						$responseAjax->setSuccess(false);
						
						$errorMessage = 'An error occurred creating the shipment.';
						if (is_int($trackingNumber))
						{
							switch ($trackingNumber)
							{
								case self::ERROR_CARRIER_NO_LABELS:
									$errorMessage = Mage::helper('frans')->__('That carrier does not support printing labels.');
									break;
								case self::ERROR_INVALID_CARRIER:
									$errorMessage = Mage::helper('frans')->__('That carrier is no longer supported.');
									break;
							} 
						}
						$responseAjax->setMessage($errorMessage);
            		}




            	}



                //save the ship method to the order.
                $carrier = false;
                $shipMethod = $this->_getShippingMethod();

                if ($shipMethod instanceof Varien_Object) {
                    $className = Mage::getStoreConfig('carriers/' . $shipMethod->getCarrierCode() . '/model');
                    if ($className) {
                        $carrier = Mage::getModel($className);
                    }
                }

                $order->setIcePacks($params["ice_packs"]);
                $order->setShippingDescription($this->_getShippingDescription($carrier, $shipMethod->getMethod()));
                $order->setShippingMethod($shipMethod->getFullMethod());


            	if ($success)
            	{

                    if(!$order->getIsMultishipChildOrder() && !$order->getIsMultishipParent()){
                        $shipment->setEmailSent(true);
                    }

                    $this->_saveShipment($shipment);

                    if(!$order->getIsMultishipChildOrder() && !$order->getIsMultishipParent()){
                        $shipment->sendEmail();
                    }

                    if(!empty($params['capture'])){
                        $this->_capturePayment($shipment);
                    }

					$shippedBlock = $this->getLayout()
									->createBlock('core/template')
									->setTemplate('shipmentManager/success.phtml')
									->setOrder($order)
									->setTrackingNumber($trackingNumber)
									->setIsManual(!$printLabel);
					
					$responseAjax->setSuccess(true);
					$responseAjax->setHtml($shippedBlock->toHtml());
            	}
			}
			catch (Exception $exception)
			{
				Mage::logException($exception);
				
				$responseAjax->setSuccess(false);
				$responseAjax->setMessage(Mage::helper('frans')->__('An error occurred creating the shipment. ' . $exception->getMessage() ));
			}
		}
		else
		{
			
			$responseAjax->setSuccess(false);
			$responseAjax->setMessage(Mage::helper('frans')->__('Order could not be located or is not shippable.'));
		}
		
		$this->getResponse()->setBody($responseAjax->toJson());
	}

	/**
	 * Reprint already printed shipping label
	 */
	public function reprintAction()
	{
		$params = $this->getRequest()->getParams();

		if(isset($params['shipment']))
		{
			Mage::log("Starting to reprint shipping label for shipment #: " . $params['shipment']);

			//reprint the label.
			$shipment = Mage::getModel('sales/order_shipment')->loadByIncrementId($params['shipment']);
			$printStationId = $params["print_station"];
			$document = $shipment->getShippingLabel();
			$entityType = "sales/order";
			$entityId = $shipment->getOrderId();

			$printJob = Mage::getModel("frans/printJobManager")->addPrintJob($printStationId, GoSolid_Frans_Model_PrintJob::JOB_TYPE_ZPLII, $document, $entityType, $entityId);

			//set the cookie for the default print station.
			Mage::getModel('core/cookie')->set("default_print_station", $printStationId);

			Mage::getSingleton('core/session')->addSuccess("Shipping label printed for order # " . $params['order_id'] . ", shipment # " . $params['shipment'] . ".");
		}
		else
		{
			if(!isset($params['shipment']))
				Mage::getSingleton('core/session')->addError("No shipping label was selected for printing.");
			else
				Mage::getSingleton('core/session')->addError("There was a problem reprinting the shipping label.");

			/*
			 * if(!isset($params['shipment']))
				$this->_getSession()->addError("No shipping label was selected for printing.");
			else
				$this->_getSession()->addError("There was a problem reprinting the shipping label.");
			*/
		}

		$this->_redirect("*/*");
	}

	protected function _buildShippedData($order)
	{
				
		$data = array(
					'html' => $shippedBlock->toHtml(),
					'success' => true
		);
		
		return $data;
	}
	
	protected function _sendJsonResponse($data)
	{
		$jsonData = Mage::helper('core')->jsonEncode($data);
		
		$this->getResponse()->setHeader('Content-type', 'application/json');
		$this->getResponse()->setBody($jsonData);
	}
	
    /**
     * Save shipment and order in one transaction
     *
     * @param Mage_Sales_Model_Order_Shipment $shipment
     * @return Mage_Adminhtml_Sales_Order_ShipmentController
     */
    protected function _saveShipment($shipment)
    {
        $shipment->getOrder()->setIsInProcess(true);
        $transactionSave = Mage::getModel('core/resource_transaction')
            ->addObject($shipment)
            ->addObject($shipment->getOrder())
            ->save();

        return $this;
    }
	
    /**
     * Initialize shipment items QTY
     */
    protected function _getItemQtys()
    {
        $params = $this->getRequest()->getParams();
        if (isset($params['items'])) {
            $qtys = $params['items'];
        } else {
            $qtys = array();
        }


        return $qtys;
    }
    
    /**
     * Initialize shipment model instance
     * Adopted from base ShipmentController, but we don't have to worry about existing shipments
     *
     * @return Mage_Sales_Model_Order_Shipment|bool
     */
    protected function _initShipment($order)
    {
        $shipment = false;

        # get the qtys they may have passed in
        $qtys = $this->_getItemQtys();
		$shipment = Mage::getModel('sales/service_order', $order)->prepareShipment($qtys);

		# read the other information from our params
		$this->_setShipmentProperties($shipment);
		
        Mage::register('current_shipment', $shipment);
        return $shipment;
    }
    
    protected function _setShipmentProperties($shipment)
    {
    	$params = $this->getRequest()->getParams();
    	$icePacks = $params['ice_packs'] ? $params['ice_packs'] : 0;
    	$shipment->setIcePacks($icePacks);
    	
    	$icePackModel = Mage::getModel('frans/icePack');
    	
    	# calculate the total weight from the items that are being shipped
    	$totalWeight = 0;
    	foreach ($shipment->getAllItems() as $item)
    	{
    		$totalWeight += ($item->getWeight() * $item->getQty());
    	}
		$totalWeight += ($icePacks * $icePackModel->getWeight());

    	# also calculate the weight, based on ice packs * ice pack weight + order weight
    	$shipment->setTotalWeight($totalWeight);
    	
    	$shipment->setShipDate($params['ship_date']);
    }
	
    /**
     * Create shipping label for specific shipment with validation.
     *
     * @param Mage_Sales_Model_Order_Shipment $shipment
     * @return bool|string
     */
    protected function _createShippingLabel(Mage_Sales_Model_Order_Shipment $shipment)
    {
        if (!$shipment) {
            return false;
        }
        $carrier = false;
        $shipMethod = $this->_getShippingMethod();

		if ($shipMethod instanceof Varien_Object) {
        	$className = Mage::getStoreConfig('carriers/' . $shipMethod->getCarrierCode() . '/model');
			if ($className) {
            	$carrier = Mage::getModel($className);
			}
		}
        
		if ($carrier === false)
		{
			return self::ERROR_INVALID_CARRIER;
		}
		else if (!$carrier->isShippingLabelsAvailable())
		{
			return self::ERROR_CARRIER_NO_LABELS;
		}
		
        # for shipment manager/quickship, we always have one package, just need to set the items/weight
        $packageItems = array();
        foreach ($shipment->getAllItems() as $shipmentItem)
        {
        	$packageItems[$shipmentItem->getOrderItemId()] = 
        		array(
        			'qty' => $shipmentItem->getQty(),
        			'customs_value' => $shipmentItem->getPrice(),
        			'price' => $shipmentItem->getPrice(),
        			'name' => $shipmentItem->getName(),
        			'weight' => $shipmentItem->getWeight(),
        			'product_id' => $shipmentItem->getProductId()
        		);
        }
        $shipment->setPackages(
        			array(
	        			1 => array(
	        				'params' => array(
	        					'weight' => $shipment->getTotalWeight(), 
	        					'container' => 'YOUR_PACKAGING',
	        					'weight_units' => 'POUND',
	        					'delivery_confirmation' => 'NO_SIGNATURE_REQUIRED',
	        					),
	        				'items' => $packageItems
	        					)
	        			)
        );
        
        # use special override to request a specific method (not use the order)
        $response = Mage::getModel('shipping/shipping')->requestToShipment($shipment, $shipMethod);

//var_dump($response); die;

        if ($response->hasErrors()) {
            Mage::throwException($response->getErrors());
        }
        if (!$response->hasInfo()) {
            return false;
        }
        $labelsContent = array();
        $trackingNumbers = array();
        $info = $response->getInfo();
        foreach ($info as $inf) {
            if (!empty($inf['tracking_number']) && !empty($inf['label_content'])) {
                $labelsContent[] = $inf['label_content'];
                $trackingNumbers[] = $inf['tracking_number'];
            }
        }



       // $outputPdf = $this->_combineLabelsPdf($labelsContent);
        $shipment->setShippingLabel($labelsContent[0]); //assuming only one shipment.
        $carrierCode = $carrier->getCarrierCode();
        $carrierTitle = Mage::getStoreConfig('carriers/'.$carrierCode.'/title', $shipment->getStoreId());
        
        $returnTrackingNumber = 'Unknown';
        if ($trackingNumbers) {
            foreach ($trackingNumbers as $trackingNumber) {
            	$track = Mage::getModel('sales/order_shipment_track')
                        ->setNumber($trackingNumber)
                        ->setCarrierCode($carrierCode)
                        ->setTitle($carrierTitle)
                        # also set the method/description
                        ->setShippingMethod($shipMethod->getFullMethod())
                        ->setShippingDescription($this->_getShippingDescription($carrier, $shipMethod->getMethod()));
                $shipment->addTrack($track);
                
                $returnTrackingNumber = $trackingNumber;
            }
        }


        //set the orders ship method.


        return $returnTrackingNumber;
    }

    /**
     * Retrieve shipping method from passed in parameters
     * Adapted from order version
     *
     * @return string|Varien_Object
     */
    private function _getShippingMethod()
    {
        $shippingMethod = $this->getRequest()->getParam('shipping_method');
        list($carrierCode, $method) = explode('_', $shippingMethod, 2);

        return new Varien_Object(array(
                'carrier_code' => $carrierCode,
                'method'       => $method,
        		'full_method'  => $shippingMethod,
            ));
    }

    private function _getShippingDescription($carrierModel, $method) 
    {
    	$methods = $carrierModel->getAllowedMethods();
    	return $carrierModel->getConfigData('title') . ' - ' . $methods[$method];
	}
     
    
    /**
     * Combine array of labels as instance PDF
     *
     * @param array $labelsContent
     * @return Zend_Pdf
     */
    protected function _combineLabelsPdf(array $labelsContent)
    {
        $outputPdf = new Zend_Pdf();
        foreach ($labelsContent as $content) {
            if (stripos($content, '%PDF-') !== false) {
                $pdfLabel = Zend_Pdf::parse($content);
                foreach ($pdfLabel->pages as $page) {
                    $outputPdf->pages[] = clone $page;
                }
            } else {
                $page = $this->_createPdfPageFromImageString($content);
                if ($page) {
                    $outputPdf->pages[] = $page;
                }
            }
        }
        return $outputPdf;
    }
    
    /**
     * Create Zend_Pdf_Page instance with image from $imageString. Supports JPEG, PNG, GIF, WBMP, and GD2 formats.
     *
     * @param string $imageString
     * @return Zend_Pdf_Page|bool
     */
    protected function _createPdfPageFromImageString($imageString)
    {
        $image = imagecreatefromstring($imageString);
        if (!$image) {
            return false;
        }

        $xSize = imagesx($image);
        $ySize = imagesy($image);
        $page = new Zend_Pdf_Page($xSize, $ySize);

        imageinterlace($image, 0);
        $tmpFileName = sys_get_temp_dir() . DS . 'shipping_labels_'
                     . uniqid(mt_rand()) . time() . '.png';
        imagepng($image, $tmpFileName);
        $pdfImage = Zend_Pdf_Image::imageWithPath($tmpFileName);
        $page->drawImage($pdfImage, 0, 0, $xSize, $ySize);
        unlink($tmpFileName);
        return $page;
    }

    private function _forceShipment()
    {
    	$params = $this->getRequest()->getParams();
    	return isset($params['force']) && $params['force'];
    }
    protected function _isAllowed(){
        return Mage::getSingleton('admin/session')->isAllowed('sales/shipmentmanager');
    }

    protected function _capturePayment($shipment){

        // reload fresh from db
        $order_id = $shipment->getOrder()->getId();
        $order = Mage::getModel('sales/order')->load($order_id);

        // only invoice fully shipped, and invoice-able, orders
        if($order->canShip() || !$order->canInvoice()){
            return $this;
        }

        $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();

        if (!$invoice->getTotalQty()) {
            return $this;
        }

        $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_ONLINE);
        $invoice->register();

        $invoice_order = $invoice->getOrder();
        
        $transactionSave = Mage::getModel('core/resource_transaction')
                                                ->addObject($invoice)
                                                ->addObject($invoice_order);

        $transactionSave->save();

    }
}