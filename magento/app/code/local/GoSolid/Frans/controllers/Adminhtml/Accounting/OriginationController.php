<?php

class GoSolid_Frans_Adminhtml_Accounting_OriginationController extends Mage_Adminhtml_Controller_Action
{
	protected function _initAction() {
        $this->loadLayout()
            ->_setActiveMenu('accounting/accountingoriginations')
            ->_addBreadcrumb($this->__('Accounting'), $this->__('Accounting'));
		//$this->_title($this->__('frans'))->_title($this->__('PST Admin'))->_title($this->__('Event Type Management'));

		return $this;
	}

	public function indexAction() {

		$this->_initAction()
			->renderLayout();

	}

	public function editAction() {

		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('frans/accounting_origination')->load($id);

		Mage::register('origination_data', $model);

		if ($model->getId() || $id == 0) {
				$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
				if (!empty($data)) {
					$model->setData($data);
				}

				$this->loadLayout();

				$this->_addContent($this->getLayout()->createBlock('frans/adminhtml_accounting_origination_edit'))
					->_addLeft($this->getLayout()->createBlock('frans/adminhtml_accounting_origination_edit_tabs'));

				$this->renderLayout();

			}
			else
			{
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('frans')->__('Record does not exist'));
				$this->_redirect('*/*/');
			}

	}


	public function newAction() {
		$this->_forward('edit');

	}

	public function saveAction() {
		$session = Mage::getSingleton('adminhtml/session');
		if ($data = $this->getRequest()->getPost()) {

			$model = Mage::getModel('frans/accounting_origination');
			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));

			try {


				$model->save();

				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('frans')->__('Record was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;

			}
			catch(Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
			}

		}

		//on save and edit
		 if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('page_id' => $model->getId(), '_current'=>true));
                    return;
                }


		//no agency to save.
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('frans')->__('Record not found.'));
        $this->_redirect('*/*/');

	}


}