<?php
class GoSolid_Frans_Adminhtml_IndexController extends Mage_Adminhtml_Controller_action
{
	protected function _initAction() {
		$this->loadLayout();
		return $this;
	}   
 
	public function indexAction() {
		
		$this->_initAction();
		$this->renderLayout();
	}
    /** Daniel did so frans can test the help desk module cron email function  */
    //TODO:remove before pushing code to produciton
    public function helpTestAction() {

        $helpDeskCron = Mage::getModel('helpdesk/cron');
        $helpDeskCron->run();
    }

    /** Steve J, adding testing action to test shipment mail issues  */
    public function shipmentMailTestingAction()
    {
        $mailCronModel = Mage::getModel('frans/sales_observer');
        $mailCronModel->sendShipmentNotificationEmails(null);
    }
    protected function _isAllowed(){
        return Mage::getSingleton('admin/session')->isAllowed('retail');
    }
}