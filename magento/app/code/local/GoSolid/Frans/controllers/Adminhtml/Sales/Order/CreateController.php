<?php
/**
 * Created by goSolid.
 */
class GoSolid_Frans_Adminhtml_Sales_Order_CreateController extends Mage_Adminhtml_Sales_Order_CreateController
{
    protected function _getQuote()
    {
        $quote =  $this->_getSession()->getQuote();
        return Mage::getModel("sales/quote")->load($quote->getId());
    }

    public function loadQuoteAction()
    {
        if ($quoteId = $this->getRequest()->getParam('quote_id', false))
        {
            // make sure it exists
            $quote = Mage::getModel('sales/quote')->loadByIdWithoutStore($quoteId);

            if ($quote->getId())
            {
                $this->_getSession()->setQuoteId($quote->getId());
                $this->_redirect('*/*');
            }
        }
    }

    public function printViewAction()
    {
        $this->_initSession();
        $this->loadLayout()
             ->renderLayout();
    }

    public function importQuoteAction()
    {
        $this->loadLayout('empty')
            ->renderLayout();
    }

    /**
     * Validate uploaded CSV files action.
     * Adapted from Magento Core ImportController
     * @see Mage_ImportExport_Adminhtml_ImportController::validateAction
     *
     * @return void
     */
    public function validateImportAction()
    {
        $data = $this->getRequest()->getPost();
        if ($data) {
            $this->loadLayout(false);
            /** @var $resultBlock Mage_ImportExport_Block_Adminhtml_Import_Frame_Result */
            $resultBlock = $this->getLayout()->getBlock('import.frame.result');
            // common actions
            $resultBlock->addAction('show', 'import_validation_container')
                ->addAction('clear', array(
                        Mage_ImportExport_Model_Import::FIELD_NAME_SOURCE_FILE,
                        Mage_ImportExport_Model_Import::FIELD_NAME_IMG_ARCHIVE_FILE)
                );

            try {
//                $adapter = Mage::getModel('frans/importExport_import_entity_quote_address');
//                /** @var $import GoSolid_Frans_Model_ImportExport_Import */
//                $import = Mage::getModel('importexport/import')
//                            ->setEntityAdapter($adapter);
//
//                $validationResult = $import->validateSource($import->setData($data)->uploadSource());
                $import = Mage::getModel('frans/importExport_import');

                $validationResult = $import->validateSource($import->setData($data)->uploadSource());

                // always need a notice about wiping out addresses
                if (count($this->_getQuote()->getAllShippingAddresses()) > 1)
                {
                    $resultBlock->addNotice($this->__('This order already contains shipment data. Any previously existing shipment data will be removed.'));
                }

                if (!$import->getProcessedRowsCount()) {
                    $resultBlock->addError($this->__('File does not contain data. Please upload another one'));
                } else {
                    if (!$validationResult) {
                        if ($import->getProcessedRowsCount() == $import->getInvalidRowsCount()) {
                            $resultBlock->addNotice(
                                $this->__('File is totally invalid. Please fix errors and re-upload file')
                            );
                        } elseif ($import->getErrorsCount() >= $import->getErrorsLimit()) {
                            $resultBlock->addNotice(
                                $this->__(
                                    'Errors limit (%d) reached. Please fix errors and re-upload file',
                                    $import->getErrorsLimit()
                                )
                            );
                        } else {
                            if ($import->isImportAllowed()) {
                                $resultBlock->addNotice(
                                    $this->__('Please fix errors and re-upload file or simply press "Import" button to skip rows with errors'),
                                    true
                                );
                            } else {
                                $resultBlock->addNotice(
                                    $this->__('File is partially valid, but import is not possible'), false
                                );
                            }
                        }
                        // errors info
                        foreach ($import->getErrors() as $errorCode => $rows) {
                            $error = $errorCode . ' ' . $this->__('in rows:') . ' ' . implode(', ', $rows);
                            $resultBlock->addError($error);
                        }
                    } else {
                        if ($import->isImportAllowed()) {
                            $resultBlock->addSuccess(
                                $this->__('File is valid! To start import process press "Import" button'), true
                            );
                        } else {
                            $resultBlock->addError(
                                $this->__('File is valid, but import is not possible'), false
                            );
                        }
                    }
                    $resultBlock->addNotice($import->getNotices());
                    $resultBlock->addNotice(
                        $this->__(
                            'Checked rows: %d, checked entities: %d, invalid rows: %d, total errors: %d',
                            $import->getProcessedRowsCount(), $import->getProcessedEntitiesCount(),
                            $import->getInvalidRowsCount(), $import->getErrorsCount()
                        )
                    );
                }
            } catch (Exception $e) {
                Mage::logException($e);
                $resultBlock->addNotice($this->__('Please fix errors and re-upload file'))
                    ->addError($e->getMessage());
            }
            $this->renderLayout();
        } elseif ($this->getRequest()->isPost() && empty($_FILES)) {
            $this->loadLayout(false);
            $resultBlock = $this->getLayout()->getBlock('import.frame.result');
            $resultBlock->addError($this->__('File was not uploaded'));
            $this->renderLayout();
        } else {
            $this->_getSession()->addError($this->__('Data is invalid or file is not uploaded'));
            $this->_redirect('*/*/index');
        }
    }

    /**
     * Start import process action.
     *
     * @return void
     */
    public function startImportAction()
    {
        $data = $this->getRequest()->getPost();
        if ($data) {
            $this->loadLayout(false);

//            /** @var $resultBlock Mage_ImportExport_Block_Adminhtml_Import_Frame_Result */
//            $resultBlock = $this->getLayout()->getBlock('import.frame.result');
            /** @var $importModel Mage_ImportExport_Model_Import */
            //$adapter = Mage::getModel('frans/importExport_import_entity_quote_address');
            /** @var $import GoSolid_Frans_Model_ImportExport_Import */
            $importModel = Mage::getModel('frans/importExport_import');
//                ->setEntityAdapter($adapter);
            $responseAjax = new Varien_Object();

            try {
                $importModel->importSource();
                $importModel->invalidateIndex();
//                $resultBlock->addAction('show', 'import_validation_container')
//                    ->addAction('innerHTML', 'import_validation_container_header', $this->__('Status'));
                $responseAjax->setData('success', true);
            } catch (Exception $e) {
                Mage::logException($e);
                $responseAjax->setData('success', false)
                            ->setMessage($e->getMessage());
                return;
            }
            $this->getResponse()->setBody($responseAjax->toJson());

        } else {
            $this->_redirect('*/*/index');
        }
    }


    public function validateCsvAction()
    {
        // steps - load all addresses
        // convert street into array
        // look up region_id from region code
        // figure out shipping method
        //

        $filePath = Mage::getBaseDir('var') . DS . 'importexport' . DS . 'corcoran_new.csv';
        Mage::log("File path: $filePath");

        $quoteImport = Mage::getModel('frans/importExport_import_quote', $filePath);

        var_dump($colNames = $quoteImport->getColNames());

        echo "did things.";

        $count = 0;

        $skuData = $quoteImport->getSkuData();

        var_dump($skuData);

//        while ($quoteImport->valid())
//        {
//            $row = $quoteImport->current();
//            var_dump($quoteImport->key(), $row);
//            $quoteImport->next();
//
//
//            if ($count > 1000)
//            {
//                break;
//            }
//        }
    }

    public function saveQuoteNoteAction(){
        $responseAjax = new Varien_Object();
        try {
            $request = $this->getRequest()->getParams();

            $quote = $this->_getSession()->getQuote();
            $quoteItems = $quote->getAllItems();
            foreach($quoteItems as $quoteItem)
            {
                if($quoteItem->getId() == $request['item_id'])
                {
                    $quoteItem->setNotes($request['item_note']);
                }

            }

            $quote->save();
            $responseAjax->setData('success',true);
        }
        catch (Mage_Core_Exception $e) {
            $responseAjax->setData('success',false)
                         ->setMessage($e->getMessage());

        }

        $this->getResponse()->setBody($responseAjax->toJson());
    }


    public function saveQuoteProgressAction(){

        $request = $this->getRequest()->getParams();
        $progressStepsDone = $request['progress'];

        $quoteId = $this->_getSession()->getQuote()->getId();
        if($quoteId) {
            $store = Mage::getSingleton('core/store')->load(1);
            $quote = Mage::getModel('sales/quote')->setStore($store)->load($quoteId);
            if($quote->getEntityId()){
                $quote->setData('progress', $progressStepsDone);
                $quote->save();
            }
        }
    }

    public function getQuoteProgressAction(){

        $quote = $this->_getSession()->getQuote();
        $progressStepsDone = $quote->getData('progress');
        echo $progressStepsDone;
        return true;
    }

}