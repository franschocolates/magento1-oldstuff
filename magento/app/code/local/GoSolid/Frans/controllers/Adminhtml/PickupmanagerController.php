<?php

class GoSolid_Frans_Adminhtml_PickupmanagerController extends Mage_Adminhtml_Controller_action
{
	protected function _initAction() {
		$this->loadLayout();
		
		//Optional
		$this->_setActiveMenu('retail');
		$this->_title($this->__('Retail'))->_title($this->__('Pick Up Manager'));
		
		return $this;
	}   
 
	public function indexAction() {

		$this->_initAction()
			->renderLayout();

	}

	public function editAction()
    {
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('sales/order')->load($id);

        $billingAddress = $model->getBillingAddress();
        $billingName = $billingAddress->getFirstname() . ' ' . $billingAddress->getLastname();

        $shippingAddress = $model->getShippingAddress();
        $shippingName = $shippingAddress->getFirstname() . ' ' . $shippingAddress->getLastname();

        $model->setBillingName($billingName);
        $model->setBillingCompany($billingAddress->getCompany());
        $model->setShippingName($shippingName);

		Mage::register('pickupmanager_data', $model);


		if ($model->getId() || $id == 0) {
				$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
				if (!empty($data)) {
					$model->setData($data);
				}

				$this->loadLayout();
				
				$this->_addContent($this->getLayout()->createBlock('frans/adminhtml_pickupmanager_edit'))
					->_addLeft($this->getLayout()->createBlock('frans/adminhtml_pickupmanager_edit_tabs'));
					
				$this->renderLayout();

			}
			else
			{
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('frans')->__('Record does not exist'));
				$this->_redirect('*/*/');
			}

	}
	
	
	public function newAction() {
		$this->_forward('edit');

	}
	
	public function saveAction() {
		$session = Mage::getSingleton('adminhtml/session');
		if ($data = $this->getRequest()->getPost()) {

            $id = $this->getRequest()->getParam('id');

            /** @var GoSolid_Frans_Model_Sales_Order $model */
			$model = Mage::getModel('sales/order')->load($id);

            $model->setStatus($data["status_label"]);

			try {

                if($data["status_label"] == "picked_up")
                {
                    // make sure it's in local
                    $today = Mage::getModel('core/date')->date('Y-m-d');
                    $model->setShipDate($today);

                    if (!$model->getIsMultishipChildOrder())
                    {
                        $model->setIsReadyForCapture(true);
                    }
                }
                elseif($data["status_label"] == "processing")
                {
                    $model->unsetData('ship_date');
                    $model->setIsReadyForCapture(false);
                }

				$model->save();

                //send the email if it is ready for pickup or has been picked up.
                if(in_array($data["status_label"], array("ready_for_pickup","picked_up")))
                {
                    $model->sendShipmentEmail(true, $comment = "", $batch = false );
                }

                $session->addSuccess(Mage::helper('frans')->__('Record was successfully saved'));
                $session->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
				
			}
			catch(Exception $e)
            {
                $session->addError($e->getMessage());
                $session->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
			}
			
		}
		
		//on save and edit
		 if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('page_id' => $model->getId(), '_current'=>true));
                    return;
                }

		//no agency to save.
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('frans')->__('Record not found.'));
        $this->_redirect('*/*/');
		
	}
    protected function _isAllowed(){
        return Mage::getSingleton('admin/session')->isAllowed('retail/pickupmanager');
    }
}