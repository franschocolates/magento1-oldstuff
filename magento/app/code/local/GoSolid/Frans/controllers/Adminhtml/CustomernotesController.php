<?php
class GoSolid_Frans_Adminhtml_CustomernotesController extends Mage_Adminhtml_Controller_action
{
    protected function _initAction() {
        $this->loadLayout();

        return $this;
    }

    public function saveAction() {

        // get the parameters to find the customer ID
        $customerId = $this->getRequest()->getParam("customer_id");
        $orderId = $this->getRequest()->getParam('order_id');
        $customerNotes = $this->getRequest()->getParam("customer_notes");

        if (!isset($customerNotes))
        {
            // nothing to do
            $jsonData = Mage::helper('core')->jsonEncode(array('status' => 'fail', 'error' => 'missing'));
            $this->getResponse()->setHeader('Content-type', 'application/json');
            $this->getResponse()->setBody($jsonData);
            return;
        }

        Mage::getModel('frans/customerNotes')->saveNotes($customerNotes, $customerId, $orderId);

        $jsonData = Mage::helper('core')->jsonEncode(array('status' => 'success'));
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody($jsonData);
        return;
    }

    private function _getCustomer($customerId)
    {
        $customer = Mage::getModel('customer/customer')->load($customerId);
        return $customer;
    }

    private function _getCustomerForOrder($order)
    {
        $customerId = $order->getCustomerId();

        if ($customerId)
        {
            // we need to make sure to load the referral code
            return Mage::getModel('customer/customer')->load($customerId, array('referral_code'));
        }

        return false;
    }
	protected function _isAllowed(){
        return Mage::getSingleton('admin/session')->isAllowed('sales/order');
    }
}