<?php

class GoSolid_Frans_Adminhtml_RetailstoresController extends Mage_Adminhtml_Controller_action
{
	protected function _initAction() {
		$this->loadLayout();
		
		//Optional
		//$this->_setActiveMenu('system/pstadmin');
		//$this->_title($this->__('frans'))->_title($this->__('PST Admin'))->_title($this->__('Event Type Management'));
		
		return $this;
	}   
 
	public function indexAction() {

		$this->_initAction()
			->renderLayout();

	}

	public function editAction() {

		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('frans/retailStore')->load($id);
		
		Mage::register('index_data', $model);
		
		if ($model->getId() || $id == 0) {
				$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
				if (!empty($data)) {
					$model->setData($data);
				}

				$this->loadLayout();
				
				//Optional for WYSIWYG must change loading blocks to XML
				//$this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
				//$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
				
				$this->_addContent($this->getLayout()->createBlock('frans/adminhtml_retailstore_edit'))
					->_addLeft($this->getLayout()->createBlock('frans/adminhtml_retailstore_edit_tabs'));
					
				$this->renderLayout();

			}
			else
			{
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('frans')->__('Record does not exist'));
				$this->_redirect('*/*/');
			}

	}
	
	
	public function newAction() {
		$this->_forward('edit');

	}
	
	public function saveAction() {
		$folder = Mage::getModel("frans/retailStore")->getMediaPath();

        $session = Mage::getSingleton('adminhtml/session');
		if ($data = $this->getRequest()->getPost()) {
			
			foreach ($_FILES as $key => $value)
			{
				if(isset($_FILES[$key]['name']) and (file_exists($_FILES[$key]['tmp_name']))) {
					//save the flyer images
					$path = Mage::getBaseDir('media') . DS . $folder . DS ;
					$uploader = new Varien_File_Uploader($key);
				    $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png')); // or pdf or anything
				    $uploader->setAllowRenameFiles(true);
				    $uploader->setFilesDispersion(false);

				    $saveArr = $uploader->save($path, $_FILES[$key]['name']);			   
				    $data[$key] = $folder . DS .  $saveArr['file'];
				}
				elseif (array_key_exists("delete", $data[$key]) && $data[$key]["delete"] == "1" )
				{
					$path = Mage::getBaseDir('media') . DS . $folder ;
					$path = $path . $data[$key]["value"] ; 
					unlink($path);  
					$data[$key] = null;
				}
				else
				{
					unset($data[$key]);
				}
			}

			$model = Mage::getModel('frans/retailStore');
			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));

			try {

				$model->save();

				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('frans')->__('Record was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
				
			}
			catch(Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
			}
			
		}
		
		//on save and edit
		 if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('page_id' => $model->getId(), '_current'=>true));
                    return;
                }
		
		
		//no agency to save.
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('frans')->__('Record not found.'));
        $this->_redirect('*/*/');
		
	}
	
	public function deleteAction()
	{
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('frans/retailStore')->load($id);
		
		$model->delete();
		$model->save();
		
		Mage::getSingleton('adminhtml/session')->addSuccess('Record was successfully deleted');
		Mage::getSingleton('adminhtml/session')->setFormData(false);

	
		$this->_redirect('*/*/index/');
		return;
	}

	
}