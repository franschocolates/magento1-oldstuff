<?php
/**
 * Created by goSolid.
 */
require_once 'Mage/Adminhtml/controllers/CustomerController.php';


class GoSolid_Frans_Adminhtml_CustomerController extends Mage_Adminhtml_CustomerController
{

    /**
     * This function is never called, and I am commenting it out
     * This is because currently all controller actions are After rather than Before
     * But we did need to override this action in order to prevent customer addresses
     * from being removed. I am putting this comment here in case people start trying to figure out
     * how it works. look for  adminhtml_customer_prepare_save event in config, and code in Customer/Observer.php
     */
//    public function saveAction()
//    {
//        // should really not remove addresses, use event instead
//        return parent::saveAction();
//    }

    /**
     * This is an ugly way to change the create functionality without really changing it
     * Just invoke the create, then fallback
     */
    public function saveAndCreateOrderAction()
    {
        $this->saveAction();

        $customer = Mage::registry('current_customer');

        // if the customer now has an ID, assume it worked and redirect back to creating an order
        if ($customer->getId())
        {
            $this->getResponse()->setRedirect($this->getUrl('*/sales_order_create/start', array('customer_id' => $customer->getId())));
        }
    }

    /**
     * Customer addresses grid
     *
     */
    public function addressesAction()
    {
        $this->_initCustomer();
        $this->loadLayout();
        $this->renderLayout();
    }

    public function addressDeleteAction()
    {
        $this->_initCustomer()
             ->_initCustomerAddress();

        $customerAddress = $this->_getCustomerAddress();

        if ($customerAddress->getId())
        {
            $customerAddress->delete();
        }

        // use layout for normal customer address stuff
        $this->loadLayout('adminhtml_customer_addresses')
            ->renderLayout();
    }

    public function addressAddAction()
    {
        $this->_initCustomer()
            ->_initCustomerAddress()
            ->loadLayout()
            ->renderLayout();
    }

    public function addressEditAction()
    {
        $this->_initCustomer()
             ->_initCustomerAddress()
             ->loadLayout()
             ->renderLayout();
    }

    public function addressSaveAction()
    {
        $this->_initCustomer()
            ->_initCustomerAddress();

        try
        {
            $customer = $this->_getCustomer();
            $data = $this->getRequest()->getParams();
            $addressForm = Mage::getModel('customer/form');
            $addressForm->setFormCode('adminhtml_customer_address')->ignoreInvisible(false);
            $address = $this->_getCustomerAddress();
            $formData = $addressForm->setEntity($address)
                ->extractData($this->getRequest());

            $errors = $addressForm->validateData($formData);
            if ($errors !== true) {
                foreach ($errors as $error) {
                    $this->_getSession()->addError($error);
                }
                $this->_getSession()->setCustomerData($data);
                $this->getResponse()->setRedirect($this->getUrl('*/customer/edit', array(
                        'id' => $customer->getId())
                ));
                return;
            }
            else
            {
                $addressForm->compactData($formData);
                if (!$address->getId())
                {
                    $address->setCustomer($customer);
                    $customer->addAddress($address);
                }

                $address->save();

                $defaultBilling = $this->getRequest()->getParam('default_billing', false);
                $defaultShipping = $this->getRequest()->getParam('default_shipping', false);

                // update it if its now default, or it was primary before
                if ($defaultBilling || $defaultShipping || $customer->isAddressPrimary($address))
                {
                    Mage::log("Should now be setting default something.");
                    if ($defaultBilling)
                    {
                        $customer->setDefaultBilling($address->getId());
                    }
                    else if ($customer->getDefaultBillingAddress() && $customer->getDefaultBillingAddress()->getId() == $address->getId())
                    {
                        $customer->unsetData('default_billing');
                    }

                    if ($defaultShipping)
                    {
                        $customer->setDefaultShipping($address->getId());
                    }
                    else if ($customer->getDefaultShippingAddress() && $customer->getDefaultShippingAddress()->getId() == $address->getId())
                    {
                        $customer->unsetData('default_shipping');
                    }

                    $customer->save();
                }
            }
        }
        catch (Exception $e)
        {
            Mage::logException($e);
            throw $e;
        }
    }

    public function xcartOrdersAction()
    {
        $this->_initCustomer();
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * @return Mage_Customer_Model_Address
     */
    protected function _getCustomerAddress()
    {
        return Mage::registry('current_customer_address');
    }

    /**
     * @return Mage_Customer_Model_Customer
     */
    protected function _getCustomer()
    {
        // this is set in our parent
        return Mage::registry('current_customer');
    }

    protected function _initCustomerAddress()
    {
        $customerAddress = false;

        if ($addressId = $this->getRequest()->getParam('address_id'))
        {
            /** @var Mage_Customer_Model_Customer $customer */
            $customer = Mage::registry('current_customer');
            // we use this to get it from the collection - could return false.
            $customerAddress = $customer->getAddressItemById($addressId);
        }

        if (!$customerAddress)
        {
            // default to empty
            $customerAddress = Mage::getModel('customer/address');
        }
        Mage::register('current_customer_address', $customerAddress);

        return $this;
    }

}