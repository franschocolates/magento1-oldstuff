<?php
class GoSolid_Frans_BusinessgiftingController extends Mage_Core_Controller_Front_Action
{
	
	const XML_PATH_EMAIL_RECIPIENT  = 'contacts/business_gifting/recipient_email';
    const XML_PATH_EMAIL_SENDER     = 'contacts/business_gifting/sender_email_identity';
    const XML_PATH_EMAIL_TEMPLATE   = 'contacts/business_gifting/email_template';
    
	protected function _initAction() {
		$this->loadLayout();
		$this->_initLayoutMessages('customer/session');
		return $this;
	}
	
	public function indexAction() {
		$this->_initAction();
		$this->renderLayout();
	}
	
	public function contactAction() {
		
		$post = $this->getRequest()->getPost();
        if ( $post ) {
            $translate = Mage::getSingleton('core/translate');
            /* @var $translate Mage_Core_Model_Translate */
            $translate->setTranslateInline(false);
            try {
                $postObject = new Varien_Object();
                $postObject->setData($post);

                $error = false;

                if (!Zend_Validate::is(trim($post['firstname']) , 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['lastname']) , 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['phone']), 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['subject']), 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['body']), 'NotEmpty')) {
                    $error = true;
                }

                if ($error) {
                    throw new Exception();
                }
                $mailTemplate = Mage::getModel('core/email_template');
                /* @var $mailTemplate Mage_Core_Model_Email_Template */
                $mailTemplate->setDesignConfig(array('area' => 'frontend'))
                    ->setReplyTo($post['email'])
                    ->sendTransactional(
                        Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE),
                        Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER),
                        Mage::getStoreConfig(self::XML_PATH_EMAIL_RECIPIENT),
                        null,
                        array('data' => $postObject)
                    );

                if (!$mailTemplate->getSentSuccess()) {
                    throw new Exception();
                }

	            $output = array(
	                success => true
	            );
				echo json_encode($output);
                return;
            } catch (Exception $e) {
                $output = array(
	                success => false
	            );
				echo json_encode($output);
                return;
            }

        }
		
	}
	
}