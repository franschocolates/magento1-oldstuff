<?php
/**
 * Created by goSolid.
 * Date: 2/11/15
 * Time: 10:15 AM
 */
require_once 'Mage/Newsletter/controllers/ManageController.php';

class GoSolid_Frans_Newsletter_ManageController extends Mage_Newsletter_ManageController {

	public function indexAction()
	{
		$this->loadLayout();
		$this->_initLayoutMessages('customer/session');
		$this->_initLayoutMessages('catalog/session');

		// add breadcrumbs
		$breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');
		if ($breadcrumbs) {
			$breadcrumbs->addCrumb('home', array(
				'label' => $this->__('Home'),
				'title' => $this->__('Go to Home Page'),
				'link'  => Mage::getBaseUrl()
			));
			$breadcrumbs->addCrumb('customer', array(
				'label' => $this->__("Your Account")
			));
		}

		if ($block = $this->getLayout()->getBlock('customer_newsletter')) {
			$block->setRefererUrl($this->_getRefererUrl());
		}
		$this->getLayout()->getBlock('head')->setTitle($this->__('Your Account'));
		$this->renderLayout();
	}

}