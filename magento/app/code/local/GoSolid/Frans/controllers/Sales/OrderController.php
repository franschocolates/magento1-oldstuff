<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Sales orders controller
 *
 * @category   Mage
 * @package    Mage_Sales
 * @author      Magento Core Team <core@magentocommerce.com>
 */

require_once 'Mage/Sales/controllers/OrderController.php';

class GoSolid_Frans_Sales_OrderController extends Mage_Sales_OrderController
{
	protected function _getSession()
    {
        return Mage::getSingleton('customer/session');
    }

    /**
     * Action predispatch
     *
     * Check customer authentication for some actions
     */

    public function preDispatch()
    {
        $action = $this->getRequest()->getActionName();
        // delegate to grandparent (not parent)
        // to run standard processing. Without this, the session values aren't initialized properly
        // we can't go to parent, because it will try and redirect
        Mage_Sales_Controller_Abstract::preDispatch();

        // now that our session works, we can check for the combination of our special action (printorder)
        // and a session variable indicating that's ok
        // if either of those is not true, then fall back to normal processing (copied from parent)
        if ($action != 'printorder' || !$this->_getSession()->getLastOrderId())
        {
            $loginUrl = Mage::helper('customer')->getLoginUrl();

            if (!Mage::getSingleton('customer/session')->authenticate($this, $loginUrl)) {
                $this->setFlag('', self::FLAG_NO_DISPATCH, true);
            }
            return $this;
        }

        // figure out why it complains about double sending headers
        return $this;
    }

	/**
	 * Customer order history
	 */
	public function historyAction()
	{
		$this->loadLayout();
		$this->_initLayoutMessages('catalog/session');

		$this->getLayout()->getBlock('head')->setTitle($this->__('Your Account'));

		// add breadcrumbs
		$breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');
		if ($breadcrumbs) {
			$breadcrumbs->addCrumb('home', array(
				'label' => $this->__('Home'),
				'title' => $this->__('Go to Home Page'),
				'link'  => Mage::getBaseUrl()
			));
			$breadcrumbs->addCrumb('customer', array(
				'label' => $this->__("Your Account")
			));
		}

		if ($block = $this->getLayout()->getBlock('customer.account.link.back')) {
			$block->setRefererUrl($this->_getRefererUrl());
		}
		$this->renderLayout();
	}

    public function vieworderAction()
	{
		$this->loadLayout();
        $this->renderLayout();
		
        $orderId = $this->getRequest()->getParam("order_id");
        
        //get the order.
        if($orderId){
			$order = Mage::getModel("sales/order")->load($orderId);
        } else {
        	$incrementId = $this->getRequest()->getParam("increment_id");
        	$order = Mage::getModel("sales/order")->loadByIncrementId($incrementId);
        }
		
		//make sure that this order belongs to this customer.
		if($this->_canViewOrder($order) )
		{
			$block = $this->getLayout()->createBlock('core/template')->setTemplate('sales/order/popup.phtml');
			$block->setOrder($order);
		
			$response = array(
       						'html'=>$block->toHtml(),
       						'status' => 'success'
       					);
		}
		else
		{
			$response = array(
       						'html'=> "<div class='modal-error-page'>" . $this->__("The order could not be found.") . "</div>",
       						'status' => 'error'
       					);
		}
		
		
        echo json_encode($response);
		die;
	
	}
	
	public function printorderAction()
	{

        $orderId = $this->getRequest()->getParam("order_id");
        
		//get the order.
		$order = Mage::getModel("sales/order")->load($orderId);
		
		//make sure that this order belongs to this customer.
		if($this->_canViewOrder($order) )
		{
            $this->loadLayout();
            $this->renderLayout();

			$block = $this->getLayout()->createBlock('core/template')->setTemplate('sales/order/popup.phtml')->setIsPrint(true);
			$block->setOrder($order);
		
			echo $block->toHtml();
            die;
		}
		else
		{
			Mage::getSingleton('core/session')->addError($this->__('The order can not be found.'));
            $this->_redirect('sales/order/history');
		}
		
	}
	
	
	public function sendorderemailAction()
	{

		$orderId = $this->getRequest()->getParam("order_id");
        
		//get the order.
		$order = Mage::getModel("sales/order")->load($orderId);

		if($order->getIsMultishipChildOrder()){
			//can't send emails for child orders, so grab the parent instead
			$order = $order->getMultishipParentOrder();
		}
		
		//make sure that this order belongs to this customer.
		if($this->_canViewOrder($order) )
		{

			$order->sendNewOrderEmail();

            Mage::getSingleton('core/session')->addSuccess($this->__('The order has been resent to the original order email address.'));
            
			$this->_redirect('sales/order/history');
            return;
		}
		else
		{
			//create an error.
			Mage::getSingleton('core/session')->addError($this->__('The order can not be found. No email sent.'));
            $this->_redirect('sales/order/history');
		}
		
		return;
		
	}

    /**
     * Determines whether they have permisssion to view an order
     * Overridden to allow us to show orders if they have an order ID in their session.
     *
     * @param Mage_Sales_Model_Order $order
     * @return bool
     */
    protected function _canViewOrder($order)
    {
        $availableStates = Mage::getSingleton('sales/order_config')->getVisibleOnFrontStates();
        // either use parent,
        // or if that fails, fall back to see if they have something in their session indicating
        // this was their last order
        return parent::_canViewOrder($order) ||
            (
                $order->getId() == $this->_getSession()->getLastOrderId()
                && in_array($order->getState(), $availableStates, $strict = true)
            );
    }
}
