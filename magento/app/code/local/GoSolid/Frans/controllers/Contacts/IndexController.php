<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Contacts
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Contacts index controller
 *
 * @category   Mage
 * @package    Mage_Contacts
 * @author      Magento Core Team <core@magentocommerce.com>
 */

require_once 'Mage/Contacts/controllers/IndexController.php';

class GoSolid_Frans_Contacts_IndexController extends Mage_Contacts_IndexController
{
    const HELPDESK_NEW_TICKET_PRIORITY          = 'helpdesk/general/default/priority';

    const XML_PATH_DEPARTMENT                   = 'contacts/department/default';
	const XML_PATH_DEPARTMENT_ORDERASSISTANCE	= 'contacts/department/orderassistance';
	const XML_PATH_DEPARTMENT_CATALOGREQUEST	= 'contacts/department/catalogrequest';
	const XML_PATH_DEPARTMENT_PRESSINQUIRIES	= 'contacts/department/pressinquiries';
	const XML_PATH_DEPARTMENT_DONATIONS		= 'contacts/department/donations';
	const XML_PATH_DEPARTMENT_JOBS				= 'contacts/department/jobs';
	const XML_PATH_DEPARTMENT_WHOLESALE		= 'contacts/department/wholesale';
	const XML_PATH_DEPARTMENT_INTERNATIONAL    = 'contacts/department/international';
	const XML_PATH_DEPARTMENT_OTHER			= 'contacts/department/other';
    const XML_PATH_DEPARTMENT_BUSINESSGIFTING	= 'contacts/department/businessgifting';

	const XML_PATH_AUTORESPONDER_TEMPLATE   = 'contacts/email/autoresponder_template';
    const AUTORESPONDER_SENDER   = 'general'; // corresponds to trans_email/ident_general/email

    // CMS static block identifiers
    const AUTORESPONDER_BLOCKID_ORDERASSISTANCE     = 'email_autoreply-order_assistance';
    const AUTORESPONDER_BLOCKID_CATALOGREQUEST      = 'email_autoreply-catalog_request';
    const AUTORESPONDER_BLOCKID_PRESSINQUIRIES      = 'email_autoreply-press_inquiries';
    const AUTORESPONDER_BLOCKID_DONATIONS           = 'email_autoreply-donations';
    const AUTORESPONDER_BLOCKID_JOBS                = 'email_autoreply-jobs';
    const AUTORESPONDER_BLOCKID_WHOLESALE           = 'email_autoreply-wholesale';
    const AUTORESPONDER_BLOCKID_OTHER               = 'email_autoreply-other';
    const AUTORESPONDER_BLOCKID_INTERNATIONAL       = 'email_autoreply-international_orders';

    
	protected function _initAction() {
		$this->loadLayout();
		$this->_initLayoutMessages('customer/session');
		return $this;
	}
	
	public function indexAction() {
		$this->_initAction();
		$this->renderLayout();
	}

	public function postAction()
    {
        $post = $this->getRequest()->getPost();
        if ( $post ) {
            $translate = Mage::getSingleton('core/translate');
            /* @var $translate Mage_Core_Model_Translate */
            $translate->setTranslateInline(false);
            try {
                $postObject = new Varien_Object();
                $postObject->setData($post);

                // different fields are required depending on the "action" post var
                $action = $post['action'];
                switch($action){
                	case 'catalog-request': // "Catalog Request" form submission
		                // validate all required fields for this action, throw exception if any fails
		                switch(false){
		                	case Zend_Validate::is(trim($post['firstname']), 'NotEmpty'):
		                	case Zend_Validate::is(trim($post['lastname']), 'NotEmpty'):
		                	case Zend_Validate::is(trim($post['address']), 'NotEmpty'):
		                	case Zend_Validate::is(trim($post['city']), 'NotEmpty'):
		                	case Zend_Validate::is(trim($post['region_id']), 'NotEmpty'):
		                	case Zend_Validate::is(trim($post['zip']), 'NotEmpty'):
		                		throw new Exception();
		                		break;
		                }
		                break;
	                case 'international-orders': // "International Orders" form submission
		                // validate all required fields for this action, throw exception if any fails
		                switch(false){
			                case Zend_Validate::is(trim($post['firstname']), 'NotEmpty'):
			                case Zend_Validate::is(trim($post['lastname']), 'NotEmpty'):
			                case Zend_Validate::is(trim($post['shippingaddress']), 'NotEmpty'):
			                case Zend_Validate::is(trim($post['phone']), 'NotEmpty'):
			                case Zend_Validate::is(trim($post['email']), 'NotEmpty'):
			                case Zend_Validate::is(trim($post['itemdetail']), 'NotEmpty'):
				                throw new Exception();
				                break;
		                }
		                break;
                    case 'business-gifting': // "Business Gifting" form submission
                        // validate all required fields for this action, throw exception if any fails

                        switch(false){
                            case Zend_Validate::is(trim($post['firstname']), 'NotEmpty'):
                            case Zend_Validate::is(trim($post['lastname']), 'NotEmpty'):
                            case Zend_Validate::is(trim($post['companyname']), 'NotEmpty'):
                            case Zend_Validate::is(trim($post['email']), 'NotEmpty'):
                            case Zend_Validate::is(trim($post['phone']), 'NotEmpty'):
                            case Zend_Validate::is(trim($post['subject']), 'NotEmpty'):
                            case Zend_Validate::is(trim($post['body']), 'NotEmpty'):
                                throw new Exception();
                                break;
                        }
                        break;
                	default: // "Order Assistance" and "Other" forms have the same required fields
                		// validate all required fields for this action, throw exception if any fails
		                switch(false){
		                	case Zend_Validate::is(trim($post['firstname']), 'NotEmpty'):
		                	case Zend_Validate::is(trim($post['lastname']), 'NotEmpty'):
		                	case Zend_Validate::is(trim($post['email']), 'NotEmpty'):
		                	case Zend_Validate::is(trim($post['comment']), 'NotEmpty'):
		                		throw new Exception();
		                		break;
		                }
		                break;
                }
                
                // each form will generate an email with a unique subject line, according to this array
                $subjectLines = array(
                	'order-assistance'	    => Mage::helper('frans')->__('Order Assistance Request'),
                	'catalog-request'	    => Mage::helper('frans')->__('Catalog Request'),
                	'press-inquiries'	    => Mage::helper('frans')->__('Press Inquiry'),
                	'donations'			    => Mage::helper('frans')->__('Donation Inquiry'),
                	'jobs'				    => Mage::helper('frans')->__('Employment Inquiry'),
                	'wholesale'			    => Mage::helper('frans')->__('Wholesale Inquiry'),
	                'international-orders'  => Mage::helper('frans')->__('International Order'),
                    'business-gifting'      => Mage::helper('frans')->__('Business Gifting'),
                	'other'				    => Mage::helper('frans')->__('General Inquiry')
                );
                $postObject->setSubject($subjectLines[$action]);


                $messageContent = array();
                $content = '';
                // determine email recipient based on which form was sent
                $dept = Mage::getStoreConfig(self::XML_PATH_DEPARTMENT); // start with the default email address
                switch($action) {
                    case 'order-assistance':

                        $dept = Mage::getStoreConfig(self::XML_PATH_DEPARTMENT_ORDERASSISTANCE);

                        if(Zend_Validate::is(trim($post['ordernumber']), 'NotEmpty')){
                            $content .= 'Order #' . $post['ordernumber'] . PHP_EOL;
                        }
                        if(Zend_Validate::is(trim($post['companyname']), 'NotEmpty')){
                            $content .= 'Business Name: ' . $post['companyname'] . PHP_EOL;
                        }
                        if(Zend_Validate::is(trim($post['phone']), 'NotEmpty')){
                            $content .= 'Phone #: ' . $post['phone'] . PHP_EOL . PHP_EOL;
                        }
                        $content .= $post['comment'] . PHP_EOL;

                        $block = Mage::getModel('cms/block')->load(self::AUTORESPONDER_BLOCKID_ORDERASSISTANCE);
                        $messageContent['title'] = $block->getTitle();
                        $messageContent['content'] = $block->getContent();
                		break;
                	case 'catalog-request':
                        $dept = Mage::getStoreConfig(self::XML_PATH_DEPARTMENT_CATALOGREQUEST);

                        if(Zend_Validate::is(trim($post['companyname']), 'NotEmpty')){
                            $content .= 'Business Name: ' . $post['companyname'] . PHP_EOL;
                        }
                        if(Zend_Validate::is(trim($post['phone']), 'NotEmpty')){
                            $content .= 'Phone #: ' . $post['phone'] . PHP_EOL;
                        }
                        $content .= $post['address'] . PHP_EOL;
                        if(Zend_Validate::is(trim($post['address2']), 'NotEmpty')){
                            $content .= $post['address2'] . PHP_EOL;
                        }
                        $content .= $post['city'] . ', ';
                        $content .= Mage::getModel('directory/region')->load($post['region_id'])->getName() . ' ';
                        $content .= $post['zip'];

                        $block = Mage::getModel('cms/block')->load(self::AUTORESPONDER_BLOCKID_CATALOGREQUEST);
                        $messageContent['title'] = $block->getTitle();
                        $messageContent['content'] = $block->getContent();
                		break;
                	case 'press-inquiries':
                        $dept = Mage::getStoreConfig(self::XML_PATH_DEPARTMENT_PRESSINQUIRIES);

                        if(Zend_Validate::is(trim($post['companyname']), 'NotEmpty')){
                            $content .= 'Business Name: ' . $post['companyname'] . PHP_EOL;
                        }
                        if(Zend_Validate::is(trim($post['phone']), 'NotEmpty')){
                            $content .= 'Phone #: ' . $post['phone'] . PHP_EOL . PHP_EOL;
                        }
                        $content .= $post['comment'] . PHP_EOL;

                        $block = Mage::getModel('cms/block')->load(self::AUTORESPONDER_BLOCKID_PRESSINQUIRIES);
                        $messageContent['title'] = $block->getTitle();
                        $messageContent['content'] = $block->getContent();
                		break;
                	case 'donations':
                        $dept = Mage::getStoreConfig(self::XML_PATH_DEPARTMENT_DONATIONS);

                        if(Zend_Validate::is(trim($post['companyname']), 'NotEmpty')){
                            $content .= 'Business Name: ' . $post['companyname'] . PHP_EOL;
                        }
                        if(Zend_Validate::is(trim($post['phone']), 'NotEmpty')){
                            $content .= 'Phone #: ' . $post['phone'] . PHP_EOL . PHP_EOL;
                        }
                        $content .= $post['comment'] . PHP_EOL;

                        $block = Mage::getModel('cms/block')->load(self::AUTORESPONDER_BLOCKID_DONATIONS);
                        $messageContent['title'] = $block->getTitle();
                        $messageContent['content'] = $block->getContent();
                		break;
                	case 'jobs':
                        $dept = Mage::getStoreConfig(self::XML_PATH_DEPARTMENT_JOBS);

                        if(Zend_Validate::is(trim($post['companyname']), 'NotEmpty')){
                            $content .= 'Business Name: ' . $post['companyname'] . PHP_EOL;
                        }
                        if(Zend_Validate::is(trim($post['phone']), 'NotEmpty')){
                            $content .= 'Phone #: ' . $post['phone'] . PHP_EOL . PHP_EOL;
                        }
                        $content .= $post['comment'] . PHP_EOL;

                        $block = Mage::getModel('cms/block')->load(self::AUTORESPONDER_BLOCKID_JOBS);
                        $messageContent['title'] = $block->getTitle();
                        $messageContent['content'] = $block->getContent();
                		break;
	                case 'wholesale':
                        $dept = Mage::getStoreConfig(self::XML_PATH_DEPARTMENT_WHOLESALE);

                        if(Zend_Validate::is(trim($post['companyname']), 'NotEmpty')){
                            $content .= 'Business Name: ' . $post['companyname'] . PHP_EOL;
                        }
                        if(Zend_Validate::is(trim($post['phone']), 'NotEmpty')){
                            $content .= 'Phone #: ' . $post['phone'] . PHP_EOL . PHP_EOL;
                        }
                        $content .= $post['comment'] . PHP_EOL;

                        $block = Mage::getModel('cms/block')->load(self::AUTORESPONDER_BLOCKID_WHOLESALE);
                        $messageContent['title'] = $block->getTitle();
                        $messageContent['content'] = $block->getContent();
		                break;
	                case 'international-orders':
                        $dept = Mage::getStoreConfig(self::XML_PATH_DEPARTMENT_INTERNATIONAL);
                        $content .= 'Phone #: ' . $post['phone'] . PHP_EOL;
                        $content .= 'Shipping Address:' . PHP_EOL . PHP_EOL . $post['shippingaddress'] . PHP_EOL . PHP_EOL;
                        $content .= 'Item Detail:' . PHP_EOL . PHP_EOL . $post['itemdetail'];

                        $block = Mage::getModel('cms/block')->load(self::AUTORESPONDER_BLOCKID_INTERNATIONAL);
                        $messageContent['title'] = $block->getTitle();
                        $messageContent['content'] = $block->getContent();
		                break;
                    case 'business-gifting':

                        $dept = Mage::getStoreConfig(self::XML_PATH_DEPARTMENT_BUSINESSGIFTING);
                        $content .= 'Business Name: ' . $post['companyname'] . PHP_EOL;
                        $content .= 'Phone #: ' . $post['phone'] . PHP_EOL;
                        $content .= 'Subject: ' .  $post['subject']. PHP_EOL. PHP_EOL;
                        $content .= 'Message:' . PHP_EOL . PHP_EOL . $post['body'];

                        $block = Mage::getModel('cms/block')->load(self::AUTORESPONDER_BLOCKID_OTHER);
                        $messageContent['title'] = $block->getTitle();
                        $messageContent['content'] = $block->getContent();
                        break;
                	case 'other':
                        $dept = Mage::getStoreConfig(self::XML_PATH_DEPARTMENT_OTHER);

                        if(Zend_Validate::is(trim($post['companyname']), 'NotEmpty')){
                            $content .= 'Business Name: ' . $post['companyname'] . PHP_EOL;
                        }
                        if(Zend_Validate::is(trim($post['phone']), 'NotEmpty')){
                            $content .= 'Phone #: ' . $post['phone'] . PHP_EOL . PHP_EOL;
                        }
                        $content .= $post['comment'] . PHP_EOL;

                        $block = Mage::getModel('cms/block')->load(self::AUTORESPONDER_BLOCKID_OTHER);
                        $messageContent['title'] = $block->getTitle();
                        $messageContent['content'] = $block->getContent();
                		break;
                }


                $ticketPost = array(
                    'customer_email' => $post['email'],
                    'customer_name' => $post['firstname'] . ' ' . $post['lastname'],
                    'name' => 'Contact Form:' . $subjectLines[$action], //this is the name of the ticket
                    'priority_id' =>Mage::getStoreConfig(self::HELPDESK_NEW_TICKET_PRIORITY),
                    'department_id' => $dept,
                    'message' => $content
                );

                $ticketProcess =  Mage::helper('helpdesk/process');

                $ticketProcess->createFromPost($ticketPost, Mirasvit_Helpdesk_Model_Config::CHANNEL_CONTACT_FORM);

            } catch (Exception $e) {
                Mage::logException($e);
                $output = array( 'success' => false );
				echo json_encode($output);
                return;
            }
            
            $output = array( 'success' => true );
			echo json_encode($output);
            return;

        } else {
            $this->_redirect('contact');
        }
    }

}
