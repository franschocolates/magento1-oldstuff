<?php
class GoSolid_Frans_BusinessaccountsController extends Mage_Core_Controller_Front_Action
{
    
	protected function _initAction() {
		$this->loadLayout();

		// add breadcrumbs
		$breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');
		if ($breadcrumbs) {

			$breadcrumbs->addCrumb('home', array(
				'label' => $this->__('Home'),
				'title' => $this->__('Go to Home Page'),
				'link'  => Mage::getBaseUrl()
			));
			$breadcrumbs->addCrumb('business-accounts', array(
				'label' => $this->__('Business Accounts')
			));

		}

		$this->_initLayoutMessages('customer/session');
		return $this;
	}
	
	public function indexAction() {
		$this->_initAction();
		$this->renderLayout();
	}
	
}