<?php
class GoSolid_Frans_StyleguideController extends Mage_Core_Controller_Front_Action
{
    
	protected function _initAction() {
		$this->loadLayout();
		$this->_initLayoutMessages('customer/session');
		return $this;
	}
	
	public function indexAction() {
		$this->_initAction();
		$this->renderLayout();
	}
	
}