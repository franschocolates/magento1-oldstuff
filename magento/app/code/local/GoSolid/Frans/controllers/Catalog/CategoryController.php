<?php
/**
 * Created by goSolid.
 * Date: 8/18/15
 * Time: 3:58 PM
 */

require_once 'Mage/Catalog/controllers/CategoryController.php';

class GoSolid_Frans_Catalog_CategoryController extends Mage_Catalog_CategoryController {

	public function viewAction()
	{

		if($category = $this->_initCatagory()) {

			if($categoryRedirectUrl = $category->getData('category_redirect_url')){
				Mage::app()->getResponse()
					->setRedirect($categoryRedirectUrl, 301)
					->sendResponse();
			}

			if($category->getProductCount() === 1)
			{
				$product = $category->getProductCollection()->getFirstItem();
				$this->_redirect($product->getProductPath());
			}
			else
			{
				$design = Mage::getSingleton('catalog/design');
				$settings = $design->getDesignSettings($category);

				// apply custom design
				if ($settings->getCustomDesign()) {
					$design->applyCustomDesign($settings->getCustomDesign());
				}

				Mage::getSingleton('catalog/session')->setLastViewedCategoryId($category->getId());

				$update = $this->getLayout()->getUpdate();
				$update->addHandle('default');

				if (!$category->hasChildren()) {
					$update->addHandle('catalog_category_layered_nochildren');
				}

				$this->addActionLayoutHandles();
				$update->addHandle($category->getLayoutUpdateHandle());
				$update->addHandle('CATEGORY_' . $category->getId());
				$this->loadLayoutUpdates();

				// apply custom layout update once layout is loaded
				if ($layoutUpdates = $settings->getLayoutUpdates()) {
					if (is_array($layoutUpdates)) {
						foreach($layoutUpdates as $layoutUpdate) {
							$update->addUpdate($layoutUpdate);
						}
					}
				}

				$this->generateLayoutXml()->generateLayoutBlocks();
				// apply custom layout (page) template once the blocks are generated
				if ($settings->getPageLayout()) {
					$this->getLayout()->helper('page/layout')->applyTemplate($settings->getPageLayout());
				}

				if ($root = $this->getLayout()->getBlock('root')) {
					$root->addBodyClass('categorypath-' . $category->getUrlPath())
						->addBodyClass('category-' . $category->getUrlKey());
				}

				$this->_initLayoutMessages('catalog/session');
				$this->_initLayoutMessages('checkout/session');
				$this->renderLayout();
			}
		}
		elseif (!$this->getResponse()->isRedirect()) {
			$this->_forward('noRoute');
		}
	}

}