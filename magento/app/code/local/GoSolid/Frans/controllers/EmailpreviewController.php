<?php

class GoSolid_Frans_EmailpreviewController extends Mage_Core_Controller_Front_Action
{
	public function indexAction() {
		$params = $this->getRequest()->getParams();
		$layout = $this->loadLayout()->getLayout();
		$layout->getBlock('emailcontent')
			->setTemplate('emailpreview/bootstrap.phtml')
			->setEmailTemplate($params['template']);
		return $this->renderLayout();
	}
	
	
	
}