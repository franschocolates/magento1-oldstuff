<?php

require_once Mage::getModuleDir('controllers', 'GoSolid_Frans') . DS . "CheckoutController.php";

class GoSolid_Frans_Checkout_MultishipController extends GoSolid_Frans_CheckoutController
{
    private $_checkoutBlock = null;
    private $_addressBlock = null;
    private $_paymentStepBlock = null;

    /**
     * Validate ajax request and redirect on failure
     * Rewrite parent to change the multiship check
     *
     * @return bool
     */
    protected function _expireCheckout($ajax = true)
    {
        if (!$this->_getQuote()->hasItems()
            || $this->_getQuote()->getHasError()
            || !$this->_getQuote()->getIsMultiShipping()) {

                // if no items, or has errors, or is not multiship...
                $this->_getQuote()->setIsMultiShipping(false);

                // save the quote
                $this->_getQuote()->save();

                // if this request is not ajax...
                if (!$ajax){

                    // redirect back to the cart
                    $this->_redirect('checkout/cart');
                } else {

                    // session expired redirect
                    $this->_ajaxRedirectResponse();
                }
                return true;
            }
        return false;
    }

	public function indexAction() {
		
		$this->_initAction();
		$quote = $this->_getQuote();

		// if multishipping is not set, set multishipping to true
        // and save the quote
        if (!$quote->getIsMultiShipping())
        {
			$quote->setIsMultiShipping(true);
			$quote->save();
		}

        // now that multishipping is set, let's perform our other checks
        // to see if items are out of stock
        if ($this->_expireCheckout(false)) {
            return;
        }

        // this function passes a quote object to
        // to GoSolid_Frans_Model_Checkout_Type_Multiship::init(), which
        // calls GoSolid_Frans_Model_Sales_Quote::doMultishipAllocations()
        // on the passed quote, which gets/sets the quantities of each item
        // being sent to each address and returns them
        $this->_getMultishipCheckout($quote);
        $this->renderLayout();
    }


    protected function _getCheckoutBlock()
    {
        if ($this->_checkoutBlock != null)
        {
            return $this->_checkoutBlock;
        }
        return ($this->_checkoutBlock = $this->getLayout()
            ->createBlock('frans/checkout_multiship')
            ->setQuote($this->_getQuote()));
    }

    protected function _getEditAddressBlock()
    {
        if ($this->_addressBlock != null)
        {
            return $this->_addressBlock;
        }
        return ($this->_addressBlock = $this->getLayout()
            ->createBlock('frans/checkout_multiship')
            ->setQuote($this->_getQuote()));
    }

    /**
     * Used when we just need to get/refresh the shipping info action.
     */
    public function getShippingInfoAction()
    {
        if ($this->_expireCheckout()) {
            return;
        }

        $responseAjax = new Varien_Object();

        try
        {
            // if we don't collect totals, some of the multishipping info is confused.
            $this->_getQuote()->doMultishipAllocations(true);
            $this->_addShippingSectionsToResponse($this->_getQuote(), $responseAjax,
                array('toHtml' => 'shippingHtml'));
        } catch (Exception $e) {
            Mage::logException($e);

            # need to tell them it didn't work.
            $responseAjax->setError(true);
            $responseAjax->setMsg(array('Unable to refresh shipping info.'));
        }

        $this->getResponse()->setBody($responseAjax->toJson());
    }

    public function saveShippingInfoAction()
	{
        if ($this->_expireCheckout()) {
            return;
        }

        $responseAjax = new Varien_Object();

        try
        {
            $quote = $this->_getQuote();
            $multishipCheckout = $this->_getMultishipCheckout($quote);

            $shippingInfo = $this->getRequest()->getParam('shipping_info');

            $multishipCheckout->setShippingInfo($shippingInfo);
            $quote->save();

            $this->_addShippingSectionsToResponse(
                $quote, $responseAjax,
                array('getItemsHtml' => 'itemsHtml',
                    'getTotalsHtml' => 'totalsHtml')
            );
        } catch (Exception $e) {
            Mage::logException($e);

            # need to tell them it didn't work.
            $responseAjax->setError(true);
            $responseAjax->setMsg(array('Unable to update shipping info.'));
        }

        $this->getResponse()->setBody($responseAjax->toJson());
    }
	
	/**
	 * 
	 * Begin add item to cart action. Redirects to a URL based on configuration, and
	 * sets a param to indicate returning back to multiship when done.
	 */
	public function addItemAction()
	{
		$session = Mage::getSingleton('checkout/session');
		$session->setReturnToMultiship(true);
		
		$redirectPath = Mage::getStoreConfig('frans/checkout_options/add_item_url');
		$this->_redirect($redirectPath);
	}
	
	public function removeItemAction()
	{
        if ($this->_expireCheckout()) {
            return;
        }

        $responseAjax = new Varien_Object();
		$id = (int) $this->getRequest()->getParam('id');
        if ($id) {
            try {
                $this->_getCart()->removeItem($id)
                  ->save();

                $quote = $this->_getCart()->getQuote();
                $quote->doMultishipAllocations(true);

                $this->_addShippingSectionsToResponse(
                			$quote, $responseAjax, 
                				array('getItemsHtml' => 'itemsHtml',
                					  'getTotalsHtml' => 'totalsHtml',
                					  'getAddressItemsHtml' => 'addressItemsHtml',
                					  'getPagerHtml' => 'pagerHtml')
                		);
            } catch (Exception $e) {
                Mage::logException($e);
                
                # need to tell them it didn't work.
                $responseAjax->setError(true);
                $responseAjax->setMsg(array('Unable to remove item from bag.'));
            }
        }
        else {
        	$responseAjax->setError(true);
        	$responseAjax->setMsg(array('No item was provided to remove.'));
        }

		$this->getResponse()->setBody($responseAjax->toJson());
	}
	
	public function addShippingAddressAction()
	{
        if ($this->_expireCheckout()) {
            return;
        }

        $responseAjax = new Varien_Object();

        $responseAjax->setHtml(
        	$this->_getAddressBlock(false)->toHtml()
        );
        $responseAjax->setStatus('success');
        
		$this->getResponse()->setBody($responseAjax->toJson());
	}

	public function editShippingAddressAction()
	{
        if ($this->_expireCheckout()) {
            return;
        }


        $responseAjax = new Varien_Object();

        if ($id = $this->getRequest()->getParam('id'))
        {


        	$addressBlock = $this->_getAddressBlock($id, $this->getRequest()->getParam('delivery', false));
        	
        	$responseAjax->setHtml(
        		$addressBlock->toHtml()
        	);
        	$responseAjax->setStatus('success');
        
        }
        else 
        {
        	$responseAjax->setStatus('error');
        	$responseAjax->setMsg(array('No address was provided.'));
        }

		$this->getResponse()->setBody($responseAjax->toJson());
	}

    public function saveAddressPostAction()
    {
        if ($this->_expireCheckout()) {
            return;
        }

        $helper = Mage::helper('addressValidation');
        $responseAjax = new Varien_Object();

        try
        {
            if ($addressId = $this->getRequest()->getParam('id'))
            {


                $quote = $this->_getQuote();
                $multishipCheckout = $this->_getMultishipCheckout($quote);


				$data = $this->getRequest()->getParams();






                $addresses = $multishipCheckout->updateAddress($data, $addressId);

                $address = $addresses["quote_address"];

                $customerAddress = $addresses["customer_address"];

                $responseAjax->setStatus('success');
                $responseAjax->setData('editAddress', 'Yes');
                $addressBlock = $this->_getEditAddressBlock()->setIsFormSubmitted(true);




                if ($address)
                {


                    //address is validated and validation info is saved back to the address.
                    $validationResult = $helper->validateAddress($address);


                    //save the results back to the address for the next pass
                    $address->setIsValid($validationResult["is_valid"]);

                    //set a var to show proposed. Used by view.
                    $address->setSuggestAddress($validationResult["suggest_address"]);

                    $address->save();









                    if($validationResult["is_valid"] == false)
                    {
                        //set the response as though it is an invalid address.
                        if ( count($validationResult["invalid_fields"]) > 0 ) {

                            //setting response info and HTML to be reloaded into the page
                            $checkoutBlock = $this->_getCheckoutBlock()->setIsFormSubmitted(true);
                            $checkoutBlock->setAddressId($address->getId());


                            $responseAjax->setStatus('success');
                            $responseAjax->setData('invalidFields', "Yes");
                            $responseAjax->setData('score', $validationResult["score"]);
                            $responseAjax->setData('shippingHtml', $checkoutBlock->getEditAddressHtml($addressId, $this->getRequest()->getParam('delivery', false), $validationResult["invalid_fields"]));

                        }

                    }
                    else
                    {
                        // it is valid move on.

                        //save the quote address to a customer address.
                        $customerAddress = $address->convertToCustomerAddress();


                        $address->resetAddressValidation();
                        $address->setIsValid(true);

                        $address->save();


                        //set the response.
                        $responseAjax->setStatus('success');

                        $responseAjax->setData('score', $validationResult["score"]);


                        //we want to update the customer list now as well..
                        $responseAjax->setData('editUrl', Mage::getUrl('*/*/editShippingAddress', array("id" => $address->getId())));
                        $this->_addShippingSectionsToResponse(
                            $quote, $responseAjax,
                            array('getAddressesHtml' => 'addressesHtml',
                                'getAddressItemsHtml' => 'addressItemsHtml')
                        );


                    }


				}
				else
				{
		            $responseAjax->setStatus('error');

		            if (is_array($address))
		            {
		            	$responseAjax->setMsg(array($address['message']));
		            }
		            else
		            {
						$responseAjax->setMsg(array('Failed when saving address.'));
		            }
				}
	        }
	        else 
	        {
	        	$responseAjax->setStatus('error');
	        	$responseAjax->setMsg(array('No address was provided.'));
	        }



        } catch (Exception $e) {
            Mage::logException($e);

            # need to tell them it didn't work.
            $responseAjax->setStatus('error');
            $responseAjax->setMsg(array('Unable to save address.'));
        }




        $this->getResponse()->setBody($responseAjax->toJson());
    }



	public function addShippingAddressPostAction()
	{
        if ($this->_expireCheckout()) {
            return;
        }
        $helper = Mage::helper('addressValidation');
        $responseAjax = new Varien_Object();

		try {

            $quote = $this->_getQuote();
            $multishipCheckout = $this->_getMultishipCheckout($quote);
            $addressId = $this->getRequest()->getParam('address_id');
            $addressData = $this->getRequest()->getParams();

            $initialShippingAddresses = $quote->getAllShippingAddresses();
            if(count($initialShippingAddresses) === 1){
                $firstShippingAddress = $initialShippingAddresses[0];
                $street = $firstShippingAddress->getStreet()[0];
                $streetLength = strlen($street);
                if($streetLength === 0 || $streetLength === null){
                    $addressId = $firstShippingAddress->getAddressId();
                }
            }


            $address = null;

            if($addressId != null)
            {
                $address = Mage::getModel('sales/quote_address')->load($addressId);
            }
            else
            {
                $address = Mage::getModel('sales/quote_address');
            }



            //save the post data to the address


            //sometimes the address is an empty string and magento things there is already an address in the db.
            //this clears that up.
            if(array_key_exists("address_id", $addressData) &&  strlen(trim($addressData["address_id"])) == 0 ) {
                unset($addressData["address_id"]);
            }

            //convert street[] to a string with a line break.
            $addressData["street"] = $addressData["street"][0] . "\r\n" .  $addressData["street"][1];


            $address->addData($addressData);
            $address->setAddressType("shipping");
            $address->setQuoteId($quote->getId());
            $address->save();



            //if it is new, save it to the quote regardless of validity.
            if($quote->getAddressById($address->getId()) == false )
            {
                $res = $quote->addAddress($address);

            }



            //address is validated and validation info is saved back to the address.
            $validationResult = $helper->validateAddress($address);


            //save the results back to the address for the next pass
            $address->setIsValid($validationResult["is_valid"]);

            //set a var to show proposed. Used by view.
            $address->setSuggestAddress($validationResult["suggest_address"]);

            $address->save();



            //this is to make sure that the quote has the latest address in it for the ajax response.
            $quote =  Mage::getModel('sales/quote')->load($quote->getId());




            if($validationResult["is_valid"] == false)
            {
                //set the response as though it is an invalid address.
                if ( count($validationResult["invalid_fields"]) > 0 ) {



                    //setting response info and HTML to be reloaded into the page
                    $checkoutBlock = $this->_getCheckoutBlock()->setIsFormSubmitted(true);
                    $checkoutBlock->setAddressId($address->getId());


                    $responseAjax->setStatus('success');
                    $responseAjax->setData('invalidFields', "Yes");
                    $responseAjax->setData('score', $validationResult["score"]);
                    $responseAjax->setData('shippingHtml', $checkoutBlock->getShippingModalHtml($validationResult["invalid_fields"]));

                }

            }
            else
            {
                // it is valid move on.

                //save the quote address to a customer address.
                $customerAddress = $address->convertToCustomerAddress();


                $address->resetAddressValidation();
                $address->setIsValid(true);
                $address->save();


                //set the response.
                $responseAjax->setStatus('success');

                $responseAjax->setData('score', $validationResult["score"]);


                //we want to update the customer list now as well..
                $responseAjax->setData('editUrl', Mage::getUrl('*/*/editShippingAddress', array("id" => $address->getId())));
                $this->_addShippingSectionsToResponse(
                    $quote, $responseAjax,
                    array('getAddressesHtml' => 'addressesHtml',
                        'getAddressItemsHtml' => 'addressItemsHtml')
                );


            }


		} catch (Exception $e) {
        	Mage::logException($e);



			# need to tell them it didn't work.
	        $responseAjax->setStatus('error');
        	$responseAjax->setMsg(array('Unable to add address.' . $e->getMessage() ));
		}

		$this->getResponse()->setBody($responseAjax->toJson());

	}



    public function addChosenMultishipAddressAction(){
        if ($this->_expireCheckout()) {
            return;
        }

        $responseAjax = new Varien_Object();
        $addressData = $this->getRequest()->getPost();

        $newCustomerAddressId = $addressData['new_customer_address_id'];


        $addressId = $addressData['address_id'];
        $addressChoice = $addressData['address_choice'];
        $address = Mage::getModel('sales/quote_address')->load($addressId);

        if($address->getCustomerAddressId()){
            $customerAddressId = $address->getCustomerAddressId();
            $customerAddress = Mage::getModel('customer/address')->load($customerAddressId);
        }

        $quote = $this->_getQuote();

        if($addressChoice === 'suggested') {
            $address->setStreet($addressData['address']['suggested']['street1'] . PHP_EOL . $addressData['address']['suggested']['street2']);
            $address->setPostcode($addressData['address']['suggested']['postcode']);
            $address->setCity($addressData['address']['suggested']['city']);
            $address->setRegionId($addressData['address']['suggested']['region_id']);
            $address->setCountyId($addressData['address']['suggested']['country_id']);

            if($customerAddress) {
                $customerAddress->setStreet($addressData['address']['suggested']['street1'] . PHP_EOL . $addressData['address']['suggested']['street2']);
                $customerAddress->setPostcode($addressData['address']['suggested']['postcode']);
                $customerAddress->setCity($addressData['address']['suggested']['city']);
                $customerAddress->setRegionId($addressData['address']['suggested']['region_id']);
                $customerAddress->setCountyId($addressData['address']['suggested']['country_id']);
            }

        } else {
            $address->setStreet($addressData['address']['original']['street']);
            $address->setPostcode($addressData['address']['original']['postcode']);
            $address->setCity($addressData['address']['original']['city']);
            $address->setRegionId($addressData['address']['original']['region_id']);
            $address->setCountyId($addressData['address']['original']['country_id']);

            if($customerAddress) {
                $customerAddress->setStreet($addressData['address']['original']['street']);
                $customerAddress->setPostcode($addressData['address']['original']['postcode']);
                $customerAddress->setCity($addressData['address']['original']['city']);
                $customerAddress->setRegionId($addressData['address']['original']['region_id']);
                $customerAddress->setCountyId($addressData['address']['original']['country_id']);
            }
        }

        //make this a customer address.
        $customerAddress = $address->convertToCustomerAddress();

        $address->resetAddressValidation();


        $address->save();


        if($newCustomerAddressId){
            //update customer address
            $custAddress   = Mage::getModel('customer/address')->load($newCustomerAddressId);
            $custAddress->setStreet($address->getStreet());
            $custAddress->setPostcode($address->getPostcode());
            $custAddress->setCity($address->getCity());
            $custAddress->setRegionId($address->getRegionId());
            $custAddress->setCountyId($address->getCountyId());
            $custAddress->save();
        }

        //setting response info, edit URL, and HTML to be reloaded into the page
        $responseAjax->setData('editUrl', Mage::getUrl('*/*/editShippingAddress', array("id" => $address->getId())));
        $this->_addShippingSectionsToResponse(
            $quote, $responseAjax,
            array('getAddressesHtml' => 'addressesHtml',
                'getAddressItemsHtml' => 'addressItemsHtml')
        );

        $this->getResponse()->setBody($responseAjax->toJson());


    }

	public function removeShippingAddressAction()
	{
        if ($this->_expireCheckout()) {
            return;
        }

        $responseAjax = new Varien_Object();
		
        if ($id = $this->getRequest()->getParam('id'))
        {
       		$checkout = $this->_getMultishipCheckout($this->_getQuote());
        	if  ($checkout->removeShippingAddress($id))
       		{
       			$this->_getQuote()->save();
				$this->_addShippingSectionsToResponse(
                			$this->_getQuote(), $responseAjax, 
                				array('getAddressesHtml' => 'addressesHtml',
                					  'getAddressItemsHtml' => 'addressItemsHtml',
                					  'getTotalsHtml' => 'totalsHtml',
                				)
                		);
       		}
       		else 
       		{
       			$responseAjax->setStatus('error');
       			$responseAjax->setMsg(array('Unable to locate address to remove'));
       		}
        }
        else 
        {
        	$responseAjax->setStatus('error');
        	$responseAjax->setMsg(array('Address not found.'));
        }

		$this->getResponse()->setBody($responseAjax->toJson());
	}
	
	/**
	 * 
	 * Validates the shipping info and proceeds to next step if valid
	 */
	public function validateShippingAction()
	{
        if ($this->_expireCheckout()) {
            return;
        }

        $responseAjax = new Varien_Object();

        // do validation logic
        $checkout = $this->_getMultishipCheckout($this->_getQuote());

        $valid = $checkout->validateShippingData();

        if ($valid === true)
        {
            $quote = $this->_getQuote();
            foreach ($quote->getAllShippingAddresses() as $shippingAddress)
            {
                $shippingAddress->setCollectShippingRates(true);
            }

            $quote->collectTotals()->save();

        	// build the response delivery output
        	$responseAjax->setData('deliveryHtml',
        		$this->_getDeliveryBlock()->toHtml()
        	);
        	$responseAjax->setStatus('success');
        }
        else if (is_array($valid))
        {
            $responseAjax->setStatus('error');
            $responseAjax->setData('invalid_addresses', $valid);

        }
	
		$this->getResponse()->setBody($responseAjax->toJson());
	}

    /**
     *
     * Validates the delivery info and proceeds to next step if valid
     */
    public function validateDeliveryAction()
    {
        if ($this->_expireCheckout()) {
            return;
        }

        $responseAjax = new Varien_Object();

        // do validation logic
        $quote = $this->_getQuote();
        $addressData = $this->getRequest()->getParam('addresses');

        $checkout = $this->_getMultishipCheckout($quote);

        $result = $checkout->setDeliveryInfo($addressData);
        if (!is_array($result))
        {
            // build the response delivery output
            $responseAjax->setData('paymentHtml',
                $this->_getPaymentStepBlock()->toHtml()
            );
            $responseAjax->setStatus('success');
        }
        else
        {
            $responseAjax->setStatus('failure');
            $responseAjax->setMessage('Delivery info was not complete or was invalid.');
            $responseAjax->setAddressIds($result);
        }

        $this->getResponse()->setBody($responseAjax->toJson());
    }

    public function saveOrderAction()
    {
        if ($this->_expireCheckout()) {
            return;
        }

        $responseAjax = new Varien_Object();

        $billingData = $this->getRequest()->getParam('billing');
        $paymentData = $this->getRequest()->getParam('payment');

        Mage::log("Billing data: " . print_r($billingData, true));
        if ($billingData && $paymentData)
        {
            try
            {
                $checkout = $this->_getMultishipCheckout($this->_getQuote());

                $result = $checkout->saveOrder($billingData, $paymentData);
                if ($result && !is_array($result))
                {
                    // succeeded, pretend we worked
                    $responseAjax->setStatus('success');
                }

            }
            catch (Exception $e)
            {
                Mage::logException($e);

                $responseAjax->setStatus('failure');
                $responseAjax->setMessage($e->getMessage());
            }
        }
        else
        {
            $responseAjax->setStatus('failure');
            $responseAjax->setMessage('Please specify billing and payment info.');
        }

        $this->getResponse()->setBody($responseAjax->toJson());
    }

    protected function _getAddressBlock($addressId = false, $delivery = false)
    {
        if ($addressId && ($quoteAddress = $this->_getQuote()->getAddressById($addressId)))
        {
            $address = $quoteAddress;
        }
        else
        {
            $address = Mage::getModel('sales/quote_address');
        }

        return $this->getLayout()
            ->createBlock('frans/sales_quote_address_edit', '', array($address))
            ->setDelivery($delivery)
            ->setTemplate('customer/address/edit.phtml')
            ->setIsTemplateAddress(false);
    }
    /**
	 * 
	 * Builds a delivery block
	 * @return GoSolid_Frans_Block_Checkout_Multiship_Delivery
	 */
	protected function _getDeliveryBlock()
	{
		return $this->getLayout()
					->createBlock('frans/checkout_multiship_delivery')
					->setTemplate('checkout/multiship/delivery.phtml')
					->setQuote($this->_getQuote());
	}
	
	protected function _getShippingBlock($quote)
	{
		return $this->getLayout()->createBlock('frans/checkout_multiship_shipping')
                                ->setTemplate('checkout/multiship/shipping.phtml')
								->setQuote($quote);
	}

    protected function _getPaymentStepBlockType()
    {
        return 'frans/checkout_multiship_payment';
    }

    protected function _getSummaryHtml($showShipping = false, $showDelivery = false)
    {
        return $this->_getPaymentStepBlock()->getSummaryHtml($showShipping, $showDelivery);
    }

    protected function _buildReturnArray($quote)
	{
		$array = parent::_buildReturnArray($quote);
		
		$array['success'] = true;
		$array['totalsHtml'] = $this->getLayout()
									->createBlock('frans/checkout_multiship_shipping')
									->setQuote($quote)
									->getTotalsHtml();
									
		return $array;
	}


    /**
     * Overridden to try and cache the result
     */
    protected function _getPaymentStepBlock()
    {
        if ($this->_paymentStepBlock != null)
        {
            return $this->_paymentStepBlock;
        }

        return ($this->_paymentStepBlock = parent::_getPaymentStepBlock());
    }

	/**
	 * 
	 * @param	Mage_Sales_Model_Quote	$quote
	 * @param	Varien_Object			$responseAjax
	 * @params	array					$sections
	 */
	protected function _addShippingSectionsToResponse($quote, $responseAjax, $sections)
	{
		$responseAjax->setStatus('success');
		
		$shippingBlock = $this->_getShippingBlock($quote);
		
		foreach ($sections as $getter => $responseFieldName)
		{
			$responseAjax->setData($responseFieldName, $shippingBlock->$getter());
		}

	}

    protected function _handleDeliveryAddressInResponse($address, $responseAjax)
    {
        // how much the address changed dictates how much we need to do
        if ($address->getLocationHasChanged())
        {
            // location change means that the actual shipping info (e.g. city/street/etc) changed enough
            // that we had to revalidate the address. we need to recalc shipping
            $address->setCollectShippingRates(true);
            $address->getQuote()->collectTotals()->save();

            // build the response delivery output
            $responseAjax->setRefreshDelivery(1);
            $responseAjax->setData('deliveryHtml',
                $this->_getDeliveryBlock()->toHtml()
            );
        }
        else
        {
            $responseAjax->setAddressId($address->getId());
            $responseAjax->setAddressHtml(
                $this->_getDeliveryBlock()->getAddressHtml($address)
            );
        }

        $responseAjax->setStatus('success');
    }
}