<?php

require_once Mage::getModuleDir('controllers', 'GoSolid_Frans') . DS . "CheckoutController.php";

class GoSolid_Frans_Checkout_CalendarController extends GoSolid_Frans_CheckoutController
{
	protected function _initAction() {
		$this->loadLayout();
		return $this;
	}   
 
	public function indexAction() {
		
		$this->_initAction();
		$this->renderLayout();
	}
	
	public function updateCalendarAction()
	{
		$params = $this->getRequest()->getParams();
		
		// no matter what, we need the calendar block
		$calendarBlock = $this
							->getLayout()
							->createBlock('frans/checkout_calendar')
							->setTemplate('checkout/calendar.phtml');
		
		/* var $quote GoSolid_Frans_Model_Sales_Quote */
		$quote = Mage::getModel('checkout/cart')->getQuote();
							
		if (isset($params['address_id']))
		{
			Mage::log('Getting address getAvailableArrivalDatesAndMethods', Zend_Log::DEBUG, 'SaturdayShipping.log');
			$addressId = $params['address_id'];

			$address = $quote->getAddressById($addressId);
			
			if ($address && $address->getId())
			{

				$arrivalDatesAndMethods = $address->getAvailableArrivalDatesAndMethods();
			}
			else
			{
				// TODO: what if not attached to this quote?
			}
		}
		else
		{
			Mage::log('Getting quote getAvailableArrivalDatesAndMethods', Zend_Log::DEBUG, 'SaturdayShipping.log');
			$arrivalDatesAndMethods = $quote->getAvailableArrivalDatesAndMethods();
		}
		
		$responseArray = array();

		$calendarBlock->setArrivalDatesAndMethods($arrivalDatesAndMethods);
		
		$responseArray['data']['html'] = $calendarBlock->toHtml();
		$responseArray['data']['messages'] = $calendarBlock->getMessages();
		echo json_encode($responseArray);
		die;
	}
}