<?php

require_once 'Mage/Checkout/controllers/CartController.php';

/**
 * 
 * Rewrite of CartController to prevent cart from being displayed in the case of multiship
 * @author goSolid
 *
 */
class GoSolid_Frans_Checkout_CartController extends Mage_Checkout_CartController
{
	
	/**
	 * Overrides indexAction
	 * 	- in the case of multiship, do the basic logic, and then return to the multiship checkout page
	 *  - in non multiship, just do normal business.
	 * @see Mage_Checkout_CartController::indexAction()
	 */
	public function indexAction()
	{
        $cart = $this->_getCart();

        if ($cart->getQuote()->isLargerOrder() )
        {
        $message = Mage::getStoreConfig('frans/checkout_options/large_order_message');
        $cart->getCheckoutSession()->addNotice($message);
        }


        if (!$cart->getQuote()->getIsMultiShipping())
        {
        	return parent::indexAction();
        }

        // The following logic was all copied from the base CartController
        // so that we still pull notices for multiship
        if ($cart->getQuote()->getItemsCount()) {
            $cart->init();
            $cart->save();

            if (!$this->_getQuote()->validateMinimumAmount()) {
                $minimumAmount = Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())
                    ->toCurrency(Mage::getStoreConfig('sales/minimum_order/amount'));

                $warning = Mage::getStoreConfig('sales/minimum_order/description')
                    ? Mage::getStoreConfig('sales/minimum_order/description')
                    : Mage::helper('checkout')->__('Minimum order amount is %s', $minimumAmount);

                $cart->getCheckoutSession()->addNotice($warning);
            }
        }

        // Compose array of messages to add
        $messages = array();
        foreach ($cart->getQuote()->getMessages() as $message) {
            if ($message) {
                // Escape HTML entities in quote message to prevent XSS
                $message->setCode(Mage::helper('core')->escapeHtml($message->getCode()));
                $messages[] = $message;
            }
        }
        $cart->getCheckoutSession()->addUniqueMessages($messages);

        /**
         * if customer enteres shopping cart we should mark quote
         * as modified bc he can has checkout page in another window.
         */
        $this->_getSession()->setCartWasUpdated(true);
		
        $this->_goBack();
        
	}
	
	/**
	 * Overrides go back so that we always go back to multiship if we are a multiship quote
	 * @see Mage_Checkout_CartController::_goBack()
	 */
	protected function _goBack()
	{
		$quote = $this->_getQuote();
		
		if ($quote->getIsMultiShipping())
		{
			$this->getResponse()->setRedirect(Mage::getUrl('frans/checkout_multiship'));
			return $this;
		}
		else
		{
			return parent::_goBack();
		}
		
	}

    /**
     * Add product to shopping cart action
     *
     * Form key validation commented out by GoSolid
     *
     * @return void
     */
    public function addAction()
    {
//        if (!$this->_validateFormKey()) {
//            $this->_goBack();
//            return;
//        }
        $cart   = $this->_getCart();
        $params = $this->getRequest()->getParams();
        try {
            if (isset($params['qty'])) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $product = $this->_initProduct();
            $related = $this->getRequest()->getParam('related_product');

            /**
             * Check product availability
             */
            if (!$product) {
                $this->_goBack();
                return;
            }

            $cart->addProduct($product, $params);
            if (!empty($related)) {
                $cart->addProductsByIds(explode(',', $related));
            }

            $cart->save();

            $this->_getSession()->setCartWasUpdated(true);

            /**
             * @todo remove wishlist observer processAddToCart
             */
            Mage::dispatchEvent('checkout_cart_add_product_complete',
                array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
            );

            if (!$this->_getSession()->getNoCartRedirect(true)) {
//                if (!$cart->getQuote()->getHasError()) {
//                    $message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->escapeHtml($product->getName()));
//                    $this->_getSession()->addSuccess($message);
//                }
                $this->_goBack();
            }
        } catch (Mage_Core_Exception $e) {
            if ($this->_getSession()->getUseNotice(true)) {
                $this->_getSession()->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $this->_getSession()->addError(Mage::helper('core')->escapeHtml($message));
                }
            }

            $url = $this->_getSession()->getRedirectUrl(true);
            if ($url) {
                $this->getResponse()->setRedirect($url);
            } else {
                $this->_redirectReferer(Mage::helper('checkout/cart')->getCartUrl());
            }
        } catch (Exception $e) {
            $this->_getSession()->addException($e, $this->__('Cannot add the item to shopping cart.'));
            Mage::logException($e);
            $this->_goBack();
        }
    }
}