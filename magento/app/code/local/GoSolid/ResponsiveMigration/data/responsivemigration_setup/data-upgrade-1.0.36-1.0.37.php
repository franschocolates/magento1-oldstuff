<?php

$installer = $this;
$installer->startSetup();

// create the table for the nutrition data
$sql = <<<EOF

DROP TABLE IF EXISTS `frans_catalog_product_nutrition`;

CREATE TABLE `frans_catalog_product_nutrition` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `accpac_sku` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `ingredients` text,
  `serving_size` varchar(255) DEFAULT NULL,
  `weight` varchar(255) DEFAULT NULL,
  `net_weight` varchar(255) DEFAULT NULL,
  `calories` varchar(255) DEFAULT NULL,
  `calories_from_fat` varchar(255) DEFAULT NULL,
  `total_fat` varchar(255) DEFAULT NULL,
  `total_fat_dv` varchar(255) DEFAULT NULL,
  `saturated_fat` varchar(255) DEFAULT NULL,
  `saturated_fat_dv` varchar(255) DEFAULT NULL,
  `trans_fat` varchar(255) DEFAULT NULL,
  `trans_fat_dv` varchar(255) DEFAULT NULL,
  `cholesterol` varchar(255) DEFAULT NULL,
  `cholesterol_dv` varchar(255) DEFAULT NULL,
  `sodium` varchar(255) DEFAULT NULL,
  `sodium_dv` varchar(255) DEFAULT NULL,
  `total_carbohydrate` varchar(255) DEFAULT NULL,
  `total_carbohydrate_dv` varchar(255) DEFAULT NULL,
  `dietary_fiber` varchar(255) DEFAULT NULL,
  `dietary_fiber_dv` varchar(255) DEFAULT NULL,
  `sugars` varchar(255) DEFAULT NULL,
  `protein` varchar(255) DEFAULT NULL,
  `vitamin_a` varchar(255) DEFAULT NULL,
  `vitamin_c` varchar(255) DEFAULT NULL,
  `calcium` varchar(255) DEFAULT NULL,
  `iron` varchar(255) DEFAULT NULL,
  `is_gluten_free` tinyint(1) DEFAULT NULL,
  `is_non_gmo` tinyint(1) DEFAULT NULL,
  `is_vegan` tinyint(1) DEFAULT NULL,
  `background_image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

EOF;

$installer->run($sql);

// the nutrition_ingredients attribute already exists but needs to be changed to work with the new table
$installer->updateAttribute('catalog_product', 'nutrition_ingredients', array(
	'source_model'    => 'frans/catalog_product_nutrition_select',
	'frontend_input'     => 'select',
	'note'              => 'This list is managed in the frans_catalog_product_nutrition database table.'
));

$installer->endSetup();