<?php
// add new "Loved Items" attribute for categories

$installer = $this;
$installer->startSetup();

// remove the attribute before creating it, if it already exists for some reason
$installer->removeAttribute('catalog_product', 'loved_items');

$attr = array (
	'group' => 'General Information',
	'type' => 'text',
	'input' => 'multiselect',
	'label' => 'Loved Items',
	'source' => 'frans/attribute_source_loveditems',
	'required' => '0',
	'user_defined' => '1',
	'unique' => '0',
	'note' => 'Choose up to 5 simple products from this category to feature. Configurable products will display their default child products.',
	'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
	'visible_on_front' => '1',
	'is_html_allowed_on_front' => '0',
	'is_used_for_price_rules' => '1',
	'filterable_in_search' => '1',
	'used_in_product_listing' => '1',
	'used_for_sort_by' => '0',
	'is_configurable' => '1',
	'visible_in_advanced_search' => '1',
	'position' => '60',
	'sort_order' => 60
);

$installer->addAttribute('catalog_category', 'loved_items', $attr);

$installer->endSetup();