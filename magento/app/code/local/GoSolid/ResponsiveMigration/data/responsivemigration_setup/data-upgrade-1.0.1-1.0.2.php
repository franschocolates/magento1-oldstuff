<?php
// add new "Tagline" attribute for categories

$installer = $this;
$installer->startSetup();

$entityTypeId     = $installer->getEntityTypeId('catalog_category');
$attributeSetId   = $installer->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = 4; // General Information tab

$installer->addAttribute('catalog_category', 'tagline',  array(
	'group'             => 'General Information',
	'label'             => 'Tagline',
	'input'             => 'text',
	'type'              => 'varchar',
	'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
	'visible'           => true,
	'required'          => false,
	'user_defined'      => true,
	'note'              => 'Appears above subcategories in main menu',
	'default'           => '',
	'backend'           => '',
	'sort_order'        => 2
));

$installer->endSetup();