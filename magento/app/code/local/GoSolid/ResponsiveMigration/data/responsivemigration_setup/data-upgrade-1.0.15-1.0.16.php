<?php

$installer = $this;
$installer->startSetup();

$tableName = $installer->getTable('frans/retailStore');

$installer->getConnection()->addColumn($tableName, 'store_image_tile', array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 255,
    'nullable' => true,
    'default' => null,
    'comment' => 'Store Image Tile'
));
$installer->getConnection()->addColumn($tableName, 'store_image_banner', array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 255,
    'nullable' => true,
    'default' => null,
    'comment' => 'Store Image Banner'
));
$installer->getConnection()->addColumn($tableName, 'url_key', array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 255,
    'nullable' => true,
    'default' => null,
    'comment' =>  'URL key'
));


//$sql = <<<EOSQL
//
//  ALTER TABLE `{$tableName}`
//  ADD COLUMN `store_image_tile` VARCHAR(255) DEFAULT NULL AFTER `store_image_thumbnail`,
//  ADD COLUMN `store_image_banner` VARCHAR(255) DEFAULT NULL AFTER `store_image_large`,
//  ADD COLUMN `url_key` VARCHAR(255) DEFAULT NULL AFTER `maps_url`;
//
//EOSQL;
//
//$installer->run($sql);

$store = Mage::getModel('frans/retailStore')->load('DT', 'store_code');
$store->setUrlKey('downtown-seattle');
$store->save();

$store = Mage::getModel('frans/retailStore')->load('UV', 'store_code');
$store->setUrlKey('university-village');
$store->save();

$store = Mage::getModel('frans/retailStore')->load('BV', 'store_code');
$store->setUrlKey('bellevue');
$store->save();

// NOTE: This assumes the store code is "Pike" but it should possibly be changed to GT
$store = Mage::getModel('frans/retailStore')->load('Pike', 'store_code');
$store->setUrlKey('georgetown');
$store->save();

$installer->endSetup();