<?php

$installer = $this;
$installer->startSetup();

// set a defaule list of customer group IDs discounts will be visible for, deleting it first if one already exists
$sql = <<<EOSQL
DELETE FROM `core_config_data` WHERE `path`='frans/customer_discounts/visible_discount_customer_group_ids';
REPLACE INTO `core_config_data` (`scope`, `scope_id`, `path`, `value`)
VALUES ('default', 0, 'frans/customer_discounts/visible_discount_customer_group_ids', '5,6,7,8,9');
EOSQL;

$installer->run($sql);

$installer->endSetup();