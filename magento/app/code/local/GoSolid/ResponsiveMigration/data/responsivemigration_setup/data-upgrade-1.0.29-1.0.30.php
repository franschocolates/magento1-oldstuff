<?php

$installer = $this;
$installer->startSetup();

// Change Charitable Giving CMS page content
$cmsPage = Mage::getModel('cms/page');

$content = <<<EOHTML

<h2 class="page-title">Charitable Giving</h2>
<h3>Joyful Heart Foundation</h3>
<p>Fran's Chocolates is a proud supporter of the <a title="Visit the Joyful Heart Foundation" href="http://www.joyfulheartfoundation.org/" target="_blank">Joyful Heart Foundation</a>. Joyful Heart was founded in 2004 by activist and actress, Mariska Hargitay. Joyful Heart Foundation's mission is to heal, educate and empower survivors of sexual assault, domestic violence and child abuse, and to shed light into the darkness that surrounds these issues.</p>
<h3>Neighborhood House</h3>
<p>Fran's Chocolates is a proud supporter of <a title="Visit Neighborhood House" href="http://www.nhwa.org/" target="_blank">Neighborhood House</a>. The nonprofit Neighborhood House has been building better futures for immigrant, refugee and low-income families since 1906. From sparking a love of learning in young children, to helping someone find a job, to keeping seniors active and healthy, Neighborhood House finds creative and sustainable solutions to the challenges of poverty.</p>
<h3>Safe Crossings Foundation</h3>
<p>The mission of <a title="Visit Safe Crossings Foundation" href="http://www.safecrossingsfoundation.org/" target="_blank">Safe Crossings Foundation</a> is to be a leader in funding services that help grieving children heal. Our vision is that all grieving children receive the emotional support they need. Services we fund:</p>
<ul>
<li>Home visits by grief support counselors</li>
<li>A weekend at Camp Erin, a bereavement camp for children and teens</li>
<li>The creation of an artistic, therapeutic book for elementary school children experiencing loss ("Draw it Out" book)</li>
<li>A home like setting for kids, teens and adults to gather for connection with others who are working through the same emotions</li>
<li>Grief groups held at elementary, middle and high schools so that kids and teens can get support and provide others support without having to leave their school</li>
</ul>
<p>While you cannot spare these children and teens from the heartache of loss, you can help them learn how to be resilient so they can get back to just being kids. Join us and fill your heart and the hearts of children in your community by giving generously to Safe Crossings Foundation.</p>
<p class="text-center"><a class="button button-dark" title="Shop charitable giving" href="{{store url="shop-charitable-giving"}}">Shop Charitable Giving</a></p>
<p class="text-center"><img src="{{skin url='images/cms/charitable-heart.png'}}" /></p>

EOHTML;

// Update page with new content and title, remove content heading
$cmsPage->setTitle('Charitable Giving')
	->setIdentifier('charitable-giving')
	->setRootTemplate('one_column')
	->setContentHeading('')
	->setContent($content)
	->setStores(array(1))
	->save();

$installer->endSetup();