<?php

$installer = $this;
$installer->startSetup();

// New content for Confection care page
$content = <<<EOHTML

<h1 class="page-title">Confection Care</h1>
<p>Our confections are hand-made daily in small batches from the finest, freshest, and all natural ingredients. Our chocolates should be stored out of direct sunlight in a cool environment between 64&deg; and 68&deg;F. We do not recommend refrigeration.</p>
<p style="text-align: center;"><strong>To fully appreciate their quality and flavor please enjoy your chocolates within:</strong></p>
<ul>
<li>
<ul>
<li>6 days</li>
<li>figs</li>
</ul>
</li>
<li>
<ul>
<li>10 days</li>
<li>truffles, coconut gold bars, orange confit</li>
</ul>
</li>
<li>
<ul>
<li>1 month</li>
<li>caramels, nuts, gold bars and goldbites, ginger, apricots, palermo bars, marzipan</li>
</ul>
</li>
<li>
<ul>
<li>6 months</li>
<li>chocolate bars, thins, hot chocolate, sauces</li>
</ul>
</li>
</ul>

EOHTML;

// Update page title and save the new content
try {
    $cmsPage = Mage::getModel('cms/page')->load('Confection Care & Freshness', 'title');

    $cmsPage->setTitle('Confection Care')
        ->setContent($content)
        ->setStores(array(1))
        ->save();
} catch(Exception $e){}


$installer->endSetup();