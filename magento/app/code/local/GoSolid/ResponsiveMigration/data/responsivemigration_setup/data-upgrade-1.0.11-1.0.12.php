<?php

$installer = $this;
$installer->startSetup();

// update URL rewrites for new Business Accounts page

$sql = <<<EOSQL

UPDATE `core_url_rewrite`
SET `target_path`='business-accounts', `options`='RP'
WHERE `request_path`='about/business-gifting';

UPDATE `core_url_rewrite`
SET `target_path`='business-accounts'
WHERE `request_path`='about/business_gifting.php?id=general';

REPLACE INTO `core_url_rewrite` (`store_id`, `id_path`, `request_path`, `target_path`, `is_system`)
VALUES (1, 'business-accounts-page', 'business-accounts', 'frans/businessaccounts', 0);

EOSQL;

$installer->run($sql);

$installer->endSetup();