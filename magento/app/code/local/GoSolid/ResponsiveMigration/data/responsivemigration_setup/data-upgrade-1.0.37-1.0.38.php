<?php

$installer = $this;
$installer->startSetup();

// New content for Privacy Policy page
$content = <<<EOHTML

<h2 class="page-title">Privacy Policy</h2>
<p>Privacy is important to Fran's Chocolates, Ltd. and its affiliates (collectively, "FCL"). This privacy statement explains data collection and use practices of the <a href="{{config path="web/unsecure/base_url"}}">www.frans.com</a> site (the "Site"); it does not apply to other online or offline FCL sites, products or services. By accessing the Site, you are consenting to the information collection and use practices described in this privacy statement.</p>
<h3>Collection Of Your Personal Information</h3>
<p>We will ask you when we need information that personally identifies you ("personal information") or allows us to contact you to provide a product or service or carry out a transaction that you have requested. The personal information we collect may include, without limitation, your name, e-mail address, credit card information, address, and phone number.</p>
<p>The Site may collect certain information about your visit, such as the name of the Internet service provider and the Internet Protocol (IP) address through which you access the Internet; the date and time you access the Site; the pages that you access while at the Site and the Internet address of the Web site from which you linked directly to the Site. This information is used to help improve the Site, analyze trends, and administer the Site.</p>
<h3>Use Of Your Personal Information</h3>
<p>The personal information collected on this Site will be used to operate the Site and to provide the products or services or carry out the transactions you have requested or authorized.</p>
<p>In support of these uses, FCL may use personal information to provide you with more effective customer service, to improve the Site and any related FCL products or services, and to make the Site easier to use by eliminating the need for you to repeatedly enter the same information. In order to offer you a more consistent experience in your interactions with FCL, information collected by the Site may be combined with information collected by FCL from other sources.</p>
<p>We may use your personal information to provide you with important information about the product or service that you have purchased or inquired about. Additionally, we may send you information about other FCL products and services, and/or share information with FCL partners so they may send you information about their products and services.</p>
<p>We may merge Site-visitation data with demographic information, and we may use this information to provide more relevant content. We may combine Site-visitation data with your personal information in order to provide you with personalized content.</p>
<p>FCL may hire other companies to provide products or services on our behalf, and we may provide such companies with the personal information they need to perform their duties.</p>
<p>FCL may disclose personal information if required to do so by law or in the good faith belief that such action is necessary to (a) conform to the edicts of the law or comply with legal process served on FCL; (b) protect and defend the rights or property of FCL; or (c) act in urgent circumstances to protect the personal safety of FCL employees or agents, users of FCL products or services, or members of the public.</p>
<p>Personal information collected on the Site may be stored and processed in the State of Washington or any other state or country in which FCL or its agents maintain facilities, and by using the Site you consent to any such transfer of information to another state or country.</p>
<h3>Control Of Your Personal Information</h3>
<p>Please be aware that this privacy statement and any choices you make on the Site will not necessarily apply to personal information you may have provided to FCL in the context of other FCL products or services. If you give us your e-mail address, FCL may send out e-mails to you with information on our products or services.</p>
<h3>Security of Your Personal Information</h3>
<p>FCL will use commercially reasonable efforts to protect your personal information from unauthorized access, use, or disclosure. However, due to computer hackers, electronic malfunctions, and other events, FCL cannot guaranty that such safeguards will always protect such information.</p>
<h3>Protection Of Children's Personal Information</h3>
<p>The Site is a general audience web site and does not knowingly collect any personal information from children.</p>
<h3>Use Of Cookies</h3>
<p>We may use cookies on this Site to enhance your user experience. A cookie is a small text file that is placed on your computer's hard disk by a Web page server. One of the primary purposes of cookies is to provide a convenience feature to save you time. For example, if you navigate within a site, a cookie helps the site to recall your specific information on subsequent visits. This simplifies the process of delivering relevant content, eases site navigation, and so on.</p>
<p>You have the ability to accept or decline cookies. Most Web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. If you choose to decline cookies, you may not be able to fully experience the features of this or other Web sites you visit.</p>
<p>Web beacons, also known as clear gif technology, or action tags, may be used to assist in delivering the cookie on the Site. This technology is a tool we may use to compile aggregated statistics about the Site usage, such as how many visitors clicked on key elements (such as links or graphics) on a web page. We may share such Site statistics with partner companies.</p>
<p>FCL reserves the right at all times to disclose any information as FCL deems necessary to satisfy any applicable law, regulation, legal process or governmental request, or to edit, refuse to post or to remove any information or materials, in whole or in part, in FCL's sole discretion.</p>
<h3>Enforcement Of This Privacy Statement</h3>
<p>If you have questions regarding this statement, you should contact FCL.</p>
<h3>Changes To This Statement</h3>
<p>We may update this privacy statement from time to time. When we do, we will also revise the "last updated" date at the top of the privacy statement. We encourage you to periodically review this privacy statement to stay informed about our practices. Your continued use of the Site constitutes your agreement to this privacy statement and any updates.</p>
<h3>Contact Information</h3>
<p>FCL welcomes your comments regarding this privacy statement. If you believe that FCL has not adhered to this privacy statement, please contact us electronically or via postal mail at the address provided below, and we will use commercially reasonable efforts to promptly determine and remedy the problem.</p>
<p>Attn: CEO<br />Fran's Chocolates, Ltd.<br />{{config path="shipping/origin/street_line1"}}<br />{{config path="shipping/origin/city"}}, WA {{config path="shipping/origin/postcode"}}<br />Ph. {{config path="general/store_information/phone"}}<br />E-mail: <a href="mailto:{{config path="trans_email/ident_general/email"}}">{{config path="trans_email/ident_general/email"}}</a></p>

EOHTML;

// Update page title and save the new content
$cmsPage = Mage::getModel('cms/page')->load('Privacy Policy', 'title');

$cmsPage->setContent($content)
	->setContentHeading('')
	->setStores(array(1))
	->save();

// New content for Terms of Use page
$content = <<<EOHTML

<h2 class="page-title">Terms of Use</h2>
<p><em>Last Updated: September 2014</em></p>
<h3>Acceptance of Terms</h3>
<p>This site is provided to you by Fran's Chocolates, Ltd. and its affiliates (collectively, "FCL") and is subject to the following Terms of Use ("TOU"). FCL reserves the right to update the TOU at any time without notice to you. The most current version of the TOU can be reviewed by clicking on the "Terms of Use" hypertext link located at the bottom of our web pages.</p>
<h3>Description of Services</h3>
<p>Through its web sites, FCL may provide you with access to a variety of resources, including but not limited to information on, and the ability to purchase, various products or services (collectively "Services"). The Services, including any updates, enhancements, new features, and/or the addition of any new web pages, are subject to the TOU.</p>
<h3>Use Limitation</h3>
<p>No one may modify, copy, distribute, transmit, display, perform, reproduce, publish, license, create derivative works from, transfer, or sell any information, photos, data, products or services obtained from the Services, without the prior written consent of FCL in each instance.</p>
<h3>Privacy and Protection of Personal Information</h3>
<p>See the <a href="/privacy/">Privacy Policy</a> disclosures relating to the collection and use of your information.</p>
<h3>Disclaimers</h3>
<p>FCL and/or its agents make no representations about the suitability of the information contained in this site for any purpose. This site is provided by FCL on an "as is" basis and "with all faults". FCL makes no representations or warranties of any kind, express or implied, as to the operation of the site or the information, content, materials, or services included on this site. To the fullest extent permissibel by applicable law, FCL disclaims all warranties, express or implied, including, but not limited to, implied warranties or merchantability, fitness for a particular purpose, title and noninfringement. In no event shall FCL and/or its agents be liable for any damages of any kind arising from the use of this site or the information contained therein, including, but not limited to indirect, special, incidental, punitive, or consequential damages, or any damages whatsoever resulting from loss of use, data or profits, whether in an action based on contract, negligence or other tortious action.</p>
<p>The information published on this site could include technical inaccuracies or typographical errors. Changes are periodically added to the information in this site. FCL and/or its agents may make changes to the information contained in this site at any time.</p>
<h3>User Account, Password, and Security</h3>
<p>If any of the Services requires you to open an account, you must complete the registration process by providing us with current, complete and accurate information as prompted by the applicable registration form. You also may be asked to choose a password and a user name. You are entirely responsible for maintaining the confidentiality of your password and account. Furthermore, you are entirely responsible for any and all activities that occur under your account. You agree to notify FCL immediately of any unauthorized use of your account or any other breach of security. FCL will not be liable for any loss that you may incur as a result of someone else using your password or account, either with or without your knowledge. However, you could be held liable for losses incurred by FCL or another party due to someone else using your account or password. You may not use anyone else's account at any time, without the permission of the account holder.</p>
<h3>No Unlawful or Prohibited Use</h3>
<p>As a condition of your use of the Services, you will not use the Services for any purpose that violates any applicable laws or regulations, or that is prohibited by the TOU. You may not use the Services in any manner that could damage, disable, overburden, or impair any server that hosts the FCL web sites, or the network(s) connected to any such server, or interfere with any other party's use and enjoyment of any Services. You may not attempt to gain unauthorized access to other accounts or to any of the Services, through hacking, password mining or any other means. You may not obtain or attempt to obtain any materials or information through any means not intentionally made available through the Services.</p>
<p>FCL reserves the right at all times to disclose any information as FCL deems necessary to satisfy any applicable law, regulation, legal process or governmental request, or to edit, refuse to post or to remove any information or materials, in whole or in part, in FCL's sole discretion.</p>
<h3>Materials Provided to FCL</h3>
<p>FCL does not claim ownership of the information you provide to FCL (including feedback and suggestions) or that you input or submit to any Services (collectively "Submissions"). However, by providing Submissions you are granting FCL a royalty-free, sublicensable right to use your Submissions as FCL sees fit in connection with the operation of FCL's business and the modification or development of products or services.</p>
<h3>Notices and Procedure for Making Claims of Copyright or Other Intellectual Property Infringement</h3>
<p>FCL's agent for notice of claims of copyright or other intellectual property infringement can be reached as follows:</p>
<p>Attn: CEO<br />Fran's Chocolates, Ltd.<br />{{config path="shipping/origin/street_line1"}}<br />{{config path="shipping/origin/city"}}, WA {{config path="shipping/origin/postcode"}}<br />Ph. {{config path="general/store_information/phone"}}<br />E-mail: <a href="mailto:{{config path="trans_email/ident_general/email"}}">{{config path="trans_email/ident_general/email"}}</a></p>
<h3>Links to Third Party Sites</h3>
<p>FCL may provide links that will let you leave FCL's site. The linked sites are not under the control of FCL and FCL is not responsible for the contents of any linked site or any link contained in a linked site, or any changes or updates to such sites. FCL is not responsible for webcasting or any other form of transmission received from any linked site. FCL is providing these links to you only as a convenience, and the inclusion of any link does not imply endorsement by FCL of the site.</p>
<h3>Copyright Notice</h3>
<p>{{config path="design/footer/copyright"}}</p>
<p>Any rights not expressly granted herein are reserved.</p>

EOHTML;

// Update page title and save the new content
try {
$cmsPage = Mage::getModel('cms/page')->load('Terms of Use', 'title');

$cmsPage->setContent($content)
	->setContentHeading('')
	->setStores(array(1))
	->save();
} catch (Exception $e) {}

$installer->endSetup();