<?php

$installer = $this;
$installer->startSetup();

// Decrease the heading level of all CMS page headings
$changes = array(
	'h3'    => 'h4',
	'h2'    => 'h3',
	'h1'    => 'h2'
);

foreach($changes as $from => $to){

	$cmsPages = Mage::getModel('cms/page')->getCollection()->addFieldToFilter('content', array('like' => '%<' . $from . '%'));
	foreach($cmsPages as $cmsPage){

		$content = $cmsPage->getContent();
		$content = str_replace(('<' . $from), ('<' . $to), $content); // replace opening tags
		$content = str_replace(('</' . $from), ('</' . $to), $content); // replace closing tags
		$cmsPage->setContent($content)
			->setStores(array(1))
			->save();
	}

}

$installer->endSetup();