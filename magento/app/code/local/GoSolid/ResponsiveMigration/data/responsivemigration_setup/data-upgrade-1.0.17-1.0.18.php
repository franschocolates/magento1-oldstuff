<?php

$installer = $this;
$installer->startSetup();

// Change existing News CMS page (with "articles" URL key) to have a URL key of "news"
$cmsPage = Mage::getModel('cms/page')->load('News', 'title');
$cmsPage->setIdentifier('news')->save();

// Create a redirect so links to the old URL will reach the page at its new location
$sql = <<<EOSQL

REPLACE INTO `core_url_rewrite` (`store_id`, `id_path`, `request_path`, `target_path`, `is_system`, `options`)
VALUES (1, 'news-page', 'articles', 'news', 0, 'RP');

EOSQL;

$installer->run($sql);

$installer->endSetup();