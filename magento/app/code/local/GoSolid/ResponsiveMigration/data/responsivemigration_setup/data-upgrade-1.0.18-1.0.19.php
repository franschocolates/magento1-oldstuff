<?php

$installer = $this;
$installer->startSetup();

// Change all CMS pages to use "1 column" layout
$cmsPages = Mage::getModel('cms/page')->getCollection();
foreach($cmsPages as $cmsPage){
	$cmsPage->setRootTemplate('one_column')
		->setStores(array(1))
		->save();

}

$installer->endSetup();