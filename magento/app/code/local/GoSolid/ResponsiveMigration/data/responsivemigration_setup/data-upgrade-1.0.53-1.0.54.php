<?php

$installer = $this;
$installer->startSetup();

//upgrade the  Items on Order - By Item	report.
$sql = <<<EOSQL
	UPDATE `gosolid_sqlreport_report` SET
    `sql` = "

    #Upgraded via update script 8/31/2015
        SELECT
            `accpac_sku`
        , `name` AS 'Product Name'
        ,(
            SELECT title FROM eav_attribute ea
            JOIN `catalog_product_entity_int` ei ON ei.attribute_id = ea.attribute_id
            JOIN `frans_color_tile` ct ON ct.id = ei.value
            WHERE ea.attribute_code = 'color_tile'
            AND ei.entity_id = sfoi.product_id
        ) AS 'Color Tile'
        , SUM(sfoi.qty_ordered - (sfoi.qty_shipped + sfoi.qty_canceled)) AS 'Quantity'
        , DATE_FORMAT(sfo.planned_ship_date, '%m/%d/%Y') AS 'ship date'
        , IF (COUNT(DISTINCT sfo.increment_id) > 1, CONCAT(COUNT(DISTINCT sfo.increment_id), ' shipments'), GROUP_CONCAT(DISTINCT sfo.increment_id SEPARATOR '')) AS 'Order #'
        , sfoi.notes AS 'Product Notes'

    FROM
        sales_flat_order_item AS sfoi
    INNER JOIN
        sales_flat_order AS sfo
    ON
        sfo.entity_id = sfoi.order_id
    WHERE
        (sfoi.qty_ordered - (sfoi.qty_shipped + sfoi.qty_canceled)) > 0
    AND
        sfo.`status` NOT IN ('ready_for_pickup', 'picked_up')
    AND
        DATE(sfo.planned_ship_date) BETWEEN '{Start Planned Ship Date:date}' AND '{End Planned Ship Date:date}'
    GROUP BY
        sfoi.`sku`, sfoi.`name`, sfo.planned_ship_date, sfoi.notes, sfoi.`accpac_sku`
    ORDER BY
        planned_ship_date ASC, sku ASC;"
    WHERE id = 2
EOSQL;

$installer->run($sql);


$installer->endSetup();