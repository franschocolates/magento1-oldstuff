<?php
// add new "Display Section" attribute for categories

$installer = $this;
$installer->startSetup();

$attr = array (
	'group' => 'Display Settings',
	'type' => 'int',
	'input' => 'select',
	'label' => 'Display Section',
	'source' => 'frans/attribute_source_displaysection',
	'required' => '1',
	'user_defined' => '1',
	'default' => '1',
	'unique' => '0',
	'note' => 'Where this category will appear in the list of its parent\'s subcategories. Section 1 is the default.',
	'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
	'visible_on_front' => '1',
	'is_html_allowed_on_front' => '0',
	'is_used_for_price_rules' => '1',
	'filterable_in_search' => '1',
	'used_in_product_listing' => '1',
	'used_for_sort_by' => '0',
	'is_configurable' => '1',
	'visible_in_advanced_search' => '1',
	'position' => '55',
	'sort_order' => 55
);

$installer->addAttribute('catalog_category', 'display_section', $attr);

// set defaults for the new attribute

$currentStore = Mage::app()->getStore()->getId();
Mage::app()->getStore()->setId(Mage_Core_Model_App::ADMIN_STORE_ID);
$categories = Mage::getModel('catalog/category')->getCollection();
foreach($categories as $category){
	$categoryId = $category->getId();
	$loadedCategory = Mage::getModel('catalog/category')->load($categoryId);
	$loadedCategory->setDisplaySection(1);
	$loadedCategory->save();
}
Mage::app()->getStore()->setId($currentStore);

$installer->endSetup();