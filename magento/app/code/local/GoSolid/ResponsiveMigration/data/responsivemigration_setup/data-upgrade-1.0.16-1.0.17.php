<?php

$installer = $this;
$installer->startSetup();

// add URL rewrite for new store details pages

// IDs are different on production from dev/staging systems but names are consistent
$downtownId = Mage::getModel('frans/retailStore')->load('Downtown Seattle', 'name')->getId();
$universityId = Mage::getModel('frans/retailStore')->load('University Village', 'name')->getId();
$bellevueId = Mage::getModel('frans/retailStore')->load('Bellevue', 'name')->getId();
$georgetownId = Mage::getModel('frans/retailStore')->load('Georgetown', 'name')->getId();

$sql = <<<EOSQL

REPLACE INTO `core_url_rewrite` (`store_id`, `id_path`, `request_path`, `target_path`, `is_system`)
VALUES (1, 'stores-page-downtown', 'stores/downtown-seattle', 'frans/stores/view/id/{$downtownId}', 0);

REPLACE INTO `core_url_rewrite` (`store_id`, `id_path`, `request_path`, `target_path`, `is_system`)
VALUES (1, 'stores-page-university', 'stores/university-village', 'frans/stores/view/id/{$universityId}', 0);

REPLACE INTO `core_url_rewrite` (`store_id`, `id_path`, `request_path`, `target_path`, `is_system`)
VALUES (1, 'stores-page-bellevue', 'stores/bellevue', 'frans/stores/view/id/{$bellevueId}', 0);

REPLACE INTO `core_url_rewrite` (`store_id`, `id_path`, `request_path`, `target_path`, `is_system`)
VALUES (1, 'stores-page-georgetown', 'stores/georgetown', 'frans/stores/view/id/{$georgetownId}', 0);

EOSQL;

$installer->run($sql);

$installer->endSetup();