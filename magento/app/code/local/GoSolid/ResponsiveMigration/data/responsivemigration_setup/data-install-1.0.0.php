<?php

$installer = $this;
$installer->startSetup();

// automatically enable LessCSS module
Mage::getModel('core/config')->saveConfig('less/general/enabled', '1');

$installer->endSetup();