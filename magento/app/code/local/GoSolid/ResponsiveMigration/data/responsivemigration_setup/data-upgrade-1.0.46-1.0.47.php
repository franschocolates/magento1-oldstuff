<?php

$installer = $this;
$installer->startSetup();

// set new Search Results product grid layout to "Two Up"
Mage::getModel('core/config')->saveConfig('frans/search_results_page/product_grid_layout', '500');

$installer->endSetup();