<?php
// delete Corporate Links CMS block

$installer = $this;
$installer->startSetup();

//Update corporate links
$corpLinks = Mage::getModel('cms/block')->load('corporate_links', 'identifier');

if ($corpLinks->getBlockId())
{
    $corpLinks->delete();
}

$installer->endSetup();