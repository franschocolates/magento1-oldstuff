<?php

$installer = $this;
$installer->startSetup();

// remove the Piece Count attribute from the PDP attribute accordion
$installer->updateAttribute('catalog_product', 'piece_count', array(
	"is_visible_on_front" => 0
));

$installer->endSetup();