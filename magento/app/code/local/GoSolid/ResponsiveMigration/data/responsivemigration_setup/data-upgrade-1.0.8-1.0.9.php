<?php

$installer = $this;
$installer->startSetup();

$tableName = $installer->getTable('frans/productPiece');

$installer->getConnection()->addColumn($tableName, 'piece_description', array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 1024,
    'nullable' => true,
    'default' => null,
    'comment' => 'piece description'
));
//$sql = <<<EOSQL
//
//  ALTER TABLE `{$tableName}`
//  ADD COLUMN `piece_description` VARCHAR(1024) DEFAULT NULL;
//
//EOSQL;
//
//$installer->run($sql);


$installer->endSetup();