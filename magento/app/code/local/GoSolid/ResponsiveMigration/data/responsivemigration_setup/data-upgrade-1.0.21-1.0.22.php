<?php

$installer = $this;
$installer->startSetup();

// Change Shipping CMS page content
$cmsPage = Mage::getModel('cms/page')->load('shipping', 'identifier');

$content = <<<EOHTML

<h1 class="page-title">Shipping</h1>
<p>Nothing is more important to us than your complete satisfaction. That is why we take great care creating our chocolates and hand packing your order. Please note that all of our chocolates are shipped via local, two-day or overnight methods due to perishability.</p>
<h2>Local Shipping</h2>
<p>We offer local flat rate ground shipping for $10 per address to deliveries in Washington, Oregon, California, and Idaho.</p>
<h2>Shipping Charges</h2>
<p>2 Day Express and Standard Overnight shipping charges are calculated based on the product total per delivery address.</p>
<h2>Warm Weather Shipping</h2>
<p>We guarantee your chocolates will arrive in excellent condition, even in warm weather. We will pack your order with mylar envelopes and gel ice packs when necessary at no additional charge. April-September, we are often unable to ship over the weekend. Orders placed after 1 PM on Wednesday will be held to ship on Monday. We recommend alerting recipients that a sweet gift is on its way so the package is not left outside in warm weather for long periods.</p>
<h2>Signature Release</h2>
<p>FedEx has permission to leave a package at the delivery address when no one is available to sign for it. That way the recipient doesn’t have to be home to accept your gift, and the chocolates will not suffer from multiple redelivery attempts.</p>
<h2>P.O. Boxes</h2>
<p>Unfortunately, we are unable to ship to P.O. boxes. If you are able to locate the street address where the P.O. box is located, FedEx will be able to find it.</p>
<h2>Shipping and Delivery Schedule</h2>
<p>Available items ordered before 1:00 PM (Pacific), Monday through Friday, are usually shipped the same day. Orders placed Saturday and Sunday will ship Monday.</p>
<h2>International Shipping</h2>
<p>We ship to several international destinations including Japan and Canada, please call us at 206.322.0233 or Email orders@frans.com to confirm if we can ship to your desired location and to place your order. To help us provide a shipping estimate, please Email us the following information:</p>
<ul>
<li>First name</li>
<li>Family name</li>
<li>Email address</li>
<li>Phone number</li>
<li>Shipping address</li>
<li>The items you would like to order</li>
</ul>
<h3>Canadian Shipping Rates</h3>
<p>International Economy and International shipping charges are calculated based on the product total per delivery address.</p>

EOHTML;

// Update page with new content and title, remove content heading
$cmsPage->setTitle('Shipping')
	->setContentHeading('')
	->setContent($content)
	->setStores(array(1))
	->save();

$installer->endSetup();