<?php

$installer = $this;
$installer->startSetup();

// New content for 404 (no-route) page
$content = <<<EOHTML

<h2>Not Found</h2>
<p>Apologies, but the page you requested could not be found.</p>

EOHTML;

// Update page title and save the new content
try {
$cmsPage = Mage::getModel('cms/page')->load('404 Not Found', 'title');

$cmsPage->setContent($content)->save();
} catch(Exception $e) {}
$installer->endSetup();