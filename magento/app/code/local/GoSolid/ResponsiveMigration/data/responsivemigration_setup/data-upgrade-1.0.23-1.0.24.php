<?php

$installer = $this;
$installer->startSetup();

// Create the frans_color_tile table
try {
    $table = $installer->getConnection()
        ->newTable('frans_color_tile')
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
            'auto_increment' => true,
            'unsigned' => true,
            'primary' => true,
        ), 'ID')
        ->addColumn('title', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
            'nullable' => true,
            'default' => null,
            'comment' => 'Title'
        ))
        ->addColumn('image',  Varien_Db_Ddl_Table::TYPE_TEXT, 512, array(
            'nullable' => true,
            'default' => null,
            'comment' => 'Image'
        ))
        ->addColumn('status',  Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => true,
            'default' => null,
            'comment' => 'Status'
        ));

    $installer->getConnection()->createTable($table);
} catch (Exception $e){}


//$sql = <<<EOSQL
//CREATE TABLE `frans_color_tile` (
//  `id` int(11) NOT NULL AUTO_INCREMENT,
//  `title` varchar(255) DEFAULT NULL,
//  `image` varchar(512) DEFAULT NULL,
//  `status` int(11) DEFAULT NULL,
//  PRIMARY KEY (`id`)
//) ENGINE=InnoDB DEFAULT CHARSET=latin1
//EOSQL;
//
//$installer->run($sql);

$installer->endSetup();