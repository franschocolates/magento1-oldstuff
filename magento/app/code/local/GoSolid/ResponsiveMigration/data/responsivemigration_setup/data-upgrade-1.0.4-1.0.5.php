<?php
$installer = $this;
$installer->startSetup();

$installer->addAttribute("catalog_product", "product_image_style",  array(
	"type"     => "int",
	"backend"  => "",
	"frontend" => "",
	"label"    => "Product Image Style",
	"input"    => "select",
	"class"    => "",
	"source"   => "frans/catalog_product_attribute_source_imagestyle",
	"global"   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
	"visible" => true,
	"required" => true,
	"user_defined" => true,
	"default" => "0",
	"visible_on_front" => true,
	"unique" => false,
	"group" => "General",
	"position" => 115,
	"note" => "Light Image Style: makes header text dark, Dark Image Style: makes header text lighter",
	"used_in_product_listing" => false
));
$installer->endSetup();