<?php

$installer = $this;
$installer->startSetup();

// fixing sort orders of all category attributes in General Information group

$installer->updateAttribute('catalog_category', 'name', array('sort_order' => 10));
$installer->updateAttribute('catalog_category', 'is_active', array('sort_order' => 20));
$installer->updateAttribute('catalog_category', 'url_key', array('sort_order' => 30));
$installer->updateAttribute('catalog_category', 'tagline', array('sort_order' => 40));
$installer->updateAttribute('catalog_category', 'action_label', array('sort_order' => 50));
$installer->updateAttribute('catalog_category', 'inset_color', array('sort_order' => 60));
$installer->updateAttribute('catalog_category', 'inset_opacity', array('sort_order' => 70));
$installer->updateAttribute('catalog_category', 'description', array('sort_order' => 80));
// also update note for Image attribute
$installer->updateAttribute('catalog_category', 'image', array(
	'sort_order'    => 90,
	'note'          => 'Image must be 1210 pixels wide and not exceed 770 pixels tall, fading to white or transparent on the bottom'
));
// also add a note for the Thumbnail attribute
$installer->updateAttribute('catalog_category', 'thumbnail', array(
	'sort_order'    => 100,
	'note'          => 'Image dimensions must be 126x84 pixels'
));
$installer->updateAttribute('catalog_category', 'tile_image', array('sort_order' => 110));
// also add a default of "light" for the Tile Image Style attribute
$installer->updateAttribute('catalog_category', 'tile_image_style', array(
	'sort_order'    => 120,
	'default'       => '1'
));
$installer->updateAttribute('catalog_category', 'action_image', array('sort_order' => 130));
$installer->updateAttribute('catalog_category', 'all_products_tile_image', array('sort_order' => 140));
$installer->updateAttribute('catalog_category', 'meta_title', array('sort_order' => 150));
$installer->updateAttribute('catalog_category', 'meta_keywords', array('sort_order' => 160));
$installer->updateAttribute('catalog_category', 'meta_description', array('sort_order' => 170));
$installer->updateAttribute('catalog_category', 'include_in_menu', array('sort_order' => 180));
$installer->updateAttribute('catalog_category', 'include_in_submenu', array('sort_order' => 190));

$sql = <<<EOSQL

UPDATE eav_entity_attribute SET `sort_order`='10' WHERE `attribute_id`=(SELECT `attribute_id` FROM `eav_attribute` WHERE `attribute_code`='name' AND `entity_type_id`=3);
UPDATE eav_entity_attribute SET `sort_order`='20' WHERE `attribute_id`=(SELECT `attribute_id` FROM `eav_attribute` WHERE `attribute_code`='is_active' AND `entity_type_id`=3);
UPDATE eav_entity_attribute SET `sort_order`='30' WHERE `attribute_id`=(SELECT `attribute_id` FROM `eav_attribute` WHERE `attribute_code`='url_key' AND `entity_type_id`=3);
UPDATE eav_entity_attribute SET `sort_order`='40' WHERE `attribute_id`=(SELECT `attribute_id` FROM `eav_attribute` WHERE `attribute_code`='tagline' AND `entity_type_id`=3);
UPDATE eav_entity_attribute SET `sort_order`='50' WHERE `attribute_id`=(SELECT `attribute_id` FROM `eav_attribute` WHERE `attribute_code`='action_label' AND `entity_type_id`=3);
UPDATE eav_entity_attribute SET `sort_order`='60' WHERE `attribute_id`=(SELECT `attribute_id` FROM `eav_attribute` WHERE `attribute_code`='inset_color' AND `entity_type_id`=3);
UPDATE eav_entity_attribute SET `sort_order`='70' WHERE `attribute_id`=(SELECT `attribute_id` FROM `eav_attribute` WHERE `attribute_code`='inset_opacity' AND `entity_type_id`=3);
UPDATE eav_entity_attribute SET `sort_order`='80' WHERE `attribute_id`=(SELECT `attribute_id` FROM `eav_attribute` WHERE `attribute_code`='description' AND `entity_type_id`=3);
UPDATE eav_entity_attribute SET `sort_order`='90' WHERE `attribute_id`=(SELECT `attribute_id` FROM `eav_attribute` WHERE `attribute_code`='image' AND `entity_type_id`=3);
UPDATE eav_entity_attribute SET `sort_order`='100' WHERE `attribute_id`=(SELECT `attribute_id` FROM `eav_attribute` WHERE `attribute_code`='thumbnail' AND `entity_type_id`=3);
UPDATE eav_entity_attribute SET `sort_order`='110' WHERE `attribute_id`=(SELECT `attribute_id` FROM `eav_attribute` WHERE `attribute_code`='tile_image' AND `entity_type_id`=3);
UPDATE eav_entity_attribute SET `sort_order`='120' WHERE `attribute_id`=(SELECT `attribute_id` FROM `eav_attribute` WHERE `attribute_code`='tile_image_style' AND `entity_type_id`=3);
UPDATE eav_entity_attribute SET `sort_order`='130' WHERE `attribute_id`=(SELECT `attribute_id` FROM `eav_attribute` WHERE `attribute_code`='action_image' AND `entity_type_id`=3);
UPDATE eav_entity_attribute SET `sort_order`='140' WHERE `attribute_id`=(SELECT `attribute_id` FROM `eav_attribute` WHERE `attribute_code`='all_products_tile_image' AND `entity_type_id`=3);
UPDATE eav_entity_attribute SET `sort_order`='150' WHERE `attribute_id`=(SELECT `attribute_id` FROM `eav_attribute` WHERE `attribute_code`='meta_title' AND `entity_type_id`=3);
UPDATE eav_entity_attribute SET `sort_order`='160' WHERE `attribute_id`=(SELECT `attribute_id` FROM `eav_attribute` WHERE `attribute_code`='meta_keywords' AND `entity_type_id`=3);
UPDATE eav_entity_attribute SET `sort_order`='170' WHERE `attribute_id`=(SELECT `attribute_id` FROM `eav_attribute` WHERE `attribute_code`='meta_description' AND `entity_type_id`=3);
UPDATE eav_entity_attribute SET `sort_order`='180' WHERE `attribute_id`=(SELECT `attribute_id` FROM `eav_attribute` WHERE `attribute_code`='include_in_menu' AND `entity_type_id`=3);
UPDATE eav_entity_attribute SET `sort_order`='190' WHERE `attribute_id`=(SELECT `attribute_id` FROM `eav_attribute` WHERE `attribute_code`='include_in_submenu' AND `entity_type_id`=3);

EOSQL;

$installer->run($sql);

// set defaults for the new attributes

$currentStore = Mage::app()->getStore()->getId();
Mage::app()->getStore()->setId(Mage_Core_Model_App::ADMIN_STORE_ID);
$categories = Mage::getModel('catalog/category')->getCollection();
foreach($categories as $category){
	$categoryId = $category->getId();
	$loadedCategory = Mage::getModel('catalog/category')->load($categoryId);
	$loadedCategory->setActionLabel('Shop Now');
	$loadedCategory->setInsetColor('FFFFFF');
	$loadedCategory->setInsetOpacity('90');
	// "dark" style has been changed from 0 to 2 so update all products that used it
	if($loadedCategory->getTileImageStyle() == 0){
		$loadedCategory->setTileImageStyle(2);
	}
	// layout options have new numbers; existing choices will need to be reassigned
	switch($loadedCategory->getProductGridLayout()){
		case 1: // Three Up
			$newValue = 1000; break;
		case 2: // Two Up
			$newValue = 500; break;
		case 3: // Two Up, Two Mid, One Down
			$newValue = 700; break;
		case 4: // One Up, Two Mid, Two Down
			$newValue = 300; break;
		case 5: // One Up, Two Down (Anchored)
			$newValue = 200; break;
		case 6: // Two Up, One Down
			$newValue = 550; break;
		case 7: // Two Up, One Mid, One Down
			$newValue = 600; break;
		case 8: // Two Up, Three Down
			$newValue = 800; break;
		default:
			$newValue = $loadedCategory->getProductGridLayout(); break;
	}
	$loadedCategory->setProductGridLayout($newValue);
	if($loadedCategory->getChildCategoryLayout() == 1){
		$loadedCategory->setChildCategoryLayout(300);
	}
    try {
        $loadedCategory->save();
    } catch(Exception $e){}

}
Mage::app()->getStore()->setId($currentStore);


$installer->endSetup();