<?php

$installer = $this;
$installer->startSetup();

$tableName = $installer->getTable('frans/retailStore');

try {
    $installer->getConnection()->changeColumn($tableName, 'store_image', 'store_image_large', array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length' => 255,
        'comment' => 'store image large'
    ));
} catch (Exception $e) {}


$installer->getConnection()->addColumn($tableName, 'store_image_thumbnail', array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 255,
    'nullable' => true,
    'default' => null,
    'comment' => 'store image thumb'
));

//$sql = <<<EOSQL
//
//  ALTER TABLE `{$tableName}`
//  CHANGE `store_image` `store_image_large` varchar(255);
//
//  ALTER TABLE `{$tableName}`
//  ADD COLUMN `store_image_thumbnail` VARCHAR(255) DEFAULT NULL AFTER `store_image_large`;
//
//EOSQL;
//
//$installer->run($sql);

$installer->endSetup();