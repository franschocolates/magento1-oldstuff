<?php

$installer = $this;
$installer->startSetup();

// create the new Ingredient Information page
$content = <<<EOT

<h2 class="page-title">Ingredient Information</h2>
<h3 class="text-uppercase">Hand-Selected Ingredients</h3>
<p>Every distinct element is essential to the quality of our chocolates – transforming the ordinary into something truly extraordinary. Hand-selecting all of our ingredients, we believe in using only premium chocolate made from superior grade cocoa beans and other all natural, non-gmo, and when possible, organic and local ingredients.</p>
<p>Like cream from Organic Valley, a cooperative of farmers in Washington, fresh-roasted hazelnuts from the Willamette Valley in Oregon and other farms and growers we've maintained relationships with for more than 20 years.</p>
<table>
	<tr>
		<td>
			<h3>Cream &amp; Butter</h3>
			<p>Harvested from local organic farms, these fresh ingredients offer a subtle flavor that adds depth to each of our confections.</p>
		</td>
		<td>
			<h3>Vanilla</h3>
			<p>The creamy, velvety flavor that arises from our Madagascar vanilla perfectly complements the taste of rich, premium chocolate.</p>
		</td>
	</tr>
	<tr>
		<td>
			<h3>Single Malt Whiskey</h3>
			<p>With a deep depth of flavor and distinct character, our McCarthy's Oregon Single Malt Whiskey is smooth with a surprisingly clean finish.</p>
		</td>
		<td>
			<h3>Coffee</h3>
			<p>Blended in true Northern Italian tradition, our espresso is locally roasted at Seattle's own Caff&eacute; Vita.</p>
		</td>
	</tr>
	<tr>
		<td>
			<h3>Tea</h3>
			<p>Oolong tea imported from high in the mountains of Taiwan creates exceptional flavor when steeped into our premium chocolate.</p>
		</td>
		<td>
			<h3>Peppermint</h3>
			<p>Local peppermint from the Yakima Valley in Washington is infused into our premium chocolate for a delicate balance of flavors.</p>
		</td>
	</tr>
	<tr>
		<td>
			<h3>Gray Salt</h3>
			<p>Harvested off the coast of Brittany, France, sel gris or gray sea salt is high in mineral content and has a natural, pearlescent shimmer. The distinct flavor uniquely complements our dark chocolate covered caramels.</p>
		</td>
		<td>
			<h3>Smoked Salt</h3>
			<p>A Welsh sea salt, smoked over oak, creates a reduced salinity which pairs beautifully with our milk chocolate covered caramels.</p>
		</td>
	</tr>
</table>
<hr class="plain" />
<h3 class="text-uppercase">Ethical Sourcing</h3>
<p>We are ultimately committed to evaluating the integrity of our sources and maintaining the highest standards with our partnerships and our products.</p>
<table>
	<tr>
		<td>
			<h3 class="badge organic">Organic</h3>
			<p>We source local and organic ingredients whenever we can. Some of the organic ingredients we use are sugar, cream, butter, tapioca syrup, lemon juice, ginger, hazelnuts, almonds and macadamia nuts.</p>
		</td>
		<td>
			<h3 class="badge gmo">Non-GMO</h3>
			<p>All of our products are made with all non-gmo ingredients. Our chocolate includes a small amount of European non-gmo soy lecithin which is used as an emulsifier.</p>
		</td>
	</tr>
	<tr>
		<td>
			<h3 class="badge vegan">Vegan Confections</h3>
			<p>We have several confections with ingredient lists free of animal products. Try our dark chocolate almond trios, apricots in dark chocolate, organic ginger in dark chocolate, orange confit in dark chocolate, pistachio marzipan, yuzu or lemon marzipan, spiced macadamia nuts, almonds in dark chocolate, caramelized nuts and nibs, Palermo 64% bars, nibbits in dark chocolate, raspberry dessert sauce, dark hot chocolate (when prepared with water), or any of our dark chocolate bars, thins or seasonal molded dark chocolates such as our santas, hearts, pumpkins, and turkeys.</p>
		</td>
		<td>
			<h3 class="badge fairtrade">Fair Trade</h3>
			<p>Fair Trade is one way of evaluating if farmers and producers are getting a fair price for a crop. Fair Trade looks at a variety of social and environmental criteria, and certifies a producer of a product. However, quality is not one of the criteria. Therefore Fair Trade, although a trusted system by many companies around the globe, is not an appropriate measure for the chocolate that goes into Fran's products. Since, we purchase chocolate that is made with the highest quality beans available – the chocolate makers that we work with are paying prices for cacao that are well above "fair-trade" standards. For more information, or for our full sourcing statement, please <a href="{{config path="web/unsecure/base_url"}}contact/?form=other">contact us</a>.</p>
		</td>
	</tr>
</table>
<hr class="plain" />
<h3 class="text-uppercase">Allergen Information</h3>
<p>We are happy to provide you with as much information as you and your doctor may need to make an informed decision. We do not want to deprive anyone of our confections unnecessarily! Please call us at {{config path="general/store_information/phone"}} for a complete ingredient list, allergen results from laboratory tests and answers to any questions you may have. We have a licensed and registered dietician on staff to assist you with your inquiries.</p>
<table>
	<tr>
		<td>
			<h3 class="badge soy">Soy</h3>
			<p>Most of our confections contain soy lecithin. According to research completed by the Food Allergy Research and Resource Program (FAARP), soy lecithin does not contain sufficient soy protein to provoke allergic reactions in the majority of soy allergic individuals. We recommend you discuss with your doctor if soy lecithin is safe for you.</p>
		</td>
		<td>
			<h3 class="badge gluten">Gluten</h3>
			<p>We do not use any gluten containing ingredients in our confections. We submitted our truffles, caramels and chocolate covered fruit to the Food Allergy Research and Resource Program (FARRP). No detectable levels of gluten were found (<5ppm).</p>
		</td>
	</tr>
	<tr>
		<td>
			<h3 class="badge milkeggs">Milk &amp; Eggs</h3>
			<p>Our truffles and caramels contain dairy in the form of butter, milk or cream. Our solid milk and white chocolates contain milk. None of our confections contain eggs.</p>
		</td>
		<td>
			<h3 class="badge nuts">Nuts</h3>
			<p>We process macadamias, almonds, hazelnuts, peanuts and coconut in our facility. We take pride in the cleanliness of our production facility but it is possible our confections may contain traces of these nuts.</p>
		</td>
	</tr>
</table>

EOT;


$cmsPageData = array(
	'title' => 'Ingredient Information',
	'root_template' => 'one_column',
	'identifier' => 'ingredient-information',
	'content_heading' => '',
	'stores' => array(0),
	'content' => $content
);

Mage::getModel('cms/page')->setData($cmsPageData)->save();

// delete the old "Our Ingredients" page.
$page = Mage::getModel('cms/page')->load('ingredients');
$page->delete();
$page->save();

$installer->endSetup();