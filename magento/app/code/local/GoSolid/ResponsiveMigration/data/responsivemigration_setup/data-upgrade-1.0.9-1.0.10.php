<?php

$installer = $this;
$installer->startSetup();

// create some CMS blocks for Gift Services section on PDP

$contentFirst = <<<EOT

<p><img src="{{skin url="images/gift-services/gift-message-and-envelope.png"}}" /></p>
<p><strong>Gift Message and Envelope</strong></p>
<p>Your message is important and is delivered with an elegant ivory gift enclosure and matching envelope.</p>

EOT;

$contentSecond = <<<EOT

<p><img src="{{skin url="images/gift-services/gift-boxes-and-ribbons.png"}}" /></p>
<p><strong>Our Signature Gift Boxes and Hand Tied Satin Ribbons</strong></p>
<p>Inspired by the vibrant colors of the seasons, as well as the holidays, our elegant boxes are gifts in and amongst themselves. Our satin ribbons are hand-tied for a perfect bow, every time. Our generous ribbon selection will complement any color scheme.</p>

EOT;

$stores = array(0);

foreach($stores as $store){
	$cmsBlock = Mage::getModel('cms/block');
    if($cmsBlock->load('gift-services-message', 'identifier')) {
        $cmsBlock->setTitle('Gift Services: Gift Message and Envelope')
            ->setIdentifier('gift-services-message')
            ->setStores(array($store))
            ->setIsActive(1)
            ->setContent($contentFirst);
        $cmsBlock->save();
    }
}

foreach($stores as $store){
	$cmsBlock = Mage::getModel('cms/block');
    if($cmsBlock->load('gift-services-boxes', 'identifier')){
        $cmsBlock->setTitle('Gift Services: Gift Boxes and Ribbons')
            ->setIdentifier('gift-services-boxes')
            ->setStores(array($store))
            ->setIsActive(1)
            ->setContent($contentSecond);
        $cmsBlock->save();
    }
}

// Associate the widget frontend model with the new product attributes
$sql = <<<EOSQL

UPDATE `eav_attribute`
SET `frontend_model`='frans/catalog_product_attribute_frontend_widget'
WHERE `attribute_code` IN ('inside_the_box', 'product_care', 'nutrition_ingredients', 'gift_services', 'shipping_packaging');

EOSQL;

$installer->run($sql);

$installer->endSetup();