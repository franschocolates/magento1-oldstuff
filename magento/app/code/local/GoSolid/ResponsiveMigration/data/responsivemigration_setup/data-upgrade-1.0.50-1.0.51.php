<?php

$installer = $this;
$installer->startSetup();

// removes blurb about using 'pieces="..."' in Inside the Box attribute; that feature has some bugs so we're deprecating it
$installer->updateAttribute('catalog_product', 'inside_the_box', array(
	"note" => 'Catalog > Manage Product Pieces. {{block type="frans/catalog_product_widget_insidethebox" template="catalog/product/widget/insidethebox.phtml" pieces="QTY|PIECE_CODE,QTY|PIECE_CODE"}}'
));

$installer->endSetup();