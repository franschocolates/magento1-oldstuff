<?php

$installer = $this;
$installer->startSetup();

// update note verbiage for some of our product attributes
$installer->updateAttribute('catalog_product', 'product_care', array(
	"note" => 'Displays a static block. Example: {{block type="cms/block" block_id="product-care-default"}}'
));

$installer->updateAttribute('catalog_product', 'nutrition_ingredients', array(
	"note" => 'Displays a static block. Example: {{block type="core/template" template="catalog/product/widget/nutritionandingredients.phtml"}}'
));

$installer->updateAttribute('catalog_product', 'shipping_packaging', array(
	"note" => 'Displays a static block. Example: {{block type="cms/block" block_id="shipping-and-packaging-default"}}'
));

// create new CMS blocks
$stores = array(0);

$productCareDefaultContent = <<<EOT
<p>Fran&rsquo;s Gray &amp; Smoked Salt Caramels are hand-crafted, hand-packed and hand-packaged in Seattle, WA from organic ingredients.</p>
<p>For optimal freshness and flavor, please enjoy your Gray &amp; Smoked Salt within 10 days.</p>
<p>Store your caramels in a cool, dry place between 64 &ndash; 68 F or 14 &ndash; 16C.</p>
<p>In order to avoid xxxxx of your chocolates, please avoid placing in the refrigerator.</p>
EOT;

$shippingPackagingDefaultContent = <<<EOT
<p>Nothing is more important to us than your complete satisfaction. That is why we take great care creating our chocolates and hand packing your order. Please note that all of our chocolates are shipped via local, two-day or overnight methods due to perishability.</p>
<p><strong>LOCAL SHIPPING</strong></p>
<p>We offer local flat rate ground shipping for $10 per address to deliveries in Washington, Oregon, California, and Idaho.</p>
<p><strong>SHIPPING CHARGES</strong></p>
<p>2 Day Express and Standard Overnight shipping charges are calculated based on the product total per delivery address.</p>
<p><strong>WARM WEATHER SHIPPING</strong></p>
<p>We guarantee your chocolates will arrive in excellent condition, even in warm weather. We will pack your order with mylar envelopes and gel ice packs when necessary at no additional charge. April-September, we are often unable to ship over the weekend. Orders placed after 1 PM on Wednesday will be held to ship on Monday. We recommend alerting recipients that a sweet gift is on its way so the package is not left outside in warm weather for long periods.</p>
<p><strong>CHOOSE A PREFERRED ARRIVAL DATE</strong></p>
<p>Now or in the future, our shipping calendar allows you to choose when you want your gift to arrive &ndash; today, tomorrow or next year. Upon checkout, choose when you want your gift to arrive in the Delivery Options.</p>
<p><strong>MULTIPLE GIFTS FOR MULTIPLE PEOPLE?</strong></p>
<p>Our Multi-Ship tool makes giving to many simple and easy. Choose Shipping To Multiple Addresses when checking out and easily assign the appropriate gift to your intended recipient.</p>
EOT;
try {
$cmsBlock = Mage::getModel('cms/block');
$cmsBlock->setTitle('Product Care: Default Content')
	->setIdentifier('product-care-default')
	->setStores($stores)
	->setIsActive(1)
	->setContent($productCareDefaultContent);
$cmsBlock->save();

$cmsBlock = Mage::getModel('cms/block');
$cmsBlock->setTitle('Shipping & Packaging: Default Content')
	->setIdentifier('shipping-and-packaging-default')
	->setStores($stores)
	->setIsActive(1)
	->setContent($shippingPackagingDefaultContent);
$cmsBlock->save();
} catch(Exception $e) {}

$installer->endSetup();