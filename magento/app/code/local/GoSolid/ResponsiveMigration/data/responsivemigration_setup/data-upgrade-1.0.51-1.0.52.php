<?php

$installer = $this;
$installer->startSetup();

// clean up duplicate configurable product URLs
$products = Mage::getModel('catalog/product')->getCollection();
$products->addAttributeToFilter('type_id', 'configurable');
foreach($products as $product)
{
	$product->save();
}

$installer->endSetup();