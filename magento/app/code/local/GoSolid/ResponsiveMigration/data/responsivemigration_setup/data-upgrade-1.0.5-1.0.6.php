<?php

$installer = $this;
$installer->startSetup();

// change "Pieces Description" to "What's Inside the Box"
$installer->updateAttribute('catalog_product', 'pieces_description', array(
	'attribute_code' => 'inside_the_box',
	'frontend_label' => "What's Inside the Box"
));
$attributeId = $this->getAttributeId('catalog_product', 'inside_the_box');
$attribute = Mage::getModel('eav/entity_attribute')->load($attributeId);
$attribute->setStoreLabels(array("What's Inside the Box", "What's Inside the Box"))->save();

$installer->addAttribute("catalog_product", "product_care",  array(
	"type"     => "text",
	"backend"  => "",
	"frontend" => "",
	"label"    => "Product Care",
	"input"    => "textarea",
	"class"    => "",
	"global"   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
	"visible" => true,
	"required" => false,
	"user_defined" => true,
	"unique" => false,
	"group" => "General",
	"position" => 120,
	"used_in_product_listing" => false
));
$installer->updateAttribute('catalog_product', 'product_care', array(
	"is_wysiwyg_enabled" => 1,
	"is_visible_on_front" => 1,
	"is_html_allowed_on_front" => 1
));

$installer->addAttribute("catalog_product", "nutrition_ingredients",  array(
	"type"     => "text",
	"backend"  => "",
	"frontend" => "",
	"label"    => "Nutrition & Ingredients",
	"input"    => "textarea",
	"class"    => "",
	"global"   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
	"visible" => true,
	"required" => false,
	"user_defined" => true,
	"unique" => false,
	"group" => "General",
	"position" => 130,
	"used_in_product_listing" => false
));
$installer->updateAttribute('catalog_product', 'nutrition_ingredients', array(
	"is_wysiwyg_enabled" => 1,
	"is_visible_on_front" => 1,
	"is_html_allowed_on_front" => 1
));

$installer->addAttribute("catalog_product", "gift_services",  array(
	"type"     => "text",
	"backend"  => "",
	"frontend" => "",
	"label"    => "Gift Services",
	"input"    => "textarea",
	"class"    => "",
	"global"   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
	"visible" => true,
	"required" => false,
	"user_defined" => true,
	"unique" => false,
	"group" => "General",
	"position" => 140,
	"used_in_product_listing" => false
));
$installer->updateAttribute('catalog_product', 'gift_services', array(
	"is_wysiwyg_enabled" => 1,
	"is_visible_on_front" => 1,
	"is_html_allowed_on_front" => 1
));

$installer->addAttribute("catalog_product", "shipping_packaging",  array(
	"type"     => "text",
	"backend"  => "",
	"frontend" => "",
	"label"    => "Shipping & Packaging",
	"input"    => "textarea",
	"class"    => "",
	"global"   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
	"visible" => true,
	"required" => false,
	"user_defined" => true,
	"unique" => false,
	"group" => "General",
	"position" => 150,
	"used_in_product_listing" => false
));
$installer->updateAttribute('catalog_product', 'shipping_packaging', array(
	"is_wysiwyg_enabled" => 1,
	"is_visible_on_front" => 1,
	"is_html_allowed_on_front" => 1

));

$installer->endSetup();