<?php
// add new "Product Grid Layout" attribute for categories

$installer = $this;
$installer->startSetup();

$attr = array (
	'group' => 'Display Settings',
	'type' => 'int',
	'input' => 'select',
	'label' => 'Product Grid Layout',
	'source' => 'frans/attribute_source_productgridlayout',
	'required' => '1',
	'user_defined' => '1',
	'default' => '1',
	'unique' => '0',
	'note' => 'The frontend arrangement of product tiles. Each layout expects the product count to be a multiple of some number, and repeats until all products are shown.',
	'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
	'visible_on_front' => '1',
	'is_html_allowed_on_front' => '0',
	'is_used_for_price_rules' => '1',
	'filterable_in_search' => '1',
	'used_in_product_listing' => '1',
	'used_for_sort_by' => '0',
	'is_configurable' => '1',
	'visible_in_advanced_search' => '1',
	'position' => '60',
	'sort_order' => 60
);

$installer->addAttribute('catalog_category', 'product_grid_layout', $attr);

$installer->endSetup();