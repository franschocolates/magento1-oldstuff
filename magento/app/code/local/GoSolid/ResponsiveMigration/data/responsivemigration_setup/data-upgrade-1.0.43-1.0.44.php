<?php

$installer = $this;
$installer->startSetup();

// Change existing All Products CMS block to reference new all-products.phtml template
$cmsBlock = Mage::getModel('cms/block')->load('All Products', 'title');
$content = $cmsBlock->getContent();
$updatedContent = str_replace('catalog/category/frans-all-products.phtml', 'catalog/category/list/layout/all-products.phtml', $content);
$cmsBlock->setContent($updatedContent)->save();

$installer->endSetup();