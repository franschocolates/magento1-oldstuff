<?php

$installer = $this;
$installer->startSetup();

// delete the CMS page used by the old home page (it's been replaced by a .phtml)

$page = Mage::getModel('cms/page')->load('home');
$page->delete();
$page->save();

$installer->endSetup();