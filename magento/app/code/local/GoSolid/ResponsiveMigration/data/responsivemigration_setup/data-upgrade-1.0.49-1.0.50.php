<?php

$installer = $this;
$installer->startSetup();

// remove Loved Items attribute, we're not using this after all
$installer->removeAttribute('catalog_product', 'loved_items');

// remove the Is Cross-Sell attribute before creating it, if it already exists for some reason
$installer->removeAttribute('catalog_product', 'is_cross_sell');

// create Is a Cross-Sell Category attribute
$attr = array (
	'group' => 'General Information',
	'type' => 'int',
	'input' => 'select',
	'source'  => 'eav/entity_attribute_source_boolean',
	'label' => 'Is a Cross-Sell Category',
	'required' => '0',
	'user_defined' => '1',
	'unique' => '0',
	'note' => 'The products in a cross-sell category appear in a special section on its parent category page. Do not include cross-sell categories in the navigation menu.',
	'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
	'visible_on_front' => '1',
	'is_used_for_price_rules' => '1',
	'filterable_in_search' => '1',
	'used_in_product_listing' => '1',
	'used_for_sort_by' => '0',
	'is_configurable' => '1',
	'position' => '30',
	'sort_order' => 30
);

$installer->addAttribute('catalog_category', 'is_cross_sell', $attr);

// add new columns to retail store table

$installer->getConnection()->addColumn('frans_retail_store', 'cross_sell_label', array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 255,
    'nullable' => false,
    'comment' => 'Cross Sell Labels'
));

$installer->getConnection()->addColumn('frans_retail_store', 'cross_sell_skus', array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 1024,
    'nullable' => false,
    'comment' => 'Cross Sell SKUs'
));

//$sql = <<<EOSQL
//	ALTER TABLE `frans_retail_store`
//	ADD COLUMN `cross_sell_label` VARCHAR(255) NOT NULL AFTER `subtitle`,
//	ADD COLUMN `cross_sell_skus` TEXT(1024) NOT NULL AFTER `cross_sell_label`;
//EOSQL;
//
//$installer->run($sql);

$installer->endSetup();