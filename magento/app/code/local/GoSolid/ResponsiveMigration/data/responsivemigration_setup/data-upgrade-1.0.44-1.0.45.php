<?php

$installer = $this;
$installer->startSetup();

// Remove Category ID list from All Products CMS block
$cmsBlock = Mage::getModel('cms/block')->load('All Products', 'title');
$content = $cmsBlock->getContent();

// get the category_ids attribute value, a comma-separated list of category IDs
$categoryIds = preg_replace("/.*category_ids=\"/", '', $content);
$categoryIds = preg_replace("/\".+/", '', $categoryIds);

// remove the category_ids attribute from the CMS block content and save it
$updatedContent = preg_replace("/ category_ids=\".+?\"/", '', $content);
$cmsBlock->setContent($updatedContent)->save();

// save the category IDs string into config
$config = new Mage_Core_Model_Config();
$config->saveConfig('frans/all_products_page/category_ids', $categoryIds, 'default', 0);

$installer->endSetup();