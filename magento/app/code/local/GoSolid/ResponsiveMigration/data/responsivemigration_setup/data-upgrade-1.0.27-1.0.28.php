<?php

$installer = $this;
$installer->startSetup();

// Create the frans_configurable_product_urls table
try{
    $table = $installer->getConnection()
        ->newTable('frans_configurable_product_urls')
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
            'auto_increment' => true,
            'unsigned' => true,
            'primary' => true,
        ), 'ID')
        ->addColumn('configurable_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => true,
            'default' => null
        ), 'Configurable Product ID')
        ->addColumn('simpe_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => true,
            'default' => null
        ), 'Simple Product ID')
        ->addColumn('category_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => true,
            'default' => null
        ), 'Category ID')
        ->addColumn('path', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
            'nullable' => true,
            'default' => null
        ), 'URL path');

    $installer->getConnection()->createTable($table);
} catch (Exception $e) {}



//$sql = <<<EOSQL
//CREATE TABLE `frans_configurable_product_urls` (
//  `id` int(10) NOT NULL AUTO_INCREMENT,
//  `configurable_id` int(10) DEFAULT NULL,
//  `simple_id` int(10) DEFAULT NULL,
//  `category_id` int(10) DEFAULT NULL,
//  `path` varchar(255) DEFAULT NULL,
//  PRIMARY KEY (`id`)
//) ENGINE=InnoDB DEFAULT CHARSET=latin1
//EOSQL;
//
//$installer->run($sql);

$installer->endSetup();