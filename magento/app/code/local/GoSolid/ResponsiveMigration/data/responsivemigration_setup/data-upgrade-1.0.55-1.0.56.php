<?php

$installer = $this;
$installer->startSetup();

// delete the frans_seasonal_theme table if it exists, this was a table created briefly during development and no longer used
// also delete a config setting related to that
$sql = <<<EOSQL
DROP TABLE IF EXISTS `frans_seasonal_theme`;
DELETE FROM `core_config_data` WHERE `path`='frans/seasonal_theme/seasonal_theme';
EOSQL;

$installer->run($sql);

// we're not using the Banner module for managing Hero content anymore, so delete all applicable banners
$banners = Mage::getModel('banner/banner')->getCollection()->addFieldToFilter('type', 'homepage_hero');
foreach($banners as $banner)
{
	$banner->delete();
}

// delete the Home CMS page, this is controlled entirely by layout (cms.xml) now
Mage::getModel('cms/page')->load('home', 'identifier')->delete();

$installer->endSetup();