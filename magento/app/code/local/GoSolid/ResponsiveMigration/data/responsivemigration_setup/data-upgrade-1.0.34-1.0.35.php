<?php

$installer = $this;
$installer->startSetup();

// set a catalog placeholder image, deleting it first if one already exists
$sql = <<<EOSQL
DELETE FROM `core_config_data` WHERE `path`='catalog/placeholder/image_placeholder';
REPLACE INTO `core_config_data` (`scope`, `scope_id`, `path`, `value`)
VALUES ('default', 0, 'catalog/placeholder/image_placeholder', 'default/image.jpg');
EOSQL;

$installer->run($sql);

$installer->endSetup();