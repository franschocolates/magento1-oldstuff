<?php
// add new "Product Grid Layout" attribute for categories

$installer = $this;
$installer->startSetup();

$attr = array (
	'group' => 'Display Settings',
	'type' => 'int',
	'input' => 'select',
	'label' => 'Child Category Layout',
	'source' => 'frans/attribute_source_childcategorylayout',
	'required' => '1',
	'user_defined' => '1',
	'default' => '1',
	'unique' => '0',
	'note' => 'The frontend arrangement of child category tiles. Most layouts expect the category count to be a multiple of some number, and repeat until all are shown.',
	'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
	'visible_on_front' => '1',
	'is_html_allowed_on_front' => '0',
	'is_used_for_price_rules' => '1',
	'filterable_in_search' => '1',
	'used_in_product_listing' => '1',
	'used_for_sort_by' => '0',
	'is_configurable' => '1',
	'visible_in_advanced_search' => '1',
	'position' => '70',
	'sort_order' => 70
);

$installer->addAttribute('catalog_category', 'child_category_layout', $attr);

$installer->endSetup();