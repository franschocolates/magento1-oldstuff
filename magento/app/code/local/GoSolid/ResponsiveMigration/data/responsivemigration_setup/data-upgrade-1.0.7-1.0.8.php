<?php

$installer = $this;
$installer->startSetup();

$tableName = $installer->getTable('frans/productPiece');

$sql = <<<EOSQL
CREATE TABLE IF NOT EXISTS `{$tableName}` (
  `piece_id` int(11) NOT NULL AUTO_INCREMENT,
  `piece_code` varchar(155) DEFAULT NULL,
  `piece_label` varchar(255) DEFAULT NULL,
  `piece_image` varchar(275) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`piece_id`),
  UNIQUE KEY `piece_code` (`piece_code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
EOSQL;

$installer->run($sql);

$installer->endSetup();