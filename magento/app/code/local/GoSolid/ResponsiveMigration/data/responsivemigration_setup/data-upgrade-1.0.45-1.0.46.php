<?php

$installer = $this;
$installer->startSetup();

$attr = array (
	'group'             => 'General',
	'type'              => 'int',
	'frontend'          => '',
	'backend'           => '',
	'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
	'required'          => true,
	'visible_on_front'  => false,
	'input'             => 'select',
	'label'             => 'Default Child Product',
	'apply_to'          => 'configurable',
	'unique'            => false,
	'is_configurable'   => false,
	'sort_order'        => 10,
	'source'            => 'frans/catalog_product_defaultchild_select',
	'frontend_input'    => 'select',
	'note'              => 'Choose a default child (must be enabled) for this configurable product.'
);

//TODO: Remove this line before adding to SVN
$this->removeAttribute('catalog_product', 'default_child');

$this->addAttribute('catalog_product','default_child',$attr);

// make sure the attribute only applies to configurable product types
$installer->run("UPDATE catalog_eav_attribute SET apply_to = 'configurable' WHERE attribute_id = (SELECT attribute_id FROM eav_attribute WHERE attribute_code = 'default_child');");

// set defaults for the new attribute
$currentStore = Mage::app()->getStore()->getId();
Mage::app()->getStore()->setId(Mage_Core_Model_App::ADMIN_STORE_ID);
$products = Mage::getModel('catalog/product')->getCollection();
foreach($products as $product){
	$productId = $product->getId();
	$loadedProduct = Mage::getModel('catalog/product')->load($productId);
	$loadedProduct->setDefaultChild(0);
	$loadedProduct->save();
}
Mage::app()->getStore()->setId($currentStore);

$installer->endSetup();