<?php

$installer = $this;
$installer->startSetup();

// create the new Event Favors page URL rewrite
$urlRewrite = Mage::getModel('core/url_rewrite');

if(!$urlRewrite->load('event-favors-page', 'id_path')->getIdPath()){
    $urlRewrite->setIsSystem(0)
        ->setIdPath('event-favors-page')
        ->setRequestPath('event-favors')
        ->setTargetPath('frans/eventfavors');
    $urlRewrite->save();
}

$installer->endSetup();