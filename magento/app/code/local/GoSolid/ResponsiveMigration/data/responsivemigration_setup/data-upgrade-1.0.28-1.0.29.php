<?php

$installer = $this;
$installer->startSetup();

// add a configurable_product_id column to the quote_item table
    $installer->getConnection()
        ->addColumn($installer->getTable('sales/quote_item'), 'configurable_product_id', array(
            'TYPE'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
            'COMMENT'   => 'ID of the configurable product',
            'required' => 0,
            'comment'   => 'Configurable Product IDs'
        ));


$installer->endSetup();