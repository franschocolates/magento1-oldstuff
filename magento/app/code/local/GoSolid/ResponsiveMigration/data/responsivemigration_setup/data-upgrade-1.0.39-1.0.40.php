<?php

$installer = $this;
$installer->startSetup();

// remove requirement for description attribute
$installer->updateAttribute('catalog_product', 'description', array(
	"is_required" => 0
));

// hide short description attribute from backend
$installer->updateAttribute('catalog_product', 'short_description', array(
	"is_visible" => 0,
	"is_required" => 0
));

$installer->endSetup();