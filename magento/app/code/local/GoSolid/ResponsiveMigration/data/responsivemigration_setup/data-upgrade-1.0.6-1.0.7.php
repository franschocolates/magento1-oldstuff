<?php

$installer = $this;
$installer->startSetup();

// make Description attribute visible on frontend (so it will appear in PDP attribute accordion)
$installer->updateAttribute('catalog_product', 'description', array(
	"is_visible_on_front" => 1
));

$installer->endSetup();