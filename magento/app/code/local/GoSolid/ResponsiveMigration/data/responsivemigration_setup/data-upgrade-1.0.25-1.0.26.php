<?php

$installer = $this;
$installer->startSetup();

// create the Piece Count attribute
$installer->addAttribute("catalog_product", "piece_count",  array(
	"type"     => "int",
	"backend"  => "",
	"frontend" => "",
	"label"    => "Piece Count",
	"input"    => "select",
	"class"    => "",
	"global"   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
	"visible" => true,
	"required" => false,
	"user_defined" => true,
	"unique" => false,
	"group" => "General",
	"position" => 120,
	"used_in_product_listing" => false,
	'is_configurable' => '1',
	'apply_to' => 'configurable'
));
$installer->updateAttribute('catalog_product', 'piece_count', array(
	"is_visible_on_front" => 1
));

$installer->endSetup();