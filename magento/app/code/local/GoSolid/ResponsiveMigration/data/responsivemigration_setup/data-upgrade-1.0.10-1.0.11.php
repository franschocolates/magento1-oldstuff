<?php

$installer = $this;
$installer->startSetup();

// remove WYSIWYG buttons for PDP accordion attributes, and add instructive notes
$installer->updateAttribute('catalog_product', 'inside_the_box', array(
	"is_wysiwyg_enabled" => 0,
	"note" => 'Catalog > Manage Product Pieces. {{block type="frans/catalog_product_widget_insidethebox" template="catalog/product/widget/insidethebox.phtml" pieces="QTY|PIECE_CODE,QTY|PIECE_CODE"}} (change pieces to products & piece codes to skus for products instead)'
));

$installer->updateAttribute('catalog_product', 'product_care', array(
	"is_wysiwyg_enabled" => 0,
	"note" => 'Displays a static block. Example: {{block type="cms/block" template="catalog/product/widget/giftservices.phtml" block_id="BLOCK_ID"}}'
));

$installer->updateAttribute('catalog_product', 'nutrition_ingredients', array(
	"is_wysiwyg_enabled" => 0
));

$installer->updateAttribute('catalog_product', 'gift_services', array(
	"is_wysiwyg_enabled" => 0,
	"note" => 'Displays multiple CMS static blocks by ID. See CMS > Static Blocks. Example: {{block type="frans/catalog_product_widget_giftservices" template="catalog/product/widget/giftservices.phtml" blocks="BLOCK_ID,BLOCK_ID"}}'
));

$installer->updateAttribute('catalog_product', 'shipping_packaging', array(
	"is_wysiwyg_enabled" => 0,
	"note" => 'Displays a static block. Example: {{block type="cms/block" template="catalog/product/widget/giftservices.phtml" block_id="BLOCK_ID"}}'
));

$installer->endSetup();