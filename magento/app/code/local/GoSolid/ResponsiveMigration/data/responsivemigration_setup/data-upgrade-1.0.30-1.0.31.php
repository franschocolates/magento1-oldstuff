<?php

$installer = $this;
$installer->startSetup();

// add new "Action Label" attribute for categories

$installer->addAttribute('catalog_category', 'action_label', array(
	'group' => 'General Information',
	'type' => 'varchar',
	'input' => 'text',
	'label' => 'Action Label',
	'required' => false,
	'user_defined' => false,
	'default' => 'Shop Now',
	'unique' => false,
	'note' => 'Call to action displayed in category tiles',
	'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
	'visible' => true,
	'visible_on_front' => true,
	'used_in_product_listing' => false,
	'is_html_allowed_on_front' => false,
	'position' => '3',
	'sort_order' => 3
));

// add new "Inset Color" attribute for categories

$installer->addAttribute('catalog_category', 'inset_color', array(
	'group' => 'General Information',
	'type' => 'varchar',
	'input' => 'text',
	'label' => 'Inset Color',
	'required' => false,
	'user_defined' => false,
	'default' => 'FFFFFF',
	'unique' => false,
	'note' => 'Background color (hex value) of tile inset',
	'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
	'visible' => true,
	'visible_on_front' => true,
	'used_in_product_listing' => false,
	'is_html_allowed_on_front' => false,
	'position' => '113',
	'sort_order' => 113
));

// add new "Inset Opacity" attribute for categories

$installer->addAttribute('catalog_category', 'inset_opacity', array(
	'group' => 'General Information',
	'type' => 'varchar',
	'input' => 'text',
	'label' => 'Inset Opacity',
	'required' => false,
	'user_defined' => false,
	'default' => '90',
	'unique' => false,
	'note' => 'Opacity of tile inset background (percentage)',
	'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
	'visible' => true,
	'visible_on_front' => true,
	'used_in_product_listing' => false,
	'is_html_allowed_on_front' => false,
	'position' => '117',
	'sort_order' => 117
));

// add "Tile Image" attribute for categories

$installer->addAttribute('catalog_category', 'tile_image', array(
	'group' => 'General Information',
	'type' => 'varchar',
	'input' => 'image',
	'label' => 'Tile Image',
	'required' => false,
	'user_defined' => false,
	'unique' => false,
	'note' => 'Image dimensions vary depending on size of tile',
	'backend' => 'catalog/category_attribute_backend_image',
	'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
	'visible' => true,
	'visible_on_front' => true,
	'used_in_product_listing' => false,
	'is_html_allowed_on_front' => false,
	'position' => '63',
	'sort_order' => 63
));

// add "Action Image" attribute for categories

$installer->addAttribute('catalog_category', 'action_image', array(
	'group' => 'General Information',
	'type' => 'varchar',
	'input' => 'image',
	'label' => 'Action Image',
	'required' => false,
	'user_defined' => false,
	'unique' => false,
	'note' => 'Image displayed in category tile insets between description and action label',
	'backend' => 'catalog/category_attribute_backend_image',
	'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
	'visible' => true,
	'visible_on_front' => true,
	'used_in_product_listing' => false,
	'is_html_allowed_on_front' => false,
	'position' => '65',
	'sort_order' => 65
));

// add "All Products Tile Image" attribute for categories

$installer->addAttribute('catalog_category', 'all_products_tile_image', array(
	'group' => 'General Information',
	'type' => 'varchar',
	'input' => 'image',
	'label' => 'All Products Tile Image',
	'required' => false,
	'user_defined' => false,
	'unique' => false,
	'note' => 'Used on All Products page. Image dimensions must be 390x350 pixels',
	'backend' => 'catalog/category_attribute_backend_image',
	'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
	'visible' => true,
	'visible_on_front' => true,
	'used_in_product_listing' => false,
	'is_html_allowed_on_front' => false,
	'position' => '67',
	'sort_order' => 67
));

// rename "Category Image Style" to "Tile Image Style"

$installer->updateAttribute('catalog_category', 'category_image_style', array(
	'attribute_code'    => 'tile_image_style',
	'frontend_label'    => 'Tile Image Style',
	'note'              => 'Light Image Style: makes text dark, Dark Image Style: makes text lighter'
));

$installer->endSetup();