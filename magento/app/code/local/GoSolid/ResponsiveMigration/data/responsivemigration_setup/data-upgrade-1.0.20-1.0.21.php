<?php

$installer = $this;
$installer->startSetup();

// Change existing News CMS page (with "articles" URL key) to have a URL key of "news"
$cmsPage = Mage::getModel('cms/page')->load('story', 'identifier');

// New content for Fran's Story page
$content = <<<EOHTML

{{block type="core/template" template="cms/includes/frans-story-header.phtml"}}
<h1 class="page-title">Say Hello to Fran Bigelow</h1>
<hr />
<p>It has been said that the passion of an artist is found deep within the soul. It is what separates those who settle for good from those who are compelled to see greatness and seek perfection.</p>
<p>These artists have a quiet obsession and intense dedication to create the exceptional. It is soft and subtle, and is rarely noticed upon first glance. It&rsquo;s only as the art begins to unfold that you get an inkling of the magic taking place before your eyes.</p>
<blockquote>Her love of chocolate is intense, and her pursuit of the extraordinary is evident.</blockquote>
<p>For the lucky few who have met Fran Bigelow, you&rsquo;ll find that underneath her warm and engaging exterior lies both a great artist and a master chocolatier. Her love of chocolate is intense, and her pursuit of the extraordinary is evident. A feast for all your senses, her chocolates are sublime and the hand packaging of her confections is almost as delicious.</p>
<p class="aside"><img src="{{media url="wysiwyg/bg-frans-truffle.png"}}" /></aside>
<p>She is also a person who loves to celebrate. She celebrates life and the pursuit of joy in any given situation, regardless of a specific occasion. From the way the chocolates are presented in the gorgeous gift boxes to savoring the first bite of one of her creations, each element is truly infused with love, care and consideration. And it&rsquo;s why Fran&rsquo;s has become a key component to so many thoughtful exchanges, joyful declarations, special occasions and countless other celebrations.</p>
<p>A visit to Paris inspired this European-like passion for pure flavors and simple, yet exquisite ingredients. She opened her first patisserie and chocolate shop thirty-two years ago to share that philosophy of joie-de-vivre. Many confections later, she has been credited for sparking the artisan chocolate renaissance in the United States, and is considered one of the best chocolatiers in the nation. Her pursuit of maintaining the purity of chocolate and pairing it with only the finest ingredients has been an inspiration to both chocolate artisans and aficionados alike.</p>
<p>From the tantalizing light ganache found in her luscious truffles, to the bold, and slightly addictive gray and smoked salt caramels, take a moment to note the textures and harmonious flavors that layer themselves, one on top of the other. It is the magic of this experience that compels Fran&rsquo;s to continue to seek the extraordinary.</p>

EOHTML;

// Update page with new content
$cmsPage->setIdentifier('frans-story')
	->setContent($content)
	->setStores(array(1))
	->save();

// Create a redirect so links to the old URL will reach the page at its new location
$sql = <<<EOSQL

REPLACE INTO `core_url_rewrite` (`store_id`, `id_path`, `request_path`, `target_path`, `is_system`, `options`)
VALUES (1, 'frans-story-page', 'story', 'frans-story', 0, 'RP');

EOSQL;

$installer->run($sql);

$installer->endSetup();