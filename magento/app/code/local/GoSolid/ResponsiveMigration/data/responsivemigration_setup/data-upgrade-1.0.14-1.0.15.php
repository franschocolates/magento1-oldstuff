<?php

$installer = $this;
$installer->startSetup();

// delete the CMS page used by the old Boutiques page (it's been replaced by a .phtml)

$page = Mage::getModel('cms/page')->load('locations');
$page->delete();
$page->save();

// add URL rewrite for new Stores page

$sql = <<<EOSQL

REPLACE INTO `core_url_rewrite` (`store_id`, `id_path`, `request_path`, `target_path`, `is_system`)
VALUES (1, 'stores-page', 'stores', 'frans/stores', 0);

REPLACE INTO `core_url_rewrite` (`store_id`, `id_path`, `request_path`, `target_path`, `options`, `is_system`)
VALUES (1, 'locations-page', 'locations', 'stores', 'RP', 0);

EOSQL;

$installer->run($sql);

$installer->endSetup();