<?php

class GoSolid_OrderQueues_Adminhtml_OrderqueuesController extends Mage_Adminhtml_Controller_Action
{
	protected function _initAction() {
		$this->loadLayout();

		//Optional
		//$this->_setActiveMenu('system/pstadmin');
		//$this->_title($this->__('orderQueues'))->_title($this->__('PST Admin'))->_title($this->__('Event Type Management'));

		return $this;
	}   
 
	public function indexAction() {

        $this->_initAction()
			->renderLayout();

	}

	public function editAction() {
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('orderQueues/orderQueue')->load($id);
		
		Mage::register('orderqueues_data', $model);
		
		if ($model->getId() || $id == 0) {
				$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
				if (!empty($data)) {
					$model->setData($data);
				}

				$this->loadLayout();
				
				//Optional for WYSIWYG must change loading blocks to XML
				//$this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
				//$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
				
				$this->_addContent($this->getLayout()->createBlock('orderQueues/adminhtml_orderQueues_edit'))
					->_addLeft($this->getLayout()->createBlock('orderQueues/adminhtml_orderQueues_edit_tabs'));
					
				$this->renderLayout();

			}
			else
			{
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('orderQueues')->__('Record does not exist'));
				$this->_redirect('*/*/');
			}

	}
	
	
	public function newAction() {

        $this->_forward('edit');

	}
	
	public function saveAction() {

        $session = Mage::getSingleton('adminhtml/session');
		if ($data = $this->getRequest()->getPost()) {
			
			$model = Mage::getModel('orderQueues/orderQueue');
			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));

			try {


				$model->save();

				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('orderQueues')->__('Record was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
				
			}
			catch(Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
			}
			
		}
		
		//on save and edit
		 if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('page_id' => $model->getId(), '_current'=>true));
                    return;
                }
		
		
		//no agency to save.
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('orderQueues')->__('Record not found.'));
        $this->_redirect('*/*/');
		
	}


    public function massChangeQueueAction(){
        $session = Mage::getSingleton('adminhtml/session');
        if ($data = $this->getRequest()->getPost()) {
            $orders = Mage::getModel('sales/order')
                            ->getCollection()
                            ->addAttributeToFilter('entity_id', array ('in' => $data['order_ids']));
            $updated = 0;
            try{
                foreach ($orders as $order)
                {
                    try
                    {
                        $order->setProcessingQueue($data['queue']);
                        $order->save();
                        $updated++;
                    } catch (Exception $e) {
                        throw new Exception('Unable to save ' . $order->getIncrementId());
                    }
                }
                $this->_getSession()->addSuccess($this->__('Changed total of %d order(s) to new queue.', $updated));
            }catch(Exception $ex){
                $this->_getSession()->addError($ex->getMessage());
            }
            $this->_redirect('adminhtml/sales_order/index');
        }
    }

    public function saveQueueAction()
    {
        $response = false;
        try {
            $request = $this->getRequest()->getParams();
            $order = Mage::getModel('sales/order')->load($request['order_id']);
            $order->setProcessingQueue($request['queue']);
            $order->save();

            $response = array('success' => true);
        }
        catch (Mage_Core_Exception $e) {
            $response = array(
                'error'     => true,
                'message'   => $e->getMessage(),
            );
        }
        catch (Exception $e) {
            $response = array(
                'error'     => true,
                'message'   => $this->__("Cannot update Processing Queue.")
            );
        }
        if (is_array($response)) {
            $response = Mage::helper('core')->jsonEncode($response);
            $this->getResponse()->setBody($response);
        }
    }
	
}