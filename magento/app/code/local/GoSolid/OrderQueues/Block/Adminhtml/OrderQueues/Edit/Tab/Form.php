<?php

class GoSolid_OrderQueues_Block_Adminhtml_OrderQueues_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

	protected function _prepareForm()
	{

		$form = new Varien_Data_Form(); 
		$this->setForm($form);
		$fieldset = $form->addFieldset('orderQueues_fs', array('legend'=>Mage::helper('orderQueues')->__('Order Queues')));

        //text
        $fieldset->addField('label', 'text', array(
            'title'     => Mage::helper('orderQueues')->__('Label'),
            'label'     => Mage::helper('orderQueues')->__('Label'),
            'name'      => 'label',
            'class'     => 'required-entry',
            'required'  => true,
            //'note'	  => 'My Note Here',
        ));

        $fieldset->addField('type', 'select',array(
            'label'     => Mage::helper('orderQueues')->__('Type'),
            'required'  => true,
            'name'      => 'type',
            'options'   => Mage::getSingleton('orderQueues/orderQueue')->__getQueueTypes(),
        ));

        $fieldset->addField('is_active', 'select',array(
            'label'     => Mage::helper('orderQueues')->__('Status'),
            'required'  => true,
            'name'      => 'is_active',
            'options'   => Mage::getSingleton('orderQueues/orderQueue')->__getStatuses(),
        ));

	
		if ( Mage::getSingleton('adminhtml/session')->getOrderqueuesData() )
		{
	    	$form->setValues(Mage::getSingleton('adminhtml/session')->getOrderqueuesData());
	    	Mage::getSingleton('adminhtml/session')->getOrderqueuesData(null);
		}
		elseif(Mage::registry('orderqueues_data'))
		{
	    	$form->setValues(Mage::registry('orderqueues_data')->getData());
		}
		
		return parent::_prepareForm();
	}
  
}