<?php
class GoSolid_OrderQueues_Block_Adminhtml_OrderQueues_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{

		parent::__construct();
		$this->setId('orderQueuesGrid');
		
		//TODO: Set default sort
		//$this->setDefaultSort('id');
		//$this->setDefaultDir('ASC');
		
		//TODO: Optional
		//$this->setSaveParametersInSession(true);
		//$this->setDefaultFilter(array('status' => 'Enabled'));
	}
 
	protected function _prepareCollection()
	{
		$collection = Mage::getModel('orderQueues/orderQueue')->getCollection();

		$this->setCollection($collection);
		return parent::_prepareCollection();
	}	
		
		
	protected function _prepareColumns()
  	{
        $this->addColumn('label', array(
            'header'    => Mage::helper('orderQueues')->__('Label'),
            'align'     =>'left',
            'index'     => 'label',
        ));

        $this->addColumn('type', array(
            'header'    => Mage::helper('orderQueues')->__('Type'),
            'align'     =>'left',
            'width'     => '350px',
            'index'     => 'type',
            'type'      => 'options',
            'options'   => Mage::getSingleton('orderQueues/orderQueue')->__getQueueTypes()
        ));

        $this->addColumn('is_active', array(
            'header'    => Mage::helper('orderQueues')->__('Status'),
            'align'     =>'left',
            'width'     => '150px',
            'index'     => 'is_active',
            'type'	  => 'options',
            'options'   => Mage::getSingleton('orderQueues/orderQueue')->__getStatuses()
        ));
		  

		  return parent::_prepareColumns();
	}
	
	public function getRowUrl($row)
  	{
    	return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  	}
}