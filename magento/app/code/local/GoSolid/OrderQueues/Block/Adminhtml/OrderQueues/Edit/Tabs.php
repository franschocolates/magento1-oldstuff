<?php

class GoSolid_OrderQueues_Block_Adminhtml_OrderQueues_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{  
	public function __construct()
  	{
		$this->setId('orderQueues_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('orderQueues')->__('General'));
		parent::__construct();
  	}
	

	protected function _beforeToHtml()
  	{

	 $this->addTab('form_section', array(
          	'label'     => Mage::helper('orderQueues')->__('General'),
         	'title'     => Mage::helper('orderQueues')->__('General'),
          	'content'   =>  $this->getLayout()->createBlock('orderQueues/adminhtml_orderQueues_edit_tab_form')->toHtml(),
			'active'    => true
      ));


      return parent::_beforeToHtml();
  }
  
}