<?php
 
class GoSolid_OrderQueues_Block_Adminhtml_OrderQueues extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {

	$this->_controller = 'adminhtml_orderQueues';
	$this->_blockGroup = 'orderQueues';
	$this->_headerText = Mage::helper('orderQueues')->__("Manage Order Queues");
	$this->_addButtonLabel = Mage::helper('orderQueues')->__("Add");
    parent::__construct();
    
  }
}