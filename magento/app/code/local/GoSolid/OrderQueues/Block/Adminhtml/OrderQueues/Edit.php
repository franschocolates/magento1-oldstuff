<?php

class GoSolid_OrderQueues_Block_Adminhtml_OrderQueues_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'orderQueues';
        $this->_controller = 'adminhtml_orderQueues';
        
        // Optional
        //$this->_updateButton('save', 'label', Mage::helper('event')->__('Save'));
		$this->_removeButton('delete');
		$this->_removeButton('reset');
		
		$this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('orderQueues')->__('Save and Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);
		
        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
			
        ";
    }

	public function getHeaderText()
    {  
        if( Mage::registry('orderqueues_data') && Mage::registry('orderqueues_data')->getId() ) {
            return Mage::helper('orderQueues')->__("Editing Queue: " . Mage::registry('orderqueues_data')->getLabel());
		} else {
            return Mage::helper('orderQueues')->__('Add Queue');
        }
    }

}