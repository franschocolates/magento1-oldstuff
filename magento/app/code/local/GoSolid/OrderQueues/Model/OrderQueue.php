<?php
class GoSolid_OrderQueues_Model_OrderQueue extends Mage_Core_Model_Abstract 
{
    const QUEUE_TYPE_PROCESSING = 'processing';
    const QUEUE_TYPE_LABEL_PROCESSING = 'Processing';

    const QUEUE_STATUS_ENABLED = 1;
    const QUEUE_STATUS_ENABLED_LABEL = 'Enabled';

    const QUEUE_STATUS_DISABLED = 2;
    const QUEUE_STATUS_DISABLED_LABEL = 'Disabled';

    public function _construct()
    {
        parent::_construct();
        $this->_init('orderQueues/orderQueue');
    }

    public function __getQueueTypes(){
        return array(null => "-- Please Select --",
                     self::QUEUE_TYPE_PROCESSING => self::QUEUE_TYPE_LABEL_PROCESSING);
    }

    public function __getQueueTypesOptionArray(){

    }

    public function __getStatuses(){
        return array(null => "-- Please Select --",
                     self::QUEUE_STATUS_ENABLED => self::QUEUE_STATUS_ENABLED_LABEL,
                     self::QUEUE_STATUS_DISABLED => self::QUEUE_STATUS_DISABLED_LABEL);
    }

    public function __getStatusOptionArray(){

    }

    public function __getOrderGridOptionArray(){
        #return Mage::getModel('orderQueues/orderQueue')->getCollection()->addFilter('is_active',1)->addFilter('type',QUEUE_TYPE_PROCESSING)->toGridOptionArray('id','label');

        return Mage::getModel('orderQueues/orderQueue')->getCollection()->addFilter('is_active',1)->toGridOptionArray('id','label',array("na"=>"Not Assigned"));
    }
}