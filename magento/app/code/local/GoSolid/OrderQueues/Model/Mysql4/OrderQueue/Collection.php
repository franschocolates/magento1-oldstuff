<?php
class GoSolid_OrderQueues_Model_Mysql4_OrderQueue_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct() 
    {
        parent::_construct();
        $this->_init('orderQueues/orderQueue');
    }

	public function toOptionArray($value = "id", $label = "name", $selectText = null)
    {
		$array =  $this->_toOptionArray($value, $label);
		
		if($selectText != null)
		{
			$array = array('' =>  Mage::helper('orderQueues')->__("-- %s --", $selectText)) + $array;
		}
		
		return $array;
		
    }
    
    
	public function toGridOptionArray($value = "id", $label = "name", $preDefault = array())
    {
    	$list = array();

        if(count($preDefault)){
            foreach($preDefault as $preKey=>$preVal){
                $list[$preKey] = $preVal;
            }
        }
    	foreach($this as $item)
    	{
    		$list[$item->getData($value)] = $item->getData($label);	
    	
    	}       
		return $list;
    }
    
}