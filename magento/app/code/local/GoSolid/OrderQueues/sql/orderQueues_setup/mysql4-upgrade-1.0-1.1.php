<?php
$installer = $this;
$installer->startSetup();

$installer->run("ALTER TABLE sales_flat_order ADD COLUMN processing_queue int(10) NULL;");

$installer->run("ALTER TABLE sales_flat_order_grid ADD COLUMN processing_queue int(10) NULL;");

$installer->endSetup();
