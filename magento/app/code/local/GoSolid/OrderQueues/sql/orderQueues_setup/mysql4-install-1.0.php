<?php

$installer = $this;

$installer->startSetup();
/*
$table = $installer->getConnection()->newTable(
    $installer->getTable($installer->getTable())
        ->addColumn('id',Varien_Db_Ddl_Table::TYPE_INTEGER,null, array(
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
            'identity' => true
        ), 'ID')
        ->addColumn('label', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(),'Label')
        ->addColumn('type', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(),'Type')
        ->addColumn('is_active', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(),'Enabled')
);
*/
$installer->run("DROP TABLE IF EXISTS `frans_order_queues`;

CREATE TABLE `frans_order_queues` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `is_active` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

");

$installer->endSetup();