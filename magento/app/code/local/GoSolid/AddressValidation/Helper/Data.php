<?php
/**
 * Created by GoSolid.
 * Date: 11/11/15
 * Time: 8:41 AM
 */
class GoSolid_AddressValidation_Helper_Data extends Mage_Core_Helper_Abstract {

	protected $_session;

	private $_origStreetData;

	public function __construct()
	{
		$this->_session = Mage::getSingleton('core/session');
	}



	public function validateAddress($address, $oldHash=null)
	{
		$result = array(
			"score" => $this->getScore($address),
			"invalid_fields" => $this->getInvalidFields($address),
			"is_valid" => true,
			"suggest_address" => false
		);

		$address->resetAddressValidation();

		try{
			if($address->hasProposedAddress()){
				$proposed_address = json_decode($address->getProposedAddress());
				if(isset($proposed_address->ValidationResponse->AddressResults->ParsedAddressPartsDetail->ParsedStreetLine->POBox)){
					$result['is_valid'] = false;
					$address->setValidationMessage("We are unable to ship to PO boxes. Please enter a street address.");
					$result["invalid_fields"] = array("street_1");
				}
			}

		} catch (Exception $e){
			Mage::logException($e->message);
		}

		return $result;
	}

	// returns the FedEx validation score or null if there isn't one yet
	public function getScore($address)
	{
		$score = json_decode($this->getProposedAddress($address))->Score;
		return $score;
	}

	public function isPoBox($street)
	{
		if(preg_match("/[P|p]*(OST|ost)*\.*\s*[O|o|0]*(ffice|FFICE)*\.*\s*[B|b][O|o|0][X|x]/", $street)){
			return true;
		}

		return false;
	}

	// returns an array of IDs of form fields to be flagged as invalid
	public function getInvalidFields($address)
	{

		// todo: fix this
		return array();

		$fields = array();
		$wasModified = false;

		$this->_origStreetData = $address->getData()['street'];

		$explosion = explode(PHP_EOL, $address->getData()['street']);
		if(strstr($address->getData()['street'], PHP_EOL)){
			$address->setStreet($explosion[0]);
		}

		$proposedAddress = json_decode($this->getProposedAddress($address));

		$address->setStreet($this->_origStreetData);

		// note that proposed address object property keys are CaSe-SeNsItIvE!

		// first read and interpret the change codes in the data from FedEx
		if(isset($proposedAddress->Changes))
		{
			if(is_string($proposedAddress->Changes))
			{
				$proposedAddress->Changes = array($proposedAddress->Changes);
			}

			foreach($proposedAddress->Changes as $change)
			{
				switch($change)
				{
					case 'HOUSE_OR_BOX_NUMBER_NOT_FOUND':
						$fields[] = 'street_1';
						break;
					case 'APARTMENT_NUMBER_NOT_FOUND':
					case 'APARTMENT_NUMBER_REQUIRED':
					case 'BOX_NUMBER_REQUIRED':
//						$fields[] = 'street_2';
						break;
					case 'INSUFFICIENT_DATA':
						// completely unable to resolve address, invalid fields are unknown so flag them all
						$fields[] = 'street_1';
						$fields[] = 'street_2';
						$fields[] = 'city';
						$fields[] = 'region_id';
						$fields[] = 'shipping[region]';
						$fields[] = 'zip';
						$fields[] = 'country';
						break;
					case 'MODIFIED_TO_ACHIEVE_MATCH':
						// FedEx made some changes, but we'll need to dig deeper below to see what they were
						$wasModified = true;
						break;
				}
			}
		}
		// if the address was changed, examine the data from FedEx to see what fields were modified
		if($wasModified)
		{
			if(isset($proposedAddress->Address))
			{
				if(isset($proposedAddress->Address->StreetLines))
				{
					$parsedAddress = $this->getParsedAddress($proposedAddress, false);
					$addressLine1 = implode(' ', $parsedAddress['street_1']);
					$addressLine2 = implode(' ', $parsedAddress['street_2']);

					if(strtolower($this->escapeHtml($addressLine1)) != strtolower($this->escapeHtml($address->getStreet(1))))
					{
						$fields[] = 'street_1';
					}
					if(!$this->startsWith($addressLine1, $address->getStreet(1)))
					{
						$fields[] = 'street_1';
					}
					if(!empty($parsedAddress['street_2'])){
						if(strtolower($this->escapeHtml($addressLine2)) != strtolower($this->escapeHtml($address->getStreet(2))))
						{
							$fields[] = 'street_2';
						}
						if(!$this->startsWith($addressLine2, $address->getStreet(2)))
						{
							$fields[] = 'street_2';
						}
					}

				}
				if(isset($proposedAddress->Address->City))
				{

					if(strtolower($this->escapeHtml($proposedAddress->Address->City)) != strtolower($this->escapeHtml($address->getCity())))
					{
						$fields[] = 'city';
					}
					if (!$this->startsWith($proposedAddress->Address->City, $address->getCity())) {
						$fields[] = 'city';
					}

				}
				if(isset($proposedAddress->Address->CountryCode))
				{
					if(isset($proposedAddress->Address->StateOrProvinceCode))
					{
						$regionCode = $proposedAddress->Address->StateOrProvinceCode;
						$countryCode = $proposedAddress->Address->CountryCode;
						$regionId = Mage::getModel('directory/region')->loadByCode($regionCode, $countryCode)->getRegionId();
						if($address->getRegionId() != $regionId)
						{
							$fields[] = 'region_id';
							$fields[] = 'shipping[region]';
						}
					}
					if($proposedAddress->Address->CountryCode != $address->getCountryId())
					{
						$fields[] = 'country';
					}
				}
				if(isset($proposedAddress->Address->PostalCode))
				{
					if($proposedAddress->Address->PostalCode != $address->getPostcode())
					{
						$fields[] = 'zip';
					}
				}
			}
		}
		$fields = array_unique($fields);



		return $fields;
	}

	// increment a counter in session data whenever the same address is received consecutively, otherwise reset counter
	public function countError($address)
	{
		$hash = $this->hashAddress($address);
		$isPoBox = false;
		$parsedProposed = json_decode($address->getProposedAddress(), true);
		if(isset($parsedProposed['ParsedAddress']['ParsedStreetLine']['Elements'])){
			$elements = $parsedProposed['ParsedAddress']['ParsedStreetLine']['Elements'];
			if(isset($elements['Name'])){
				if($elements['Name'] == 'postOfficeBoxNumber'){
					$isPoBox = true;
				}
			}
		}
		$score = $this->getScore($address);
		if($isPoBox){
			$score = 0;
		}
		$errors = array(
			'hash' => $hash,
			'count' => ($score == 0) ? 1 : 2 // account for either non-found or non-suggested ("entered") addresses
		);
		if($this->_session->hasAddressValidationErrors())
		{
			$existingErrors = $this->_session->getAddressValidationErrors();
			if($existingErrors['hash'] == $hash)
			{
				// increment error count by one
				$errors['count'] = $existingErrors['count'] + 1;
			}
		}

		$this->_session->setAddressValidationErrors($errors);
		return $this;
	}

	public function getErrorCount()
	{
		if($this->_session->hasAddressValidationErrors())
		{
			$errors = $this->_session->getAddressValidationErrors();
			return $errors['count'];
		}
		return 0;
	}

	public function resetErrorCount()
	{
		$this->_session->unsAddressValidationErrors();
		return $this;
	}

	// get the address's proposedAddress JSON data and MD5 it for quick comparisons
	public function hashAddress($address)
	{
		return md5($this->getProposedAddress($address));
	}

	// sometimes if FedEx can't find an address it doesn't return anything (oops), so account for that
	public function getProposedAddress($address)
	{

		if(!$address->hasProposedAddress())
		{
			// make a bare-bones JSON object we can use in place of a real FedEx response
			$address->setProposedAddress(json_encode(array(
				'Score' => 0
			)));

			$address->save();
		}
		return $address->getProposedAddress();
	}

	public function startsWith($haystack, $needle) {
		// search backwards starting from haystack length characters from the end
		return $needle === "" || strripos($haystack, $needle, -strlen($haystack)) !== FALSE;
	}

	public function getParsedAddress($address, $is_encoded)
	{
		if($is_encoded) {
			$proposedAddress =  json_decode($address);
		} else {
			$proposedAddress = $address;
		}

		$finalAddress = array();
		$street1Fields = array('houseNumber', 'leadingDirectional', 'streetName', 'streetSuffix', 'trailingDirectional');

		foreach($proposedAddress->ParsedAddress->ParsedStreetLine->Elements as $element) {
			if(in_array($element->Name, $street1Fields)){
				$finalAddress['street_1'][] = $element->Value;
			} else {
				$finalAddress['street_2'][] = $element->Value;
			}
		}
		return $finalAddress;
	}

	public function getProposedAddressForMultiship($address){
		$parsedAddress = json_decode($address->getData()['proposed_address']);
		$address->getData()['postcode'] = $parsedAddress->Address->PostalCode;

	}


}
