<?php

/**
 * Grid default child product column renderer
 */
class GoSolid_CategoryProductDefaultChild_Block_Adminhtml_Widget_Grid_Column_Renderer_Defaultchild
	extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

	protected $_values;

	/**
	 * Renders grid column
	 *
	 * @param   Varien_Object $row
	 * @return  string
	 */
	public function render(Varien_Object $row)
	{

		$html = '';

		// if product is configurable, show the default child product dropdown
		if($row->getData('type_id') == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE)
		{
			$product = Mage::getModel('catalog/product')->load($row->getData('entity_id'));

			Mage::unregister('product');
			Mage::register('product', $product);

			$options = Mage::getSingleton('frans/catalog_product_defaultchild_select')->getAllOptions();

			$value = $row->getData($this->getColumn()->getIndex());

			$select = $this->getLayout()->createBlock('adminhtml/html_select')
				->setOptions($options)
				->setData(array(
					'name' => $this->getColumn()->getId(),
					'class' => 'select" style="' . $this->getColumn()->getInlineCss(),
					'value' => $value
				));

			$html = $select->getHtml();

		}
		else // non-configurable product, just show a hidden input
		{
			$html = '<input type="hidden" name="' . $this->getColumn()->getId() . '" />';
		}

		return $html;
	}
}
