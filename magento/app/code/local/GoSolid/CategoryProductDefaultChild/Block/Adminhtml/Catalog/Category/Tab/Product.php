<?php
/**
 * Created by GoSolid.
 * Date: 10/19/15
 * Time: 4:18 PM
 */ 
class GoSolid_CategoryProductDefaultChild_Block_Adminhtml_Catalog_Category_Tab_Product extends Mage_Adminhtml_Block_Catalog_Category_Tab_Product {

	protected function _prepareCollection()
	{
		if ($this->getCategory()->getId()) {
			$this->setDefaultFilter(array('in_category'=>1));
		}
		$collection = Mage::getModel('catalog/product')->getCollection()
			->addAttributeToSelect('name')
			->addAttributeToSelect('sku')
			->addAttributeToSelect('price')
			->addStoreFilter($this->getRequest()->getParam('store'))
			->joinField('position',
				'catalog/category_product',
				'position',
				'product_id=entity_id',
				'category_id='.(int) $this->getRequest()->getParam('id', 0),
				'left')
			->joinField('default_child',
				'catalog/category_product',
				'default_child',
				'product_id=entity_id',
				'at_default_child.category_id='.(int) $this->getRequest()->getParam('id', 0),
				'left');
		$this->setCollection($collection);

		if ($this->getCategory()->getProductsReadonly()) {
			$productIds = $this->_getSelectedProducts();
			if (empty($productIds)) {
				$productIds = 0;
			}
			$this->getCollection()->addFieldToFilter('entity_id', array('in'=>$productIds));
		}

		if ($this->getCollection()) {

			$this->_preparePage();

			$columnId = $this->getParam($this->getVarNameSort(), $this->_defaultSort);
			$dir      = $this->getParam($this->getVarNameDir(), $this->_defaultDir);
			$filter   = $this->getParam($this->getVarNameFilter(), null);

			if (is_null($filter)) {
				$filter = $this->_defaultFilter;
			}

			if (is_string($filter)) {
				$data = $this->helper('adminhtml')->prepareFilterString($filter);
				$this->_setFilterValues($data);
			}
			else if ($filter && is_array($filter)) {
				$this->_setFilterValues($filter);
			}
			else if(0 !== sizeof($this->_defaultFilter)) {
				$this->_setFilterValues($this->_defaultFilter);
			}

			if (isset($this->_columns[$columnId]) && $this->_columns[$columnId]->getIndex()) {
				$dir = (strtolower($dir)=='desc') ? 'desc' : 'asc';
				$this->_columns[$columnId]->setDir($dir);
				$this->_setCollectionOrder($this->_columns[$columnId]);
			}

			if (!$this->_isExport) {
				$this->getCollection()->load();
				$this->_afterLoadCollection();
			}
		}

		return $this;
	}

	protected function _prepareColumns()
	{

		$this->addColumnAfter('default_child', array(
			'header'        => Mage::helper('catalog')->__('Default Child Product'),
			'sortable'      => false,
			'position'      => 100,
			'width'         => '175px',
			'editable'      => !$this->getCategory()->getProductsReadonly(),
			'renderer'      => 'adminhtml/widget_grid_column_renderer_defaultchild',
			'index'         => 'default_child',
			'inline_css'    => 'width: 175px;'
		), 'name');

		return parent::_prepareColumns();
	}

}