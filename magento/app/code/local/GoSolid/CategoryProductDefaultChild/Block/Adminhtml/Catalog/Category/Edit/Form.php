<?php
/**
 * Created by GoSolid.
 * Date: 10/20/15
 * Time: 10:59 AM
 */ 
class GoSolid_CategoryProductDefaultChild_Block_Adminhtml_Catalog_Category_Edit_Form extends Mage_Adminhtml_Block_Catalog_Category_Edit_Form {

	public function getProductsDefaultChildJson()
	{
		$productsDefaultChild = $this->getCategory()->getProductsDefaultChild();
		if (!empty($productsDefaultChild)) {
			return Mage::helper('core')->jsonEncode($productsDefaultChild);
		}
		return '{}';
	}

	// simple renaming of existing function for less ambiguity about what it's doing
	public function getProductsPositionJson()
	{
		return $this->getProductsJson();
	}

}