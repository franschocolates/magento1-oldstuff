<?php

class GoSolid_CategoryProductDefaultChild_Model_Resource_Catalog_Category extends GoSolid_Frans_Model_Resource_Catalog_Category {

	/**
	 * Get positions of associated to category products
	 *
	 * @param Mage_Catalog_Model_Category $category
	 * @return array
	 */
	public function getProductsDefaultChild($category)
	{
		$select = $this->_getWriteAdapter()->select()
			->from($this->_categoryProductTable, array('product_id', 'default_child'))
			->where('category_id = :category_id');
		$bind = array('category_id' => (int)$category->getId());

		return $this->_getWriteAdapter()->fetchPairs($select, $bind);
	}

	/**
	 * Save category products relation
	 *
	 * @param Mage_Catalog_Model_Category $category
	 * @return Mage_Catalog_Model_Resource_Category
	 */
	protected function _saveCategoryProducts($category)
	{
		$category->setIsChangedProductList(false);
		$id = $category->getId();
		/**
		 * new category-product relationships
		 */
		$products = $category->getPostedProducts();
		$productsDefaultChild = $category->getPostedProductsDefaultChild();

		/**
		 * Example re-save category
		 */
		if ($products === null) {
			return $this;
		}

		/**
		 * old category-product relationships
		 */
		$oldProducts = $category->getProductsPosition();
		$oldProductsDefaultChild = $category->getProductsDefaultChild();

		// no need to consider default child products when determining products to insert or delete
		$insert = array_diff_key($products, $oldProducts);
		$delete = array_diff_key($oldProducts, $products);

		/**
		 * Find product ids which are presented in both arrays
		 * and saved before (check $oldProducts array)
		 */
		$updateProductsPosition = array_intersect_key($products, $oldProducts);
		$updateProductsPosition = array_diff_assoc($updateProductsPosition, $oldProducts);

		// do the same for default child products
		$updateProductsDefaultChild = array_intersect_key($productsDefaultChild, $oldProductsDefaultChild);
		$updateProductsDefaultChild = array_diff_assoc($updateProductsDefaultChild, $oldProductsDefaultChild);

		// merge (using the + operator) the results of both to-update checks to get all product IDs to update
		// NOTE: values in this array could arbitrarily be either positions or default child IDs, so don't use them for anything
		$update = $updateProductsPosition + $updateProductsDefaultChild;

		$adapter = $this->_getWriteAdapter();

		/**
		 * Delete products from category
		 */
		if (!empty($delete)) {
			$cond = array(
				'product_id IN(?)' => array_keys($delete),
				'category_id=?' => $id
			);
			$adapter->delete($this->_categoryProductTable, $cond);
		}

		/**
		 * Add products to category
		 */
		if (!empty($insert)) {
			$data = array();
			foreach ($insert as $productId => $position) {
				$defaultChild = array_key_exists($productId, $productsDefaultChild) ? $productsDefaultChild[$productId] : null;
				$data[] = array(
					'category_id'   => (int)$id,
					'product_id'    => (int)$productId,
					'default_child' => $defaultChild == 0 ? null : $defaultChild,
					'position'      => (int)$position
				);
			}
			$adapter->insertMultiple($this->_categoryProductTable, $data);
		}

		/**
		 * Update product positions and default child IDs in category
		 */
		if (!empty($update)) {
			foreach ($update as $productId => $value) {
				// remember, $value is not useful for anything here as it could be either a position or a default child ID
				$where = array(
					'category_id = ?'=> (int)$id,
					'product_id = ?' => (int)$productId
				);
				$defaultChild = array_key_exists($productId, $productsDefaultChild) ? $productsDefaultChild[$productId] : null;
				$bind  = array(
					'default_child' => $defaultChild == 0 ? null : $defaultChild,
					'position' => (int)$products[$productId]
				);
				$adapter->update($this->_categoryProductTable, $bind, $where);
			}
		}

		if (!empty($insert) || !empty($delete)) {
			$productIds = array_unique(array_merge(array_keys($insert), array_keys($delete)));
			Mage::dispatchEvent('catalog_category_change_products', array(
				'category'      => $category,
				'product_ids'   => $productIds
			));
		}

		if (!empty($insert) || !empty($update) || !empty($delete)) {
			$category->setIsChangedProductList(true);

			/**
			 * Setting affected products to category for third party engine index refresh
			 */
			$productIds = array_keys($insert + $delete + $update);
			$category->setAffectedProductIds($productIds);
		}
		return $this;
	}

}