<?php
class GoSolid_CategoryProductDefaultChild_Model_Catalog_Product extends GoSolid_Frans_Model_Catalog_Product {

	public function getDefaultChildProduct()
	{
		if($this->_defaultChildProduct)
		{
			return $this->_defaultChildProduct;
		}

		// attempt to get the default child of this product from the category
		$category = $this->getCategory();
		if($category)
		{
			$defaultChildProductIds = Mage::getResourceModel('catalog/category')->getProductsDefaultChild($category);
			$defaultChildProductId = $defaultChildProductIds[$this->getId()];
			$this->setDefaultChildProduct($defaultChildProductId);
		}

		return parent::getDefaultChildProduct();
	}

}