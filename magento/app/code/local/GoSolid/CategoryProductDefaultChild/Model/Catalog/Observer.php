<?php
/**
 * Created by GoSolid.
 * Date: 10/20/15
 * Time: 12:44 PM
 */ 
class GoSolid_CategoryProductDefaultChild_Model_Catalog_Observer extends GoSolid_Frans_Model_Catalog_Observer {

	public function saveCategoryProductDefaultChildProducts(Varien_Event_Observer $observer)
	{
		$category = $observer->getCategory();
		$data = $observer->getRequest()->getParams();

		if (isset($data['category_products_defaultchild']) &&
			!$category->getProductsReadonly()) {
			$productsDefaultChild = array();
			parse_str($data['category_products_defaultchild'], $productsDefaultChild);
			$category->setPostedProductsDefaultChild($productsDefaultChild);
		}

	}

}