<?php
class GoSolid_CategoryProductDefaultChild_Model_Catalog_Category extends GoSolid_Frans_Model_Catalog_Category {

	/**
	 * Retrieve array of product id's for category
	 *
	 * array($productId => $defaultChildProductId)
	 *
	 * @return array
	 */
	public function getProductsDefaultChild()
	{
		if(!$this->getId())
		{
			return array();
		}
		$array = $this->getData('products_default_child');
		if(is_null($array))
		{
			$array = $this->getResource()->getProductsDefaultChild($this);
			$this->setData('products_default_child', $array);
		}
		return $array;
	}

}