<?php

$installer = $this;
$installer->startSetup();

// add Default Child Product column to catalog_category_product table and associated index tables
$installer->getConnection()
	->addColumn($installer->getTable('catalog/category_product'),'default_child', array(
		'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
		'nullable'  => true,
		'length'    => 10,
		'comment'   => 'Default child product ID for this category product'
	));

//$installer->getConnection()
//	->addColumn($installer->getTable('catalog/category_product_index'),'default_child', array(
//		'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
//		'nullable'  => true,
//		'length'    => 10,
//		'comment'   => 'Default child product ID for this category product'
//	));

//$installer->getConnection()
//	->addColumn($installer->getTable('catalog/category_product_index_idx'),'default_child', array(
//		'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
//		'nullable'  => true,
//		'length'    => 10,
//		'comment'   => 'Default child product ID for this category product'
//	));
//
//$installer->getConnection()
//	->addColumn($installer->getTable('catalog/category_product_index_tmp'),'default_child', array(
//		'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
//		'nullable'  => true,
//		'length'    => 10,
//		'comment'   => 'Default child product ID for this category product'
//	));

$installer->endSetup();