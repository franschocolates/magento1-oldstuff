<?php
class GoSolid_SqlReports_Model_SqlReport extends Mage_Core_Model_Abstract
{
    const COL_ORDER_VIEW = 'order_view';

    private $_params = null;
    private $_subQueryKey = null;
    private $_finalSubQuery = array();
    private $_totalColumns = array();
    private $_hasOrderView = false;

    public function _construct()
    {

        parent::_construct();
        $this->_init('sqlReports/sqlReport');
    }

    public function load($id, $field = null)
    {
        parent::load($id, $field);
        $this->_prepare(); //get the report ready.
        return $this;
    }

    public function getSampleData($tableName)
    {
        $sql = "select * from `" . $tableName . "` limit 10";

        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');

        try {
            $result = $conn->query($sql);

            $data = array();
            $i = 0;
            while ($row=mysql_fetch_assoc($result))
            {
                $data[$i] = $row;
                $i++;
            }


            //build the columns
            $columnCount = $result->columnCount();
            $columns = array();
            for($i = 0; $i < $columnCount; $i++)
            {

                $column = $result->getColumnMeta($i);


                $columns[] = array(
                    "type" => $column["native_type"],
                    "name" => $column["name"],
                );
            }




            $results = array(
                "columns" => $columns,
                "data"	=> $result->fetchAll()
            );

        }
        catch (Exception $ex)
        {
            $results = array("error" => $ex->getMessage() );
        }



        return $results;
    }

    public function getListOfTables()
    {
        $sql = "SHOW FULL  TABLES FROM `" . Mage::getConfig()->getNode('global/resources/default_setup/connection/dbname')."`";

        $results = $this->getSqlResults($sql)->fetchAll();

        $tables = array();

        foreach($results as $result)
        {
            $tables[] = $result["Tables_in_" . Mage::getConfig()->getNode('global/resources/default_setup/connection/dbname')];
        }



        return $tables;
    }

    public function getQueryParameters()
    {
        return $this->_params;

    }

    private function _prepare()
    {
        $this->_buildReportParams();
        $this->_setSubQueryKey();
        $this->_setTotalColumns();
    }

    private function _getVarMatches($sql)
    {
        $matches = array();
        //get all the params from the query.
        preg_match_all('/\{(.*?)\}/', $sql, $matches);

        return $matches;
    }

    /*
     * This function set the keys from the sub query to be replaced by each row of the parent results.
     */
    private function _setSubQueryKey()
    {
        //Get the stored sub query
        $subQuery = $this->getSubQuery();

        $matches = $this->_getVarMatches($subQuery);

        foreach($matches[1] as $matchKey => $match)
        {
            $this->_subQueryKey[$match] = $matches[0][$matchKey];
        }

    }

    private function _buildReportParams()
    {
        $params = array();

        //get the query
        $sql = $this->getSql();

        //get all the params from the query.
        $matches = $this->_getVarMatches( $sql );

        //no matches, get out of here.
        if($matches == null || count($matches) < 2 || count($matches[1]) == 0)
        {
            // try looking for some in the pre-sql too
            $preSql = $this->getPreSql();

            if (strlen($preSql))
            {
                $matches = $this->_getVarMatches( $preSql );
            }

            if ($matches == null || count($matches) == 0)
            {
                return;
            }

        }

        //convert the matches to parameters that are unique
        $matched = array();
        foreach($matches[1] as $match)
        {
            $parsed = explode(":", $match);
            $type = $parsed[1];

            $anyMatches = preg_match_all('/(multiselect|select)*\[(.*?)\]/', $type, $typeMatches);

            if($anyMatches > 0)
            {
                $type = $typeMatches[1][0];
                $optionsType = $typeMatches[2][0];

                $options = $this->getOptions($optionsType);

            }

            if(in_array($match, $matched) == false)
            {
                $params[] = array(
                    "label" => $parsed[0],
                    "type" => $type,
                    "options" => $options,
                    "match" => $match
                );

                $matched[] = $match;
            }

        }

        $this->_params = $params;
    }

    private function _buildColumns($result, $isExport)
    {
        //build the columns
        $columnCount = $result->columnCount();
        $columns = array();

        for($i = 0; $i < $columnCount; $i++)
        {
            /*
            echo "<pre>";
            var_dump($result->getColumnMeta($i));
            echo "</pre>";
            */
            $column = $result->getColumnMeta($i);

            if (strcasecmp($column["name"], self::COL_ORDER_VIEW) == 0)
            {
                $this->_hasOrderView = true;
                // if we are exporting, we don't want the order view stuff
                // so just don't add it to the columns
                if (!$isExport)
                {
                    $columns[] = array(
                        "type" => 'string', // will be a URL, make it a string.
                        "name" => $column["name"],
                    );
                }
            }
            else
            {
                $columns[] = array(
                    "type" => $column["native_type"],
                    "name" => $column["name"],
                );
            }
        }

        return $columns;

    }

    public function getFinalSql($params, $sql = null)
    {


        //replace the query params with the actual values.
        if ($sql == null)
        {
            $sql = $this->getSql();
        }

        foreach($params as $key => $value)
        {
            //get the original query param to be replaced.
            $replace = "";
            foreach($this->_params as $p)
            {
                if($p["label"] == $key)
                {

                    //if type date convert to store date time
                    //adjust for sever time
                    if($p['type'] == 'date')
                    {
                        $value = $this->formatAmericanDateForSQL($value);
                    }

                    if(is_array($value))
                    {
                        $replace = "{" . $p["match"]  . "}";
                        //prepare the value
                        $prepValue = "'" . implode("', '", $value) . "'";
                    }
                    else
                    {
                        $replace = "{" . $p["match"]  . "}";
                        //prepare the value
                        $prepValue = $this->mysql_escape_mimic($value);
                    }

                    break;
                }
            }

            $sql = str_replace($replace, $prepValue, $sql);
        }

        return $sql;

    }

    public function getHasSubQuery()
    {
        $hasSubQuery = false;
        if($this->_subQueryKey != null)
        {
            $hasSubQuery = true;
        }
        return $hasSubQuery;
    }

    public function getSubQueryKey()
    {
        return $this->_subQueryKey;
    }

    public function getSubQueryResults($row)
    {
        $subQuery = $this->getSubQuery();


        foreach( $this->_subQueryKey as $key => $column )
        {
            $subQuery = str_ireplace($column, $row[$key], $subQuery);
        }
         $this->_finalSubQuery[] = $subQuery;

        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        $result = $conn->query( $subQuery );

        return $result;
    }

    public function getFinalSubQuery()
    {
        return $this->_finalSubQuery;
    }

    private function mysql_escape_mimic($inp) {
        if(is_array($inp))
            return array_map(__METHOD__, $inp);

        if(!empty($inp) && is_string($inp)) {
            return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp);
        }

        return $inp;
    }

    public function runReport($params, $isExport = false)
    {

        $sql = $this->getFinalSql($params);

        $conn = Mage::getSingleton('core/resource')->getConnection('core_write');

        try {
            set_time_limit(30);

            //run any pre ql if there is any
            if(strlen(trim($this->getPreSql())) > 0)
            {
                // also try with params
                $preSql = $this->getFinalSql($params, trim($this->getPreSql()));

                $conn->query( $preSql );
            }

            //run the report.
            $result = $conn->query($sql);

            $columns = $this->_buildColumns($result,$isExport);
            $hasTotals = $this->getHasTotals();
            $hasOrderView = $this->_hasOrderView;

            $data = $result->fetchAll();

            if ($hasOrderView)
            {
                // if we have an order view column, we either need to
                // remove it (if exporting), or convert to a URL (if displaying)
                foreach ($data as $index => $row)
                {
                    if ($isExport)
                    {
                        // just remove it
                        unset($data[$index][self::COL_ORDER_VIEW]);
                    }
                    else
                    {
                        $orderUrl = Mage::getUrl('adminhtml/sales_order/view', array( 'order_id' => $row[self::COL_ORDER_VIEW]));
                        $data[$index][self::COL_ORDER_VIEW] = "<a href='$orderUrl' target='_blank'>View</a>";
                    }
                }
            }

            if ($hasTotals)
            {
                $data[] = $this->_buildTotalsRow($columns, $data);
            }

            $results = array(
                "columns" => $columns,
                "data"	=> $data
            );

            if($this->_subQueryKey)
            {
                //we could add the extra content here...
                foreach ($data as $index => $row) {

                    //We may need to skip the totals column
                    if($hasTotals && $index == (count($data) - 1))
                    {
                        break;
                    }

                        $subQueryResult = $this->getSubQueryResults($row);
                        $subQueryColumns = $this->_buildColumns($subQueryResult, $isExport);
                        $subQueryData = $subQueryResult->fetchAll();

                        $results["sub_data"][] = array(
                            "columns" => $subQueryColumns,
                            "data" => $subQueryData
                        );

                }

            }
        }
        catch (Exception $ex)
        {
            $results = array("error" => $ex->getMessage() );
        }

        return $results;

    }

    /*
     * Used to convert date format from date input box (mm/dd/yy) to date format used by Magento (yyyy-mm-dd)
     */
    public function formatAmericanDateForSQL($date)
    {
        $dateArr = explode('/', $date);

        $date = '20' . $dateArr[2] . '-' . $dateArr[0] . '-' . $dateArr[1];

        return $date;
    }


    private function getSqlResults($sql)
    {
        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        $result = $conn->query( $sql );

        return $result;
    }

    private function getSelectOptionsFromSql($sql)
    {
        $options = array();

        $conn = Mage::getSingleton('core/resource')->getConnection('core_read');
        $result = $conn->query( $sql );

        $i = 0;
        foreach ( $result->fetchAll() as $row  )
        {

            $options[$i] = array(
                "label" => $row["label"],
                "value" => $row["value"]
            );
            $i++;
        }

        return $options;

    }

    private function getOptions($type)
    {
        $options = null;

        switch($type)
        {
            case 'usregions':
                $options = $this->getSelectOptionsFromSql("SELECT default_name AS `label`, default_name AS `value` FROM `directory_country_region` WHERE country_id = 'us' ORDER BY default_name");
                break;
            case 'customergroups':
                $options = $this->getSelectOptionsFromSql("SELECT customer_group_code AS `label`, customer_group_code AS `value` FROM `customer_group` ORDER BY customer_group_code");
                break;
            default:
                $options = $this->getSelectOptionsFromSql($type);
                break;
        }

        return $options;
    }

    public function getHasTotals()
    {
        $hasTotalColumns = false;

        if(count($this->_totalColumns))
        {
            $hasTotalColumns = true;
        }

        return $hasTotalColumns;
    }

    public  function arrayToCsv( array &$fields, $delimiter = ';', $enclosure = '"', $encloseAll = false, $nullToMysqlNull = false ) {
        $delimiter_esc = preg_quote($delimiter, '/');
        $enclosure_esc = preg_quote($enclosure, '/');

        $output = array();
        foreach ( $fields as $field ) {
            if ($field === null && $nullToMysqlNull) {
                $output[] = 'NULL';
                continue;
            }

            // Enclose fields containing $delimiter, $enclosure or whitespace
            if ( $encloseAll || preg_match( "/(?:${delimiter_esc}|${enclosure_esc}|\s)/", $field ) ) {
                $output[] = $enclosure . str_replace($enclosure, $enclosure . $enclosure, $field) . $enclosure;
            }
            else {
                $output[] = $field;
            }
        }

        return implode( $delimiter, $output );
    }

    /*
     * Transforms the CRLF delimited list they input with an array of colum names
     *
    */
    private function _setTotalColumns()
    {
        if ($this->getColumnNamesToTotal())
        {
            foreach (explode(PHP_EOL, $this->getColumnNamesToTotal()) as $index => $colName)
            {
                if (strlen(trim($colName)) > 0)
                    $totalColumns[trim($colName)] = 0;
            }

        $this->_totalColumns =  $totalColumns;
        }
    }

    private function _buildTotalsRow($allColumns, $data)
    {
        $totalColumns = $this->_totalColumns;
        // loop through each one and sum it.
        foreach ($data as $row)
        {
            foreach ($totalColumns as $colName => $total)
            {
                if ($row[$colName])
                {
                    $totalColumns[$colName] = $total + $row[$colName];
                }
            }
        }

        // now build a row with the correct number of columns
        $sumRow = array();
        $first = true;
        foreach ($allColumns as $col)
        {
            $colName = $col["name"];
            // make the first entry say "totals"
            if ($first)
            {
                $sumRow[$colName] = "Totals";
                $first = false;
            }
            else
            {
                if ($this->isNumber($col['type']) == true)
                {
                    $sumRow [$colName]= (array_key_exists($colName, $totalColumns)) ? $totalColumns[$colName] : '';
                }
                else
                {
                    $sumRow [$colName]= (array_key_exists($colName, $totalColumns)) ? 'N/A' : '';
                }
            }
        }

        return $sumRow;
    }

    public function isNumber($type)
    {
        switch($type)
        {
            case "SHORT":
                return true;
            case "LONG":
                return true;
            case "LONGLONG":
                return true;
            case "NEWDECIMAL":
                return true;
        }
    }


}