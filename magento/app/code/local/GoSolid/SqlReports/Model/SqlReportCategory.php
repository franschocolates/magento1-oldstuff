<?php
class GoSolid_SqlReports_Model_SqlReportCategory extends Mage_Core_Model_Abstract 
{
    public function _construct()
    {
        parent::_construct(); 
        $this->_init('sqlReports/sqlReportCategory');
    }
	
    public function getCategoryOptions()
    {
    	$options = array();
    	$collection = Mage::getModel("sqlReports/sqlReportCategory")->getCollection();
    	foreach($collection as $c)
    	{
    		$options[$c->getId()] =  $c->getTitle();
    			
    		
    	}

    	
    	return $options;
    }
    
}