<?php
 
class GoSolid_SqlReports_Block_Adminhtml_Sqlcategories extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {

	$this->_controller = 'adminhtml_sqlcategories';
	$this->_blockGroup = 'sqlReports';
	$this->_headerText = Mage::helper('sqlReports')->__("Sql Categories");
	$this->_addButtonLabel = Mage::helper('sqlReports')->__("Add");
	
    parent::__construct();
    
  }
}