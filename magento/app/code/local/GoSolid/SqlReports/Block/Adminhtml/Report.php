<?php
class GoSolid_SqlReports_Block_Adminhtml_Report extends Mage_Adminhtml_Block_Abstract
{
	public function __construct()
  	{
  		$this->setTemplate("sqlreports/report.phtml");
		parent::_construct();
  	}
  	
  	
  	public function getReport()
  	{
  		$report = Mage::getModel("sqlReports/sqlReport")->load( Mage::registry('report_id') );
  		return $report;
  	}
  	
}