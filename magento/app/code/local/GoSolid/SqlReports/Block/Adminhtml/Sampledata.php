<?php
class GoSolid_SqlReports_Block_Adminhtml_Sampledata extends Mage_Adminhtml_Block_Abstract
{
	public function __construct()
  	{
  		$this->setTemplate("sqlreports/sampledata.phtml");
		parent::_construct();
  	}
  	
  	public function getTableName()
  	{
  		$tableName = Mage::registry('sampledata_tablename');
  		return $tableName;
  	}

  	public function getResults()
  	{
		
  		$tableName =$this->getTableName();
		return Mage::getModel("sqlReports/sqlReport")->getSampleData($tableName);
  	}
  	
}