<?php

class GoSolid_SqlReports_Block_Adminhtml_Sqlcategories_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{  
	public function __construct()
  	{
		$this->setId('sqlReports_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('sqlReports')->__('Category'));
		parent::__construct();
  	}
	

	protected function _beforeToHtml()
  	{

	 $this->addTab('form_section', array(
          	'label'     => Mage::helper('sqlReports')->__('General'),
         	'title'     => Mage::helper('sqlReports')->__('General'),
          	'content'   =>  $this->getLayout()->createBlock('sqlReports/adminhtml_sqlcategories_edit_tab_form')->toHtml(),
			'active'    => true
      ));


      return parent::_beforeToHtml();
  }
  
}