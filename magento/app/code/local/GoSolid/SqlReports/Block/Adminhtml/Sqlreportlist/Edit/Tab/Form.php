<?php

class GoSolid_SqlReports_Block_Adminhtml_Sqlreportlist_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

	protected function _prepareForm()
	{

		$form = new Varien_Data_Form(); 
		$this->setForm($form);
		$fieldset = $form->addFieldset('sqlReports_fs', array('legend'=>Mage::helper('sqlReports')->__('Report')));
	
	    
		$fieldset->addField('title', 'text', array(
	          'title'     => Mage::helper('sqlReports')->__('Title'),
			  'label'     => Mage::helper('sqlReports')->__('Title'),
	          'name'      => 'title',
			  'class'     => 'required-entry', 
	          'required'  => true,
			  //'note'	  => 'My Note Here',
	      	));
	      	
	    $categories = Mage::getModel("sqlReports/sqlReportCategory")->getCategoryOptions();
	    $fieldset->addField('category_id', 'select', array(
	          'title'     => Mage::helper('sqlReports')->__('Category'),
			  'label'     => Mage::helper('sqlReports')->__('Category'),
	          'name'      => 'category_id',
			  'class'     => 'required-entry', 
	          'required'  => true,
	    	  'options'   => $categories
			  //'note'	  => 'My Note Here',
	      	));
	      	
		$fieldset->addField('position', 'text', array(
	          'title'     => Mage::helper('sqlReports')->__('Position'),
			  'label'     => Mage::helper('sqlReports')->__('Position'),
	          'name'      => 'position',
			  'class'     => 'required-entry', 
	          'required'  => true,
			  //'note'	  => 'My Note Here',
	      	));
		
		/*
	    * EXAMPLE CODE. CAN BE REMOVED.
	    *
		
		//text
		$fieldset->addField('mytext', 'text', array(
	          'title'     => Mage::helper('sqlReports')->__('MyText'),
			  'label'     => Mage::helper('sqlReports')->__('MyText'),
	          'name'      => 'name',
			  //'class'     => 'required-entry', 
	          //'required'  => true,
			  //'note'	  => 'My Note Here',
	      	));
		
	    //yes no
	    $fieldset->addField('myyesno', 'select', array(
		          'title'    	 => Mage::helper('sqlReports')->__('Add To Cart'),
				  'label'    	 => Mage::helper('sqlReports')->__('Add To Cart'),
		          //'class'     => 'required-entry', 
				  //'required'  	=> true,
		          'name'     	 => 'myyesno',
		    	  'checked'=> false,
		    	  'name'  => 'myyesno',
		    	  'options'   => array(
									'1' => 'Yes',
									'0' => 'No',
						  ),
				  'note'	=> Mage::helper('sqlReports')->__("My Note."),

      		));   	
	   
		//money
	   	$fieldset->addField('myprice', 'text', array(
          'title'     => Mage::helper('sqlReports')->__('MyPrice'),
		  'label'     => Mage::helper('sqlReports')->__('MyPrice'),
       	  'name'      => 'myprice',   
	   	  //'class'     => 'required-entry validate-zero-or-greater',
          //'required'  => true,
		  //'note' => 	 Mage::helper('sqlReports')->__("My Note Here.")
      		));
	  
      	//options
      	$options = Mage::getSingleton('event/facility')->getCollection()->setOrder("name", "asc")->toOptionArray("id", "name", "Select");
      	$fieldset->addField('myoptions', 'select', array(
          'label'     => Mage::helper('sqlReports')->__('MyOptions'),
		  'title'     => Mage::helper('sqlReports')->__('MyOptions'),
          //'class'     => 'required-entry',
          //'required'  => true,
          'name'      => 'myoptions',
		  'values'   => $options
      	));
		
		//status
		$fieldset->addField('status', 'select',array(
			'label'     => Mage::helper('sqlReports')->__('Status'),
			'required'  => true,
			'name'      => 'status',
			'options'   => array(
					  		'Enabled' => 'Enabled',
					  		'Disabled' => 'Disabled',
				  ),
		));
		
		// END EXAMPLE CODE
		*/
		
	
	
		if ( Mage::getSingleton('adminhtml/session')->getSqlreportlistData() )
		{
	    	$form->setValues(Mage::getSingleton('adminhtml/session')->getSqlreportlistData());
	    	Mage::getSingleton('adminhtml/session')->getSqlreportlistData(null);
		}
		elseif(Mage::registry('sqlreportlist_data'))
		{
	    	$form->setValues(Mage::registry('sqlreportlist_data')->getData());
		}
		
		return parent::_prepareForm();
	}
  
}