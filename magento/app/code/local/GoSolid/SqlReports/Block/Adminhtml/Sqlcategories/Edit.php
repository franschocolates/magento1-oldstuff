<?php

class GoSolid_SqlReports_Block_Adminhtml_Sqlcategories_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'sqlReports';
        $this->_controller = 'adminhtml_sqlcategories';
        
        // Optional
        //$this->_updateButton('save', 'label', Mage::helper('event')->__('Save'));
		//$this->_removeButton('delete');
		//$this->_removeButton('reset');
		
		$this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('sqlReports')->__('Save and Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);
		
        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
			
        ";
    }

	public function getHeaderText()
    {  
        if( Mage::registry('sqlcategories_data') && Mage::registry('sqlcategories_data')->getId() ) {
            return Mage::helper('sqlReports')->__("Edit");
		} else {
            return Mage::helper('sqlReports')->__('Add');
        }
    }

}