<?php
 
class GoSolid_SqlReports_Block_Adminhtml_Sqlreportlist extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {

	$this->_controller = 'adminhtml_sqlreportlist';
	$this->_blockGroup = 'sqlReports';
	$this->_headerText = Mage::helper('sqlReports')->__("Sql Reports");
	$this->_addButtonLabel = Mage::helper('sqlReports')->__("Add");
	
    parent::__construct();
    
  }
}