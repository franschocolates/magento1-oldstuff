<?php
class GoSolid_SqlReports_Block_Adminhtml_List extends Mage_Adminhtml_Block_Abstract
{
	public function __construct()
  	{
  		
  		
  		$this->setTemplate("sqlreports/list.phtml");
		parent::_construct();
  	}
  	
  	
  	public function getCategories()
  	{
  		$collection = Mage::getModel("sqlReports/sqlReportCategory")->getCollection();
  		$collection->getSelect()->order("position asc");
  		$categories = $collection;
  		return $categories;
  	}
  	
  	public function getReports($category)
  	{
  		$reports = Mage::getModel("sqlReports/sqlReport")->getCollection()
  			->addFieldToFilter("category_id", $category->getId() );
  			
  		return $reports;
  	}
  	
}