<?php
class GoSolid_SqlReports_Block_Adminhtml_Grid extends Mage_Adminhtml_Block_Abstract
{
	protected $_report = null;
	public function __construct()
  	{
  		$this->setTemplate("sqlreports/grid.phtml");
		parent::_construct();
		$this->_setReport();
  	}


	protected function _setReport()
  	{
  		$this->_report = Mage::getModel("sqlReports/sqlReport")->load(Mage::registry('report_id'));
  	}
  	
  	public function getSql()
  	{
  		return $this->_report->getFinalSql(Mage::registry('report_params'));
  	}
  	
  	public function getFinalPreSql()
  	{
  		$report = $this->_report;
  		if ($preSql = $report->getPreSql())
  		{
  			return $this->_report->getFinalSql(Mage::registry('report_params'), $preSql);
  		}
  		
  		return '';
  	}

	public function getHasSubQuery()
	{
		return $this->_report->getHasSubQuery();
	}

	public function getSubQueryKey()
	{
		return array_keys($this->_report->getSubQueryKey());
	}

  	public function getResults()
  	{
  		return $this->_report->runReport(Mage::registry('report_params'));
  	}
  	
  	public function isNumber($type)
  	{
	  	return Mage::getModel('sqlReports/sqlReport')->isNumber($type);
  	}

	public function getHasTotals()
	{
		return $this->_report->getHasTotals();
	}

	public function getFinalSubQuery()
	{
		$finalSubQuery = '';

		foreach( $this->_report->getFinalSubQuery() as $query)
		{
			$finalSubQuery .= $query.'; <br />';
		}
		return $finalSubQuery;
	}
}