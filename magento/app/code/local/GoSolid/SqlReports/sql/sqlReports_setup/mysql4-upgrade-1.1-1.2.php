<?php

$installer = $this;
$installer->startSetup();
$installer->run("
    ALTER TABLE `{$installer->getTable('sqlReports/sqlReport')}`
    ADD COLUMN sub_query TEXT NULL;
");

$installer->endSetup();