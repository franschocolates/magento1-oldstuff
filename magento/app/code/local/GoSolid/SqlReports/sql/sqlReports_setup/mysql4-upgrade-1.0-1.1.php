<?php

$installer = $this;
$installer->startSetup();
$installer->run("
    ALTER TABLE `{$installer->getTable('sqlReports/sqlReport')}`
    ADD COLUMN column_names_to_total TEXT NULL,
    ADD COLUMN pre_sql TEXT NULL;
    
");

$installer->endSetup();