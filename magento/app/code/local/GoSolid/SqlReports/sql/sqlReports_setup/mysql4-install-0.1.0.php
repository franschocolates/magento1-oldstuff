<?php 

$installer = $this;

$installer->startSetup();

$installer->run("DROP TABLE IF EXISTS `gosolid_sqlreport_category`;

CREATE TABLE `gosolid_sqlreport_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `position` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

");


$installer->run("DROP TABLE IF EXISTS `gosolid_sqlreport_report`;

CREATE TABLE `gosolid_sqlreport_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `sql` text,
  `category_id` int(11) DEFAULT NULL,
  `position` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

");

$installer->run("insert  into `gosolid_sqlreport_category`(`id`,`title`,`position`) values (1,'Sales',0),(2,'Inventory',1);
");