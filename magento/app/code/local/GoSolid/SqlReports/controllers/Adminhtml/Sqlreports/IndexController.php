<?php
class GoSolid_SqlReports_Adminhtml_Sqlreports_IndexController extends Mage_Adminhtml_Controller_action
{
	
	protected function hasPermission()
	{
		if (!Mage::getSingleton('admin/session')->isAllowed('admin/report/sqlreports/sqlreports2'))
		{
			$this->getResponse()->setRedirect($this->getUrl('*/*/denied'));
		}
	}
	
	
	
	protected function _initAction() 
	{			
		
		$layout = $this->loadLayout();
				
		$layout->loadLayout()
			->_setActiveMenu('report/sqlreports');
	
		return $this;
	}   
 
	public function indexAction() {
	
		$this->hasPermission();
		
		$this->_initAction();
		$this->renderLayout();
		
	}
	
	
	public function viewreportAction() {
		
		$this->hasPermission();
		
		Mage::register('report_id', $this->getRequest()->getParam("id"));
		
		
		$this->_initAction();
		$this->renderLayout();
	}
	
	
	public function savereportAction()
	{
		$this->hasPermission();
		
		$params = $this->getRequest()->getParams();
		
		//load up the report.
		$report = Mage::getModel("sqlReports/sqlReport")->load($this->getRequest()->getParam("id"));
		$report->setData($params);
		$report->save();
		
		
		$redirectUrl = $this->getUrl('*/sqlreports_index/viewreport' , array("id" => $this->getRequest()->getParam("id"))) ;
		

		Mage::app()->getFrontController()->getResponse()->setRedirect($redirectUrl);
		
		
	}
	
	public function getsampledataAction()
	{
		$this->hasPermission();
		
		Mage::register('sampledata_tablename', $this->getRequest()->getParam("id"));

		$grid = $this->getLayout()->createBlock("sqlReports/adminhtml_sampledata");
		
		echo $grid->toHtml();
	}
	
	
	
	public function runreportAction() {

		$this->hasPermission();
		
		$id = $this->getRequest()->getParam("id");
		$params = $this->getRequest()->getParam("params");
		
		if($this->getRequest()->getParam("export") == "true")
		{
			$report = Mage::getModel("sqlReports/sqlReport")->load($id);
			$results = $report->runReport($params, true);
			
			$columns = $results["columns"];
			$data = $results["data"];

			//merge and create export.
			$columnNames = array();

			foreach($columns as $c)
			{
				$columnNames[] = $c["name"];
			}

			if($report->getHasSubQuery())
			{
				$newDataWithSubQuery =array();

				$subQueryColumns = $results["sub_data"][0]["columns"];
				$subQueryData = $results["sub_data"];


				$preSubQueryColumnsNames = $columnNames;

				foreach($subQueryColumns as $c)
				{
					$columnNames[] = $c["name"];
				}

				$subQueryMergeCount = 0;
				foreach ($data as $key => $d) {
					//var_dump($d);

					$emptyArrayKey = array();
					foreach(array_keys($d) as $arrayKeys)
					{
						$emptyArrayKey[$arrayKeys] = null;
					}

					foreach ($subQueryData[$key]['data'] as $subQueryColumnKey => $subQueryColumn) {

						if($subQueryColumnKey == 0) {
							$newDataWithSubQuery[$subQueryMergeCount] = $d + $subQueryColumn;
						}else
						{
							$newDataWithSubQuery[$subQueryMergeCount] = $emptyArrayKey+ $subQueryColumn;
						}

						$subQueryMergeCount++;
					}

					$subQueryMergeCount++;
				}

				$data = $newDataWithSubQuery;
			}

			$all = array_merge(array($columnNames), $data);
			
			header("Content-type: text/csv");
		    header("Content-Disposition: attachment; filename=" . str_replace(" ", "_", $report->getTitle())  . ".csv");
		    header("Pragma: no-cache");
		    header("Expires: 0");

	        $outputBuffer = fopen("php://output", 'w');
	        foreach($all as $val) {
	            fputcsv($outputBuffer, $val);
	        }
	        fclose($outputBuffer);

		}
		else
		{
			Mage::register('report_id', $id);
			Mage::register('report_params', $params);
			
			$grid = $this->getLayout()->createBlock("sqlReports/adminhtml_grid");
			
			echo $grid->toHtml();
		}
		
		

	}
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('report');
    }
}