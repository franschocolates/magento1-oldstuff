<?php

class GoSolid_SqlReports_Adminhtml_Sqlreports_SqlreportsController extends Mage_Adminhtml_Controller_action
{
	
	protected function hasPermission()
	{
		if (!Mage::getSingleton('admin/session')->isAllowed('admin/report/sqlreports/sqlreports'))
		{
			$this->getResponse()->setRedirect($this->getUrl('*/*/denied'));
		}
	}
	
	
	protected function _initAction() 
	{
		//TODO: Remove this controller after validating that it is not needed.
		
		$layout = $this->loadLayout();
		
		
		
		//Optional
		//$this->_setActiveMenu('system/pstadmin');
		//$this->_title($this->__('sqlReports'))->_title($this->__('PST Admin'))->_title($this->__('Event Type Management'));
		
		return $this;
	}   
 
	public function indexAction() {

		
		$this->_initAction()
			->renderLayout();
		

	}

	public function editAction() {

		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('sqlReports/sqlReport')->load($id);
		
		Mage::register('sqlreports_data', $model);
		
		if ($model->getId() || $id == 0) {
				$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
				if (!empty($data)) {
					$model->setData($data);
				}

				$this->loadLayout();
				
				//Optional for WYSIWYG must change loading blocks to XML
				//$this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
				//$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
				
				$this->_addContent($this->getLayout()->createBlock('sqlReports/adminhtml_sqlreports_edit'))
					->_addLeft($this->getLayout()->createBlock('sqlReports/adminhtml_sqlreports_edit_tabs'));
					
				$this->renderLayout();

			}
			else
			{
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('sqlReports')->__('Record does not exist'));
				$this->_redirect('*/*/');
			}

	}
	
	
	public function newAction() {
		$this->_forward('edit');

	}
	
	public function saveAction() {
		$session = Mage::getSingleton('adminhtml/session');
		if ($data = $this->getRequest()->getPost()) {
			
			$model = Mage::getModel('sqlReports/sqlReport');
			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));

			try {


				$model->save();

				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('sqlReports')->__('Record was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
				
			}
			catch(Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
			}
			
		}
		
		//on save and edit
		 if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('page_id' => $model->getId(), '_current'=>true));
                    return;
                }
		
		
		//no agency to save.
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('sqlReports')->__('Record not found.'));
        $this->_redirect('*/*/');
		
	}
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('report');
    }
	
}