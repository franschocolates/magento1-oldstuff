<?php
class GoSolid_JssCssFlush_Model_Design_Package extends Mage_Core_Model_Design_Package
{	
	 /**
     * Merge specified css files and return URL to the merged file on success
     *
     * @param $files
     * @return string
     */
    public function getMergedCssUrl($files)
    {
        // secure or unsecure
        $isSecure = Mage::app()->getRequest()->isSecure();
        $mergerDir = $isSecure ? 'css_secure' : 'css';
        $targetDir = $this->_initMergerDir($mergerDir);
        if (!$targetDir) 
        {
            return '';
        }

        // base hostname & port
        $baseMediaUrl = Mage::getBaseUrl('media', $isSecure);
        $hostname = parse_url($baseMediaUrl, PHP_URL_HOST);
        $port = parse_url($baseMediaUrl, PHP_URL_PORT);
        if (false === $port) 
        {
            $port = $isSecure ? 443 : 80;
        }
	        
        // merge into target file
        $targetFilename = md5(implode(',', $files) . "|{$hostname}|{$port}") . '.css';
        
        $newTargetFilename = $this->_getUniqueFileName($targetFilename, "css");
        
        $mergeFilesResult = $this->_mergeFiles(
            $files, $targetDir . DS . $newTargetFilename,
            false,
            array($this, 'beforeMergeCss'),
            'css'
        );
        
        
        if ($mergeFilesResult) 
        {
            return $baseMediaUrl . $mergerDir . '/' . $newTargetFilename;
        }
        return '';
    }
    
	public function getMergedJsUrl($files)
    {
        $targetFilename = md5(implode(',', $files)) . '.js';
        $targetDir = $this->_initMergerDir('js');
        
        
        
        $newTargetFilename = $this->_getUniqueFileName($targetFilename, "js");
        
        
        
        
        if (!$targetDir) {
            return '';
        }
        if ($this->_mergeFiles($files, $targetDir . DS . $newTargetFilename, false, null, 'js')) {
            return Mage::getBaseUrl('media', Mage::app()->getRequest()->isSecure()) . 'js/' . $newTargetFilename;
        }
        return '';
    }
    
    protected function _getUniqueFileName($targetFileName, $type)
    {
    	$newFileName = $targetFileName;
    	
    	// check for merged file key
    	$filePath = Mage::getBaseDir('media') . DS . $type . DS . 'mergedFile.txt';
    	
    	$mergedValues = $this->_getMergedValues($type);
    	
    	if(array_key_exists($targetFileName, $mergedValues))
    	{
    		//key is already there
    		$newFileName = $mergedValues[$targetFileName];
    	}
    	else
    	{
    		//key is NOT there
    		
    		//create the new file name
    		$newFileName .= $this->_generateRandomString(10) . "." . $type;
    		
    		//save the new key
    		$this->_saveMergedValue(
    			array($targetFileName => $newFileName), $type
    		);
    	}
    	return $newFileName;    	
    }
    
    
    protected function _getMergedValues($type)
    {
 
    	$filePath = Mage::getBaseDir('media') . DS . $type . DS . 'mergedFile.txt';
    	
    	if (file_exists($filePath))
    	{
    		$mergedValues = parse_ini_file($filePath);
    		return $mergedValues;
    	}
    	else
    	{
    		// create a merged file
    		//$this->_write_ini_file(array(), $filePath, $has_sections=FALSE);
    		return array();
    	}
    	
    }
    
    protected function _saveMergedValue($value, $type)
    {
    	$filePath = Mage::getBaseDir('media') . DS . $type . DS . 'mergedFile.txt';
    	
    	$mergedValues = $this->_getMergedValues($type);
    	
    	$mergedValues = array_merge($mergedValues, $value);
    	
    	$this->_write_ini_file($mergedValues, $filePath);    	
    }
    
	protected function _generateRandomString($length = 10) 
	{
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) 
	    {
	        $randomString .= $characters[rand(0, strlen($characters) - 1)];
	    }	
	    return $randomString;
	}
    
	protected function _write_ini_file($assoc_arr, $path, $has_sections=FALSE) 
	{ 
	    $content = ""; 
	    
		foreach ($assoc_arr as $key=>$elem) 
		{ 
	    	$content .= $key . " = " .$elem . "\r\n"; 
	    } 	   
	
	    if (!$handle = fopen($path, 'w')) 
	    { 
	        return false; 
	    } 
	    if (!fwrite($handle, $content)) 
	    { 
	        return false; 
	    } 
	    
	    fclose($handle); 
	    return true; 
	}


}