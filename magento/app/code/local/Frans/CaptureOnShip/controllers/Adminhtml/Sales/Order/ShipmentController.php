<?php

require_once "Mage/Adminhtml/controllers/Sales/Order/ShipmentController.php";

class Frans_CaptureOnShip_Adminhtml_Sales_Order_ShipmentController extends Mage_Adminhtml_Sales_Order_ShipmentController{

    protected function _saveShipment($shipment)
    {

        // save shipment
        parent::_saveShipment($shipment);

        // if specified, capture
        $shipment_post_data = $this->getRequest()->getPost('shipment');
        if(!empty($shipment_post_data['capture_payment'])){
            $this->_capturePayment($shipment);
        }

        return $this;
    }

    protected function _capturePayment($shipment){

        // reload fresh from db
        $order_id = $shipment->getOrder()->getId();
        $order = Mage::getModel('sales/order')->load($order_id);

        // only invoice fully shipped, and invoice-able, orders
        if($order->canShip() || !$order->canInvoice()){
            return $this;
        }

        $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();

        if (!$invoice->getTotalQty()) {
            return $this;
        }

        $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_ONLINE);
        $invoice->register();

        $invoice_order = $invoice->getOrder();
        
        $transactionSave = Mage::getModel('core/resource_transaction')
                                                ->addObject($invoice)
                                                ->addObject($invoice_order);

        $transactionSave->save();

    }

}