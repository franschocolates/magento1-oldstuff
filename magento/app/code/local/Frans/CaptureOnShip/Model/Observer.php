<?php

class Frans_CaptureOnShip_Model_Observer{


    // note: currently we handle this in the controller to give admins
    // the ability to toggle on/off whether to capture, per shipment
    // but keeping this around in case it comes in handy as business
    // logic evolves.

    // public function capturePaymentOnShip(Varien_Event_Observer $observer){

    //     $shipment = $observer->getEvent()->getShipment();
    //     $order = $shipment->getOrder();

    //     // only invoice fully shipped, and invoice-able, orders
    //     if($order->canShip() || !$order->canInvoice()){
    //         return $this;
    //     }

    //     $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();

    //     if (!$invoice->getTotalQty()) {
    //         return $this;
    //     }

    //     $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_ONLINE);
    //     $invoice->register();
    //     $transactionSave = Mage::getModel('core/resource_transaction')
    //                                             ->addObject($invoice)
    //                                             ->addObject($invoice->getOrder());

    //     $transactionSave->save();

    //     return $this;
    // }


}