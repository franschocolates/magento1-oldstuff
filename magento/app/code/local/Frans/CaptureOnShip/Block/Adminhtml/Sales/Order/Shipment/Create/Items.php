<?php

class Frans_CaptureOnShip_Block_Adminhtml_Sales_Order_Shipment_Create_Items
    extends Mage_Adminhtml_Block_Sales_Order_Shipment_Create_Items{

        public function shouldOfferToCapturePayment(){
            // the overridden order model is multi-ship aware
            // when determining if we can invoice
            // so our job here is simple
            return $this->getOrder()->canInvoice();
        }

}