<?php

class Frans_PaymentsHelper_Model_Payment_Method_Gravity extends Mage_Payment_Model_Method_Abstract{

    const CODE = 'gravity';

    protected $_code = self::CODE;
    protected $_canUseInternal = true;
    
    # this means frontend, so we can filter so that it's admin only
    protected $_canUseCheckout = false;
    protected $_canOrder       = true;

}