<?php

require_once "aws-autoloader.php";
use Aws\Ec2\Ec2Client;
use Aws\ElasticBeanstalk\ElasticBeanstalkClient;


class Frans_ContentDelivery_Helper_Turpentine_Varnish extends Nexcessnet_Turpentine_Helper_Varnish{

    const CACHE_KEY = "frans:contentDelivery:varnishInstances";

    protected function _getELBInstancePrivateIPs(){

        $app = Mage::app();

        // test if we've run this within the last hour
        $private_ips = $app->loadCache(self::CACHE_KEY);

        // return if they're fresh
        if($private_ips):
            return unserialize($private_ips);
        endif;

        $elb_client = ElasticBeanstalkClient::factory(array(
                            'region' => 'us-west-2',
                            'version' => '2010-12-01',
                            'credentials' => array(
                                'key' => getenv('AWS_ACCESS_KEY_ID'),
                                'secret' => getenv('AWS_SECRET_ACCESS_KEY')
                            )
                        ));

        $env_name = getenv('FRANS_ENV');

        // our production admin environment is called "magento1-production-admin"
        // instead of "magento1-production-admin-alb" so
        // we just have to tell our production admin environment to target
        // the public production environment
        if($env_name == 'production-admin'){
            $env_name = 'production';
        }

        $instance_health = $elb_client->describeInstancesHealth(
                                array('EnvironmentName' => 'magento1-' . $env_name . '-alb'));


        $instance_ids = array_map(function ($i){
                                    return $i['InstanceId'];
                                }, $instance_health['InstanceHealthList']);

        $private_ips = array();

        $ec2_client = Ec2Client::factory(array(
                            'region' => 'us-west-2',
                            'version' => '2015-10-01',
                            'credentials' => array(
                                'key' => getenv('AWS_ACCESS_KEY_ID'),
                                'secret' => getenv('AWS_SECRET_ACCESS_KEY')
                            )
                        ));

        $reservations = $ec2_client->describeInstances(array('InstanceIds' => $instance_ids))['Reservations'];

        foreach($reservations as $reservation):
            foreach($reservation['Instances'] as $instance):
                array_push($private_ips, $instance['PrivateIpAddress'] . ":2000");
            endforeach;
        endforeach;

        // cache for one hour.
        $app->saveCache(serialize($private_ips), self::CACHE_KEY, array('config'), 3600);

        return $private_ips;

    }

    public function getSockets() {

        $env = getenv('FRANS_ENV');

        // use default behavior for non-AWS / EB environments.
        if(!$env){
            return parent::getSockets();
        }

        $sockets = array();
        $servers = $this->_getELBInstancePrivateIPs();

        $key = str_replace('\n', PHP_EOL,
            Mage::getStoreConfig('turpentine_varnish/servers/auth_key'));
        $version = Mage::getStoreConfig('turpentine_varnish/servers/version');
        if ($version == 'auto') {
            $version = null;
        }
        foreach ($servers as $server) {
            $parts = explode(':', $server);
            $sockets[] = $this->getSocket($parts[0], $parts[1], $key, $version);
        }
        return $sockets;
    }


}