<?php


class Frans_ContentDelivery_Model_Observer{

    public function purgeHomepageAfterUpdatingHero($observer){
        $varnish = Mage::getModel('turpentine/varnish_admin');
        $varnish->flushUrl('/');
    }

    public function purgeBannerUrls($observer){

        $event = $observer->getEvent();
        $banner = $event->getObject();

        $varnish = Mage::getModel('turpentine/varnish_admin');

        // based on GoSolid_Banner_Model_Banner line 13
        $banner_positions_to_urls = array(
            'businessaccounts_backdrop' => '/business-accounts/',
            'businessaccounts_tile' => '/business-accounts/',
            'contact_backdrop' => '/contact/',
            'eventfavors_backdrop' => '/event-favors/',
            'eventfavors_tile' => '/event-favors/',
            'giftcards_backdrop' => '/gifts/gift-cards',
            'giftcardbalance_backdrop' => '/gifts/gift-cards',
            'homepage_tile' => '/',
            'homepage_retail' => '/',
            'cms_backdrop_default' => '.*'  // go big
        );

        if(array_key_exists($banner->getType(), $banner_positions_to_urls)){
            $varnish->flushUrl($banner_positions_to_urls[$banner->getType()]);
        } else {
            $varnish->flushUrl('/' . $banner->getType() . '*');
        }

    }


}