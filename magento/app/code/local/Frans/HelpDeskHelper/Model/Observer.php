<?php


class Frans_HelpDeskHelper_Model_Observer{

    public function setStoreIdOnTicket($observer){

        $ticket = $observer->getEvent()->getTicket();

        if(!$ticket->getStoreId()){
            // should be a dynamic value but if the lookup is failing elsewhere,
            // let's just set it to what we need it to be, for now.
            $ticket->setStoreId(1);
        }
    }

}