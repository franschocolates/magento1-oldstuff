<?php

$installer = $this;
$installer->startSetup();

$orderGridTable = $installer->getTable('sales/order_grid');

$installer->getConnection()->addColumn($orderGridTable,
    'gift_message_id', 'int(11) null default null AFTER `origination`');

$installer->endSetup(); 

?>