<?php

$installer = $this;

$installer->startSetup();

$installer = $this;
$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable('admin/user'), 'default_origination', array(
    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length' => 256,
    'nullable' => true,
    'default' => null,
    'comment' => 'Default Origination'
));

$installer->endSetup(); 

?>