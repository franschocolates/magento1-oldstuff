<?php

$installer = $this;
$installer->startSetup();

$orderTable = $installer->getTable('sales/order');

$installer->getConnection()->modifyColumn($orderTable,
	'origination', "varchar(255) null default 'WEB' AFTER `gift_message_id`");

$installer->getConnection()->modifyColumn($orderTable,
	'accounting_label', "varchar(255) null default 'WEB' AFTER `origination`");

$installer->endSetup(); 

?>