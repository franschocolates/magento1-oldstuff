<?php

$installer = $this;
$installer->startSetup();

$orderGridTable = $installer->getTable('sales/order_grid');

$installer->getConnection()->addColumn($orderGridTable,
    'admin_note_count', 'int(11) not null default 0');

$orderTable = $installer->getTable('sales/order');

$installer->getConnection()->addColumn($orderTable,
    'admin_note_count', 'int(11) not null default 0');

$installer->endSetup(); 

?>