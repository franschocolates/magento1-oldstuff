<?php

$installer = $this;

$installer->startSetup();

$installer = $this;
$installer->startSetup();

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$entityTypeId     = $setup->getEntityTypeId('customer');
$attributeSetId   = $setup->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);

// first, adding Customer Number
$setup->addAttribute('customer', 'customer_number', array(
    'input'         => 'text',
    'type'          => 'int',
    'label'         => 'Customer Number',
    'visible'       => true,
    'required'      => false,
    'user_defined'  => true,
    'unique'        => true
));

$setup->addAttributeToGroup(
 $entityTypeId,
 $attributeSetId,
 $attributeGroupId,
 'customer_number',
 '999'  //sort_order
);

$oAttribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'customer_number');
$oAttribute->setData('used_in_forms', array('adminhtml_customer'));
$oAttribute->save();

$setup->endSetup();

$installer->addAttribute('order', 'origination', array(
    'type'     => Varien_Db_Ddl_Table::TYPE_VARCHAR,
    'visible'  => true,
    'required' => false
));

$installer->addAttribute('quote', 'origination', array(
    'type'     => Varien_Db_Ddl_Table::TYPE_VARCHAR,
    'visible'  => true,
    'required' => false
));

$installer->addAttribute('order', 'accounting_label', array(
    'type'     => Varien_Db_Ddl_Table::TYPE_VARCHAR,
    'visible'  => true,
    'required' => false
));

$installer->addAttribute('quote', 'accounting_label', array(
    'type'     => Varien_Db_Ddl_Table::TYPE_VARCHAR,
    'visible'  => true,
    'required' => false
));


$installer->endSetup(); 

?>