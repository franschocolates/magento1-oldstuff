<?php

$installer = $this;
$installer->startSetup();

$orderGridTable = $installer->getTable('sales/order_grid');

$installer->getConnection()->modifyColumn($orderGridTable,
    'origination', "varchar(255) null default 'WEB' AFTER `updated_at`");

$installer->getConnection()->addColumn($orderGridTable,
    'accounting_label', "varchar(255) null default 'WEB' AFTER `updated_at`");

$installer->endSetup(); 

?>