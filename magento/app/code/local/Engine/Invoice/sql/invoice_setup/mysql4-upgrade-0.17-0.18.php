<?php

$installer = $this;
$installer->startSetup();

$orderGridTable = $installer->getTable('sales/order_grid');

$installer->getConnection()->addColumn($orderGridTable,
    'ship_note_id', 'int(11) null default null AFTER `gift_message_id`');

$installer->endSetup(); 

?>