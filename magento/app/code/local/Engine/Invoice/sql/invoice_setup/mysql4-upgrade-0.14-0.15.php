<?php

$installer = $this;
$installer->startSetup();

$orderGridTable = $installer->getTable('sales/order_grid');

$installer->getConnection()->addColumn($orderGridTable,
    'origination', 'varchar(255) null default null AFTER `store_id`');

$installer->endSetup(); 

?>