<?php

$installer = $this;
$installer->startSetup();

$orderGridTable = $installer->getTable('sales/order_grid');

$installer->getConnection()->addColumn($orderGridTable,
    'shipping_city_state', "varchar(255) null default null AFTER `billing_name`");

$installer->endSetup(); 

?>