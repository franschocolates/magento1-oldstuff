<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()->modifyColumn($installer->getTable('admin/user'),
    'default_origination', "VARCHAR(255) null default 'MO'");

$installer->endSetup(); 

?>