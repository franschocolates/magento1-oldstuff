<?php

$installer = $this;
$installer->startSetup();

$orderGridTable = $installer->getTable('sales/order_grid');

$installer->getConnection()->addColumn($orderGridTable,
    'shipping_description', "varchar(255) null default null AFTER `shipping_city_state`");

$installer->endSetup(); 

?>