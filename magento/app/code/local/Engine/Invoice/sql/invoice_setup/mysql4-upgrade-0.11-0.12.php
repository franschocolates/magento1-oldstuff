<?php

$installer = $this;

$installer->startSetup();

$installer->addAttribute('quote_item', 'notes', array(
    'type'     => Varien_Db_Ddl_Table::TYPE_VARCHAR,
    'visible'  => true,
    'required' => false
));

$installer->addAttribute('quote_item', 'category', array(
    'type'     => Varien_Db_Ddl_Table::TYPE_VARCHAR,
    'visible'  => true,
    'required' => false
));

$installer->addAttribute('quote_item', 'order_type', array(
    'type'     => Varien_Db_Ddl_Table::TYPE_VARCHAR,
    'visible'  => true,
    'required' => false
));

$installer->addAttribute('order_item', 'notes', array(
    'type'     => Varien_Db_Ddl_Table::TYPE_VARCHAR,
    'visible'  => true,
    'required' => false
));

$installer->addAttribute('order_item', 'category', array(
    'type'     => Varien_Db_Ddl_Table::TYPE_VARCHAR,
    'visible'  => true,
    'required' => false
));

$installer->addAttribute('order_item', 'order_type', array(
    'type'     => Varien_Db_Ddl_Table::TYPE_VARCHAR,
    'visible'  => true,
    'required' => false
));

$installer->endSetup(); 

?>