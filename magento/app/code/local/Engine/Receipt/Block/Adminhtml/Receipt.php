<?php

class Engine_Receipt_Block_Adminhtml_Receipt
    extends Mage_Adminhtml_Block_Template
{

  public function getOrderIds(){
    $orderIds = explode(',', $this->getRequest()->getParam('order_ids'));
    return $orderIds;
  }

  public function getOrders(){
    $orderIds = $this->getOrderIds();
    $orders = array();
    foreach($orderIds as $orderId){
    	$orders[] = Mage::getModel('sales/order')->load($orderId);
    }
    return $orders;
  }

    /**
     * Transforms something that could have multiple lines (like an admin note)
     * Into HTML with multiple lines
     * So that they don't have to put br tags into the fields
     *
     * @param $value
     */
    public function getMultilineField($value)
    {
        // I tried to use PHP_EOL but for some reason on windows it didn't work
        // so using \n, since that will happen on both windows & linux (not sure if mac clients will have a problem)
        $replaced = str_replace("\n", '<br />', $value);
        return $replaced;
    }
}