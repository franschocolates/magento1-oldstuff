<?php

class Engine_Receipt_Block_Adminhtml_Receipt_Multiple extends Engine_Receipt_Block_Adminhtml_Receipt_Container{

  public function _beforeToHtml(){  	
	$data = $this->getData();
	if ($data['mode'])
	{
		$mode = "'" . $data['mode'] . "'";
	}	
	
  	$params = $this->getRequest()->getParams();
	$returnUrl = isset($params['return_order_id']) ? 
  		'setLocation(\'' . $this->getUrl('*/*/view', array('order_id' => $params['return_order_id'])) . '\');'
  		: 'setLocation(\'' . $this->getUrl('*/*/index') . '\');';

    $this->_addButton('back', array(
            'label'   => Mage::helper('sales')->__('Back'),
            'onclick' => $returnUrl,
            'class'   => 'back'
        ));

    if(!$this->hasMultiple()):
      $this->_addButton('print', array(
              'label'   => Mage::helper('sales')->__('Print'),
              'onclick' => "frans.printAction($mode);"
          ));
    endif;

  }
  
}