<?php

class Engine_Receipt_Block_Adminhtml_Sales_Order_View extends Mage_Adminhtml_Block_Sales_Order_View{

	public function __construct(){
		parent::__construct();

		$this->_addButton('receipt', array(
				'label' 	=> Mage::helper('receipt')->__('Print Receipts'),
				'onclick'	=> 'setLocation(\'' . $this->getUrl('*/sales_order/receipt') . '\');',
				'class'		=> 'go'
				), 0, 100, 'header', 'header');
	}

}