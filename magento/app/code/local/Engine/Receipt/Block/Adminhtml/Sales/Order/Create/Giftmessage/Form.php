<?php

class Engine_Receipt_Block_Adminhtml_Sales_Order_Create_Giftmessage_Form extends Mage_Adminhtml_Block_Sales_Order_Create_Giftmessage_Form
{

    protected function _prepareVisibleFields(Varien_Data_Form_Element_Fieldset $fieldset)
    {
        // made these hidden fields...
        $fieldset->addField('sender', 'hidden',
            array(
                'name'     => $this->_getFieldName('sender'),
                'label'    => Mage::helper('sales')->__('From'),
                'required' => $this->getMessage()->getMessage() ? true : false
            )
        );
        $fieldset->addField('recipient', 'hidden',
            array(
                'name'     => $this->_getFieldName('recipient'),
                'label'    => Mage::helper('sales')->__('To'),
                'required' => $this->getMessage()->getMessage() ? true : false
            )
        );

        $fieldset->addField('message', 'textarea',
            array(
                'name'      => $this->_getFieldName('message'),
                'label'     => Mage::helper('sales')->__('Message'),
                'rows'      => '5',
                'cols'      => '20',
            )
        );
        return $this;
    }

}
