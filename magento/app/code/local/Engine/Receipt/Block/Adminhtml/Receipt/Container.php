<?php

class Engine_Receipt_Block_Adminhtml_Receipt_Container extends Mage_Adminhtml_Block_Widget_Container{

  public function __construct(){
    parent::__construct();

    $session = Mage::getSingleton('adminhtml/session');
    $order_collection = $this->getPrintedOrderCollection();

    foreach($order_collection as $order):
        $session->addError("Heads up! Order #" . $order->getIncrementId() . " has been at least partially printed!");
    endforeach;

  }


  public function hasMultiple(){
    return $this->getMode() == 'all';
  }

  public function getIsVisible($receiptIdentifier){
    return in_array($this->getMode(), array('all', $receiptIdentifier));
  }

  public function _beforeToHtml(){

    // just one order. back to it.
    $this->_addButton('back', array(
            'label'   => Mage::helper('sales')->__('Back'),
            'onclick' => 'setLocation(\'' . $this->getUrl('*/*/view', array('order_id' => $this->getRequest()->getParam('order_id'))) . '\');',
            'class'   => 'back'
        ));

  }

  public function getOrderIds(){
    $order_ids = $this->getRequest()->getPost('order_ids', $this->getRequest()->getParam('order_id', null));
    if(gettype($order_ids) == "string" && strpos($order_ids, ",") !== false){
      $order_ids = explode($order_ids, ",");
    }
    return $order_ids;
  }

  public function getOrderCollection(){
    $order_ids = $this->getOrderIds();
    return Mage::getModel('sales/order')->getCollection()->addAttributeToFilter('entity_id', array('in' => $order_ids));
  }

  public function getPrintedOrderCollection(){
    $order_ids = $this->getOrderIds();
    return Mage::getModel('sales/order')
                                ->getCollection()
                                ->addAttributeToFilter('entity_id', array('in' => $order_ids))
                                ->addAttributeToFilter('status', GoSolid_Frans_Model_Sales_Order::STATUS_PRINTED);
  }
  
}