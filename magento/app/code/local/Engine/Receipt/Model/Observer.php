<?php

class Engine_Receipt_Model_Observer{
	
	public function applyShippedLabelToShippedOrder($observer){
		$event = $observer->getEvent();
		$shipment = $event->getShipment();
		$order = $shipment->getOrder();

		// we set state first so as to out-clever Magento;
		// without this, Magento will auto-convert any 'new' orders to 'processing'
		// see app/code/core/Mage/Sales/Model/Order.php line 2110
		// printed orders should already be flipped to 'processing'
		// but just in case...
		$order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, false)
				->setStatus('shipped');

		return $this;
	}

	public function captureAdminUser($observer){

		$quote = $observer->getQuote();
		$adminUser = Mage::getSingleton('admin/session')->getUser();
		$adminUserName = $adminUser->getName();
		
		if ($quote->getIsMultiShipping())
		{
			$allOrders = $observer->getOrders();

            if(!$allOrders)
            {
                $order = $observer->getOrder();
                $allOrders = $order->getChildOrders();
                $allOrders[] = $order;
            }

			foreach ($allOrders as $order)
			{
				$order->setSalesperson($adminUserName)->save();
			}
		}
		else
		{
			$order = $observer->getOrder();
			$order->setSalesperson($adminUserName)->save();
		}

		return $this;
	}

}