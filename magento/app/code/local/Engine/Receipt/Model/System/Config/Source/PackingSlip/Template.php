<?php

class Engine_Receipt_Model_System_Config_Source_PackingSlip_Template extends Varien_Object
{

    public function toOptionArray()
    {
       return array(

            array(
                    'label' => 'Original',
                    'value' => 'receipt/packing-slips.phtml'
                ),
            array(
                    'label' => 'New - Half Legal',
                    'value' => 'receipt/packing-slip-new-halflegal.phtml'
                ),
            array(
                    'label' => 'New - A5',
                    'value' => 'receipt/packing-slip-new-a5.phtml'
            )

       );
    }

}
