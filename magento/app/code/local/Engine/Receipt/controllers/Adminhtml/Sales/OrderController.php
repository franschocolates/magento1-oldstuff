<?php

require_once "Mage/Adminhtml/controllers/Sales/OrderController.php";
class Engine_Receipt_Adminhtml_Sales_OrderController extends Mage_Adminhtml_Sales_OrderController{
	
	protected function _construct()
    {
        $this->setUsedModuleName('Mage_Sales');
    }

    /**
     * Init layout, menu and breadcrumb
     *
     * @return Mage_Adminhtml_Sales_OrderController
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('sales/order')
            ->_addBreadcrumb($this->__('Sales'), $this->__('Sales'))
            ->_addBreadcrumb($this->__('Orders'), $this->__('Orders'));
        return $this;
    }

	public function receiptAction(){
        $orderId = $this->getRequest()->getParam('order_id');
        $order = $this->_initOrder();
        $cache = Mage::app()->getCache();
        
        if ($order->getIsMultishipParent())
        {
        	$ordersArray = $order->getChildOrders();
        	$orderIdsArray = array();
        	foreach ($order->getChildOrders() as $childOrder)
        	{
				$orderIdsArray[] = $childOrder->getId();        		
        	}
       	}
        else
        {
        	$ordersArray = array($order);
        	$orderIdsArray = array($orderId);
        }

        $cache->save(serialize($ordersArray), 'orders-for-receipts-' . $orderId, array('orders_for_receipts'), 60*10);

        Mage::register('order_ids', $orderIdsArray);
        $this->_initAction();
        $this->renderLayout();
    }

    protected function _setOrders(){
        $orderIds = $this->getRequest()->getParam('order_ids', array());
        if ($orderIds && !is_array($orderIds))
        {
        	$orderIds = explode(',', $orderIds);
        }
        $this->loadLayout();
    }



    public function pickTicketAction(){
        $this->_setOrders();
        $root = $this->getLayout()->getBlock('root');
        $root->setStoreName(Mage::getStoreConfig('general/store_information/name'));
        $root->setStoreAddress(Mage::getStoreConfig('general/store_information/address'));
        $root->setStorePhone(Mage::getStoreConfig('general/store_information/phone'));
        $this->renderLayout();
    }

    public function pickInvoiceAction(){
        $this->_setOrders();
        $root = $this->getLayout()->getBlock('root');
        $root->setStoreName(Mage::getStoreConfig('general/store_information/name'));
        $root->setStoreAddress(Mage::getStoreConfig('general/store_information/address'));
        $root->setStorePhone(Mage::getStoreConfig('general/store_information/phone'));
        $this->renderLayout();
    }

    public function packingSlipAction(){
        $this->_setOrders();

        // set template based on config setting
        $packing_slip_template = Mage::getStoreConfig('frans/packing_slips/template');
        $root = $this->getLayout()->getBlock('root');
        $root->setTemplate($packing_slip_template);

        $this->renderLayout();
    }

    public function giftMessageAction(){
        $orderIds = $this->getRequest()->getParam('order_ids', array());
        if ($orderIds && !is_array($orderIds))
        {
            $orderIds = explode(',', $orderIds);
        }


        $orders = array();
        foreach($orderIds as $orderId){
            $orders[] = Mage::getModel('sales/order')->load($orderId);
        }


        $size = array(82.55,127.0); //w:3.25in h:5.00in



        //loop all the orders and create the PDF
        $html = "";
        foreach($orders as $o)
        {

            if($o->hasGiftMessage() == true)
            {
                $block = $this->getLayout()->createBlock("core/template")->setOrder($o)->setTemplate("receipt/gift-message.phtml");
                $html .= $block->toHtml();
                $html .= '<div style="page-break-after:always; clear:both"></div>';

            }

        }

        Mage::helper('frans/pdf')->htmlToPdf($html, GoSolid_Frans_Helper_Pdf::OUTPUT_BROWSER, "test.pdf", $size);


        //echo "dd";


        //$this->renderLayout();
    }

    // for use by Frans-specific mass actions below.
    protected function _initOrdersForReceipts(){

        $this->_initAction();

        $orderIds = $this->getRequest()->getPost('order_ids', array());
        $orders = array();
        foreach ($orderIds as $orderId) {
            $orders[] = Mage::getModel('sales/order')->load($orderId);
        }

        $cache = Mage::app()->getCache();
        $cache->save(serialize($orders), 'orders-for-receipts-' . implode('-', $orderIds), array('orders_for_receipts'), 60*10);

        Mage::register('order_ids', $orderIds);
        $this->loadLayout();
        $this->renderLayout();
    }

    // commence mass-printing actions.
    public function massPrintPickingTicketsAction(){
        $this->_initOrdersForReceipts();
    }

    public function massPrintPackingSlipsAction(){
        $this->_initOrdersForReceipts();
    }

    public function massPrintGiftMessagesAction(){
        $this->_initOrdersForReceipts();
    }

    public function massPrintAllReceiptsAction(){
        $this->_initOrdersForReceipts();
    }

    public function barcodeAction(){

        $text       = $this->getRequest()->getParam('text', 1);
        $quality    = $this->getRequest()->getParam('quality', 100);
        $width      = $this->getRequest()->getParam('width', 160);
        $height     = $this->getRequest()->getParam('height', 80);
        $format     = $this->getRequest()->getParam('format', 'PNG');
        $barcode    = $this->getRequest()->getParam('barcode', '');

        Mage::getModel('receipt/barcode')->generate($barcode, $width, $height, $quality, $format, $text);
    }

    public function massMarkReadyForCaptureAction(){

        $order_ids = $this->getRequest()->getPost('order_ids', array());
        $collection = Mage::getResourceModel('sales/order_collection')->addFieldToFilter('entity_id', array('in' => $order_ids));
        foreach($collection->getItems() as $order):
            $order->setIsReadyForCapture(true);
            $order->save();
        endforeach;
        $this->_getSession()->addSuccess(Mage::helper('receipt')->__('Successfully marked ' . $collection->count() . ' order(s) as Ready for Capture.'));
        return $this->_redirect('*/sales_order_accounting/index');

    }


}