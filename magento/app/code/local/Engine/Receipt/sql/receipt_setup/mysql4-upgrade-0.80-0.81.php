<?php

$installer = $this;

$installer->startSetup();

// create a printed status, which maps to 'processing'
$status = Mage::getModel('sales/order_status');
$status->setStatus('printed');
$status->setLabel('Printed');
$status->assignState('processing');
$status->save();

$installer->endSetup(); 

?>