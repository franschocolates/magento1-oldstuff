<?php

$installer = $this;

$installer->startSetup();

// create a printed status, which maps to 'processing'
$status = Mage::getModel('sales/order_status');
$status->setStatus('shipped');
$status->setLabel('Shipped');
$status->assignState('processing');
$status->save();

$installer->endSetup(); 

?>