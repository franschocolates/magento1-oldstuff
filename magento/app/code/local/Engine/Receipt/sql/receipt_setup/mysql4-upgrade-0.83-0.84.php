<?php

$installer = $this;

$installer->startSetup();

// create a 'new' status, which maps to 'processing'
$status = Mage::getModel('sales/order_status');
$status->setStatus('new');
$status->setLabel('New');
$status->assignState('processing');
$status->save();

$installer->endSetup(); 

?>