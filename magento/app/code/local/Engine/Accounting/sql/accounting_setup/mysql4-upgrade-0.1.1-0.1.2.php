<?php

$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable('sales/quote'), 'bill_date', array(
    'type' => Varien_Db_Ddl_Table::TYPE_DATETIME,
    'default' => null,
    'comment' => 'Bill Date'
));

$installer->getConnection()->addColumn($installer->getTable('sales/order'), 'bill_date', array(
    'type' => Varien_Db_Ddl_Table::TYPE_DATETIME,
    'default' => null,
    'comment' => 'Bill Date'
));

$installer->getConnection()->addColumn($installer->getTable('sales/order_grid'), 'bill_date', array(
    'type' => Varien_Db_Ddl_Table::TYPE_DATETIME,
    'default' => null,
    'comment' => 'Bill Date'
));

$installer->endSetup(); 

?>