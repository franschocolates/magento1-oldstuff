<?php

$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable('sales/quote'), 'ship_date', array(
    'type' => Varien_Db_Ddl_Table::TYPE_DATETIME,
    'default' => null,
    'comment' => 'Ship Date'
));

$installer->getConnection()->addColumn($installer->getTable('sales/order'), 'ship_date', array(
    'type' => Varien_Db_Ddl_Table::TYPE_DATETIME,
    'default' => null,
    'comment' => 'Ship Date'
));

$installer->getConnection()->addColumn($installer->getTable('sales/order_grid'), 'ship_date', array(
    'type' => Varien_Db_Ddl_Table::TYPE_DATETIME,
    'default' => null,
    'comment' => 'Ship Date'
));

$installer->endSetup(); 

?>