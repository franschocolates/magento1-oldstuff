<?php

$installer = $this;

$installer->startSetup();

// bill date

$installer->getConnection()->modifyColumn($installer->getTable('sales/quote'), 'bill_date', array(
    'type' => Varien_Db_Ddl_Table::TYPE_DATE,
    'default' => null,
    'comment' => 'Bill Date'
));

$installer->getConnection()->modifyColumn($installer->getTable('sales/order'), 'bill_date', array(
    'type' => Varien_Db_Ddl_Table::TYPE_DATE,
    'default' => null,
    'comment' => 'Bill Date'
));

$installer->getConnection()->modifyColumn($installer->getTable('sales/order_grid'), 'bill_date', array(
    'type' => Varien_Db_Ddl_Table::TYPE_DATE,
    'default' => null,
    'comment' => 'Bill Date'
));

// ship date

$installer->getConnection()->modifyColumn($installer->getTable('sales/quote'), 'ship_date', array(
    'type' => Varien_Db_Ddl_Table::TYPE_DATE,
    'default' => null,
    'comment' => 'Ship Date'
));

$installer->getConnection()->modifyColumn($installer->getTable('sales/order'), 'ship_date', array(
    'type' => Varien_Db_Ddl_Table::TYPE_DATE,
    'default' => null,
    'comment' => 'Ship Date'
));

$installer->getConnection()->modifyColumn($installer->getTable('sales/order_grid'), 'ship_date', array(
    'type' => Varien_Db_Ddl_Table::TYPE_DATE,
    'default' => null,
    'comment' => 'Ship Date'
));

$installer->endSetup(); 

?>