<?php

class Engine_Accounting_Helper_Data extends Mage_Core_Helper_Abstract{

    public function todaysDate(){
        return Mage::app()->getLocale()->date(null, Zend_Date::DATE_SHORT, null, false);
    }
    
}