<?php

class Engine_Accounting_ProductNotesController extends Mage_Adminhtml_Controller_Action{

    public function saveAction(){
        
        $item_id = $this->getRequest()->getParam('id');
        $data = $this->getRequest()->getParam('item');
        $item = Mage::getModel('sales/order_item')->load($item_id);
        
        $item->setOrderType($data[$item_id]['order_type']);
        $item->setNotes($data[$item_id]['notes']);
        $item->setCategory($data[$item_id]['category']);
        $item->save();

    }

}