<?php

class Engine_Accounting_Block_Adminhtml_Sales_Order_Grid extends Mage_Adminhtml_Block_Sales_Order_Grid
{

    protected function _prepareColumns()
    {

        parent::_prepareColumns();

        $this->addColumnAfter('shipping_city_state', array(
            'header' => Mage::helper('sales')->__('Destination'),
            'index'  => 'shipping_city_state',
        ), 'shipping_name');

        $this->addColumnAfter('shipping_description', array(
            'header' => Mage::helper('sales')->__('Shipping'),
            'index'  => 'shipping_description',
        ), 'shipping_city_state');

        $this->addColumnAfter('ship_date', array(
            'header' => Mage::helper('sales')->__('Ship Date'),
            'index'  => 'ship_date',
            'type'   => 'date'
        ), 'shipping_description');

        $labels = Mage::getModel('accounting/label')->toOptionHash();

        $this->addColumnAfter('accounting_label', array(
            'header' => Mage::helper('accounting')->__('Label'),
            'width'  => '80px',
            'type'   => 'options',
            'options' => $labels,
            'index'  => 'accounting_label',
        ), 'status');

        $this->addColumnAfter('gift_message_id', array(
            'index'  => 'gift_message_id',
            'header' => Mage::helper('accounting')->__('Gift'),
            'width'  => '65px',
            'type'   => 'isnull',
            'align'  => 'center',
      		'renderer'  => 'Engine_Accounting_Block_Adminhtml_Sales_Order_Grid_Renderer_GiftMessage',
        //  'html_decorators' => array('isnull'),
            ), 'origination');

        $this->addColumnAfter('ship_note_id', array(
            'index'   => 'ship_note_id',
            'header'  => Mage::helper('accounting')->__('Special Instructions'),
            'width'   => '70px',
            'type'    => 'isnull',
            'align'   => 'center',
            'html_decorators' => array('isnull'),
            ), 'gift_message_id');

        // removing a couple of redundant columns.
        // 'base' grand total is really only relevant with multiple currencies
        // and action is supplanted since you can click on any row
        $this->removeColumn('shipping_name');
        $this->removeColumn('base_grand_total');
        $this->removeColumn('action');
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('order_ids');
        $this->getMassactionBlock()->setUseSelectAll(false);

        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/cancel')) {
            $this->getMassactionBlock()->addItem('cancel_order', array(
                 'label'=> Mage::helper('sales')->__('Cancel'),
                 'url'  => $this->getUrl('*/sales_order/massCancel'),
            ));
        }

        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/hold')) {
            $this->getMassactionBlock()->addItem('hold_order', array(
                 'label'=> Mage::helper('sales')->__('Hold'),
                 'url'  => $this->getUrl('*/sales_order/massHold'),
            ));
        }

        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/unhold')) {
            $this->getMassactionBlock()->addItem('unhold_order', array(
                 'label'=> Mage::helper('sales')->__('Unhold'),
                 'url'  => $this->getUrl('*/sales_order/massUnhold'),
            ));
        }

        $this->getMassactionBlock()->addItem('capture_order', array(
                'label' => Mage::helper('accounting')->__('Capture'),
                'url'   => $this->getUrl('*/sales_order/massCapture')
            ));

        $this->getMassactionBlock()->addItem('receipt_picktickets', array(
                'label' => Mage::helper('sales')->__('Print Picking Tickets'),
                'url' => $this->getUrl('*/sales_order/massPrintPickingTickets')
            ));

        $this->getMassactionBlock()->addItem('receipt_packingslips', array(
                'label' => Mage::helper('sales')->__('Print Packing Slips'),
                'url' => $this->getUrl('*/sales_order/massPrintPackingSlips')
            ));

        $this->getMassactionBlock()->addItem('receipt_giftmessages', array(
                'label' => Mage::helper('sales')->__('Print Gift Messages'),
                'url' => $this->getUrl('*/sales_order/massPrintGiftMessages')
            ));

        $this->getMassactionBlock()->addItem('receipt_all', array(
                'label' => Mage::helper('sales')->__('Print All Receipts'),
                'url' => $this->getUrl('*/sales_order/massPrintAllReceipts')
            ));

        return $this;
    }

}
