<?php

class Engine_Accounting_Block_Adminhtml_Widget_Grid_Column_Filter_Greaterthanzero extends Mage_Adminhtml_Block_Widget_Grid_Column_Filter_Select
{
    protected function _getOptions()
    {
        return array(
                array('label' => '', 'value' => null),
                array('label' => 'Yes', 'value' => 1),
                array('label' => 'No', 'value' => 0)
            );
    }

    public function getCondition()
    {
        return $this->getValue() ? array('gt' => 0) : array('eq' => 0);
        
    }
}
