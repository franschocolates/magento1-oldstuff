<?php
class Engine_Accounting_Block_Adminhtml_Sales_Order_Grid_Renderer_GiftMessage extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	public function render(Varien_Object $row)
	{	
		if ($row->getGiftMessageId() != 0 && $row->getGiftMessageId() != null)
		{
			return 'X';
		}
		return '';
	}
}
?>