<?php

class Engine_Accounting_Block_Adminhtml_Widget_Grid_Column_Renderer_Greaterthanzero
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    protected function _getValue(Varien_Object $row)
    {
        $data = parent::_getValue($row);
        return (bool)$data;
    }
}
