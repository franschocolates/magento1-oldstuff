<?php

class Engine_Accounting_Model_Observer{

    public function captureOrderGridFields($observer){
        $event = $observer->getEvent();
        $resource = $event->getResource();

        $adapter       = $resource->getReadConnection();
        $concatCityState = $adapter->getConcatSql(array('{{table}}.city', $adapter->quote(', '), '{{table}}.region'));
        $resource->addVirtualGridColumn(
                'shipping_city_state',
                'sales/order_address',
                array('shipping_address_id' => 'entity_id'),
                $concatCityState
            );

        return $this;
    }

    public function captureBillDate($observer){

        $event = $observer->getEvent();
        $invoice = $event->getInvoice();
        $order = $invoice->getOrder();

        $order->setBillDate(Mage::helper('accounting')->todaysDate());

        return $this;
    }

}