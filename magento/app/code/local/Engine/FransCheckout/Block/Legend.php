<?php
/**
 * Frans
   ["billing"] => array(3) {
    ["label"] => string(19) "Billing Information"
    ["is_show"] => bool(true)
    ["allow"] => bool(true)
  }
  ["shipping"] => array(2) {
    ["label"] => string(20) "Shipping Information"
    ["is_show"] => bool(true)
  }
  ["shipping_method"] => array(2) {
    ["label"] => string(15) "Shipping Method"
    ["is_show"] => bool(true)
  }
  ["payment"] => array(2) {
    ["label"] => string(19) "Payment Information"
    ["is_show"] => bool(true)
  }
  ["review"] => array(2) {
    ["label"] => string(12) "Order Review"
    ["is_show"] => bool(true)
  }
**/
class Engine_FransCheckout_Block_Legend extends Mage_Checkout_Block_Onepage_Progress 
{
	public $currentStep;
	
	public function __construct()
	{
	    parent::__construct();	
	    $this->getCurrentStep();    
	    //$this->setTemplate('checkout/legend.phtml');
	}
	
	public function getCurrentStep(){
		if ($this->isStepComplete('shipping')){
			$this->currentStep = 'billing done';
		}else{
			$this->currentStep = 'testingtesting';			
		}		
	}
	
}