<?php

require_once 'Mage/Checkout/controllers/MultishippingController.php';

class Engine_FransCheckout_Checkout_MultishippingController extends Mage_Checkout_MultishippingController {

    /**
     * Action predispatch
     *
     * Check customer authentication for some actions
     *
     * @return Mage_Checkout_MultishippingController
     */
    public function preDispatch()
    {
        parent::preDispatch();

        if ($this->getFlag('', 'redirectLogin')) {
            return $this;
        }

        $action = $this->getRequest()->getActionName();

        $checkoutSessionQuote = $this->_getCheckoutSession()->getQuote();
        /**
         * Catch index action call to set some flags before checkout/type_multishipping model initialization
         */
        if ($action == 'index') {
            $checkoutSessionQuote->setIsMultiShipping(true);
            $this->_getCheckoutSession()->setCheckoutState(
                Mage_Checkout_Model_Session::CHECKOUT_STATE_BEGIN
            );
        } elseif (!$checkoutSessionQuote->getIsMultiShipping() &&
            !in_array($action, array('login', 'register', 'success'))
        ) {
        	/* ENGINE */
        	if(!Mage::helper('customer')->isLoggedIn()){
        		$customer = $this->createCustomer();
        		//echo "<pre>"; print_r($customer); echo "</pre>";
	        	//echo "<pre>"; print_r(Mage::helper('customer')->getCustomer()->getData()); echo "</pre>";
        	}
        	/* /ENGINE */
            $this->_redirect('*/*/index');
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
            return $this;
        }

        if (!in_array($action, array('login', 'register'))) {
            if (!Mage::getSingleton('customer/session')->authenticate($this, $this->_getHelper()->getMSLoginUrl())) {
                $this->setFlag('', self::FLAG_NO_DISPATCH, true);
            }

            if (!Mage::helper('checkout')->isMultishippingCheckoutAvailable()) {
                $error = $this->_getCheckout()->getMinimumAmountError();
                $this->_getCheckoutSession()->addError($error);
                $this->_redirectUrl($this->_getHelper()->getCartUrl());
                $this->setFlag('', self::FLAG_NO_DISPATCH, true);
                return $this;
            }
        }

        if (!$this->_preDispatchValidateCustomer()) {
            return $this;
        }

        if ($this->_getCheckoutSession()->getCartWasUpdated(true) &&
            !in_array($action, array('index', 'login', 'register', 'addresses', 'success'))
        ) {
            $this->_redirectUrl($this->_getHelper()->getCartUrl());
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
        }

        if ($action == 'success' && $this->_getCheckout()->getCheckoutSession()->getDisplaySuccess(true)) {
            return $this;
        }

        $quote = $this->_getCheckout()->getQuote();
        if (!$quote->hasItems() || $quote->getHasError() || $quote->isVirtual()) {
            $this->_redirectUrl($this->_getHelper()->getCartUrl());
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
            return;
        }

        return $this;
    }

    /*
    	creates a new customer
    	needs email, firstname, lastname, password
    */
    private function createCustomer(){
		/* Create a new customer */            
		$customer = Mage::getModel('customer/customer');
		//$customer  = new Mage_Customer_Model_Customer();
		$random = uniqid();
		$info['firstname'] = 'Guest';
		$info['lastname'] = 'Guest';
		$info['email'] = 'guest-' . $random . '@example.com';
		$info['password'] = md5( uniqid() );
		
		$customer->setWebsiteId(Mage::app()->getWebsite()->getId());
		$customer->loadByEmail( $info['email'] );
		//Zend_Debug::dump($customer->debug()); exit;
		if(!$customer->getId()) {
		    $customer->setEmail(  $info['email'] );
		    $customer->setPassword( $info['password'] );
		    $customer->setFirstname( $info['firstname'] );
		    $customer->setLastname( $info['lastname'] );
		    $customer->setGroupId( '11' );
		}
		try {
		    $customer->save();
		    $customer->setConfirmation(null);
		    $customer->save();
		    //$customer->sendNewAccountEmail();
		    //Make a "login" of new customer
		    Mage::getSingleton('customer/session')->loginById($customer->getId());			    
		    //Zend_Debug::dump($customer);
		    return $customer;
		}
		catch (Exception $ex) {
		    //Zend_Debug::dump($ex->getMessage());
		}    
    }
	
}