<?php
/**
 * Frans
 */

/**
 * Customer account controller
 */
 
require_once 'Mage/Customer/controllers/AccountController.php';
 
class Engine_FransCheckout_AccountController extends Mage_Customer_AccountController
{
    /**
     * Action predispatch
     *
     * Check customer authentication for some actions
     */
    public function preDispatch()
    {
        parent::preDispatch();

        // a brute-force protection here would be nice

        if (!$this->getRequest()->isDispatched()) {
            return;
        }

        $action = $this->getRequest()->getActionName();
        $openActions = array(
            'create',
            'login',
            'logoutsuccess',
            'forgotpassword',
            'forgotpasswordpost',
            'resetpassword',
            'resetpasswordpost',
            'confirm',
            'confirmation'
        );
        $pattern = '/^(' . implode('|', $openActions) . ')/i';

	    /** ENGINE **/
	    /**
	    	see if this is a guest
	    	should log out on my account view
	    **/
    	if( Mage::helper('customer')->isLoggedIn() ){
	    	$c = Mage::helper('customer')->getCustomer()->getData();
        	if( $c['group_id'] == 11 && $action != 'logout'){
	        	Mage::getSingleton('customer/session')->logout();
        	    //echo "<pre>"; print_r($c); echo "</pre>";
	        	//die('GUEST');     	
        	}
        }	
	    /** /ENGINE **/

        if (!preg_match($pattern, $action)) {
            if (!$this->_getSession()->authenticate($this)) {
                $this->setFlag('', 'no-dispatch', true);
            }
        } else {
            $this->_getSession()->setNoReferer(true);
        }
    }
}
