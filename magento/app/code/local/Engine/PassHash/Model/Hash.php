<?php
class Engine_PassHash_Model_Hash extends Mage_Core_Model_Encryption
{

	public function validateHash($password, $hash)
	{
        if ( strlen($hash) > 60 ) {            
            // create a hash of the plain text password and compare to the stored hash
            $hashed = $this->getHashSha($password);
            return $hashed == $hash;
        }
        return parent::validateHash($password, $hash);
    }
    
    /*
    	gets hash for old frans xcart site passwords
    */
    public function getHashSha($password, $salt = "-bahasdnfIdhasfdN$779#44-")
    {	    
        return $this->hashSha( $password . $salt );
    }

    /*
    	SHA 256 hash
    */
    public function hashSha($data)
    {
        return hash('sha256' , $data);
    }
    
    
}