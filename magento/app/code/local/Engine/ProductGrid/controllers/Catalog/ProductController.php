<?php
/**
 * Frans Extend
 * http://www.magentocommerce.com/boards/viewthread/9744/
 * http://stackoverflow.com/questions/11281543/magento-overload-adminhtml-productcontrollers-saveaction-method-wont-work
 */

class Engine_ProductGrid_Catalog_ProductController extends Mage_Adminhtml_Controller_Action
{
	public function massAttributeSetAction()
    {
        $productIds = $this->getRequest()->getParam('product');
        $storeId = (int)$this->getRequest()->getParam('store', 0);
        if(!is_array($productIds)) {
            $this->_getSession()->addError($this->__('Please select product(s)'));
        } else {
            try {
                foreach ($productIds as $productId) {
                    $product = Mage::getSingleton('catalog/product')
                        ->unsetData()
                        ->setStoreId($storeId)
                        ->load($productId)
                        ->setAttributeSetId($this->getRequest()->getParam('attribute_set'))
                        ->setIsMassupdate(true)
                        ->save();
                }
                Mage::dispatchEvent('catalog_product_massupdate_after', array('products'=>$productIds));
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($productIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/', array('store'=>(int)$this->getRequest()->getParam('store', 0)));
    }
}
