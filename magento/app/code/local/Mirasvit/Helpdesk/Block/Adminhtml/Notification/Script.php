<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at http://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   Help Desk MX
 * @version   1.5.4
 * @build     2445
 * @copyright Copyright (C) 2017 Mirasvit (http://mirasvit.com/)
 */



class Mirasvit_Helpdesk_Block_Adminhtml_Notification_Script extends Mage_Core_Block_Abstract {

    public function addNotificationScript()
    {
        $moduleName = Mage::app()->getRequest()->getControllerModule();
        if($moduleName == 'Mirasvit_Helpdesk_Adminhtml') {
            $parent = $this->getParentBlock();
            $parent->addJs('mirasvit/code/helpdesk/adminhtml/notification.js');
        }
    }

}