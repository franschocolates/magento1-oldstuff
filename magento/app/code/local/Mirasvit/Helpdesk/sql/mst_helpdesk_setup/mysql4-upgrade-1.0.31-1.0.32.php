<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at http://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   Help Desk MX
 * @version   1.5.4
 * @build     2445
 * @copyright Copyright (C) 2017 Mirasvit (http://mirasvit.com/)
 */


/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$version = Mage::helper('mstcore/version')->getModuleVersionFromDb('mst_helpdesk');
if ($version == '1.0.32') {
    return;
} elseif ($version != '1.0.31') {
    die('Please, run migration Helpdesk 1.0.31');
}
$installer->startSetup();
if (Mage::registry('mst_allow_drop_tables')) {
    $sql = "
       DROP TABLE IF EXISTS `{$this->getTable('helpdesk/third_party_email')}`;
       DROP TABLE IF EXISTS `{$this->getTable('helpdesk/third_party_email_store')}`;
       DROP TABLE IF EXISTS `{$this->getTable('helpdesk/third_party_email_department')}`;
    ";
    $installer->run($sql);
}
$sql = "
CREATE TABLE `{$this->getTable('helpdesk/third_party_email')}` (
  `third_party_email_id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NULL,
  `email` VARCHAR(255) NULL,
  `is_active` TINYINT(4) NULL,
  PRIMARY KEY (`third_party_email_id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `{$this->getTable('helpdesk/third_party_email_store')}` (
    `third_party_email_store_id` int(11) NOT NULL AUTO_INCREMENT,
    `ees_third_party_email_id` INT(11) NOT NULL,
    `ees_store_id` SMALLINT(5) unsigned NOT NULL,
    KEY `fk_helpdesk_third_party_email_store_third_party_email_id` (`ees_third_party_email_id`),
    CONSTRAINT `mst_1a81d169dfgdsfgdff13a00ef551f0c` FOREIGN KEY (`ees_third_party_email_id`) REFERENCES `{$this->getTable('helpdesk/third_party_email')}` (`third_party_email_id`) ON DELETE CASCADE ON UPDATE CASCADE,
    KEY `fk_helpdesk_third_party_email_store_store_id` (`ees_store_id`),
    CONSTRAINT `mst_46a07dfa45645692cc3136b8a804bfd80` FOREIGN KEY (`ees_store_id`) REFERENCES `{$this->getTable('core/store')}` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (`third_party_email_store_id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `{$this->getTable('helpdesk/third_party_email_department')}` (
    `third_party_email_department_id` int(11) NOT NULL AUTO_INCREMENT,
    `eed_third_party_email_id` INT(11) NOT NULL,
    `eed_department_id` SMALLINT(5) unsigned NOT NULL,
    KEY `fk_helpdesk_third_party_email_department_third_party_email_id` (`eed_third_party_email_id`),
    CONSTRAINT `mst_1a81d169d679d645436f13a00ef551f0d` FOREIGN KEY (`eed_third_party_email_id`) REFERENCES `{$this->getTable('helpdesk/third_party_email')}` (`third_party_email_id`) ON DELETE CASCADE ON UPDATE CASCADE,
    KEY `fk_helpdesk_third_party_email_department_department_id` (`eed_department_id`),
    CONSTRAINT `mst_46a07dfa1c318erte2cc3136b8a804bfd81` FOREIGN KEY (`eed_department_id`) REFERENCES `{$this->getTable('helpdesk/department')}` (`department_id`) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (`third_party_email_department_id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;
";

$helper = Mage::helper('helpdesk/migration');
$helper->trySql($installer, $sql);

/*                                    **/

$installer->endSetup();
