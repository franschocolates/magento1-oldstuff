<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at http://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   Help Desk MX
 * @version   1.5.4
 * @build     2445
 * @copyright Copyright (C) 2017 Mirasvit (http://mirasvit.com/)
 */


/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$version = Mage::helper('mstcore/version')->getModuleVersionFromDb('mst_helpdesk');
if ($version == '1.0.34') {
    return;
} elseif ($version != '1.0.33') {
    die('Please, run migration Helpdesk 1.0.34');
}
$installer->startSetup();
if (Mage::registry('mst_allow_drop_tables')) {
    $sql = "
       DROP TABLE IF EXISTS `{$this->getTable('helpdesk/customer')}`;
    ";
    $installer->run($sql);
}

$sql = "
CREATE TABLE `{$this->getTable('helpdesk/customer')}` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `customer_id` INT(10) UNSIGNED NOT NULL,
  `customer_note` TEXT,
  KEY `fk_helpdesk_customer_customer_id` (`customer_id`),
  CONSTRAINT `mst_d0532616888f8e2d970bsdfr74091585` FOREIGN KEY (`customer_id`) REFERENCES " .
    "`{$this->getTable('customer/entity')}` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;
";

$helper = Mage::helper('helpdesk/migration');
$helper->trySql($installer, $sql);

/*                                    **/

$installer->endSetup();
