<?php
/**
* notes:
* http://frans2.dev/nofrills_booklayout/index/helloblock
* http://frans2.dev/nofrills_booklayout/index/layout

Core Layout // adding a space at the slash , and capitalizing
Core Model Layout // Add the word Model in between
Mage Core Model Layout // Add the word Mage before
Mage_Core_Model_Layout // underscore the spaces

**/
class Nofrills_Booklayout_IndexController extends Mage_Core_Controller_Front_Action
{	
	public function indexAction()
	{
		//var_dump(__METHOD__);
		//$block = new Mage_Core_Block_Text ();
		//$block -> setText (" Hello World ");
		//var_dump ( $block -> getTemplateFile ()); //show where phtml should be located		
		/*
		$paragraph_block = new Mage_Core_Block_Text();
		$paragraph_block->setText('One paragraph to rule them all.');
		
		$main_block = new Mage_Core_Block_Template();
		$main_block->setTemplate('helloworld.phtml');
		
		$main_block->setChild( 'the_first' , $paragraph_block );
		*/
		$block_1 = new Mage_Core_Block_Text();
		$block_1->setText('Original Text');
		
		$block_2 = new Mage_Core_Block_Text();
		$block_2->setText('The second sentence.');
		
		$main_block = new Mage_Core_Block_Template();
		$main_block->setTemplate('helloworld.phtml');
		
		$main_block->setChild('the_first' , $block_1);
		$main_block->setChild('the_second' , $block_2);
		
		$block_1->setText('Wait, I want this text instead.');
		echo $main_block->toHtml();
	}
		
	public function helloblockAction() {
		$main_block = new Nofrills_Booklayout_Block_Helloworld();
		echo $main_block->toHtml(); 
	}
	
	public function layoutAction()
	{
		$layout = Mage::getSingleton('core/layout');
		$block = $layout->createBlock('core/template','root');
		$block->setTemplate('helloworld-2.phtml');
		echo $block->toHtml();
	}
}