<?php
/**
 * Stripe Info Form
 *
 * @category   IntellectLabs
 * @package    IntellectLabs_Stripe
 * @author     Matt Kammersell <matt@kammersell.com>
 * @copyright  Intellect Labs, Inc <http://www.intellectlabs.com>
 * @license		http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class IntellectLabs_Stripe_Block_Info_Stripe extends Mage_Payment_Block_Info_Cc
{
	protected function _isStripeCustomerCreate($info)
	{
		return ($info->getCreateStripeCustomer()>0 || strlen($info->getStripeCustomerId())>0); 
	}
	
    protected function _prepareSpecificInformation($transport = null)
    {
    	# goSolid - removed the functionality to call their API.
    	# technically this should actually all be moot, since we also removed the
    	# block seutp in the Payment class, but playing it safe (and trying to avoid misleading code)
    	return parent::_prepareSpecificInformation($transport);
   	}
}