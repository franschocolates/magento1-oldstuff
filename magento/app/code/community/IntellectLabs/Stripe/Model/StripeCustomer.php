<?php 
class IntellectLabs_Stripe_Model_StripeCustomer extends Mage_Core_Model_Abstract
{
	private $_stripeCustomerId = null;
	private $_stripeCustomer = null;
	
	
	public function init($customer)
	{
		$stripeCustomerId = $customer->getStripeCustomerId();
		$this->_stripeCustomerId = $stripeCustomerId;
		
		$payment = Mage::getModel("stripe/payment");
		$this->_stripeCustomer = $payment->getStripeCustomer($stripeCustomerId);
		return $this;
	}
	
	
	//return a list of cards.
	public function getCards()
	{
		$info = array();
		
		$cards = $this->_stripeCustomer->cards->data;
		
		foreach($cards as $card)
		{
			$info[] =  array(
				"last4" => $card->last4,
				"type" => $card->type,
				"exp_month" => $card->exp_month,
				"exp_year" => $card->exp_year,
				
			);
		}
		
		
		return $info;
		
		
	}
	
	//returns the information for the primary card.
	public function getPrimaryCardInfo()
	{
		if(count($this->getCards()) > 0)
		{
			$data = $this->_stripeCustomer->cards->data[0];
			
			$info = array(
				"last4" => $data->last4,
				"type" => $data->type,
				"exp_month" => $data->exp_month,
				"exp_year" => $data->exp_year,
			);
		}
		else
		{
			$info = null;
		}
		
		return $info;
	}
	
	
}
