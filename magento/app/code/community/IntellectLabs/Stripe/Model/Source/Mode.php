<?php
class IntellectLabs_Stripe_Model_Source_Mode
{

	const TEST = 'test';
	const LIVE = 'live';

	public function toOptionArray()
	{
		return array(
				array(
						'value' => self::TEST,
						'label' => Mage::helper('stripe')->__('Test')
				),
				array(
						'value' => self::LIVE,
						'label' => Mage::helper('stripe')->__('Live')
				),
		);
	}
}