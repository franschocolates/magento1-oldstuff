<?php
/**
 * Stripe payment method model
 *
 * @category	IntellectLabs
 * @package		IntellectLabs_Stripe
 * @author		Matthew Kammersell <matt@kammersell.com>
 * @copyright	Intellect Labs, Inc. <http://www.intellectlabs.com/>
 * @license		http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
require_once Mage::getBaseDir('lib') . DS . 'Stripe' . DS . 'Stripe.php';

class IntellectLabs_Stripe_Model_Payment extends Mage_Payment_Model_Method_Cc
{
	const VISA_REGEX = '^4[0-9]{12}(?:[0-9]{3})?$';
	const MC_REGEX = '^5[1-5][0-9]{14}$';
	const AMEX_REGEX = '^3[47][0-9]{13}$';
	const DINERS_REGEX = '^3(?:0[0-5]|[68][0-9])[0-9]{11}$';
	const DISCOVER_REGEX = '^6(?:011|5[0-9]{2})[0-9]{12}$';
	const JCB_REGEX = '^(?:2131|1800|35\d{3})\d{11}$';
	
	protected $_code	=	"stripe";
	
	protected $_formBlockType = 'stripe/form_stripe';
	protected $_isGateway				= true;
	protected $_canCapture				= true;
	protected $_canCapturePartial		= true;
	protected $_canRefund				= true;
	protected $_canRefundInvoicePartial = true;
	protected $_hasStripeProfile		= false;
	protected $_createStripeProfile 	= false;
	protected $_canSaveCc 				= false;
    protected $_canVoid                 = true; // added by goSolid to support canceling
	
	protected $_supportedCurrencyCodes = array("USD","CAN");
	protected $_minOrderTotal = 0.5;
	
	
	public function __construct()
	{
		Stripe::setApiKey($this->retrieveKey());
	}
	
	public function retrieveKey($public=false) {
		// modified to respect test & live mode.
		$mode = $this->getConfigData('mode');
		
		if ($public) {
			return $this->getConfigData("publishable_key_$mode");
		} else {
			return $this->getConfigData("secret_key_$mode");
		}
	}
	
	public function createStripeCustomer($token, $customer, $payment=false, $orderIncrementId = false) 
	{
		try {
			$metadata = array();
			
			if ($customer->getId())
			{
				$metadata['customer_id'] = $customer->getId();
				if ($orderIncrementId)
				{
					$metadata['created_for_order_id'] = $orderIncrementId;
				}
				$description = sprintf("Magento Customer %s <%s>", $customer->getName(), $customer->getEmail());
			}
			else
			{
				$description = "Magento Guest [Order #$orderIncrementId]";
			}
			
			// create a Customer
			$stripeCustomer = Stripe_Customer::create(array(
					'card' 		  => $token, 
					'email'		  => $customer->getEmail(),
					'metadata'	  => $metadata,
					"description" => $description,
			));
			
			# only save stripe customer if they have elected to
			if ($customer->getId() && ($payment === false || $payment->getCreateStripeCustomer())) {
				$customer->setStripeCustomerId($stripeCustomer->id)->save();
			}

			if ($payment) {
				$payment->setStripeCustomerId($stripeCustomer->id);
			}

			return $this;
		} catch(Stripe_CardError $e) {
			$this->debugData($e->getMessage());
			Mage::throwException($e->getMessage());
		} catch (Exception $e) {
			$this->debugData($e->getMessage());
            Mage::throwException(Mage::helper('stripe')->__($e->getMessage()));
		}
	}

    /**
     * Updates a StripeCustomer to have a new card, optionally setting it as default
     * If not default, you should pass in a variable to store the new Card ID, for charging
     *
     * @param $token
     * @param $customer
     * @param bool $setAsDefault
     * @param null $newCardId
     * @return $this
     */
    public function updateStripeCustomer( $token, $customer, $setAsDefault = true, &$newCardId = NULL)
	{
		try {
			$stripeCustomer = $this->getStripeCustomer($customer->getStripeCustomerId());

            // if we want to set as default, then we just set the card and that takes care of it
            // otherwise we add a card, and then add it to our list of temp cards
            // since they haven't indicated they want to save forever
            $newCard = $stripeCustomer->cards->create(array( 'card' => $token ) );

            if ($setAsDefault)
            {
                $stripeCustomer->default_card = $newCard->id;
            }
            else
            {
                // to update/change metadata, we have to actually set the value again to an array
                // we can't just change it inline (it doesn't detect the change, and so nothing happens)
                // so we force a new array with our temp card data and pass that in
                $metadata = isset($stripeCustomer->metadata) ?
                                $stripeCustomer->metadata->__toArray() : array();
                $tempCardIds = isset($metadata['temp_cards']) ?
                                explode(',', $metadata['temp_cards']) : array();
                $tempCardIds[] = $newCard->id;
                $metadata['temp_cards'] = implode(',', $tempCardIds);
                // reset to force it to save
                $stripeCustomer->metadata = $metadata;

                $newCardId = $newCard->id;
            }
            $stripeCustomer->save();


		} catch (Exception $e) {
            Mage::logException($e);
			Mage::throwException($e->getMessage());
		}		
		return $this;
	}

    /**
     * @param $stripeCustomerId
     * @return Stripe_Customer
     */
    public function getStripeCustomer($stripeCustomerId)
	{
		if ($stripeCustomerId) {
            try{
                return Stripe_Customer::retrieve($stripeCustomerId);
            } catch(Exception $e){
                Mage::log($e->getMessage(), null, null, 'exception.log', false);
                return array('error' => $e->getMessage());
            }
		}
		return false;
	}

    public function getStripeCustomerDefaultCard($stripeCustomerId)
    {
        if ($stripeCustomerId)
        {
            $stripeCustomer = $this->getStripeCustomer($stripeCustomerId);
            if (!is_object($stripeCustomer) || !$stripeCustomer->default_card)
            {
                return false;
            }

            $defaultCard = $stripeCustomer->default_card;
            $defaultCardList = array_filter($stripeCustomer->cards->data,
                function($c) use ($defaultCard) { return $c->id == $defaultCard; });

            if (count($defaultCardList) > 0)
            {
                return array_shift($defaultCardList);
            }
        }

        return false;
    }

	public function isStripeCustomer($stripeCustomerId) 
	{
		if (is_object($this->getStripeCustomer($stripeCustomerId))) {
			return true;
		} 
		return false;
	}
	
	public function getStripeToken($stripeTokenId)
	{
		if ($stripeTokenId) {
			return Stripe_Token::retrieve($stripeTokenId);
		}
		return false;
	}
	
	/**
	 * Assign data to info model instance
	 *
	 * @param   mixed $data
	 * @return  Mage_Payment_Model_Info
	 */
	public function assignData($data)
	{
		if (!($data instanceof Varien_Object)) {
			$data = new Varien_Object($data);
		}
		
		$info = $this->getInfoInstance();
		$info->setCreateStripeCustomer((bool)$data->getCreateStripeCustomer());		
		if ($data->getStripeCustomerId() != "") {
			$stripeCustomer = Mage::getModel('stripe/payment')->getStripeCustomer($data->getStripeCustomerId());
			$info->setStripeCustomerId($data->getStripeCustomerId());
			if ($data->getStripeToken()) {
				$info->setStripeToken($data->getStripeToken());
			}
		} else {
			$stripeToken = Stripe_Token::retrieve($data->getStripeToken());
			$info->setStripeToken($data->getStripeToken());
		}
		
		return $this;
	}
	

	public function validate()
	{
		$paymentInfo = $this->getInfoInstance();
		$_token = $paymentInfo->getStripeToken();
		$_customerId = $paymentInfo->getStripeCustomerId();
		
		if (!$_token && !$_customerId) {
			Mage::throwException($this->_getHelper()->__("[Stripe] Missing token or customer information."));
		}
		
		if ($paymentInfo->getStripeCustomerId() == ""||$paymentInfo->getStripeToken()!="") {
			$token = $this->_lookupTokenOrCard($_token, $paymentInfo->getStripeCustomerId(), true);
			if (($token instanceof Stripe_Token && $token->used) || !$token) {
				Mage::throwException($this->_getHelper()->__('[Stripe] Token already used or invalid, please re-enter.'));
			}
            $card = ($token instanceof Stripe_Card) ? $token : $token->card;

			if ($card->address_zip_check == "fail") {
				Mage::throwException($this->_getHelper()->__('[Stripe] Address Zip Check Failed'));
			}
            // Fran's does not want address line 1 check.
            // leaving this in case we need to reintroduce it in the future
//			if ($card->address_line1_check == "fail") {
//				Mage::throwException($this->_getHelper()->__('Address Line 1 Failed'));
//			}

			// save cc type for later.
			// edit by Jon Culver. I don't think this belongs here.
			// $paymentInfo->setCcType($token->card->type);
		} else {
			$customer = Stripe_Customer::retrieve($paymentInfo->getStripeCustomerId());
			if (!$customer->default_card) {
				Mage::throwException($this->_getHelper()->__('[Stripe] No Card on File'));
			} else {
                /* @var Stripe_Customer $customer */
                // find it in the list of cards
                $defaultCard = false;
                foreach ($customer->cards->data as $cardData)
                {
                    if ($cardData->id == $customer->default_card)
                    {
                        $defaultCard = $cardData;
                    }
                }

                if (!$defaultCard)
                {
                    Mage::throwException($this->_getHelper()->__('[Stripe] No match for default card'));
                }
				if ($defaultCard->address_zip_check == "fail") {
					Mage::throwException($this->_getHelper()->__('[Stripe] Address Zip Check Failed'));
				}
                // Fran's does not want address line 1 check.
                // leaving this in case we need to reintroduce it in the future
//				if ($defaultCard->address_line1_check == "fail") {
//					Mage::throwException($this->_getHelper()->__('Address Line 1 Failed'));
//				}

				// another edit by Jon Culver. this should be validating not setting data.
				// $paymentInfo->setCcType($customer->active_card->type);
			}
		}
		return $this;
	}

    /**
     * Implemented to allow us to copy payment data to a new order
     *
     * @return array
     */
    public function getCloneData()
    {
        $infoInstance = $this->getInfoInstance();

        if (!($infoInstance instanceof Mage_Sales_Model_Order_Payment))
        {
            Mage::throwException('Can only clone stripe payment from order payment');
        }

        // this will get the original data, and then remove anything
        // (or replace) that isn't needed or won't work in a new order
        // basically, we always set certain stripe fields, and replace the token with a card
        $data = $this->getInfoInstance()->getData();

        // stripe_customer_id stays the same
        // we don't want to create a new one
        $data['create_stripe_customer'] = 0;

        // finally, we need to get our original card, and replace the token with that
        $transaction = $infoInstance
            ->lookupTransaction(false, Mage_Sales_Model_Order_Payment_Transaction::TYPE_CAPTURE);

        if (!$transaction || !$transaction->getId())
        {
            $transaction = $infoInstance->lookupTransaction(false, Mage_Sales_Model_Order_Payment_Transaction::TYPE_AUTH);
        }

        if (!$transaction || !$transaction->getId())
        {
            Mage::throwException('No capture or auth transaction, cannot look up card details');
        }

        // the following code is used to look up the specific card used
        // and put info in the token. We always do this so that we try and charge the same card
        // even if it happens to be their default card (because we can't know if that has changed)
        $chargeId = $transaction->getTxnId();

        // get it from stripe
        $stripeCharge = Stripe_Charge::retrieve($chargeId);

        $data['stripe_token'] = $stripeCharge->card->id;

        return $data;
    }

    /**
     * Send authorize request to gateway
     *
     * @param  Mage_Payment_Model_Info $payment
     * @param  decimal $amount
     * @return IntellectLabs_Stripe_Model_Payment
     */
    public function authorize(Varien_Object $payment, $amount)
    {
        if ($amount <= 0) {
            Mage::throwException(Mage::helper('stripe')->__('Invalid amount for authorization.'));
        }

        // we need to validate the customer's stripe ID is valid before we move on... if it isn't,
        // we set it to null, triggering the system to generate a fresh one when evaluated
        $checkCustomer = $this->getStripeCustomer($payment->getStripeCustomerId());
        if(isset($checkCustomer['error']) || $checkCustomer->deleted){
            $payment->setStripeCustomerId(null);
        }

        $this->validate();

        // took me a bit to figure out that, by the time authorize() actually gets called
        // $payment is now tied to an order, not a quote.
        // see app/code/core/Mage/Sales/Model/Order/Payment.php, _authorize function.
        $order = $payment->getOrder();
        $orderIncrementId = $order->getOrderNumberWithPrefix();
		$billing = $order->getBillingAddress();
        
        // we may not have a customer (guest checkout)
        // but we can spoof one
        if ($order->getCustomerId())
        {
			$customer = Mage::getModel('customer/customer')->load($order->getCustomerId());        	
        }
        else
        {
        	# spoof it enough so that we have the info we need
        	$customer = new Varien_Object();
        	$customer->setEmail($billing->getEmail());
        	$customer->setId(0);
        }

		$paymentInfo = $this->getInfoInstance();
		$token = $paymentInfo->getStripeToken();

        // if we are copying the order, our token can be a card
        // in which case we want to look up the card object and use that for the token
        $tokenOrCard = $this->_lookupTokenOrCard($token, $payment->getStripeCustomerId(), false);

		// note: Fran's is currently on authorize only (capture is on invoice)
		// so this customer creation business happens in the authorize method.
		// we always want to create a customer in stripe, unless we already have one
		// it doesn't matter if it's guest checkout or not
        $stripeCard = false;
		if (!$payment->getStripeCustomerId()) 
		{
			// we might not really have a customer (guest checkout)
			$this->createStripeCustomer($token,$customer,$payment, $orderIncrementId);

            // always set stripe ID for the customer, update the customer and save
            $customer->setStripeCustomerId($payment->getStripeCustomerId());

            //save customer object if not a guest (ID 0)
            if($customer->getId() != 0){
                $customer->save();
            }
		}
        // this annoying if statement tries to capture whether they are already a stripe customer
        // but have a card specified, so that we add the card to them
        // if the token is actually a card (in the case of reorder), we don't want to do anything
        elseif (
            $checkCustomer
            &&
            ($token && !($tokenOrCard instanceof Stripe_Card))
        ) {
			// if we have a token, then we need to create another card
            // it's just a matter of whether it will be the default or not.
            $setAsDefault = $payment->getCreateStripeCustomer();
            $newCardId = '';
            if($checkCustomer->cards){
                $this->updateStripeCustomer($token, $customer, $setAsDefault, $newCardId);
            }

            if (!$setAsDefault)
            {
                $stripeCard = $newCardId;
            }
		}
        elseif ($tokenOrCard instanceof Stripe_Card)
        {
            $stripeCard = $tokenOrCard;
        }
/*        elseif (($token = $payment->getStripeToken())
            && !$payment->getCreateStripeCustomer()
            && ($stripeCustomer = $this->getStripeCustomer($payment->getStripeCustomerId())))
        {
            // another option is that they have a token, but they don't want to create a customer
            // in that case, we have a token, and we should already have a customer ID
            Mage::log("Creating a new card for customer with stripe ID of {$stripeCustomer->id}");
            //$newCard = $stripeCustomer->cards->create(array("card" => $token));
            $stripeCard = $token;
        }
*/
		if ($payment->getStripeToken()!=""&&$payment->getStripeCustomerId()=="") {

			$token = $payment->getStripeToken();
			try {
				$charge = Stripe_Charge::create(array(
						'amount'		=> $amount*100,
						'currency'		=> 'usd',			// todo: would be cleaner to pull this from config.
						'card' 			=> $token,
						'description' 	=> $order->getOrderNumberWithPrefix(),
						'capture'		=> false 			// capture => false is what indicates it's an authorize
				));
			} catch(Stripe_CardError $e) {
				$this->debugData($e->getMessage());
				Mage::throwException($e->getMessage());
			} catch (Exception $e) {
				$this->debugData($e->getMessage());
				Mage::throwException(Mage::helper('stripe')->__('Error capturing payment.'));
			}
			
			$payment->setTransactionId($charge->id)->setIsTransactionClosed(0);
			
		} else {
			
			try
            {
                // charge the Customer instead of the card
                $createParams = array(
                    "amount" => $amount*100,
                    "currency" => "usd",						// todo: same as above; would be cleaner to pull from config.
                    "customer" => $payment->getStripeCustomerId(),
                    "description" => $order->getOrderNumberWithPrefix(),
                    'capture'		=> false 					// capture => false is what indicates it's an authorize
                );
                // in some cases we have a specific card; append that in those cases
                if ($stripeCard)
                {
                    $createParams['card'] = $stripeCard;
                }
			    $charge = Stripe_Charge::create($createParams);
			} catch(Stripe_CardError $e)
            {
				$this->debugData($e->getMessage());
				Mage::throwException($e->getMessage());
			} catch (Exception $e)
            {
				$this->debugData($e->getMessage());
				Mage::throwException($this->_getHelper()->__('Error capturing payment.'));
			}
				
			$payment->setTransactionId($charge->id)->setIsTransactionClosed(0);

            // we can also set the other properties of the payment
            // from the CC info that stripe returns on the charge
            try
            {
                $card = $charge->card;
                $payment->setCcLast4($card->last4);
                $payment->setCcType($card->type);
                $payment->setCcExpMonth($card->exp_month);
                $payment->setCcExpYear($card->exp_year);
            }
            catch (Exception $e)
            {
                Mage::logException($e);
                Mage::log("Unable to set payment properties from Stripe card: {$e->getMessage()}");
                // do nothing, don't want to stop processing.
            }
		}


        return $this;
    }
	
	/**
	 * Capture
	 * 
	 * @param Varien_Object $payment
	 * @param float $amount
	 * 
	 * @return IntellectLabs_Stripe_Model_Payment
	 */
	public function capture(Varien_Object $payment, $amount)
	{
		$order = $payment->getOrder();
		$token = $payment->getStripeToken();

		// let's pick what we charge against
		// either a previous authorization (most likely),
		// failing that, either a stripe token, or a stripe customer/ID.
		if($payment->getAuthorizationTransaction()):
			
			try{
                // get the card with it
                // in case we need it.
				$charge = Stripe_Charge::retrieve(array(
                    "id" => $payment->getAuthorizationTransaction()->getTxnId()
                    // , "expand" => array('card'))
                    ));
                if (!$charge->captured)
                {
                    $charge->capture(array('amount' => $amount*100));
                }
                else
                {
                    Mage::throwException("Charge {$payment->getAuthorizationTransaction()->getTxnId()} for Order {$order->getIncrementId()} aleady captured, skipping.");
                }
			}
            catch(Stripe_Error $e)
            {
                $charge = $this->_handleStripeError($payment, $charge, $e);
            }
            catch (Exception $e) {
                Mage::logException($e);
				$this->debugData($e->getMessage());
				Mage::throwException(Mage::helper('stripe')->__('Error capturing payment: ' . $e->getMessage()));
			}

			// save transaction ID for later (for credit memos and such)
			$payment->setTransactionId($charge->id)->setIsTransactionClosed(0);

		else:

			if(!empty($token)):
				$chargeSubject = array('card' => $token);
			else:
				$chargeSubject = array('customer' => $payment->getStripeCustomerId());
			endif;
			
			try {
				$chargeParams = array_merge(array(
											'amount'		=> $amount*100,
											'currency'		=> strtolower($order->getBaseCurrencyCode()),
											'description'	=> $order->getOrderNumberWithPrefix()
									), $chargeSubject);
				$charge = Stripe_Charge::create($chargeParams);
			} catch(Stripe_CardError $e) {
				$this->debugData($e->getMessage());
				Mage::throwException($e->getMessage());
			} catch (Exception $e) {
				$this->debugData($e->getMessage());
				Mage::throwException(Mage::helper('stripe')->__('Error capturing payment.'));
			}
			
			$payment->setTransactionId($charge->id)->setIsTransactionClosed(0);

		endif;
		
		return $this;
		
	}

	/**
	 * Refund
	 * 
	 * @param Varien_Object $payment
	 * 
	 * @return IntellectLabs_Stripe_Model_Payment
	 */
	public function refund(Varien_Object $payment, $amount)
	{
		$transactionId = $payment->getParentTransactionId();
	
		try {
			Stripe_Charge::retrieve($transactionId)->refund(array('amount'=>$amount*100));
		} catch (Exception $e) {
			$this->debugData($e->getMessage());
			Mage::throwException(Mage::helper('stripe')->__('Error creating refund.'));
		}
		
		$shouldCloseCaptureTransaction = $payment->getOrder()->canCreditmemo() ? 0 : 1;
		
		$payment
		->setTransactionId($transactionId . '-' . Mage_Sales_Model_Order_Payment_Transaction::TYPE_REFUND)
		->setParentTransactionId($transactionId)
		->setIsTransactionClosed(1)
		->setShouldCloseParentTransaction($shouldCloseCaptureTransaction);
	
		return $this;
	}
	
	/**
	 * Set transaction ID into creditmemo for informational purposes
	 * 
	 * @param Mage_Sales_Model_Order_Creditmemo $creditmemo
	 * @param Mage_Sales_Model_Order_Payment $payment
	 * @return Mage_Payment_Model_Method_Abstract
	 */
	public function processCreditmemo($creditmemo, $payment)
	{
		return Mage_Payment_Model_Method_Abstract::processCreditmemo($creditmemo, $payment);
	}
	
	/**
	 * Is Available
	 * 
	 * @param Mage_Sales_Model_Quote
	 * 
	 * @return bool
	 */
	public function isAvailable($quote = null)
	{
		if($quote && $quote->getBaseGrandTotal()<$this->_minOrderTotal) {
			return false;
		}
		 
		return $this->getConfigData('publishable_key', ($quote ? $quote->getStoreId() : null))
		&& parent::isAvailable($quote);
	}
	
	/**
	 * Can Use For Currency
	 * @see Mage_Payment_Model_Method_Abstract::canUseForCurrency()
	 * @param string $currencyCode
	 * @return bool
	 */
	public function canUseForCurrency($currencyCode)
	{
		if (!in_array($currencyCode, $this->_supportedCurrencyCodes)) {
			return false;
		}
		return true;
	}

    /**
     * Void payment method
     *
     * @param Varien_Object $payment
     *
     * @return IntellectLabs_Stripe_Model_Payment
     */
    public function cancel(Varien_Object $payment)
    {
        // lookup the authorization transaction and see if it has been refunded
        // if not, then do it.
        $authTransaction = $payment->getAuthorizationTransaction();

        if ($authTransaction && ($transactionId = $authTransaction->getTxnId()))
        {
            Mage::log("Will check on whether to void authorization with transaction ID of $transactionId");
            $charge = Stripe_Charge::retrieve($transactionId);

            if ($charge)
            {
                if (!$charge->refunded && !$charge->captured)
                {
                    Mage::log("Not captured and not refunded, proceeding with refund.");
                    $charge->refund();
                }
                else if ($charge->refunded)
                {
                    Mage::log("Charge was already refunded, nothing to do.");
                }
                else
                {
                    Mage::throwException("Charge with ID of $transactionId not in proper state to refund (may already be captured).");
                }
            }
            else
            {
                Mage::throwException("Could not find charge for ID $transactionId, cannot void.");
            }
        }
        else
        {
            Mage::throwException("Could not find authorization transaction, cannot void.");
        }

        return $this;
    }

    /**
     * Void payment method
     *
     * @param Varien_Object $payment
     *
     * @return IntellectLabs_Stripe_Model_Payment
     */
    public function void(Varien_Object $payment)
    {
        // from what I can tell, void & cancel seem to be the same in terms of payment
        // so just delegate to cancel
        // there are different buttons in the UI, but they should accomplish the same thing.
        return $this->cancel($payment);
    }

    /**
     * @param Mage_Sales_Model_Order_Payment $payment
     * @param Stripe_Charge $charge
     * @param Stripe_Error $stripeError
     */
    protected function _handleStripeError($payment, $charge, $stripeError)
    {
        // for known stripe error, we will try and go back and get the stripe customer ID
        // and do a new charge for it
        $this->debugData($stripeError->getMessage());

        // for now, retry on all stripe errors
        // eventually we could try and filter by specific error
        // known errors:
        // For expired authorization:
        // Stripe_InvalidRequestError (Message = exception 'Stripe_InvalidRequestError' with message 'Charge ch_103p602bWRxhnVcvlkaLGwlf cannot be captured because the charge has expired (a charge must be captured within 7 days). You will need to create a new charge to retry the payment.)
        try
        {
            // copy everything from the existing charge except the amount as that could have been changed.
            $customer = Stripe_Customer::retrieve($charge->customer);
            $newCharge = Stripe_Charge::create(array(
                'amount'		=> $payment->getOrder()->getBaseTotalDue()*100,
                'currency'		=> $charge->currency,
                'card' 			=> $charge->card,
                'description' 	=> $charge->description,
                'customer'      => $customer,
                'capture'		=> true 			// this is our re-do, we want it right now, so capture it
            ));

            return $newCharge;

        }
        catch (Exception $e)
        {
            Mage::log("Stripe retry failed for order {$payment->getOrder()->getId()}. Message: {$e->getMessage()}", Zend_Log::ERR, 'stripe-retry-errror.log', true);
            throw $e;
        }

    }

    /**
     * Used to hide whether a specific token value is a token or a card
     * If we are copying an order, we will have the card, not the token.
     *
     * @param string $token token value to check
     * @return Stripe_Token|Stripe_Card
     *
     */
    protected function _lookupTokenOrCard($token, $stripeCustomerId, $tokenAsObject = false)
    {
        if ($token && substr($token, 0, 4) === 'card')
        {
            $customer = Stripe_Customer::retrieve($stripeCustomerId);
            return $customer->cards->retrieve($token);
        }

        // sometimes we want the token as an object (to verify properties like if its used)
        // and sometimes we just want it as a string (to create a charge)
        return ($tokenAsObject) ? Stripe_Token::retrieve($token) : $token;
    }
}