<?php

class GoSolid_Brick_Helper_Data extends Mage_Core_Helper_Abstract
{

	public function getConfigValueByPath($path)
	{
		$config = Mage::getConfig();
		$node = $config->getNode($path);
		$value= null;
		if($node)
		{
			$value = $node->asArray();
		}
		return $value;
	}

	public function getIsProduction()
	{
		return $this->getConfigValueByPath('brick/production');
	}


	public function getCanShowBanner()
	{
		$canShowBanner = false;

		if($this->getIsProduction() == false && $this->getConfigValueByPath('brick/banner') !== null)
		{
			$canShowBanner = true;

		}
		return $canShowBanner;
	}

}