<?
class GoSolid_Brick_Model_Observer
{
	const BRICK_COOKIE_NAME = 'Brick';
	const BRICK_COOKIE_PARAM = '?brick=off';

	public function addBrick(Varien_Event_Observer $observer)
	{
		$request    = Mage::app()->getRequest();
		$brickParam = $request->getParam('brick');

		if($brickParam)
		{
			Mage::getModel('core/cookie')->set(self::BRICK_COOKIE_NAME, 1, $brickCookieLifetime);
			$urlWithoutParameters = str_ireplace(self::BRICK_COOKIE_PARAM,'',$request->getRequestUri());
			return Mage::app()->getResponse()->setRedirect($urlWithoutParameters);
		}

		// check if the banner can be shown
		$canShowBanner = Mage::helper('brick')->getCanShowBanner();
		// check to see if there is a cookie turning this off
		$brickCookie = Mage::getModel('core/cookie')->get(self::BRICK_COOKIE_NAME);
		// check if the banner has been shown already for this request
		$hasRun = Mage::registry('has_run');

		// if the banner shouldn't be shown, get out of here
		if(!$canShowBanner || $brickCookie || $hasRun){
			return;
		}

		// assign default config data if it's not already set
		$brickCookieLifetime = Mage::helper('brick')->getConfigValueByPath('brick/banner/cookie_lifetime');
		if(!$brickCookieLifetime)
		{
			$brickCookieLifetime = 60;
		}

		$brickEnvironment = Mage::helper('brick')->getConfigValueByPath('brick/banner/environment');
		if(!$brickEnvironment)
		{
			$brickEnvironment = Mage::helper('brick')->__('unknown');
		}

		$brickMessage = Mage::helper('brick')->getConfigValueByPath('brick/banner/message');
		if(!$brickMessage)
		{
			$brickMessage = Mage::helper('brick')->__('There is a problem with you local.xml %s env vars, please see the admin configuration on how to fix this issue.', $brickEnvironment);
		}

		$brickColor = Mage::helper('brick')->getConfigValueByPath('brick/banner/color');
		if(!$brickColor)
		{
			$brickColor = 'FireBrick';
		}

		$brickTextColor = Mage::helper('brick')->getConfigValueByPath('brick/banner/text_color');
		if(!$brickTextColor)
		{
			$brickTextColor = 'white';
		}


		// assign our needed block data
		$type = $observer->getBlock()->getType();

		if(!$brickCookie)
		{
			if($type == "adminhtml/page" || $type == "page/html" )
			{
				$brickHtml = '<div id="brick" style="background:'.$brickColor.'; width: 100%; padding: 10px 0; color: '.$brickTextColor.'">
                    <span style="text-align: center;margin: 0px auto;display: block;">'.$brickMessage.'</span>
                    <a style="cursor: pointer; color: '.$brickTextColor.';font-weight: bold; position: absolute;padding-right: 20px;top: 10px;right: 0px; padding-right: 20px;" onclick="window.location=window.location+\''.self::BRICK_COOKIE_PARAM.'\'">Hide</a></div>';

				$html = $observer->getTransport()->getHtml();

				$splitPosition  = strpos($html,'<body ') ;

				if($splitPosition){
					$htmlHead = substr($html, 0, $splitPosition);
					$htmlBody = substr($html, $splitPosition);

					$observer->getTransport()->setHtml($htmlHead.$brickHtml.$htmlBody);

					Mage::register('has_run', true);
				}
			}
		}

	}

}
