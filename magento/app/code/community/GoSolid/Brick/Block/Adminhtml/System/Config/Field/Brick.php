<?php
/**
 * Created by PhpStorm.
 * User: Steve
 * Date: 8/8/14
 * Time: 5:20 PM
 */

class GoSolid_Brick_Block_Adminhtml_System_Config_Field_Brick extends Mage_Adminhtml_Block_System_Config_Form_Field
{

    /**
     * Get country selector html
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $elementHtml = "<div>".$this->getConfigValue($element->getId())."</div>";

        return $elementHtml;
    }

    protected function getConfigValue($elementId)
    {
        $config = Mage::getConfig();
        $brickConfig = $config->getNode('brick');
        //$brickConfigValue = $Mage::helper('');
        if($brickConfig)
        {
            $brickConfigArray = array(
                'brick_options_general_production' => '/production',
                'brick_options_banner_cookie_lifetime' => '/banner/cookie_lifetime',
	            'brick_options_banner_color' => '/banner/color',
	            'brick_options_banner_text_color' => '/banner/text_color',
	            'brick_options_banner_message' => '/banner/message'
            );

            $brickConfigValue = Mage::helper('brick')->getConfigValueByPath('brick'.$brickConfigArray[$elementId]);

            if($brickConfigValue != null)
            {
                if($elementId == 'brick_options_general_production')
                {
                    $brickConfigValue = ($brickConfigValue) ? 'true' : 'false';
                }
            }
            else
            {
                $brickConfigValue = $this->__('There is no config value in the local.xml for this field.');
            }

        }

        return $brickConfigValue;
    }

}
