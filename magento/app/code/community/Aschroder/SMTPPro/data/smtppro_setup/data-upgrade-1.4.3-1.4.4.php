<?

$config = new Mage_Core_Model_Config();

//Get current passwords
$googlePassword = Mage::app()->getStore()->getConfig('system/googlesettings/gpassword');
$smtpProPassword = Mage::app()->getStore()->getConfig('system/smtpsettings/password');
$awsSecretKey = Mage::app()->getStore()->getConfig('system/sessettings/aws_private_key');

//set module params
if($googlePassword !== null)
{
    $config->saveConfig('system/googlesettings/gpassword', Mage::helper('core')->encrypt( $googlePassword ), 'default', 0);
}

if($smtpProPassword !== null)
{
    $config->saveConfig('system/smtpsettings/password', Mage::helper('core')->encrypt( $smtpProPassword ), 'default', 0);
}

if($awsSecretKey !== null)
{
    $config->saveConfig('system/sessettings/aws_private_key', Mage::helper('core')->encrypt( $awsSecretKey ), 'default', 0);
}
