<?php

/**
 * Module's main multi-purpose model
 *
 * @category   Ebizmarts
 * @package    Ebizmarts_MageMonkey
 * @author     Ebizmarts Team <info@ebizmarts.com>
 * @license    http://opensource.org/licenses/osl-3.0.php
 */
class Ebizmarts_MageMonkey_Model_Monkey
{
    /**
     * Webhooks request url path
     *
     * @const string
     */

    const WEBHOOKS_PATH = 'monkey/webhook/index/';

    /**
     * Process Webhook request
     *
     * @param array $data
     * @return void
     */
    public function processWebhookData(array $data)
    {
        $listId = $data['data']['list_id']; //According to the docs, the events are always related to a list_id
//        $store = Mage::helper('monkey')->getStoreByList($listId);
        if($data['type'] == 'upemail')
        {
            $subscriber = Mage::getModel('newsletter/subscriber')
                ->loadByEmail($data['data']['old_email']);
        }
        else
        {
            $subscriber = Mage::getModel('newsletter/subscriber')
                ->loadByEmail($data['data']['email']);
        }
        $storeId = $subscriber->getStoreId();
        $store = Mage::getModel('core/store')->load($storeId);
        if (!is_null($store)) {
            $curstore = Mage::app()->getStore();
            Mage::app()->setCurrentStore($store);
        }

        //Object for cache clean
        $object = new stdClass();
        $object->requestParams = array();
        $object->requestParams['id'] = $listId;

        if(!isset($data['data']['email']))
        {
            $object->requestParams['email_address'] = $data['data']['old_email'];
        }
        else
        {
            $object->requestParams['email_address'] = $data['data']['email'];
        }
        $cacheHelper = Mage::helper('monkey/cache');

        switch ($data['type']) {
            case 'subscribe':
                $this->_subscribe($data);
                $cacheHelper->clearCache('listSubscribe', $object);
                break;
            case 'unsubscribe':
                $this->_unsubscribe($data);
                $cacheHelper->clearCache('listUnsubscribe', $object);
                break;
            case 'cleaned':
                $this->_clean($data);
                $cacheHelper->clearCache('listUnsubscribe', $object);
                break;
            case 'campaign':
                $this->_campaign($data);
                break;
            //case 'profile': Cuando se actualiza email en MC como merchant, te manda un upmail y un profile (no siempre en el mismo órden)
            case 'upemail':
                $this->_updateEmail($data);
                $cacheHelper->clearCache('listUpdateMember', $object);
                break;
            case 'profile':
                // $this->_profile($data);
                // $cacheHelper->clearCache('listUpdateMember', $object);
                break;
        }

        if (!is_null($store)) {
            Mage::app()->setCurrentStore($curstore);
        }
    }

    /**
     * Update customer email <upemail>
     *
     * @param array $data
     * @return void
     */
    protected function _updateEmail(array $data)
    {
        $old = $data['data']['old_email'];
        $new = $data['data']['new_email'];

        // Update customer first (if exists)
        // This takes care of the newsletter email, too
        $customer = $this->loadCustomerByEmail($old);

        if($customer->getId())
        {
            $customer = Mage::getModel('customer/customer')->load($customer->getId());
            $customer->setEmail($new);
            $customer->save();
        }
        // Update only the subscription
        else
        {
            $oldSubscriber = $this->loadByEmail($old);
            $newSubscriber = $this->loadByEmail($new);

            if (!$newSubscriber->getId() && $oldSubscriber->getId()) {
                $oldSubscriber->setSubscriberEmail($new)
                    ->save();
            } elseif (!$newSubscriber->getId() && !$oldSubscriber->getId()) {
                Mage::getModel('newsletter/subscriber')
                    ->setImportMode(TRUE)
                    ->setStoreId(Mage::app()->getStore()->getId())
                    ->subscribe($new);
            }
        }
    }

    /**
     * Add "Cleaned Emails" notification to Adminnotification Inbox <cleaned>
     *
     * @param array $data
     * @return void
     */
    protected function _clean(array $data)
    {

        if (Mage::helper('monkey')->isAdminNotificationEnabled()) {
            $text = Mage::helper('monkey')->__('MailChimp Cleaned Emails: %s %s at %s reason: %s', $data['data']['email'], $data['type'], $data['fired_at'], $data['data']['reason']);

            $this->_getInbox()
                ->setTitle($text)
                ->setDescription($text)
                ->save();
        }

        //Delete subscriber from Magento
        $s = $this->loadByEmail($data['data']['email']);

        if ($s->getId()) {
            try {
                $s->delete();
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }
    }

    /**
     * Add "Campaign Sending Status" notification to Adminnotification Inbox <campaign>
     *
     * @param array $data
     * @return void
     */
    protected function _campaign(array $data)
    {

        if (Mage::helper('monkey')->isAdminNotificationEnabled()) {
            $text = Mage::helper('monkey')->__('MailChimp Campaign Send: %s %s at %s', $data['data']['subject'], $data['data']['status'], $data['fired_at']);

            $this->_getInbox()
                ->setTitle($text)
                ->setDescription($text)
                ->save();
        }

    }

    /**
     * Subscribe email to Magento list, store aware
     *
     * @param array $data
     * @return void
     */
    protected function _subscribe(array $data)
    {
        try {

            //TODO: El método subscribe de Subscriber (Magento) hace un load by email
            // entonces si existe en un store, lo acutaliza y lo cambia de store, no lo agrega a otra store
            //VALIDAR si es lo que se requiere

            $subscriber = Mage::getModel('newsletter/subscriber')
                ->loadByEmail($data['data']['email']);
            if ($subscriber->getId()) {
                $subscriber->setStatus(Mage_Newsletter_Model_Subscriber::STATUS_SUBSCRIBED)
                    ->save();
            } else {
                $subscriber = Mage::getModel('newsletter/subscriber')->setImportMode(TRUE);
                if(isset($data['data']['fname'])){
                    $subscriber->setSubscriberFirstname($data['data']['fname']);
                }
                if(isset($data['data']['lname'])){
                    $subscriber->setSubscriberLastname($data['data']['lname']);
                }

                // // Set the origin
                // $subscriber->subscribe($data['data']['email'], $data['data']['merges']['ORIGIN']);

            }
            $customer = Mage::getSingleton('customer/customer')
                ->getCollection()
                ->addAttributeToFilter('email', array('eq' => $data['data']['email']) )
                ->getFirstItem();

            if($customer){
                $customerId = $customer->getId();
                $storeId = $customer->getStoreId();
                
                $subscriber->setCustomerId($customerId);
                $subscriber->setStoreId($storeId);
                $subscriber->save();

                $customer = Mage::getModel('customer/customer')->load($customerId);
                $customer->subscribeEmail();
                // $customer->setIsSubscribed(1);
                // $customer->setSubscriptionOrigin($data['data']['merges']['ORIGIN']);
                $customer->save();

                // Update subscriber on mailchimp
                $api = Mage::getSingleton('monkey/api', array('store' => $customer->getStoreId()));
                $defaultList = Mage::getStoreConfig(Ebizmarts_MageMonkey_Model_Config::GENERAL_LIST, $customer->getStoreId());
                $mergeVars = Mage::helper('monkey')->mergeVars($customer, TRUE, $defaultList);
                $api->listUpdateMember($defaultList, $data['data']['email'], $mergeVars, '', false);

                // // Update data on MailChimp
                // $defaultList = Mage::getStoreConfig(Ebizmarts_MageMonkey_Model_Config::GENERAL_LIST, $customer->getStoreId());

                // Mage::getSingleton('monkey/api', array('store' => $customer->getStoreId()))->listUpdateMember($defaultList, $data['data']['email']);
            }
            // if($customer && Mage::getStoreConfig('sweetmonkey/general/active', $storeId)){
            //     Mage::helper('sweetmonkey')->pushVars($customer);
            // }
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

    /**
     * Unsubscribe or delete email from Magento list, store aware
     *
     * @param array $data
     * @return void
     */
    protected function _unsubscribe(array $data)
    {
        $subscriber = $this->loadByEmail($data['data']['email']);

        if (!$subscriber->getId()) {
            $subscriber = Mage::getModel('newsletter/subscriber')
                ->loadByEmail($data['data']['email']);
        }

        if ($subscriber->getId()) {
            try {
                // if(!Mage::getStoreConfig(Ebizmarts_MageMonkey_Model_Config::GENERAL_CONFIRMATION_EMAIL, $subscriber->getStoreId())){
                //     $subscriber->setImportMode(true);
                // }

                switch ($data['data']['action']) {
                    case 'delete' :
                        //if config setting "Webhooks Delete action" is set as "Delete customer account"
                        if (Mage::getStoreConfig("monkey/general/webhook_delete") == 1) {
                            $subscriber->delete();
                        } else {
                            $subscriber->unsubscribe();
                        }

                        $this->_removeSubscriber($subscriber);

                        break;
                    case 'unsub':
                        $customer = Mage::getModel('customer/customer')->load($subscriber->getCustomerId());

                        if($customer->getId() != null)
                        {
                            $customer->subscribeEmail(false);
                            $customer->save();
                        }

                        $subscriber->setStatus(Mage_Newsletter_Model_Subscriber::STATUS_UNSUBSCRIBED);
                        $subscriber->save();

                        break;
                }
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }
    }

    private function _removeSubscriber($subscriber)
    {
        $customer = Mage::getModel('customer/customer')->load($subscriber->getCustomerId());

        if($customer->getId() != null)
        {
            $customer->subscribeEmail();
            $customer->save();
        }
    }

    protected function _profile(array $data)
    {
        $email = $data['data']['email'];
        $subscriber = $this->loadByEmail($email);

        $customerCollection = Mage::getModel('customer/customer')->getCollection()
            ->addFieldToFilter('email', array('eq' => $email));

        if (count($customerCollection) > 0)
        {
            $toUpdate = $customerCollection->getFirstItem();
            $customerId = $toUpdate->getId();
            $customer = Mage::getModel('customer/customer')->load($customerId);
            $toUpdate = $customer;

            // Billing address (the only one synced)
            $billingAddress = $customer->getPrimaryBillingAddress();
            if($billingAddress != null)
            {
                $newAddress = $data['data']['merges']['BILLING'];

                $billingAddress->setFirstname($data['data']['merges']['FNAME']);
                $billingAddress->setLastname($data['data']['merges']['LNAME']);
                $billingAddress->setStreet(array($newAddress['addr1'], $newAddress['addr2']));
                $billingAddress->setCity($newAddress['city']);
                $billingAddress->setCountryId($newAddress['country']);
                $billingAddress->setRegion($newAddress['state']);
                $billingAddress->setPostcode($newAddress['zip']);
                // Telephone
                $billingAddress->setTelephone($data['data']['merges']['TELEPHONE']);
                // Company
                $billingAddress->setCompany($data['data']['merges']['COMPANY']);

                $billingAddress->save();
            }

            // Set DOB at the customer level
            $toUpdate->setDob($data['data']['merges']['DOB']);
        }
        elseif($subscriber->getId())
        {
            $toUpdate = $subscriber;
        }
        else
        {
            return;
        }

        // Set the origin
        $toUpdate->setSubscriptionOrigin($data['data']['merges']['ORIGIN']);


        // $toUpdate->setFirstname($data['data']['merges']['FNAME']);
        // $toUpdate->setLastname($data['data']['merges']['LNAME']);

        $toUpdate->save();
    }

    /**
     * Return Inbox model instance
     *
     * @return Mage_AdminNotification_Model_Inbox
     */
    protected function _getInbox()
    {
        return Mage::getModel('adminnotification/inbox')
            ->setSeverity(4)//Notice
            ->setDateAdded(Mage::getModel('core/date')->gmtDate());
    }

    /**
     * Load newsletter_subscriber by email
     *
     * @param string $email
     * @return Mage_Newsletter_Model_Subscriber
     */
    public function loadByEmail($email)
    {
        return Mage::getModel('newsletter/subscriber')
            ->getCollection()
            ->addFieldToFilter('subscriber_email', $email)
            // ->addFieldToFilter('store_id', Mage::app()->getStore()->getId())
            ->getFirstItem();
    }

    /**
     * Load customer by email
     *
     * @param string $email
     * @return Customer or false
     */
    public function loadCustomerByEmail($email)
    {
        $customerCollection = Mage::getModel('customer/customer')
            ->getCollection()
            ->addFieldToFilter('email', array('eq' => $email));

        if(count($customerCollection) > 0)
        {
            return $customerCollection->getFirstItem();
        }

        return false;
    }
}
