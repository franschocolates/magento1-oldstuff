<?php

$installer = $this;

$tableName = $installer->getTable('core_config_data');

$sql = "
INSERT INTO {$tableName} (`scope`, `scope_id`, `path`, `value`)
VALUES
    ('default', 0, 'monkey/general/active', '1'),
    ('default', 0, 'monkey/general/maxlistsamount', '25'),
    ('default', 0, 'monkey/general/changecustomergroup', '0'),
    ('default', 0, 'monkey/general/showreallistname', '0'),
    ('default', 0, 'monkey/general/additional_lists', NULL),
    ('default', 0, 'monkey/general/confirmation_email', '0'),
    ('default', 0, 'monkey/general/map_fields', 'a:7:{i:0;a:2:{s:7:\"magento\";s:9:\"firstname\";s:9:\"mailchimp\";s:5:\"FNAME\";}i:1;a:2:{s:7:\"magento\";s:8:\"lastname\";s:9:\"mailchimp\";s:5:\"LNAME\";}i:2;a:2:{s:7:\"magento\";s:3:\"dob\";s:9:\"mailchimp\";s:3:\"DOB\";}i:4;a:2:{s:7:\"magento\";s:15:\"billing_address\";s:9:\"mailchimp\";s:7:\"BILLING\";}i:12;a:2:{s:7:\"magento\";s:9:\"telephone\";s:9:\"mailchimp\";s:9:\"TELEPHONE\";}i:13;a:2:{s:7:\"magento\";s:7:\"company\";s:9:\"mailchimp\";s:7:\"COMPANY\";}s:18:\"_1470252040273_273\";a:2:{s:7:\"magento\";s:19:\"subscription_origin\";s:9:\"mailchimp\";s:6:\"ORIGIN\";}}'),
    ('default', 0, 'monkey/general/guest_name', 'FNAME'),
    ('default', 0, 'monkey/general/guest_lastname', 'LNAME'),
    ('default', 0, 'monkey/general/checkout_subscribe', '0'),
    ('default', 0, 'monkey/general/markfield', 'OPTEDIN'),
    ('default', 0, 'monkey/general/cron_import', '200'),
    ('default', 0, 'monkey/general/cron_export', '200'),
    ('default', 0, 'monkey/general/enable_webhooks', '1'),
    ('default', 0, 'monkey/general/adminhtml_notification', '0'),
    ('default', 0, 'monkey/general/enable_log', '0'),
    ('default', 0, 'monkey/ecommerce360/active', '0'),
    ('default', 0, 'monkey/ecommerce360/attributes', NULL),
    ('default', 0, 'newsletter/subscription/success_email_template', 'newsletter_subscription_success_email_template'),
    ('default', 0, 'newsletter/subscription/un_email_identity', 'support'),
    ('default', 0, 'newsletter/subscription/un_email_template', 'newsletter_subscription_un_email_template'),
    ('default', 0, 'newsletter/subscription/success_email_identity', 'general'),
    ('default', 0, 'newsletter/subscription/confirm_email_template', 'newsletter_subscription_confirm_email_template'),
    ('default', 0, 'newsletter/subscription/confirm', '0'),
    ('default', 0, 'newsletter/subscription/confirm_email_identity', 'support'),
    ('default', 0, 'newsletter/subscription/allow_guest_subscribe', '1'),
    ('default', 0, 'monkey/general/webhook_delete', '0');
";

$installer->run($sql);

$installer->endSetup();
