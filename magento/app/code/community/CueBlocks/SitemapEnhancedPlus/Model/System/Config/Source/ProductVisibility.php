<?php
/**
 * Created by PhpStorm.
 * User: cueblocks
 * Date: 3/7/14
 * Time: 11:37 AM
 */

class CueBlocks_SitemapEnhancedPlus_Model_System_Config_Source_ProductVisibility {

    public function toOptionArray() {

       return array(
            array('label' => Mage::helper('catalog')->__('Catalog'),                 'value'=>Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG),
            array('label' => Mage::helper('catalog')->__('Search'),                  'value'=>Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_SEARCH),
            array('label' => Mage::helper('catalog')->__('Catalog, Search'),         'value' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)
        );
    }

}