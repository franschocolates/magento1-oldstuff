<?php

/**
 * Description of SitemapEnhancedPlus
 * @package   CueBlocks_SitemapEnhancedPlus
 * @company    CueBlocks - http://www.cueblocks.com/
 
 */
class CueBlocks_SitemapEnhancedPlus_Model_Mysql4_Processor_Catalog_Product extends Mage_Sitemap_Model_Mysql4_Catalog_Product
{
    private $_store;
    private $_alias;
    protected $_attributesCache = array();

    function _setAlias()
    {
        $alias = 'e';

        if (Mage::helper('sitemapEnhancedPlus')->isMageAbove18()) {
            $alias = 'main_table';
        }
        $this->_alias = $alias;

        return $this->_alias;
    }

    /**
     * Get category collection array
     *
     * @param unknown_type $storeId
     * @return Zend_Db_Statement_Interface
     */
    public function getCollection($storeId, $filterOutOfStock = false, $filterInStock = false, $useRootCatalogMode = false, $filterByCatId = false, $useCatPathInUrl = false, $groupByAttributeCode = false)
    {
        $store = Mage::app()->getStore($storeId);
        /* @var $store Mage_Core_Model_Store */
        if (!$store) {
            return false;
        }
        // set env. values
        $this->_store = $store;
        $this->_setAlias();

        // base query
        $this->_select = $this->_getSelect();
        // url join (works only < 1.13.1)
        $this->_addUrlJoin($filterByCatId, $useCatPathInUrl);
        // filter by stock
        $this->_addStockFilterJoin($filterOutOfStock, $filterInStock);
        //filter products catalog by category root
        if ($useRootCatalogMode) {
            $this->_addRootFilterJoin();
        }

        //filter products by category
        if ($filterByCatId) {
            $this->_addCategoryFilterJoin($filterByCatId);
        }
        if ($groupByAttributeCode) {
            $this->_addAttributeFilterJoin($groupByAttributeCode);
        }


        $this->_addFilter($storeId, 'visibility', explode(',',Mage::getStoreConfig('sitemap_enhanced_plus/product/visibility')), 'in');
        $this->_addFilter($storeId, 'status', Mage::getSingleton('catalog/product_status')->getVisibleStatusIds(), 'in');

// die((string)($this->_select));

        $query = $this->_getWriteAdapter()->query($this->_select);

        return $query;
    }

    protected function _getSelect()
    {
        $select = $this->_getWriteAdapter()->select()
            ->from(array($this->_alias => $this->getMainTable()), array($this->getIdFieldName(), 'DATE(updated_at) as updated_at'))
            ->join(
                array('w' => $this->getTable('catalog/product_website')),
                $this->_alias . '.entity_id=w.product_id',
                array()
            )
            ->where('w.website_id=?', $this->_store->getWebsiteId());
//            $this->_select->where('ur.request_path IS NOT NULL');

        return $select;
    }

    /**
     * TODO: Fix forEE 1.13.1
     * `core/url_rewrite` is not present
     */
    protected function _addUrlJoin($filterByCatId, $useCatPathInUrl)
    {
        $storeId = (int)$this->_store->getId();

        if (Mage::helper('sitemapEnhancedPlus')->isMageAboveEE12()) {
            // For EE > 1.12
            $this->_joinTableToSelect($this->_select, $storeId);
        } else {
            // For CE & EE < 1.12
            $conditions = array(
                $this->_alias . '.entity_id=ur.product_id',
                'ur.category_id IS NULL',
                $this->_getWriteAdapter()->quoteInto('ur.store_id=?', $storeId),
                $this->_getWriteAdapter()->quoteInto('ur.is_system=?', 1),
            );

            if ($filterByCatId) {
                // use category path only is enabled
                $seo_cat_path = Mage::getStoreConfig('catalog/seo/product_use_categories', $storeId);

                if ($seo_cat_path && $useCatPathInUrl) {
                    $conditions = array(
                        $this->_alias . '.entity_id=ur.product_id',
                        $this->_getWriteAdapter()->quoteInto('ur.category_id=?', $filterByCatId),
                        $this->_getWriteAdapter()->quoteInto('ur.store_id=?', $storeId),
                        $this->_getWriteAdapter()->quoteInto('ur.is_system=?', 1),
                    );
                }
            }

            $this->_select = $this->_select->joinLeft(
                array('ur' => $this->getTable('core/url_rewrite')),
                join(' AND ', $conditions),
                array('url' => 'request_path')
            );
        }
        return $this;
    }

    /**
     * Necessary for EE > 1.12
     *
     * @param Varien_Db_Select $select
     * @param $storeId
     * @return $this
     */
    protected function _joinTableToSelect(Varien_Db_Select $select, $storeId)
    {
        $requestPath = $this->_getWriteAdapter()->getIfNullSql('url_rewrite.request_path', 'default_ur.request_path');
        $urlSuffix = Mage::helper('catalog/product')->getProductUrlSuffix($storeId);
        $urlSuffix = ($urlSuffix) ? '.' . $urlSuffix : '';

        $select->joinLeft(
            array('url_rewrite_product' => $this->getTable('enterprise_catalog/product')),
            'url_rewrite_product.product_id = main_table.entity_id ' .
            'AND url_rewrite_product.store_id = ' . (int)$storeId,
            array('url_rewrite_product.product_id' => 'url_rewrite_product.product_id'))
            ->joinLeft(array('url_rewrite' => $this->getTable('enterprise_urlrewrite/url_rewrite')),
                'url_rewrite_product.url_rewrite_id = url_rewrite.url_rewrite_id AND url_rewrite.is_system = 1',
                array(''))
            ->joinLeft(array('default_urp' => $this->getTable('enterprise_catalog/product')),
                'default_urp.product_id = main_table.entity_id AND default_urp.store_id = 0',
                array(''))
            ->joinLeft(array('default_ur' => $this->getTable('enterprise_urlrewrite/url_rewrite')),
                'default_ur.url_rewrite_id = default_urp.url_rewrite_id',
                array('url' => 'concat( ' . $requestPath . ',"' . $urlSuffix . '")'));
        return $this;
    }

    protected function _addStockFilterJoin($filterOutOfStock, $filterInStock)
    {
        $storeId = (int)$this->_store->getId();
        $conditions = null;

        // filter in/out of stock
        $manageStockConfig = Mage::getStoreConfig('cataloginventory/item_options/manage_stock', $storeId);

        if ($filterOutOfStock) {
            $sql = $this->_alias . '.entity_id=stk.product_id ';

            if ($manageStockConfig) {
                $sql .= ' AND IF (stk.use_config_manage_stock = 1, stk.is_in_stock = 1';
            } else {
                $sql .= ' AND IF (stk.use_config_manage_stock = 1, TRUE';
            }
            $sql .= ' ,(stk.manage_stock = 0 OR (stk.manage_stock = 1 AND stk.is_in_stock = 1)) )';
            $conditions = $sql;

        } elseif ($filterInStock) {

            $sql = $this->_alias . '.entity_id=stk.product_id ';

            if ($manageStockConfig) {
                $sql .= ' AND IF (stk.use_config_manage_stock = 1, stk.is_in_stock = 0';
            } else {
                $sql .= ' AND IF (stk.use_config_manage_stock = 1, FALSE';
            }
            $sql .= ' ,stk.manage_stock = 1 AND stk.is_in_stock = 0)';
            $conditions = $sql;
        }

        if ($conditions) {
            $this->_select = $this->_select->join(
                array('stk' => $this->getTable('cataloginventory/stock_item')),
                $conditions,
                array('is_in_stock', 'manage_stock', 'use_config_manage_stock'));
        }

        return $this;
    }

    protected function _addCategoryFilterJoin($filterByCatId)
    {
        $storeId = (int)$this->_store->getId();

        $conditions = array(
            $this->_alias . '.entity_id=cat_index.product_id',
            $this->_getWriteAdapter()->quoteInto('cat_index.store_id=?', $storeId),
            $this->_getWriteAdapter()->quoteInto('cat_index.category_id=?', $filterByCatId),
            $this->_getWriteAdapter()->quoteInto('cat_index.is_parent=?', 1),
        );
        $this->_select = $this->_select->join(
            array('cat_index' => $this->getTable('catalog/category_product_index')),
            join(' AND ', $conditions),
            array()
        );
//                ->distinct(true);

        return $this;
    }

    protected function _addRootFilterJoin()
    {
        $storeId = (int)$this->_store->getId();

//        if (Mage::helper('sitemapEnhancedPlus')->isMageAboveEE12()) {
        // For EE > 1.12
        /**
         * using 'catalog_category_product_index'
         * checking for a row with 'is_parent = 1' and current store_id
         * similar way as for category_id filter
         */

        // filter products that are not assigned to any sub category of the root category
        $conditions = array(
            $this->_alias . '.entity_id=rootfilter.product_id',
            $this->_getWriteAdapter()->quoteInto('  rootfilter.store_id=?', $storeId),
            $this->_getWriteAdapter()->quoteInto('rootfilter.is_parent=?', 1),
        );
        $this->_select = $this->_select->join(
            array('rootfilter' => $this->getTable('catalog/category_product_index')),
            join(' AND ', $conditions)
        );

        /*
         * check if a product is not assigned to any category
         * without this filter the product is shown in all store
         * ( because 'is_parent' is set to 1 )
         */
        $this->_select = $this->_select->join(
            array('emptyFilter' => $this->getTable('catalog/category_product')),
            $this->_alias . '.entity_id=emptyFilter.product_id'
        );
        $this->_select->group($this->_alias . '.entity_id');

//        } else {
//            // For CE & EE < 1.12
//
//            $rootId = $this->_store->getRootCategoryId();
//            $_category = Mage::getModel('catalog/category')->load($rootId); //get category model
//
//            $child_categories = $_category->getAllChildren(true); //array consisting of all child categories id
//            // remove root
//            array_shift($child_categories); //array consisting of all child categories id
//
//            // filter products that are not assigned to any sub category of the root category
//            $conditions = array(
//                $this->_alias . '.entity_id=rootfilter.product_id',
//                $this->_getWriteAdapter()->quoteInto('rootfilter.store_id=?', $storeId),
//                $this->_getWriteAdapter()->quoteInto('rootfilter.is_system=?', 1),
//                $this->_getWriteAdapter()->quoteInto('rootfilter.category_id in (?)', $child_categories)
//            );
//
//            $this->_select = $this->_select->join(
//                array('rootfilter' => $this->getTable('core/url_rewrite')),
//                join(' AND ', $conditions),
//                array('url1' => 'request_path',)
//            );
//            $this->_select->group($this->_alias . '.entity_id');
//        }

        return $this;
    }

    protected function _addAttributeFilterJoin($attributeCode)
    {
        $storeId = (int)$this->_store->getId();
        $this->_addAttribute($storeId, $attributeCode, true);
        return $this;
    }

    protected function _addAttribute($storeId, $attributeCode, $orderBy = false)
    {
        if (!isset($this->_attributesCache[$attributeCode])) {
            $this->_loadAttribute($attributeCode);
        }

        $attribute = $this->_attributesCache[$attributeCode];

        if (!$this->_select instanceof Zend_Db_Select) {
            return false;
        }

        if ($attribute['backend_type'] == 'static') {
            $this->_select->columns($this->_alias . '.' . $attributeCode);
        } else {
            $this->_select->joinLeft(
                array('t1_' . $attributeCode => $attribute['table']),
                $this->_alias. '.entity_id=t1_' . $attributeCode . '.entity_id AND t1_' . $attributeCode . '.store_id=0' .
                ' AND t1_' . $attributeCode . '.attribute_id=' . $attribute['attribute_id'],
                array()
            );

            if ($attribute['is_global']) {
                $this->_select->columns(array($attributeCode => 't1_' . $attributeCode . '.value'));
            } else {
                $ifCase = $this->_select->getAdapter()->getCheckSql('t2_' . $attributeCode . '.value_id > 0',
                    't2_' . $attributeCode . '.value', 't1_' . $attributeCode . '.value'
                );
                $this->_select->joinLeft(
                    array('t2_' . $attributeCode => $attribute['table']),
                    $this->_getWriteAdapter()->quoteInto(
                        't1_' . $attributeCode . '.entity_id = t2_' . $attributeCode . '.entity_id AND t1_'
                        . $attributeCode . '.attribute_id = t2_' . $attributeCode . '.attribute_id AND t2_'
                        . $attributeCode . '.store_id = ?', $storeId
                    ),
                    array($attributeCode => $ifCase)
                );
            }
        }
        $this->_select->order($attributeCode);
        return $this->_select;
    }

    /**
     * It looks that this
     */
    protected function _getAllStoreProd($storeId)
    {

        $store = Mage::app()->getStore($storeId);
        /* @var $store Mage_Core_Model_Store */

        if (!$store) {
            return false;
        }

        $storeId = (int)$store->getId();
        $this->_setAlias();

        $this->_select = $this->_getWriteAdapter()->select()
            ->from(array($this->_alias => $this->getMainTable()), array($this->getIdFieldName(), 'updated_at'))
            ->join(
                array('w' => $this->getTable('catalog/product_website')),
                $this->_alias . '.entity_id=w.product_id',
                array()
            )
            ->where('w.website_id=?', $store->getWebsiteId());

        $this->_addFilter($storeId, 'visibility', explode(',',Mage::getStoreConfig('sitemap_enhanced_plus/product/visibility')), 'in');
        $this->_addFilter($storeId, 'status', Mage::getSingleton('catalog/product_status')->getVisibleStatusIds(), 'in');

//        die((string)($this->_select));

        $query = $this->_getWriteAdapter()->query($this->_select);

        return $query;
    }

    /**
     * Look for product not associated with any category or to any configurable product
     * @param int $storeId
     * @return array
     */
    public function getOrphan($storeId = 0)
    {
        $store = Mage::app()->getStore($storeId);
        /* @var $store Mage_Core_Model_Store */

        if (!$store) {
            return false;
        }

        $storeId = (int)$store->getId();

        $this->_setAlias();
        $this->_select = $this->_getWriteAdapter()->select()
            ->from(
                array($this->_alias => $this->getMainTable()),
                array($this->getIdFieldName(), 'sku'))
            ->joinLeft(
                array('w' => $this->getTable('catalog/product_website')),
                $this->_alias . '.entity_id=w.product_id',
                array('w.website_id')
            )
            ->where('w.website_id=?', $store->getWebsiteId());

//        $urlConditions = array(
//            $this->_alias . '.entity_id=ur.product_id',
//            'ur.category_id IS NULL',
//            $this->_getWriteAdapter()->quoteInto('ur.store_id=?', $storeId),
//            $this->_getWriteAdapter()->quoteInto('ur.is_system=?', 1),
//        );

//        $this->_select = $this->_select->joinLeft(
//            array('ur' => $this->getTable('core/url_rewrite')),
//            join(' AND ', $urlConditions),
//            array('url' => 'request_path')
//        );

        $this->_select = $this->_select
            ->joinLeft(
                array('cat' => $this->getTable('catalog/category_product')),
                $this->_alias . '.entity_id=cat.product_id',
                array())
            ->where('cat.product_id is null');

        $this->_select = $this->_select
            ->joinLeft(
                array('rel' => $this->getTable('catalog/product_relation')),
                $this->_alias . '.entity_id=rel.child_id',
                array('rel.parent_id'))
            ->where('rel.parent_id is null');

        $this->_addFilter($storeId, 'visibility', explode(',',Mage::getStoreConfig('sitemap_enhanced_plus/product/visibility')), 'in');
        $this->_addFilter($storeId, 'status', Mage::getSingleton('catalog/product_status')->getVisibleStatusIds(), 'in');

//        die((string)($this->_select));

        $query = $this->_getWriteAdapter()->query($this->_select);

        return $query;
    }

    /** Look for orphan product that are not in the sitemap
     * @param int $storeId
     * @return array
     */
    public function getExcludedOrphan($storeId = 0)
    {
//        $all_ids = array();
        $sitemap_ids = array();
        $orphan_ids = array();

        $result = $this->getOrphan($storeId);
        while ($id = $result->fetchColumn(0)) {
            $orphan_ids[] = $id;
        }
        unset($result);

        /* @var $result Zend_Db_Statement_Interface */
        $result = $this->getCollection($storeId);
        while ($id = $result->fetchColumn(0)) {
            $sitemap_ids[] = $id;
        }
        unset($result);
        $excluded = array_diff($orphan_ids, $sitemap_ids);
        return $excluded;
    }

    /**
     * Look for any product that are not in the sitemap
     * @param int $storeId
     * @return array
     */
    public function getAllExcluded($storeId = 0)
    {
        $all_ids = array();
        $sitemap_ids = array();

        /* @var $result Zend_Db_Statement_Interface */
        $result = $this->getCollection($storeId);
        while ($id = $result->fetchColumn(0)) {
            $sitemap_ids[] = $id;
        }
        unset($result);

        $result = $this->_getAllStoreProd($storeId);
        while ($id = $result->fetchColumn()) {
            $all_ids[] = $id;
        }
        unset($result);

        $excluded = array_diff($all_ids, $sitemap_ids);
        return $excluded;
    }

    /**
     * for compatibility with < 1.8
     */
    protected function _loadAttribute($attributeCode)
    {
        $attribute = Mage::getSingleton('catalog/product')->getResource()->getAttribute($attributeCode);

        $this->_attributesCache[$attributeCode] = array(
            'entity_type_id' => $attribute->getEntityTypeId(),
            'attribute_id' => $attribute->getId(),
            'table' => $attribute->getBackend()->getTable(),
            'is_global' => $attribute->getIsGlobal() == Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'backend_type' => $attribute->getBackendType()
        );
        return $this;
    }
}
