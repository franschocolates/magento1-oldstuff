<?php

/**
 * Description of SitemapEnhancedPlus
 * @package   CueBlocks_SitemapEnhancedPlus
 * @company   CueBlocks - http://www.cueblocks.com/
 
 */
class CueBlocks_SitemapEnhancedPlus_Model_Mysql4_Processor_Catalog_Tag extends Mage_Core_Model_Mysql4_Abstract
{

    /**
     * Collection Zend Db select
     *
     * @var Zend_Db_Select
     */
    protected $_select;

    /**
     * Init resource model (catalog/category)
     *
     */
    protected function _construct()
    {
        $this->_init('tag/summary', 'tag_id');
    }

    /**
     * Get Tag for storeView
     *
     * @param unknown_type $storeId
     * @return Zend_Db_Statement_Interface
     */
    public function getCollection($storeId)
    {

        if (trim($storeId) == '') {
            return false;
        }

        $relConditions = array(
            $this->_getWriteAdapter()->quoteInto('r.active=?', 1),
            'r.tag_id=t.tag_id'
        );

        $this->_select = $this->_getWriteAdapter()->select()
                ->from(array('t' => $this->getMainTable()), array('t.tag_id'))
                ->where('t.store_id =?', $storeId)
                ->join(
                array('r' => $this->getTable('tag/relation')), join(' AND ', $relConditions), array());


        $query = $this->_getWriteAdapter()->query($this->_select);

//        die((string) ($this->_select));

        return $query;
    }

}