<?php

/** @TODO: NEED TO BE COMPLETED
 * Description of SitemapEnhancedPlus
 * @package   CueBlocks_SitemapEnhancedPlus
 * @company   CueBlocks - http://www.cueblocks.com/
 
 */
class CueBlocks_SitemapEnhancedPlus_Model_Processor_Catalog_Category_Image extends CueBlocks_SitemapEnhancedPlus_Model_Processor_Catalog_Category_Abstract
{

    protected $_sourceModel = 'sitemapEnhancedPlus/processor_catalog_category_image';
    protected $_counterLabel = 'image';

    protected function _getProcessCollection($addFirst = false)
    {
        $imgXml = '';

        $queryCollection = $this->getQueryCollection();
        while ($row = $queryCollection->fetch()) {
            $imgXml .= $this->_processRow($row);
        }
        unset($queryCollection);

        return $imgXml;
    }

    protected function _processRow($row)
    {
        $imgXml = '';

        $imgUrl = $this->_getUrl($row);
        $imgXml .= sprintf('<image:image><image:loc>%s</image:loc></image:image>', $imgUrl);

        $this->_increaseLinkCounter();

        return $imgXml;
    }

    protected function _getUrl($row)
    {
        $url = '';
        $url = $this->getMediaUrl() . 'catalog/category' . $row['path'];

        return $url;
    }

    protected function getMediaUrl()
    {
        return Mage::app()
            ->getStore($this->getSitemap()->getStoreId())
            ->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA,false);
    }
}