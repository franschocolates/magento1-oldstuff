<?php

/**
 * Description of SitemapEnhancedPlus
 * @package   CueBlocks_SitemapEnhancedPlus
 * @company   CueBlocks - http://www.cueblocks.com/
 
 */
class CueBlocks_SitemapEnhancedPlus_Model_Processor_Catalog_Product_Image extends CueBlocks_SitemapEnhancedPlus_Model_Processor_Catalog_Product_Abstract
{

    protected $_sourceModel = 'sitemapEnhancedPlus/processor_catalog_product_image';
    protected $_counterLabel = 'image';

    protected function _getProcessCollection($addFirst = false)
    {
        $imgXml = '';

        $queryCollection = $this->getQueryCollection();
        while ($row = $queryCollection->fetch()) {
            $imgXml .= $this->_processRow($row);
        }
        unset($queryCollection);

        return $imgXml;
    }

    protected function _processRow($row)
    {
        $imgXml = '';
        $title = '';
        $caption = '';
        /**
         * EXCLUDE VALUE SHOULD NOT BE USED:
         * ALSO IF AN IMAGE IS EXCLUDED IT IS SHOWN
         * IF ASSIGNED TO SOMETHING
         */
//            $img_enabled = true;

//            if ($imageRow['disabled'] === '1') {
//                $img_enabled = false;
//            } elseif ($imageRow['disabled'] === null && $imageRow['disabled_default'] === '1') {
//                $img_enabled = false;
//            }

//            if ($img_enabled) {
        $location = sprintf('<image:loc>%s</image:loc>', $this->_getUrl($row));
        if ($row['label']) {
            $title = sprintf('<image:title>%s</image:title>', htmlspecialchars($row['label']));
        }
        if ($row['short_description']) {
            $caption = sprintf('<image:caption>%s</image:caption>', htmlspecialchars($row['short_description']));
        }

        $imgXml .= sprintf('<image:image>%s %s %s</image:image>', $location, $title, $caption);
        $this->_increaseLinkCounter();

        return $imgXml;
    }

    protected function _getUrl($row)
    {
        $url = '';
        $url = htmlspecialchars($this->getMediaUrl() . 'catalog/product' . $row['path']);

        return $url;
    }

    protected function getMediaUrl()
    {
        return Mage::app()
            ->getStore($this->getSitemap()->getStoreId())
            ->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA,false);
    }
}